init -1 python:
    def play_light(trans, st, at):
        renpy.play("se/light.ogg", channel="sound")

    def play_earth1(trans, st, at):
        renpy.play("se/earth1.ogg", channel="audio")

    def play_fire2(trans, st, at):
        renpy.play("se/fire2.ogg", channel="audio")

    def play_wind2(trans, st, at):
        renpy.play("se/wind2.ogg", channel="audio")

    def play_slash(trans, st, at):
        renpy.play("se/slash3.ogg", channel="audio")

    def play_slash5(trans, st, at):
        renpy.play("se/slash5.ogg", channel="audio")

    def play_dageki(trans, st, at):
        renpy.play("se/dageki.ogg", channel="sound")

init -8 python:
##########################################################################
# Функция тряски экрана
##########################################################################
    class Shaker(object):
        anchors = {
            'top' : 0.0,
            'center' : 0.5,
            'bottom' : 1.0,
            'left' : 0.0,
            'right' : 1.0,
            }

        def __init__(self, start, child, dist):
            if start is None:
                start = child.get_placement()
            #
            self.start = [ self.anchors.get(i, i) for i in start ]  # central position
            self.dist = dist    # maximum distance, in pixels, from the starting point
            self.child = child

        def __call__(self, t, sizes):
            # Float to integer... turns floating point numbers to
            # integers.
            def fti(x, r):
                if x is None:
                    x = 0
                if isinstance(x, float):
                    return int(x * r)
                else:
                    return x

            xpos, ypos, xanchor, yanchor = [fti(a, b) for a, b in zip(self.start, sizes) ]

            xpos = xpos - xanchor
            ypos = ypos - yanchor

            nx = xpos + (1.0-t) * self.dist * (rand().random()*2-1)
            ny = ypos + (1.0-t) * self.dist * (rand().random()*2-1)

            return (int(nx), int(ny), 0, 0)

    def _Shake(start, time, child=None, dist=100.0, **properties):
        move = Shaker(start, child, dist=dist)

        return renpy.display.layout.Motion(move, time, child, add_sizes=True,
                                            **properties)

    Quake = renpy.curry(_Shake)

    def _shake_function(trans, st, at, dt=.5, dist=256, xy="xy"):
        if st <= dt:
            if xy != "y":
                trans.xoffset = int((dt-st)*dist*(.5-rand().random())*2)

            if xy != "x":
                trans.yoffset = int((dt-st)*dist*(.5-rand().random())*2)

            return .01
        else:
            None

##########################################################################
# Переключатель с трансформацией
##########################################################################
    class TransitionConditionSwitch(renpy.Displayable):
        def __init__(self, transition, *args, **kwargs):
            super(TransitionConditionSwitch, self).__init__(**kwargs)
            self.transition = transition
            self.d = args
            self.time_reset = True
            self.old_d = None
            self.current_d = None
            self.ta = None
            self.old_st = 0

        def render(self, width, height, st, at):
            if self.ta is None:
                self.per_interact()
            if self.time_reset:
                self.time_reset = False
                self.st = st
                self.at = at
                self.old_st = 0
            if st < self.old_st:
                self.st, self.at, st, at = 0, 0, 1000000.0, 1000000.0
            self.old_st = st
            return renpy.render(self.ta, width, height, st-self.st, at-self.at)

        def per_interact(self):
            change_to = self.current_d #default value
            for name, d in self.d:
                if eval(name):
                    change_to = d
                    break

            if change_to is not self.current_d:
                self.time_reset = True
                self.old_d = self.current_d
                self.current_d = change_to
                if self.old_d is None:
                    self.old_d = self.current_d
                self.ta = anim.TransitionAnimation(self.old_d, 0.00, self.transition, self.current_d)
                renpy.redraw(self, 0)

        def visit(self):
            return [ self.ta ]

    def lightwave(name, p=15.0, a=5.0, s=0.25):
        renpy.transition(dissolve, layer="master")

        if isinstance(name, str):
            name = [name]

        for sprite in name:
            renpy.show(sprite, at_list=[wave(p, a, s)])

        renpy.pause(1.0, hard=True)

        for sprite in name:
            renpy.hide(sprite)
            renpy.show(sprite)

    def dissolve_show(*args, **kwargs):
        renpy.transition(dissolve, layer="master")
        renpy.show(*args, **kwargs)

    def dissolve_hide(*args, **kwargs):
        renpy.transition(dissolve, layer="master")
        renpy.hide(*args, **kwargs)

####################################################
#
# Transition
#
####################################################
define crash = ImageDissolve('images/Transition/crash.webp', 4.5)
define blinds = ImageDissolve('images/Transition/blinds.webp', 2.0)
define blinds_r = ImageDissolve(im.Flip('images/Transition/blinds.webp', horizontal=True), 2.0)
define blinds2 = MultipleTransition([False, blinds, '#000', blinds, True])
define blinds2_r = MultipleTransition([False, blinds_r, '#000', blinds_r, True])

define syasei1 = Fade(0.3, 0.5, 0.3, color="#fff")
define syasei2 = Fade(0.2, 0.2, 0.2, color="#fff")
define flash = Fade(0.3, 0, 0.3, color="#fff")
define flash2 = Fade(0.15, 0.5, 0.15, color="#fff")

define openeye1 = ImageDissolve("images/ngdata/Transitions/eye.webp", 2.0, 20)

####################################################
#
# Transforms
#
####################################################
transform monocro(color, saturation=0.0):
    matrixcolor TintMatrix(color) * SaturationMatrix(saturation)

transform wave(period, amp, speed):
    function WaveShader(period=period, amp=amp, speed=speed)

transform sk:
    on show:
        alpha 0
        align (.5, 0.05)
        xoffset -60
        linear 0.5 xoffset 0 alpha 1.0

    on hide:
        alpha 1.0
        xoffset 0
        linear 0.5 xoffset 60 alpha 0

transform shake(dt=.4, dist=128):
    function renpy.curry(_shake_function)(dt=dt, dist=dist)

transform xshake(new_widget, old_widget, dt=.4, dist=256):
    delay dt
    # truecenter

    contains:
        new_widget
        truecenter
        function renpy.curry(_shake_function)(dt=dt, dist=dist, xy="x")

    contains:
        old_widget
        truecenter
        alpha 0
        # function renpy.curry(_shake_function)(dt=dt, dist=dist, xy="x")

transform yshake(dt=.4, dist=128):
    function renpy.curry(_shake_function)(dt=dt, dist=dist, xy="y")

transform slash:
    ease .1 alpha 0
    ease .1 alpha 1.0
    repeat 3

transform xyzoom(factor):
    zoom factor

transform xzoom(factor):
    xzoom factor

transform yzoom(factor):
    yzoom factor

transform alpha_off:
    alpha 1.0

transform xy(X=0, Y=0.5, XA=0.5, YA=0.5):
    anchor (XA, YA)
    pos (X, Y)

transform miss:
    subpixel True
    linear 0.25 xoffset 80
    linear 0.25 xoffset 0

transform miss2:
    subpixel True
    zoom 1.25
    linear 0.25 xoffset 80
    linear 0.25 xoffset 0
    zoom 1.0

transform maou_cruelty:
    truecenter

    parallel:
        ease 2.0 zoom 32.0
    parallel:
        "#fff" with Dissolve(2.0, alpha=False)

transform summon_effect:
    subpixel True
    anchor (.5, .5)
    pos (0, 300)
    alpha 0.0
    easein 1.7 xpos 400 alpha 1.0 zoom 1.1
    easeout 1.7 xpos 800 alpha 0 zoom 1.0

transform summon_effect2:
    subpixel True
    alpha 1.0
    xcenter 0
    zoom 0.9
    easein 1.7 xcenter 400 zoom 1.0
    easeout 1.7 xcenter 800 zoom 0.9

transform flash_but:
    alpha 0
    .4
    alpha 1.0
    .4
    repeat

transform appear(t):
    delay t
    alpha 0.0
    subpixel True
    pause t
    linear .5 alpha 1.0

transform cutin(x, y):
    pos (x, y)
    alpha 0
    linear .7 ypos -50 alpha 1.0

transform daystar:
    align (0.5, 0.5)
    pause(1.0)
    linear 1.5 zoom 3.0

transform counter:
    align (0.5, 0.5)
    zoom 0.2
    alpha 0.0
    subpixel True
    easeout 0.75 rotate 360 zoom 1.0 alpha 1.0
    easeout 1.5 alpha 0.0
    #easein 1.0 yalign 0.20
    #easeout 1.4 yalign 1.3 alpha 0.0

transform align(x, y):
    align (x, y)

####################################################
#
# Animation
#
####################################################
image blaze:
    "images/Effects/blaze/001.webp"
    0.1
    "images/Effects/blaze/002.webp"
    0.1
    "images/Effects/blaze/003.webp"
    0.1
    "images/Effects/blaze/004.webp"
    0.1
    "images/Effects/blaze/005.webp"
    0.1
    "images/Effects/blaze/006.webp"
    0.1
    "images/Effects/blaze/007.webp"
    0.1
    "images/Effects/blaze/008.webp"
    0.1
    "images/Effects/blaze/009.webp"
    0.1
    "images/Effects/blaze/010.webp"
    0.1
    "images/Effects/blaze/011.webp"
    0.1
    "images/Effects/blaze/012.webp"
    0.1
    "images/Effects/blaze/013.webp"
    0.1
    "images/Effects/blaze/014.webp"
    0.1
    "images/Effects/blaze/015.webp"
    0.1
    "images/Effects/blaze/016.webp"
    0.1
    "images/Effects/blaze/017.webp"
    0.1
    "images/Effects/blaze/018.webp"
    0.1
    "images/Effects/blaze/019.webp"
    0.1
    "images/Effects/blaze/020.webp"
    0.1
    "images/Effects/blaze/021.webp"
    0.1
    "images/Effects/blaze/022.webp"
    0.1
    "images/Effects/blaze/023.webp"
    0.1
    "images/Effects/blaze/024.webp"
    0.1
    "images/Effects/blaze/025.webp"
    0.1
    "images/Effects/blaze/026.webp"
    0.1
    "images/Effects/blaze/027.webp"
    0.1
    "images/Effects/blaze/028.webp"
    0.1
    "images/Effects/blaze/029.webp"
    0.1
    "images/Effects/blaze/030.webp"
    0.1
    "images/Effects/blaze/031.webp"
    0.1
    "images/Effects/blaze/032.webp"
    0.1
    "images/Effects/blaze/033.webp"
    0.1
    "images/Effects/blaze/034.webp"
    0.1
    "images/Effects/blaze/035.webp"
    0.1
    "images/Effects/blaze/036.webp"
    0.1
    "images/Effects/blaze/037.webp"
    0.1
    "images/Effects/blaze/038.webp"
    0.1
    "images/Effects/blaze/039.webp"
    0.1
    "images/Effects/blaze/040.webp"
    0.1
    "images/Effects/blaze/041.webp"
    0.1
    "images/Effects/blaze/042.webp"
    0.1
    "images/Effects/blaze/043.webp"
    0.1
    "images/Effects/blaze/044.webp"
    0.1

image demon decapitation:
    "images/Effects/009_01.webp"
    pause .05
    "images/Effects/009_02.webp"
    pause .05
    "images/Effects/009_03.webp"
    pause .05
    "images/Effects/009_04.webp"
    pause .05
    "images/Effects/009_05.webp"
    pause .05

image thunder thrust a:
    "images/Effects/009a_01.webp"
    0.05
    "images/Effects/009a_02.webp"
    0.05
    "images/Effects/009a_03.webp"
    0.05
    "images/Effects/009a_04.webp"
    0.05
    "images/Effects/009a_05.webp"
    0.05

image thunder thrust b:
    "images/Effects/009b_01.webp"
    0.05
    "images/Effects/009b_02.webp"
    0.05
    "images/Effects/009b_03.webp"
    0.05
    "images/Effects/009b_04.webp"
    0.05
    "images/Effects/009b_05.webp"
    0.05

image spica:
    "images/Effects/029_01.webp"
    0.15
    "images/Effects/029_02.webp"
    0.15
    function play_earth1
    "images/Effects/029_03.webp"
    0.15
    "images/Effects/029_04.webp"
    0.15
    function play_fire2
    "images/Effects/029_05.webp"
    0.15
    "images/Effects/029_06.webp"
    0.15
    function play_wind2
    "images/Effects/029_07.webp"
    0.15
    "images/Effects/029_08.webp"
    0.15

image effect 010:
    "images/Effects/010_01.webp"
    0.05
    "images/Effects/010_02.webp"
    0.05
    "images/Effects/010_03.webp"
    0.05
    "images/Effects/010_04.webp"
    0.05

image effect 011:
    "images/Effects/011_01.webp"
    0.1
    "images/Effects/011_02.webp"
    0.1
    "images/Effects/011_03.webp"
    0.1
    "images/Effects/011_04.webp"
    0.1
    "images/Effects/011_05.webp"
    0.1

image effect 012:
    "images/Effects/012_01.webp"
    0.1
    "images/Effects/012_02.webp"
    0.1
    "images/Effects/012_03.webp"
    0.1
    "images/Effects/012_04.webp"
    0.1
    "images/Effects/012_05.webp"
    0.1
    "images/Effects/012_06.webp"
    0.1

image effect 014:
    "images/Effects/014_01.webp"
    0.1
    "images/Effects/014_02.webp"
    0.1
    "images/Effects/014_03.webp"
    0.1
    "images/Effects/014_04.webp"
    0.1
    "images/Effects/014_05.webp"
    0.1
    "images/Effects/014_06.webp"
    0.1
    "images/Effects/014_07.webp"
    0.1
    "images/Effects/014_08.webp"
    0.1
    "images/Effects/014_09.webp"
    0.1
    "images/Effects/014_10.webp"
    0.1
    "images/Effects/014_11.webp"
    0.1
    "images/Effects/014_12.webp"
    0.1
    "images/Effects/014_13.webp"
    0.1
    "images/Effects/014_14.webp"
    0.1

image effect 015:
    "images/Effects/015_01.webp"
    0.05
    "images/Effects/015_02.webp"
    0.05
    "images/Effects/015_03.webp"
    0.05
    "images/Effects/015_04.webp"
    0.05

image effect 016:
    "images/Effects/016_01.webp"
    0.05
    "images/Effects/016_02.webp"
    0.05
    "images/Effects/016_03.webp"
    0.05
    "images/Effects/016_04.webp"
    0.05
    "images/Effects/016_05.webp"
    0.05

image effect 017:
    "images/Effects/017_01.webp"
    0.05
    "images/Effects/017_02.webp"
    0.05
    "images/Effects/017_03.webp"
    0.05
    "images/Effects/017_04.webp"
    0.05
    "images/Effects/017_05.webp"
    0.05

image effect 023:
    "images/Effects/023_01.webp"
    0.05
    "images/Effects/023_02.webp"
    0.05
    "images/Effects/023_03.webp"
    0.05
    "images/Effects/023_04.webp"
    0.05

image effect 024:
    function play_slash5
    "images/Effects/024_01.webp"
    0.15
    "images/Effects/024_02.webp"
    0.15
    function play_slash5
    "images/Effects/024_03.webp"
    0.15
    "images/Effects/024_04.webp"
    0.15
    function play_slash5
    "images/Effects/024_05.webp"
    0.15
    "images/Effects/024_06.webp"
    0.15
    function play_slash5
    "images/Effects/024_07.webp"
    0.15
    "images/Effects/024_08.webp"
    0.15
    repeat 2

image effect 025:
    "images/effects/025_01.webp"
    0.1 #2
    "images/effects/025_02.webp"
    0.1 #2
    "images/effects/025_03.webp"
    0.1 #2
    function play_light
    "images/effects/025_04.webp"
    0.1 #2
    "images/effects/025_05.webp"
    0.1 #2
    "images/effects/025_06.webp"
    0.1 #2
    "images/effects/025_07.webp"
    0.1 #2
    "images/effects/025_08.webp"
    0.1

image death sword chaos star:
    function play_slash
    "images/Effects/005_01.webp"
    0.125 #2
    function play_slash
    "images/Effects/005_02.webp"
    0.125 #2
    "images/Effects/005_03.webp"
    0.125 #2
    "images/Effects/005_04.webp"
    0.125 #2
    function play_slash
    "images/Effects/005_01.webp"
    0.125 #2
    "images/Effects/005_02.webp"
    0.125 #2
    "images/Effects/005_03.webp"
    0.125 #2
    "images/Effects/005_04.webp"
    0.125 #2
    "images/Effects/005_01.webp"
    0.125 #2
    "images/Effects/005_02.webp"
    0.125 #2
    "images/Effects/005_03.webp"
    0.125 #2
    "images/Effects/005_04.webp"
    0.125 #2

image vaporizing rebellion sword:
    function play_slash
    function play_fire2
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08

image vaporizing rebellion sword typhoon:
    function play_slash
    function play_fire2
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/ngdata/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/ngdata/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/ngdata/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/ngdata/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/ngdata/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/ngdata/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/ngdata/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    function play_slash
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    function play_slash
    "images/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    function play_slash
    "images/ngdata/Effects/005_01a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/ngdata/Effects/005_02a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/ngdata/Effects/005_03a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08
    "images/ngdata/Effects/005_04a.webp"
    0.08
    "images/Effects/fire.webp"
    0.08

image spirit fight:
    function play_dageki
    Composite((388 ,352),
        (10, 45), "images/Characters/sylph/sylph st21.webp",
        (203, 0), "images/Characters/gnome/gnome st32.webp")
    0.05
    Composite((388 ,352),
        (0, 41), "images/Characters/sylph/sylph st22.webp",
        (108, 0), "images/Characters/gnome/gnome st33.webp")
    0.05
    repeat 40

image rakshasa:
    function play_slash
    "images/Effects/005_01.webp"
    0.04 #2
    "images/Effects/005_02.webp"
    0.04 #2
    "images/Effects/005_03.webp"
    0.04 #2
    "images/Effects/005_04.webp"
    0.04 #2
    repeat 9

image rakshasa wildfire:
    function play_slash
    "images/Effects/005_01a.webp"
    0.04
    "images/Effects/fire.webp"
    0.04
    "images/Effects/005_02a.webp"
    0.04
    "images/Effects/fire.webp"
    0.04
    "images/Effects/005_03a.webp"
    0.04
    "images/Effects/fire.webp"
    0.04
    "images/Effects/005_04a.webp"
    0.04
    "images/Effects/fire.webp"
    0.04
    repeat 9

image shamshir:
    "images/Effects/015_01.webp"
    0.05
    "images/Effects/015_02.webp"
    0.05
    "images/Effects/015_03.webp"
    0.05
    "images/Effects/015_04.webp"

    0.2

    "images/Effects/017_01.webp"
    0.05
    "images/Effects/017_02.webp"
    0.05
    "images/Effects/017_03.webp"
    0.05
    "images/Effects/017_04.webp"
    0.05
    "images/Effects/017_05.webp"
    0.05

####################################################
#
# Images with effects
#
####################################################
image effect heavenly demon revival = "images/Effects/001.webp"
image effect daystar = "images/Effects/002.webp"
image effect fireball = "images/ngdata/Effects/002.webp"
image effect daystar red = Transform("images/Effects/002.webp", matrixcolor=
                                        TintMatrix((190, 25, 25)) * SaturationMatrix(0))

image effect crush = "images/Effects/003.webp"
image effect slash 1 = "images/Effects/005_01.webp"
image effect slash 2 = "images/Effects/005_02.webp"
image effect slash 3 = "images/Effects/005_03.webp"
image effect slash 4 = "images/Effects/005_04.webp"

image effect slash 1 a = "images/Effects/005_01a.webp"
image effect slash 2 a = "images/Effects/005_02a.webp"
image effect slash 3 a = "images/Effects/005_03a.webp"
image effect slash 4 a = "images/Effects/005_04a.webp"

image effect 006 = "images/Effects/006.webp"
image effect serene demon sword = "images/Effects/006a.webp"
image effect flash kill = "images/Effects/007.webp"
image effect flame = "images/Effects/008.webp"

image effect magnetic storm = "images/Effects/018.webp"
image effect moonlight cannon = "images/Effects/019.webp"
image effect pulse field = "images/Effects/020.webp"
image effect holy fire = "images/Effects/021.webp"
image effect messiah = "images/Effects/022.webp"

image effect spica 1 = "images/Effects/029_01.webp"
image effect spica 2 = "images/Effects/029_02.webp"
image effect spica 3 = "images/Effects/029_03.webp"
image effect spica 4 = "images/Effects/029_04.webp"
image effect spica 5 = "images/Effects/029_05.webp"
image effect spica 6 = "images/Effects/029_06.webp"
image effect spica 7 = "images/Effects/029_07.webp"
image effect spica 8 = "images/Effects/029_08.webp"

image effect wind = "images/Effects/wind.webp"
image effect earth = "images/Effects/earth.webp"
image effect fire = "images/Effects/fire.webp"
image effect aqua = "images/Effects/aqua.webp"

# image m_circle1 = "images/Effects/m_circle1.webp"
image effect m_circle2 = "images/Effects/m_circle2.webp"
image effect thunder = "images/ngdata/Effects/thunder.webp"
