## Основное ####################################################################

## Читаемое название игры. Используется при установке стандартного заголовка
## окна, показывается в интерфейсе и отчётах об ошибках.
##
## Символы "_()", окружающие название, отмечают его как пригодное для перевода.

define config.name = "Monster Girl Quest NG+"


## Определяет, показывать ли заголовок, данный выше, на экране главного меню.
## Установите на False, чтобы спрятать заголовок.

define gui.show_name = True


## Версия игры.

define config.version = "3.3.2+2"

## Заголовок окна

define config.window_title = f'{config.name} v{config.version}'


## Короткое название игры, используемое для исполняемых файлов и директорий при
## постройке дистрибутивов. Оно должно содержать текст формата ASCII и не должно
## содержать пробелы, двоеточия и точки с запятой.

define build.name = "MGQ_NGPlus"


## Звуки и музыка ##############################################################

## Эти три переменные управляют, среди прочего, тем, какие микшеры показываются
## игроку по умолчанию. Установка одной из них в False скроет соответствующий
## микшер.

define config.has_sound = True
define config.has_music = True
define config.has_voice = True


## Чтобы разрешить игроку тестировать громкость на звуковом или голосовом
## каналах, раскомментируйте строчку и настройте пример звука для прослушивания.

define config.sample_sound = "se/m_papier03.opus"
define config.sample_voice = "voice/ilias1_08.ogg"


## Раскомментируйте следующую строчку, чтобы настроить аудиофайл, который будет
## проигрываться в главном меню. Этот файл продолжит проигрываться во время
## игры, если не будет остановлен, или не начнёт проигрываться другой аудиофайл.

define config.main_menu_music = "ngdata/bgm/gn_night.ogg"


## Переходы ####################################################################
##
## Эти переменные задают переходы, используемые в различных событиях. Каждая
## переменная должна задавать переход или None, чтобы указать на то, что переход
## не должен использоваться.

## Вход и выход в игровое меню.

define config.enter_transition = dissolve
define config.exit_transition = dissolve


## Переход между экранами игрового меню.

define config.intra_transition = dissolve

## Вызов функций при загрузке сохранения

define config.after_load_callbacks = [game_update]

## Переход, используемый после загрузки слота сохранения.

define config.after_load_transition = dissolve


## Используется при входе в главное меню после того, как игра закончится.

define config.end_game_transition = blinds


## Переменная, устанавливающая переход, когда старт игры не существует. Вместо
## неё используйте функцию with после показа начальной сценки.


## Управление окнами ###########################################################
##
## Эта строка контролирует, когда появляется диалоговое окно. Если "show" — оно
## всегда показано. Если "hide" — оно показывается, только когда представлен
## диалог. Если "auto" — окно скрыто до появления оператора scene и показывается
## при появлении диалога.
##
## После начала игры этот параметр можно изменить с помощью "window show",
## "window hide" и "window auto".

define config.window = "auto"


## Переходы, используемые при показе и скрытии диалогового окна

define config.window_show_transition = Dissolve(.2)
define config.window_hide_transition = Dissolve(.2)


## Стандартные настройки #######################################################

## Контролирует стандартную скорость текста. По умолчанию, это 0 — мгновенно,
## в то время как любая другая цифра — это количество символов, печатаемых в
## секунду.

default preferences.text_cps = 50


## Стандартная задержка авточтения. Большие значения означают долгие ожидания, а
## от 0 до 30 — вполне допустимый диапазон.

default preferences.afm_time = 15


## Директория сохранений #######################################################
##
## Контролирует зависимое от платформы место, куда Ren'Py будет складывать файлы
## сохранения этой игры. Файлы сохранений будут храниться в:
##
## Windows: %APPDATA\RenPy\<config.save_directory>
##
## Macintosh: $HOME/Library/RenPy/<config.save_directory>
##
## Linux: $HOME/.renpy/<config.save_directory>
##
## Этот параметр обычно не должен изменяться, а если и изменился, должен быть
## текстовой строчкой, а не выражением.

define config.save_directory = None


## Иконка ######################################################################
##
## Иконка, показываемая на панели задач или на dock.

define config.window_icon = "window_icon.webp"

# Курсор
define config.mouse = {"default": [("mouse/cursor.webp", 0, 0)]}

# Автоскрытие диалога в этих функциях
define config.window_auto_hide = ["scene", "city", "cmd", "hp", "menu", "map"]

# Переопределить функции показа и скрытия для плавности
define config.show = dissolve_show
define config.hide = dissolve_hide

# Всегда включен для отладки пользователями
define config.developer = True

# Логирование диалогов
define config.log = "say.txt"

# Локальное сохранение скриншотов, ибо лень тянуться
define config.screenshot_pattern = f"{renpy.sys.path[0]}/screenshots/screenshot%04d.png"

# Чтобы помещалось
define config.autosave_slots = 6
define config.quicksave_slots = 6

define config.gestures["s_e"] = "game_menu"
define config.gestures["w"] = "K_PAGEUP"
define config.gestures["e"] = "K_PAGEDOWN"

define config.image_cache_size = 32

# Авто-индексирование ресурсов
define config.search_prefixes = ["", "audio/", "fonts/", "gui/", "images/"]

# Принудительное включение OpenGL 2.0
define config.gl2 = True

init python:
    # регистрируем дополнительлные аудио каналы
    renpy.music.register_channel("sound2", mixer='sfx', loop=False)
    renpy.music.register_channel("hseanwave", mixer='sfx', loop=True)
    renpy.music.register_channel("hseanvoice", mixer='voice', loop=False)

    def get_what(d):
        if _last_say_what is None:
            d['_save_name'] = ""
        else:
            last_say = renpy.substitute(getattr(store, "_last_say_what", ""))
            last_say = re.sub("\{.+?\}", "", last_say)
            last_say = "%s..." % last_say[0:80].replace("\n", " ")
            d['_save_name'] = last_say

    config.save_json_callbacks = [get_what]

## Настройка Дистрибутива ######################################################
##
## Эта секция контролирует, как Ren'Py строит дистрибутивные файлы из вашего
## проекта.

init python:
    build.directory_name = "MGQ-NGPlus"
    build.executable_name = "MGQ_NGPlus"

    ## Следующие функции берут образцы файлов. Образцы файлов не учитывают
    ## регистр и соответствующе зависят от директории проекта (base), с или без
    ## учёта /, задающей директорию. Если обнаруживается множество одноимённых
    ## файлов, то используется только первый.
    ##
    ## Инструкция:
    ##
    ## / — разделитель директорий.
    ##
    ## * включает в себя все символы, исключая разделитель директорий.
    ##
    ## ** включает в себя все символы, включая разделитель директорий.
    ##
    ## Например, "*.txt" берёт все файлы формата txt из директории base, "game/
    ## **.ogg" берёт все файлы ogg из директории game и всех поддиректорий, а
    ## "**.psd" берёт все файлы psd из любого места проекта.

    ## Классифицируйте файлы как None, чтобы исключить их из дистрибутивов.

    build.classify('**~', None)
    build.classify('**/src', None)
    build.classify('**.bak', None)
    build.classify('**/.**', None)
    build.classify('**/#**', None)
    build.classify('**/thumbs.db', None)
    build.classify("**.xcf", None)
    build.classify("**.bat", None)
    build.classify("tools", None)

    ## Чтобы архивировать файлы, классифицируйте их, например, как 'archive'.

    # build.archive("scripts", "all")
    # build.archive("images", "all")
    # build.archive("gui", "all")
    # build.archive("audio", "all")
    # build.archive("fonts", "all")

    # build.classify("game/**.rpy", "scripts")
    # build.classify("game/**.rpyc", "scripts")

    # build.classify("game/audio/**.ogg", "audio")
    # build.classify("game/audio/**.opus", "audio")

    # build.classify("game/images/**.webp", "images")

    # build.classify("game/presplash*.png", "gui")
    # build.classify("game/icon.png", "gui")
    # build.classify("game/icon.ico", "gui")
    # build.classify("game/gui/**.webp", "gui")

    # build.classify("game/fonts/**.ttf", "fonts")
    # build.classify("game/fonts/**.otf", "fonts")

    ## Файлы, соответствующие образцам документации, дублируются в приложениях
    ## Mac, чтобы они появлялись и в приложении, и в zip архиве.

    build.documentation('*.html')
    build.documentation('*.txt')

    build.packages = []
    # bzip2 очень медленный, а выигрыш по размеру незначителен.
    build.package("linux", "zip", "linux linux_arm renpy all", "Linux")
    build.package("mac", "app-zip app-dmg", "mac renpy all", "Macintosh")
    build.package("win", "zip", "windows renpy all", "Windows")
    build.package("android", "directory", "android all", hidden=True, update=False, dlc=True)
    build.package("web", "zip", "web renpy all")

    build.include_i686 = False
