label slime_start:
    python:
        world.troops[0] = Troop(world)
        world.troops[0].enemy[0] = Enemy(world)

        world.troops[0].enemy[0].name = Character("Девушка-слизь")
        world.troops[0].enemy[0].sprite = ["slime st01", "slime st02", "slime st03"]
        world.troops[0].enemy[0].position = center
        world.troops[0].enemy[0].defense = 100
        world.troops[0].enemy[0].evasion = 95
        world.troops[0].enemy[0].max_life = world.troops[0].battle.difficulty(20, 30, 40)
        world.troops[0].enemy[0].power = world.troops[0].battle.difficulty(0, 1, 2)

        world.troops[0].battle.tag = "slime"
        world.troops[0].battle.name = world.troops[0].enemy[0].name
        world.troops[0].battle.background = "bg 004"
        world.troops[0].battle.music = 1
        world.troops[0].battle.exit = "lb_0002"

        world.troops[0].battle.init()

    world.battle.enemy[0].name "Ха-ха-ха, а вот и вкусный на вид парнишка показался!"
    "Слизь засмеялась.{w}\nВсё её вязкое тело подрагивало и покачивалось во время смеха."
    l "..."
    "Это действительно девушка-слизь из начала моего приключения.{w}\nНо как это вообще возможно?!"
    world.battle.enemy[0].name "Ой-ой-ой? Неужели я первый монстр, которого ты увидел?"
    l "... Технически да.{w}\nТы была первым монстром, которого я повстречал."
    "Ну, по крайней мере теперь у меня есть Сияние Ангела.{w}\nК томе же, кольцо моей матери целое."
    "Может быть у меня всё ещё есть свои силы?"

    pause 1.0

    "Это может быть опасно, но я должен знать!"
    "Я быстро снимаю своё кольцо и..."

    $ world.party.player.skill_set(2)

label slime_v:
    show slime st03

    world.battle.enemy[0].name "Ай!"

    play sound "se/escape.ogg"
    hide slime

    "Девушка-слизь убегает в ужасе!"

    stop music fadeout 1.0

    "............................"

    $ world.battle.victory(2)

label slug_start:
    python:
        world.troops[1] = Troop(world)
        world.troops[1].enemy[0] = Enemy(world)

        world.troops[1].enemy[0].name = Character("Девушка-слизняк")
        world.troops[1].enemy[0].sprite = ["slug st11", "slug st02", "slug st12"]
        world.troops[1].enemy[0].position = xy(0.45)
        world.troops[1].enemy[0].defense = 100
        world.troops[1].enemy[0].evasion = 95
        world.troops[1].enemy[0].max_life = world.troops[1].battle.difficulty(25, 30, 35)
        world.troops[1].enemy[0].power = world.troops[1].battle.difficulty(0, 1, 2)

        world.troops[1].battle.tag = "slug"
        world.troops[1].battle.name = world.troops[1].enemy[0].name
        world.troops[1].battle.background = "bg 008"
        world.troops[1].battle.music = 1
        world.troops[1].battle.exp = 10
        world.troops[1].battle.exit = "lb_0005"

        world.troops[1].battle.init()

    l "Что за?.."
    "На первый взгляд это была прекрасная женщина.{w}\nОднако её нижняя половина тела была как у слизняка."
    world.troops[1].enemy[0].name "... Путешественник?{w}\n... К тому же некрещённый. Ты выглядишь аппетитно."
    l "Ну если ты так хочешь драки."

    $ world.party.player.skill_set(2)

    world.troops[1].enemy[0].name "Ух...{w}\nВозможно мне следует просто уйти..."
    l "Если ты атакуешь невинных людей, то так просто отпустить тебя я не могу!"
    world.troops[1].enemy[0].name "... Ох.{w}\nО боже."
    world.troops[1].enemy[0].name "... Но мне всего лишь нужно заставить тебя кончить, чтобы победить..."
    "Похоже она всерьёз собирается сражаться.{w}\nУ меня не было выбора!"

    $ world.battle.main()

label slug_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label slug_v:
    $ world.battle.face(3)

    world.troops[1].enemy[0].name "Ааах!"

    play sound "se/syometu.ogg"
    hide slug with crash

    "Девушка-слизняк была запечатана в слизня!"

    $ world.battle.victory(1)

label slug_a:
    if world.party.player.bind == 1:
        call slug_a4
        $ world.battle.main()

    while True:
        $ random = rand().randint(1, 3)

        if random == 1:
            call slug_a1
            $ world.battle.main()
        elif random == 2:
            call slug_a2
            $ world.battle.main()
        elif random == 3 and world.battle.delay[0] > 2:
            call slug_a3
            $ world.battle.main()

label slug_a1:
    $ world.battle.skillcount(1, 3004)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я дам тебе попробовать моей клейкой слизи.",
        "Моя клейкая слизь... восхитительна, да?",
        "Я покрою твой член ещё большим количеством слизи..."
    ])

    $ world.battle.show_skillname("Липкая слизь")
    play sound "se/ero_slime2.ogg"

    "Слизняк обливает липкой слизью пах Луки!{w}\nЛука покрыт слизью!"

    python:
        world.battle.enemy[0].damage = {1: (3, 4), 2: (4, 6), 3: (6, 8)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label slug_a2:
    $ world.battle.skillcount(1, 3005)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "...Липко-липко...",
        "Разве липкая слизь не приятна на ощупь?",
        "Моё тело липкое, не так ли?"
    ])

    $ world.battle.show_skillname("Липкая ласка")
    play sound "se/ero_name1.ogg"

    "Слизь трётся своим липким телом о Луку!{w}\nЛука покрыт липкой слизью!"

    python:
        world.battle.enemy[0].world.battle.enemy[0].damage = {1: (4, 5), 2: (6, 7), 3: (8, 10)}
        world.battle.enemy[0].world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label slug_a3:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Я вся такая липкая...{w} ... я заберусь на тебя..."

        $ world.battle.show_skillname("Позиция: Сверху")

    play sound "se/ero_name2.ogg"

    "Слизняк забирается на Луку!{w}\nЛуку придавило липким телом Слизняка!"

    python:
        world.battle.enemy[0].damage = {1: (4, 5), 2: (6, 7), 3: (6, 7)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.party.player.bind = 1
        world.party.player.status_print()

    "Луку придавило липким телом Слизняка!"

    $ world.battle.hide_skillname()

    python:
        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.surrendered:
            renpy.return_statement()

    world.battle.enemy[0].name "Вот... отведай моего липкого ада!{w}\nИ когда ты не сможешь больше сдерживаться... выплесни всё!"

    if persistent.difficulty == 1:
        $ world.battle.enemy[0].power = 1
    elif persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 1
    elif persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 2

    return

label slug_a4:
    $ world.battle.skillcount(1, 3006)

    "Слизняк сидит верхом на Луке"

    python:
        world.battle.cry(world.battle.enemy[0].name, [
            "Сопротивляться бесполезно... ты можешь только стать моей едой...",
            "Вот тебе ещё липкой слизи...",
            "Я ещё немного с тобой поиграю...",
            "Хо-хо... тебе нравится?",
            "Как тебе?..",
            "Чем больше ты сопротивляешься, тем приятнее тебе будет...",
            "Я могу двигаться так, что тебе будет ещё приятнее!",
            "Тебе нравится это клейкое чувство?..",
            "Твоему члену, должно быть, хорошо...",
            "Дать тебе кончить сейчас?.."
        ])

        world.battle.show_skillname("Изнасилование Слизняка")
        renpy.sound.play("se/ero_name2.ogg")

        world.battle.cry(narrator, [
            "Слизняк прижимает к Луке своё упругое липкое тело!",
            "Из Слизняка начинает течь слизь, заливающая Луку!",
            "Лука весь покрыт слизью, его будто лижут сотни языков.",
            "Лука всем телом ощущает липкое удовольствие!",
            "Член Луки тонет в липкой дырочке Слизняка!",
            "Слизняк не выпускает член Луки!",
            "Слизняк начинает двигаться, всё ещё сидя верхом на Луке!",
            "Член Луки трогает что-то склизкое и упругое!",
            "Липкая слизь, окружающая член Луки, начинает тянуть и гладить его!",
            "Слизняк сжимается вокруг члена Луки!"
        ], True)

        world.battle.enemy[0].damage = {1: (4, 5), 2: (6, 7), 3: (7, 10)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label slug_kousan:
    $ world.battle.common_surrender()

    "Лука поддаётся на обещания наслаждения."

    $ world.battle.face(2)

    world.battle.enemy[0].name "Так ты хочешь испытать липкий ад...{w}\nЛадно... Пробуй сколько пожелаешь!"

    $ world.battle.enemy[0].attack()

label slug_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Липкая слизь"
    $ list2 = "Липкая ласка"
    $ list3 = "Изнасилование Слизняка"

    if persistent.skills[3004][0]:
        $ list1_unlock = 1

    if persistent.skills[3005][0]:
        $ list2_unlock = 1

    if persistent.skills[3006][0]:
        $ list3_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump slug_onedari1
    elif world.battle.result == 2:
        jump slug_onedari2
    elif world.battle.result == 3 and not world.party.player.bind:
        jump slug_onedari3
    elif world.battle.result == 3 and world.party.player.bind == 1:
        jump slug_onedari4
    elif world.battle.result == -1:
        jump slug_main


label slug_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "Ты хочешь покрыть член моей слизью?..{w}\nХорошо... Можешь хоть утонуть в ней!"

    while True:
        call slug_a1


label slug_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "Хочешь, чтобы я накрыла тебя своим липким телом..?{w}\nЛадно... Ты станешь его пленником!"

    while True:
        call slug_a2


label slug_onedari3:
    call onedari_syori

    world.battle.enemy[0].name "Хочешь испытать липкий ад?..{w}\nЛадно... Можешь пробовать сколько душе угодно!"

    call slug_a3

    world.battle.enemy[0].name "Кончай прямо мне в слизь..."

    while True:
        call slug_a4


label slug_onedari4:
    call onedari_syori

    world.battle.enemy[0].name "Играть с тобой так и дальше?..{w}\nЛадно, я буду играть, пока ты не кончишь..."

    while True:
        call slug_a4


label slug_h1:
    $ world.party.player.moans(1)
    with syasei1
    show slug bk02 at xy(X=183) zorder 10 as bk
    $ world.ejaculation()

    "Luka orgasms, shooting semen from his mucus covered dick!"

    $ world.battle.lose()

    $ world.battle.count([11, 1])
    $ bad1 = "Covered in sticky mucus, Luka ejaculated."
    $ bad2 = "From then on, Luka spent the rest of his life as the Slug's mate."
    $ world.battle.face(2)

    world.battle.enemy[0].name "You let it out... haha."
    "I ejaculated on the Slug's body...{w}\nThe proof of my defeat starts to get absorbed by the Slug's membrane..."

    hide bk
    jump slug_h


label slug_h2:
    $ world.party.player.moans(1)
    with syasei1
    show slug bk03 at xy(X=183) zorder 10 as bk
    $ world.ejaculation()

    "Unable to resist the sticky mucus, I ejaculate, shooting out my semen all over the Slug's membrane."

    $ world.battle.lose()

    $ world.battle.count([15, 1])
    $ bad1 = "Luka fell to the Slug's sticky caress."
    $ bad2 = "From then on, Luka spent the rest of his life as the Slug's mate."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Your sperm... delicious."
    "The proof of my defeat starts to get absorbed by the Slug's membrance..."

    hide bk
    jump slug_h


label slug_h3:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Oh... too quick."

    with syasei1
    show slug bk03 at xy(X=183) zorder 10 as bk
    $ world.ejaculation()

    "With the Slug only climbing onto me, I'm unable to resist any longer.{w}\nMy semen shoots out all over the Slug's membrane..."

    $ world.battle.lose()

    $ world.battle.count([15, 1])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka, unable to resist the Slug climbing on top of him, ejaculates."
    $ bad2 = "From then on, Luka spent the rest of his life as the Slug's mate."

    world.battle.enemy[0].name "...You weren't able to resist it?{w}\nWell, you'll still get to enjoy my sticky hell..."
    "The proof of my defeat starts to get absorbed by the Slug's membrance..."
    world.battle.enemy[0].name "Your semen... delicious."

    hide bk
    jump slug_h


label slug_h4:
    $ world.party.player.moans(2)
    with syasei1
    show slug bk03 at xy(X=183) zorder 10 as bk
    $ world.ejaculation()

    "With the slug raping me, I'm unable to resist the pleasure any longer."
    "My semen shoots out all over the Slug's membrane..."

    $ world.battle.lose()

    $ world.battle.count([15, 1])
    $ bad1 = "Raped by the Slug, Luka is unable to resist the pleasure."
    $ bad2 = "From then on, Luka spent the rest of his life as the Slug's mate."

    world.battle.enemy[0].name "Your semen... delicious."

    hide bk

    "The proof of my defeat starts to get absorbed by the Slug's membrance..."

    jump slug_h


label slug_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    world.battle.enemy[0].name "Hey... don't you want to do something even better?"
    "The Slug girl whispered into my ear as I lay on the ground in defeat."
    world.battle.enemy[0].name "Mate with me...{w}\nJust stick it in this hole right here..."
    "The Slug slowly crawls over my body, and opens up a hole in her membrane."
    world.battle.enemy[0].name "Once you put your dick in here, you won't need to think about anything else...{w}\nThe only thing you need to do is give me your semen, over and over..."
    l "Ahh... uuu...."
    "Though the hole looked similar to a woman's, it had mucus dripping out of it."
    "That sticky, stretchy mucus... what is it going to feel like..."
    world.battle.enemy[0].name "Fufu... do you want to put it in?{w}\nMate with me, so I can become pregnant..."

    play sound "se/ero_name2.ogg"

    "The mucus covered hole starts to wriggle, as if inviting me."
    l "Auu...."
    "The excitement and horror mixes in me, giving me conflicting feelings..."
    world.battle.enemy[0].name "If you won't put it in yourself... I'll do it by force.{w}\nYou don't have the power to resist me anymore, after all."
    l "St... stop!"

    play hseanwave "se/hsean01_innerworks_a1.ogg"
    hide bk
    show slug h01 at center
    with Dissolve(1.5)

    "The Slug's sticky body leans into me, wrapping me in her viscous membrane."
    l "Ahhhh!"
    "My penis is sucked into the surprisingly warm, mucus filled hole."
    world.battle.enemy[0].name "I'll melt your clothes...{w}\nYou want to feel me with your bare skin, don't you?"
    "The mucus that was covering my body starts to melt off my clothes.{w}\nI can feel the sticky mucus covering my body all over with my bare skin..."
    l "Fuuaaa!"
    "Covered by the Slug, I let loose a pleasure filled sigh.{w} I can feel my penis get even harder inside the Slug."
    world.battle.enemy[0].name "This is how a Slug mates...{w}\nWe wrap up the male, so they can't move...{w}\nAnd then violate them!"
    l "Auuu..."
    "My dick is stuck in the mucus filled hole that the Slug showed me before...{w}\nI'm unable to focus on anything but the sticky feeling filling my member..."
    world.battle.enemy[0].name "Don't let it out yet...{w} I want it in my deepest part..."
    l "Ugh! Uuu...."

    play hseanwave "se/hsean04_innerworks_a4.ogg"

    "The Slug moves her waist, forcing me even deeper into her."

    show slug h02

    "Her rubbery walls pull my dick deeper and deeper into her hole..."
    l "Ahhhg!"
    "Even deeper inside of her, it felt surprisingly slippery.{w}\nThe entrance was sticky, but the inside was slippery... the alternating textures caused ecstasy to fill my body with every movement."
    "With such amazing pleasure like this, it felt like I was in heaven."
    l "Uhhgg... I... I can't take it anymore!{w}\nI'm going to come!"
    world.battle.enemy[0].name "Already coming?{w} I thought you would last longer..."
    "The Slug starts to quickly move her body, causing intense stimulation from the sticky mucus moving back and forth."
    l "Ahhhhhh!{w}\nI... if you move like that!"
    world.battle.enemy[0].name "Let it out just like that...{w}\nGive me your semen... so I can have your children..."
    "Mating with a Slug...{w}\nThe immoral feeling filled me with even more sick pleasure."
    "Being forcefully raped like this dealt a critical blow to my male pride."
    l "C...coming!{w} Ahhhhh!"

    with syasei1
    show slug h03
    $ world.ejaculation()

    "I shoot my semen inside the soft walls of the Slug's hole.{w}\nI'm being raped by a Slug... but I still orgasmed in her..."
    world.battle.enemy[0].name "Haha, I can feel them inside me...{w} Your energetic little children..."
    l "No... I let it out...{w}\nEven though I'm a human, I just mated with a Slug..."
    "Breaking one of the strictest taboos of humanity, I was filled with a sickly pleasure."
    "I don't think I can escape from this pleasure...{w} If I just accept that, then I can just enjoy this... right?"
    world.battle.enemy[0].name "Mating with me... how was it?{w}\nDo you want even more?.."
    world.battle.enemy[0].name "Over and over again...{w}\nI'll have lots of your children."
    l "Auu..."
    world.battle.enemy[0].name "More... more...{w} Give me more of your semen..."
    l "Ahh!{w}\nAuu..."

    play hseanwave "se/hsean08_innerworks_a8.ogg"

    "The slippery interior of the Slug starts to rub against my penis, bringing me to the height of ecstasy."
    l "Ahh.. inside... mov..ing...{w}\nIf you do that... I... come again..."
    world.battle.enemy[0].name "Come as much as you want...{w}\nGive me more of your children..."
    "Her slimy mucus filled hole wrapped around my dick starts to squirm around."
    l "Am... amazing!{w}\nToo... too good!"

    with syasei1
    show slug h04
    $ world.ejaculation()

    "I quickly ejaculate a third time..."
    world.battle.enemy[0].name "You let out a lot...{w} But I still want more..."
    l "Haa...."
    "My dick is continually squeezed inside of the Slug, without being given a moment to rest."
    "I've completely surrendered to mating with the Slug..."
    world.battle.enemy[0].name "You'll be my mate...{w}\nI won't let you go..."
    l "Faaaa!"

    with syasei1
    $ world.ejaculation()

    "While coming a fourth time, my body writhes in pleasure.{w}\nFrom now on, I'll keep giving this Slug my semen..."
    world.battle.enemy[0].name "More... more..."
    l "Ahhhh!"

    with syasei1
    $ world.ejaculation()
    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "My adventure ends here...{w}\nI'll live out the rest of my life as the Slug's mate..."
    "It might not be so bad..."
    "........."

    jump badend

label granberia_ng_start:
    python:
        world.troops[2] = Troop(world)
        world.troops[2].enemy[0] = Enemy(world)

        world.troops[2].enemy[0].name = Character("Гранберия")
        world.troops[2].enemy[0].sprite = ["granberia st41", "granberia st42", "granberia st07"]
        world.troops[2].enemy[0].position = center
        world.troops[2].enemy[0].defense = 70
        world.troops[2].enemy[0].evasion = 80
        world.troops[2].enemy[0].max_life = world.troops[2].battle.difficulty(32000, 40000, 52000)
        world.troops[2].enemy[0].henkahp[0] = world.troops[2].battle.difficulty(8000, 9000, 10000)
        world.troops[2].enemy[0].power = world.troops[2].battle.difficulty(0, 1, 2)

        world.troops[2].battle.tag = "granberia_ng"
        world.troops[2].battle.name = world.troops[2].enemy[0].name
        world.troops[2].battle.background = "bg 013"
        world.troops[2].battle.music = 4
        world.troops[2].battle.ng = True
        world.troops[2].battle.exp = 1500000
        world.troops[2].battle.exit = "lb_0008"

        world.troops[2].battle.init(0)

        world.party.player.earth_keigen = 50
        world.party.player.skill_set(2)

    pause .5

    l "Тебе же лучше сдаться, Гранберия!{w}\nПрекрати нападать на невинных людей и захватывать города! В противном случае я буду вынужден применить силу!"
    g "Ты доволно самоуверен, не так ли?{w}\nЧто ж, посмотрим на что ты способен!"

    $ world.battle.show_skillname("Рассекающий удар")
    play sound "se/karaburi.ogg"

    "Гранберия взмахивает своим мечом, кося всё на своём пути!"

    $ world.battle.show_skillname("Шаг в сторону")
    play sound "se/miss.ogg"
    $ renpy.show(world.battle.background, at_list=[miss2])

    "Но Лука легко уклоняется от удара просто отшагнув!"

    $ world.battle.hide_skillname()
    show granberia st04

    g "Хмпф.{w}\nЭто сложнее, чем я думала..."
    l "Ты действительно надеялась, что это будет так легко?"

    show granberia st02

    g "Не зазнавайся!"

    show granberia st41

    g "Как тебе такое?!"

    $ world.battle.show_skillname("Удар Дракона-Мясника")
    play sound "se/karaburi.ogg"

    "Меч Гранберии обрушивается на Луку!"

    pause .3
    $ world.battle.show_skillname("Безмятежное движение")
    play sound "se/miss_aqua.ogg"
    $ renpy.show(world.battle.background, at_list=[miss2])

    "Но Лука уклоняется, словно поток воды, обегающий препятствие!"

    $ world.battle.hide_skillname()
    show granberia st06

    g "Что?..{w} Это движение!"

    show granberia st07

    g "Где ты ему научился?!{w}{nw}"

    show granberia st41

    extend "\nВпрочем, не важно, хватит уже этих игр."

    show granberia st04

    $ world.battle.show_skillname("Безмятежный Демонический Клинок")
    play sound "se/aqua2.ogg"

    "Гранберия вкладывает свой меч в ножны.{w}{nw}"

    show granberia st41
    $ world.party.player.skill13a()

    extend "\nИ резко вынимая, прорезает всё на своём пути!"

    $ world.battle.show_skillname("Безмятежное движение")
    play sound "se/miss_aqua.ogg"
    $ renpy.show(world.battle.background, at_list=[miss2])

    "Но Лука едва уходит от атаки!"

    $ world.battle.hide_skillname()
    show granberia st07

    g "!!!"
    l "Я не позволю тебе захватить Илиасбург!"

    $ world.battle.counter()
    $ world.battle.show_skillname("Денница")

    "Ослепительная звезда обрушивается в ад!"

    python:
        world.party.player.skill22a()
        for line in range(3):
            renpy.sound.play("se/damage2.ogg")
            world.party.player.damage = rand().randint(3200,3600)
            if not line:
                _window_show()
                renpy.show_screen("hp")
                _window_auto = True

            world.battle.enemy[0].raw_life_calc(line+1)

        world.battle.hide_skillname()

    show granberia st06

    g "!!!{w}\nЭта сила!.."
    "На мгновение Гранберия застыла в неподвижной позе, шокированная моей атакой."

    show granberia st04
    stop music fadeout 1.0

    g "Хорошо.{w}\nЕсли ты так силён..."

    show granberia st41

    if persistent.music:
        if persistent.difficulty == 3:
            $ world.battle.music = 28
        else:
            $ world.battle.music = 20

    $ world.battle.musicplay(world.battle.music)

    g "Тогда время игр закончено, пацан!"

    $ world.battle.show_skillname("Ярость")
    play sound "se/fire4.ogg"
    show fire
    show granberia st42
    show effect flame
    $ world.battle.enemy[0].sprite[0] = "granberia st42"
    pause .5
    hide fire
    hide effect flame

    "Языки пламени пробегают по мечу Гранберии, воспламеняя её дух!"

    $ world.battle.enemy[0].life_regen(world.battle.enemy[0].max_life - world.battle.enemy[0].life)
    $ world.battle.hide_skillname()

    g "Покажи свои лучшие приёмы, малыш!{w}\nТебе они понадобятся!"
    l "С удовольствием..."
    "Я до сих пор не совсем понимаю, что тут творится...{w}\nНо если мне снова придётся победить Гранберию, то так тому и быть!"

    if persistent.difficulty == 3:
        $ world.battle.nostar = 1
        if _in_replay:
            $ world.party.player.skill[0] = 26

    $ world.battle.main()

label granberia_ng_main:
    python:
        if world.battle.result == 15:
            renpy.jump(f"{world.battle.tag}_edging")

        if world.battle.enemy[0].life < world.battle.enemy[0].henkahp[0] and not world.battle.progress:
            renpy.jump(f"{world.battle.tag}_001")

        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1 and world.party.player.aqua:
            world.battle.common_attack()
        elif world.battle.result == 1 and world.party.player.aqua < 1:
            renpy.jump(f"{world.battle.tag}_miss")
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label granberia_ng_nostar:
    call granberia_ng_a10

    jump granberia_ng_main

label granberia_ng_edging:
    show granberia st02

    g "Это ещё что, чёрт возьми, за приём такой?!"
    l "Ух-ох...{w}\nОбычно я получаю возможность снова атаковать после его использования..."
    "Возможно я как-то не продумал данный ход..."

    show granberia st04

    g "... Хорошо.{w}\nЕсли ты так хочешь этого..."

    show granberia st41

    g "Тогда я просто тебя убью!"

    play sound "se/karaburi.ogg"
    show effect serene demon sword zorder 30
    pause .3
    hide effect with ImageDissolve("images/Transition/mask01.webp", 0.5)
    play sound "se/damage2.ogg"
    with Quake((0, 0, 0, 0), 1.0, dist=15)
    $ world.party.player.life = 0
    pause .5
    stop music fadeout 1.0

    show bg 013 at monocro("#c00")
    show granberia st41 at monocro("#c00")
    with Dissolve(1.5)

    "Я чувствую, как острый клинок пронзает насквозь моё тело. Моё сознание начинает постепенно угасать..."

    hide screen hp
    scene bg black with Dissolve(2.0)
    $ world.battle.bad = [
        "Будучи полным придурком, коим он, собственно, и является, Лука попытался использовать крайность против Гранберии.",
        "Она просто убила его за это.",
    ]

    $ world.battle.badend()

label granberia_ng_miss:
    $ world.party.player.before_action = 3
    $ world.battle.cry(world.party.player.name[0], [
        "Хаа!",
        "Ораа!",
        "Получай!"
    ])

    "Лука атакует!"

    $ world.party.player.mp_regen()
    play sound "se/karaburi.ogg"
    $ world.battle.enemy[0].evade()

    "Но Гранберия легко уклоняется!"

    if world.battle.first[0]:
        $ world.battle.enemy[0].attack()

    $ world.battle.first[0] = True

    l "Что за?..{w}\nЯ промахнулся?!"
    g "Ты действительно надеялся, что это будет так легко?{w}\nАтаки, подобно этой, сродни укусам комаров!"
    "Обычные атаки против неё бесползены...{w}\nМожет мне попробывать что-то ещё?"

    $ world.battle.enemy[0].attack()

label granberia_ng_001:
    show granberia st07

    g "Это... просто нереально!.."
    g "Что ты за безумный демон такой?!"
    g "Кто-то с подобной силой сражается на стороне Илиас..."
    l "Я не сражаюсь за Илиас!"
    l "Просто сдайся, Гранберия!{w}\nЯ правда не хочу запечатывать тебя..."

    show granberia st02

    g "Ни за что... "

    show granberia st41

    extend "Я не могу оставить кого-то, подобно тебе, в живых!"

    show granberia st42
    $ world.battle.show_skillname("Ярость")
    play sound "se/fire4.ogg"
    show fire
    show granberia st42
    show effect flame
    pause .5

    "Языки пламени пробегают по мечу Гранберии, полностью восстанавливая её боевой дух!"

    hide fire
    hide effect flame
    $ world.battle.hide_skillname()

    l "!"
    "Она не собирается сдаваться...{w}\nОна всерьёз планирует драться до смерти одного из нас!{w}\nНеужели мне действительно придётся её запечатать?.."

    $ world.battle.progress = 1

    $ world.battle.enemy[0].attack()

label granberia_ng_v:
    if persistent.difficulty == 3:
        $ persistent.granberia_hell = True

    show granberia st07

    g "Нгх...{w}\nЧёрт бы тебя побрал!.."

    play sound "se/down.ogg"

    "Гранберия обессиленно падает на колени."
    l "Прости меня, Гранберия, но я сумел защитить Илиасбург.{w}\nТвоя осада провалилась."
    l "Если ты согласна прекратить нападать на людей, я позволю тебе уйти."
    g "Никогда..."

    stop music fadeout 1.0

    a "Эй, идиоты."

    show alice st11b at xy(0.25)
    show granberia st07 at xy(0.75)

    a "Прекращайте."
    l "Алиса?"

    show granberia st06

    g "?!{w} Т-ты!"

    $ renpy.end_replay()

    $ world.battle.victory(1)

label granberia_ng_a:
    if world.battle.enemy[0].life < world.battle.enemy[0].henkahp[0] and not world.battle.progress:
        jump granberia_ng_001

    if world.battle.progress:
        while True:
            if not world.party.player.aqua:
                $ random = rand().randint(1, 3)

                if random == 1:
                    call granberia_ng_a9
                    $ world.battle.main()
                elif random == 2:
                    call granberia_ng_a8
                    $ world.battle.main()
                elif random == 3:
                    call granberia_ng_a7
                    $ world.battle.main()
            else:
                $ random = rand().randint(1, 5)

                if random == 1:
                    call granberia_ng_a9
                    $ world.battle.main()
                elif random == 2:
                    call granberia_ng_a8
                    $ world.battle.main()
                elif random > 2:
                    call granberia_ng_a7
                    $ world.battle.main()

    else:
        while True:
            $ random = rand().randint(1, 9)

            if not world.party.player.aqua and random < 3:
                call granberia_ng_a1
                $ world.battle.main()
            elif not world.party.player.aqua and 2 < random < 5:
                call granberia_ng_a2
                $ world.battle.main()
            elif not world.party.player.aqua and 4 < random < 7:
                call granberia_ng_a3
                $ world.battle.main()

            elif world.party.player.aqua and random < 2:
                call granberia_ng_a1
                $ world.battle.main()
            elif world.party.player.aqua and 1 < random < 4:
                call granberia_ng_a2
                $ world.battle.main()
            elif world.party.player.aqua and 3 < random < 7:
                call granberia_ng_a3
                $ world.battle.main()

            elif random == 7:
                call granberia_ng_a4
                $ world.battle.main()
            elif random == 8:
                call granberia_ng_a5
                $ world.battle.main()
            elif random == 9:
                call granberia_ng_a6
                $ world.battle.main()

label granberia_ng_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ха!",
        "От атаки, подобно этой, легко увернуться!",
        "Просто лёгкий тычок."
    ])

    $ world.battle.show_skillname("Рассекающий удар")
    play sound "audio/se/karaburi.ogg"

    "Гранберия взмахивает своим мечом, кося всё на своём пути!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(70, 65, 55):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (300, 350), 2: (380, 410), 3: (600, 650)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.battle.badend("h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label granberia_ng_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Как насчёт этого?!",
        "А вот и я!",
        "Сможешь ли ты увидеть эту атаку?!"
    ])

    $ world.battle.show_skillname("Демоническое Обезглавливание")
    play sound "audio/se/tuki.ogg"

    $ narrator(f"Гранберия быстро делает шаг вперёд и нацеливает мощный взмах мечом на шею {world.party.player.name[1].name}!")

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(75, 70, 55):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (500, 550), 2: (600, 650), 3: (750, 800)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 15, 2: 10, 3: 5}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.battle.badend("h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label granberia_ng_a3:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Съешь это!",
        "Слишком медленно!",
        "Сможешь ли ты уследить за моими движениями?!"
    ])

    $ world.battle.show_skillname("Кровопускающий Громовой Выпад: Ураган")
    play sound "audio/se/karaburi.ogg"

    "Гранберия ступает вперёд, словно гром, и делает быстрый выпад!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(10, 7, 5):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (260, 320), 2: (340, 400), 3: (400, 450)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.battle.badend("h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label granberia_ng_a4:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хаа!",
        "Ощути на себе всю мощь моего клинка!",
        "Я не собираюсь поддаваться тебе!"
    ])

    $ world.battle.show_skillname("Удар Дракона-Мясника")
    play sound "audio/se/karaburi.ogg"

    $ narrator(f"Меч Гранберии обрушивается на {world.party.player.name[2].name}!")

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(75, 70, 65):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (700, 750), 2: (800, 900), 3: (900, 1000)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 5, 2: 3, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], 1)

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.battle.badend("h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label granberia_ng_a5:
    world.battle.enemy[0].name "Со всей моей силой!.."

    $ world.battle.show_skillname("Демонический Крушитель Черепов: Очищение")
    $ world.party.player.skill5a()

    "Гранберия прыгает в воздух и обрушивается вниз!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(80, 75, 70):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (800, 900), 2: (950, 1050), 3: (1050, 1200)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], 1)

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.battle.badend("h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label granberia_ng_a6:
    world.battle.enemy[0].name "Я всверлю в тебя ужас от этого умения!"

    $ world.battle.show_skillname("Гибельный Клинок Звезды Хаоса")
    play sound "audio/se/karaburi.ogg"

    "Меч Гранберии блестит как хаотичная падающая звезда!"

    python:
        world.party.player.skill6a()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(75, 70, 45):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = (120, 150)

        for i in range(1, 6):
            if i == 1:
                line = 1

            if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
                world.party.player.wind_guard()
            else:
                world.battle.enemy[0].deal(world.battle.enemy[0].damage, line=line)

            line += 1
            if line > 3:
                line = 1

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.battle.badend("h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label granberia_ng_a7:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Следуя течению... Безмятежный Демонический Меч!",
        "Я рассеку всё на своём пути... Безмятежный Демонический Меч!"
    ])

    "Гранберия вкладывает свой меч в ножны.{w}{nw}"

    $ world.battle.show_skillname("Безмятежный Демонический Меч")
    $ world.party.player.skill13a()

    extend "\nИ резко вынимая, прорезает всё на своём пути!"

    python:
        if world.party.player.aqua:
            renpy.jump(f"{world.battle.tag}_a7a")

        world.battle.enemy[0].damage = {1: (850, 1000), 2: (1050, 1200), 3: (1250, 1500)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            narrator("Атака Гранберии достигает Луку в мгновение ока!")
            renpy.sound.play("audio/se/damage2.ogg")
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.battle.badend("h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label granberia_ng_a7a:
    hide ct
    $ world.battle.show_skillname("Безмятежное движение")
    play sound "audio/se/miss_aqua.ogg"
    $ renpy.show(world.battle.background, at_list=[miss2])

    $ narrator("%s пытается уклониться!{w}\nНо атака всё равно цепляет его!" % world.party.player.name[0].name)

    python:
        world.battle.enemy[0].damage = {1: (450, 500), 2: (550, 600), 3: (720, 750)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    return

label granberia_ng_a8:
    world.battle.enemy[0].name "Получай!"

    $ world.battle.show_skillname("Сотрясающее Землю Обезглавливание")
    $ world.party.player.skill12a()

    $ narrator(f"Наполнив свой меч силой Земли, Гранберия обрушивает его на {world.party.player.name[2].name}!")

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(60, 50, 45):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (1050, 1200), 2: (1250, 1400), 3: (1800, 2200)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            narrator(f"Мощный удар обрушивается на голову {world.party.player.name[1].name}!")
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], 1)

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.battle.badend("h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label granberia_ng_a9:
    world.battle.enemy[0].name "Мой лучший приём... Неудержимый Испепеляющий Клинок!"

    $ world.battle.show_skillname("Неудержимый Испепеляющий Клинок")

    "Меч Гранберии замелькал, проводя бесчисленные атаки!"

    python:
        renpy.pause(0.5)

        world.party.player.skill16a()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(60, 55, 50):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = (210, 250)

        for i in range(1, 6):
            if i == 1:
                line = 1

            if world.party.player.holy and world.party.player.wind_guard_calc({1: 5, 2: 3, 3: 1}):
                world.party.player.wind_guard()
            else:
                world.battle.enemy[0].deal(world.battle.enemy[0].damage, line=line)

            line += 1
            if line > 3:
                line = 1

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.battle.badend("h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label granberia_ng_a10:
    window show
    show screen hp
    window auto

    $ world.battle.face(1)

    g "Против меня это не сработает дважды!"

    $ world.battle.counter()

    "Гранберия подпрыгивает вверх к звезде!"

    play sound "audio/se/karaburi.ogg"
    hide granberia
    pause .3
    play sound "audio/se/karaburi.ogg"
    show effect serene demon sword zorder 30
    pause .3
    show effect daystar with ImageDissolve("images/Transition/mask01.webp", 0.5)
    pause .3
    play sound "audio/se/karaburi.ogg"
    hide effect

    "Она ударяет своим пылающим лезвием прямо в звезду!"

    play sound "audio/se/bom2.ogg"
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=20), "master")
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=20), "screens")
    play sound "audio/se/fire2.ogg"
    pause 3.0
    play sound "audio/se/power.ogg"
    $ world.battle.show_skillname("Звёздный Клинок")

    "Свет звезды резонирует с огнём на лезвиях!"
    "Сила света и огня сливаются воедино в мече Гранберии!"

    play sound "audio/se/power.ogg"
    pause 1.0
    play sound "audio/se/karaburi.ogg"

    "Наконец, она обрушивает его на Луку!"

    play sound "audio/se/bom2.ogg"
    pause .4
    play sound "audio/se/bom3.ogg"
    show bg white with flash
    pause .3
    $ renpy.transition(Quake((0, 0, 0 ,0), 2.0, dist=30), "master")
    $ renpy.transition(Quake((0, 0, 0 ,0), 2.0, dist=30), "screens")

    $ world.battle.enemy[0].damage = (3200, 3600)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, 1)

    $ world.battle.hide_skillname()

    $ renpy.show(world.battle.background)
    $ renpy.transition(Dissolve(1.5), layer="master")
    $ world.battle.face(1)
    play sound "audio/se/down.ogg"

    if not world.party.player.life:
        $ world.battle.badend("h1")
    else:
        $ persistent.starblade = 1

    play sound "audio/se/down.ogg"

    $ world.battle.main()

label granberia_ng_kousan:
    "Если я сдамся, она сразу же меня прикончит..."

    $ world.battle.main()

label granberia_ng_onedari:
    "Она не тот противник, кого я могу попросить об атаке!"

    $ world.battle.main()

label granberia_ng_h1:
    l "Гхаа!"

    stop music fadeout 1.0
    play sound "se/down.ogg"

    "После серьёзных ран Лука упал на землю."
    "Лука был побежден Гранберией"

    show granberia st01

    g "Я победила.{w}\nТы больше не сможешь сражаться за Илиас."
    l "Я не сражался за Илиас!{w}\nМне вообще плевать на неё!"
    l "Я лишь пытался защитить город..."
    g "Ммм?{w}\n... Впрочем, уже ен важно, ты побеждён."
    "Гранберия смотрела на мое покрытое синяками и порезами тело.{w}\nЕе взгляд был холоден и словно застыл на мне. Даже не знаю, о чем она сейчас думает..."
    g "Всё же твои силы поистине впечатляют... Ты сражался на пределе.{w}\nЗдесь нечему стыдиться."
    l "Пожалуйста,  оставь этот город в покое...{w}\nОни не заслужили такой судьбы..."
    "Я  сделал последнюю попытку вразумить её"
    g "Ты хоть понимаешь зачем я здесь?"

    show granberia st02

    l "Да, но захват храма Илиас не остановит войну людей и монстров.{w}\nДля начала, большинство людей здесь даже не ненавидят их."
    l "Это заставит людей только еще больше возненавидеть монстров.{w}\nЭто ничего не решит..."
    l "Пожалуйста... просто оставь их в покое."

    show granberia st04

    g "..........{w}\nЯ недооценила тебя."
    g "Хорошо.{w} Пока что я оставлю этот город в покое."
    g "Но, если герои не остановятся, я вернусь."
    g "А теперь...{w}\nВставай."
    l "А?"
    "Гранберия помогает мне встать на ноги."
    "Хоть их и трясло, но я смог удержать равновесие."
    "Ей удалось отправить меня в нокаут без серьезных травм."
    "Её мастерство и вправду поражает..."
    g "Пойдём со мной."
    "Гранберия уводит меня из города."

    play sound "se/asioto2.ogg"

    jump granberia_ng_h

label granberia_ng_h:
    scene bg 006

    "Гранберия отвела меня в лес на приличном расстоянии от города.{w}\nВообще, здесь довольно спокойно и мирно..."

    show granberia st02

    g "Ни один человек не мог сравниться со мной по силе, кроме тебя.{w}{nw}"

    show granberia st03

    extend "\nБыло бы не очень умно просто отпустить кого-то, вроде тебя, не так ли?"

    play sound "se/dassyutu.ogg"

    l "Уаа!"
    "Вскоре после этих слов, Гранберия начала рвать на мне одежду!{w}{nw}"

    play sound "se/escape.ogg"
    pause 2.0
    show granberia st52
    play sound "se/break.ogg"

    extend "\n... А мгновением позже, и доспехи Гранберии упали на землю с громким лязгом."
    l "Арх!..{w} Прямо здесь?!"
    "Эта славная битва меня так возбудила...{w}\nЯ больше не могу сдерживаться!.."

    play sound "se/down.ogg"
    hide granberia

    l "Аах!"
    "Гранберия повалила меня на землю.{w}\nЕё голос и движение отражали возбуждение, практически нетерпение от того что должно случиться.{w}\nПохоже она и правда жаждет этого."

    play sound "se/ero_paizuri.ogg"

    "Она села на мою талию, ее мягкие, теплые бедра обхватили меня."
    "Её обнаженное тело освещал мягкий солнечный свет, пробивающийся сквозь ветви деревьев.{w}\nДаже в моём положении я не мог отвести от неё взгляда."
    "... И нравилось мне это зрелище или нет, мой член мгновенно стал твёрже в предвкушении"
    "Она и правда собирается меня изнасиловать прямо здесь, на земле, словно дикое животное..."
    g "Я победила тебя.{w}\nПришёл черед для моей награды"
    "Чуть привстав с меня...{w}{nw}"

    play sound "se/ero_pyu1.ogg"
    show granberia hg1
    play music "bgm/ero1.ogg"

    extend "\nОна опускает свои бедра, загоняя мой пенис внутрь!{w}\nЭто чувство опьяняет..."
    l "Ааах!.."

    play sound "se/ero_pyu5.ogg"

    "Внутри её киски было так жарко, я ощущал, что словно таю.{w}\n"
    g "Хьяя...{w}\nДовольно не плохо, да?{w}\nРади тебя я постараюсь сделать ещё приятнее..."

    play hseanwave "se/hsean02_innerworks_a2.ogg"

    "Произнеся это, Гранберия начала раскачивать своими бедрами взад и вперед.{w}\nМой член прыгал верх и вниз внутри её киски, терясь о горячую плоть."
    l "Угх...{w}\nТак хорошо..."
    g "Верно.{w}\nБудь моя воля, я могла бы завладеть любым мужчиной.{w}\nЯ только начала, а ты уже еле держишься.{w}\nРазве я не права?.."

    play sound "se/ero_pyu1.ogg"
    pause 1.0
    play sound "se/ero_pyu1.ogg"

    "Сказала она, слегка качнув бёдрами.{w}\n... Правда прозвучало так, словно она больше хотела убедить саму себя, нежели кого-то другого."
    g "Только представь, это лишь начало..."

    play sound "se/ero_chupa5.ogg"
    pause 0.5

    l "Ааах!"
    "После этих слов Гранберия туго втянула живот, сжав свое влагалище, оседлав меня."
    "Она медленно раскачивалась взад и вперед, её мягкие, покрытые чешуёй бедра тёрлись, о мои собственные.{w}\nКаждое её движение отдавалось волной наслаждения в моём теле"
    "Даже её киска, казалось, стала \"пылать\" сильнее туже сжимая мой член."
    l "Ах..."
    g "Ммм...{w}\nУже собираешься кончить?"

    pause 1.0

    g "Ну?{w}\nТак что?"
    "Спросила она с неким раздражением в голосе."
    l "Д-да..."
    g "Отлично.{w}\nКак только ты кончишь, мы закончим."

    play sound "se/ero_pyu1.ogg"
    pause 0.7
    play sound "se/ero_pyu1.ogg"
    pause 0.7
    play sound "se/ero_pyu1.ogg"
    play hseanwave "se/hsean04_innerworks_a4.ogg"

    "С этими словами, Гранберия начала медленно ускорять свои движения в позе наездницы.{w}\nДвигаясь с каждой секундой все быстрее...{w} Пока не начала остервенело скакать на мне словно в безумии."
    "Я так долго не продержусь"
    l "Аххх...{w}\nОстано... вись..."
    g "Мфааа..."
    "Чувствуя, что я скоро кончу, она только одарила меня садистской ухмылкой.{w}\nВзгляд чистой животной похоти возник на её лице, ведь я был под её полным контролем."
    g "Ммм...{w}\nХннн!"
    l "Ааах!"

    play sound "se/ero_pyu5.ogg"

    "Тихо смеясь, она лишь сильнее втянула живот, сжав свою киску так сильно, как только могла.{w}\nЯ чувствовал напряжение в месте ниже живота/талии и желание выпустить всё из себя."

    pause 0.5

    l "Аааа!"

    stop hseanwave
    show granberia hg2
    with syasei1
    $ world.ejaculation()

    "Окончательно сдавшись на волю Гранберии, я быстро начал кончать под её мягким телом."
    g "Мммм!..{w}\nАааах~{image=note}..."
    "На лице Гранберии сверкает похотливая улыбка, она нежно стонет, когда я пускаю свое семя глубже в её тело.{w}\nЕё улыбка выглядит самодовольное, видимо она наслаждается от того, как легко заставила меня кончить."
    g "Пха.{w}\nСтоль умелый воин, да так легко сражён..."
    g "Ну как, тебе понравилось?"
    l "Ах...{w}\nДа..."
    "Мне даже не было так стыдно за это..."
    g "В таком случае....{w}\nДумаю, нам стоит повеселиться ещё немного."

    play hseanwave "se/hsean04_innerworks_a4.ogg"

    "И с этими словами Гранберия вновь начала двигать своими бёдрами, пусть и казалось, что она остановилась.{w}\nВперед-назад, вперед-назад...{w}\nНо...{w} В этот раз ощущения были иными."
    "Она словно начала нагреваться внутри.{w}{nw}"

    play sound "se/ero_pyu1.ogg"

    extend "\nТемпература росла все выше и выше, пока она сжимала мой член в своём чреве."
    l "Ахх!{w}\nТак горячо!.."
    "Но я мог лишь слабо извиваться под ней, пока она продолжала."
    g "Не волнуйся.{w} Она тебе не навредит.{w}\nЭто особая способность драконов."

    play sound "se/ero_pyu2.ogg"
    pause 0.5
    play sound "se/ero_pyu2.ogg"

    "Игнорируя мои мольбы, она продолжила, а её киска становилась только горячее.{w}\nОбхватив мои бёдра руками, она начала быстро двигать ими навстречу своим."
    l "Ннгх..."
    "Казалось, что мой член просто расплавиться от сочетания жара и наслаждения."
    l "Уггх...{w}\nТ-так хорошо!..{w} Аххх!"
    g "Хех...{w}\nЯ знала, ты долго не выстоишь против такого."
    g "Даже не пытайся сопротивляться.{w}\nЭто раздражает..."

    play sound "se/ero_chupa5.ogg"
    pause 0.7
    play sound "se/ero_chupa5.ogg"

    "Непристойные звуки исходили от Гранберии, пока она продолжала скакать на мне.{w}\nГорячая субстанция начинает медленно вытекать из ее влагалища и пропитывать мою промежность, почти обжигая на ощупь."
    "Я только что кончил, но она явно старается изо всех сил заставить меня кончить снова.{w}\nНе знаю даже, как долго я смогу так продержаться."
    g "Мгхм...{w}\nЧто, ты уже снова собираешься кончить?"
    g "Хорошо, что нас никто не видит.{w}\nЗнаешь, я могла бы просто изнасиловать тебя прямо там, на городской площади..."
    g "Ты даже не представляешь, как трудно было сопротивляться этому желанию..."

    play sound "se/ero_pyu2.ogg"
    pause 0.7
    play sound "se/ero_pyu3.ogg"

    l "Аааах!"
    "Она продолжает быстро подпрыгивать на мне, вверх и вниз, постоянно стимулируя мой пенис."
    l "Cлишком быстро!.."
    l "Аах!"
    "И всего через несколько мгновений мое тело снова на пределе своих возможностей."

    stop hseanwave
    show granberia hg2
    with syasei1
    $ world.ejaculation()

    "Не в силах больше терпеть, я кончаю во второй раз."
    g "Нгхх!..{w}\nКак хорошо!.."
    "Все ее тело дрожит и содрогается, когда я выстреливаю в нее своей спермой во второй раз.{w}\nОна стонет так громко, что практически кричит..."
    "Это было невероятно эротично.{w}\nДаже в моем жалком положении я не мог не восхищаться ее великолепным телом..."
    g "Ннх...{w}\nЭто было невероятно..."
    g "Хорошо, я решила.{w}\nТы станешь моим рабом."
    l "Рабом?.."
    "Разве я не заслужил нечто большее?"
    g "Ты слишком сильный и милый, чтобы убить тебя.{w}\nЯ долго ждала воина, который бы соответствовал моей силе..."
    g "Но давай пока продолжим."
    l "Уууу..."

    play hseanwave "se/ero_pyu5.ogg"

    "Гранберия снова начинает двигаться вверх и вниз.{w}\nТаки темпами она выжмет меня досуха."
    "Быстро и грубо прыгая на мне сверху, она скачет на мне так сильно, как только может."
    l "Ааах..."
    g "У тебя милый голос.{w}\nПостони ради меня еще немного..."
    "Она не собирается останавливаться.{w}\nБудет продолжать, пока я не провалюсь в обморок..."
    "Может быть, всё это не так уж и плохо..."
    "Когда я принял свою судьбу, она продолжала еще несколько минут, прежде чем я испытываю оргазм в последний раз, и теряю сознание."

    stop hseanwave
    show granberia hg2
    with syasei1
    $ world.ejaculation()

    scene bg black with Dissolve(1.5)
    "Хотя Илиасбург был защищен, мое путешествие оканчивается здесь.{w}\nЯ так и не узнал, что со мной случилось.{w}\nТеперь я супруг Гранберии, и проведу с ней остаток своей жизни."
    "................."

    $ world.battle.bad = [
        "Впечатлённая силой Луки, Гранберия отвела его в Замок Владыки монстров.",
        "Так Лука и прожил остаток своих дней, сражаясь и занимаясь с ней сексом."
    ]

    $ persistent.count_end += 1
    $ world.battle.lose()

    return

label delf_a_start:
    python:
        world.troops[3] = Troop(world)
        world.troops[3].enemy[0] = Enemy(world)

        world.troops[3].enemy[0].name = Character("Тёмная эльфийка-мечница")
        world.troops[3].enemy[0].sprite = ["delf_a st01", "delf_a st02", "delf_a st03"]
        world.troops[3].enemy[0].position = xy(0.5, 0.5)
        world.troops[3].enemy[0].defense = 100
        world.troops[3].enemy[0].evasion = 95
        world.troops[3].enemy[0].max_life = world.troops[3].battle.difficulty(180, 230, 250)
        world.troops[3].enemy[0].power = world.troops[3].battle.difficulty(0, 1, 2)

        world.troops[3].battle.tag = "delf_a"
        world.troops[3].battle.name = world.troops[3].enemy[0].name
        world.troops[3].battle.background = "bg 004"
        world.troops[3].battle.music = 1
        world.troops[3].battle.exp = 50
        world.troops[3].battle.exit = "lb_0011"

        world.troops[3].battle.init()

    "Эльфийка... {i}Страж леса{/i}."
    "Обычно они добры и дружелюбны, но некоторые из них отдались тьме."
    "Таких называют {i}Тёмными Эльфами{/i}."
    dark_elf "Возвращайся откуда пришёл.{w}\nЭто место запретно для людей."
    "Тёмная Эльфийка вынимает из ножен меч и направляет его на меня."
    l "Я пришёл поговорить с Микаэлой.{w}\nПожалуйста, не заставляй меня запечатывать тебя..."
    dark_elf "Мне придётся применить силу, если ты не уберёшься отсюда."
    l "........"
    $ world.party.player.skill_set(2)
    "Полагаю у меня нет выбора."
    "Я вынимаю свой меч и направляю его на тёмную эльфийку."
    "Я хочу создать мир, где люди могу мирно сосуществовать с монстрами.{w}\nМне нужно попасть в эту деревню любой ценой!"

    $ world.battle.face(2)

    dark_elf "Если ты не отступишь, я погружу тебя в пучину разврата.{w}\nЯ оскверню тебя своей тьмой и превращу в своего раба!"

    $ world.battle.face(1)
    $ world.battle.main()


label delf_a_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label delf_a_v:
    $ world.battle.face(3)

    dark_elf "Что за?{w} Моё тело..."

    play sound "se/syometu.ogg"
    hide delf_a with crash

    "Тёмная эльфийка становится такой маленькой, что с лёгкостью бы уместилась у меня в ладони."
    dark_elf "Запечатал меня...{w} Я припомню тебе это!"

    play sound "se/escape.ogg"

    "Проклиная меня напоследок, тёмная эльфийка убегает прочь!"

    $ world.battle.victory(1)

label delf_a_a:
    while True:
        $ random = rand().randint(1,4)

        if random == 1:
            call delf_a_a1
            $ world.battle.main()
        elif random == 2:
            call delf_a_a2
            $ world.battle.main()
        elif random == 3:
            call delf_a_a3
            $ world.battle.main()
        elif random == 4 and not world.party.player.status and world.battle.delay[0] > 2:
            call delf_a_a4
            $ world.battle.main()


label delf_a_a1:
    $ world.battle.skillcount(1, 3032)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Своим ртом я отправлю тебя в пучину наслаждения...",
        "*Сосёт* *Сосёт*",
        "Я покрою его своей слюной...",
        "Попробуем..."
    ])

    $ world.battle.show_skillname("Минет эльфа")
    play sound "se/ero_buchu3.ogg"

    $ world.battle.cry(narrator, [
        "Тёмная эльфийка сосёт член Луки!",
        "Тёмная эльфийка интенсивно сосёт член Луки!",
        "Тёмная эльфийка облизывает весь член Луки!",
        "Тёмная эльфийка берёт член Луки в рот, вылизывая языком!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (11, 14), 2: (16, 21), 3: (22, 28)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label delf_a_a2:
    $ world.battle.skillcount(1, 3033)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я использую свою руку...",
        "Своей развратной рукой...",
        "Падёшь ли ты в пучину разврата... от этих движений?"
    ])

    $ world.battle.show_skillname("Мастурбация эльфа")
    play sound "se/ero_koki1.ogg"

    $ world.battle.cry(narrator, [
        "Тёмная эльфийка начинает тереть член Луки!",
        "Тёмная эльфийка интенсивно мнёт член Луки!",
        "Тёмная эльфийка быстро дрочит член Луки!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (10, 12), 2: (15, 18), 3: (20, 24)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label delf_a_a3:
    $ world.battle.skillcount(1, 3034)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Как тебе моя тёмная грудь?..",
        "Хо-хо... Сможешь ли ты выдержать это?",
        "Падёшь ли ты от этого?.."
    ])

    $ world.battle.show_skillname("Пайзури эльфа")
    play sound "se/ero_koki1.ogg"

    $ world.battle.cry(narrator, [
        "Тёмная эльфийка прижимает член Луки к своей груди!",
        "Тёмная эльфийка прижимается своей грудью к паху Луки!",
        "Тёмная эльфийка сдавливает член Луки своей грудью!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (12, 15), 2: (18, 22), 3: (24, 30)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label delf_a_a4:
    $ world.battle.skillcount(1, 3035)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Это поцелуй тьмы...",
        "Пади во тьму...",
        "Утони в экстазе от моего поцелуя..."
    ])

    $ world.battle.show_skillname("Поцелуй удовольствия")
    play sound "se/ero_chupa2.ogg"

    "Тёмная эльфийка целует Луку!{w}\nТело Луки содрогается от экстаза!"

    $ world.battle.enemy[0].damage = (5, 8)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)

    if not world.party.player.status:
        $ world.party.player.status = 1
        $ world.party.player.status_print()
        "Лука очарован!"
    elif world.party.player.status == 1:
        $ world.party.player.status_print()
        "Лука в трансе!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h4")

    if world.party.player.surrendered and world.party.player.max_life > world.party.player.life*2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.surrendered and world.party.player.max_life > world.party.player.life*5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    if world.party.player.surrendered:
        return

    l "Ах...."
    dark_elf "Давай, позволь экстазу поглотить себя.{w}\nУж я-то позабочусь о твоём разложении."

    $ world.party.player.status_turn = 3

    if persistent.difficulty == 3:
        $ world.party.player.status_turn = rand().randint(3,4)

    return

label delf_a_kousan:
    $ world.battle.common_surrender()

    "Лука поддаётся на обещания наслаждения."

    $ world.battle.face(2)

    dark_elf "Мудрый выбор.{w}\nА теперь... пади во тьму..."

    $ world.battle.enemy[0].attack()

label delf_a_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Elf Blowjob"
    $ list2 = "Elf Handjob"
    $ list3 = "Elf Tit Fuck"
    $ list4 = "Kiss of Ecstasy"

    if persistent.skills[3032][0]:
        $ list1_unlock = 1

    if persistent.skills[3033][0]:
        $ list2_unlock = 1

    if persistent.skills[3034][0]:
        $ list3_unlock = 1

    if persistent.skills[3035][0]:
        $ list4_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == -1:
        jump delf_a_main
    else:
        jump expression "delf_a_onedari%s" % world.battle.result


label delf_a_onedari1:
    call onedari_syori

    dark_elf "You want to fall using my mouth?{w}\nOk... Scatter the proof of your corruption in my mouth..."

    while True:
        call delf_a_a1


label delf_a_onedari2:
    call onedari_syori

    dark_elf "You want to fall using my hand?{w}\nOk... Scatter the proof of your corruption on my hand..."

    while True:
        call delf_a_a2


label delf_a_onedari3:
    call onedari_syori

    dark_elf "You want to fall using my chest?{w}\nOk... Scatter the proof of your corruption on my chest..."

    while True:
        call delf_a_a3


label delf_a_onedari4:
    call onedari_syori

    dark_elf "You want to fall while enraptured?{w}\nOk... I'll make sure you never escape from a world of ecstasy."

    while True:
        call delf_a_a4


label delf_a_h1:
    $ world.party.player.moans(1)
    with syasei1
    show elf bk01 at xy(X=100, Y=-100) zorder 10 as bk
    $ world.ejaculation()

    "Luka explodes in the Dark Elf's mouth!"

    $ world.battle.lose()

    $ world.battle.count([4, 6])
    $ bad1 = "Luka succumbs to the Dark Elf's mouth technique."

    if world.party.player.status == 1:
        $ bad1 = "While enraptured, Luka succumbs to the Dark Elf's mouth technique."
    $ bad2 = "Luka fell into degeneration, and was raped by the Dark Elf until his death."
    $ world.battle.face(2)

    "Semen dripping out of her mouth, the Dark Elf gives a cold smile at world.party.player.{w}\nShowing me the proof of my surrender, she laughs at me."

    jump delf_a_h


label delf_a_h2:
    $ world.party.player.moans(1)
    with syasei1
    show elf bk03 at xy(X=100, Y=-100) zorder 10 as bk
    $ world.ejaculation()

    "Luka explodes in the Dark Elf's hand!"

    $ world.battle.lose()

    $ world.battle.count([5, 6])
    $ bad1 = "Luka succumbs to the Dark Elf's hand technique."

    if world.party.player.status == 1:
        $ bad1 = "While enraptured, Luka succumbs to the Dark Elf's hand technique."
    $ bad2 = "Luka fell into degeneration, and was raped by the Dark Elf until his death."
    $ world.battle.face(2)

    "The Dark Elf plays with the sticky semen in her hand as she looks at me.{w}\nShowing me the proof of my surrender, she laughs at me."

    jump delf_a_h


label delf_a_h3:
    $ world.party.player.moans(1)
    with syasei1
    show elf bk02 at xy(X=100, Y=-100) zorder 10 as bk
    $ world.ejaculation()

    "Luka explodes on the Dark Elf's chest!"

    $ world.battle.lose()

    $ world.battle.count([7, 6])
    $ bad1 = "Luka succumbs to the Dark Elf's titty fuck."

    if world.party.player.status == 1:
        $ bad1 = "While enraptured, Luka succumbs to the Dark Elf's titty fuck."
    $ bad2 = "Luka fell into degeneration, and was raped by the Dark Elf until his death."
    $ world.battle.face(2)

    "The Dark Elf plays with the sticky semen on her chest as she looks at me.{w}\nShowing me the proof of my surrender, she laughs at me."

    jump delf_a_h


label delf_a_h4:
    $ world.party.player.moans(1)
    with syasei1
    show delf_a bk01 at xy(X=100, Y=-100) zorder 10 as bk
    $ world.ejaculation()

    "As the Dark Elf leans over and kisses me, I ejaculate, shooting my seed out all over her stomach."

    $ world.battle.lose()

    $ world.battle.count([18, 6])
    $ bad1 = "Luka succumbs to the Dark Elf's kiss of ecstasy."
    $ bad2 = "Luka fell into degeneration, and was raped by the Dark Elf until his death."
    $ world.battle.face(2)

    "The Dark Elf plays with the sticky semen on her stomach as she looks at me.{w}\nShowing proof of my surrender, she laughs at me."

    jump delf_a_h


label delf_a_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    dark_elf "Fufu... This white semen is proof of your defeat.{w}\nHow miserable... Falling like this to a monster."
    l "Uhgg...{w} Stop..."
    dark_elf "Oh? You haven't fallen completely yet, then?{w}\nHow strange... Do you have some sort of divine protection?"
    dark_elf "If our genitals touch, you will be corrupted for sure.{w}\nThis time for sure, you will surrender to the darkness..."

    hide bk
    show delf_a h1 at center

    "The Dark Elf straddles over top of me."
    dark_elf "Now, I'm going to put it in...{w}\nI'll make you surrender to the deprivation with my pussy..."
    l "No... Don't do that..."
    "No matter how hard I struggle, I'm unable to get her off of me.{w}\nHaving already orgasmed, I can't sum up the needed power..."
    dark_elf "Now... Let's connect..."
    "As she says the, she slams down her waist.{w}{nw}"

    play hseanwave "se/hsean01_innerworks_a1.ogg"
    show delf_a h2
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nMy penis is rammed into her wet vagina."
    l "Auuuu!"
    "As soon as it enters her, I feel her wet walls move around me.{w}\nEven without any movement, it felt amazing."
    dark_elf "You were a virgin...{w}\nA pure innocent instantly becoming corrupted..."
    l "Aaaaa!"

    play hseanwave "se/hsean02_innerworks_a2.ogg"

    "The Dark Elf starts to shake her waist.{w}\nRubbing against me with every movement, extreme pleasure starts to spread through me."
    "I have the feeling that if I give in to this pleasure, there won't be any escape."
    "Falling into darkness, and surrendering my body and mind to this Dark Elf...{w}\nThe fear at such an outcome makes me start to panic."
    l "Stop! Let go of me!"
    dark_elf "Fufufu... It feels amazing to become corrupted...{w}\nIf you pour your semen into me, you'll be sure to fall..."
    l "I don't want that!"
    "I desperately try to push the Dark Elf off of me, but she easily holds me down."
    dark_elf "Fufu... You can't escape any longer.{w}\nUntil you die, we're going to keep this up."
    l "Auu..."
    "I put all of my concentration into my waist to hold off my orgasm.{w}\nHer waist continues to move, slowly whittling away my endurance."
    dark_elf "It's pointless to try to endure... just let it out in me."
    dark_elf "As soon as you give in to the pleasure, you will fall into the darkness.{w}\nThen we'll be able to continue this fun until you die..."
    l "Auuuu...."
    "The Dark Elf laughs as she watches my face contorted in both horror and pleasure."
    "As she continues to pump me, I feel myself rising to the edge."
    dark_elf "Now, let it out inside of me.{w}\nYour white semen will stain my dark interior..."
    "The Dark Elf looks down at me as she moves her hips, tempting me into the darkness..."
    l "N...No..."
    "Summoning the last of my strength, I manage to hold on.{w}\nSuddenly, the Dark Elf leans over and kisses me."
    dark_elf "Fufu... my kiss of ecstasy.{w}\nWhile drowning in ecstasy, give in to deprivation..."
    l "Faaaa!"
    "As soon as her lips touched mine, a sweet trance starts to overpower my brain."
    "A sweet numbness starts to flow down my neck into my chest..."
    "I can feel the numbness go lower and lower, until it reaches my waist...{w}\nAs it enters my penis, all of my endurance vanishes."
    l "Aaaahhhhh!"

    with syasei1
    show delf_a h3
    $ world.ejaculation()
    stop hseanwave fadeout 1

    "I explode inside of the Dark Elf.{w}\nIt wasn't anything like a usual orgasm..."
    "As if my soul itself was shooting out of me, I feel a sweet ecstasy start to wash over my body."
    "Each twitch of my penis that shoots out semen, more of that feeling flows through me."
    "It continues for an unusually long time, letting out more and more..."
    dark_elf "Fufu... Do you understand now what it means to fall into darkness?{w}\nIt isn't possible to return anymore..."
    l "Faa...."
    "The long ejaculation continues, sucking out more of my soul...{w}\nSurrendering my body and soul to this Dark Elf, the ecstasy washes over me."
    l "Auu... Aaa...."
    "When it finally ended, I had fallen in body and mind."
    dark_elf "Fufu... You're finally fully corrupt.{w}\nLet's have some more fun now..."
    dark_elf "I'll let you enjoy it until you're no more...{w}\nHahahahaha."
    l "........."
    "The Dark Elf continues to rape me.{w}\nSurrendering to the darkness, I am unable to resist anything."
    "This will continue until my death..."

    scene bg black with Dissolve(3.0)

    ".........."

    jump badend

label gob_start:
    python:
        world.troops[4] = Troop(world)
        world.troops[4].enemy[0] = Enemy(world)

        world.troops[4].enemy[0].name = Character("Девочка-гоблин")
        world.troops[4].enemy[0].sprite = ["gob st01", "gob st03", "gob st04"]
        world.troops[4].enemy[0].position = center
        world.troops[4].enemy[0].defense = 100
        world.troops[4].enemy[0].evasion = 95
        world.troops[4].enemy[0].max_life = world.troops[4].battle.difficulty(75, 112, 132)
        world.troops[4].enemy[0].power = world.troops[4].battle.difficulty(0, 1, 2)

        world.troops[4].battle.tag = "gob"
        world.troops[4].battle.name = world.troops[4].enemy[0].name
        world.troops[4].battle.background = "bg 020"
        world.troops[4].battle.music = 1
        world.troops[4].battle.exit = "lb_0016"

        world.troops[4].battle.init()

    world.battle.enemy[0].name "Эй! Отдавай мне все свои деньги!"
    "Передо мной появляется маленький миленький монстр...{w}\nС огромным молотом наперевес, который ей явно великоват."
    l "..............................."

    $ world.party.player.skill_set(2)
    show gob st21

    world.battle.enemy[0].name "Уааа!"

    play sound "se/escape.ogg"
    hide gob

    "Девочка-гоблин убегает в ужасе!"

    stop music fadeout 1.0

    "................."

    $ world.battle.victory(2)

label mitubati_start:
    python:
        world.troops[5] = Troop(world)
        world.troops[5].enemy[0] = Enemy(world)

        world.troops[5].enemy[0].name = Character("Девушка-пчела")
        world.troops[5].enemy[0].sprite = ["mitubati st01", "mitubati st02", "mitubati st03"]
        world.troops[5].enemy[0].position = center
        world.troops[5].enemy[0].defense = 100
        world.troops[5].enemy[0].evasion = 95
        world.troops[5].enemy[0].max_life = world.troops[5].battle.difficulty(150, 180, 180)
        world.troops[5].enemy[0].power = world.troops[5].battle.difficulty(0, 1, 2)

        world.troops[5].battle.tag = "mitubati"
        world.troops[5].battle.name = world.troops[5].enemy[0].name
        world.troops[5].battle.background = "bg 011"
        world.troops[5].battle.music = 1
        world.troops[5].battle.exp = 40
        world.troops[5].battle.exit = "lb_0019"

        world.troops[5].battle.init()

    world.battle.enemy[0].name "Охо-хо... Неблагословлённый путешественник.{w}\nТы выглядишь аппетитно."
    "Девушка-пчела облизывает губы, смотря на меня.{w}\nМёд капает из гнезда на её животе"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Я покрою всего тебя мёдом и буду облизывать тебя...{w}\nМой язычок вылижет тебя полностью..."

    $ world.battle.face(1)
    $ world.battle.progress = 1
    $ world.battle.main()

label mitubati_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind == 2:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label mitubati_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Что... это? Мои силы..."

    play sound "se/syometu.ogg"
    hide mitubati with crash

    "Получив смертельный удар, Девушка-пчела превращается в маленькую пчелу.{w}\nСияние Ангела запечатывает её в безвредную форму!"
    "Пчела куда-то улетает!"

    $ world.battle.victory(1)

label mitubati_a:
    if world.battle.progress == 1:
        call mitubati_a5
        $ world.battle.main()

    if world.party.player.bind == 1:
        $ random = rand().randint(1,2)

        if random == 1:
            call mitubati_a3
            $ world.battle.main()
        elif random == 2:
            call mitubati_a4
            $ world.battle.main()

    while True:
        $ random = rand().randint(1,2)

        if random == 1:
            call mitubati_a1
            $ world.battle.main()
        elif random == 2 and world.battle.delay[0] > 2:
            call mitubati_a2
            $ world.battle.main()


label mitubati_a1:
    $ world.battle.skillcount(1, 3015)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "*Лижет* *Лижет*",
        "Дай мне попробовать...",
        "Охо-хо... Вкуснятина..."
    ])

    $ world.battle.show_skillname("Лизание")
    play sound "audio/se/ero_pyu1.ogg"

    "Язык девушки-пчелы вытягивается, быстро вылизывая член Луки!"

    python:
        world.battle.enemy[0].damage = {1: (9, 11), 2: (13, 16), 3: (18, 22)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label mitubati_a2:
    $ world.battle.skillcount(1, 3016)
    $ world.battle.face(2)

    if world.party.player.surrendered or world.battle.first[0]:
        world.battle.enemy[0].name "Сколь долго же тебя придётся лизать?.."
    elif not world.party.player.surrendered and not world.battle.first[0]:
        world.battle.enemy[0].name "Охо-хо... Я поймаю тебя."

    $ world.battle.show_skillname("Минет в захвате")
    play sound "audio/se/ero_pyu2.ogg"
    show mitubati ct01 at topright zorder 15 as ct

    "Девушка-пчела цепляется к Луке!{w}\nИ начинает облизывать член Луки!"

    $ world.battle.enemy[0].damage = {1: (13, 16), 2: (19, 24), 3: (26, 32)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print

    "Тело Луки накрепко схвачено девушкой-пчелой!"

    python:
        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

    hide ct

    if world.party.player.surrendered:
        return

    if world.battle.first[0]:
        world.battle.enemy[0].name "Не можешь забыть мой язычок, да?{w}\nНа этот раз пока ты не сдашься..."
    else:
        world.battle.enemy[0].name "Позволь мне вылезать тебя.{w} *Лижет*"
        world.battle.enemy[0].name "Утони в наслаждении от моего язычка..."

    $ world.battle.first[0] = True

    if persistent.difficulty == 1:
        $ world.battle.enemy[0].power = 1
    elif persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 1
    elif persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 2

    return

label mitubati_a3:
    $ world.battle.skillcount(1, 3016)

    "Тело Луки плотно схвачено девушкой-пчелой!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хаа-хаа... попробуй вырваться.",
        "Я покрою твоё милое личико мёдом...",
        "Позволь облизать всё твоё тело...",
        "Как мило... ты такой чувствительный.",
        "Ммм, потрясающе..."
        "Уже слабеешь?.."
        "Сопротивляться — в порядке вещей... если можешь, конечно."
        "Из твоей головки уже начала течь смазка...",
        "Обещаю, будет приятно!"
        "Сюда я тоже намажу немного мёда..."
    ])

    $ world.battle.show_skillname("Облизывание в захвате")
    show mitubati ct01 at topright zorder 15 as ct
    play sound "audio/se/ero_pyu2.ogg"

    $ world.battle.cry(narrator, [
        "Язык девушки-пчелы обвивает Луку!",
        "Девушка-пчела вылизывает лицо Луки!",
        "Девушка-пчела вылизывает тело Луки своим липким язычком!",
        "Язык девушки-пчелы обвивает член Луки, покрывая его мёдом!",
        "Язык девушки-пчелы вылизывает пах Луки!"
        "Девушка-пчела облизывает шею Луки!"
        "Девушка-пчела вылизывает тело Луки своим липким язычком!"
        "Язычок девушки-пчелы проникает в уретру Луки!",
        "Девушка-пчела вылизывает член Луки против его воли!"
        "Девушка-пчела вылизывает основание члена Луки!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (13, 16), 2: (19, 24), 3: (26, 32)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label mitubati_a4:
    "Тело Луки крепко-накрепко удерживает девушка-пчела!"

    $ world.battle.skillcount(1, 3221)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ха-ха, твои соски встали... должно быть унизительно.",
        "Пусть ты и мужчина, тебе всё равно должно это понравится",
        "Я поиграю с твоими сосками своим язычком..."
    ])

    $ world.battle.show_skillname("Облизывание сосков")
    play sound "audio/se/ero_pyu1.ogg"

    "Девушка-пчела лижет соски Луки!"

    python:
        world.battle.enemy[0].damage = {1: (12, 15), 2: (18, 22), 3: (24, 30)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label mitubati_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Так ты хочешь, чтобы я поиграла своим язычком?..{w}\nХорошо... почувствуй же райское наслаждение!"

    $ world.battle.enemy[0].attack()

label mitubati_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Lick"
    $ list2 = "Binding Lick"
    $ list3 = "Nipple Lick"

    if persistent.skills[3015][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3016][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3221][0] > 0:
        $ list3_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == -1:
        jump mitubati_main
    else:
        jump expression "mitubati_onedari%s" % world.battle.result


label mitubati_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want to be defeated by my licking...?{w}{nw}"

    if world.battle.progress == 1:
        extend "\nThen, first..."

        call mitubati_a5
    elif world.battle.progress == 2:
        extend "\nAlright, I'll give you what you desire..."

    while True:
        call mitubati_a1


label mitubati_onedari2:
    call onedari_syori_k

    world.battle.enemy[0].name "You want to be defeated by my licking...?{w}{nw}"

    if world.battle.progress == 1:
        extend "\nThen, first..."

        call mitubati_a5
    elif world.battle.progress == 2:
        extend "\nAlright, I'll give you what you desire..."

    if world.party.player.bind != 1:
        call mitubati_a2

        world.battle.enemy[0].name "Now, let me lick you even more.{w}\nSurrender everything to my tongue..."

    while True:
        call mitubati_a3


label mitubati_onedari3:
    call onedari_syori_k

    world.battle.enemy[0].name "Even being a man, you want me to lick your nipples...?{w}{nw}"

    if world.battle.progress == 1:
        extend "\nThen, first..."

        call mitubati_a5
    elif world.battle.progress == 2:
        extend "\nAlright, I'll give you what you desire..."

    if world.party.player.bind != 1:
        call mitubati_a2

        world.battle.enemy[0].name "Now, let me lick you even more.{w}\nI'll tease your nipples..."

    while True:
        call mitubati_a4

label mitubati_a5:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Я покрою всего тебя мёдом..."

    $ world.battle.show_skillname("Сладкий мёд")
    play sound "audio/se/ero_slime2.ogg"

    "Девушка-пчела извергает мёд на тело Луки!"

    $ world.battle.enemy[0].damage = {1: (3, 5), 2: (4, 7), 3: (6, 10)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    l "Что за?.."
    "Липкий мёд покрывает моё тело.{w}\nСладкий запах от него всё сильнее затуманивает моё сознание."

    $ world.battle.face(2)

    world.battle.enemy[0].name "Уфу-фу... Я так счастлива.{w}\nА теперь позволь мне вылизать всё тело тело, покрытое моим мёдом."
    world.battle.enemy[0].name "Конечно же, я полижу и твой член...{w}\nТвоя вкусная сперма будет отличной добавкой к моему мёду."

    l "..."
    "Но не похоже, чтобы это сработало."

    $ world.battle.face(1)
    $ world.battle.progress = 2
    return

label mitubati_h1:
    $ world.party.player.moans(1)
    with syasei1
    show mitubati bk02 zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the Bee Girl's tongue, I ejaculate.{w}\nMy overflowing semen mixes with the honey all over me."

    $ world.battle.lose()

    $ world.battle.count([4, 3])
    $ bad1 = "Luka surrenders to the Bee Girl's tongue."
    $ bad2 = "Luka remained the Bee Girl's ejaculation slave until he died."
    $ world.battle.face(2)
    jump mitubati_ha


label mitubati_h2:
    $ world.party.player.moans(1)
    with syasei1
    show mitubati ct02 at topright zorder 15 as ct
    show mitubati h2
    $ world.ejaculation()

    "Unable to endure the Bee Girl's tongue, I ejaculate.{w}\nMy overflowing semen mixes with the honey all over me."

    $ world.battle.lose()

    $ world.battle.count([4, 3])
    $ bad1 = "Bound by the Bee Girl's tongue, Luka surrenders."
    $ bad2 = "Luka remained the Bee Girl's ejaculation slave until he died."
    jump mitubati_hb


label mitubati_h3:
    $ world.party.player.moans(1)
    with syasei1
    show mitubati h2
    $ world.ejaculation()

    "As the Bee Girl licks my nipples, I orgasm, shooting semen all over the Bee Girl's waist!"

    $ world.battle.lose()

    $ world.battle.count([23, 3])
    $ bad1 = "Bound by the Bee Girl's tongue, Luka surrenders."
    $ bad2 = "Luka remained the Bee Girl's ejaculation slave until he died."
    jump mitubati_hb


label mitubati_ha:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    world.battle.enemy[0].name "Haha, you let out your male juice.{w}\nMixed with my honey... delicious."
    l "Faaaa..."

    play hseanvoice "audio/voice/fera_zyuru1.ogg"
    hide ct
    hide bk
    show mitubati h1


    "The bee licks the tip of my penis as I ejaculate, licking up the semen and the honey.{w}\nThe intense stimulation causes my body to shake."
    world.battle.enemy[0].name "Your honey covered penis shooting off so much semen... Fufu."
    world.battle.enemy[0].name "I'll mix even more of my honey on you to make the ultimate seasoning...{w}\nSemen filled honey... it would taste heavenly, don't you think?"

    show mitubati ct01 at topright zorder 15 as ct

    "The Bee Girl's tongue starts to wrap around my shaft in a spiral.{w}\nIt slowly creeps up until it's completely wrapped around me."
    "Her tongue starts to gently massage the tip of my penis, slowly licking up the semen-mixed honey."
    l "Haauu...."
    "Wrapped around me, my back arches as the pleasure was more than I expected."
    world.battle.enemy[0].name "You men... you're all weak here."
    "Wrapped around my honey-covered dick, the bee increases the force that she's licking my tip with."
    l "Hyaaaa!{w}\nDon't lick my tip like that!"
    world.battle.enemy[0].name "Fufu... honey and semen... delicious."
    l "Faaa... Stop..."
    "Her rough tongue, wrapped tightly around me, starts to slowly move up and down, massaging my sensitive area.{w}\nThe sensation makes shivers run through my body."
    world.battle.enemy[0].name "Mmmm.{w}\n*Лижет*{w} *Лижет*"
    l "Auuuu!"
    "Her warm and sticky tongue licks me without mercy.{w}\nAs she tastes my sticky honey covered penis, a sweet pleasure spreads through my waist."
    world.battle.enemy[0].name "I can feel a slightly salty taste now...{w}\nYour male juice is leaking out again..."
    "Her tongue starts to poke at the urethral opening, lapping up all of the pre-come that's coming out."
    l "Faaaa!"
    "Stimulating my most sensitive part, the trembling in my body gets more violent."
    world.battle.enemy[0].name "It looks like I lapped it all up...{w}\nI guess I need to just force more out of you...{w} *Лижет*"
    l "Aahh... Faaa....{w}\nThat's... Amazing!"
    "As the Bee Girl licks my tip, she tightens the part of her tongue that's coiled around my dick."
    "From the base to the shaft to the tip, my entire glans is stimulated."
    l "Co...coming!{w}\nAaaahhh!"

    with syasei1
    show mitubati h2
    show mitubati ct02 at topright zorder 15 as ct
    $ world.ejaculation()

    "Receiving her sweet stimulation, my cock explodes, shooting semen all over her tongue."
    world.battle.enemy[0].name "Your semen...{w} Delicious."
    world.battle.enemy[0].name "*Лижет*{w} *Лижет*"
    "Drunk on the pleasure, my strength fades as her violent licking continues."
    "Due to all her licking, the honey is almost all gone."

    stop hseanvoice fadeout 1

    world.battle.enemy[0].name "Fufu... Do you want more honey?"
    l "Ah...."
    "The bee pours honey from the hive on her back all over me.{w}{nw}"

    play hseanvoice "audio/voice/fera_gutyu.ogg"

    extend "\nNow completely covered in her warm honey, the Bee Girl tightens her tongue around my sticky dick."
    l "Faaaa!"
    world.battle.enemy[0].name "*Лижет*{w} *Лижет*"
    "The rough tongue massaging me, covered in sticky honey, is too much for me to bear.{w}\nMy vision goes hazy as my endurance fades."
    "Her tongue, tightly wrapped around me, continues to tighten and play with me as my body unconsciously twitches with every lick."
    l "Ahiii... Amazing...!"
    "Her movements start to slowly change from before."
    "As if simply licking candy instead of trying to stimulate me, the unexpected change in licking brought intense pleasure."
    "Slowly crawling all over me, I twitch helplessly as her tongue forces pleasure into me."
    "Brought to the limit, I'm unable to hold on any longer."
    l "Aaaah!"

    with syasei1
    show mitubati h3
    show mitubati ct03 at topright zorder 15 as ct
    $ world.ejaculation()

    "So soon after my last orgasm, I explode again.{w}\nNot letting up, the Bee Girl sticks her tongue on my tip, licking even harder as I spurt."
    l "Faaa... don't lick... the hole where it comes out...!"
    world.battle.enemy[0].name "Mmm... You let it out again.{w} Delicious."
    world.battle.enemy[0].name "*Лижет*{w} *Лижет*"
    "Ignoring my pleas, the Bee Girl continues to forcefully lick my urethral opening, licking up the honey-mixed semen."
    world.battle.enemy[0].name "*Лижет*{w} *Лижет*"
    l "Ahiii!"
    "As soon as she lapped everything up, the bee tightens her tongue to force even more out of me."
    "My increasingly sensitive glans, I'm too weak to resist her tongue.{w}\nAs her tongue continues to lick my tip, I feebly yell out."
    l "St...op...! Coming...again..."
    world.battle.enemy[0].name "Fufu... shoot it out!{w}\n*Лижет*{w} *Лижет*"
    "The Bee Girl looks at me as she tightens her tongue."
    "Poking my tip with her tongue, the rising feeling of another ejaculation becomes overpowering."
    "Even though I just came, her tongue is forcing me to reach another height..."
    l "Aaa.... Coming.....{w} Ah...h...."

    with syasei1
    show mitubati h4
    show mitubati ct04 at topright zorder 15 as ct
    $ world.ejaculation()

    "Her sweet tongue stimulates me over and over, bringing me to repeated heights.{w}\nHow many times has it been now...? I've lost track..."
    world.battle.enemy[0].name "Mmm...{w} Delicious."
    world.battle.enemy[0].name "*Лижет*{w} *Лижет*"
    "After each ejaculation, she laps everything up and continues stimulating me towards the next."

    stop hseanvoice fadeout 1

    world.battle.enemy[0].name "Your sperm is unusually delicious...{w}\nI can't let such a fine catch get away."
    l "Eh...?"
    "Finished licking up everything from my last orgasm, the Bee Girl starts to laugh."
    world.battle.enemy[0].name "I'll carry you back to my hive and keep you...{w}\nMy semen slave... Fufufu."
    l "No wa...{w} Ahiii!"

    play hseanvoice "audio/voice/fera_zyuru1.ogg"

    "My protest is cut short by her tongue stimulating my tip again."
    "If I'm brought back to her hive, and forced to continually ejaculate like this for her..."
    "I'll be nothing but a source of food for her..."
    l "I don't want that!"
    world.battle.enemy[0].name "Fufu... Delicious.{w}\n*Лижет*{w} *Лижет*"

    stop hseanvoice fadeout 1
    scene bg black with Dissolve(3.0)

    "Meeting a miserable end, she ignores my protest and takes me back to her hive.{w}\nA tragic fate, I've been made into her semen slave."
    "My adventure is over..."
    "........."

    jump badend

label mitubati_hb:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    enemy "You let it out... your male juice.{w}\nMixed with honey... delicious."
    l "Faaa..."
    "Right after ejaculating, the Bee Girl licks all over my dick.{w}\nThe suddenly stimulation causes shivers to run through my body."
    enemy "Fufu... Your semen is delicious.{w}\nYou're just a source of food for me but... I wouldn't mind you in here for now."

    hide ct
    hide bk

    "The bee grips both of my shoulders and pulls me towards her.{w}\nHeld tightly, I can't break her grip."
    l "Wh... What are you doing!?"
    enemy "Fufu... don't you want to feel more of me?{w}\nI'll even let you shoot it out inside of me..."
    "The Bee Girl moves her waist over my groin and...{w}{nw}"

    play hseanwave "audio/se/hsean20_tentacle1.ogg"
    show mitubati ctb01 at topright zorder 15 as ct
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nForces my dick into her!"
    l "Aaauuu... Ahhhh!"
    "Her seductive insect vagina...{w}\nProtected by a hard insect shell, she exposes her raw hole as she forces me into her."
    enemy "An insect's reproductive system is different from a human's...{w}\nThey both accept a male's penis, but that's about it."
    enemy "It feels quite different from a bland humans, no?"
    l "Aauuuu!{w}\nInside... Amazing!"
    "Tiny pieces of hair seem to line her pussy's walls, giving a bristly feeling.{w}\nThe bristly hairs feel like they're covered in honey..."
    "The peculiar bristly feeling combined with the warm honey shoots an intense pleasure through my waist with every movement."
    "Every time the bee shakes her hips, more sticky honey gets on me.{w}\nThe sticky feeling brings more pleasure from the friction, making my dick feel like it's melting in her."
    l "Faaa...{w} Too good..."
    enemy "Arara... What a bad little boy.{w}\nI thought your god forbade you from doing things like this?"
    l "Aaauuu..."
    "That's right... I'm mating with a monster now...{w}\nI broke her commandment.... I'm committing the ultimate taboo..."
    l "N... No!{w} Get off of me!"
    enemy "No.{w} Just shoot your semen into me like this..."
    "The Bee Girl shakes her waist as she plays with me.{w}\nMy penis twitches from the shaking inside her sticky hole."
    "The intense stimulation brings me to the edge."
    l "Auuuu!"
    "I somehow managed to endure, but it doesn't seem like the bee will allow it."
    "She increases her movements, trying to force it out of me."
    enemy "Explode in me...{w}\nI know you can't resist the pleasure..."
    "Her renewed attacks breaks down my resistance as I feel the orgasm start to rise through me."
    l "Ahh... I can't..."
    "At last, I can't withstand the pleasure any longer.{w}\nI don't want to commit the taboo, but I can't hold on any longer...{w}\nMy lower body relaxes."

    with syasei1
    show mitubati ctb02 at topright zorder 15 as ct
    show mitubati h2
    $ world.ejaculation()

    l "Auuuu..."
    "I orgasm, shooting my semen into her.{w}\nMating with a monster... the humiliation builds in me."
    enemy "Fufu... was it good?{w}\nThis is what it's like to mate with a monster..."
    enemy "The pleasure your goddess forbade to you..."
    l "Faaa..."
    "Not releasing me after my orgasm, the bee holds me in her, but stops moving her waist."
    enemy "Next, you move.{w}\nI'll let you use my pussy... just thrust and make yourself feel good."
    l "I don't..."
    "Though I say that, I start to slowly move my hips."
    "I start to push deep into her honey filled hole."
    l "Aahh... Ok..."
    "I want to feel more of her warm insect vagina..."
    "Covered in her sticky honey, the pleasure starts to quickly feel unbearable."
    l "Faa... This feels good...{w}\nMore... More..."
    enemy "Hahaha... You're completely hooked.{w}\nIndulging in the pleasure only a monster can bring..."
    enemy "Committing the ultimate taboo... is it really ok with you?"
    l "Faaa...."
    "Such a thing has become meaningless as I taste more of her honey."
    "No... Even the immorality of the pleasure seems to increase the stimulation."
    "Thrusting into her, I feel another climax coming."
    l "Ahhhh!{w}\nI'm coming again!"

    with syasei1
    show mitubati ctb03 at topright zorder 15 as ct
    show mitubati h3
    $ world.ejaculation()

    "I explode again inside of her.{w}\nAs I come, it feels as if my dick is melting in her."
    l "Faa...."
    enemy "Fufu... was it good?{w}\nThen... This time, I'll move."

    play hseanwave "audio/se/hsean21_tentacle2.ogg"

    "The Bee Girl gently pulls me out of her, then pushes her waist against me until I'm all the way in her."
    "The Bee Girl pumps me with no mercy, bringing intense friction as the sticky honey rubs all over her insides."
    l "Ahiii! Ahhhhh!"
    enemy "More... More... Soak in the pleasure of mating with a monster.{w}\nI'll take all of your sperm..."
    "The stimulating becoming more intense, I start to leak out into her sticky interior."
    "I feel the pressure from another orgasm rise through my waist."
    l "Like this... I'm going to come... again!"
    enemy "Already? What a pathetic man...{w}\nJust let it out, then!"
    l "Faaaa!"

    with syasei1
    show mitubati ctb04 at topright zorder 15 as ct
    show mitubati h4
    $ world.ejaculation()
    stop hseanwave fadeout 1

    "Coming again, I slump in exhaustion."
    enemy "Fufu... Giving up just with this?{w}\nYou're weak..."
    enemy "It can't be helped I guess... I'll just need to train you more.{w}\nI'll take you back to my hive and mate with you even more."
    l "Auuu... No way..."
    "Even if I resist, I have no strength to do anything.{w}\nSince I lost, I have no choice in the matter..."
    enemy "But... Before that, let me squeeze out some more."

    play hseanwave "audio/se/hsean21_tentacle2.ogg"

    l "Ahiii! Ahhhh!"
    "The Bee Girl starts to shake her waist again.{w}\nAs she pumps me, her long tongue starts to lick me all over my body."
    "Feeling pleasure all over my body, I start to go crazy."
    enemy "Shoot out even more!"
    l "Ahhhhhh!"

    with syasei1
    $ world.ejaculation()

    "Licking and pumping without mercy, she squeezes even more out of me.{w}\nMy adventure ends here, as I'm turned into the Bee Girl's semen slave..."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    ".........."

    jump badend


label harpy_a_start:
    python:
        world.troops[6] = Troop(world)
        world.troops[6].enemy[0] = Enemy(world)

        world.troops[6].enemy[0].name = Character("Гарпия")
        world.troops[6].enemy[0].sprite = ["hapy_a st01", "hapy_a st02", "hapy_a st03", "hapy_a h2"]
        world.troops[6].enemy[0].position = center
        world.troops[6].enemy[0].defense = 100
        world.troops[6].enemy[0].evasion = 95
        world.troops[6].enemy[0].max_life = world.troops[6].battle.difficulty(150, 150, 150)
        world.troops[6].enemy[0].power = world.troops[6].battle.difficulty(0, 1, 2)

        world.troops[6].battle.tag = "harpy_a"
        world.troops[6].battle.name = world.troops[6].enemy[0].name
        world.troops[6].battle.background = "bg 023"
        world.troops[6].battle.music = 1
        world.troops[6].battle.exp = 50
        world.troops[6].battle.exit = "lb_0020"

        world.troops[6].battle.init()

    show hapy_a st01 at xy(0.6)
    show mob syounen at xy(0.2)

    little_boy "Уваа! Помогите!"
    "Гарпия схватила мальчика когтями и уже собиралась улетать."
    l "Стой! Отпусти ребёнка!"
    "Я достал свой меч и побежал к Гарпии.{w}\nВсе деревенские заперлись в домах."
    world.battle.enemy[0].name "Хах? Я раньше не видела тебя в этой деревне...{w}\nТы путешественник?"
    "Гарпия оценивает меня взглядом.{w}{nw}"

    show hapy_a st02

    extend "{w=.7}\nВнезапно, облизнув губы, Гарпия отпускает ребёнка."
    world.battle.enemy[0].name "Хорошо... Я отпущу мальчишку.{w}\nВместо этого... Я украду тебя~{image=note}!"
    l "Парнишка, убегай!"
    little_boy "Х-хорошо!"

    play sound "se/escape.ogg"
    hide mob

    "Как только я убедился, что мальчик в безопасности, я поднял меч."

    show hapy_a st02 at center

    world.battle.enemy[0].name "Хее-хее... ты намного приятнее...{image=note}{w}\nЯ отнесу тебя в своё гнездо, и мы сделаем много детишек."
    world.battle.enemy[0].name "Или мы можем заняться этим прямо здесь?..{w}\nБудь хорошим мальчиком, давай займёмся этим..."

    $ world.battle.face(1)

    l "..........."
    "Я правда не хочу её запечатывать, но..."
    world.battle.enemy[0].name "А вот и я!"

    $ world.battle.progress = 1
    $ world.battle.main()

label harpy_a_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result in (1, 2) and world.party.player.bind == 2:
            world.battle.common_struggle()
        elif world.battle.result == 2 and world.party.player.bind == 1:
            world.battle.common_struggle()
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label harpy_a_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "А-а-а-а!"

    play sound "se/syometu.ogg"
    hide hapy_a with crash

    "Гарпия превращается в милую маленькую птичку!"

    $ world.battle.victory(1)

label harpy_a_a:
    if world.party.player.bind == 1:
        call harpy_a_a4
        $ world.battle.main()
    elif world.party.player.bind == 2:
        $ random = rand().randint(1, 2)

        if random == 1:
            call harpy_a_a5
            $ world.battle.main()
        elif random == 2:
            call harpy_a_a6
            $ world.battle.main()

    if world.battle.turn == 3:
        call harpy_a_a3
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,3)

        if random == 1:
            call harpy_a_a1
            $ world.battle.main()
        elif random == 2:
            call harpy_a_a2
            $ world.battle.main()
        elif random == 3 and world.battle.progress > 1 and world.battle.delay[0] > 3:
            call harpy_a_a3
            $ world.battle.main()

label harpy_a_a1:
    $ world.battle.skillcount(1, 3017)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я поиграю с тобой своими ножками~{image=note}.",
        "Готовься к соитию...",
        "Хе-хе-хе-хе...{image=note}"
    ])

    $ world.battle.show_skillname("Растирание ступней гарпии")
    play sound "audio/se/ero_koki1.ogg"

    "Гарпия наступает на промежность Луки!"

    python:
        world.battle.enemy[0].damage = {1: (9, 11), 2: (13, 16), 3: (18, 22)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label harpy_a_a2:
    $ world.battle.skillcount(1, 3018)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Давай я помассирую своими перьями~{image=note}.",
        "Эхе-хе, они мягкие!~{image=note}",
        "Классные на ощупь, правда ведь?"
    ])

    $ world.battle.show_skillname("Массаж крыльями")
    play sound "audio/se/umaru.ogg"

    "Гарпия трёт Луку своими крыльями!"

    python:
        world.battle.enemy[0].damage = {1: (11, 13), 2: (16, 19), 3: (22, 26)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label harpy_a_a3:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Хе-хе-хе... время совокупляться."

    $ world.battle.show_skillname("Бросок наземь")

    "Гарпия припечатывает Луку к земле!{w}{nw}"

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    extend "{w=.7}\nИ восседает верхом на нём!"

    $ world.battle.hide_skillname()

    if world.battle.first[0]:
        return

    if world.party.player.surrendered:
        return

    world.battle.enemy[0].name "Детородное отверстие гарпий восхитительно... Приготовься."
    world.battle.enemy[0].name "Внутри так тепло и узко..."
    world.battle.enemy[0].name "Ни один человек не может сопротивляться ей~{image=note}!"

    $ world.battle.first[0] = True
    return

label harpy_a_a4:
    world.battle.enemy[0].name "Я собираюсь вставить его~{image=note}."

    $ world.battle.show_skillname("Изнасилование гарпии")
    play sound "audio/se/ero_pyu2.ogg"

    "Гарпия начинает двигать бёдрам, вгоняя член Луки внутрь себя!"

    $ world.party.player.bind = 2
    $ world.battle.face(4)

    python:
        world.battle.enemy[0].damage = {1: (16, 19), 2: (24, 28), 3: (32, 38)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    "Гарпия насилует Луку!"
    world.battle.enemy[0].name "Хе-хе-хе... как ощущения?{w}\nВагина гарпии... потрясающа, правда?"
    world.battle.enemy[0].name "Я собираюсь сделать тебе ещё приятнее... так что выпусти всё в меня~{image=note}."

    return

label harpy_a_a5:
    $ world.battle.skillcount(1, 3019)

    "Гарпия насилует Луку!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Как только перестанешь сопротивляться, выпусти в меня всю свою сперму~{image=note}.",
        "Ха-ха... ну как тебе?",
        "Хе-хе-хе... я хочу от тебя ребёночка~{image=note}.",
        "Давай уже, кончай~{image=note}!",
        "Позволь мне выность твоего здорового малыша~{image=note}."
    ])

    $ world.battle.show_skillname("Изнасилование гарпии")
    play sound "audio/se/ero_chupa4.ogg"

    if random == 1:
        "Luka is stimulated in the Harpy's pussy!"
    elif random == 2:
        "The Harpy squeezes around Luka!"
    elif random == 3:
        "Luka is stimulated by the Harpy's pussy!"
    elif random == 4:
        "The Harpy tightens around Luka!"
    elif random == 5:
        "Luka is massaged by the Harpy's tight insides!"

    python:
        world.battle.enemy[0].damage = {1: (16, 19), 2: (24, 28), 3: (32, 38)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label harpy_a_a6:
    $ world.battle.skillcount(1, 3020)

    "Гарпия насилует Луку!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ну же, просто кончи в меня~{image=note}!",
        "Аха-ха-ха, оплодотвори меня~{image=note}!",
        "Аха-ха-ха, оплодотвори меня~{image=note}!",
        "Ха-ха, кончай уже~{image=note}!",
        "Аха-ха, сможешь ли ты пережить это?"
    ])

    $ world.battle.show_skillname("Танец живота гарпии")
    play sound "audio/se/ero_chupa3.ogg"

    "Гарпия трясёт своей талией!"

    $ world.battle.cry(narrator, [
        "Член Луки массируют внутри гарпии!",
        "Лука корчится от наслаждения!",
        "Гарпия стимулирует Луку!",
        "Член Луки подрагивает в мягкой киске гарпии!",
        "Член Луки массируют внутри гарпии!",
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (16, 19), 2: (24, 28), 3: (32, 38)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label harpy_a_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Хе-хе-хе, уже сдаёшься? Тогда давай делать ребёночка...{w}\nДавай же... оплодотвори меня!{image=note}"

    $ world.battle.progress = 2
    $ world.battle.enemy[0].attack()

label harpy_a_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Harpy Footjob"
    $ list2 = "Wing Massage"
    $ list3 = "Harpy Rape"
    $ list4 = "Harpy Waist Shake"

    if persistent.skills[3017][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3018][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3019][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3020][0] > 0:
        $ list4_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == -1:
        jump harpy_a_main
    else:
        jump expression "harpy_a_onedari%s" % world.battle.result


label harpy_a_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want me to step on your penis...?{w}\nHmm... You're a strange one."

    while True:
        call harpy_a_a1


label harpy_a_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "Hahaha, you want me to play with you with my fluffy wings?{w}\nI'll right... I'll make you feel good{image=note}."

    while True:
        call harpy_a_a2


label harpy_a_onedari3:
    call onedari_syori_k

    world.battle.enemy[0].name "So you want to mate with me?{w}\nThen I'll let you have your hearts content...{image=note}"

    if not world.party.player.bind:
        call harpy_a_a3

    if world.party.player.bind == 1:
        call harpy_a_a4

    while True:
        call harpy_a_a5


label harpy_a_onedari4:
    call onedari_syori_k

    world.battle.enemy[0].name "Hahaha, so you want me to shake my hips?{image=note}{w}\nAlright, I'll play with you until you inseminate me!{image=note}"

    if not world.party.player.bind:
        call harpy_a_a3

    if world.party.player.bind == 1:
        call harpy_a_a4

    while True:
        call harpy_a_a6


label harpy_a_h1:
    $ world.party.player.moans(1)
    with syasei1
    show harpy_a bk01 zorder 10 as bk
    $ world.ejaculation()

    world.battle.enemy[0].name "Stepped on by the Harpy, Luka orgasms all over her feet."
    world.battle.enemy[0].name "Luka's body trembles with the humiliation of climaxing by being stepped on."

    $ world.battle.lose()

    $ world.battle.count([6, 10])
    $ bad1 = "Luka succumbs to the Harpy's footjob."
    $ bad2 = "After that, Luka spent every day making children with the Harpy."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Ahh... You let out so much.{w}\nBut this time, you need to let it out inside me so I can get pregnant."

    jump harpy_a_h


label harpy_a_h2:
    $ world.party.player.moans(1)
    with syasei1
    show harpy_a bk02 zorder 10 as bk
    $ world.ejaculation()

    "Massaged by her wings, Luka ejaculates all over her feathers."

    $ world.battle.lose()

    $ world.battle.count([13, 10])
    $ bad1 = "Luka succumbs to the Harpy's wings."
    $ bad2 = "After that, Luka spent every day making children with the Harpy."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hahaha, my wings felt that good?{w}\nBut this time, you need to let it out inside me so I can get pregnant."

    jump harpy_a_h


label harpy_a_h3:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Eh...?"

    with syasei1
    show hapy_a h3
    $ world.ejaculation()

    "As soon as the Harpy forces me into her, I climax.{w}\nI wasn't able to endure the feeling of insertion."

    $ world.battle.lose()

    $ world.battle.count([1, 10])
    $ bad1 = "Luka ejaculates as soon the Harpy violates him."
    $ bad2 = "After that, Luka spent every day making children with the Harpy."

    world.battle.enemy[0].name "Hahaha! You came so fast!{image=note}{w}\nOrgasming as soon as I put it in... how pathetic!{image=note}"

    show hapy_a st02
    show harpy_a bk03 zorder 10 as bk

    "The Harpy laughs as she stands up, semen dripping from her vagina."
    world.battle.enemy[0].name "Hehe... you let out a lot{image=note}.{w}\nIt felt that good inside of me...?"
    world.battle.enemy[0].name "This time, see if you can last a little longer..."

    $ persistent.count_bouhatu += 1
    jump harpy_a_h


label harpy_a_h4:
    $ world.party.player.moans(2)
    with syasei1
    show hapy_a h3
    $ world.ejaculation()

    "Unable to withstand the pleasure, Luka ejaculates inside the Harpy!"

    $ world.battle.lose()

    $ world.battle.count([1, 10])
    $ bad1 = "Raped by the Harpy, Luka can't withstand any longer."
    $ bad2 = "After that, Luka spent every day making children with the Harpy."

    world.battle.enemy[0].name "Haha... you let it out!{w}\nAll of your baby-making sperm are inside of me..."

    show hapy_a st02
    show harpy_a bk03 zorder 10 as bk

    "The Harpy laughs as she stands up, semen dripping from her vagina."
    "Hehe... you let out a lot{image=note}.{w}\nIt felt that good inside of me...?"
    "Let's make sure I get pregnant, so give me more..."

    jump harpy_a_h


label harpy_a_h5:
    $ world.party.player.moans(2)
    with syasei1
    show hapy_a h3
    $ world.ejaculation()

    "Luka ejaculates as the Harpy shakes her waist!"

    $ world.battle.lose()

    $ world.battle.count([1, 10])
    $ bad1 = "Luka succumbs to the Harpy's shaking waist."
    $ bad2 = "After that, Luka spent every day making children with the Harpy."

    world.battle.enemy[0].name "Haha... you let it out!{w}\nAll of your baby-making sperm are inside of me..."

    show hapy_a st02
    show harpy_a bk03 zorder 10 as bk

    "The Harpy laughs as she stands up, semen dripping from her vagina."
    "Hehe... you let out a lot{image=note}.{w}\nIt felt that good inside of me...?"
    "Let's make sure I get pregnant, so give me more..."

    jump harpy_a_h


label harpy_a_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    play sound "audio/se/bird.ogg"
    scene bg black with Dissolve(3.0)

    "The Harpy picks up my body and takes off into the sky."
    l "What are you..."
    world.battle.enemy[0].name "Heeheehee, you're mine now.{w}\nI'm going to take you to my nest and make you my baby-making slave."
    world.battle.enemy[0].name "We're going to mate a lot from now on!{image=note}"
    l "No way..."
    "No matter how hard I struggle, I'm unable to break her grip on me."

    scene bg 024 with blinds2
    show hapy_a st01

    world.battle.enemy[0].name "Heehee, welcome to the Harpy's village."
    "She lands in a strange village."

    show hapy_a h4

    world.battle.enemy[0].name "Let's not waste any time{image=note}."
    "As soon as the Harpy drops me on the ground, she gets on top of me."
    l "In a place like this..."
    world.battle.enemy[0].name "It's fine, isn't it?{w}\nIsn't mating outdoors natural?"
    world.battle.enemy[0].name "Come on, I'll put it in{image=note}."
    "The Harpy quickly drops her waist.{w}{nw}"

    play hseanwave "audio/se/hsean02_innerworks_a2.ogg"
    hide bk
    show hapy_a h5
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nI sink through her feathers into her warm vagina!"
    l "Ahh....! Ahhhhh..."
    "Her insides were warm and moving around like it was a separate animal."
    "The walls of the Harpy's insides are unusually rough, rubbing me with its grainy texture with every movement."
    world.battle.enemy[0].name "It feels amazing when I move like this, doesn't it?{w}\nYou'll be able to enjoy it a lot as my little stallion."
    world.battle.enemy[0].name "Don't bother trying to hold it in, just let it all out in me."
    l "Auuuuu!"
    "The Harpy tightens her insides as I let out a yell."
    "My penis is pressed from the base to the tip, the pleasure bringing me instantly to the edge."
    "Only just having penetrated her, I'm already so close..."
    world.battle.enemy[0].name "Fufu, are you already at your limit?{w}\nIt's alright, just mate a lot with me{image=note}."
    "Moving in her wriggling insides, the Harpy tightens around me.{w}\nAs the Harpy tightens, the rough feeling from her grainy vagina gives a sharp increase in the pleasure in my waist."
    world.battle.enemy[0].name "Well? My child-making hole is the greatest, isn't it?{w}\nLet it all out inside, and get me pregnant{image=note}."
    l "Amazing... Your insides..."
    "Unable to resist, the desire to come in her rises in me.{w}\nActually making a monster pregnant... If I can't endure, it would be the absolute worst form of breaking Ilias's law..."
    "The desire starts to overpower the horror of breaking the taboo...{w}\nIn this amazing pussy, I want to let it out..."
    l "Ahh... I can't... I'm coming!"
    world.battle.enemy[0].name "Then... Let me help!{image=note}"
    "As she says that, the Harpy girl tightly squeezes around me!{w}\nWith the intense stimulation from her tightness, my body joins my mind in it's surrender to the Harpy."

    with syasei1
    show hapy_a h6
    $ world.ejaculation()

    "I explode inside the Harpy's hole.{w}\nHaving surrendered both physically and mentally, I accept my position as the Harpy's child-making mate."
    world.battle.enemy[0].name "Hahaha, you let out so much.{w}\nWith this much, I'm sure to conceive{image=note}."

    play hseanwave "audio/se/hsean06_innerworks_a6.ogg"

    "As she laughs and rubs her lower waist, suddenly the Harpy starts to violently grind her waist against me!"
    l "Wh... What are you..."
    "The sudden stimulation as I'm still sensitive after ejaculating causes my body to writhe."
    world.battle.enemy[0].name "I want more of your energetic semen...{w}\nLet out more!"
    world.battle.enemy[0].name "Put more of your virile milk in me!{image=note}"
    "As if trying to hurt me, she slams her waist back and forth into mine.{w}\nCompletely at her mercy, I writhe as she pumps me."
    l "Aaaaaahhhh!"
    "Violently moving, the Harpy keeps moving faster and faster.{w}\nUnable to concentrate on anything but the forced pleasure, my vision starts to go hazy."
    "Being completely dominated by her, I have no choice but to accept the torture."
    world.battle.enemy[0].name "Hahaha... shoot more in me!{w}\nI know you can't endure much more of this{image=note}."
    l "No more...{w} Ahhhhh!"
    "Her fast movements cause the roughness of her walls to shoot ecstasy through me with their grainy texture."
    "From the surface to the tip, my dick feels like it's melting inside of her."
    world.battle.enemy[0].name "Now... Inseminate me again{image=note}."
    l "Aaa....aahhhhh..."

    with syasei1
    show hapy_a h7
    $ world.ejaculation()

    "I explode inside of the Harpy again, shooting more of my seed into her.{w}\nEven though it was my third time, there was a surprising amount."
    world.battle.enemy[0].name "That's it...{w}\nPut all of your child-making seed in me...{w}\nWe can make a lot of cute babies{image=note}."
    "After my last spurt, the Harpy starts to move her waist again.{w}\nMaking deep movements, she continues to rape me."
    "Each forced thrust of pleasure seems to drain more of my energy."
    l "St...op it alre...ady..."
    world.battle.enemy[0].name "It's way too early to stop{image=note}.{w}\nUntil there's nothing left, I want all of your seed."
    world.battle.enemy[0].name "We'll live together forever in this village after this{image=note}."
    l "Ahhhhh!"

    with syasei1
    show hapy_a h8
    $ world.ejaculation()

    "My mating with the Harpy continued for a long time after that...{w}\nForced to come over and over, I lose consciousness."
    "Squeezed until I was empty, I fainted in ecstasy."

    stop hseanwave fadeout 1

    world.battle.enemy[0].name "Oh? Down already?{w}\nWell, I'll let you rest for a while.{w}\nAfter that round, I'm sure I'll be pregnant{image=note}."

    scene bg black with Dissolve(3.0)

    "There's no escaping from this Harpy.{w}\nMy adventure ends here, stuck in the Harpy's village."
    "I'll mate with the Harpy every day...{w}\nMaybe it won't be so bad..."
    "..........."

    jump badend

label harpy_bc_start:
    python:
        world.troops[7] = Troop(world)
        world.troops[7].enemy[0] = Enemy(world)

        world.troops[7].enemy[0].name = Character("Гарпии-сёстры")
        world.troops[7].enemy[0].sprite = ["hapy_bc st01", "hapy_bc st02", "hapy_bc st03"]
        world.troops[7].enemy[0].position = center
        world.troops[7].enemy[0].defense = 100
        world.troops[7].enemy[0].evasion = 95
        world.troops[7].enemy[0].max_life = world.troops[7].battle.difficulty(120, 160, 160)
        world.troops[7].enemy[0].power = world.troops[7].battle.difficulty(0, 1, 2)

        world.troops[7].battle.tag = "harpy_bc"
        world.troops[7].battle.name = world.troops[7].enemy[0].name
        world.troops[7].battle.background = "bg 027"
        world.troops[7].battle.music = 1
        world.troops[7].battle.exp = 60
        world.troops[7].battle.exit = "lb_0021"
        world.troops[7].battle.plural_name = True

        world.troops[7].battle.init()

    younger_harpy "Сестричка, а я не видела этого мальчика раньше."
    older_harpy "И правда...{w}\nЧто ты здесь делаешь?.."
    l "Дерьмо..."

    show hapy_bc st01a

    younger_harpy "Эй... сестра...{w}\nЯ хочу этого мальчика..."
    older_harpy "Хо-хо... Нравится?{w}\nЛадно, он будет твоим первым партнёром."
    older_harpy "Для начала его надо ослабить..."
    l "......................"

    $ world.party.player.skill_set(2)

    younger_harpy "Аах!{w}\nСестрёнка, я боюсь его!"
    older_harpy "Быстрее, улетаем от сюда!"

    play sound "se/bird.ogg"
    hide hapy_bc

label harpy_bc_v:
    $ world.party.player.skill_set(1)
    $ world.battle.victory(2)

label queenharpy_start:
    python:
        world.troops[8] = Troop(world)
        world.troops[8].enemy[0] = Enemy(world)

        world.troops[8].enemy[0].name = Character("Королева гарпий")
        world.troops[8].enemy[0].sprite = ["queenharpy st01", "queenharpy st03", "queenharpy st04"]
        world.troops[8].enemy[0].position = center
        world.troops[8].enemy[0].defense = 100
        world.troops[8].enemy[0].evasion = 100
        world.troops[8].enemy[0].max_life = world.troops[8].battle.difficulty(350, 350, 350)
        world.troops[8].enemy[0].power = world.troops[8].battle.difficulty(0, 1, 2)

        world.troops[8].battle.tag = "queenharpy"
        world.troops[8].battle.name = world.troops[8].enemy[0].name
        world.troops[8].battle.background = "bg 027"
        world.troops[8].battle.music = 2

        world.troops[8].battle.init()

    $ world.battle.face(1)

    l "Гх..."
    "Я правда не хочу прибегать к такому методу решения проблемы...{w}\nНо, похоже, у меня нет иного выбора!"
    world.battle.enemy[0].name "Направлять на Королеву меч — это серьёзный проступок.{w}\nИ карается он всегда одинаково — изнасилованием."
    world.battle.enemy[0].name "Я лично исполню приговор."

    $ world.battle.enemy[0].wind = 0

    $ world.battle.progress = 1
    $ world.battle.main()

label queenharpy_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.party.player.bind in (1, 2):
            world.battle.common_struggle()
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label queenharpy_v:
    stop music fadeout 2.0
    $ world.battle.face(3)

    world.battle.enemy[0].name "... Ауч."
    "Королева гарпий смотрит на меня широко открытыми от удивления глазами."
    "Это всё?{w} Я выиграл?"

    $ world.battle.face(2)

    "... Похоже нет."

    world.battle.enemy[0].name "... Знаешь, изначально я думала, что это несправедливо - сражаться с таким мальчиком."
    world.battle.enemy[0].name "Но теперь..."

    $ world.battle.progress = 2

    jump queenharpy_ng_start

label queenharpy_a:
    if world.party.player.bind == 1:
        call queenharpy_a6
        $ world.battle.main()

    elif world.party.player.bind == 2:
        $ random = rand().randint(1,2)

        if random == 1:
            call queenharpy_a7
            $ world.battle.main()
        elif random == 2:
            call queenharpy_a8
            $ world.battle.main()

    if not world.party.player.surrendered:
        $ world.battle.stage[1] += 1

        if world.battle.stage[1] == 3 and world.battle.progress == 2:
            call queenharpy_x1
            $ world.battle.main()

        if world.battle.enemy[0].counter == 1:
            call queenharpy_x2
            $ world.battle.main()
        elif world.battle.enemy[0].counter == 2:
            call queenharpy_x3
            $ world.battle.main()

    while True:
        $ random = rand().randint(1,6)

        if random == 1 and world.battle.delay[0] > 1:
            call queenharpy_a5
            $ world.battle.main()
        elif random == 2:
            call queenharpy_a1
            $ world.battle.main()
        elif random == 3:
            call queenharpy_a2
            $ world.battle.main()
        elif random == 4:
            call queenharpy_a3
            $ world.battle.main()
        elif random == 5:
            call queenharpy_a4
            $ world.battle.main()

label queenharpy_a1:
    $ world.battle.skillcount(1, 3025)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Должна ли я наступить на тебя?",
        "Неужели мужчинам это правда нравится?",
        "Почувствовать удовольствие от такого..."
    ])

    $ world.battle.show_skillname("Растирание ступней гарпии")
    play sound "audio/se/ero_koki1.ogg"

    "Королева гарпий наступает на промежность Луки!"

    python:
        world.battle.enemy[0].damage = {1: (11, 13), 2: (14, 17), 3: (20, 24)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label queenharpy_a2:
    $ world.battle.skillcount(1, 3026)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "*Лижет* *Сосёт*",
        "Королева должна использовать её язык...",
        "Я награжу твои генитателии королевским поцелуем.",
        "Должна ли я его немного пососать?..",
        "Если я сделаю так, ты больше не сможешь сдерживаться..."
    ])

    $ world.battle.show_skillname("Минет гарпии")
    play sound "audio/se/ero_buchu3.ogg"

    $ world.battle.cry(narrator, [
        "Королева гарпий сосёт член Луки!",
        "Королева гарпий лижет член Луки!",
        "Королева гарпий целует член Луки!",
        "Королева гарпий сосёт член Луки!..",
        "Королева гарпий лижет член Луки!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (14, 16), 2: (19, 22), 3: (26, 30)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label queenharpy_a3:
    $ world.battle.skillcount(1, 3027)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Может мне тебя потереть своими крыльями?..",
        "Почувствуй мои объятия...",
        "Пади в глубины наслаждений от моих перьев..."
    ])

    $ world.battle.show_skillname("Массаж перьями")
    play sound "audio/se/umaru.ogg"

    "Королева Гарпий трётся о Луку своими мягкими перьями!"

    python:
        world.battle.enemy[0].damage = {1: (15, 17), 2: (20, 23), 3: (28, 32)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label queenharpy_a4:
    $ world.battle.skillcount(1, 3028)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Уфу-фу... как тебе тебе?",
        "Отведай мою грудь...",
        "Уфу-фу... как тебе тебе?",
        "Просто кончи мне на грудь...",
        "Утони в экстазе..."
    ])

    $ world.battle.show_skillname("Королевское пайзури")
    play sound "audio/se/ero_koki1.ogg"

    $ world.battle.cry(narrator, [
        "Королева гарпий зажимает член Луки меж своих сисек!",
        "Королева гарпий зажимает член Луки меж своих сисек!",
        "Пади в глубины наслаждений от моих перьев...",
        "Королева гарпий сдавливает свою грудь, зажимая член Луки!",
        "Королева гарпий сжимает свою грудь, зажимая член Луки!",
        "Королева гарпий запихивает член Луки меж своих сисек и начинает трясти ими!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (15, 17), 2: (20, 23), 3: (28, 32)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label queenharpy_a5:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Должны ли мы стать немного... ближе?"

    $ world.battle.show_skillname("Бросок наземь")

    "Королева гарпий толкает Луку на землю!"

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    extend "{w=.7}\nИ восседает верхом на нём!"

    $ world.battle.hide_skillname()

    $ world.battle.face(2)

    world.battle.enemy[0].name "Возрадуйся же.{w}\nТакому как ты выпала честь совокупиться с королевой.{w}\nОтдай же всё своё семя мне в матку..."

    return

label queenharpy_a6:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Должны ли мы стать немного... ближе?"

    $ world.battle.show_skillname("Королевский секс")
    play sound "audio/se/ero_pyu5.ogg"
    show queenharpy h1
    show queenharpy ct01 at topleft zorder 15 as ct

    "Королева гарпий резко опускает бёдра, вгоняя член Луки внутрь себя!"

    $ world.party.player.bind = 2
    $ world.party.player.status_print()

    python:
        world.battle.enemy[0].damage = (18, 20)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h5")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    world.battle.enemy[0].name "Уфу-фу... как тебе ощущение от королевского нутра?{w}\nПохоже твоему члену нравится..."

    return

label queenharpy_a7:
    $ world.battle.skillcount(1, 3029)

    "Королева гарпий насилует Луку!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Вкуси удовольствия, которое может дать только киска королевы...",
        "Потрясающе, не так ли?",
        "Такой слабый мальчик как ты не сможет одолеть королеву...",
        "Выпусти всё в меня...",
        "Просто сдайся и утони в экстазе..."
    ])

    $ world.battle.show_skillname("Королевская киска")
    play sound "audio/se/ero_chupa4.ogg"

    $ world.battle.cry(narrator, [
        "Королева гарпий двигает своими бёдрами!",
        "Вагина королевы гарпий начинает сжиматься!",
        "Вагина королевы гарпий сжимается!",
        "Выпусти всё в меня...",
        "Сладкое наслаждение от схватившей его королевы гарпий захлёстывает разум Луки."
    ], True)

    python:
        world.battle.enemy[0].damage = (18, 20)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h6")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label queenharpy_a8:
    $ world.battle.skillcount(1, 3030)

    "Королева гарпий насилует Луку!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Уфу-фу... сможешь ли ты пережить это?",
        "Погрузись в экстаз...",
        "Это ты не переживёшь...",
        "Уфу-фу... кончи же!",
        "Выпусти всё в меня..."
    ])

    $ world.battle.show_skillname("Королевская наездница")
    play sound "audio/se/ero_chupa3.ogg"

    $ world.battle.cry(narrator, [
        "Королева гарпий начинает резво скакать на Луке!",
        "Королева гарпий прижимается к Луке, вгоняя его член глубоко внутрь себя!",
        "Королева гарпий качает своими бёдрами!{w}\nУдовольствие начинает затмивать разум Луки!",
        "Королева гарпий медленно поднимает-опускает свои бёдра!{w}\nЕё нутро нежно щекочит член Луки!",
        "Королева гарпий начинает резво скакать на Луке!"
    ], True)

    python:
        world.battle.enemy[0].damage = (21, 23)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h7")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label queenharpy_x1:
    world.battle.enemy[0].name "Охо-хо..."

    $ world.battle.enemy[0].sprite[0] = "queenharpy st02_2"
    $ world.battle.enemy[0].sprite[1] = "queenharpy st02_2"
    show queenharpy st02_2
    $ world.battle.show_skillname("Королевское приглашение")
    play sound "audio/se/wind1.ogg"

    "Королева гарпий расправляет крылья!{w}\nНо остаётся неподвижной!"

    $ world.battle.hide_skillname()

    if not world.battle.first[0]:
        world.battle.enemy[0].name "Покажи свой лучший удар.{w}\nТы слишком жалок, чтобы навредить мне."
        l "Что?.."
        "У меня плохое предчувствие...{w}\nДолжен ли я атаковать?.."
    else:
        world.battle.enemy[0].name "Не подойдёшь? Уфу-фу..."

    $ world.battle.first[0] = True
    $ world.battle.enemy[0].counter = 1

    return

label queenharpy_x2:
    world.battle.enemy[0].name "Охо-хо..."

    $ world.battle.show_skillname("Royal Invitation")
    play sound "audio/se/wind1.ogg"

    "Королева гарпий расправляет крылья!{w}\nНо остаётся неподвижной!"

    $ world.battle.hide_skillname()

    world.battle.enemy[0].name "Уфу-фу.. игнорируешь моё приглашение?"
    l ".........."

    $ world.battle.enemy[0].counter = 2

    return

label queenharpy_x3:
    world.battle.enemy[0].name "А ты осторожен..."

    $ world.battle.enemy[0].sprite[0] = "queenharpy st01"
    $ world.battle.enemy[0].sprite[1] = "queenharpy st03"
    $ world.battle.face(1)

    "Королева гарпий опускает крылья!"

    $ world.battle.enemy[0].counter = 0
    $ world.battle.stage[1] = 0

    return

label queenharpy_counter:
    $ world.battle.skillcount(1, 3031)

    if world.battle.surrendered:
        show queenharpy st03_2

        world.battle.enemy[0].name "I'll return it...{w}\nSurrender inside of me!"

    $ world.battle.show_skillname("Happiness Rondo")
    play sound "audio/se/ero_pyu2.ogg"
    show queenharpy h1
    show queenharpy ct01 at topleft zorder 15 as ct

    "The Queen Harpy pins down Luka and forces him inside of her!{w}\nLuka's penis is wrapped by her warm vagina!"

    $ world.battle.enemy[0].damage = (30, 35)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    play sound "audio/se/ero_chupa4.ogg"

    "Luka is being forcefully rubbed by her tight pussy!"

    $ world.battle.enemy[0].damage = (30, 35)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    play sound "audio/se/ero_chupa5.ogg"

    "Luka is violently massaged deep in the Queen Harpy!{w}\nAs if being milked, her walls rub against Luka!"

    python:
        world.battle.enemy[0].damage = (30, 35)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h7")

    return

label queenharpy_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Значит ты признаёшь своё неизбежное поражение.{w}\nПозволь мне играть с тобой пока мне не наскучит..."

    $ world.battle.enemy[0].attack()

label queenharpy_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Harpy Footjob"
    $ list2 = "Минет Гарпии"
    $ list3 = "Feather Massage"
    $ list4 = "Royal Tit Fuck"
    $ list5 = "Royal Pussy"
    $ list6 = "Royal Pumping"
    $ list7 = "Happiness Rondo"

    if persistent.skills[3025][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3026][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3027][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3028][0] > 0:
        $ list4_unlock = 1

    if persistent.skills[3029][0] > 0:
        $ list5_unlock = 1

    if persistent.skills[3030][0] > 0:
        $ list6_unlock = 1

    if persistent.skills[3031][0] > 0:
        $ list7_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump queenharpy_onedari1
    elif world.battle.result == 2:
        jump queenharpy_onedari2
    elif world.battle.result == 3:
        jump queenharpy_onedari3
    elif world.battle.result == 4:
        jump queenharpy_onedari4
    elif world.battle.result == 5:
        jump queenharpy_onedari5
    elif world.battle.result == 6:
        jump queenharpy_onedari5
    elif world.battle.result == 7:
        jump queenharpy_onedari6
    elif world.battle.result == -1:
        jump queenharpy_main


label queenharpy_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You wish to be trampled under the Queen's talon?{w}\nAllow me to fulfill your desire..."

    while True:
        call queenharpy_a1


label queenharpy_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want to be defeated by the Queen's mouth?{w}\nAllow me to fulfill your desire..."

    while True:
        call queenharpy_a2


label queenharpy_onedari3:
    call onedari_syori

    world.battle.enemy[0].name "You want to be defeated by the Queen's wings?{w}\nAllow me to fulfill your desire..."

    while True:
        call queenharpy_a3


label queenharpy_onedari4:
    call onedari_syori

    world.battle.enemy[0].name "You want to be defeated by the Queen's breasts?{w}\nAllow me to fulfill your desire..."

    while True:
        call queenharpy_a4


label queenharpy_onedari5:
    call onedari_syori_k

    if world.battle.result == 5:
        world.battle.enemy[0].name "So you want me to start pumping you?"
    elif world.battle.result == 6:
        world.battle.enemy[0].name "So you want to thrust inside the Queen's pussy?"

    world.battle.enemy[0].name "Allow me to fulfill your desire..."

    if not world.party.player.bind:
        call queenharpy_a5

    if world.party.player.bind == 1:
        call queenharpy_a6

    while True:
        if world.battle.result == 5:
            call queenharpy_a7

        if world.battle.result == 6:
            call queenharpy_a8


label queenharpy_onedari6:
    call onedari_syori

    world.battle.enemy[0].name "You want me to use my ultimate technique?{w}\nThen, I'll finish you off in one shot..."

    while True:
        call queenharpy_counter


label queenharpy_h1:
    $ world.party.player.moans(1)
    with syasei1
    show queenharpy bk03 zorder 10 as bk
    $ world.ejaculation()

    "Unable to resist any longer, Luka explodes all over the Queen Harpy's foot."

    $ world.battle.lose()

    $ world.battle.count([6, 1])
    $ bad1 = "Luka succumbs to the Queen Harpy's footjob."
    $ bad2 = "After that, Luka spent the rest of his days as the Queen's mate, indulging in pleasure every day."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Climaxing because of my foot?{w}\nHow miserable."
    "The Queen Harpy laughs as she shows me her semen covered foot, as if taunting me with proof of my defeat."
    world.battle.enemy[0].name "That's the end of the foreplay...{w}\nNext is the real thing.{w}\nLet's mate without holding back..."

    jump queenharpy_h


label queenharpy_h2:
    $ world.party.player.moans(1)
    with syasei1
    show queenharpy bk01 zorder 10 as bk
    $ world.ejaculation()

    "Unable to resist any longer, Luka explodes inside the Queen Harpy's mouth."

    $ world.battle.lose()

    $ world.battle.count([4, 1])
    $ bad1 = "Luka succumbs to the Queen Harpy's blowjob."
    $ bad2 = "After that, Luka spent the rest of his days as the Queen's mate, indulging in pleasure every day."
    $ world.battle.face(2)

    world.battle.enemy[0].name "You let out so much in my mouth...{w}\nBut if you put it all out in here, I won't be able to conceive..."
    "The Queen Harpy laughs as she shows me her semen filled mouth, as if taunting me with proof of my defeat."
    world.battle.enemy[0].name "That's the end of the foreplay...{w}\nNext is the real thing.{w}\nLet's mate without holding back..."

    jump queenharpy_h


label queenharpy_h3:
    $ world.party.player.moans(1)
    with syasei1
    show queenharpy bk04 zorder 10 as bk
    $ world.ejaculation()

    "Unable to resist any longer, Luka explodes all over the Queen Harpy's wings."

    $ world.battle.lose()

    $ world.battle.count([13, 1])
    $ bad1 = "Luka succumbs to the Queen Harpy's feather massage."
    $ bad2 = "After that, Luka spent the rest of his days as the Queen's mate, indulging in pleasure every day."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Fufu... You covered my wings with your sticky semen.{w}\nMaking them so dirty... How impolite."
    "The Queen Harpy laughs as she shows me her semen covered wings, as if taunting me with proof of my defeat."
    world.battle.enemy[0].name "That's the end of the foreplay...{w}\nNext is the real thing.{w}\nLet's mate without holding back..."

    jump queenharpy_h


label queenharpy_h4:
    $ world.party.player.moans(1)
    with syasei1
    show queenharpy bk02 zorder 10 as bk
    $ world.ejaculation()

    "Unable to resist any longer, Luka explodes all over the Queen Harpy's chest."

    $ world.battle.lose()

    $ world.battle.count([7, 1])
    $ bad1 = "Luka succumbs to the Queen Harpy's tit fuck."
    $ bad2 = "After that, Luka spent the rest of his days as the Queen's mate, indulging in pleasure every day."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Making my chest so dirty...{w}\nDid it really feel that good?"
    "The Queen Harpy laughs as she shows me her semen covered chest, as if taunting me with proof of my defeat."
    world.battle.enemy[0].name "That's the end of the foreplay...{w}\nNext is the real thing.{w}\nLet's mate without holding back..."

    jump queenharpy_h


label queenharpy_h5:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Ara?"

    with syasei1
    show queenharpy h2
    show queenharpy ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Unable to resist the pleasure just from insertion, I explode inside of the Queen Harpy."

    $ world.battle.lose()

    $ world.battle.count([1, 1])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka succumbs by accident just from insertion."
    $ bad2 = "After that, Luka spent the rest of his days as the Queen's mate, indulging in pleasure every day."

    world.battle.enemy[0].name "Fufu... How pathetic.{w}\nComing so quickly... The ultimate disgrace of a man."

    hide ct
    show queenharpy st02
    show queenharpy bk05 zorder 10 as bk

    "The Queen Harpy stands up and takes my penis out of her, laughing down at me."
    "The semen I shot into her drips out, giving proof of my defeat."
    world.battle.enemy[0].name "Now, let's do it again.{w}\nI know you haven't had enough from just doing it once.{w}\nI'll let you indulge in the pleasure for a long time..."

    jump queenharpy_h


label queenharpy_h6:
    $ world.party.player.moans(2)
    with syasei1
    show queenharpy h2
    show queenharpy ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Unable to withstand her the pleasure her pussy was bringing, I explode inside of the Queen Harpy."

    $ world.battle.lose()

    $ world.battle.count([1, 1])
    $ bad1 = "Luka succumbs to the Queen Harpy's tight pussy."
    $ bad2 = "After that, Luka spent the rest of his days as the Queen's mate, indulging in pleasure every day."

    world.battle.enemy[0].name "Fufu... How was the Queen's pussy?"

    hide ct
    show queenharpy st02
    show queenharpy bk05 zorder 10 as bk

    "The Queen Harpy stands up and takes my penis out of her, laughing down at me."
    "The semen I shot into her drips out, giving proof of my defeat."
    world.battle.enemy[0].name "Now, let's do it again.{w}\nI know you haven't had enough from just doing it once.{w}\nI'll let you indulge in the pleasure for a long time..."

    jump queenharpy_h


label queenharpy_h7:
    $ world.party.player.moans(2)
    with syasei1
    show queenharpy h2
    show queenharpy ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Unable to withstand her the pleasure her pumping waist was bringing, I explode inside of the Queen Harpy."

    $ world.battle.lose()

    $ world.battle.count([1, 1])
    $ bad1 = "Luka succumbs to the Queen's thrusting waist."
    $ bad2 = "After that, Luka spent the rest of his days as the Queen's mate, indulging in pleasure every day."

    world.battle.enemy[0].name "Well?{w}\nDid the technique of a Queen send you to heaven?"

    hide ct
    show queenharpy st02
    show queenharpy bk05 zorder 10 as bk

    "The Queen Harpy stands up and takes my penis out of her, laughing down at me."
    "The semen I shot into her drips out, giving proof of my defeat."
    world.battle.enemy[0].name "Now, let's do it again.{w}\nI know you haven't had enough from just doing it once.{w}\nI'll let you indulge in the pleasure for a long time..."

    jump queenharpy_h


label queenharpy_h8:
    $ world.party.player.moans(2)
    with syasei1
    show queenharpy h2
    show queenharpy ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    l "Auuuu!"
    "My body twitches as I explode inside the Queen Harpy."
    "The Queen Harpy continues to pump me even as I shoot into her.{w}\nSqueezed to the last drop, I bask in the pleasure."

    $ world.battle.lose()

    $ world.battle.count([1, 1])
    $ bad1 = "Luka succumbs instantly to the Happiness Rondo"
    $ bad2 = "After that, Luka spent the rest of his days as the Queen's mate, indulging in pleasure every day."
    $ world.battle.enemy[0].sprite[0] = "queenharpy st01"
    $ world.battle.enemy[0].sprite[1] = "queenharpy st03"
    $ world.battle.enemy[0].sprite[2] = "queenharpy st04"
    $ world.battle.enemy[0].sprite[3] = "queenharpy st03"

    world.battle.enemy[0].name "Fufu... Instant loss.{w}\nIf I become even a little serious, someone like you stands no chance."

    hide ct
    show queenharpy st02
    show queenharpy bk05 zorder 10 as bk

    "The Queen Harpy stands up and takes my penis out of her, laughing down at me."
    "The semen I shot into her drips out, giving proof of my defeat."
    world.battle.enemy[0].name "Now, let's do it again.{w}\nI know you haven't had enough from just doing it once.{w}\nI'll let you indulge in the pleasure for a long time..."

    jump queenharpy_h


label queenharpy_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    "The Queen Harpy holds me tightly, and forces my waist into hers."
    world.battle.enemy[0].name "The Queen is the center of the tribe.{w}\nTo have lots of children, I need to mate with many men."
    world.battle.enemy[0].name "Because of that, I have to mate quickly.{w}\nA Queen's pussy is so exquisite that a normal man can barely last ten seconds."
    world.battle.enemy[0].name "Shall I give you a taste...?"

    play hseanwave "audio/se/hsean01_innerworks_a1.ogg"
    hide ct
    hide bk
    show queenharpy h1

    "The Queen Harpy tightly holds me, and pushes me into her."
    l "Aaaa!"

    show queenharpy ct01 at topleft zorder 15 as ct

    "Her insides were slimy and warm, and slightly loose."
    "But her walls were covered with rough spots, and the grainy texture sent chills down my spine with every movement."
    world.battle.enemy[0].name "Do the soft bumps inside of me feel good?{w}\nJust being inserted into the Queen is an unbearable pleasure, isn't it?"
    world.battle.enemy[0].name "Once you reach all the way inside, there's no way you can endure."
    l "Ahiii!"
    "If I hadn't just ejaculated, there's no way I would have endured this long."
    "Wrapped inside of her, a strange pleasure spreads throughout my dick."
    l "What is this...?"
    world.battle.enemy[0].name "At the deepest place inside of me is a special part that fits the tip of the penis.{w}\nWith it, I can specially massage the tip separate from the rest of the penis."
    world.battle.enemy[0].name "With the Queen's special technique, I can force any man to come whenever I want."
    l "Ahhhh!"

    play hseanwave "audio/se/hsean04_innerworks_a4.ogg"

    "In her deepest part, I can feel her walls start to close around my tip.{w}\nThe intense pleasure makes my body start to writhe."
    world.battle.enemy[0].name "Fufu... I only did it for a second, but you avoided an instant death, huh?"
    world.battle.enemy[0].name "But no matter.{w}\nI'll keep doing it, and squeeze everything out of you until you're no longer able to move."
    l "Ahii! Amazing!"
    l "Aahhhh!"
    "The Queen Harpy tightens around my tip again, deep inside of her.{w}\nSurrounded by her soft flesh, I go over the edge."
    "Even though I just came, I feel myself coming again."
    l "Haauuu!"

    with syasei1
    show queenharpy h2
    show queenharpy ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    "I came again so soon...{w}\nSurrendering to the pleasure, I explode inside of her."
    "The Queen Harpy continues to squeeze me deep inside of her, even as I orgasm."
    world.battle.enemy[0].name "Fufu... Already giving up?{w}\nYou actually managed to last twenty seconds..."
    world.battle.enemy[0].name "Shall I force you to come a few more times?"
    l "Ahii! Ahhhh!"
    "Even before I finish ejaculating, she's already trying to squeeze out the next."
    "Stuck in her, I have no choice but to accept the pleasure."
    l "Stop... Or I'll come again..."
    world.battle.enemy[0].name "Come.{w}\nThere's no point holding on.{w} Here, I'll do this."
    l "Hauuu!"
    "The Queen Harpy suddenly contracts around me as my mind goes blank."

    with syasei1
    $ world.ejaculation()

    "I just came a few seconds ago, but I'm already forced to another height."
    world.battle.enemy[0].name "Fufu... This is what it's like to mate with the Queen."
    world.battle.enemy[0].name "I can force a man to ejaculate whenever I wish."
    world.battle.enemy[0].name "Like this, I can mate many times every day.{w}\nLet's go again. Can you last longer than a few seconds this time?"
    l "Haaa! Stop it!"
    "Ignoring my pleas, she tightens around my tip deep inside of her again."
    "Her unending assault leads me to a world of pleasure."
    "It was as if as soon as I entered into her, I was forced to obey every command this Queen gave."
    "There's no way I can endure this..."
    l "Co...coming!"

    with syasei1
    $ world.ejaculation()

    "The Queen stares at me as I shoot more proof of my surrender into her."
    world.battle.enemy[0].name "Fufu... And that's how a Queen Harpy mates with men.{w}\nThe ultimate pleasure forces men into surrender and shame."
    world.battle.enemy[0].name "But soon you'll be a slave to my whims, and ejaculate whenever I wish.{w}\nAccept that position as your new joy."
    l "Auu.. Such a thing..."
    "Fear starts to run through me at her words, as I try to pull out of her.{w}\nBut..."
    world.battle.enemy[0].name "Once you're in the Queen, you can't get out.{w}\nIf you try to pull out..."
    l "Ahh! Ahhhh!"
    "When I try to pull out of her, the friction caused by her deepest part holding onto my tip is too much to bear."
    world.battle.enemy[0].name "A harpy has a different vagina than other monsters.{w}\nOnce you're in the deepest part, if you try to pull it out the pleasure only increases."
    world.battle.enemy[0].name "The extreme pleasure will force a man to come if he tries to escape."
    l "Ahi... Ha..."
    "I can't pull out of her even a little bit.{w}\nWhenever I do, an even more powerful pleasure attacks me."
    "Trying one last time, the pleasure is too much for me and sends me over the edge."
    l "Aahhh!"

    with syasei1
    show queenharpy h3
    show queenharpy ct03 at topleft zorder 15 as ct
    $ world.ejaculation()

    world.battle.enemy[0].name "Do you understand now?{w}\nThere's no escape from me..."
    "I explode yet again inside of the Queen Harpy.{w}\nUnable to pull out, I writhe helplessly inside of her."
    world.battle.enemy[0].name "Let me wring out a few more from you.{w}\nDon't worry though, I won't kill you.{w}\nFor now, let's just go until you faint."
    l "Haaaaa!"
    "The Queen Harpy squeezes me deep inside of her yet again.{w}\nUnable to resist her, I go over the edge again."
    l "Aahhh..."

    with syasei1
    $ world.ejaculation()

    world.battle.enemy[0].name "More... More...{w}\nLet me drain everything out of you..."
    l "Sto...p... Faa...."

    with syasei1
    $ world.ejaculation()

    "I pour even more into the Queen Harpy.{w}\nUnable to escape, I'm brought to the peak over and over."
    "Dozens of times... I've lost count."
    l "Ahiiii!"

    with syasei1
    show queenharpy h4
    show queenharpy ct04 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Over and over again, my vision starts to fade.{w}\nOn the border of consciousness, I hear the Queen's words."
    world.battle.enemy[0].name "This is your punishment for raising your sword to me.{w}\nI don't care what your motive was."
    world.battle.enemy[0].name "Your courage and determination was admirable, though.{w}\nI shall make you my mate."
    world.battle.enemy[0].name "With your genes, we're sure to have many excellent offspring."
    l "Aaa...."

    with syasei1
    $ world.ejaculation()
    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Ejaculating one final time, I faint."
    world.battle.enemy[0].name "Oh, you fainted?{w}\nWith today's effort, I should be able to have three excellent children..."
    l "Aaa...."
    "From now on, I live only to inseminate the Queen.{w}\nEvery day, I'm violated until I faint.{w}\nIt isn't so bad..."
    ".........."

    jump badend

label queenharpy_ng_start:
    python:
        world.troops[9] = Troop(world)
        world.troops[9].enemy[0] = Enemy(world)

        world.troops[9].enemy[0].name = Character("Королева гарпий")
        world.troops[9].enemy[0].sprite = ["queenharpy st13", "queenharpy st24", "queenharpy st22", "queenharpy st23"]
        world.troops[9].enemy[0].position = center
        world.troops[9].enemy[0].defense = 90
        world.troops[9].enemy[0].evasion = 100
        world.troops[9].enemy[0].max_life = world.troops[9].battle.difficulty(25000, 30000, 35000)
        world.troops[9].enemy[0].henkahp = [11000, 5000]
        world.troops[9].enemy[0].power = world.troops[9].battle.difficulty(0, 1, 2)

        world.troops[9].battle.tag = "queenharpy_ng"
        world.troops[9].battle.name = world.troops[9].enemy[0].name
        world.troops[9].battle.background = "bg 027"

        if persistent.music:
            world.troops[9].battle.music = 26
        else:
            world.troops[9].battle.music = 2

        world.troops[9].battle.ng = True
        world.troops[9].battle.nocharge = 1
        world.troops[9].battle.exp = 950000
        world.troops[9].battle.exit = "lb_0022"

        world.troops[9].battle.init(0)

    world.battle.enemy[0].name "Я в самом деле хочу изнасиловать тебя."
    l "Подожди-подожди, что?"
    "Этого не было раньше..."

    $ world.troops[9].enemy[0].sprite[0] = "queenharpy st21"
    play sound "se/escape.ogg"
    show queenharpy st23

    "... Этого определённо раньше не было!"
    world.battle.enemy[0].name "Пришла пора показать тебе истинную силу Королевы Гарпий!"

    play sound "se/bird.ogg"

    "Королева гарпий призывает силу ветра!"

    play sound "se/wind2.ogg"
    show effect wind zorder 30
    pause 1.0
    hide effect

    if persistent.difficulty < 3:
        $ world.battle.enemy[0].wind = 2
        $ world.battle.enemy[0].wind_turn = 6
    else:
        $ world.battle.enemy[0].wind = 1
        $ world.battle.enemy[0].wind_turn = -1
        if _in_replay:
            $ world.party.player.skill[0] = 26

    $ world.battle.face(1)

    l "Гах..."
    "Этот бой будет намного труднее, чем раньше.{w}\nПолагаю, у меня не остаётся выбора!"

    $ world.party.player.skill_set(2)

    "Я неохотно снимаю кольцо матери и встаю в боевую стойку."

    $ world.battle.main()

label queenharpy_ng_main:
    python:
        if not world.battle.enemy[0].wind and world.battle.enemy[0].wind_turn:
            world.battle.enemy[0].wind_turn = 0

        if world.battle.result == 44 and world.battle.enemy[0].wind and not world.party.player.daystar and world.battle.stage[0] == 1:
            world.battle.enemy[0].wind_turn = 0
            world.battle.enemy[0].element_end()
            world.battle.enemy[0].wind_turn = 1
        elif world.battle.result == 44 and world.battle.enemy[0].wind and not world.party.player.daystar and world.battle.stage[0] == 2:
            world.battle.enemy[0].wind_turn = 0
            world.battle.enemy[0].element_end()
            world.battle.enemy[0].wind_turn = 2

        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1 and world.battle.enemy[0].wind:
            renpy.jump(f"{world.battle.tag}_miss")
        elif world.battle.result == 1 and not world.battle.enemy[0].wind:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label queenharpy_ng_miss:
    l "Хаа!"
    "Лука атакует!"

    $ world.party.player.mp_regen()
    pause .5
    play sound "se/bird.ogg"
    hide queenharpy
    pause .5
    $ world.battle.face(2)

    "Но Королева Гарпий двигается подобно ветру и уворачивается!"

    if not world.battle.first[0]:
        call queenharpy_ng001

    $ world.battle.face(1)

    $ world.battle.enemy[0].attack()

label queenharpy_ng001:
    l "Что за?!"
    "Она просто взлетела в воздух!"
    "И эта скорость!..{w}\nДолжно быть она быстрее даже Альмы Эльмы!"

    show queenharpy st23

    queen_harpy "Ха-ха.{w}\nИ это все на что ты способен?"
    l "Дерьмо..."
    "У меня нет Сильфы или Гномы с собой, чтобы противостоять ей.{w}\nЧто же мне делать?.."

    $ world.battle.first[0] = True

    return

label queenharpy_ng002:
    $ world.battle.face(1)

    queen_harpy "Ты довольно силён."
    "Королева Гарпий смотрит на меня удивлённым взглядом."
    queen_harpy "Однако, я не позволю тебе победить меня!"

    $ world.battle.show_skillname("Магическая концентрация")
    play sound "se/power.ogg"

    "Королева Гарпий концентрирует вокруг себя магическую энергию!"

    $ world.battle.hide_skillname()

    l "Гх!.."
    "Я улавливаю странную энергию. слабо исходящую от Королевы Гарпий."

    if world.party.player.aqua:
        extend "\nБезмятежный разум не сработает!"

    "Мне нужно что-то сделать!{w}\nНо что?.."

    $ world.battle.stage[0] = 1

    $ world.battle.main()

label queenharpy_ng_v:
    if persistent.difficulty == 3:
        $ persistent.queenhapy_hell = 1
        $ persistent.count_hellvic = True

    $ world.achivements(1)
    $ world.battle.face(3)

    if world.party.player.life == world.party.player.max_life and not world.party.player.nodamage:
        $ persistent.count_hpfull = True

    if world.party.player.status == 7:
        $ persistent.count_vickonran = True

    $ renpy.end_replay()

    queen_harpy "Ах!{w}\nОн слишком силён!.."
    "Нанеся последний удар, я заставил Королеву Гарпий пасть на землю.{w}\nНе похоже, что у неё осталось сил сопротивляться."

    $ world.battle.victory(1)

label queenharpy_ng_a:
    if not world.party.player.bind:
        $ world.battle.progress = 0

    if world.battle.result == 17 and world.party.player.skill[0] < 26 and world.battle.enemy[0].wind > 1:
        $ world.battle.enemy[0].wind_turn = 0
        $ world.battle.enemy[0].element_end()
        $ world.battle.enemy[0].wind_turn = 1

    elif world.battle.result == 17 and world.party.player.skill[0] > 25 and persistent.difficulty == 3 and world.battle.enemy[0].wind > 1:
        $ world.battle.enemy[0].wind_turn = 0
        $ world.battle.enemy[0].element_end()
        $ world.battle.enemy[0].wind_turn = 1

    if world.battle.stage[0] == 1:
        call queenharpy_ng_a12
        $ world.battle.main()

    if world.battle.enemy[0].life < world.battle.enemy[0].max_life/2 and world.battle.stage[0] < 1:
        jump queenharpy_ng002

    if world.battle.enemy[0].wind == 0:
        $ world.battle.stage[2] -= 1

    if world.battle.enemy[0].wind > 0:
        $ world.battle.stage[2] = 3

    if world.party.player.bind and world.battle.progress == 0:
        call queenharpy_ng_a6
        $ world.battle.main()

    elif world.party.player.bind and world.battle.progress == 1:
        call queenharpy_ng_a8
        $ world.battle.main()

    elif world.party.player.bind and world.battle.progress == 2:
        call queenharpy_ng_a9
        $ world.battle.main()

    elif world.party.player.bind and world.battle.progress == 3:
        call queenharpy_ng_a10
        $ world.battle.main()

    while True:
        if persistent.difficulty < 3:
            $ random = rand().randint(1, 9)

        elif persistent.difficulty == 3:
            $ random = rand().randint(1, 11)

        if random == 1 and world.battle.enemy[0].life < world.battle.enemy[0].max_life/2 and world.battle.stage[4] < 1:
            call queenharpy_ng_a5
            $ world.battle.main()

        elif random == 2:
            call queenharpy_ng_a1
            $ world.battle.main()

        elif random == 3:
            call queenharpy_ng_a2
            $ world.battle.main()

        elif random == 4:
            call queenharpy_ng_a3
            $ world.battle.main()

        elif random == 5:
            call queenharpy_ng_a4
            $ world.battle.main()

        elif persistent.difficulty < 3 and random == 6 and world.battle.stage[0] == 2:
            call queenharpy_ng_a12
            $ world.battle.main()

        elif persistent.difficulty < 3 and random > 6 and world.battle.enemy[0].wind < 1 and world.battle.enemy[0].wind_turn < 1 and world.battle.stage[2] < 1:
            call queenharpy_ng_a11
            $ world.battle.main()

        elif persistent.difficulty == 3 and random > 9 and world.battle.enemy[0].wind < 1 and world.battle.enemy[0].wind_turn < 1 and world.battle.stage[2] < 1:
            call queenharpy_ng_a11
            $ world.battle.main()

        elif persistent.difficulty == 3 and random > 5 and world.battle.stage[0] == 2:
            call queenharpy_ng_a12
            $ world.battle.main()

label queenharpy_ng_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Некоторые человеческие мужчины такие странные...",
        "Не волнуйся, я не обижу тебя...",
        "Это не больно... возможно."
    ])

    $ world.battle.show_skillname("Растирание ногой гарпии")
    play sound "audio/se/ero_koki1.ogg"

    "Королева гарий наступает на член Луки!"

    python:
        world.battle.enemy[0].damage = {1: (260, 280), 2: (300, 320), 3: (500, 550)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if world.battle.enemy[0].wind:
            world.battle.enemy[0].damage = {1: (260, 280), 2: (300, 320), 3: (500, 550)}
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label queenharpy_ng_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Некоторые человеческие мужчины такие странные...",
        "Не волнуйся, я не обижу тебя...",
        "Это не больно... возможно."
    ])

    $ world.battle.show_skillname("Минет гарпии")
    play sound "audio/se/ero_buchu3.ogg"

    "Королева гарий сосёт член Луки!"

    python:
        world.battle.enemy[0].damage = {1: (220, 240), 2: (260, 280), 3: (450, 470)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if world.battle.enemy[0].wind:
            world.battle.enemy[0].damage = {1: (220, 240), 2: (260, 280), 3: (450, 470)}
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label queenharpy_ng_a3:
    $ world.battle.skillcount(2, 3027)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ты ведь не против моих крыльев...?",
        "Насладись моими перьями...",
        "Ощути наслаждение от моих перьев..."
    ])

    $ world.battle.show_skillname("Массаж перьями")
    play sound "audio/se/umaru.ogg"

    "Королева гарий трётся своими перьями о Луку!"

    python:
        world.battle.enemy[0].damage = {1: (250, 270), 2: (290, 310), 3: (490, 560)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if world.battle.enemy[0].wind:
            world.battle.enemy[0].damage = {1: (250, 270), 2: (290, 310), 3: (490, 560)}
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label queenharpy_ng_a4:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хи-хи... Как насчёт этого?",
        "Насладись моей грудью...",
        "Хи-хи... Как насчёт этого?",
        "Просто выплести всё на мою грудь...",
        "Предайся наслаждению..."
    ])

    $ world.battle.show_skillname("Королевское пайзури")
    play sound "audio/se/ero_koki1.ogg"

    "Королева Гарпий трётся своими сиськами о член Луки!"

    python:
        world.battle.enemy[0].damage = {1: (250, 290), 2: (310, 330), 3: (520, 540)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if world.battle.enemy[0].wind:
            world.battle.enemy[0].damage = {1: (250, 290), 2: (310, 330), 3: (520, 540)}
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label queenharpy_ng_a5:
    world.battle.enemy[0].name "Может станем немного... ближе?"

    $ world.battle.show_skillname("Бросок наземь")
    play sound "audio/se/down.ogg"

    "Королева Гарпий прижимает Луку к земле!"

    if world.party.player.daystar:
        $ world.party.player.skill22x()

    $ world.party.player.bind = 1
    $ world.party.player.status_print(1)
    $ world.battle.enemy[0].power = 1

    "Королева Гарпий сидит верхом на Луке!"

    $ world.battle.hide_skillname()

    l "Гх..."
    "На этот раз она удерживает мои руки в захвате.{w}\nЯ не могу атаковать в таком положении!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Ты удостоился чести спариться с Королевой.{w}\nРазве ты не счастлив?"
    world.battle.enemy[0].name "Выплесни всю свою сперму в мою матку..."

    return

label queenharpy_ng_a6:
    $ world.battle.skillcount(2, 3029)

    $ world.battle.progress += 1

    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Время спариваться..."

    $ world.rape["queenharpy"] += 1

    $ world.battle.show_skillname("Королевское изнасилование")
    play sound "audio/se/ero_pyu5.ogg"

    show queenharpy h1
    show queenharpy ct01 as ct

    "Королева Гарпий садится на член Луки, насильно вставляя его внутрь!"

    $ world.party.player.bind = 2

    python:
        world.battle.enemy[0].damage = {1: (600, 650), 2: (600, 650), 3: (900, 950)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

    world.battle.enemy[0].name "Хи-хи... ну как тебе киска Королевы?{w}\nПохоже твоему члену это нравится..."

    return

label queenharpy_ng_a8:
    $ world.battle.skillcount(2, 3029)
    $ world.battle.progress += 1

    "Королева гарпий насилует Луку!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Вкуси удовольствия, которое может дать только киска королевы...",
        "Потрясающе, не так ли?",
        "Такой слабый мальчик как ты не сможет одолеть королеву...",
        "Выпусти всё в меня...",
        "Просто сдайся и утони в экстазе..."
    ])

    $ world.battle.show_skillname("Королевская киска")
    play sound "audio/se/ero_chupa4.ogg"

    "Королева Гарпий двигает своей талией!"

    python:
        world.battle.enemy[0].damage = (600, 650)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

    return

label queenharpy_ng_a9:
    $ world.battle.skillcount(2, 3030)
    $ world.battle.progress += 1

    "Королева гарпий насилует Луку!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Уфу-фу... сможешь ли ты пережить это?",
        "Погрузись в экстаз...",
        "Это ты не переживёшь...",
        "Уфу-фу... кончи же!",
        "Выпусти всё в меня..."
    ])

    $ world.battle.show_skillname("Королевская наездница")
    play sound "audio/se/ero_chupa3.ogg"

    $ world.battle.cry(narrator, [
        "Королева гарпий начинает резво скакать на Луке!",
        "Королева гарпий прижимается к Луке, вгоняя его член глубоко внутрь себя!",
        "Королева гарпий качает своими бёдрами!{w}\nУдовольствие начинает затмивать разум Луки!",
        "Королева гарпий медленно поднимает-опускает свои бёдра!{w}\nЕё нутро нежно щекочит член Луки!",
        "Королева гарпий начинает резво скакать на Луке!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (850, 900), 2: (950, 1050), 3: (1200, 1300)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

    return

label queenharpy_ng_a10:
    world.battle.enemy[0].name "Хм, пора бы уже заканчивать с этим."

    $ world.battle.show_skillname("Королевские объятия")
    play sound "audio/se/ero_chupa3.ogg"

    "Королева Гарпий прижимает Луку так сильно, как это вообще возможно, заставляя его кончить!"

    python:
        world.battle.enemy[0].damage = (1300, 1400)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

    return


label queenharpy_ng_a11:
    world.battle.enemy[0].name "..........................."

    $ world.battle.show_skillname("Ураган")
    play sound "audio/se/wind2.ogg"

    show effect wind zorder 30
    pause 1.0
    hide effect

    "Королева Гарпий призывает силу ветра!"

    if persistent.difficulty == 3 or world.party.player.surrendered:
        $ world.battle.enemy[0].wind = 1
        $ world.battle.enemy[0].wind_turn = -1

    else:
        $ world.battle.enemy[0].wind = 2
        $ world.battle.enemy[0].wind_turn = 6

    $ world.battle.hide_skillname()

    return

label queenharpy_ng_a12:
    $ world.battle.skillcount(2, 8500)
    $ world.battle.stage[0] = 2

    world.battle.enemy[0].name "Посмотри в мои глаза..."

    $ world.battle.show_skillname("Гипноз")
    play sound "audio/se/flash.ogg"

    show bg white with flash
    pause .3
    $ renpy.show(world.battle.background)
    $ renpy.transition(Dissolve(1.5), layer="master")

    "Глаза Королевы Гарпий вспыхивают белым светом!"

    if world.party.player.daystar:
        $ world.party.player.skill22x()

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 100, 2: 100, 3: 100}):
        $ world.party.player.wind_guard()
        return
    else:
        $ world.party.player.status = 4
        $ world.party.player.status_print()

    "Лука сдаётся Королеве Гарпий!"

    l "Ах!{w}\nЧто за?!"
    world.battle.enemy[0].name "Хи-хи...{w}\nТеперь ты подчинишься любому моему приказу.{w}\nЯ могу делать с тобой всё, что захочу."

    show queenharpy st23

    world.battle.enemy[0].name "Отныне ты моя личная игрушка!"

    $ world.battle.show_skillname("Бросок наземь")
    play sound "audio/se/down.ogg"

    "Королева Гарпий прижимает Луку к земле!"

    $ world.party.player.bind = 1
    $ world.party.player.status_print(1)

    "Королева Гарпий обвивает Луку своими мягкими перьями!"
    "После чего садиться своей промежностью на его рот."

    $ world.battle.hide_skillname()

    world.battle.enemy[0].name "А теперь... лижи."
    l "Хах..."
    "Несмотря на то, что мой разум пытается сопротивляться, тело покорно выполняет её команду."

    $ world.battle.show_skillname("Королевская езда на лице")
    play sound "audio/se/ero_chupa4.ogg"

    "Язык Луки проскальзывает сквозь перья Королевы Гарпий и входит в её киску!"

    python:
        world.battle.enemy[0].damage = {1: (450, 500), 2: (450, 500), 3: (450, 500)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

    l "Мммпхм..."
    world.battle.enemy[0].name "Продолжай..."

    $ world.battle.show_skillname("Королевская езда на лице")
    play sound "audio/se/ero_chupa5.ogg"

    "Язык Луки лижет грубые стенки влагалища Королевы!"

    python:
        world.battle.enemy[0].damage = {1: (450, 500), 2: (450, 500), 3: (450, 500)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

    world.battle.enemy[0].name "Ара, дай мне тебе помочь..."

    $ world.battle.show_skillname("Королевская езда на лице")
    play sound "audio/se/ero_pyu1.ogg"

    "Королева Гарпий начинает тереться своей промежностью о рот Луки!{w}\nПосле чего прижимает своими крыльями голову Луки к себе!"

    python:
        world.battle.enemy[0].damage = {1: (450, 500), 2: (450, 500), 3: (450, 500)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

    world.battle.enemy[0].name "Глубже..."

    $ world.battle.show_skillname("Королевская езда на лице")
    play sound "audio/se/ero_chupa4.ogg"

    "Королева опускает бёдра ещё сильнее из-за чего язык Луки походит глубже в неё!{w}\nОн начинает лизать её клитор!"

    python:
        world.battle.enemy[0].damage = {1: (450, 500), 2: (450, 500), 3: (450, 500)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

    "Жидкость из киски Королевы Гарпий начала стекать Луке в рот!"

    play sound "audio/se/ero_chupa4.ogg"

    python:
        world.battle.enemy[0].damage = {1: (150, 200), 2: (150, 200), 3: (150, 200)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

    world.battle.enemy[0].name "Ммм...{w}\nПришло время закончить это..."

    $ world.battle.show_skillname("Королевская езда на лице")
    play sound "audio/se/ero_chupa4.ogg"

    "Королева Гарпий прижимает лицо Луки так сильно, насколько это вообще возможно!"

    python:
        world.battle.enemy[0].damage = {1: (850, 900), 2: (850, 900), 3: (850, 900)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

    "Ещё больше смазки начинает стекать в рот Луки!"

    python:
        world.battle.enemy[0].damage = {1: (350, 400), 2: (350, 400), 3: (350, 400)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

        world.battle.hide_skillname()

    jump badend_h

label queenharpy_ng_chargecancel:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Оу нет, не выйдет!"

    $ world.battle.counter()
    $ world.battle.show_skillname("Отмена разгона")

    "Королева Гарпий резко бросается к Луке и начинает ласкать его!"

    $ world.battle.enemy[0].damage = {1: (250, 300), 2: (350, 400), 3: (450, 500)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

    "Лука теряет концентрацию!"

    $ world.battle.hide_skillname()
    $ world.battle.face(1)

    if world.battle.stage[1] < 1:
        $ world.battle.stage[1] = 1

        l "Ах!{w}\nЧто...?"
        world.battle.enemy[0].name "Ты действительно думаешь, что я настолько глупа?{w}\nТы слишком медленный, чтобы проворачивать такие трюки со мной, мальчик."

    $ world.battle.enemy[0].attack()

label queenharpy_ng_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Значит признаёшь своё неизбежное поражение.{w}\nПозволь мне наградить тебя за твою смелость..."

    $ world.battle.enemy[0].attack()

label rahure_start:
    python:
        world.troops[10] = Troop(world)
        world.troops[10].enemy[0] = Enemy(world)

        world.troops[10].enemy[0].name = Character("Девушка-раффлезия")
        world.troops[10].enemy[0].sprite = ["rahure st01", "rahure st02", "rahure st03", "rahure st11"]
        world.troops[10].enemy[0].position = center
        world.troops[10].enemy[0].defense = 100
        world.troops[10].enemy[0].evasion = 99
        world.troops[10].enemy[0].max_life = world.troops[10].battle.difficulty(200, 250, 300)
        world.troops[10].enemy[0].power = world.troops[10].battle.difficulty(0, 1, 2)

        world.troops[10].battle.tag = "rahure"
        world.troops[10].battle.name = world.troops[10].enemy[0].name
        world.troops[10].battle.background = "bg 030"
        world.troops[10].battle.music = 1
        world.troops[10].battle.exp = 220
        world.troops[10].battle.exit = "lb_0025"

        world.troops[10].battle.init()

    $ world.battle.face(2)

    world.battle.enemy[0].name "Прекрасно... Настало моё время быть опылённой...{w}\nЭй... Ты не спаришься со мной?"
    world.battle.enemy[0].name "Покрой весь мой пестик своим семенем..."
    l "... Нет, я так не думаю."
    world.battle.enemy[0].name "Это будет нетрудно...{w}\nПросто оставь всё мне."
    world.battle.enemy[0].name "Я сделаю тебе приятно, и всё, что от тебя потребуется — только кончать.{w}\nЯ исполню все твои самые заветные желания... Разве ты не счастливчик?"
    l "....................."
    "Эту штуку нужно стереть в порошок."

    $ world.battle.face(1)
    $ world.battle.main()

label rahure_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind == 2:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label rahure_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Нет... Моё тело..."

    play sound "se/syometu.ogg"
    hide rahure with crash

    "Девушка-раффлезия уменьшилась и превратилась в обычную Раффлезию!"
    "Не похоже, что теперь, став обычным растением, она сможет передвигаться."
    "В такой форме её не отличишь от других растений, растущих поблизости."

    $ world.battle.victory(1)

label rahure_a:
    if world.party.player.bind == 1:
        call rahure_a4
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,3)

        if random == 1:
            call rahure_a1
            $ world.battle.main()
        elif random == 2:
            call rahure_a2
            $ world.battle.main()
        elif random == 3 and world.battle.delay[0] > 2:
            call rahure_a3
            $ world.battle.main()


label rahure_a1:
    $ world.battle.skillcount(1, 3042)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хе-хе-хе...",
        "Готовлюсь к опылению...",
        "Я возбужу тебя..."
    ])

    $ world.battle.show_skillname("Ласки щупальцем")
    play sound "audio/se/ero_makituki2.ogg"

    "Девушка-раффлезия ласкает Луку одним из своих щупалец!"

    python:
        world.battle.enemy[0].damage = {1: (12, 14), 2: (18, 21), 3: (24, 28)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label rahure_a2:
    $ world.battle.skillcount(1, 3043)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Как насчёт этого?..",
        "Это лишь прелюдия...",
        "Мои лепестки тоже потрясающи..."
    ])

    $ world.battle.show_skillname("Щекотка лепестками")
    play sound "audio/se/ero_name1.ogg"

    "Мясистые лепестки поглаживают нижнюю часть тела Луки!{w}\nЕго член массируют лепестки девушки-раффлезии!"

    python:
        world.battle.enemy[0].damage = {1: (13, 15), 2: (19, 22), 3: (26, 30)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label rahure_a3:
    if world.party.player.surrendered or not world.battle.first[0]:
        world.battle.enemy[0].name "Время опыления...{w}\nДля начала я сделаю так, чтобы ты перестал сопротивляться..."
    elif not world.party.player.surrendered and world.battle.first[0]:
        world.battle.enemy[0].name "Ещё одна попытка...{w}\nНа этот раз тебе не убежать..."

    $ world.battle.show_skillname("Опьянающий аромат")
    play sound "audio/se/gas.ogg"

    "Сладкий запах исходит от девушки-раффлезии!{w}{nw}"

    $ world.party.player.status = 1
    $ world.party.player.status_print()

    extend "{w=.7}\nЛука очарован!"

    $ world.battle.hide_skillname()
    $ world.battle.face(2)

    if world.battle.first[0]:
        world.battle.enemy[0].name "Back inside..."
    else:
        world.battle.enemy[0].name "Now... I'll start...{w}\nI'll make you feel really good, so pollinate me..."

    $ world.battle.show_skillname("Захват")
    play sound "audio/se/ero_makituki2.ogg"

    "Щупальца девушки-раффлезии хватают опутывают тело Луки!{w}\nЛуку затягивает в лоно девушки-раффлезии!"

    $ world.battle.enemy[0].deal((12, 14))
    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Тело Луки застряло в цветоложе девушки-раффлезии!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    if not world.battle.first[0]:
        l "Faa..."
        "The inside of the Rafflesia Girl was filled with a sticky mucus.{w}\nBathed in the slimy warmth, I feel my power slowly draining."

    "Luka is entranced!"

    if world.battle.first[0]:
        world.battle.enemy[0].name "Next, I'll cover you with pollen..."
    else:
        world.battle.enemy[0].name "First, let me cover you with pollen..."

    $ world.battle.skillcount(1, 3044)
    $ world.battle.show_skillname("Stamen")
    show rahure ct01 at topright zorder 15 as ct
    play sound "audio/se/ero_makituki3.ogg"

    "The innumerable stamens cover Luka's penis!{w}\nThey cover Luka with pollen!"

    $ world.battle.enemy[0].damage = (12, 14)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h5")

    "Luka is entranced!"

    if not world.battle.first[0]:
        world.battle.enemy[0].name "Ejaculate in my pistil like this...{w}\nBlending your semen with my pollen, you can pollinate me..."
        world.battle.enemy[0].name "Come on...{w}\nShoot your semen out..."
    else:
        world.battle.enemy[0].name "The preparations are complete... Now, shoot it all out..."

    $ world.battle.skillcount(1, 3044)
    $ world.battle.show_skillname("Pistil")
    show rahure ct11 at topright zorder 15 as ct
    play sound "audio/se/ero_makituki3.ogg"

    "The pistil expands and wraps around Luka's penis!{w}\nWith it's slimy surface, it rubs against Luka!"

    $ world.battle.enemy[0].damage = (12, 14)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h4")

    if world.party.player.surrendered:
        return

    l "Уууу..."

    if world.battle.first[0]:
        world.battle.enemy[0].name "Разве это не потрясающе?{w}\nНа этот раз тебе не уйти..."
    else:
        world.battle.enemy[0].name "Уфу-фу... просто выплесни всё на меня.{w}\nОпыли меня своим семенем..."

    if persistent.difficulty < 2:
        $ world.party.player.status_turn = 0
    elif persistent.difficulty == 2:
        $ world.party.player.status_turn = 0
    elif persistent.difficulty == 3:
        $ world.party.player.status_turn = rand().randint(1,2)

    $ world.battle.first[0] = True

    if persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 0
    elif persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 1
    elif persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 2

    return

label rahure_a4:
    $ world.battle.skillcount(1, 3044)

    "Лука целиком поглощён цветоложем девушки-раффлезии!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Мой пестик потрясающий на ощупь...",
        "Выплесни всё в него...",
        "Я буду продолжать, пока ты не кончишь...",
        "Я поиграю с твоим слабым местом...",
        "Ты ощутишь наслаждение от моего пестика..."
    ])

    $ world.battle.show_skillname("Пестик")
    play sound "audio/se/ero_makituki3.ogg"

    $ world.battle.cry(narrator, [
        "Пестик стимулирует Луку!",
        "Пестик девушки-раффлезии входит в уретру Луки!",
        "Пестик опутывает член Луки!",
        "Пестик мнёт мошонку Луки!",
        "Пестик опутывает член Луки и сжмается!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (12, 14), 2: (18, 21), 3: (24, 28)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label rahure_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Уфу-фу... так ты хочешь опылить меня?{w}\nВ таком случае... извергни много семени!"

    $ world.battle.enemy[0].attack()

label rahure_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Tentacle Caress"

    if persistent.skills[3042][0] > 0:
        $ list1_unlock = 1
    $ list2 = "Petal Massage"

    if persistent.skills[3043][0] > 0:
        $ list2_unlock = 1
    $ list3 = "Forced Pollination"

    if persistent.skills[3044][0] > 0:
        $ list3_unlock = 1
    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump rahure_onedari1
    elif world.battle.result == 2:
        jump rahure_onedari2
    elif world.battle.result == 3 and world.party.player.bind == 0:
        jump rahure_onedari3
    elif world.battle.result == 3 and world.party.player.bind == 1:
        jump rahure_onedari4
    elif world.battle.result == -1:
        jump rahure_main
    jump rahure_onedari


label rahure_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want to be caressed by my tentacles? You're strange...{w}\nAfter that, I'll be sure you properly pollinate me..."

    while True:
        call rahure_a1


label rahure_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want to be massaged by my petals? Alright...{w}\nAfter that, I'll be sure you properly pollinate me..."

    while True:
        call rahure_a2


label rahure_onedari3:
    call onedari_syori

    world.battle.enemy[0].name "Hahaha, so you do want to pollinate me after all?{w}\nThen, let's mate without holding anything back..."

    call rahure_a3

    while True:
        "Luka is entranced!"

        call rahure_a4


label rahure_onedari4:
    call onedari_syori_k

    world.battle.enemy[0].name "So you want to come Нравится?{w}\nThen, make sure you let out a lot..."

    while True:
        call rahure_a4


label rahure_h1:
    $ world.party.player.moans(1)
    with syasei1
    show rahure bk05 zorder 10 as bk
    $ world.ejaculation()

    "Massaged by the Rafflesia Girl's tentacles, Luka explodes all over them."

    $ world.battle.lose()

    $ world.battle.count([10, 1])
    $ bad1 = "Luka succumbs to the Rafflesia Girl's tentacle caress."
    $ bad2 = "After pollinating the Rafflesia Girl, Luka was kept as a breeding slave."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Ahh... You wasted so much.{w}\nThis time, properly pollinate me."

    jump rahure_h


label rahure_h2:
    $ world.party.player.moans(1)
    with syasei1
    show rahure bk06 zorder 10 as bk
    $ world.ejaculation()

    "Massaged by the Rafflesia Girl's fleshy petals, Luka explodes all over them."

    $ world.battle.lose()

    $ world.battle.count([12, 1])
    $ bad1 = "Luka succumbs to the Rafflesia Girl's petals."
    $ bad2 = "After pollinating the Rafflesia Girl, Luka was kept as a breeding slave."

    world.battle.enemy[0].name "Ahh... You wasted so much.{w}\nThis time, properly pollinate me."

    jump rahure_h


label rahure_h3:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Hey! Wait!"

    with syasei1
    show rahure bk07 zorder 10 as bk
    show rahure st12
    $ world.ejaculation()

    "As soon as Luka is sucked into the Raffles Girl's mouth, Luka ejaculates."

    $ world.battle.lose()

    $ world.battle.count([12, 1])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Held by the Rafflesia Girl, Luka ejaculates on accident."
    $ bad2 = "After pollinating the Rafflesia Girl, Luka was kept as a breeding slave."

    world.battle.enemy[0].name "Arara... I didn't finish the preparations.{w}\nI can't be pollinated like this..."
    world.battle.enemy[0].name "We'll have to do it again.{w}\nThis time, properly pollinate me."

    jump rahure_h


label rahure_h4:
    $ world.party.player.moans(2)
    with syasei1
    show rahure ct12 at topright zorder 15 as ct
    show rahure bk07 zorder 10 as bk
    show rahure st12
    $ world.ejaculation()

    "Stimulated by the Rafflesia Girl, Luka ejaculates all over her pistil."

    $ world.battle.lose()

    $ world.battle.count([12, 1])
    $ bad1 = "Luka succumbs to the Rafflesia Girl's pistil, pollinating her."

    if world.party.player.status == 1:
        $ bad1 = "While entranced, Luka succumbs to the Rafflesia Girl's pistil, pollinating her."
    $ bad2 = "After pollinating the Rafflesia Girl, Luka was kept as a breeding slave."

    world.battle.enemy[0].name "Hahaha, you let out so much on my pistil!{w}\nI'm glad you can put out so much!{image=note}"
    world.battle.enemy[0].name "Hey, don't you want to pollinate me even more?{w}\nJust look at you drooling so much after only doing it once{image=note}."
    l "That..."
    world.battle.enemy[0].name "Let's do it even more{image=note}."

    jump rahure_h


label rahure_h5:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Wait! I'm still covering it with pollen!"

    with syasei1
    show rahure ct02 at topright zorder 15 as ct
    show rahure bk07 zorder 10 as bk
    show rahure st12
    $ world.ejaculation()

    "Unable to endure the stimulation brought by her stamen, Luka ejaculates."

    $ world.battle.lose()

    $ world.battle.count([12, 1])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Not yet covered with pollen, Luka ejaculates on accident."
    $ bad2 = "After pollinating the Rafflesia Girl, Luka was kept as a breeding slave."

    world.battle.enemy[0].name "Arara... I didn't finish the preparations.{w}\nI can't be pollinated like this..."
    world.battle.enemy[0].name "We'll have to do it again.{w}\nThis time, properly pollinate me."

    jump rahure_h


label rahure_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.party.player.bind != 1:
        play sound "audio/se/ero_makituki2.ogg"

        "The Rafflesia Girl's tentacle grabs my body!"
        l "Ahhh!"

        hide ct
        hide bk
        show rahure st11


        "Wrapped in her tentacle, my body is forced into her flower mouth!"
        "The flower was filled with sticky mucus.{w}\nMy lower body covered in the warm mucus, I feel my strength drain."
        world.battle.enemy[0].name "Now, let me sprinkle some pollen..."

    play hseanwave "audio/se/hsean21_tentacle2.ogg"
    show rahure ct01 at topright zorder 15 as ct

    "Something like a tentacle starts to coil around my penis.{w}\nFull of pollen, numerous stamens start to rub me."
    l "Ahhh!"
    world.battle.enemy[0].name "First, let me cover you with my pollen{image=note}."
    "The stamen surround my penis, spreading pollen all over me.{w}\nThey spread pollen all over my body, but pay special attention to the tip of my penis."
    "The sticky pollen covers my urethral opening, as the massage brings me close to the edge."
    l "Auu... That's so good..."
    world.battle.enemy[0].name "I haven't finished preparing it yet, but you're already feeling good?{w}\nIf you feel this good with just my stamen, wait until you feel the pistil..."
    "The stimulation fills me with both pleasure and disgust, as she continues to rub pollen on me."
    l "Auuu... Not the stamen..."
    world.battle.enemy[0].name "Hahaha, even if you say stamen, it isn't a male organ.{w}\nI'm obviously a female, so relax."
    l "Auuu..."
    "Helplessly writhing, she finishes rubbing me with pollen.{w}\nUnable to resist, my only choice is to try to endure the rising orgasm."
    world.battle.enemy[0].name "...That's it for the preparations.{w}\nNow, I'll use my pistil... make sure you cover it fully."

    play hseanwave "audio/se/hsean01_innerworks_a1.ogg"
    show rahure ct11 at topright zorder 15 as ct
    show rahure st11

    "Her slimy pistil starts to slowly wrap around my penis, as it stretches until it's all the way around."
    l "Ahhh!"

    play hseanwave "audio/se/hsean10_innerworks_b2.ogg"

    "My penis, covered in her sticky pollen, causes the pistil to stick to its surface."
    world.battle.enemy[0].name "When you shoot our your seed on my pistil, it will mix with the pollen.{w}\nThat's how a human male can fertilize me."
    world.battle.enemy[0].name "So just let it out...{w}\nHere, I'll make you feel even better..."
    l "Ahiii!"
    "The pistil coiled around my dick starts to move up and down, rubbing me with it's slimy membrane."
    "My body writhes helplessly as I'm attacked by the soft slimy pleasure."
    world.battle.enemy[0].name "This is how you mate with a Rafflesia...{w}\nYou're having sex with a plant..."
    world.battle.enemy[0].name "Hey, how do you like it?"
    l "This... Ahhhh!"
    "This is completely different from being with a human...{w}\nThe immorality of this pistil making me feel so good brings about a strange excitement in me."
    world.battle.enemy[0].name "You're feeling good, aren't you?"
    world.battle.enemy[0].name "Sex with a plant... Much better than you thought, right?"
    world.battle.enemy[0].name "Just come with your human penis like that..."
    "The pistil tightens around me, as if forcing me to ejaculate.{w}\nThe tip of her pistil starts to rub my tip, as I twist helplessly."
    l "Auu... I'm going to come..."
    "If I can't endure, I'm going to inseminate this plant...{w}\nFertilizing a monster plant...{w}\nI have to avoid that at any cost..."
    l "No... No...{w}\nThis... No..."
    world.battle.enemy[0].name "It's useless to try to endure.{w}\nLike this, you have no choice but to pollinate me{image=note}."
    "The Rafflesia Girl laughs at me as I try to endure."
    "Her pistil starts to move up and down as it's wrapped around me, crushing my resistance."
    world.battle.enemy[0].name "Come!{image=note}{w}\nPour your semen all over my pistil!{image=note}"
    "I'm unable to endure her intense stimulation any longer...{w}\nI know it's forbidden, but I can't hold on any more..."
    l "Aaaaah!"

    with syasei1
    show rahure ct12 at topright zorder 15 as ct
    show rahure st12
    $ world.ejaculation()

    "I explode, shooting my cloudy seed all over the pistil of the Rafflesia Girl."
    l "No... I came..."
    "I can't even begin to understand the act that I've just done.{w}\nUnable to stop the pleasure, I just pollinated this plant..."
    "Breaking the worst taboo for a human...{w}\nSoaked in the mucus of this plant, all of my power to escape vanishes."
    world.battle.enemy[0].name "Fufu... With that, I've been successfully fertilized."
    world.battle.enemy[0].name "And your genes seem to be pretty good, too...{w}\nLetting someone as good as you go would be foolish..."
    l "Eh...?"
    world.battle.enemy[0].name "I'll keep you just like this.{w}\nEvery breeding season, I'll force you to pollinate me like this."
    l "No..."
    "I struggle fruitlessly, trying to escape.{w}\nI don't want to be stuck as her reproductive slave!"
    world.battle.enemy[0].name "Hahaha, don't worry.{w}\nI'll be sure to feed you and take care of you properly."
    l "I don't want that!{w} Guh..."
    "A tentacle is forced into my mouth, and a sweet liquid is forced down my throat."
    "Wrapped tightly in her flower mouth, I'm unable to escape."
    l "Ghhmff... Mmff..."
    "The next instant, the pistil starts to violently stimulate me again, bringing me to another height."

    with syasei1
    show rahure ct13 at topright zorder 15 as ct
    show rahure st13
    $ world.ejaculation()

    l "Mmmfff..."
    "Her tentacle in my mouth, I pitifully ejaculate again onto her pistil."
    world.battle.enemy[0].name "Haha, are you happy?{w}\nEvery breeding season, I'll make you this happy."
    world.battle.enemy[0].name "To make sure you're always this energetic, I'll also let you ejaculate a couple times each day."
    world.battle.enemy[0].name "I'll take good care of you!"
    l "Mmmfff..."
    "The pistil continues to massage me as I'm unable to fight back."

    with syasei1
    show rahure ct14 at topright zorder 15 as ct
    show rahure st14
    $ world.ejaculation()

    "Struggling pointlessly, the last of my strength fades.{w}\nFrom now on, I'm kept only as her breeding slave."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "After that, I was never released.{w}\nForce fed like I was completely disabled, my only purpose is to breed with the Rafflesia Girl."
    "My adventure ends in a cruel and disgraceful way..."
    "........."

    jump badend

label ropa_start:
    python:
        world.troops[11] = Troop(world)
        world.troops[11].enemy[0] = Enemy(world)

        world.troops[11].enemy[0].name = Character("Ловчая")
        world.troops[11].enemy[0].sprite = ["ropa st01", "ropa st02", "ropa st03", "ropa h2"]
        world.troops[11].enemy[0].position = center
        world.troops[11].enemy[0].defense = 100
        world.troops[11].enemy[0].evasion = 95
        world.troops[11].enemy[0].max_life = world.troops[11].battle.difficulty(220, 270, 320)
        world.troops[11].enemy[0].power = world.troops[11].battle.difficulty(0, 1, 2)

        world.troops[11].battle.tag = "ropa"
        world.troops[11].battle.name = world.troops[11].enemy[0].name
        world.troops[11].battle.background = "bg 030"
        world.troops[11].battle.music = 1
        world.troops[11].battle.exp = 250
        world.troops[11].battle.exit = "lb_0026"

        world.troops[11].battle.init()

    l "Ловчая, хах..."
    "Она — весьма известный и доставляющий много проблем монстр."
    "Она ослабляет свою добычу сдавливанием или заставляя её кончать, затем растворяет добычу и пожирает её."
    "Действительно жуткий монстр."

    $ world.battle.face(2)

    world.battle.enemy[0].name "Прошло уже столько времени с тех пор, как я хорошо кушала в последний раз..."
    l "..................."
    "Даже если Алиса думает, что я просто задираю монстров, я не могу позволить этому нечто безнаказанно уйти!"

    $ world.battle.face(1)

    if not persistent.skills[3048][0]:
        $ world.battle.skillcount(1, 3048)

    $ world.battle.main()

label ropa_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind == 2:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label ropa_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Что... это?.."

    play sound "se/syometu.ogg"
    hide ropa with crash

    "Ловчая превращается в крошечную щупальцеподобную тварь!"
    "Ловчая, теперь настолько маленькая, что может поместиться в моей ладони, гневно взмахнула своими щупальцами перед тем, как сбежать!"

    $ world.battle.victory(1)

label ropa_a:
    if world.party.player.bind == 1:
        $ random = rand().randint(1,2)

        if random == 1:
            call ropa_a3
            $ world.battle.main()
        elif random == 2:
            call ropa_a4
            $ world.battle.main()

    while True:
        $ random = rand().randint(1,2)

        if random == 1:
            call ropa_a1
            $ world.battle.main()
        elif random == 2 and world.battle.delay[0] > 3:
            call ropa_a2
            $ world.battle.main()


label ropa_a1:
    $ world.battle.skillcount(1, 3045)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Сюда...",
        "Я поиграю с тобой...",
        "Я ослаблю тебя..."
    ])

    $ world.battle.show_skillname("Ласки щупальцем")
    play sound "audio/se/ero_makituki2.ogg"

    "Ловчая вытягивает одно из своих склизких щупалец и массирует им тело Луки!"

    python:
        world.battle.enemy[0].damage = {1: (9, 11), 2: (13, 16), 3: (18, 22)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label ropa_a2:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Хе-хе-хе..."

    $ world.battle.show_skillname("Опутывание щупальцами")
    play sound "audio/se/ero_makituki3.ogg"

    "Ловчая опутывает свои щупальцами тело Луки!"

    $ world.battle.enemy[0].damage = (13, 15)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Лука схвачен Ловчей!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h2")

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    if world.battle.first[0]:
        world.battle.enemy[0].name "На этот раз тебе не уйти...{w}{nw}"
    else:
        world.battle.enemy[0].name "Поймала...{w}{nw}"

    extend "\nI'll weaken you just like this..."

    $ world.battle.first[0] = True

    if persistent.difficulty == 1:
        $ world.battle.enemy[0].power = 2
    elif persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 2
    elif persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return

label ropa_a3:
    $ world.battle.skillcount(1, 3046)

    "Тело Луки опутано щупальцами Ловчей!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я поиграю с твоим слабым местом...",
        "Вот как можно ослабить мужчину...",
        "Подразнить мужской пенис...",
        "Подразнить головку..."
        "Свались от агонии..."
    ])

    $ world.battle.show_skillname("Ласка щупальцами в захвате")
    play sound "audio/se/ero_makituki2.ogg"

    $ world.battle.cry(narrator, [
        "Щупальца Ловчей обвивается вокруг члена Луки!",
        "Склизкое щупальце Ловчей щекочет член Луки!",
        "Щупальца Ловчей скользит по члену Луки!",
        "Щупальца Ловчей сдавливает головку члена Луки!"
        "Щупальца Ловчей проникает внутрь уретры Луки!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (19, 22), 2: (28, 33), 3: (38, 44)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label ropa_a4:
    $ world.battle.skillcount(1, 3047)

    "Тело Луки опутано щупальцами Ловчей!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я сломаю тебя...",
        "Позволь мне услышать твои истошные крики...",
        "Ты это не переживёшь...",
        "Сначала я ослаблю тебя этим... а затем сожру...",
        "Я сожму их..."
    ])

    $ world.battle.show_skillname("Удушение")
    play sound "audio/se/ero_simetuke1.ogg"

    $ world.battle.cry(narrator, [
        "Щупальца Ловчей стягивают тело Луки до предела!",
        "Ловчая сжимает тело Луки!",
        "Ловчая сжимает тело Луки!",
        "Ловчая сжимает тело Луки, вызывая невыносимую боль!",
        "Щупальца Ловчей сжимают тело Луки!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (16, 18), 2: (24, 27), 3: (32, 36)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label ropa_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"
    world.battle.enemy[0].name "Не сопротивляешься?..{w}\nТогда позволь тебя съесть..."

    $ world.battle.enemy[0].attack()

label ropa_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Tentacle Caress"
    $ list2 = "Bound Tentacle Caress"
    $ list3 = "Bound Tentacle Tightening"
    $ list4 = "Predation"

    if persistent.skills[3045][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3046][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3047][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3048][0] > 0:
        $ list4_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump ropa_onedari1
    elif world.battle.result == 2:
        jump ropa_onedari2
    elif world.battle.result == 3:
        jump ropa_onedari3
    elif world.battle.result == 4:
        jump ropa_onedari4
    elif world.battle.result == -1:
        jump ropa_main


label ropa_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want me to weaken you...?"

    while True:
        call ropa_a1


label ropa_onedari2:
    call onedari_syori_k

    if world.party.player.bind != 1:
        world.battle.enemy[0].name "You want my tentacles to weaken you...?"

        call ropa_a2

    world.battle.enemy[0].name "You want me to weaken you like this...?{w}\nAfter I weaken you, I'll eat..."

    while True:
        call ropa_a5


label ropa_onedari3:
    call onedari_syori_k

    if world.party.player.bind != 1:
        world.battle.enemy[0].name "You want me to torture you with my tentacles?"

        call ropa_a2

    world.battle.enemy[0].name "You want to be squeezed like this, right?{w}\nAfter I weaken you, I'll eat..."

    while True:
        call ropa_a6


label ropa_onedari4:
    call onedari_syori_k
    $ world.battle.skillcount(1, 3048)

    world.battle.enemy[0].name "You want to be dissolved inside of me and eaten...?{w}\nThen..."

    if not world.party.player.bind:
        call ropa_a2

    "Luka's body is pulled towards the Roper!"

    $ world.battle.lose(1)

    $ world.battle.count([33, 11])
    $ bad1 = "Luka offered his body to the Roper, and allowed her to eat him."
    $ bad2 = ""
    $ kousan = 4
    jump ropa_h


label ropa_h1:
    $ world.party.player.moans(1)
    with syasei1
    show ropa bk05 zorder 10 as bk
    $ world.ejaculation()

    "Tentacles playing with his body, Luka ejaculates all over them."

    $ world.battle.lose()

    $ world.battle.count([10, 11])
    $ bad1 = "Luka succumbs to the Roper's tentacles playing with him."
    $ bad2 = "Melted by the Roper, Luka is eaten."

    world.battle.enemy[0].name "You've been weakened enough... It's time now..."
    l "Guh... Stop..."
    "Hundreds of tentacles shoot out and pull me toward the Roper!"

    jump ropa_h


label ropa_h2:
    $ world.party.player.moans(2)
    with syasei1
    show ropa h2
    $ world.ejaculation()

    "Tentacles playing with his body while bound, Luka ejaculates all over them."

    $ world.battle.lose()

    $ world.battle.count([10, 11])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka succumbs to the Roper's tentacles playing with him as he is bound."
    $ bad2 = "Melted by the Roper, Luka is eaten."

    world.battle.enemy[0].name "You already let it out...?{w}\nNext is for me to melt you..."

    jump ropa_h


label ropa_h3:
    $ world.party.player.moans(1)
    with syasei1
    show ropa h2
    $ world.ejaculation()

    "Tentacles playing with his body while bound, Luka ejaculates all over them."

    $ world.battle.lose()

    $ world.battle.count([10, 11])
    $ bad1 = "Luka succumbs to the Roper's tentacles playing with him as he is bound."
    $ bad2 = "Melted by the Roper, Luka is eaten."

    world.battle.enemy[0].name "You're weak enough now...{w}\nWith my digestive juices, I'll slowly melt you..."

    jump ropa_h


label ropa_h4:
    l "Guh! Uuuu..."
    "As my entire body is strangled by her tentacles, the tentacles wrapped around my penis stimulate me."

    with syasei1
    show ropa h2
    $ world.ejaculation()

    "While feeling the pain from her strangling, I ejaculate on her."

    $ world.battle.lose()

    $ world.battle.count([10, 11])
    $ bad1 = "Bound and strangled by the Roper, Luka ejaculates."
    $ bad2 = "Melted by the Roper, Luka is eaten."

    world.battle.enemy[0].name "You're weak enough now...{w}\nWith my digestive juices, I'll slowly melt you..."

    jump ropa_h


label ropa_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if not world.party.player.bind > 0:
        l "Aaaaa!"

        play sound "audio/se/ero_makituki3.ogg"
        hide ct
        hide bk
        show ropa h1


        "Pulled by her tentacles, I'm forced into her body.{w}\nHer soft body is covered in a sticky mucus."

        if persistent.vore == 1:
            jump vore

        "The mucus starts to cover my body...{w}\nThis fluid can't be..."
        world.battle.enemy[0].name "With my digestive juices, I'll slowly melt you..."

    if kousan < 4:
        l "No! Stop it!"
    elif kousan == 4:
        l "Aaaaa"

    if kousan != 4:
        "While screaming, I struggle violently.{w}\nBut her tentacles wrap tighter around me, stopping my movement."

    "My waist is forced into the Roper.{w}\nThere, a hole opens its mouth as if inviting me inside."

    if kousan < 4:
        l "Hii! What are you doing?!"
        "Unable to resist, she forces me into the hole."
    elif kousan == 4:
        l "Auuu... What's that...?"
        "She lets me stick my penis into it."

    play hseanwave "audio/se/hsean12_innerworks_c2.ogg"
    show ropa ct01 at topright zorder 15 as ct

    "My penis gets stuck inside of her!"
    l "Aaahh... What is this..."
    "Countless tentacles are wriggling inside of her, rubbing my penis.{w}\nIt's as if I was inserted into a sea of wriggling tentacles."
    l "Faaa... It feels good..."
    "The pleasure overpowers me, causing me to feel even weaker."
    world.battle.enemy[0].name "Making you weaker with pleasure...{w}\nWhile doing so, I'll melt you..."

    if kousan < 4:
        l "N...No..."
    elif kousan == 4:
        l "A...Amazing..."

    "The pleasure drains me of any strength I had left in me.{w}\nThe wriggling tentacles inside of her stimulate me without mercy."
    "The pleasure too intense, my mind goes blank as the orgasm rises in me."
    l "Aaa... Coming!{w} Ahhh!"

    with syasei1
    show ropa ct02 at topright zorder 15 as ct
    show ropa h2
    $ world.ejaculation()

    "As I explode inside of her wriggling hole, the digestive juices completely cover my body."
    "Instead of pain, the horrible liquid is covering my body with a numbing pleasure."

    if kousan != 4:
        "Terror fills me.{w}\nHow can I feel such sweet pleasure as my body is dissolved..."
        l "No!{w} Let go!{w} Help me... Someone..."

    world.battle.enemy[0].name "Melt while you feel the pleasure..."

    play hseanwave "audio/se/hsean13_innerworks_c3.ogg"

    "The wriggling tentacles in her hole continue to force pleasure through me.{w}\nThis pleasure is not for reproduction, but only to restrain her prey."
    "Weakening her prey, then melting them so she can eat..."

    if kousan != 4:
        l "No...{w} Help me..."

    if kousan < 4:
        world.battle.enemy[0].name "I won't let go... Just melt away like this..."
    elif kousan == 4:
        world.battle.enemy[0].name "Just melt into mush like this..."

    "Her tentacles wrapped around me start to rub more of the digestive juices on me."
    "At the same time, the hole where I'm inserted in her starts to fill up with the juices."

    if kousan < 4:
        l "Auu... No..."
    elif kousan == 4:
        l "Auu... So good..."

    "Covered in her digestive juices, pleasure explodes in my dick."

    "Forcing her prey to stay because of the pleasure...{w}\nI'm unable to resist it..."
    l "Uaa... Coming again..."

    with syasei1
    show ropa ct03 at topright zorder 15 as ct
    show ropa h3
    $ world.ejaculation()

    "Unable to resist the stimulation, I come in her hole again.{w}\nThe only purpose to stimulate me is to weaken me..."
    world.battle.enemy[0].name "It feels good, right?{w}\nYour body is almost melted..."
    l "Aaaa...."
    "Covered in her sticky digestive juices, I feel despair rise through me."
    world.battle.enemy[0].name "Turn into mush...{w}\nYou'll be nourishment for me... Be happy..."
    l "Aaaa...."
    "As I feel my body melt away, so does the fear and despair.{w}\nFilling their place is a slow joy."
    "Slowly melted, I start to feel happy at feeding the Roper.{w}\nAt this rate, I'll be melted and eaten like an insect caught by a carnivorous plant..."
    l "Faa... It feels good..."
    "Covered by the digestive juices, the tentacles continue to rub more of the juice all over me.{w}\nThe forced stimulation is too much for me to endure."
    l "Ahiii!"

    with syasei1
    show ropa ct04 at topright zorder 15 as ct
    show ropa h4
    $ world.ejaculation()

    "As I feel myself slowly dissolving, I ejaculate inside of her hole again."
    "Completely hooked on the pleasure, the act of ejaculating in her feels incredible."
    l "Faa... Amazing..."
    world.battle.enemy[0].name "I'll melt your penis in this hole, too...{w}\nWhile it's stuck inside me... Fufu..."
    l "Aaaa...."
    "Gradually, my consciousness starts to melt along with my body."
    world.battle.enemy[0].name "You aren't afraid anymore, right?{w}\nBeing digested by me feels pleasant, doesn't it?"
    world.battle.enemy[0].name "While soaked in intoxication, melt into mush..."
    l "Aaaa...."
    "She covers me with hundreds of tentacles, and forces me deeper into her."

    show ropa h5

    "Dripping with her digestive juices, the tentacles cover every inch of my body."
    "The sweet pleasure covers my entire body as it's slowly dissolved and absorbed through her mucus covered membrane."
    "My fading consciousness is stained with joy as I feel the pleasure from being digested."
    world.battle.enemy[0].name "Mmm... You're delicious.{w}\nYour body tastes very good..."
    "Since the Roper is happy, I'm happy too...{w}\nThe Roper is eating my body..."
    l "A....a...a..."
    "Like my body, my mind melts into mush."
    "My slowly dissolving body is being absorbed by the Roper."
    "I'm being melted...{w}\nI'm being eaten..."
    "Then..."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Fully dissolved, I don't leave a single bone.{w}\nWhile tasting the highest pleasure, I'm preyed on by a monster."
    "........."

    jump badend

label youko_start:
    python:
        world.troops[12] = Troop(world)
        world.troops[12].enemy[0] = Enemy(world)

        world.troops[12].enemy[0].name = Character("Кицунэ")
        world.troops[12].enemy[0].sprite = ["youko st03", "youko st02", "youko st03", "youko st02"]
        world.troops[12].enemy[0].position = center
        world.troops[12].enemy[0].defense = 100
        world.troops[12].enemy[0].evasion = 90
        world.troops[12].enemy[0].max_life = world.troops[12].battle.difficulty(350, 500, 500)
        world.troops[12].enemy[0].henkahp[0] = world.troops[12].battle.difficulty(175, 250, 300)
        world.troops[12].enemy[0].power = world.troops[12].battle.difficulty(0, 1, 2)
        world.troops[12].enemy[0].half[0] = "Я великолепна!"
        world.troops[12].enemy[0].half[1] = "Ты кончишь очень скоро!"
        world.troops[12].enemy[0].kiki[0] = "Хехе... ты уже на пределе?"
        world.troops[12].enemy[0].kiki[1] = "Ты же хочешь кончить, правда?!"

        world.troops[12].battle.tag = "youko"
        world.troops[12].battle.name = world.troops[12].enemy[0].name
        world.troops[12].battle.background = "bg 032"
        world.troops[12].battle.music = 6
        world.troops[12].battle.exp = 280
        world.troops[12].battle.exit = "lb_0027"

        world.troops[12].battle.init()

    world.battle.enemy[0].name "Уаа! Тут человек!"
    "Похоже, она очень удивлена моему появлению."
    "Кицунэ... монстр-лиса."
    "Они могут быть как очень слабыми, так и очень сильными."
    "Чем больше хвостов, тем сильнее...{w} У этой малышки их два..."
    "Должно быть, она весьма слаба. Даже в прошлый раз одолеть её не составило особого труда."
    world.battle.enemy[0].name "Ч-Что же мне делать?..{w}\nЯ отстала от Тамамо, а теперь ещё и человек появился..."
    l "..."
    "Похоже, драка с ней бессмысленна.{w}\nМожет стоит попытаться договориться с ней, чтобы она отвела меня к Тамамо?"
    window auto
    menu:
        "Поговорить":
            jump youko_v2

        "НИКАКИХ ПЕРЕГОВОРОВ!":
            jump youko_start2

label youko_start2:
    "С другой стороны, разве она не нападает на людей?{w}\nВозможно будет лучше, если я пойду по тому же пути."
    l "Если ты атакуешь людей...{w}\nТогда я должен запечатать тебя!"

    $ world.battle.enemy[0].sprite[0] = "youko st01"

    play music "bgm/battle.ogg"
    show youko st01

    world.battle.enemy[0].name "В таком случае время битвы!"

    $ world.battle.progress = 1
    $ world.battle.main()

label youko_main:
    python:
        if world.battle.progress == 2:
            world.battle.cmd[6] = 0
        elif world.battle.progress != 2:
            world.battle.cmd[6] = 1

        world.battle.call_cmd()

        if world.battle.result == 1 and world.battle.progress == 2:
            renpy.jump(f"{world.battle.tag}_main2")
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label youko_main2:
    "Какую же кицунэ мне бить?.."

    window hide
    hide screen hp
    $ result = renpy.call_screen("kitsune")
    window auto
    show screen hp

    if result == 1:
        jump youko_x3
    elif result in (2, 3):
        jump youko_x2

label youko_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Хьяя!"

    play sound "se/syometu.ogg"
    hide youko with crash

    "Кицунэ запечатывается в милую маленькую лису!"

    $ world.battle.victory(1)

label youko_v2:
    l "Слушай, мне нужно увидеть Тамамо.{w}\nПожалуйста, отведи меня к ней."

    show youko st01

    kitsune "Тамамо?{w}\nК-кто такая «Тамамо»?"
    l "Ты только что сказала, что разделилась с ней.{w}\nЯ не настолько тупой."

    show youko st03

    kitsune "Ауу..."
    "Что за легкомысленный человек...{w} в смысле, кицунэ."
    l "Ну а теперь отведи меня к ней, пожалуйста. Мне нужно с ней поговорить."
    "Гениальная попытка, Лука. Неужели ты правда думаешь, что она поймёт твои намерения и не станет драться?"

    play music "bgm/battle.ogg"
    show youko st02

    kitsune "Эй, а ты симпатичный...{w}\nМне хотелось бы поиграть с тобой немного в постели!"
    l "....."
    "Ну да, ничего нового."
    "Раньше, во время первого путешествия, все эти битвы имели хоть какое-то значение..."

    call youko_a4

    l "......................"
    kitsune "......................"

    call youko_a4

    l "......................"
    kitsune "......................"

    call youko_a4
    call youko_a4
    call youko_a4

    kitsune "......................"
    l "Ты уже всё? Наигралась?"

    show youko st03

    kitsune "Ага..."

    $ world.battle.exit = "lb_0027a"
    $ world.battle.victory(1)

label youko_a:
    if world.battle.enemy[0].life < world.battle.enemy[0].henkahp[0] and world.battle.progress == 1:
        call youko_x1
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,4)

        if random == 1:
            call youko_a1
            $ world.battle.main()
        elif random == 2:
            call youko_a2
            $ world.battle.main()
        elif random == 3:
            call youko_a3
            $ world.battle.main()
        elif random == 4 and world.battle.progress == 3:
            call youko_a4
            $ world.battle.main()

label youko_a1:
    $ world.battle.skillcount(1, 3049)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я поиграю с тобой своим пушистым хвостиком!",
        "Мой хвостик очень пушистый!",
        "Покрой мой хвостик своей белой штучкой!"
    ])

    $ world.battle.show_skillname("Пушистый хвост")
    play sound "audio/se/umaru.ogg"
    show youko ct01 at topright zorder 15 as ct
    show youko ct02 at topright zorder 15 as ct

    "Хвост кицунэ трётся о Луку!"

    python:
        world.battle.enemy[0].damage = {1: (14, 16), 2: (17, 21), 3: (28, 32)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        renpy.hide("ct")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label youko_a2:
    $ world.battle.skillcount(1, 3050)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я полижу твой член!",
        "Ммм... интересно, каков он на вкус..."
        "*Лижет* *Лижет*",
        "Как тебе мой шершавый язычок?"
    ])

    $ world.battle.show_skillname("Лизь-лизь")
    play sound "audio/se/ero_pyu2.ogg"

    "Кицунэ зарывается лицом в пах Луки и начинает лизать его член!!"

    python:
        world.battle.enemy[0].damage = {1: (16, 18), 2: (19, 22), 3: (32, 36)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label youko_a3:
    $ world.battle.skillcount(1, 3051)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я потрусь о него!",
        "Выплесни свою белую штуку!",
        "Хе-хе-хе..."
    ])

    $ world.battle.show_skillname("Жим-жим")
    play sound "audio/se/ero_koki1.ogg"

    "Кицунэ хватается за член Луки и начинает дрочить ему!"

    python:
        world.battle.enemy[0].damage = {1: (15, 17), 2: (17, 20), 3: (30, 34)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label youko_a4:
    $ world.battle.skillcount(1, 3052)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Этой техникой я выдавлю из тебя белую штуку!",
        "Сделав так, я заставлю тебя кончить!",
        "Хе-хе-хе... Сможешь ли ты выдержать это?"
    ])

    $ world.battle.show_skillname("Две луны")

    "Два хвоста кицунэ поднялись и начали причудливый танец!"

    play sound "se/umaru.ogg"
    show youko ct01 at topright zorder 15 as ct
    show youko ct02 at topright zorder 15 as ct

    "Пушистый хвост обвился вокруг члена Луки!"

    $ world.battle.enemy[0].damage = (14, 16)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    play sound "se/umaru.ogg"

    "Другой хвост начал тереться о член Луки!"

    $ world.battle.enemy[0].damage = (14, 16)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h4")

    hide ct

    if world.party.player.max_life > world.party.player.life*2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.max_life > world.party.player.life*5 and not world.party.player.kiki:
        $ world.party.player.common_s1()

    return

label youko_x1:
    world.battle.enemy[0].name "Чёрт!{w}\nКлон-дзюцу!"

    $ world.battle.show_skillname("Техника теневого клонирования")
    play sound "audio/se/warp.ogg"

    show youko st01 at xy(0.2)
    with Dissolve(0.3)
    show youko st01 zorder 5 as youko2
    with Dissolve(0.3)
    show youko st01 at xy(0.8) zorder 5 as youko3
    with Dissolve(0.3)

    "Кицунэ создаёт двух клонов!"

    $ world.battle.enemy[0].counter = 1
    $ world.battle.hide_skillname()

    l "Что за?.."

    show youko st02

    world.battle.enemy[0].name "Уфу-фу... круто выглядит, да?!{w}\nТеперь-то ты понимаешь, что серьёзный противник?!"

    $ world.battle.enemy[0].sprite[0] = "youko st02"
    $ world.battle.enemy[0].position = xy(0.2)
    $ world.battle.progress = 2
    return


label youko_x2:
    l "Хаа!"
    "Лука атакует!"

    $ world.party.player.mp_regen()
    play sound "audio/se/karaburi.ogg"

    "Но меч Луки проходит прямо сквозь тело кицунэ!"
    world.battle.enemy[0].name "Уфу-фу... это была копия!"

    $ world.battle.enemy[0].attack()


label youko_x3:
    $ random = rand().randint(1,3)

    $ world.battle.cry(world.party.player.name[0], [
        "Хаа!",
        "Ораа!",
        "Получай!"
    ])

    $ narrator(f"{world.party.player.name[0].name} атакует!")

    $ world.party.player.mp_regen()
    play sound "audio/se/slash.ogg"

    $ world.party.player.damage = 100 + world.party.player.level
    $ world.battle.enemy[0].life_calc()

    hide youko2
    hide youko3
    show youko st03

    world.battle.enemy[0].name "Гьяф!"

    $ world.battle.enemy[0].sprite[0] = "youko st01"
    $ world.battle.enemy[0].position = center
    $ world.battle.enemy[0].counter = 0
    show youko st01 at center

    if not world.battle.enemy[0].life:
        jump youko_v

    world.battle.enemy[0].name "Д-довольно неплохо..."
    l "........"
    "У этой техники есть серьёзная проблема..."
    world.battle.enemy[0].name "Нуу! Раз уж ты такой бука, я покажу тебе всю свою истинную силу!"

    $ world.battle.skillcount(1, 3052)
    $ world.battle.show_skillname("Две луны")

    "Два хвоста кицунэ поднялись и начали причудливый танец!"

    play sound "se/umaru.ogg"
    show youko ct01 at topright zorder 15 as ct
    show youko ct02 at topright zorder 15 as ct

    "Пушистый хвост обвился вокруг члена Луки!"

    $ world.battle.enemy[0].damage = (14, 16)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    play sound "se/umaru.ogg"

    "Другой хвост начал тереться о член Луки!"

    $ world.battle.enemy[0].damage = (14, 16)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h4")

    hide ct

    if world.party.player.max_life > world.party.player.life*2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.max_life > world.party.player.life*5 and not world.party.player.kiki:
        $ world.party.player.common_s1()

    $ world.battle.progress = 3
    $ world.battle.main()

label youko_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ monster_pos = center
    hide youko2
    hide youko3
    $ world.battle.face(2)
    $ world.battle.enemy[0].counter = 0

    world.battle.enemy[0].name "Ара-ра? Хочешь кончить от моего хвостика?{w}\nХорошо! Посмотрим сколько из тебя выйдет этой белой штуки!{image=note}"

    $ world.battle.progress = 3
    $ world.battle.enemy[0].attack()

label youko_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Fluffy Tail"
    $ list2 = "Lick Lick"
    $ list3 = "Rub Rub"
    $ list4 = "Double Tail"

    if persistent.skills[3049][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3050][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3051][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3052][0] > 0:
        $ list4_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump youko_onedari1
    elif world.battle.result == 2:
        jump youko_onedari2
    elif world.battle.result == 3:
        jump youko_onedari3
    elif world.battle.result == 4:
        jump youko_onedari4
    elif world.battle.result == -1:
        jump youko_main


label youko_onedari1:
    $ monster_pos = center
    hide youko2
    hide youko3
    call onedari_syori

    world.battle.enemy[0].name "You want my fluffy tail to massage you?{w}\nOk... I'll rub you all over!{image=note}"

    while True:
        call youko_a1


label youko_onedari2:
    $ monster_pos = center
    hide youko2
    hide youko3
    call onedari_syori

    world.battle.enemy[0].name "You want me to lick you with my rough tongue...?{w}\nOk! I'll lick you all over!{image=note}"

    while True:
        call youko_a2


label youko_onedari3:
    $ monster_pos = center
    hide youko2
    hide youko3
    call onedari_syori

    world.battle.enemy[0].name "You want to come in my hand...?{w}\nOk! I'll rub you to your hearts content!{image=note}"

    while True:
        call youko_a3


label youko_onedari4:
    $ monster_pos = center
    hide youko2
    hide youko3
    call onedari_syori

    world.battle.enemy[0].name "You want to receive the secret technique of the fox?{w}\nOk... I'll make you come quickly!"

    while True:
        call youko_a4


label youko_h1:
    $ world.party.player.moans(1)
    with syasei1
    show youko ct03 at topright zorder 15 as ct
    show youko bk04 zorder 10 as bk
    $ world.ejaculation()

    "Stroked by her fluffy tail, Luka ejaculates all over her silver fur."

    $ world.battle.lose()

    $ world.battle.count([19, 10])
    $ bad1 = "Luka succumbs to the Kitsune's fluffy tail."
    $ bad2 = "Violated by the Kitsune, Luka was forced to marry her."
    $ monster_pos = center
    hide ct
    hide youko2
    hide youko3
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hahaha, you shot the white stuff all over my tail!{w}\nDid it feel good? Ehehehe..."

    jump youko_h


label youko_h2:
    $ world.party.player.moans(1)
    with syasei1
    show youko bk01 zorder 10 as bk
    $ world.ejaculation()

    "Licked by the Kitsune's rough tongue, Luka ejaculates, shooting semen all over her face."

    $ world.battle.lose()

    $ world.battle.count([4, 10])
    $ bad1 = "Luka succumbs to the Kitsune's rough tongue."
    $ bad2 = "Violated by the Kitsune, Luka was forced to marry her."
    $ monster_pos = center
    hide youko2
    hide youko3
    $ world.battle.face(2)

    world.battle.enemy[0].name "Mmm... *Лижет* *Лижет*{w}\nYou got semen all over my tongue...{w}\nDid it feel good? Ehehehe..."

    jump youko_h


label youko_h3:
    $ world.party.player.moans(1)
    with syasei1
    show youko bk03 zorder 10 as bk
    $ world.ejaculation()

    "Stroked by the Kitsune's soft palm, Luka ejaculates, covering her hand with his seed."

    $ world.battle.lose()

    $ world.battle.count([5, 10])
    $ bad1 = "Luka succumbs to the Kitsune's rubbing hand."
    $ bad2 = "Violated by the Kitsune, Luka was forced to marry her."
    $ monster_pos = center
    hide youko2
    hide youko3
    $ world.battle.face(2)

    world.battle.enemy[0].name "You covered my hands with your white stuff...{w}\nDid it feel good? Ehehehe..."

    jump youko_h


label youko_h4:
    $ world.party.player.moans(1)
    with syasei1
    show youko ct03 at topright zorder 15 as ct
    show youko bk04 zorder 10 as bk
    $ world.ejaculation()

    "Caressed by the Kitsune's two tails, Luka ejaculates, covering her silver fur."

    $ world.battle.lose()

    $ world.battle.count([19, 10])
    $ bad1 = "Luka succumbs to the Kitsune's two tails."
    $ bad2 = "Violated by the Kitsune, Luka was forced to marry her."
    $ monster_pos = center
    hide ct
    hide youko2
    hide youko3
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hahaha, you covered my tail with your white stuff!{w}\nIsn't my ultimate move amazing?"

    jump youko_h


label youko_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    world.battle.enemy[0].name "Let me replenish my energy now.{w}\nFighting you made me tired..."
    "The Kitsune pushes me to the ground!{w}\nBefore I can react, she jumps on top of me!"
    world.battle.enemy[0].name "Ehehe... I'll put you in..."
    l "Ah... Stop... Ahhhhh!"

    play hseanwave "audio/se/hsean01_innerworks_a1.ogg"
    hide ct
    hide bk
    show youko h1 at center


    "My dick is forced into her tiny honey pot."
    "Hot and narrow, she pushes me into her all the way.{w}\nThe sudden burst of pleasure causes me to push my waist up into her."
    world.battle.enemy[0].name "Do I feel good?{w}\nI'll let you put out a lot in me!{image=note}"
    l "Aaaaa...."
    "Her small hole completely swallows me up.{w}\nNot even moving, the pressure from her tightness is already exploding ecstasy through me."
    "Moreover, her walls were slightly rough, and her coarse membrane rubs against me."
    l "Faaa..."
    world.battle.enemy[0].name "Aha, you're feeling good, you're feeling good!{w}\nDoes mating with me feel that good, really?"

    play hseanwave "audio/se/hsean04_innerworks_a4.ogg"

    "Looking pleased with herself, the Kitsune shakes her waist.{w}\nHer rough walls start to violently rub against me, shooting pleasure through my waist."
    l "Aaah... I'm going to come..."
    "Stuck in her tiny rough pussy, I feel myself moving closer to the edge.{w}\nNot even in her for a minute, I feel miserable at already wanting to come."
    world.battle.enemy[0].name "Haha, just a little more and you'll shoot out your white stuff{image=note}.{w}\nOk! Now come!"
    "As the Kitsune laughs, she starts to violently grind her waist into me.{w}\nSuch a tiny girl is forcing me to come..."
    l "Aaaaa!"

    with syasei1
    show youko h2
    $ world.ejaculation()

    "Unable to last even a minute, I feel humiliated as I explode in her vagina."
    world.battle.enemy[0].name "Ahaha, you already let it out.{w}\nComing so easily, what a pathetic hero!{image=note}"
    world.battle.enemy[0].name "Here! I'll bully you even more!{image=note}"

    play hseanwave "audio/se/hsean08_innerworks_a8.ogg"

    "Laughing at me, the Kitsune starts grinding her waist against me again.{w}\nEven though I just came, she's already trying to force me to another..."
    "Stuck in her tiny, rough pussy...{w}\nThis little girl is furiously violating me..."
    l "Stop! Ahhh!"
    world.battle.enemy[0].name "Your semen is really delicious...{w}\nLet out more, more!"
    l "Auuu... If you move so much... Again..."
    "Stimulated by her rough vagina, I feel my power to escape fade."
    "Deep inside the Kitsune, I feel her tighten around the tip of my penis even more.{w}\nThe pleasure from the increased pressure sends chills down my spine."
    world.battle.enemy[0].name "Hehe... Come!{image=note}"
    "As if forcing me to ejaculate, the Kitsune tightens her pussy and pumps me even faster.{w}\nHer movements destroy the last of my endurance."
    l "Aaaaahhh!"

    with syasei1
    show youko h3
    $ world.ejaculation()

    "I've been forced to climax twice now inside of her.{w}\nFor the second time, the proof of my submission to her shoots inside her pussy."
    world.battle.enemy[0].name "Hahaha... Semen... Delicious...{w}\nI really like you!"
    world.battle.enemy[0].name "Hey hey... Do you know how a Kitsune and a human get married?"
    l "M...Marriage...?"
    world.battle.enemy[0].name "A Kitsune violates the human, and receives his seed three times.{w}\nOnce they do that, they're tied together and become a married couple."
    world.battle.enemy[0].name "So far, you've ejaculated in me twice...{w}\nJust one time left! Heehee."
    l "That... Marriage... Auuuu..."
    world.battle.enemy[0].name "I said I like you...{w}\nHey, be my husband!"
    world.battle.enemy[0].name "I'll make you feel good every day...{w}\nFrom morning till night, we'll mate all day!"
    l "That... No way...{w} Ahhh!"
    "The Kitsune looks down at me as she starts to move her hips again.{w}\nCrushed in her tight pussy, I struggle helplessly."
    l "Auu... No..."
    world.battle.enemy[0].name "Come on, come on... Let's get married...{w}\nEven if by force, I'll finish the ceremony...{image=note}"
    "The Kitsune grinds her waist into me harder than before as she laughs.{w}\nAt this rate, I'll be forced to come in her a third time..."
    l "Aaaahhh...."
    world.battle.enemy[0].name "Hahaha, can you endure it...?{w}\n...Or are you going to become my husband?"
    "The Kitsune starts to move right and left as she continues to pump me.{w}\nRubbed all over by her rough pussy, I feel myself on the edge."
    "I can't endure like this...{w}\nI'm going to come inside of her again..."
    l "Auuu... No more...{w}\nI'm coming again...!"
    world.battle.enemy[0].name "Yay! Be my husband!{image=note}"
    "The Kitsune pushes down hard, forcing my tip all the way to the entrance of her womb.{w}\nHitting her deepest point, the sudden stimulation forces me over the edge."
    l "Aaa! Ahhhh!"

    with syasei1
    show youko h4
    $ world.ejaculation()

    "My mind goes pure white as I come inside of her a third time, shooting a large amount of semen into her womb."
    world.battle.enemy[0].name "...Ehehe, you let out a lot.{w}\nThe ceremony is over with that. We're married now!"
    world.battle.enemy[0].name "Let's celebrate with Tamamo, then start living together!"
    "The Kitsune's tails start to wag furiously.{w}\nStill stuck in her pussy, the wagging causes an intense tightening around me."
    l "Aaa... That feels good..."
    world.battle.enemy[0].name "Ehehe... We'll be together forever now!{image=note}{w}\nWe'll mate every day, and have lots of cute babies!{image=note}"
    "The Kitsune shakes her hips as she talks excitedly."
    l "Auu... Coming again..."

    with syasei1
    $ world.ejaculation()

    "I continue mating with my new wife, the Kitsune, as my consciousness fades."
    "Stuck inside of her tiny pussy, everything I shoot out is sucked up by her womb."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Now her husband, I'm taken back to her village."
    "At first, all I tried to do was look for a chance to run away.{w}\nHowever, I slowly started to become happy living there."
    "Thus, I spent the rest of my life with my cute wife."
    "........."

    jump badend

label meda_start:
    python:
        world.troops[13] = Troop(world)
        world.troops[13].enemy[0] = Enemy(world)

        world.troops[13].enemy[0].name = Character("Девушка-меда")
        world.troops[13].enemy[0].sprite = ["meda st01"] * 4
        world.troops[13].enemy[0].position = center
        world.troops[13].enemy[0].defense = 100
        world.troops[13].enemy[0].evasion = 95
        world.troops[13].enemy[0].max_life = world.troops[13].battle.difficulty(280, 330, 380)
        world.troops[13].enemy[0].power = world.troops[13].battle.difficulty(0, 1, 2)

        world.troops[13].battle.tag = "meda"
        world.troops[13].battle.name = world.troops[13].enemy[0].name
        world.troops[13].battle.background = "bg 032"
        world.troops[13].battle.music = 1
        world.troops[13].battle.exp = 320
        world.troops[13].battle.exit = "lb_0028"

        world.troops[13].battle.init()

    "Это пещерный монстр...{w}\nВстретить их на поверхности — большая редкость."
    world.battle.enemy[0].name "Добыча..."
    "Девушка-меда со щупальцами вместо волос...{w}\nЕё поведение подобно поведению насекомых, поэтому она ведёт себя не так, как остальные монстры."
    l "Нечто подобное довольно редко встретишь в таких местах..."
    "Немного сбитый с толку, я обнажил свой меч."

    $ world.battle.main()

label meda_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label meda_v:
    world.battle.enemy[0].name "Ах..."

    play sound "se/syometu.ogg"
    hide meda with crash

    "Девушка-меда превращается в нечто, похожее на обыкновенную вошь!"

    $ world.battle.victory(viclose=1)

label meda_a:
    if world.party.player.bind == 1:
        call meda_a3
        $ world.battle.main()
    elif world.party.player.bind == 2:
        call meda_a4
        $ world.battle.main()

    while True:
        $ random = rand().randint(1, 2)

        if random == 1:
            call meda_a1
            $ world.battle.main()
        elif random == 2 and world.battle.delay[0] > 3:
            call meda_a2
            $ world.battle.main()


label meda_a1:
    $ world.battle.skillcount(1, 3053)

    world.battle.enemy[0].name "........."

    $ world.battle.show_skillname("Щупальца")
    play sound "audio/se/ero_makituki2.ogg"

    "Девушка-меда вытягивает свои щупальца и скользит ими по телу Луки!"

    python:
        world.battle.enemy[0].damage = {1: (13, 15), 2: (19, 22), 3: (22, 30)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label meda_a2:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Поймать..."

    $ world.battle.show_skillname("Обездвиживание")
    play sound "audio/se/ero_name2.ogg"

    "Меда обездвиживает Луку одним из своих щупалец!"

    $ world.battle.enemy[0].damage = (13, 15)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Лука опутан щупальцем!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("meda_h2")

    world.battle.enemy[0].name "Добыча... поймана..."

    $ world.battle.enemy[0].power = 1
    return


label meda_a3:
    $ world.battle.skillcount(1, 3054)

    "Лука опутан щупальцами девушки-меды!"

    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Не уйдёшь..."

    $ world.battle.show_skillname("Опутывание щупальцами")
    play sound "audio/se/ero_name2.ogg"

    "Щупальца меды начинаются щекотать член Луки!"

    $ world.party.player.bind = 2

    python:
        world.battle.enemy[0].damage = {1: (17, 19), 2: (25, 28), 3: (34, 38)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    if world.party.player.surrendered:
        return

    "Но меда не может подтянуть Луку к своему рту!"

    return


label meda_a4:
    $ world.battle.skillcount(1, 3054)

    "Лука опутан щупальцами меды!"
    world.battle.enemy[0].name "........"

    $ world.battle.show_skillname("Потирание щупальцем")
    play sound "audio/se/ero_name2.ogg"

    "Щупальце меды трётся о член Луки!"

    python:
        world.battle.enemy[0].damage = {1: (17, 19), 2: (25, 29), 3: (34, 38)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label meda_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"
    world.battle.enemy[0].name "Добыча... сдаётся..."

    $ world.battle.enemy[0].attack()

label meda_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Tentacle"
    $ list2 = "Bound Tentacle"
    $ list3 = "Thousand Worms"

    if persistent.skills[3053][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3054][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3055][0] > 0:
        $ list3_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump meda_onedari1
    elif world.battle.result == 2:
        jump meda_onedari2
    elif world.battle.result == 3:
        jump meda_onedari3
    elif world.battle.result == -1:
        jump meda_main


label meda_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "Catch... Wants...?"

    while True:
        call meda_a1


label meda_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "Catch... Wants...?"

    if not world.party.player.bind:
        call meda_a2

    call meda_a3

    while True:
        call meda_a4


label meda_onedari3:
    call onedari_syori

    world.battle.enemy[0].name "Catch... Wants...?"

    if not world.party.player.bind:
        call meda_a2

    if world.party.player.bind == 1:
        call meda_a3
    call meda_a4


label meda_h1:
    $ world.party.player.moans(1)
    with syasei1
    show meda bk03 zorder 10 as bk
    $ world.ejaculation()

    "Being rubbed by the Meda's tentacle is too much for Luka, as he ejaculates all over her!"

    $ world.battle.lose()

    $ world.battle.count([10, 4])
    $ bad1 = "Luka succumbs to the Meda's tentacles."
    $ bad2 = "The Meda continues to wring out Luka's semen until he dies."
    jump meda_h


label meda_h2:
    $ world.party.player.moans(3)
    with syasei1
    show meda bk03 zorder 10 as bk
    $ world.ejaculation()

    "Being bound by the Meda's tentacle is too much for Luka, as he ejaculates all over her!"

    $ world.battle.lose()

    $ persistent.count_bouhatu += 1
    $ world.battle.count([10, 4])
    $ bad1 = "Luka succumbs to the Meda's tentacles binding him."
    $ bad2 = "The Meda continues to wring out Luka's semen until he dies."
    jump meda_h


label meda_h3:
    $ world.party.player.moans(1)
    with syasei1
    show meda bk03 zorder 10 as bk
    $ world.ejaculation()

    "Being rubbed by the Meda's tentacle while bound is too much for Luka, as he ejaculates all over her!"

    $ world.battle.lose()

    $ world.battle.count([10, 4])
    $ bad1 = "Luka succumbs to the Meda's tentacles as he's bound."
    $ bad2 = "The Meda continues to wring out Luka's semen until he dies."
    jump meda_h


label meda_h4:
    $ world.party.player.moans(3)
    with syasei1
    show meda h2
    show meda ct02 at topright zorder 15 as ct
    $ world.ejaculation()

    "Just being forced into her mouth was too much, as he accidentally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([2, 4])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka accidentally ejaculates as soon as he's inserted into the Meda's mouth."
    $ bad2 = "The Meda continues to wring out Luka's semen until he dies."

    world.battle.enemy[0].name "Catch... Semen..."
    "Keeping me in her mouth, the Meda's tentacle start to wriggle around me."

    jump meda_h


label meda_h5:
    $ world.party.player.moans(2)
    with syasei1
    show meda h2
    show meda ct02 at topright zorder 15 as ct
    $ world.ejaculation()

    "The thousands of worm-like tentacles wriggling in the Meda's mouth are too much for Luka, as he ejaculates in her mouth!"

    $ world.battle.lose()

    $ world.battle.count([2, 4])
    $ bad1 = "Luka succumbs to the Meda's tentacles inside her mouth."
    $ bad2 = "The Meda continues to wring out Luka's semen until he dies."

    world.battle.enemy[0].name "Catch... Semen..."
    "Keeping me in her mouth, the Meda's tentacle continue to wriggle around me."

    jump meda_h


label meda_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.party.player.bind == 3:
        play hseanwave "audio/se/hsean21_tentacle2.ogg"
    else:
        world.battle.enemy[0].name "Catch... Semen..."

        play sound "audio/se/ero_name2.ogg"

        "Tentacles shoot out of the Meda's body and wrap around me!{w}\nShe starts to slowly pull me towards her."
        l "Aaa!"
        "As I'm slowly drawn towards her, she opens a mouth in her lower abdomen.{w}\nInside that mouth are thousands of wriggling tentacles..."
        "Things that look like worms or eels are wriggling and waving all over."
        l "No way..."
        "She isn't going to put me in there, is she...?{w}\nAs I shudder in horror, a sense of sick excitement rises in me."
        "In that wriggling mass of tentacles, what would it feel like...?{w}\nIf my dick was in there, how good would it feel...?"
        l "Aaa... Stop..."

        play hseanwave "audio/se/hsean21_tentacle2.ogg"
        hide bk
        show meda ct01 at topright zorder 15 as ct
        show meda h1
        with dissolve

        "With a final pull, she forces me into her mouth!"

    l "Faaa!"
    "The worm-like tentacles start to rise up one by one and wrap around my penis."
    "Squeezing, wrapping, coiling...{w}\nThe tentacles are massaging me all over as if they have a mind of their own."
    l "Hii! Stop!"
    "Fearing the creepy pleasure attacking me, I try to pull out my waist.{w}\nThe tentacles holding me don't allow me to move, though."
    "Rather than escaping, I'm instead forced deeper into her.{w}\nI'm stuck to her... There's no escape..."
    l "Auu... Let go!{w}\nAaaaa!"
    "My penis is completely covered by her writhing tentacles inside her mouth.{w}\nThe creepy feeling starts to overpower my horror at the situation."
    l "Faaa!{w} Stop...!"
    world.battle.enemy[0].name ".........."
    "The Meda stares directly into my eyes.{w}\nNot showing any emotion, it's the gaze of someone staring at their prey."
    "Stimulating her prey, forcing them to ejaculate... It's just an emotionless process to her."
    l "Auuu!"
    "Inside her mouth, the thousands of tentacles start to increase their speed."
    "The tentacles wrapped around me tighten up, the sweet pleasure causing me to drool."
    l "Faa!{w} Too good..."
    "Feeling the rising orgasm in me, I realize that I've given myself completely to this pleasure."
    "All I want to do is come just like this..."
    world.battle.enemy[0].name ".........."
    "Staring into her unfeeling eyes, the stimulation I'm feeling in her mouth suddenly becomes even more captivating."
    "Countless tentacles in her, all doing nothing but forcing me to ejaculate..."
    l "Aaaa!"

    with syasei1
    show meda ct02 at topright zorder 15 as ct
    show meda h2
    $ world.ejaculation()

    "Finally giving in to the tentacles urging me to come, I explode in her mouth, shooting semen all over the wriggling tentacles."
    world.battle.enemy[0].name ".........."

    play hseanwave "audio/se/hsean22_tentacle3.ogg"

    "As soon as the last drop is squeezed out of me, the tentacles start to massage me again."
    "Crawling all over me, the tentacles each move independently of the other as they continue to forcefully try to make me ejaculate."
    l "Hya!{w} Haaa!"
    "As if they were hungry for more, the countless tentacles squeeze me and massage me all over."
    "The creepy pleasure already starts to move me to the edge again."
    l "Hauuu...{w}\nAaaaa!"
    world.battle.enemy[0].name ".........."
    "The Meda continues to stare at me coldly, acting just like an insect mechanically eating her prey."
    "Caught by an insect, who only wants my fluids..."
    l "Ha! Haaa...."
    "The tentacles continue rubbing me, urging me towards another ejaculation."
    "Caught by this insect monster, I feel humiliated at only being viewed as a source of food."
    l "Aaaa... Stop..."
    "I beg her, but she gives no mercy.{w}\nInstead, the stimulation continues and forces me to another orgasm."
    l "C...Coming!{w} Ahhh!"

    with syasei1
    show meda ct03 at topright zorder 15 as ct
    show meda h3
    $ world.ejaculation()

    "The moment when I came, the tentacles all started wriggling around in a panic, as if they were all trying to be the first to get a taste."
    "Moving all over the place, they squeeze and rub against me in their desperation to get my semen."
    l "Aaaaa..."
    "After my ejaculation is over, they rub me even harder than before, as if urging on another.{w}\nUnable to endure the nonstop stimulation, I'm driven to the edge again."

    with syasei1
    show meda ct04 at topright zorder 15 as ct
    show meda h4
    $ world.ejaculation()

    l "Stop... Help..."
    "Even if I cry while screaming for help, the Meda just continues to stare at me.{w}\nStuck in her like this, I'm going to be milked dry..."
    "My penis treated as nothing but a source of nourishment, I feel the ultimate humiliation of a man."
    l "Faaa!"

    with syasei1
    $ world.ejaculation()

    "Forced to come over and over again, my consciousness starts to fade."
    "Even as I faint, she continues to force me to ejaculate more..."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Like that, she sucks everything out of me to the last drop."
    "After being forced to come too many times to count, my corpse remained in that dark cave..."
    ".........."

    jump badend

label kumo_start:
    python:
        world.troops[14] = Troop(world)
        world.troops[14].enemy[0] = Enemy(world)

        world.troops[14].enemy[0].name = Character("Девушка-паук")
        world.troops[14].enemy[0].sprite = ["kumo st01", "kumo st02", "kumo st03", "kumo st02"]
        world.troops[14].enemy[0].position = center
        world.troops[14].enemy[0].defense = 100
        world.troops[14].enemy[0].evasion = 95
        world.troops[14].enemy[0].max_life = world.troops[14].battle.difficulty(300, 350, 400)
        world.troops[14].enemy[0].power = world.troops[14].battle.difficulty(0, 1, 2)

        world.troops[14].battle.tag = "kumo"
        world.troops[14].battle.name = world.troops[14].enemy[0].name
        world.troops[14].battle.background = "bg 032"
        world.troops[14].battle.music = 1
        world.troops[14].battle.exp = 350
        world.troops[14].battle.exit = "lb_0029"

        world.troops[14].battle.init()

    l "Остановись!"
    "Не способный смотреть на это молча, я закричал на Девушку-Паука."

    $ world.battle.face(2)

    world.battle.enemy[0].name "Вот это да... Ещё одна глупая добыча появилась."
    world.battle.enemy[0].name "Хи-хи... Аппетитно выглядящий мальчик, к тому же.{w}\nЯ выжму твоё семя..."
    l "Ух-х! Выпусти кицунэ!"
    world.battle.enemy[0].name "Конечно...{w}\nЕсли я заполучу человеческого мальчика, мне не понадобится такой крошечный монстрик."
    world.battle.enemy[0].name "Вместо неё я съем тебя.{w}\nЯ запеленаю тебя своей паутиной и высосу тебя досуха..."

    $ world.battle.face(1)

    if not persistent.skills[3060][0]:
        $ world.battle.skillcount(1, 3060)

    $ world.battle.main()

label kumo_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            renpy.jump(f"{world.battle.tag}_miss")
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2:
            world.battle.common_struggle()
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label kumo_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Как такое возможно..."

    play sound "se/syometu.ogg"
    hide kumo with crash

    "Девушка-паук превратилась в крошечного паучка!"

    $ world.battle.victory(1)

label kumo_a:
    if world.battle.turn == 1:
        call kumo_a2
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,4)

        if random == 1:
            call kumo_a1
            $ world.battle.main()
        elif random == 2:
            call kumo_a2
            $ world.battle.main()
        elif random > 2 and world.party.player.bind == 0:
            call kumo_a3
            $ world.battle.main()
        elif random > 2 and world.party.player.bind == 1:
            call kumo_a4
            $ world.battle.main()
        elif random > 2 and world.party.player.bind == 2:
            call kumo_a5
            $ world.battle.main()
        elif random > 2 and world.party.player.bind == 3:
            call kumo_a6
            $ world.battle.main()


label kumo_a1:
    $ world.battle.skillcount(1, 3056)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я потрусь о каждый кусочек твоего тела...",
        "Я поиграю с тобой всеми своими лапами...",
        "Хе-хе... как думаешь, с какой частью тела мне поиграть?"
    ])

    $ world.battle.show_skillname("Восьминогий масстаж")
    play sound "audio/se/umaru.ogg"

    "Девушка-паук массирует тело Луки!"
    python:
        world.battle.enemy[0].damage = (3, 5)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, line=1)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, line=2)

    "Прям каждый сантиметр его тела!"
    python:
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, line=1)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, line=2)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label kumo_a2:
    $ world.battle.skillcount(1, 3057)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хочешь я покрою своими ниточками и его тоже?",
        "Я превращу его в кокон...",
        "С этим членом в паутине... ты выглядишь таким жалким."
    ])

    $ world.battle.show_skillname("Окукливание члена")
    play sound "audio/se/ero_ito.ogg"
    show kumo ct01 at topleft zorder 15 as ct
    show kumo ct02 at topleft zorder 15 as ct

    "Девушка-паук закутывает член Луки в паутину!"

    python:
        world.battle.enemy[0].damage = {1: (12, 15), 2: (18, 22), 3: (24, 30)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        renpy.hide("ct")

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label kumo_a3:
    $ world.battle.skillcount(1, 3058)

    world.battle.enemy[0].name "Я оплету всё твоё тело..."

    $ world.battle.show_skillname("Оплетение тела")
    play sound "audio/se/ero_ito.ogg"

    "Девушка-паук выстреливает клейкой нитю в Луку!"

    $ world.battle.enemy[0].damage = (15, 18)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Тело Луки обвито паутиной девушки-паука!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    $ world.battle.face(2)

    world.battle.enemy[0].name "Как тебе моя липкая нить?..{w}\nЯ продолжу тебя обвивать ею, пока ты не превратишься в кокон..."
    world.battle.enemy[0].name "А после я высосу всю сперму досуха...{w}\nИли может ты предпочтёшь быть съеденным?.."

    if world.battle.surrendered:
        return

    l "Гх..."
    "Похоже теперь я не смогу проатаковать...{w}\nЕсли она добавит ещё паутины, это может стать проблемой..."

    return

label kumo_a4:
    world.battle.enemy[0].name "Думаю стоит подбавить ещё паутины...{w}\nПревратись в кокон..."

    $ world.battle.skillcount(1, 3058)
    $ world.battle.show_skillname("Оплетение тела")
    play sound "audio/se/ero_ito.ogg"

    "Девушка-паук выстреливает клейкой нитю в Луку!"

    $ world.battle.enemy[0].damage = (15, 18)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    $ world.party.player.bind = 2

    "Тело Луки обвито паутиной девушки-паука!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    if world.battle.surrendered:
        return

    l "Угх...."
    "Уже проблематично двигаться...{w}\nДумаю сейчас я смогу бить лишь в полсилы."
    world.battle.enemy[0].name "Я продолжу оплетать тебя своей паутиной..."
    world.battle.enemy[0].name "Такими темпами ты скоро станешь коконом.{w}\nТы ведь хочешь этого?.."

    return

label kumo_a5:
    world.battle.enemy[0].name "Думаю стоит подбавить ещё паутины...{w}\nАха-ха-ха... теперь-то ты сдашься?"

    $ world.battle.skillcount(1, 3058)
    $ world.battle.show_skillname("Оплетение тела")
    play sound "audio/se/ero_ito.ogg"

    "Девушка-паук выстреливает клейкой нитю в Луку!"

    $ world.battle.enemy[0].damage = (15, 18)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    $ world.party.player.bind = 3

    "Тело Луки обвито паутиной девушки-паука!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    if world.battle.surrendered:
        return

    l "Агх..."
    "Теперь мне почти не сдвинуться.{w}\nЕщё немного и я буду полностью обездвижен..."
    world.battle.enemy[0].name "Просто сдайся уже и позволь мне сделать из тебя кокон...{w}\nЭто потрясающие ощущения, так что жди с нетерпением..."

    return

label kumo_a6:
    world.battle.enemy[0].name "И наконец... я закончила.{w}\nСтань же коконом..."

    $ world.battle.skillcount(1, 3058)
    $ world.battle.show_skillname("Body Wrap")
    play sound "audio/se/ero_ito.ogg"

    "Девушка-паук выстреливает клейкой нитю в Луку!"

    $ world.battle.enemy[0].damage = (15, 18)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    $ world.party.player.bind = 4

    "Тело Луки полностью обвито паутиной девушки-паука!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    l "Ааа!"
    "Нити стягиваются, не позволяя Луке сделать ни шагу!"
    world.battle.enemy[0].name "Уфу-фу... спасибо за ожидание!{w}\nА теперь, я выжму твоё семя..."


label kumo_a7:
    $ world.battle.skillcount(1, 3059)

    "At the tip of the Spider Girl's abdomen, a hole opens up!{w}\nThe Spider Girl forces down the hole onto Luka's penis!"

    show kumo ha1 with Dissolve(1.5)
    $ world.battle.show_skillname("Forced Violation")

    "The Spider Girl's lower entrance quickly tightens around Luka!{w}\nTiny bumps inside of her rub Luka all over!"

    $ world.battle.enemy[0].damage = (25, 30)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h4")

    l "Aaaaa!"
    world.battle.enemy[0].name "Feels good, doesn't it?{w}\nShoot out everything inside of me..."

    $ world.battle.skillcount(1, 3059)
    $ world.battle.show_skillname("Forced Violation")
    play sound "audio/se/ero_chupa4.ogg"

    "The Spider Girl's lower entrance quickly tightens around Luka!{w}\nTiny bumps inside of her rub Luka all over!"

    $ world.battle.enemy[0].damage = (25, 30)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h4")

    if world.party.player.surrendered and world.party.player.life > 30:
        call kumo_a8

    l "Hiii... No more..."
    world.battle.enemy[0].name "This next will finish it...{w}\nLet it out in me as you're pathetically stuck there..."

    $ world.battle.skillcount(1, 3059)
    $ world.battle.show_skillname("Forced Violation")
    play sound "audio/se/ero_chupa4.ogg"

    "The Spider Girl's lower entrance quickly tightens around Luka!{w}\nTiny bumps inside of her rub Luka all over!"

    $ world.battle.enemy[0].damage = (25, 30)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    $ renpy.jump(f"{world.battle.tag}_h4")

label kumo_a8:
    $ world.battle.skillcount(1, 3059)

    l "Aaaa..."

    python:
        while True:
            random = rand().randint(1,3)

            if random != ren:
                ren = random
                break

    if random == 1:
        world.battle.enemy[0].name "I'll tease you more and more..."
    elif random == 2:
        world.battle.enemy[0].name "It feels amazing inside me, doesn't it?"
    elif random == 3:
        world.battle.enemy[0].name "I'll make it feel even better..."

    $ world.battle.show_skillname("Forced Violation")
    play sound "audio/se/ero_chupa4.ogg"

    "The Spider Girl's lower entrance quickly tightens around Luka!{w}\nTiny bumps inside of her rub Luka all over!"

    $ world.battle.enemy[0].damage = (25, 30)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    if world.party.player.life > 30:
        call kumo_a8

    return

label kumo_miss:
    if world.party.player.bind == 1:
        $ world.battle.common_struggled_attack(80)
    elif world.party.player.bind == 2:
        $ world.battle.common_struggled_attack(65)
    elif world.party.player.bind == 3:
        $ world.battle.common_struggled_attack(50)

label kumo_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Аха-ха, так ты всё-таки хочешь быть добычей.{w}\nХорошо, тогда я выжму из тебя всё..."

    $ world.battle.enemy[0].attack()

label kumo_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Eight-Legged Massage"
    $ list2 = "Penis Cocoon"
    $ list3 = "Body Wrap"
    $ list4 = "Forced Violation"
    $ list5 = "Predation"

    if persistent.skills[3056][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3057][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3058][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3059][0] > 0:
        $ list4_unlock = 1

    if persistent.skills[3060][0] > 0:
        $ list5_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump kumo_onedari1
    elif world.battle.result == 2:
        jump kumo_onedari2
    elif world.battle.result == 3:
        jump kumo_onedari3
    elif world.battle.result == 4:
        jump kumo_onedari4
    elif world.battle.result == 5:
        jump kumo_onedari5
    elif world.battle.result == -1:
        jump kumo_main


label kumo_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want to be stroked by all my legs?{w}\nAlright... I'll rub you all over."

    while True:
        call kumo_a1


label kumo_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want me to do that to your important penis?{w}\nYou're an odd one..."

    while True:
        call kumo_a2


label kumo_onedari3:
    call onedari_syori

    world.battle.enemy[0].name "So you do want to be wrapped up in my silk...{w}\nAlright, I'll wrap you up nice and tight."

    if not world.party.player.bind:
        call kumo_a3
    elif world.party.player.bind == 1:
        call kumo_a4
    elif world.party.player.bind == 2:
        call kumo_a5

    call kumo_a6


label kumo_onedari4:
    call onedari_syori

    world.battle.enemy[0].name "Hahaha, so you want me to feed on your semen?{w}\nWhat a miserable man..."

    call kumo_a7


label kumo_onedari5:
    call onedari_syori
    $ world.battle.skillcount(1, 3060)

    world.battle.enemy[0].name "Hahaha, you want me to eat you?{w}\nWrapped tightly in my silk, you want to be nothing but my prey?"
    world.battle.enemy[0].name "Alright... I'll eat you nice and slowly..."
    l "........"
    "Ruled by the desire to be eaten, I surrender to the Spider.{w}\nFrom now on, I'll let her eat me..."

    $ world.battle.lose(1)

    $ world.battle.count([33, 11])
    $ bad1 = "Wanting to be eaten, Luka surrenders to the Spider Girl."
    $ bad2 = ""
    $ kousan = 4
    jump kumo_hb


label kumo_h1:
    $ world.party.player.moans(1)
    with syasei1
    show kumo bk05 zorder 10 as bk
    $ world.ejaculation()

    "Massaged by the Spider Girl's eight legs, Luka ejaculates."

    $ world.battle.lose()

    $ world.battle.count([28, 11])
    $ bad1 = "Luka succumbs to the Spider Girl's eight leg massage."
    $ bad2 = "After that, Luka is eaten by the Spider Girl..."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Letting it out that easily...?{w}\nSuch a pathetic man.{w} Now, it's time to eat..."

    jump kumo_hb


label kumo_h2:
    $ world.party.player.moans(1)
    with syasei1
    show kumo ct03 at topleft zorder 15 as ct
    $ world.ejaculation()

    "With his penis wound up in the Spider Girl's thread, Luka is forced to ejaculate."

    $ world.battle.lose()

    $ world.battle.count([20, 11])
    $ bad1 = "Luka succumbs to the Spider Girl's winding silk on his penis."
    $ bad2 = "After that, Luka is eaten by the Spider Girl..."
    show kumo ct04 at topleft zorder 15 as ct
    $ world.battle.face(2)

    world.battle.enemy[0].name "Letting it out that easily...?{w}\nSuch a pathetic man.{w} Now, it's time to eat..."

    jump kumo_hb


label kumo_h3:
    $ world.party.player.moans(3)
    with syasei1
    show kumo bk03 zorder 10 as bk
    $ world.ejaculation()

    "Still having his body wound up, Luka accidentally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([20, 11])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka succumbs to the Spider Girl's body wrap."
    $ bad2 = "After that, Luka is eaten by the Spider Girl..."
    $ world.battle.face(2)

    world.battle.enemy[0].name "I was still rolling you up... You let it out so early.{w}\nSuch a pathetic man.{w} Now, it's time to eat..."

    jump kumo_hb


label kumo_h4:
    $ world.party.player.moans(2)
    with syasei1
    show kumo ha2
    $ world.ejaculation()

    "Squeezed insider her lower entrance, Luka is forced to ejaculate."

    $ world.battle.lose()

    $ world.battle.count([2, 3])
    $ bad1 = "Luka succumbs to the Spider Girl's forced violation."
    $ bad2 = "Stuck in the Spider Girl's web for the rest of his life, Luka continually feeds her his semen..."

    world.battle.enemy[0].name "Hehe... Did it really feel that good?{w}\nI want more of your semen..."

    jump kumo_ha


label kumo_ha:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut


    l "Ahii!"

    play hseanwave "audio/se/hsean19_slime2.ogg"
    hide ct
    hide bk
    show kumo ha3(1.5)

    "The insides of the Spider Girl's lower mouth suddenly start to wriggle."
    "As if covered with bumps and mucus, every movement rubs me all over."
    world.battle.enemy[0].name "Feels good, doesn't it?{w}\nLet it all out..."
    l "Hau! Haaa!"
    "With her slimy walls wriggling around, the bumps inside of her lower mouth rub into me as the Spider Girl starts to move her abdomen up and down."
    world.battle.enemy[0].name "Haha, such pitiful prey."
    "Watching me writhe in agony, the Spider Girl gives a sadistic smile.{w}\nSqueezing out the semen of her prey, she's immersed in joy watching me struggle."
    world.battle.enemy[0].name "Hahaha.{w}\nI'll wrap my silk around you inside of me..."
    "Suddenly, the Spider Girl starts to shoot webbing inside of her hole, covering my penis in a soft, sticky silk."
    l "Auuu... The silk...{w}\nSo soft and sticky..."
    "The sticky string wrapping around me slowly starts to tighten as she covers more of me."
    "The bizarre stimulation quickly causes me to yield to the pleasure."
    l "Au! Aaaa!"

    with syasei1
    show kumo ha5
    $ world.ejaculation()

    "Still stuck in her spinneret, the spider silk wrapped around me felt amazing."
    world.battle.enemy[0].name "Fufu... You seem so weak to this.{w}\nDo you like my sticky threads that much?"
    l "Ahii... Stop..."
    "As I beg for her to stop, the Spider Girl starts to shoot more of her sticky silk over me."
    "Wrapping around my dick even tighter, the pleasure felt like it was making me melt."
    l "Aah...."
    "Even though my penis is being wrapped in her silk, the feeling from her rough walls doesn't weaken at all."
    "In fact, every bump and crease inside of her has their feelings magnified."
    l "Inside you... Amazing... Too good..."
    world.battle.enemy[0].name "Hahaha, shoot out more.{w}\nI'll suck up all your semen, and make it my food..."
    "Inside of her spinneret, she continues to tighten her sticky silk around me, while moving her waist up and down."
    "The overpowering stimulation from so many different methods quickly sends me over the edge again."
    l "Auuu.. Coming again!{w}\nAhhhh!"

    with syasei1
    show kumo ha6
    $ world.ejaculation()

    "I explode inside the Spider Girl again.{w}\nFor some reason, the orgasm gave me a strange sense of calm."
    world.battle.enemy[0].name "Aha, you let out a lot again...{w}\nYour semen is very delicious..."
    world.battle.enemy[0].name "Eating you would be too wasteful...{w}\nInstead, I'll keep you here and let you produce more semen for me."
    l "Faaa!"
    "The Spider Girl suddenly contracts her hole, and forces my tip to rub against one of her bumps."
    "The sudden pleasure forces me over the edge instantly.{w}{nw}"

    with syasei1
    $ world.ejaculation()

    extend "{w=.7}\nToyed with by the Spider Girl, I explode in her again."
    world.battle.enemy[0].name "Haha, let out more!{w}\nYou're my prey until you die, after all."
    world.battle.enemy[0].name "With no rest, I'll keep squeezing out your semen over and over.{w}\nFufu... Are you excited for it?"
    l "Aaaa...."
    "While feeling ecstasy, my consciousness slowly fades."
    "There's no escape from this web...{w}\nStuck as her semen slave, I'll be forced to ejaculate for her until I die."
    "Even while crying from the tsunami of pleasure, she forces me to come over and over again."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "........"

    jump badend


label kumo_hb:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut


    if kousan < 4:
        l "Hiii..."
    elif kousan == 4:
        l "Aaa..."

    play sound "audio/se/ero_makituki3.ogg"
    hide ct
    hide bk
    show kumo hb1 with Dissolve(1.5)

    "As I tremble, the Spider Girl quickly covers every part of my body with her sticky silk."
    "Preparing me to be eaten, she completely wraps me up."
    world.battle.enemy[0].name "Us spiders don't eat our prey like other monsters...{w}\nWe use external digestion to devour our prey..."
    world.battle.enemy[0].name "Hey... How would you like me to eat you?"
    "The Spider Girl laughs as she looks at my face, stuck in the web."
    world.battle.enemy[0].name "How about if I bite you in the neck, and inject you with my sweet pleasure venom..."
    world.battle.enemy[0].name "Then your body will slowly melt from the inside...{w}\nLeaving only your skin, your meat will be all syrupy..."
    world.battle.enemy[0].name "Then I'll quickly slurp up everything that's left...{w}\nIt will feel amazing, so get ready..."

    if kousan < 4:
        l "Hii! No! Stop!"
    elif kousan == 4:
        l "Aaa...."

    "The Spider Girl slowly moves to my neck while giving me a sadistic smile."

    if persistent.vore == 1:
        jump vore

    world.battle.enemy[0].name "Fufu... Now for the venom..."

    show kumo hb2 with Dissolve(1.5)

    world.battle.enemy[0].name "*Bite*"

    if kousan < 4:
        l "Hiii!"
    elif kousan == 4:
        l "Faaaa!"

    "There was only a slight pain in my neck.{w}\nIt was quickly replaced by a sweet pleasure that started to flow through my body."
    l "Aaa..."
    world.battle.enemy[0].name "Doesn't it feel amazing?{w}\nMy venom is inside of you now..."
    world.battle.enemy[0].name "There's nothing left but to be melted from the inside..."

    if kousan < 4:
        l "N...No!"
    elif kousan == 4:
        l "Aaaa...."

    "The pleasure from my neck slowly spreads through my body.{w}\nIt starts to fill me with an odd sense of peace."
    "The horror at my body being digested from the inside is slowly being overwritten by ecstasy."
    "Even that is simply a response to her venom..."
    l "Aaaa... My body is so warm..."
    "The feeling of my body being melted was bringing pleasure to my body..."
    world.battle.enemy[0].name "Everyone hates it at first, but their faces always become so happy...{w}\nJust like you right now, Hahaha!"
    l "Hauuu...."
    world.battle.enemy[0].name "My sweet venom is moving through your body right now, slowly digesting you.{w}\nAre you happy that you're being melted away?"
    l "Faaa...."
    "My fear completely gone, pleasure is all I can feel.{w}\nMy warm body is wrapped in ecstasy while slowly melting away."
    world.battle.enemy[0].name "...Now, it's time to eat.{w}\nLike this, I'll suck up your melting body..."
    l "Aaa... Ahi!"

    play hseanvoice "audio/voice/fera_zyubu1.ogg"
    show kumo hb3 with Dissolve(1.5)

    world.battle.enemy[0].name "Mmmm...{w} *Slurp*"
    l "Faaa!"
    "As the Spider Girl starts to suck up my body, the pleasure shoots fireworks off in my head."
    "Between my dissolving groin, semen starts to leak out."
    world.battle.enemy[0].name "Hahaha, you're coming while being eaten.{w}\nWhat a pathetic man."
    l "Ah... Hiii..."
    "My entire existence is being absorbed by the Spider Girl."
    "Yet for some reason, I feel so happy.{w}\nPleasure filling my body, I feel as if I'm in paradise."
    "Being eaten by a monster feels this good..."
    world.battle.enemy[0].name "Fufu... Stuck in your trance, be sucked away..."
    world.battle.enemy[0].name "Mmmm...{w} *Slurp*{w} *Сосёт*"
    l "Aaaaa!"
    "My consciousness starts to fade as I hear the Spider Girl sucking up my body with loud slurping noises."
    "Being made into the Spider Girl's nourishment, I feel happy at being able to be one with her."
    "Filled with ecstasy to the very end, the last of my body is sucked up by the Spider Girl."
    world.battle.enemy[0].name "Mmm... *Slurp*"
    "Sucked to the last drop, I've become nothing but nutrients for the Spider Girl."

    stop hseanvoice fadeout 1
    show kumo hb4 with Dissolve(1.5)

    world.battle.enemy[0].name "Haha... Thanks for the meal."
    "Having finished devouring her prey, the Spider Girl laughs."

    scene bg black with Dissolve(3.0)

    "........."

    jump badend

label nanabi_start:
    python:
        world.troops[15] = Troop(world)
        world.troops[15].enemy[0] = Enemy(world)

        world.troops[15].enemy[0].name = Character("Нанаби")
        world.troops[15].enemy[0].sprite = ["nanabi st01", "nanabi st02", "nanabi st03", "nanabi st02"]
        world.troops[15].enemy[0].position = center
        world.troops[15].enemy[0].defense = 100
        world.troops[15].enemy[0].evasion = 95
        world.troops[15].enemy[0].max_life = world.troops[15].battle.difficulty(2900, 3000, 3100)
        world.troops[15].enemy[0].power = world.troops[15].battle.difficulty(0, 1, 2)

        world.troops[15].battle.tag = "nanabi"
        world.troops[15].battle.name = world.troops[15].enemy[0].name
        world.troops[15].battle.background = "bg 034"
        world.troops[15].battle.music = 2
        world.troops[15].battle.exp = 1100
        world.troops[15].battle.exit = "lb_0031"

        world.troops[15].battle.init(0)

    l "Хорошо.{w}\nЭта битва не должна быть трудной!"
    "Я уже отделал её в прошлый раз.{w}\n... Правда, не помню как именно."
    "И всё же, это должно быть легко!"

    $ world.battle.main()

label nanabi_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label nanabi_v:
    world.battle.enemy[0].name "Невозможно...{w} Это..."

    play sound "se/syometu.ogg"
    hide nanabi with crash

    "Нанаби превращается в обычную лису!"

    $ world.nanabi_fight = True
    $ world.battle.victory(1)

label nanabi_a:
    if world.party.player.bind == 1:
        call nanabi_a6
        $ world.battle.main()

    if world.battle.progress == 3 and world.battle.stage[2] == 0:
        call nanabi_x1
        $ world.battle.main()
    elif world.battle.progress == 3 and world.battle.stage[2] == 1:
        call nanabi_x2
        $ world.battle.main()
    elif world.battle.progress == 3 and world.battle.stage[2] == 2:
        call nanabi_x3
        $ world.battle.main()
    elif world.battle.progress == 3 and world.battle.stage[2] == 3:
        call nanabi_x4
        jump nanabi_v

    if world.party.player.surrendered and world.battle.stage[1] > 0:
        call nanabi_x4

    if not world.party.player.surrendered:
        $ world.battle.stage[1] += 1

        if world.battle.stage[1] == 4:
            call nanabi_x0
            $ world.battle.main()
        elif world.battle.stage[2] == 1:
            call nanabi_x2
            $ world.battle.main()
        elif world.battle.stage[2] == 2:
            call nanabi_x3
            $ world.battle.main()
        elif world.battle.stage[2] == 3:
            call nanabi_x4
            $ world.battle.main()
        elif world.battle.stage[1] == 6:
            call nanabi_a0a
            $ world.battle.main()
        elif world.battle.stage[1] == 7:
            call nanabi_a0b
            $ world.battle.main()

    while True:
        $ random = rand().randint(1,5)

        if random == 1:
            call nanabi_a1
            $ world.battle.main()
        elif random == 2:
            call nanabi_a3
            $ world.battle.main()
        elif random == 3:
            call nanabi_a4
            $ world.battle.main()
        elif random == 4 and world.battle.progress > 1 and world.battle.delay[0] > 5:
            call nanabi_a2
            $ world.battle.main()
        elif random == 5 and world.battle.progress > 1 and world.battle.delay[1] > 5:
            call nanabi_a5
            $ world.battle.main()


label nanabi_a0a:
    world.battle.enemy[0].name "Без лунного света мне необходимо немного времени на восстановление магии..."
    "Нанаби отдыхает!"

    return

label nanabi_a0b:
    world.battle.enemy[0].name "А теперь... продолжим?!"
    "Нанаби отдохнула!"

    return

label nanabi_a1:
    $ world.battle.skillcount(1, 3065)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я доставлю тебе удовольствие своим хвостом...",
        "Как тебе мой хвост на ощупь?",
        "Наслаждайся моим хвостом..."
    ])

    $ world.battle.show_skillname("Массаж хвостом")
    play sound "audio/se/umaru.ogg"
    show nanabi cta01 at topright zorder 15 as ct
    show nanabi cta02 at topright zorder 15 as ct

    "Хвост Нанаби нежно трётся о пах Луки!"

    python:
        world.battle.enemy[0].damage = {1: (14, 18), 2: (19, 22), 3: (23, 29)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        renpy.hide("ct")

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label nanabi_a2:
    $ world.battle.skillcount(1, 3067)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я заставлю тебя кончить от этого...",
        "Я немного подразню тебя этим...",
        "Аха-ха, сможешь ли ты выдержать это?"
    ])

    if random == 1:
        world.battle.enemy[0].name "I'll make you come with this..."
    elif random == 2:
        world.battle.enemy[0].name "I'll tease you a little with this..."
    elif random == 3:
        world.battle.enemy[0].name "Haha, can you endure it?"

    $ world.battle.show_skillname("Две луны")

    "Два хвоста направляются к Луке!"

    play sound "audio/se/umaru.ogg"
    show nanabi cta01 at topright zorder 15 as ct
    show nanabi cta02 at topright zorder 15 as ct

    "Пушистый хвост обвился вокруг члена Луки!"

    python:
        world.battle.enemy[0].damage = (14, 18)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage)

    "Другой хвост начал слегка тереться о член Луки!"

    python:
        world.battle.enemy[0].damage = (14, 18)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage)

        world.battle.hide_skillname()

        renpy.hide("ct")

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label nanabi_a3:
    $ world.battle.skillcount(1, 3063)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я подержу его в своём рту...",
        "*Лижет* *Сосёт*",
        "Спусти всё в мой рот..."
    ])

    $ world.battle.show_skillname("Лисий минет")
    play sound "audio/se/ero_buchu2.ogg"

    "Нанаби нежно сосёт член Луки!"

    python:
        world.battle.enemy[0].damage = {1: (12, 15), 2: (15, 17), 3: (21, 25)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label nanabi_a4:
    $ world.battle.skillcount(1, 3064)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я вложу его сюда...",
        "Моя грудь очень мягкая...",
        "Выплесни своё семя на мою грудь..."
    ])

    $ world.battle.show_skillname("Лисье пайзури")
    play sound "audio/se/ero_koki1.ogg"

    "Нанаби зажимает член Луки между своих сисек!"

    python:
        world.battle.enemy[0].damage = {1: (13, 16), 2: (16, 19), 3: (23, 27)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label nanabi_a5:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Должна ли я пленить тебя своим хвостом?.."

    $ world.battle.show_skillname("Опутывание хвостом")
    play sound "audio/se/umaru.ogg"

    "Хвост Нанаби обвивается вокруг тела Луки!"

    $ world.battle.enemy[0].damage = {1: (14, 18), 2: (18, 22), 3: (25, 31)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Лука схвачен хвостами Нанаби!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h5")

    $ world.battle.face(2)

    world.battle.enemy[0].name "Shall I play with you slowly?{w}\nUntil you let it all out... Fufu."

    if persistent.difficulty == 1:
        $ world.battle.enemy[0].power = 1
    elif persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 1
    elif persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 2

    return

label nanabi_a6:
    $ world.battle.skillcount(1, 3066)

    "Лука схвачен хвостами Нанаби!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Должна ли я затянуть их туже?..",
        "Как тебе ощущения от моего хвоста?",
        "Я доставлю тебе удовольствие...",
        "Просто выплесни всё на них...",
        "Уфу-фу..."
    ])

    $ world.battle.show_skillname("Массаж хвостами")
    play sound "audio/se/umaru.ogg"
    show nanabi cta02 at topright zorder 15 as ct

    $ world.battle.cry(narrator, [
        "Хвост Нанаби стягивается вокруг члена Луки!",
        "Хвост Нанаби массирует Луку!",
        "Кончик хвоста Нанаби стимулирует Луку!",
        "Хвост Нанаби нежно поглаживает Луку",
        "Все семь хвостов опутывают член Луки, глядя его внутри!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (14, 18), 2: (18, 22), 3: (25, 31)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage)

        world.battle.hide_skillname()

        renpy.hide("ct")

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h6")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label nanabi_x0:
    world.battle.enemy[0].name "Что ж... видимо стоит стать серьёзной?"

    $ world.battle.show_skillname("Концентрация магии")
    play sound "audio/se/power.ogg"
    $ world.battle.enemy[0].sprite[0] = "nanabi st11"
    $ world.battle.enemy[0].sprite[1] = "nanabi st11"
    $ world.battle.enemy[0].sprite[2] = "nanabi st11"
    show nanabi st11

    "Нанаби начинает концентрировать свою магическую силу!"

    $ world.battle.hide_skillname()
    $ world.battle.stage[2] = 1
    return

label nanabi_x1:
    world.battle.enemy[0].name "Заставляешь меня это использовать дважды!.."

    $ world.battle.show_skillname("Концентрация магии")
    show nanabi st11
    play sound "audio/se/power.ogg"
    $ world.battle.enemy[0].sprite[0] = "nanabi st11"
    $ world.battle.enemy[0].sprite[1] = "nanabi st11"
    $ world.battle.enemy[0].sprite[2] = "nanabi st11"

    "Нанаби начинает концентрировать свою магическую силу!"

    $ world.battle.hide_skillname()
    $ world.battle.stage[2] = 1
    return

label nanabi_x2:
    world.battle.enemy[0].name "........."

    $ world.battle.show_skillname("Концентрация магии")
    play sound "audio/se/power.ogg"

    "Нанаби концентрирует свою магическую силу!"

    $ world.battle.hide_skillname()
    $ world.battle.stage[2] = 2
    return

label nanabi_x3:
    world.battle.enemy[0].name "........"

    $ world.battle.show_skillname("Концентрация магии")
    play sound "audio/se/power.ogg"

    "Нанаби заканчивает концентрировать свою магическую силу!"

    $ world.battle.hide_skillname()

    world.battle.enemy[0].name "А теперь... приготовься!"

    $ world.battle.stage[2] = 3
    return

label nanabi_x4:
    $ world.battle.skillcount(1, 3068)

    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Сойди с ума от удовольствия!"

    $ world.battle.show_skillname("Семь лун")
    play sound "audio/se/umaru.ogg"
    show nanabi ctb01 at topright zorder 15 as ct

    "Все семь хвостов Нанаби направляются в сторону Луки!"

    show nanabi ctb02 at topright zorder 15 as ct

    "Первый хвост опутывает член Луки!"

    $ world.battle.enemy[0].damage = (24, 28)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    play sound "audio/se/umaru.ogg"

    "Второй хвост щекочет головку члена!"

    $ world.battle.enemy[0].damage = (24, 28)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    play sound "audio/se/umaru.ogg"

    "Третий хвост !"

    $ world.battle.enemy[0].damage = (24, 28)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    play sound "audio/se/umaru.ogg"

    "Четвёртый хвост играет с яичками Луки!"

    $ world.battle.enemy[0].damage = (24, 28)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    play sound "audio/se/umaru.ogg"

    "Пятый хвост сжимает основание члена!"

    $ world.battle.enemy[0].damage = (24, 28)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    play sound "audio/se/umaru.ogg"

    "Шестой хвост щекочет анус Луки!"

    $ world.battle.enemy[0].damage = (24, 28)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)
    play sound "audio/se/umaru.ogg"

    "Седьмой хвост стягивается вокруг члена Луки!"

    $ world.battle.enemy[0].damage = (24, 28)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage)

    $ world.battle.hide_skillname()

    $ world.battle.enemy[0].sprite[0] = "nanabi st01"
    $ world.battle.enemy[0].sprite[1] = "nanabi st02"
    $ world.battle.enemy[0].sprite[2] = "nanabi st03"
    $ world.battle.face(1)

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h7")

    hide ct

    if world.battle.progress == 3:
        return

    world.battle.enemy[0].name "Тебе удалось пережить это...{w}\nТы весьма крепкий для человека, уфу-фу..."

    $ world.battle.stage[1] = 5
    $ world.battle.stage[2] = 0
    $ world.battle.progress = 2
    return

label nanabi_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "... Сдаёшься?{w}\nТогда я вдоволь наиграюсь с тобой своими хвостами..."

    $ world.battle.stage[1] = 8
    $ world.battle.progress = 2
    $ world.battle.enemy[0].attack()

label nanabi_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Fox Blowjob"
    $ list2 = "Fox Tit Fuck"
    $ list3 = "Tail Massage"
    $ list4 = "Bound Tail Massage"
    $ list5 = "Two Moons"
    $ list6 = "Seven Moons"

    if persistent.skills[3063][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3064][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3065][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3066][0] > 0:
        $ list4_unlock = 1

    if persistent.skills[3067][0] > 0:
        $ list5_unlock = 1

    if persistent.skills[3068][0] > 0:
        $ list6_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump nanabi_onedari1
    elif world.battle.result == 2:
        jump nanabi_onedari2
    elif world.battle.result == 3:
        jump nanabi_onedari3
    elif world.battle.result == 4 and world.party.player.bind == 0:
        jump nanabi_onedari4
    elif world.battle.result == 4 and world.party.player.bind == 1:
        jump nanabi_onedari5
    elif world.battle.result == 5:
        jump nanabi_onedari6
    elif world.battle.result == 6:
        jump nanabi_onedari7
    elif world.battle.result == -1:
        jump nanabi_main


label nanabi_onedari1:
    $ world.battle.enemy[0].sprite[0] = "nanabi st01"
    $ world.battle.enemy[0].sprite[1] = "nanabi st02"
    $ world.battle.enemy[0].sprite[2] = "nanabi st03"
    call onedari_syori

    world.battle.enemy[0].name "You want to yield to my mouth...?{w}\nThen I won't hold back..."

    while True:
        call nanabi_a3


label nanabi_onedari2:
    $ world.battle.enemy[0].sprite[0] = "nanabi st01"
    $ world.battle.enemy[0].sprite[1] = "nanabi st02"
    $ world.battle.enemy[0].sprite[2] = "nanabi st03"
    call onedari_syori

    world.battle.enemy[0].name "You want to yield to my chest...?{w}\nThen I won't hold back..."

    while True:
        call nanabi_a4


label nanabi_onedari3:
    $ world.battle.enemy[0].sprite[0] = "nanabi st01"
    $ world.battle.enemy[0].sprite[1] = "nanabi st02"
    $ world.battle.enemy[0].sprite[2] = "nanabi st03"
    call onedari_syori

    world.battle.enemy[0].name "You want to yield to my tail...?{w}\nThen I won't hold back..."

    while True:
        call nanabi_a1


label nanabi_onedari4:
    $ world.battle.enemy[0].sprite[0] = "nanabi st01"
    $ world.battle.enemy[0].sprite[1] = "nanabi st02"
    $ world.battle.enemy[0].sprite[2] = "nanabi st03"
    call onedari_syori

    world.battle.enemy[0].name "You want to yield to my tail...?{w}\nThen first, I'll deprive you of your freedom..."

    call nanabi_a5

    while True:
        call nanabi_a6


label nanabi_onedari5:
    $ world.battle.enemy[0].sprite[0] = "nanabi st01"
    $ world.battle.enemy[0].sprite[1] = "nanabi st02"
    $ world.battle.enemy[0].sprite[2] = "nanabi st03"
    call onedari_syori

    world.battle.enemy[0].name "You want to yield to my tail...?{w}\nThen I'll continue like this..."

    while True:
        call nanabi_a6


label nanabi_onedari6:
    $ world.battle.enemy[0].sprite[0] = "nanabi st01"
    $ world.battle.enemy[0].sprite[1] = "nanabi st02"
    $ world.battle.enemy[0].sprite[2] = "nanabi st03"
    call onedari_syori

    world.battle.enemy[0].name "You want to take this technique?{w}\nThen I'll play with you until you're satisfied..."

    while True:
        call nanabi_a2


label nanabi_onedari7:
    call onedari_syori

    world.battle.enemy[0].name "So you want to surrender to my ultimate attack?{w}\nI'll force you to come in humiliation, then..."

    $ world.battle.show_skillname("Концентрация магии")
    play sound "audio/se/power.ogg"
    show nanabi st11

    "Nanabi starts to accumulate magic!"
    "Nanabi continues to accumulate magic!"
    "Nanabi finishes accumulating magic!"

    $ world.battle.hide_skillname()

    while True:
        call nanabi_x4


label nanabi_h1:
    $ world.party.player.moans(1)
    with syasei1
    show nanabi cta03 at topright zorder 15 as ct
    show nanabi bk05 zorder 10 as bk
    $ world.ejaculation()

    "Caressed by the fluffy tail, Luka was forced to ejaculate."

    $ world.battle.lose()

    $ world.battle.count([19, 1])
    $ bad1 = "Luka succumbs to the Nanabi's tail."
    $ bad2 = "Luka was forced to continue being the Nanabi's mate..."
    hide ct
    $ world.battle.face(2)

    world.battle.enemy[0].name "Did my tail feel good...?{w}\nYou made my tail so dirty with your semen..."

    jump nanabi_hb


label nanabi_h2:
    $ world.party.player.moans(1)
    with syasei1
    show nanabi cta03 at topright zorder 15 as ct
    show nanabi bk05 zorder 10 as bk
    show nanabi bk06 zorder 10 as bk
    $ world.ejaculation()

    "Caressed by the two fluffy tails, Luka was forced to ejaculate."

    $ world.battle.lose()

    $ world.battle.count([19, 1])
    $ bad1 = "Luka succumbs to the Nanabi's two moons technique."
    $ bad2 = "Luka was forced to continue being the Nanabi's mate..."
    hide ct
    $ world.battle.face(2)

    world.battle.enemy[0].name "Did my technique feel good...?{w}\nYou made my tails so dirty with your semen..."

    jump nanabi_hb


label nanabi_h3:
    $ world.party.player.moans(1)
    with syasei1
    show nanabi bk01 zorder 10 as bk
    $ world.ejaculation()

    "Sucked by the Nanabi, Luka succumbs to the pleasure, ejaculating in her mouth."

    $ world.battle.lose()

    $ world.battle.count([4, 1])
    $ bad1 = "Luka succumbs to the Nanabi's blowjob."
    $ bad2 = "Luka was forced to continue being the Nanabi's mate..."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Was my blowjob technique amazing?{w}\nYou let out so much semen in my mouth..."

    jump nanabi_hb


label nanabi_h4:
    $ world.party.player.moans(1)
    with syasei1
    show nanabi bk02 zorder 10 as bk
    $ world.ejaculation()

    "Caressed between the Nanabi's breasts, Luka was forced to ejaculate."

    $ world.battle.lose()

    $ world.battle.count([7, 1])
    $ bad1 = "Luka succumbs to the Nanabi's titty fuck."
    $ bad2 = "Luka was forced to continue being the Nanabi's mate..."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Did my chest feel good...?{w}\nYou made my chest so dirty with your semen..."

    jump nanabi_hb


label nanabi_h5:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Eh...?"

    with syasei1
    show nanabi bk05 zorder 10 as bk
    $ world.ejaculation()

    world.battle.enemy[0].name "Being bound by her tails, Luka accidentally ejaculates!"

    $ world.battle.lose()

    $ world.battle.count([19, 1])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka accidentally ejaculates while still being bound."
    $ bad2 = "Luka was forced to continue being the Nanabi's mate..."
    $ world.battle.face(2)

    world.battle.enemy[0].name "What a pathetic man...{w}\nOnly being bound and letting out so much... I didn't even start my technique! "

    jump nanabi_hb


label nanabi_h6:
    $ world.party.player.moans(1)
    with syasei1
    show nanabi cta03 at topright zorder 15 as ct
    show nanabi bk05 zorder 10 as bk
    $ world.ejaculation()

    "Bound by her tails, Luka was forced to ejaculate!"

    $ world.battle.lose()

    $ world.battle.count([19, 1])
    $ bad1 = "Bound by Nanabi's tails, Luka was forced to ejaculate."
    $ bad2 = "Luka was forced to continue being the Nanabi's mate..."
    hide ct
    $ world.battle.face(2)

    world.battle.enemy[0].name "Fufu... Was it amazing?{w}\nYou made my tails so dirty with your semen..."

    jump nanabi_hb


label nanabi_h7:
    $ world.party.player.moans(2)
    with syasei1
    show nanabi bk05 zorder 10 as bk
    show nanabi bk06 zorder 10 as bk2
    show nanabi bk07 zorder 10 as bk3
    show nanabi ctb03 at topright zorder 15 as ct
    $ world.ejaculation()

    "Stimulated all over by her seven tails, Luka ejaculates!"

    $ world.battle.lose()

    $ world.battle.count([19, 3])
    $ bad1 = "Luka succumbs to the Nanabi's seven moons technique!"
    $ bad2 = "Captured by Nanabi, Luka was made into a livestock for semen."
    $ world.battle.enemy[0].sprite[0] = "nanabi st01"
    $ world.battle.enemy[0].sprite[1] = "nanabi st02"
    $ world.battle.enemy[0].sprite[2] = "nanabi st03"
    $ world.battle.enemy[0].sprite[3] = "nanabi st02"
    hide ct
    $ world.battle.face(2)

    world.battle.enemy[0].name "Fufu... Instant death.{w}\nWere you happy to be defeated by my ultimate attack?"

    jump nanabi_ha


label nanabi_ha:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut


    l "Auuu..."
    "Basking in the afterglow, Nanabi looks down at me with an air of superiority."
    "Defeated, I'm unable to resist anymore."
    world.battle.enemy[0].name "Since I used my best move, I'm a little tired...{w}\nHow about you assist me in restoring my energy?"

    hide ct
    hide bk
    hide bk2
    hide bk3
    show nanabi st21


    "Nanabi's tails change into a strange shape."
    "A strange, pink looking mouth opens up at the tip of each tail.{w}\nA slimy looking tongue is inside each, much longer than a humans."
    l "Aaa..."
    world.battle.enemy[0].name "I'll suck out all of your semen with my tails...{w}\nUntil you fill all seven, my energy won't be restored..."
    l "No... Auuu..."
    "Nanabi grabs me with one of her tails and pulls me closer to her, and...{w}{nw}"

    play hseanwave "audio/se/hsean15_kyuin2.ogg"
    show nanabi h1
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nHer pink opening sucks in my penis!"
    l "Aaa! Ahi!"
    "Her warm insides shoot pleasure through me as soon as I'm sucked into her tail opening."
    "The folds and creases inside of her rub me with every movement, as the slimy tongue inside licks me all over."
    l "Ahaa! Haaa..."
    world.battle.enemy[0].name "Fufu... Amazing, right?{w}\nJust shoot everything out inside..."
    l "Aaaa!"
    "Stuck inside of her from the tip to the base, she forcefully rubs my penis all over."
    "In addition, her slimy tongue inside the tail is gently wrapped around me, licking me gently."
    "To such overpowering double stimulation, I'm unable to resist the rising orgasm."
    l "Coming!{w} Ahhh!"

    with syasei1
    show nanabi h2
    $ world.ejaculation()

    "The overpowering pleasure quickly forces me to orgasm, as I gush out semen inside her tail."
    l "Ah.. I came so... Stop..."
    "The tongue licks my urethral opening, licking out the last of semen as my body twitches."
    world.battle.enemy[0].name "Fufu... Did it feel good?"

    stop hseanwave fadeout 1

    "She pulls her tail back, finally releasing me...{w}{nw}"

    play hseanwave "audio/se/hsean15_kyuin2.ogg"

    extend "\n...Or so I thought.{w} Another tail suddenly sucks me back up!"
    l "Auuu!"
    world.battle.enemy[0].name "I told you, remember?{w}\nYou have to let all seven of my tails get a turn."
    world.battle.enemy[0].name "Doesn't this one feel different from the last?"
    l "Aaaa...."
    "This tail had lots of little creases all over her tight walls."
    "Just from insertion into all of the little bumps and creases left me short of breath."
    l "Auu... Like this, I'll come again..."
    "While reeling from the stimulation from the creases in her, I let out a pathetic moan."
    world.battle.enemy[0].name "Arara... Already coming again?{w}\nHow pathetic."
    world.battle.enemy[0].name "Then I'll make you come quickly... Just let it out!"
    "Nanabi tightens up, forcing the creases to wrap tightly around my penis, violently rubbing me with every movement."
    "The intense pleasure forces me to the limit instantly."
    l "Aaaa!"

    with syasei1
    show nanabi h3
    $ world.ejaculation()

    "Unable to endure, I explode in the second tail, filling it with my semen."
    "As soon as the last drop is out, she quickly pulls me out of the tail and puts me in another."

    play hseanwave "audio/se/hsean16_kyuin3.ogg"

    "This tail is even tighter than the last..."
    l "Ahii!"
    "Stuck in her tight opening, I writhe pitifully.{w}\nUnable to last even a short duration, I feel pathetic."
    world.battle.enemy[0].name "There's no point in resisting...{w}\nJust surrender to the pleasure..."
    l "Aaaa!"

    with syasei1
    $ world.ejaculation()

    "Quickly forced to ejaculate, she exchanges the tail again after squeezing out the last drop."
    "It looks like she'll really rape me with all seven..."
    l "Auu... Stop already..."
    world.battle.enemy[0].name "Do you think I'd really let an excellent catch like you get away?{w}\nI'll squeeze every drop out of you until I'm satisfied."
    world.battle.enemy[0].name "After that... Shall I keep you as a semen slave?"
    l "That... Auuu..."
    "As I complain, she forcibly stimulates me with a grin on her face."

    with syasei1
    show nanabi h4
    $ world.ejaculation()

    "I'm quickly forced to ejaculate again."
    "My entire lower body feels as if it's melting in pleasure."
    world.battle.enemy[0].name "There are still more tails to go..."
    l "Auu..."
    "One after another, the tails keep violating me.{w}\nRaped by her tails, she continues to squeeze out my semen."
    l "A...aa..."
    "My whole body too weak to do anything, my consciousness starts to slowly fade."
    "A pleasurable fatigue washes over me as I'm unable to fight back."
    world.battle.enemy[0].name "Fufu... You're not used to being squeezed...{w}\nFainting with just this much..."
    world.battle.enemy[0].name "Don't worry, I'll train you in the future.{w}\nFrom now on, you're just my livestock for semen production."
    l "........."
    "As Nanabi whispers that in my ear, I lose consciousness."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Thus, my adventure ends, defeated by a powerful world.battle.enemy[0].{w}\nAfter this, I'll live out the rest of my life as her livestock, producing semen..."

    jump badend


label nanabi_hb:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut


    l "Auu..."
    "Basking in the afterglow, Nanabi looks down at me with an air of superiority."
    "Defeated, I'm unable to resist anymore."
    world.battle.enemy[0].name "You're as strong as your reputation claimed...{w}\nI wouldn't complain in having such a good partner..."
    world.battle.enemy[0].name "Fufu... I'll rape you.{w}\nWill you shoot your seed into me?"
    l "Auu... Stop..."

    hide ct
    hide bk
    hide bk2
    hide bk3
    show nanabi h5


    "As I try to refuse, Nanabi jumps on top of me, forcing me into the ground."
    "Her warm, fluffy fur rubs me all over my body."
    "Inside her fur, a pink opening moves towards my groin.{w}\nShe slowly forces my dick into her pussy..."
    l "Aaaa!"

    show nanabi ctc01 at topleft zorder 15 as ct
    show nanabi h6
    play hseanwave "audio/se/hsean09_innerworks_b1.ogg"

    world.battle.enemy[0].name "Fufu... I violated you."
    "Wrapped in her warm opening, the rough walls rub against me as I'm pushed all the way into her."
    "Covered in bumps along her walls, her pussy sends intense pleasure through me as I'm pushed into her."
    l "Aaaa..."
    "Her amazing pussy starts to tighten around me, as the ecstasy flowing through me feels like it's melting my body."
    world.battle.enemy[0].name "Mating with me feels good, doesn't it?"
    "Nanabi wiggles slightly as she questions me."
    "The movements force my penis to rub all around inside of her, massaging all of my most sensitive parts."
    l "Auuu... It's so good..."
    "The feeling would quickly force any man into submission.{w}\nFeeling that sweet stimulus, I'm quickly brought to the edge."
    l "Aahh... I'm going to come already..."
    world.battle.enemy[0].name "I don't mind, let it all out...{w}\nPut all of your seed into my vagina..."
    "Nanabi starts to move her hips more than before.{w}\nRubbed inside of her, I'm quickly brought to my limit."
    "The desire to ejaculate rises in me, until I succumb to it."
    l "Aaa...!{w} Ahhhh!"

    with syasei1
    show nanabi ctc02 at topleft zorder 15 as ct
    show nanabi h7
    $ world.ejaculation()

    "Forced deep into her, my semen gushes out into her."
    "Suddenly, her pussy starts to contract quickly, squeezing and sucking up everything I shoot out."
    l "Ahii!{w} What... Is this...?"
    "My body writhes in reaction to the sudden strange pleasure."
    world.battle.enemy[0].name "Fufu... It's to quickly suck your sperm into my womb.{w}\nDid you enjoy it?"
    world.battle.enemy[0].name "Using that, I'm able to ensure I squeeze out all of your sperm, and the best one fertilizes me..."
    l "Aa! Haaa!"

    play hseanwave "audio/se/hsean10_innerworks_b2.ogg"

    "Nanabi's pussy continues to contract in waves, as if milking everything out of me."
    "Like she said, it doesn't look like she'll leave a single drop..."
    l "Faa! Haaa...."
    "Even after her milking sucks out the last drop, the contractions don't stop."
    "Her milking continues, as my body writhes in futility."
    l "Hauuu! Stop already!"
    world.battle.enemy[0].name "I said I'd squeeze out all your sperm, didn't I?{w}\nUntil your scrotum is empty, I need to continue..."
    l "Faaa!"
    "Her relentless contractions milk me, as if encouraging me to ejaculate quickly again."
    "Even though I just came, I already can't endure the rising desire."
    world.battle.enemy[0].name "Now, let it out.{w}\nSend all of your healthy seed into my womb..."
    l "Aaaa... Coming!{w}\nAaaaaa!"

    with syasei1
    show nanabi ctc03 at topleft zorder 15 as ct
    show nanabi h8
    $ world.ejaculation()

    "I come again inside of her, as her contractions continue even stronger than before, sucking up everything."
    l "Stop... Please..."
    world.battle.enemy[0].name "You can come more, can't you?{w}\nI said I'd get everything..."
    l "Faaaa!"
    "She suddenly tightens her pussy around me, shooting me over the edge again."

    with syasei1
    show nanabi ctc04 at topleft zorder 15 as ct
    show nanabi h9
    $ world.ejaculation()

    "Unable to endure at all, I helplessly shoot out in her again."
    "Her milking continues as I come, ensuring not a single drop is wasted."
    l "Ahi... Ha..."
    world.battle.enemy[0].name "Fufu... Mating with you is very nice...{w}\nYour energetic little children are swimming so lively in me..."
    l "Hau...."

    with syasei1
    $ world.ejaculation()

    "Continuing to wring me out as she talks, I'm forced to come again."
    "Unable to do anything about the tortuous pleasure, my consciousness starts to slowly fade."
    l "Au...."
    world.battle.enemy[0].name "Fainting while still mating with me?{w}\nYou're still pretty weak..."
    world.battle.enemy[0].name "I'll train you plenty from now on.{w}\nYou're going to be my mate after all... Fufu."
    l "Aaa...."

    with syasei1
    $ world.ejaculation()

    "I faint at the same time as I ejaculate, unable to endure the overwhelming pleasure."
    "Thinking that being her mate might not be too bad, I lose consciousness."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Thus my adventure ends here, defeated by a powerful world.battle.enemy[0].{w}\nFrom now, my new life as Nanabi's mate begins."
    ".........."

    jump badend

label tamamo_start:
    python:
        world.troops[16] = Troop(world)
        world.troops[16].enemy[0] = Enemy(world)

        world.troops[16].enemy[0].name = Character("Тамамо")
        world.troops[16].enemy[0].sprite = ["tamamo st11", "tamamo st12", "tamamo st13", "tamamo st11"]
        world.troops[16].enemy[0].position = center
        world.troops[16].enemy[0].defense = 100
        world.troops[16].enemy[0].evasion = 90
        world.troops[16].enemy[0].max_life = world.troops[16].battle.difficulty(22000, 27000, 33000)
        world.troops[16].enemy[0].henkahp[0] = world.troops[16].enemy[0].max_life / 2
        world.troops[16].enemy[0].henkahp[1] = 5000
        world.troops[16].enemy[0].power = world.troops[16].battle.difficulty(0, 1, 2)

        world.troops[16].battle.tag = "tamamo"
        world.troops[16].battle.name = world.troops[16].enemy[0].name
        world.troops[16].battle.background = "bg 035"
        world.troops[16].battle.music = 4
        world.troops[16].battle.ng = True

        world.troops[16].battle.init(0)

    $ world.party.player.skill_set(2)
    pause 0.05
    play sound "se/escape.ogg"

    pause .5
    t "Тогда хорошо.{w}\nДавай посмотрим на что ты способен!"
    "Вот оно...{w}\nЕсли я сражу её, тогда останется лишь два Небесных Рыцаря!"
    l "Погнали!"

    $ world.battle.main()

label tamamo_main:
    python:
        if world.battle.enemy[0].life < world.battle.enemy[0].henkahp[0]:
            renpy.jump(f"{world.battle.tag}_v")

        world.battle.call_cmd()

        if world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label tamamo_v:
    show tamamo st15
    t "Уаа!"
    "Тамамо пятится назад, не выдержав моего напора."
    show tamamo st11
    t "... Ты что-то с чем-то, знаешь?"
    t "Но я думаю на сегодня всё.{w}\nТы выиграл этот раунд, малыш."
    l "...{w} Что?"
    "Она вот так просто сдаётся?"
    "Нет...{w}\nЧтобы заставить её следовать моим идеалам, я должен доказать свою силу!"
    l "Это ведь была твоя не полная сила, не так ли?"
    t "Ммм?"
    l "Ты знаешь, о чём я говорю."
    l "Моя победа над тобой ничего не значит, когда ты в запечатанной форме!{w}\nПокажи мне свою истинную силу, Тамамо!"
    show tamamo st16
    t "Ох...{w}\nТы хочешь увидеть ЭТУ силу?"
    "Тамамо тихо посмеивается, будто насмехается надо мной."
    t "Я не пощажу тебя, даже если ты об этом попросишь.{w}\nТы уверен, что это то, чего ты хочешь, Лука?"
    l "Именно так."
    "Я ещё никогда не побеждал её истинную форму.{w}\nНо я не хочу победы, которая ничего для неё не значит.{w}\nСитуация прямо как когда я дрался с Альмой в замке."
    stop music fadeout 1.0
    show tamamo st14
    t "Окей. Погналииии~{image=note}!"
    "....................."
    play sound "se/power.ogg"
    "Тамамо концентрирует свою магию!"
    play sound "se/flash.ogg"
    scene bg white
    "..."
    show bg 035
    show tamamo st62

    $ world.party.player_state = world.party.player

    jump tamamo_ng_start

label tamamo_a:
    if world.party.player.bind == 2:
        call tamamo_a8
        $ world.battle.main()

    while True:
        $ random = rand().randint(1, 8)

        if random == 1:
            call tamamo_a1
            $ world.battle.main()
        if random == 2:
            call tamamo_a2
            $ world.battle.main()
        if random == 3:
            call tamamo_a3
            $ world.battle.main()
        if random == 4:
            call tamamo_a4
            $ world.battle.main()
        if random == 5 and world.party.player.status == 0:
            call tamamo_a5
            $ world.battle.main()
        if random == 6:
            call tamamo_a6
            $ world.battle.main()
        if random == 7:
            call tamamo_a7
            $ world.battle.main()
        if random == 8:
            call tamamo_a9
            $ world.battle.main()

label tamamo_a1:
    $ world.battle.skillcount(2, 3641)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ты полюбишь мою руку.",
        "Приятнее всего, когда я ласкаю головку, правда ведь?",
        "Я пощекочу твою головку."
    ])

    $ world.battle.show_skillname("Мастурбация кицунэ")
    play sound "audio/se/ero_koki1.ogg"

    "Маленькие ручки Тамамо проворно и ловко ласкают член Луки"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(40, 30, 20):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (370, 400), 2: (400, 430), 3: (430, 460)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label tamamo_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я буду сосать его столько, сколько захочу{image=note}.",
        "Не хочешь кончить в мой ротик? Хи-хи...",
        "Хи-хи... Думаешь, что сможешь стерпеть то, как я ласкаю тебя своим ротиком, м?"
    ])

    $ world.battle.show_skillname("Минет Кицунэ")
    play sound "audio/se/ero_buchu3.ogg"

    "Тамамо с похотливой жадностью сосёт весь член Луки своим маленьким ртом!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(40, 30, 20):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (400, 420), 2: (430, 450), 3: (460, 480)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
            world.party.player.wind_guard()
        else:
            renpy.show("tamamo ha2")
            renpy.show("tamamo ha4")
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        world.battle.face(1)

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label tamamo_a3:
    $ world.battle.skillcount(2, 3643)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Пусть у меня маленькая грудь... но я знаю, как ей пользоваться.",
        "Я хотя бы могу прижимать твою чувствительную головку к моим сосочкам...",
        "В мире полно людей, предпочитающих маленькие груди{image=note}."
    ])

    $ world.battle.show_skillname("Потирание Плоской Грудью")
    play sound "audio/se/ero_koki1.ogg"

    "Тамамо потирает член Луки о свою плоскую грудь!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(40, 30, 20):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (380, 400), 2: (410, 430), 3: (440, 460)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label tamamo_a4:
    $ world.battle.skillcount(2, 3644)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Некоторые люди просто обожают, когда над ними доминируют{image=note}.",
        "Ты меня сильно разочаруешь, если кончишь от этого...",
        "Я буду нежно растаптывать весь твой член..."
    ])

    $ world.battle.show_skillname("Растирание ногой Луноборца")
    play sound "audio/se/ero_koki1.ogg"

    "Тамамо наступает на член Луки своей маленькой ножкой!{w}{nw}"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(40, 30, 20):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (180, 200), 2: (190, 210), 3: (200, 220)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
            world.party.player.wind_guard()
            renpy.return_statement()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)

        if not world.party.player.life:
            renpy.jump("badend_h")

    play sound "audio/se/ero_koki1.ogg"

    "Прижимая к нему свою ступню, она трётся своей маленькой ножкой о его член!{w}{nw}"

    python:
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=2)

        if not world.party.player.life:
            renpy.jump("badend_h")

    play sound "audio/se/ero_koki1.ogg"

    "Удерживая своим хвостом равновесие, она сдавливает член Луки между обеими своими ножками!{w}{nw}"

    python:
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label tamamo_a5:
    $ world.battle.skillcount(2, 3645)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Твоё личико так и манит его облизать его...",
        "Мой язычок может пленить любого мужчину...",
        "Мой язычок может завладеть разумом любого мужчины{image=note}."
    ])

    $ world.battle.show_skillname("Обессиливающая забава")
    play sound "audio/se/ero_pyu1.ogg"

    "Тамамо прыгает на Луку и крепко его обнимает!{w}\nИ на этом она начинает вылизывать всё его лицо!"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    $ world.battle.enemy[0].damage = {1: (200, 220), 2: (230, 250), 3: (260, 280)}

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 5, 2: 3, 3: 1}):
        $ world.party.player.wind_guard()
        return
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    if world.party.player.status == 0:
        $ world.party.player.status = 1
        $ world.party.player.status_print()
        "Лука очарован!"
    elif world.party.player.status == 1:
        $ world.party.player.status_print()
        "Лука в трансе..."

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    if world.party.player.surrendered:
        return

    l "Хаа..."
    world.battle.enemy[0].name "Все люди сразу же становятся лишь игрушками, когда ощущают мой язычок...{w}\nМ-м-м, как думаешь мне теперь с тобой поиграть?"

    if persistent.different == 1:
        $ world.party.player.status_turn = rand().randint(2, 3)
    elif persistent.difficulty == 2:
        $ world.party.player.status_turn = rand().randint(3, 4)
    elif persistent.difficulty == 3:
        $ world.party.player.status_turn = rand().randint(4, 5)
    return

label tamamo_a6:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Пришло время для моего лучшего приёма!",
        "Пора тебя добивать{image=note}.",
        "Этот приём доведёт тебя до оргазма{image=note}."
    ])

    $ world.battle.show_skillname("Девять лун")
    $ random = rand().randint(1, 2)

    $ world.battle.skillcount(2, 3647)

    if random == 1:
        $ world.battle.stage[1] = 1
        jump tamamo_a6a
    elif random == 2:
        $ world.battle.stage[1] = 2
        jump tamamo_a6b

label tamamo_a6a:
    show tamamo cta01 at xy(X=490) zorder 15 as ct

    "Все девять хвостов Тамамо устремляются к Луке!"

    play sound "audio/se/umaru.ogg"
    show tamamo cta02 at xy(X=490) zorder 15 as ct

    "Первый хвост обматывается вокруг его члена!"

    if world.party.player.aqua:
        hide ct
        $ world.party.player.aqua_guard()

    $ world.battle.enemy[0].damage = {1: (180, 220), 2: (184, 224), 3: (188, 228)}

    if world.party.player.holy:
        if not world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
            $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
        else:
            $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Второй хвост обвивается вокруг основания члена Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Кончик третьего хвоста щекочет отверстие его уретры!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Четвёртый хвост мягко массажирует и потискивает головку члена Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Пятый хвост поигрывает яичками Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Шестой хвост пощекочивает вверх и вниз по уздечке члена Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Седьмой хвост нежно потирается по анусу Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Восьмой хвост поглаживает вверх и вниз по всей длине спинки члена Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Девять хвостов Тамамо сходятся вместе в форме цилиндра и устремляются к Луке!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    $ persistent.nine_moons += 1

    hide ct

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label tamamo_a6b:
    play sound "audio/se/umaru.ogg"
    show tamamo ctb01 at xy(X=440) zorder 15 as ct

    if world.party.player.aqua:
        hide ct
        $ world.party.player.aqua_guard()

    if world.party.player.holy:
        if not world.party.player.wind_guard_calc({1: 40, 2: 35, 3: 20}):
            show tamamo ctb02 at Position(anchor=(0, 0), pos=(440, 0)) zorder 15 as ct
        else:
            $ world.party.player.wind_guard()
            return
    else:
        show tamamo ctb02 at Position(anchor=(0, 0), pos=(440, 0)) zorder 15 as ct

    "Девять хвостов Тамамо сходятся вместе в форме цилиндра и устремляются к Луке!"

    "Тамамо протискивает член Луки в созданную хвостами дырочку!{w}{nw}"

    $ world.battle.enemy[0].damage = {1: (540, 660), 2: (552, 672), 3: (564, 684)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)

    play sound "audio/se/umaru.ogg"

    "Мягкие и пушистые хвосты Тамамо стискиваются вокруг члена Луки!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=2)
    play sound "audio/se/umaru.ogg"

    "Её пушистые хвосты доят весь его член во всю длину, как поршень, едва ли не быстрее, чем может уловить его взгляд!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    hide ct

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label tamamo_a7:
    $ world.battle.skillcount(2, 3646)

    if not world.party.player.surrendered:
        if world.battle.first[2]:
            world.battle.enemy[0].name "Что ж, тогда я опять заверну тебя в мои хвосты!"
        else:
            world.battle.enemy[0].name "Я заверну тебя в мои хвостики!"

    $ world.battle.show_skillname("Опутывание хвостами")
    play sound "audio/se/ero_makituki3.ogg"

    "Девять хвостов Тамамо обвиваются вокруг всего тела Луки!"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    $ world.battle.enemy[0].damage = {1: (250, 280), 2: (310, 380), 3: (340, 410)}

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
        return
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Хвосты Тамамо удерживают Луку!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    if world.party.player.surrendered:
        return

    if world.battle.first[2]:
        world.battle.enemy[0].name "В этот раз ты не выберешься{image=note}."
    else:
        world.battle.enemy[0].name "Я вдо-о-оволь тебя обласкаю моими пушистыми хвостиками{image=note}."

    $ world.battle.first[2] = True

    if persistent.difficulty < 3:
        $ world.battle.enemy[0].power = 2
    elif persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return

label tamamo_a8:
    $ world.battle.skillcount(2, 3646)

    "Хвосты Тамамо удерживают Луку..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хи-хи, адская щекотка!",
        "Я люблю вот эту твою штучку{image=note}.",
        "Я обласкаю моими хвостами весь твой член...",
        "Твоё мужество станет причиной твоего поражения...",
        "Всё в порядке, если ты обрызгаешь мой хвост спермой{image=note}."
    ])

    $ world.battle.show_skillname("Опутывание лунными хвостами")
    play sound "audio/se/umaru.ogg"

    if world.battle.stage[4] == 0:
        show tamamo ctc01 at xy(X=440) zorder 15 as ct

    show tamamo ctc02 at xy(X=440) zorder 15 as ct
    $ world.battle.stage[4] = 1

    $ world.battle.cry(narrator, [
        "Кончик её хвоста щекочет член Луки!",
        "Один из хвостов Тамамо нежно поглаживает член Луки!",
        "Девять хвостов Тамамо начинают поглаживать весь член Луки!",
        "Все девять Тамамо, чередуясь, трутся о член Луки!",
        "Кончик одного из хвостов Тамамо скользит вверх-вниз по член Луки!"
    ])

    if world.party.player.aqua:
        hide ct
        $ world.party.player.aqua_guard()

    $ world.battle.enemy[0].damage = {1: (250, 280), 2: (310, 380), 3: (340, 410)}

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    $ world.battle.face(1)

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label tamamo_a9:
    if world.party.player.status == 1:
        $ world.battle.enemy[0].attack()

    $ world.battle.skillcount(2, 3648)

    world.battle.enemy[0].name "Ха!"

    $ world.battle.show_skillname("Оседлание Кицунэ")

    "Тамамо напрыгивает на Луку и насильно прижимает к земле!"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
        $ world.party.player.wind_guard()
        return

    if world.party.player.daystar:
        $ world.party.player.skill22x()

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Тамамо сидит верхом на Луке!"
    world.battle.enemy[0].name "Теперь ты мой.{w}\nВремя поиграть~{image=note}!"

    $ world.battle.show_skillname("Лунное рандеву")
    play sound "audio/se/ero_pyu1.ogg"
    show tamamo hb2

    "Тамамо садится вниз, насильно вгоняя член Луки в свою вагину!{w}{nw}"

    $ world.tamamo_rape = True
    $ world.party.player.bind = 2

    $ world.battle.enemy[0].damage = (400, 450)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    "Лука был изнасилован Тамамо!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    l "Ааах...!"
    "Я задохнулся от удовольствия, которое подарила маленькая влажная вагина Тамамо.{w}\nТакими темпами я кончу!"

    return

label tamamo_kousan:
    $ world.battle.common_surrender()

    "Лука прекращает сопротивляться..."

    $ world.battle.face(2)

    world.battle.enemy[0].name "Оу? Сдаёшься?{w}\nЧто ж, для человека ты зашёл довольно далеко."

    $ world.battle.enemy[0].attack()

label tamamo_ng_start:
    python:
        world.troops[17] = Troop(world)
        world.troops[17].enemy[0] = Enemy(world)

        world.troops[17].enemy[0].name = Character("Тамамо")
        world.troops[17].enemy[0].sprite = ["tamamo st62", "tamamo st62", "tamamo st63", "tamamo st62"]
        world.troops[17].enemy[0].position = center
        world.troops[17].enemy[0].defense = 60
        world.troops[17].enemy[0].evasion = 90
        world.troops[17].enemy[0].max_life = world.troops[17].battle.difficulty(48000, 60000, 72000)
        world.troops[17].enemy[0].henkahp[0] = 11000
        world.troops[17].enemy[0].power = world.troops[17].battle.difficulty(0, 1, 2)

        world.troops[17].battle.tag = "tamamo_ng"
        world.troops[17].battle.name = world.troops[17].enemy[0].name
        world.troops[17].battle.background = "bg 035"

        if persistent.music:
            world.troops[17].battle.music = 21
        else:
            world.troops[17].battle.music = 4

        world.troops[17].battle.ng = True
        world.troops[17].battle.exp = 4000000
        world.troops[17].battle.exit = "tamamo_ng_victory"

        world.troops[17].battle.init(0, True)

        world.battle.stage[8] = 1

    t "Ты наверное думаешь, что настолько силён, что для тебя не составит проблем победить меня в этой форме, не так ли?{w}\nЧто ж, похоже придётся немного опустить твоё эго."
    l "Угх!.."
    "Её аура невероятно подавляющая.{w}\nТакая мощь..."
    "Боюсь, даже Алиса не смогла бы противостоять истинной форме Тамамо."
    t "Я являюсь одной из шести первых монстров, что ступили на землю.{w}\nИ я была одной из подчинённых на службе у первородной владыки монстров, мощь которой превосходила даже силу Илиас."
    t "Неужели ты правда думаешь, что я проиграю какому-то юнцу?"
    l "Ну и кто из нас тут ещё самоуверенный?"
    t "Ну тогда докажи это, если считаешь себя достаточно крутым."
    l "Я с удовольствием поставлю тебя на место!"
    t "... Однако эта форма всё ещё слаба.{w}\nЯ не могу использовать всю свою силу."
    t "... Хотя капля спермы может решить эту проблему.{w}\nПоэтому, пожалуй, я буду использовать только атаки удовольствием."
    t "По рукам?"
    l "... Хорошо."
    "Полагаю, мне придётся немного подождать, чтобы сразиться с ней в полную силу.{w}\nНо я должен победить её хотя бы в этой форме, чтобы заставить принять сосуществование."
    show tamamo st61 at xy(0.75)
    show alice st03b at xy(0.33375)

    a "Эй, идиоты. Вы уже закончи-"
    show alice st07b at xy(0.33375)
    extend "\n.............................."
    "Алиса тупо уставилась на перевоплотившуюся Тамамо."
    play sound "se/escape.ogg"
    hide alice
    $ world.battle.face(1)
    extend "\nПосле чего молча уходит в дверь из которой пришла."
    t "Глупая девочка.{w}\nНеужели она правда думала, что я, прожившая много тысяч лет, так молодо выгляжу?"
    t "Но сейчас не об этом.{w}\nПокажи мне всё, на что ты способен, Лука!"

    if persistent.difficulty == 3 and _in_replay:
        $ world.party.player.skill[0] = 27

    $ world.battle.progress = 1
    $ world.battle.main()

label tamamo_ng_main:
    python:
        world.battle.stage[0] = 0
        world.battle.stage[1] = 0
        if not world.battle.enemy[0].earth:
            world.battle.enemy[0].defense = 60

        world.battle.progress += 1

        if world.party.player.bind and world.battle.stage[2]:
            renpy.show("tamamo hd02")

        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label tamamo_ng_v:
    if persistent.nine_moons:
        $ persistent.nine_moons_won = 1

    if persistent.difficulty == 3:
        $ persistent.tamamo_hell = 1

    $ world.achivements(1)
    $ world.party.player.daystar = 0

    if not _in_replay:
        $ world.tamamo_fight = 3
        $ world.actor[1].rep += 1

    show tamamo st61

    t "Ах!.."
    l "Ну всё, попалась..."

    $ world.battle.show_skillname("Денница")

    "Ослепительная звезда обрушивается в ад!"

    python:
        world.party.player.skill22a()

        for line in range(0, 3):
            renpy.sound.play("se/damage2.ogg")

            world.party.player.damage = rand().randint(3200, 3600)

            _window_show()
            renpy.show_screen("hp")
            _window_auto = True
            world.battle.enemy[0].raw_life_calc(line+1)

    $ world.battle.hide_skillname()

    t "Гах!..{w}\nТы действительно её сын..."
    "Похоже, что Тамамо больше не может сражаться.{w}\nЕсли я продолжу в том же духе, это сильно навредит ей."
    l "...{w}\nХорошо, на этом хватит."
    t "Ммм?"
    l "Если мы продолжим в том же духе, то ты сильно пострадаешь или даже будешь запечатана.{w}\nЭто не то, ради чего я сражаюсь с тобой."
    t "Как благородно...{w}{nw}"

    show tamamo st62

    extend "{w=.5} Однако!"

    $ world.battle.show_skillname("Оседлание Кицунэ")
    play sound "se/down.ogg"

    "Тамамо напрыгивает на Луку и насильно прижимает к земле!"

    $ world.party.player.bind = 1
    $ world.party.player.status_print(1)
    show tamamo hd01

    "Тамамо сидит верхом на Луке!"

    $ world.battle.hide_skillname()

    t "Ты слишком мягок."
    l "Ах!{w}\nЧто за?!"
    "Я потерял бдительность и попался в её ловушку!"
    t "Понимаешь, Лука.{w}\nПросто потому что я добрая ещё не значит, что меня легко победить."
    t "Кроме того, я уже запечатана, идиот."
    t "Теперь ты мой..."

    play sound "se/earth2.ogg"
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "master")
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "screens")
    $ DELAY = 1.0

    l "Что за?!"
    a "Я ведь говорила тебе не использовать Денницу, идиота кусок!"
    "Кричит Алиса из соседней комнаты."
    l "Что?.."

    if not world.battle.used_daystar:
        extend "\nНо я не использовал Денницу!"

    l "Что происходит?!"
    t "О, я уверена нам не о чём беспокоиться.{w}\nНу а теперь..."

    $ world.battle.show_skillname("Рандеву Лунного Света")
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "master")
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "screens")
    $ DELAY = 1.0
    pause 0.5
    play sound "se/bom2.ogg"
    scene bg white with flash

    t "Уаа?!"
    "Опоры не выдерживают и падают на Луку с Тамамо!"

    play sound "se/damage2.ogg"
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "master")
    $ renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=30), "screens")
    $ DELAY = 1.0
    $ world.battle.enemy[0].damage = rand().randint(10000, 12000)

    "Тамамо получает [world.battle.enemy[0].damage] урона!"

    $ world.battle.enemy[0].deal((2800, 3200), 1)
    $ world.battle.hide_skillname()
    stop music fadeout 1.0
    hide screen hp
    hide screen turn_scorer

    "............................"

    window hide
    play sound "se/counter.ogg"
    show expression Text("ДВОЙНОЙ НОКАУТ!", size=90, bold=True, italic="True", color="#ff0000", outlines=[(4, "#000", 0, 0)], font="Skandal.ttf") zorder 30 as txt:
        align (.5, .5)

    pause 1.0
    scene bg black with Dissolve(1.0)
    pause 1.0
    window show
    window auto

    "..............................{w}\n-диот...{w}\nЭй, идиот!{w}\nПросыпайся!"
    l "Хах?"

    scene bg 021 with Dissolve(1.0)
    show alice st03

    a "Ты чуть не похоронил всех нас в этой пещере!{w}\nПеред тем, как использовать эту свою Денницу в таких местах, может стоить сначала убедиться смогут ли выдержать опоры, идиот?!"

    if world.battle.used_daystar < 1:
        l "Но я не использовал Денницу!{w}\nТы хоть представляешь, как было сложно сдержаться?"
        a "В любом случае, ты устроил настоящий хаос, из-за чего пещера не выдержала и обрушилась."
    elif world.battle.used_daystar:
        l "Ч-что произошло?.."
        a "Пещера обрушилась, вот что произошло!"

    l "Что?!{w}\nС Тамамо всё в порядке?!"
    t "Ууу..."

    hide alice
    show tamamo st21

    ".............................."

    if world.party.player.life == world.party.player.max_life and not world.party.player.nodamage:
        $ persistent.count_hpfull = True

    if persistent.difficulty == 3:
        $ persistent.count_hellvic = True

    if world.party.player.status == 7:
        $ persistent.count_vickonran = True

    $ renpy.end_replay()
    $ world.party.player.skill_set(1)
    $ world.battle.victory(1)

label tamamo_ng_stalemate:
    show tamamo st61
    t "Эта битва...{w}\nКак-то она уже затянулась, тебе так не кажется?"
    l "А?"
    t "Наши силы примерно одинаковы, не так ли, мальчик?{w}\nИ, честно говоря, мне уже наскучило пытаться заставить тебя кончить."
    t "Почему бы нам не объявить ничью?{w}\nИсход битвы мы можем решить и в другой раз."
    l "Ух..."
    menu:
        "Объявить ничью":
            jump tamamo_ng_x1
        "Продолжить битву":
            jump tamamo_ng_x2

label tamamo_ng_x1:
    if world.party.check(2):
        $ world.actor[2].rep.rep -= 1

    $ world.tamamo_fight = 1

    l "Ты права.{w}\nЭто бессмысленно."
    "Мне тоже не очень хочеться продолжать битву в таком духе.{w}\nОбъявление ничьи ещё не значит, что мы вновь не сразимся когда-нибудь."
    t "Отлично.{w}{nw}"
    scene bg white
    "..."
    show tamamo st01
    show bg 035
    extend "\nТогда на сегодня всё."

    $ world.battle.exp = (world.battle.enemy[0].max_life - world.battle.enemy[0].life) * 50
    $ world.battle.victory(1)

    jump tamamo_ng_victory

label tamamo_ng_x2:
    l "Нет.{w}\nЯ знаю, что смогу победить!{w}\nПока я стою на ногах, я никогда не сдамся!"
    t "Хорошо.{w}{nw}"
    $ world.battle.face(1)
    extend "\nКак пожелаешь...~{image=note}"
    jump tamamo_ng_main

label tamamo_ng_v2:
    $ world.party.tamamo_fight = 2
    show tamamo st61

    t "Угх.{w}\nЯ не могу больше сражаться..."
    t "Моя сила...ы"

    scene bg white

    "Комната озаряется ярким светом!"

    show tamamo st01
    show bg 035
    play sound "audio/se/down.ogg"
    show tamamo st21

    t "Ууууууу...{w}\nНе могу больше...{w}\nЯ сдаюсь..."
    "Тамамо выдохлась!"

    $ world.battle.exp = (world.battle.enemy[0].max_life - world.battle.enemy[0].life) * 70
    $ world.battle.victory(1)

    jump tamamo_ng_victory

label tamamo_ng_a:
    if world.battle.enemy[0].earth:
        $ world.battle.enemy[0].defense = 60

    if persistent.difficulty == 1 and world.battle.progress == 70:
        jump tamamo_ng_stalemate
    if persistent.difficulty == 1 and world.battle.progress == 100:
        jump tamamo_ng_v2
    if persistent.difficulty == 2 and world.battle.progress == 85:
        jump tamamo_ng_stalemate
    if persistent.difficulty == 2 and world.battle.progress == 125:
        jump tamamo_ng_v2
    if world.party.player.bind == 1 and world.battle.stage[3]:
        call tamamo_ng_a9
        $ world.battle.main()
    if world.party.player.bind == 1:
        call tamamo_ng_a8
        $ world.battle.main()

    $ world.battle.stage[3] = 0

    if world.party.player.life < world.party.player.max_life / 2:
        call tamamo_ng_ab
        $ world.battle.main()
    elif world.party.player.mp < 3:
        call tamamo_ng_ab
        $ world.battle.main()

    if not world.party.player.aqua and not world.party.player.surrendered:
        call tamamo_ng_a6
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,12)

        if random == 1:
            call tamamo_ng_a1
            $ world.battle.main()
        if random == 2 and world.battle.stage[1] == 0:
            call tamamo_ng_a2
            $ world.battle.main()
        if random == 3 and world.battle.stage[0] == 0:
            call tamamo_ng_a3
            $ world.battle.main()
        if random == 4:
            call tamamo_ng_a4
            $ world.battle.main()
        if random == 5 and world.party.player.status == 0:
            call tamamo_ng_a5
            $ world.battle.main()
        if random == 6:
            call tamamo_ng_a6
            $ world.battle.main()
        if random == 7:
            call tamamo_ng_a7
            $ world.battle.main()
        if random == 8:
            call tamamo_ng_a9
            $ world.battle.main()
        if random > 8 and world.battle.enemy[0].earth == 0:
            call tamamo_ng_a10
            $ world.battle.main()

label tamamo_ng_ab:
    while True:
        $ random = rand().randint(1,7)

        if random == 1:
            call tamamo_ng_a2
            $ world.battle.main()
        if random == 2:
            call tamamo_ng_a3
            $ world.battle.main()
        if random == 3:
            call tamamo_ng_a5
            $ world.battle.main()
        if random == 4:
            call tamamo_ng_a6
            $ world.battle.main()
        if random == 5 and world.party.player.status == 0 and not world.party.player.aqua:
            call tamamo_ng_a5
            $ world.battle.main()
        if random == 6:
            call tamamo_ng_a7
            $ world.battle.main()
        if random == 7:
            call tamamo_ng_a9
            $ world.battle.main()

label tamamo_ng_a1:
    $ world.battle.skillcount(2, 3641)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ты полюбишь мою руку.",
        "Приятнее всего, когда я ласкаю головку, не так ли?",
        "Я пощекочу твою головку."
    ])

    $ world.battle.show_skillname("Мастурбация Кицунэ")
    play sound "audio/se/ero_koki1.ogg"

    "Руки Тамамо проворно и ловко ласкают член Луки"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(20, 10, 5):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (420, 450), 2: (450, 480), 3: (520, 550)}

        if world.party.player.holy:
            if not world.party.player.wind_guard_calc({1: 6, 2: 3, 3: 1}):
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
            else:
                world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label tamamo_ng_a2:
    $ world.battle.skillcount(2, 3642)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я буду сосать его столько, сколько захочу{image=note}.",
        "Не хочешь кончить в мой ротик? Хи-хи...",
        "Хи-хи... Думаешь, что сможешь стерпеть то, как я ласкаю тебя своим ротиком, м?"
    ])

    $ world.battle.show_skillname("Минет Кицунэ")
    play sound "audio/se/ero_buchu3.ogg"

    "Тамамо с похотливой жадностью сосёт весь член Луки своим ртом!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(80, 75, 55):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (200, 250), 2: (270, 280), 3: (390, 410)}

        if world.party.player.holy:
            if not world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
            else:
                world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        renpy.sound.play("audio/se/ero_buchu3.ogg")
        narrator("Она заглатывает всё основание члена Луки!")

        world.battle.enemy[0].damage = {1: (450, 500), 2: (480, 530), 3: (490, 560)}

        if world.party.player.holy:
            if not world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
            else:
                world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        renpy.sound.play("audio/se/ero_buchu3.ogg")
        narrator("Тамамо начинает бегло сосать и вылизывать каждый сантиметр члена Луки!")

        world.battle.enemy[0].damage = {1: (560, 600), 2: (580, 630), 3: (810, 860)}

        if world.party.player.holy:
            if not world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
            else:
                world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        world.battle.face(1)

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label tamamo_ng_a3:
    if world.party.player.surrendered:
        $ world.battle.stage[0] = 1
        if world.party.player.mp > 2:
            if world.party.player.life > world.party.player.max_life / 2 and rand().randint(1,100) < 96:
                call tamamo_ng_a

    $ world.battle.skillcount(2, 3643)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Моя потрясающая грудь пленит кого угодно~♥!",
        "Тебе ведь нравятся такие большие. Не так ли, мальчик?",
        "Ты ведь предпочитаешь пайзури?"
    ])

    $ world.battle.show_skillname("Пайзури Лисьей Королевы")
    play sound "audio/se/ero_koki1.ogg"

    "Тамамо сжимает член Луки между своих мягких сисек!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(80, 75, 55):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (420, 450), 2: (500, 550), 3: (610, 650)}

        if world.party.player.holy:
            if not world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
            else:
                world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        renpy.sound.play("audio/se/ero_koki1.ogg")
        narrator("Она зажимает член Луки между своими сиськами!")

        world.battle.enemy[0].damage = {1: (380, 420), 2: (450, 490), 3: (460, 500)}

        if world.party.player.holy:
            if not world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
            else:
                world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        renpy.sound.play("audio/se/ero_koki1.ogg")
        narrator("И начинает двигать ими, принося ему удовольствие!")

        world.battle.enemy[0].damage = {1: (450, 490), 2: (520, 560), 3: (620, 670)}

        if world.party.player.holy:
            if not world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
            else:
                world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label tamamo_ng_a4:
    $ world.battle.skillcount(2, 3644)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Некоторые люди просто обожают, когда над ними доминируют{image=note}.",
        "Ты меня сильно разочаруешь, если кончишь от этого...",
        "Я буду нежно растаптывать весь твой член..."
    ])

    $ world.battle.show_skillname("Растирание ногой Луноборца")
    play sound "audio/se/ero_koki1.ogg"

    "Тамамо наступает на член Луки своей ножкой!{w}{nw}"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and random < world.battle.difficulty(5, 4, 3):
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (180, 200), 2: (190, 210), 3: (200, 220)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
            world.party.player.wind_guard()
            renpy.return_statement()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)

        if not world.party.player.life:
            renpy.jump("badend_h")

    play sound "audio/se/ero_koki1.ogg"

    "Прижимая к нему свою ступню, она потирает его член своей ножкой!{w}{nw}"

    python:
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=2)

        if not world.party.player.life:
            renpy.jump("badend_h")

    play sound "audio/se/ero_koki1.ogg"
    "Удерживая своим хвостом равновесие, она сдавливает член Луки между обеими своими ножками!{w}{nw}"

    python:
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label tamamo_ng_a5:
    if world.party.player.aqua and rand().randint(1,100) < 95:
        $ world.battle.enemy[0].attack()

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Твоё личико так и манит его облизать его...",
        "Мой язычок может пленить любого мужчину...",
        "Мой язычок может завладеть разумом любого мужчины{image=note}."
    ])

    $ world.battle.show_skillname("Обессиливающая забава")
    play sound "audio/se/ero_pyu1.ogg"

    "Тамамо прыгает на Луку и крепко его обнимает!{w}\nИ на этом она начинает вылизывать всё его лицо!"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    $ world.battle.enemy[0].damage = {1: (500, 520), 2: (530, 550), 3: (560, 580)}

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 5, 2: 3, 3: 1}):
        $ world.party.player.wind_guard()
        return
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    if world.party.player.status == 0:
        $ world.party.player.status = 1
        $ world.party.player.status_print()
        "Лука очарован!"
    elif world.party.player.status == 1:
        $ world.party.player.status_print()
        "Лука в трансе..."

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    if world.party.player.surrendered:
        return

    l "Хаа..."
    world.battle.enemy[0].name "Все люди сразу же становятся лишь игрушками, когда ощущают мой язычок...{w}\nМ-м-м, как думаешь мне теперь с тобой поиграть?"

    if persistent.different == 1:
        $ world.party.player.status_turn = rand().randint(2, 3)
    elif persistent.difficulty == 2:
        $ world.party.player.status_turn = rand().randint(3, 4)
    elif persistent.difficulty == 3:
        $ world.party.player.status_turn = rand().randint(4, 5)
    return

label tamamo_ng_a6:
    if world.party.player.aqua and rand().randint(1,100) < 95:
        $ world.battle.enemy[0].attack()

    $ world.battle.cry(world.battle.enemy[0].name, [
        "На этот раз ты не выдержишь...",
        "Время заканчивать~{image=note}!",
        "Этот приём доведёт тебя до оргазма~{image=note}!"
    ])

    $ world.battle.show_skillname("Девять лун")
    $ random = rand().randint(1,2)

    if random == 1:
        $ world.battle.stage[1] = 1
        jump tamamo_ng_a6a
    elif random == 2:
        $ world.battle.stage[1] = 2
        jump tamamo_ng_a6b

label tamamo_ng_a6a:
    show tamamo cta01 at xy(X=490) zorder 15 as ct

    "Все девять хвостов Тамамо устремляются к Луке!"

    play sound "audio/se/umaru.ogg"
    show tamamo cta02 at xy(X=490) zorder 15 as ct

    "Первый хвост обматывается вокруг его члена!"

    if world.party.player.aqua:
        hide ct
        $ world.party.player.aqua_guard()

    $ world.battle.enemy[0].damage = {1: (180, 220), 2: (184, 224), 3: (188, 228)}

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Второй хвост обвивается вокруг основания члена Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Кончик третьего хвоста щекочет отверстие его уретры!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Четвёртый хвост мягко массажирует и потискивает головку члена Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Пятый хвост поигрывает яичками Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "audio/se/umaru.ogg"

    "Шестой хвост пощекочивает вверх и вниз по уздечке члена Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Седьмой хвост нежно потирается по анусу Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Восьмой хвост поглаживает вверх и вниз по всей длине спинки члена Луки!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/umaru.ogg"

    "Девять хвостов Тамамо сходятся вместе в форме цилиндра и устремляются к Луке!"

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    $ persistent.nine_moons += 1

    hide ct

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label tamamo_ng_a6b:
    play sound "audio/se/umaru.ogg"
    show tamamo ctb01 at xy(X=440) zorder 15 as ct

    if world.party.player.aqua:
        hide ct
        $ world.party.player.aqua_guard()

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 40, 2: 35, 3: 20}):
        $ world.party.player.wind_guard()
        return
    else:
        show tamamo ctb02 at Position(anchor=(0, 0), pos=(440, 0)) zorder 15 as ct

    "Девять хвостов Тамамо сходятся вместе в форме цилиндра и устремляются к Луке!"

    "Тамамо протискивает член Луки в созданную хвостами дырочку!{w}{nw}"

    $ world.battle.enemy[0].damage = {1: (540, 660), 2: (552, 672), 3: (564, 684)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)

    play sound "audio/se/umaru.ogg"

    "Мягкие и пушистые хвосты Тамамо стискиваются вокруг члена Луки!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=2)
    play sound "audio/se/umaru.ogg"

    "Её пушистые хвосты доят весь его член во всю длину, как поршень, едва ли не быстрее, чем может уловить его взгляд!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    hide ct

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label tamamo_ng_a7:
    if world.party.player.aqua and rand().randint(1,100) < 95:
        $ world.battle.enemy[0].attack()

    $ world.battle.skillcount(2, 3646)

    if not world.party.player.surrendered:
        if world.battle.first[2]:
            world.battle.enemy[0].name "Что ж, тогда я опять заверну тебя в мои хвосты!"
        else:
            world.battle.enemy[0].name "Я заверну тебя в мои хвостики!"

    $ world.battle.show_skillname("Опутывание хвостами")
    play sound "audio/se/ero_makituki3.ogg"

    "Девять хвостов Тамамо обвиваются вокруг всего тела Луки!"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    $ world.battle.enemy[0].damage = {1: (250, 280), 2: (380, 410), 3: (410, 440)}

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Хвосты Тамамо удерживают Луку!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    if world.party.player.surrendered:
        return

    if world.battle.first[2]:
        world.battle.enemy[0].name "В этот раз ты не выберешься{image=note}."
    else:
        world.battle.enemy[0].name "Я вдо-о-оволь тебя обласкаю моими пушистыми хвостиками{image=note}."

    $ world.battle.first[2] = True

    if persistent.difficulty < 3:
        $ world.battle.enemy[0].power = 2
    elif persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return

label tamamo_ng_a8:
    $ world.battle.skillcount(2, 3646)

    "Хвосты Тамамо удерживают Луку..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хи-хи, адская щекотка!",
        "Я люблю вот эту твою штучку{image=note}.",
        "Я обласкаю моими хвостами весь твой член...",
        "Твоё мужество станет причиной твоего поражения...",
        "Всё в порядке, если ты обрызгаешь мой хвост спермой{image=note}."
    ])

    $ world.battle.show_skillname("Опутывание лунными хвостами")
    play sound "audio/se/umaru.ogg"

    if world.battle.stage[4] == 0:
        show tamamo ctc01 at xy(X=440) zorder 15 as ct

    show tamamo ctc02 at xy(X=440) zorder 15 as ct
    $ world.battle.stage[4] = 1

    $ world.battle.cry(narrator, [
        "Кончик её хвоста щекочет член Луки!",
        "Один из хвостов Тамамо нежно поглаживает член Луки!",
        "Девять хвостов Тамамо начинают поглаживать весь член Луки!",
        "Все девять Тамамо, чередуясь, трутся о член Луки!",
        "Кончик одного из хвостов Тамамо скользит вверх-вниз по член Луки!"
    ])

    if world.party.player.aqua:
        hide ct
        $ world.party.player.aqua_guard()

    $ world.battle.enemy[0].damage = {1: (250, 280), 2: (380, 410), 3: (410, 440)}

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
        $ world.party.player.wind_guard()
    else:
        $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    $ world.battle.face(1)

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label tamamo_ng_a9:
    if not world.party.player.surrendered:
        if world.party.player.aqua and rand().randint(1,100) < 90:
            $ world.battle.enemy[0].attack()

    if world.battle.stage[3] == 1:
        call tamamo_ng_a9a
    elif world.battle.stage[3] == 2:
        call tamamo_ng_a9b
    elif world.battle.stage[3] == 3:
        call tamamo_ng_a9c

    world.battle.enemy[0].name "Ха!"

    $ world.battle.show_skillname("Оседлание Кицунэ")

    "Тамамо напрыгивает на Луку и насильно прижимает к земле!"

    if world.party.player.aqua and rand().randint(1,100) < 91:
        $ world.party.player.aqua_guard()

    if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
        $ world.party.player.wind_guard()
        return

    if world.party.player.daystar:
        $ world.party.player.skill22x()

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Тамамо сидит верхом на Луке!"
    world.battle.enemy[0].name "Теперь ты мой.{w}\nВремя поиграть~{image=note}!"

    $ world.battle.show_skillname("Лунное рандеву")
    play sound "audio/se/ero_pyu1.ogg"
    show tamamo hb2

    "Тамамо садится вниз, насильно вгоняя член Луки в свою вагину!{w}{nw}"

    $ world.tamamo_rape = True
    $ world.party.player.bind = 2

    $ world.battle.enemy[0].damage = (600, 650)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    "Лука был изнасилован Тамамо!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    l "Ааах...!"
    "Я начал задыхаться от удовольствия, которое подарила влажная вагина Тамамо.{w}\nТакими темпами я кончу!"

    return

label tamamo_ng_a10:
    world.battle.enemy[0].name "...................."
    $ world.battle.show_skillname("Дикие Земли")
    play sound "audio/se/earth2.ogg"
    "Тамамо наполняет свое тело силой земли!"
    $ world.battle.enemy[0].earth = 1
    $ world.battle.enemy[0].earth_turn = 4
    $ world.battle.enemy[0].defense = 40

    $ world.battle.hide_skillname()

    return

label tamamo_ng_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Значит сдаёшься?{w}\nУдивительно."
    world.battle.enemy[0].name "Но... ты проделал долгий путь.{w}\nЗа это я награжу тебя кое-чем приятным..."

    $ world.battle.enemy[0].attack()
