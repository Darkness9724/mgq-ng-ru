init python:
    def get_alice_ng():
        girl = Chr()
        girl.unlock = persistent.alice_ng_unlock
        girl.name = "Алиса"
        girl.pedia_name = "Алиса"
        girl.info = """Шестнадцатая и одна из сильнейших владык монстров. Алиса является частью семьи Фейтбёрн, правящей монстрами с незапамятных времён. Когда Марцелл со своими друзьями убил её мать, позволившей им это, Алиса пришла в ярость и сильно ранила его с Лазарем. Она до сих пор винит себя за этот инцидент, полагая, что из-за него жертва матери была напрасна. Всю последующую жизнь её опекала Тамамо, она редко контактировала с людьми и потому не знает как себя с ними вести.
Путешествуя вместе с Лукой в его первом приключении, она впустила людей в своё сердце и начала работать вместе с ним для достижения мира. Впервые встретив Луку в лесу неподалёку от деревни Илиас, она была полностью сбита, когда тот узнал её. Как только она выслушала его историю и увидела два Сияния Ангела в качестве доказальств его слов, Алиса приняла решение вновь отправится в путешествие с ним, чтобы понять, что с ним произошло."""
        girl.zukan_ko = None
        girl.label = "alice_ng"
        girl.label_cg = "alice_ng_cg"
        girl.m_pose = alice_ng_pose
        girl.facenum = 9
        girl.posenum = 8
        girl.bk_num = 0
        girl.x[0] = -20
        girl.x[1] = 150
        girl.lv = 145
        girl.hp = 32000
        girl.scope = {}

        return girl

    def get_granberia_ng():
        girl = Chr()
        girl.unlock = persistent.granberia_ng_unlock
        girl.name = "Гранберия"
        girl.pedia_name = "Гранберия"
        girl.info = """Молодая драконица и одна из четвёрки небесных рыцарей. Гранберия невероятно сильная мечница проклятого клинка, способная использовать множество смертиельных и эффективных техник. Вероятно является самой сильной из небесных рыцарей, так как была единственной, кто сразилась против владыки монстров на отборочном турнире. С другой стороны, Альма Эльма может легко победить её, а против истинной силы Тамамо скорее всего не продержится даже Алиса.
Хоть многие дракониды и заботят о своём потомстве, Гранберию бросили ещё когда та была малюткой. После чего она путешествовала по миру, пока не встретила Саламандру. Та стала её приёмной матерью, когда решила обучить фехтованию. И хоть сейчас Гранберия куда сильнее Саламандры, она всё ещё испытывает уважение и чувство долга по отношению к духу огня."""
        girl.zukan_ko = "granberia_ng"
        girl.label = "granberia_ng"
        girl.label_cg = "granberia_ng_cg"
        girl.m_pose = granberia_ng_pose
        girl.facenum = 7
        girl.posenum = 4
        girl.bk_num = 0
        girl.x[0] = -200
        girl.lv = 126
        girl.hp = 32000
        girl.scope = {
            "world.party.player.level": 80,
            "world.party.player.skill[0]": 25,
            "world.party.item[0]": 2
        }

        return girl

    def get_queenharpy_ng():
        girl = Chr()
        girl.unlock = persistent.queenharpy_ng_unlock
        girl.name = "Королева Гарпий"
        girl.pedia_name = "Королева Гарпий"
        girl.info = """Соответствуя своему титулу лидера гарпий, она очень сильный монстр, намного сильнее других представительнец своего вида. Обычный человек может даже не надеяться победить её в схватке из-за её быстроты и абсолютного контроля над ветром.
Так как её раса находится на грани вымирания, королева желает иметь много детей. Но даже несмотря на то, что для этого ей потребовалось бы куча мужчин, королева гарпий всё ещё желает спариваться лишь с самым сильными из них, чтобы дать своим отпрыскам хорошие гены. Судя по всему таковых в деревне Счастья нет, поэтому она постоянно пребывает в состоянии конфликта между \"нужно возрождать расу\" и \"нужно найти сильного мужчину\"."""
        girl.zukan_ko = "queenharpy_ng"
        girl.label = "queenharpy_ng"
        girl.label_cg = "queenharpy_ng_cg"
        girl.m_pose = queenharpy_ng_pose
        girl.facenum = 4
        girl.posenum = 2
        girl.bk_num = 0
        girl.x[0] = -200
        girl.lv = 113
        girl.hp = 25000
        girl.scope = {
            "world.party.player.level": 81,
            "world.party.player.skill[0]": 25,
            "world.party.item[0]": 3
        }

        return girl

    def get_tamamo_ng():
        girl = Chr()
        girl.unlock = persistent.tamamo_ng_unlock
        girl.name = "Тамамо"
        girl.pedia_name = "Тамамо"
        girl.info = """Королева кицунэ, одна из первородных шести монстров. Тамамо входит в четвёрку небесных рыцарей и в совершенстве владеет стихией земли, хотя большую часть времени ей приходится проводить в своей ослабленной форме в виде маленькой девочки, так как истинная была запечатана. Но даже так, в этой форме она способна на равных драться с остальными рыцарями, а со снятием печати, возможно, она даже затмит нынешнюю владыку монстров.
После катастрофических последствий, к котором привело восхождение Алифисы Восьмой на трон, Тамамо было поручено воспитание будущих владык монстров, чтобы история не повторилась. После смерти матери Алисы Тамамо пришлось по факту удочерить её, хотя, похоже, кицунэ с трудом справлялась с этой ролью, так как Алиса всё ещё травмирована событиями из своего прошлого и с недоверием относится к людям.
В прошлом она служила самой первой владыке монстров, и похищала души мужчин в обмен на секс с ней. Впрочем, судя по всему, с тех пор её характер значительно смягчился, и теперь она по праву считается самой доброй из четвёрки небесных рыцарей. Но не забывайте, что Тамамо всё ещё одна из самых сильных ныне живущих монстров."""
        girl.zukan_ko = "tamamo_ng"
        girl.label = "tamamo_ng"
        girl.label_cg = "tamamo_ng_cg"
        girl.m_pose = tamamo_ng_pose
        girl.facenum = 7
        girl.posenum = 4
        girl.bk_num = 0
        girl.x[0] = -200
        girl.lv = "\n128 (запечатанная),\n300? (текущая сила),\n500? (предел)"
        girl.hp = "\n22000 (запечатананя),\n48000 (текущая сила),\n72000 (предел)"
        girl.scope = {
            "world.party.player.level": 82,
            "world.party.player.skill[0]": 26,
            "world.party.player.skillset": 2,
            "world.party.item[0]": 5,
            "world.afterend": True
        }

        return girl

init -10 python:
    def alice_ng_pose(girl, x):
        imgs = []

        if girl.pose == 1 and girl.face == 10:
            girl.face = 1
        elif girl.pose == 2 and girl.face == 10:
            girl.face = 1
        elif girl.pose == 3 and girl.face == 10:
            girl.face = 1
        elif girl.pose == 4 and girl.face == 10:
            girl.face = 1
        elif girl.pose == 5 and girl.face == 3:
            girl.face = 1
        elif girl.pose == 6 and girl.face == 3:
            girl.face = 1
        elif girl.pose == 7 and girl.face == 3:
            girl.face = 1
        elif girl.pose == 8 and girl.face == 2:
            girl.face = 1
        elif girl.pose == 9:
            girl.pose = 1

        if girl.pose == 1:
            if girl.face == 1:
                imgs.append(["alice st01", (x, 0)])
            elif girl.face == 2:
                imgs.append(["alice st02", (x, 0)])
            elif girl.face == 3:
                imgs.append(["alice st03", (x, 0)])
            elif girl.face == 4:
                imgs.append(["alice st04", (x, 0)])
            elif girl.face == 5:
                imgs.append(["alice st05", (x, 0)])
            elif girl.face == 6:
                imgs.append(["alice st06", (x, 0)])
            elif girl.face == 7:
                imgs.append(["alice st07", (x, 0)])
            elif girl.face == 8:
                imgs.append(["alice st08", (x, 0)])
            elif girl.face == 9:
                imgs.append(["alice st09", (x, 0)])

            if girl.bk[0]:
                imgs.append(["alice bk01", (x, 0)])
            if girl.bk[1]:
                imgs.append(["alice bk02", (x, 0)])
            if girl.bk[2]:
                imgs.append(["alice bk03", (x, 0)])

        elif girl.pose == 2:
            x -= 80

            if girl.face == 1:
                imgs.append(["alice st01b", (x, 0)])
            elif girl.face == 2:
                imgs.append(["alice st02b", (x, 0)])
            elif girl.face == 3:
                imgs.append(["alice st03b", (x, 0)])
            elif girl.face == 4:
                imgs.append(["alice st04b", (x, 0)])
            elif girl.face == 5:
                imgs.append(["alice st05b", (x, 0)])
            elif girl.face == 6:
                imgs.append(["alice st06b", (x, 0)])
            elif girl.face == 7:
                imgs.append(["alice st07b", (x, 0)])
            elif girl.face == 8:
                imgs.append(["alice st08b", (x, 0)])
            elif girl.face == 9:
                imgs.append(["alice st09b", (x, 0)])

        elif girl.pose == 3:
            if girl.face == 1:
                imgs.append(["alice st11", (x, 0)])
            elif girl.face == 2:
                imgs.append(["alice st12", (x, 0)])
            elif girl.face == 3:
                imgs.append(["alice st13", (x, 0)])
            elif girl.face == 4:
                imgs.append(["alice st14", (x, 0)])
            elif girl.face == 5:
                imgs.append(["alice st15", (x, 0)])
            elif girl.face == 6:
                imgs.append(["alice st16", (x, 0)])
            elif girl.face == 7:
                imgs.append(["alice st17", (x, 0)])
            elif girl.face == 8:
                imgs.append(["alice st18", (x, 0)])
            elif girl.face == 9:
                imgs.append(["alice st19", (x, 0)])

        elif girl.pose == 4:
            x -= 80

            if girl.face == 1:
                imgs.append(["alice st11b", (x, 0)])
            elif girl.face == 2:
                imgs.append(["alice st12b", (x, 0)])
            elif girl.face == 3:
                imgs.append(["alice st13b", (x, 0)])
            elif girl.face == 4:
                imgs.append(["alice st14b", (x, 0)])
            elif girl.face == 5:
                imgs.append(["alice st15b", (x, 0)])
            elif girl.face == 6:
                imgs.append(["alice st16b", (x, 0)])
            elif girl.face == 7:
                imgs.append(["alice st17b", (x, 0)])
            elif girl.face == 8:
                imgs.append(["alice st18b", (x, 0)])
            elif girl.face == 9:
                imgs.append(["alice st19b", (x, 0)])

        elif girl.pose == 5:
            if girl.face == 1:
                imgs.append(["alice st21", (x, 0)])
            elif girl.face == 2:
                imgs.append(["alice st22", (x, 0)])
        elif girl.pose == 6:
            x -= 80

            if girl.face == 1:
                imgs.append(["alice st21b", (x, 0)])
            elif girl.face == 2:
                imgs.append(["alice st22b", (x, 0)])

        elif girl.pose == 7:
            if girl.face == 1:
                imgs.append(["alice st23", (x, 0)])
            elif girl.face == 2:
                imgs.append(["alice st24", (x, 0)])

        elif girl.pose == 8:
            x += 80

            if girl.face == 1:
                imgs.append(["alice st31", (x, 125)])

        return imgs

    def granberia_ng_pose(girl, x):
        imgs = []

        if girl.pose == 1 and girl.face == 8:
            girl.face = 1
        elif girl.pose == 2 and girl.face == 4:
            girl.face = 1
        elif girl.pose == 3 and girl.face == 3:
            girl.face = 1
        elif girl.pose == 4 and girl.face == 4:
            girl.face = 1
        elif girl.pose == 5:
            girl.pose = 1

        if girl.pose == 1:
            if girl.face == 1:
                imgs.append(["granberia st01", (x, 0)])
            elif girl.face == 2:
                imgs.append(["granberia st02", (x, 0)])
            elif girl.face == 3:
                imgs.append(["granberia st03", (x, 0)])
            elif girl.face == 4:
                imgs.append(["granberia st04", (x, 0)])
            elif girl.face == 5:
                imgs.append(["granberia st05", (x, 0)])
            elif girl.face == 6:
                imgs.append(["granberia st06", (x, 0)])
            elif girl.face == 7:
                imgs.append(["granberia st07", (x, 0)])

            if girl.bk[0]:
                imgs.append(["granberia bk01", (x, 0)])
            if girl.bk[1]:
                imgs.append(["granberia bk02", (x, 0)])
            if girl.bk[2]:
                imgs.append(["granberia bk03", (x, 0)])

        elif girl.pose == 2:
            if girl.face == 1:
                imgs.append(["granberia st11", (x, 0)])
            elif girl.face == 2:
                imgs.append(["granberia st12", (x, 0)])
            elif girl.face == 3:
                imgs.append(["granberia st13", (x, 0)])

        elif girl.pose == 3:
            if girl.face == 1:
                imgs.append(["granberia st41", (x, 0)])
            elif girl.face == 2:
                imgs.append(["granberia st42", (x, 0)])

        elif girl.pose == 4:
            if girl.face == 1:
                imgs.append(["granberia st51", (x, 0)])
            elif girl.face == 2:
                imgs.append(["granberia st52", (x, 0)])
            elif girl.face == 3:
                imgs.append(["granberia st53", (x, 0)])

        return imgs

    def queenharpy_ng_pose(girl, x):
        imgs = []

        if girl.pose == 1 and girl.face == 5:
            girl.face = 1
        elif girl.pose == 2 and girl.face == 5:
            girl.face = 1
        elif girl.pose == 3:
            girl.pose = 1

        if girl.pose == 1:
            if girl.face == 1:
                imgs.append(["queenharpy st11", (x, 0)])
            elif girl.face == 2:
                imgs.append(["queenharpy st12", (x, 0)])
            elif girl.face == 3:
                imgs.append(["queenharpy st13", (x, 0)])
            elif girl.face == 4:
                imgs.append(["queenharpy st14", (x, 0)])

        elif girl.pose == 2:
            if girl.face == 1:
                imgs.append(["queenharpy st21", (x, 0)])
            elif girl.face == 2:
                imgs.append(["queenharpy st22", (x, 0)])
            elif girl.face == 3:
                imgs.append(["queenharpy st23", (x, 0)])
            elif girl.face == 4:
                imgs.append(["queenharpy st24", (x, 0)])

            if girl.bk[0]:
                imgs.append(["queenharpy bk02", (x, 0)])
            if girl.bk[1]:
                imgs.append(["queenharpy bk03", (x, 0)])
            if girl.bk[2]:
                imgs.append(["queenharpy bk04", (x, 0)])
            if girl.bk[3]:
                imgs.append(["queenharpy bk05", (x, 0)])

        return imgs

    def tamamo_ng_pose(girl, x):
        imgs = []

        if girl.pose == 1 and girl.face == 4:
            girl.face = 1
        elif girl.pose == 2 and girl.face == 4:
            girl.face = 1
        elif girl.pose == 3 and girl.face == 7:
            girl.face = 1
        elif girl.pose == 4 and girl.face == 8:
            girl.face = 1
        elif girl.pose == 5:
            girl.pose = 1

        if girl.pose == 1:
            if girl.face == 1:
                imgs.append(["tamamo st51", (x, 0)])
            elif girl.face == 2:
                imgs.append(["tamamo st52", (x, 0)])
            elif girl.face == 3:
                imgs.append(["tamamo st53", (x, 0)])

        elif girl.pose == 2:
            if girl.face == 1:
                imgs.append(["tamamo st61", (x, 0)])
            elif girl.face == 2:
                imgs.append(["tamamo st62", (x, 0)])
            elif girl.face == 3:
                imgs.append(["tamamo st63", (x, 0)])

        elif girl.pose == 3:
            if girl.face == 1:
                imgs.append(["tamamo st11", (x, 0)])
            elif girl.face == 2:
                imgs.append(["tamamo st12", (x, 0)])
            elif girl.face == 3:
                imgs.append(["tamamo st13", (x, 0)])
            elif girl.face == 4:
                imgs.append(["tamamo st14", (x, 0)])
            elif girl.face == 5:
                imgs.append(["tamamo st15", (x, 0)])
            elif girl.face == 6:
                imgs.append(["tamamo st16", (x, 0)])

        elif girl.pose == 4:
            if girl.face == 1:
                imgs.append(["tamamo st01", (x, 0)])
            elif girl.face == 2:
                imgs.append(["tamamo st02", (x, 0)])
            elif girl.face == 3:
                imgs.append(["tamamo st03", (x, 0)])
            elif girl.face == 4:
                imgs.append(["tamamo st04", (x, 0)])
            elif girl.face == 5:
                imgs.append(["tamamo st05a", (x, 0)])
            elif girl.face == 6:
                imgs.append(["tamamo st06", (x, 0)])
            elif girl.face == 7:
                imgs.append(["tamamo st09", (x, 0)])

            if girl.bk[0]:
                imgs.append(["tamamo bk01", (x, 0)])
            if girl.bk[1]:
                imgs.append(["tamamo bk02", (x, 0)])
            if girl.bk[2]:
                imgs.append(["tamamo bk03", (x, 0)])

        return imgs
