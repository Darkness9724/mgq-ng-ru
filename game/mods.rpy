init offset = -1

init python:
    mods = []
    mod_api = {
        "3.3.2+0": 1,
        "3.3.2+1": 1,
        "3.3.2+2": 2,
    }

    class Mod:
        def __init__(self, name, id, start, api):
            self.name = name
            self.id = id

            if id is not None:
                mods.append(self)

            self.start = start
            self.api = api
            self.state = None

        def initialization(self):
            renpy.music.stop()
            self.state = World()
            self.state.init(mod=True)
            globals()["world"] = self.state

        def image(self, name, d):
            if d is None:
                raise Exception("Images may not be declared to be None.")

            if not isinstance(name, tuple):
                name = tuple(name.split())

            d = renpy.easy.displayable(d)
            renpy.display.image.register_image(name, d)

        def index(self):
            config.search_prefixes.append(f"mods/{self.id}/") if f"mods/{self.id}/" not in config.search_prefixes else config.search_prefixes
            config.search_prefixes.append(f"mods/{self.id}/images/") if f"mods/{self.id}/images/" not in config.search_prefixes else config.search_prefixes
            config.search_prefixes.append(f"mods/{self.id}/audio/") if f"mods/{self.id}/audio/" not in config.search_prefixes else config.search_prefixes

            for asset in renpy.list_files():
                if asset.startswith(f"mods/{self.id}/images/"):
                    self.image(os.path.splitext(os.path.basename(asset))[0], asset)

