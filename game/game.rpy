init -1 python:
    def game_update():
        _world = World()
        _party = Party(_world)
        _player = Player(_world)
        _map = Map(_world)

        diff = set(dir(_world)) - set(dir(world))
        for attr in diff:
            setattr(world, attr, getattr(World(), attr))

        diff = set(dir(_party)) - set(dir(world.party))
        for attr in diff:
            setattr(world.party, attr, getattr(_party, attr))

        diff = set(dir(_player)) - set(dir(world.party.player))
        for attr in diff:
            setattr(world.party.player, attr, getattr(_player, attr))

        diff = set(dir(_map)) - set(dir(world.map))
        for attr in diff:
            setattr(world.map, attr, getattr(_map, attr))

    class World:
        def __init__(self):
            self.actor = [None] * 10
            self.party = Party(self)
            self.battle = None
            self.battle_skip = False
            self.troops = [None] * 100
            self.stage = [0] * 20
            self.city = City(self)
            self.map = Map(self)
            self.prev_region = 0
            self.region = 0
            self.region_name = ""
            self.wind = 0
            self.earth = 0
            self.aqua = 0
            self.fire = 0
            self.environment = 0
            self.background = ""
            self.event = {
                # 1 Глава
                "iliasburg": [False] * 3,
                "enrika": [False] * 2,
                "irina": False,
                "harpy": [False] * 3,
                "iliasport": False,
                # 2 Глава
                "ipea": False,
                # 3 Глава
                "spring": [False] * 3,
                "pyramid": False,
                "sabasa": 0,
                "sara": False,
                "natalia": False,
                "succubi": False,
                "fairyisland": False
            }
            self.rape = {
                "queenharpy": 0,
                "sylph": 0,
                "micaela": False,
                "sara": False
            }
            # 1 - Алиса, 2 - Гранберия, 3 - Тамамо, 4 - Альма, 5 - Эрубети, 6 - Сильфа, 7 - Гнома, 8 - Ундина, 9 - Саламандра
            self.scene = [0] * 10
            self.nanabi_fight = False
            self.tamamo_fight = 0
            self.tamamo_tail = False
            self.granberia_rematch = False
            self.wait_alma = False
            self.sylph_hat = False
            self.salamander_event = 0
            self.afterend = False
            self.micaela_evaluations = 0

        # Реальная инициализация. По факту всё это можно было запихнуть и в __init__, но это нарушает концепцию конструктора.
        def init(self, mod=False):
            self.actor[0] = Player(self)

            self.actor[0].name = [Character("Лука"), Character("Луки"), Character("Луке"), Character("Луку"), Character("Лукой")]
            self.actor[0].level = 80
            self.actor[0].level_sin = self.actor[0].level
            self.actor[0].exp = 21999000
            self.actor[0].calc()
            self.actor[0].skillset = 1
            self.actor[0].skill[0] = 25

            self.party.member[0] = self.actor[0]
            self.party.player = self.party.member[0]

            self.party.item[0] = 1
            self.party.item[1] = 1

            if not mod:
                result = renpy.call_screen("setup")

                if result[0] > 1:
                    if result[1]:
                        world.party.join(result[1], alarm=0)

                    if result[2]:
                        world.party.join(6, alarm=0)

                    if result[3]:
                        world.party.join(7, alarm=0)

                    if result[4]:
                        world.party.join(8, alarm=0)

                    if result[5]:
                        world.party.join(9, alarm=0)

                    renpy.jump(f"part0{result[0]}")

        # Обнуление временных переменных
        def stage_null(self):
            self.stage = [0 for x in self.stage]

        # Усиление духов от окружения
        def buff(self):
            if self.party.player.skill[2] != 0:
                if self.earth == 1:
                    self.party.player.gnome_buff = self.party.player.gnome_buff*(110/100)
                elif self.earth == 2:
                    self.party.player.gnome_buff = self.party.player.gnome_buff*(130/100)
                elif self.earth == 3:
                    self.party.player.gnome_buff = self.party.player.gnome_buff*(150/100)

            if self.party.player.skill[3] != 0:
                if self.aqua == 1:
                    self.party.player.undine_buff = self.party.player.undine_buff+10
                elif self.aqua == 2:
                    self.party.player.undine_buff = self.party.player.undine_buff+20
                elif self.aqua == 3:
                    self.party.player.undine_buff = self.party.player.undine_buff+30

                if self.fire == 1:
                    self.party.player.undine_buff = self.party.player.undine_buff-10
                elif self.fire == 2:
                    self.party.player.undine_buff = self.party.player.undine_buff-20
                elif self.fire == 3:
                    self.party.player.undine_buff = self.party.player.undine_buff-30

            if self.party.player.skill[1] != 0:
                if self.wind == 1:
                    self.party.player.sylph_buff = self.party.player.sylph_buff+15
                elif self.wind == 2:
                    self.party.player.sylph_buff = self.party.player.sylph_buff+25
                elif self.wind == 3 or self.holy == 3:
                    self.party.player.sylph_buff = self.party.player.sylph_buff+35

            if self.party.player.skill[4] != 0:
                if self.fire == 1:
                    self.party.player.salamander_buff = self.party.player.salamander_buff*(125/100)
                elif self.fire == 2:
                    self.party.player.salamander_buff = self.party.player.salamander_buff*(150/100)
                elif self.fire == 3:
                    self.party.player.salamander_buff = self.party.player.salamander_buff*(175/100)

                if self.aqua == 1:
                    self.party.player.salamander_buff = self.party.player.salamander_buff-10
                elif self.aqua == 2:
                    self.party.player.salamander_buff = self.party.player.salamander_buff-20
                elif self.aqua == 3:
                    self.party.player.salamander_buff = self.party.player.salamander_buff-30

        def achivements(self, part):
            if persistent.game_clear < part:
                persistent.game_clear = part

            if part == 1:
                if world.actor[2] is not None:
                    if world.actor[2].rep > 3:
                        persistent.granberia_maxrep = True

                    elif world.actor[2].rep < 0:
                        persistent.granberia_minrep = True

                if world.actor[3] is not None:
                    if world.actor[3].rep > 3:
                        persistent.tamamo_maxrep = True

            if persistent.granberia_hell and persistent.queenhapy_hell and persistent.tamamo_hell and persistent.granberia_maxrep and persistent.granberia_minrep and persistent.tamamo_maxrep and persistent.starblade and persistent.nine_moons_won and persistent.tamamo_knows:
                persistent.ng1_achi = True

        def change_difficulty(self):
            persistent.difficulty = renpy.display_menu([
                ("Нормальная", 1),
                ("Тяжёлая", 2),
                ("Адская", 3)
            ])

        def set_region(self, id):
            self.region = id
            self.map.region = id

            region_name = {
                1: "Континент Илиас",
                2: "Наталия",
                3: "Сафина",
                4: "Гранд Ной",
                5: "Золотой регион"
            }

            self.region_name = region_name[id]

        def ejaculation(self):
            renpy.sound.play("audio/se/syasei.ogg")
            for _ in range(4):
                renpy.with_statement(syasei1)

        def summon(self, img):
            store._skipping = False
            renpy.show(img, at_list=[summon_effect], tag='summon', zorder=30)
            renpy.pause(4.0, hard=True)
            renpy.hide('summon')
            store._skipping = True

    class Party:
        def __init__(self, world):
            self.world = world
            self.player = None
            self.player_state = None
            self.member = [None] * 5
            self.sylph_rep = 0
            self.gnome_rep = 0
            self.undine_rep = 0
            self.salamander_rep = 0
            self.sylph_order = 0
            self.gnome_order = 0
            self.undine_order = 0
            self.salamander_order = 0
            self.support_turn = 0
            self.knight_previous = 0
            self.knight_places = {}
            self.party1_places = {}
            self.party2_places = {}
            self.pentagram = False
            self.pentagram_turn = 0
            self.item = [0] * 14
            self.orb = [False] * 6

        def check(self, id):
            if self.world.actor[id] is None:
                return False

            if id < 2: # Лука и Алиса
                return self.member[id] == self.world.actor[id]
            elif 1 < id < 6: # Гранберия / Тамамо / Альма / Эрубети
                return self.member[2] == self.world.actor[id]
            elif id > 5: # Ниби / Ипея / Курому / Сара
                return self.member[3] == self.world.actor[id] or self.member[4] == self.world.actor[id]

        def join(self, id, kind=1, alarm=1):
            if self.world.actor[id] is None:
                self.world.actor[id] = Player(self.world)

            # Именительный, Родительный, Винительный, Дательный, Творительный
            if id == 1:
                self.world.actor[id].name = [Character("Алиса"), Character("Алисы"), Character("Алису"), Character("Алисе"), Character("Алисой")]
                self.member[1] = self.world.actor[id]
            if 1 < id < 6:
                if id == 2:
                    self.world.actor[id].name = [Character("Гранберия"), Character("Гранберии"), Character("Гранберию"), Character("Гранберии"), Character("Гранберией")]
                    if self.world.granberia_rematch:
                        self.world.actor[id].rep += 1
                elif id == 3:
                    self.world.actor[id].name = [Character("Тамамо")] * 6
                    if self.world.tamamo_tail:
                        self.world.actor[id].rep += 1
                    if self.world.tamamo_fight in (1, 2):
                        self.world.actor[id].rep += 1
                    elif self.world.tamamo_fight == 3:
                        self.world.actor[id].rep += 2
                elif id == 4:
                    self.world.actor[id].name = [Character("Альма Эльма"), Character("Альмы Эльмы"), Character("Альму Эльму"), Character("Альме Эльме"), Character("Альмой Эльмой")]
                    if self.world.wait_alma:
                        self.world.actor[id].rep += 1
                    else:
                        self.world.actor[id].rep -= 1
                elif id == 5:
                    self.world.actor[id].name = [Character("Эрубети")] * 6
                    if self.world.event["spring"][0] == 2:
                        self.world.actor[id].rep += 1
                    elif self.world.event["spring"][0] == 1:
                        self.world.actor[id].rep -= 1

                if self.member[2] is not None:
                    self.knight_previous = self.member[2]

                self.member[2] = self.world.actor[id]
            elif id == 6:
                self.world.actor[id].name = [Character("Кицунэ")] + [Character("кицунэ")] * 5

                if self.member[3] is None:
                    self.member[3] = self.world.actor[id]
                elif self.member[4] is None:
                    self.member[4] = self.world.actor[id]
            elif id == 7:
                self.world.actor[id].name = [Character("Гарпия"), Character("гарпии"), Character("гарпии"), Character("гарпию"), Character("гарпией")]

                if self.member[3] is None:
                    self.member[3] = self.world.actor[id]
                elif self.member[4] is None:
                    self.member[4] = self.world.actor[id]
            elif id == 8:
                self.world.actor[id].name = [Character("Курому")] * 6

                if self.member[3] is None:
                    self.member[3] = self.world.actor[id]
                elif self.member[4] is None:
                    self.member[4] = self.world.actor[id]
            elif id == 9:
                self.world.actor[id].name = [Character("Сара"), Character("Сары"), Character("Сару"), Character("Саре"), Character("Сарой")]

                if self.member[3] is None:
                    self.member[3] = self.world.actor[id]
                elif self.member[4] is None:
                    self.member[4] = self.world.actor[id]

            if alarm:
                if kind == 1:
                    renpy.sound.play("se/fanfale2.ogg")
                    narrator(f"{self.world.actor[id].name[0].name} присоединилась к группе!")
                elif kind == 2:
                    renpy.sound.play("se/lvup.ogg")
                    narrator(f"{self.world.actor[id].name[0].name} получена!")

        def remember(self, place):
            self.knight_places[place] = self.member[2]
            self.party1_places[place] = self.member[3]
            self.party2_places[place] = self.member[4]

        def assisted(self, id, place):
            if place in self.knight_places or place in self.party1_places or place in self.party2_places:
                return self.check(id) and (self.knight_places[place] == self.world.actor[id]
                                        or self.party1_places[place] == self.world.actor[id]
                                        or self.party2_places[place] == self.world.actor[id])

        # Подставляет нужные переменные в имена переменных
        def spirit_order(self, name):
            order = self.player.skill[1:5].count(3)
            setattr(self, f"{name}_order", order)

            if order == 1:
                setattr(self, f"{name}_rep", getattr(self, f"{name}_rep") + 2)
            elif order == 2:
                setattr(self, f"{name}_rep", getattr(self, f"{name}_rep") + 1)

    class City:
        def __init__(self, world):
            self.world = world
            self.id = ""
            self.prev = ""
            self.background = ""
            self.specials = False
            self.button = {1: None, 2: None, 3: None, 4: None, 5: None, 6: None, 7: None,
                        8: None, 9: None, 10: None, 11: None, 12: None, 13: None, 14: None,
                        15: None, 16: None, 17: None, 18: None}
            self.action = [0] * 19
            self.exit = None
            self.result = 0

        def call_cmd(self):
            renpy.choice_for_skipping()
            while True:
                if self.specials:
                    unlocked = sum(1 for idx, button in self.button.items() if self.action[idx] and button is not None)
                    if unlocked == len_without_none(self.button.values()):
                        renpy.jump("campspecial")

                renpy.start_predict_screen("city")
                _window_hide()
                renpy.scene()
                renpy.show(self.background)
                renpy.with_statement(dissolve)
                self.result = renpy.call_screen("city")
                _window_show(auto=True)
                renpy.stop_predict_screen("city")

                if 1 <= self.result <= 18:
                    renpy.call(f"{self.id}_{self.result:02}", from_current=True)
                elif self.result == 19:
                    return

        def jump(self, key):
            return "city%d_%d" % (self.id, key)

        def call(self, tag):
            self.null()
            renpy.call(tag)

        def null(self):
            self.button = {
                key: None
                for key, value in self.button.items()
            }
            self.action = [0] * 19
