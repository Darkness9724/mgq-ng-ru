vec2 new_pos;
vec2 wave_offset = vec2(0.0, 0.0);
vec2 melt_offset = vec2(0.0, 0.0);
vec2 double_offset = vec2(0.0, 0.0);
vec2 double_pos = vec2(0.0, 0.0);
vec2 damp = vec2(1.0, 1.0);
vec4 new_color;
vec4 double_color;

if (u_damp.x >= 0.0 && u_damp.x != 1.0) {damp.x = pow(u_damp.x, v_coords.y * u_model_size.y);}
else if (u_damp.x < 0.0) {damp.x = pow((-1.0 * u_damp.x), (1.0 - v_coords.y) * u_model_size.y);}
if (u_damp.y >= 0.0 && u_damp.y != 1.0) {damp.y = pow(u_damp.y, v_coords.x * u_model_size.x);}
else if (u_damp.y < 0.0) {damp.y = pow((-1.0 * u_damp.y), (1.0 - v_coords.x) * u_model_size.x);}

if (u_direction < 2.0) {wave_offset.x = sin( u_wave_period.x * (v_coords.y + (u_shader_time * u_wave_speed.x))) * u_wave_amp.x * 0.01 * damp.x;}
else {wave_offset.x = 0.0;}
if (u_direction < 1.0 || u_direction >= 2.0) {wave_offset.y = sin( u_wave_period.y * (v_coords.x + (u_shader_time * u_wave_speed.y))) * u_wave_amp.y * 0.01 * damp.y;}
else {wave_offset.y = 0.0;}

if (u_melt >= 0.0){
    if (u_melt < 2.0) {melt_offset.x = sin( u_melt_params_x.x * (v_coords.x + (u_shader_time * u_melt_params_x.z))) * u_melt_params_x.y * 0.01 * damp.x;}
    else {melt_offset.x = 0.0;}
    if (u_melt < 1.0 || u_melt >= 2.0) {melt_offset.y = sin( u_melt_params_y.x * (v_coords.y + (u_shader_time * u_melt_params_y.z))) * u_melt_params_y.y * 0.01 * damp.y;}
    else {melt_offset.y = 0.0;}
}
new_pos.x = v_coords.x + wave_offset.x + melt_offset.x;
new_pos.y = v_coords.y + wave_offset.y + melt_offset.y;

new_color = texture2D(tex0,new_pos);

if (u_double < 0.0) {gl_FragColor = new_color;}
else{
    if (u_double < 2.0) {double_offset.x = sin( u_double_params_x.x * (v_coords.y + (u_shader_time * u_double_params_x.z))) * u_double_params_x.y * -0.01 * damp.x;}
    else {double_offset.x = 0.0;}
    if (u_double < 1.0 || u_double >= 2.0) {double_offset.y = sin( u_double_params_y.x * (v_coords.x + (u_shader_time * u_double_params_y.z))) * u_double_params_y.y * -0.01 * damp.y;}
    else {double_offset.y = 0.0;}
    double_pos.x = v_coords.x + double_offset.x + melt_offset.x;
    double_pos.y = v_coords.y + double_offset.y + melt_offset.y;
    double_color = texture2D(tex0,double_pos);
    gl_FragColor = mix(new_color, double_color, 0.5);
}
