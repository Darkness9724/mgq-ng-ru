#############################################################################
# Моды
#############################################################################
screen choice_spinoff():
    tag menu
    modal True

    add "bg 046"
    add gui.game_menu_background

    $ tooltip = GetTooltip()

    frame:
        style_group "spinoff"

        has vpgrid:
            rows len(mods)
            spacing 15
            xfill True
            if len(mods) > 9:
                scrollbars "vertical"
                mousewheel True
                draggable True

        for mod in mods:
            if mod.api != mod_api[config.version]:
                textbutton "{color=#F00}[mod.name]{/color}":
                    action NullAction()
                    tooltip "Мод использует неверную версию API"

                $ mod = None

            elif mod.start is not None:
                textbutton "[mod.name]":
                    action [Function(mod.index), Start(mod.start)]

    if tooltip is not None:
        text "[tooltip]" align (.03, .99)

    use title("Побочные истории")
    use return_button("extras")

style spinoff_frame:
    xfill True
    yfill True
    margin (10, 60)
    padding (10, 10)
    background Frame(persistent.win_bg, 5, 5)

style spinoff_button:
    background None
    # xpadding 0
    # xmargin 0
    xalign .5

style spinoff_button_text:
    is text
    xalign 0
    size 26
    font "Arciform.otf"
    idle_color "#fff000"
    idle_outlines [(2, '#000c')]
    hover_color "#000fff"
    hover_outlines [(2, '#000c')]
    insensitive_color "#8886"
    insensitive_outlines [(2, "#0008")]

style spinoff_vscrollbar:
    is pref_vscrollbar
