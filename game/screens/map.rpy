screen map0():
    add "map 0"

    style_prefix "map"
    textbutton "Регион Наталия" pos (450, 250) action Return(1)
    textbutton "Регион Сафина" pos (200, 300) action Return(2)
    textbutton "Регион Ноа" pos (530, 160) action Return(3)
    textbutton "Золотой регион" pos (320, 130) action Return(4)

screen map1():
    add "map 1"

    style_prefix "map"
    textbutton "Илиасбург" pos (370, 280) action Return(1)
    textbutton "Илиаспорт" pos (250, 40) action Return(2)
    textbutton "Горы Ирины" pos (180, 240) action Return(3)
    textbutton "Деревня Счастья" pos (630, 180) action Return(4)
    textbutton "Энрика" pos (220, 390) action Return(5)
    textbutton "Илиасвилль" pos (360, 470) action Return(6)

    if not world.event["iliasport"]:
        text "Рек. уровень: 81" pos (250, 60)
    elif world.event["iliasport"]:
        text "(На Сентору)" pos (250, 60)

    if not world.event["irina"]:
        text "Рек. уровень: ???" pos (160, 260)
    elif world.event["irina"]:
        text "Выполнено" pos (190, 260)

    if not world.event["harpy"][0]:
        text "Рек. уровень: ???" pos (630, 200)
    elif world.event["harpy"][0]:
        text "Выполнено" pos (660, 200)

    if not world.event["enrika"][0]:
        text "Рек. уровень: ???" pos (220, 410)
    elif world.event["enrika"][0]:
        text "Выполнено" pos (250, 410)

screen map1_1():
    add "map 1"

    style_prefix "map"
    text "Илиасбург" pos (370, 280) style "map_button_text"
    text "Илиаспорт" pos (250, 40) style "map_button_text"
    text "Горы Ирины" pos (180, 240) style "map_button_text"
    text "Деревня Счастья" pos (630, 180) style "map_button_text"
    text "Энрика" pos (220, 390) style "map_button_text"
    text "Илиасвилль" pos (360, 470) style "map_button_text"
    textbutton "Пещера Сокровищ" pos (430, 50) action Return(7)

screen map2():
    add "map 2"
    style_prefix "map"

    if not world.party.player.skill[1]:
        textbutton "Лес духов" pos (260, 60) action Return(1)
        text "Рек. уровень: 84" pos (220, 80)
    else:
        textbutton "{color=00C957}Лес духов{/color}" pos (260, 60) action Return(1)
        text "{color=00C957}ЗАВЕРШЕНО{/color}" pos (220, 80)

    textbutton "Карта мира" pos (600, 550) action Return(9)
    key "game_menu" action Return(9)

screen map3():
    add "map 3"
    style_prefix "map"

    if not world.party.player.skill[2]:
        textbutton "Руины Сафины" pos (350, 60) action Return(1)
        text "Рек. уровень: 83" pos (330, 80)
    else:
        textbutton "{color=F4A460}Руины Сафины{/color}" pos (350, 60) action Return(1)
        text "{color=F4A460}ЗАВЕРШЕНО{/color}" pos (330, 80)

    if world.party.player.skill[1:5].count(3) == 4 and not world.event["pyramid"]:
        textbutton "Пирамида" pos (130, 300) action Return(2)
        text "Рек. уровень: 88" pos (100, 320)
    else:
        textbutton "{color=FF4444}Пирамида{/color}" pos (130, 300) action Return(2)
        text "{color=FF4444}ЗАВЕРШЕНО{/color}" pos (100, 320)

    textbutton "Карта мира" pos (600, 550) action Return(9)
    key "game_menu" action Return(9)

screen map4():
    add "map 4"
    style_prefix "map"

    if not world.party.player.skill[3]:
        textbutton "Источник Ундины" pos (360, 490) action Return(1)
        text "Рек. уровень: 85" pos (340, 510)
    elif world.party.player.skill[1:5].count(3) == 4 and not world.event["spring"][0]:
        textbutton "Источник Ундины" pos (360, 490) action Return(1)
        text "{color=00BFFF}Рек. уровень: ???{/color}" pos (340, 510)
    else:
        textbutton "{color=00BFFF}Источник Ундины{/color}" pos (360, 490) action Return(1)
        text "{color=00BFFF}ЗАВЕРШЕНО{/color}" pos (340, 510)

    textbutton "Карта мира" pos (600, 550) action Return(9)
    key "game_menu" action Return(9)

screen map5():
    add "map 5"
    style_prefix "map"

    if not world.party.player.skill[4]:
        textbutton "Золотой вулкан" pos (380, 410) action Return(1)
        text "Рек. уровень: 86" pos (380, 430)
    else:
        textbutton "{color=FF6347}Золотой вулкан{/color}" pos (380, 410) action Return(1)
        text "{color=FF6347}ЗАВЕРШЕНО{/color}" pos (380, 430)

    textbutton "Карта мира" pos (600, 550) action Return(9)
    key "game_menu" action Return(9)

style map_button:
    is button
    xpadding 0
    xmargin 0
    background None

style map_button_text:
    is default
    font "MarckScript-Regular.ttf"
    size 22
    idle_color "#fff"
    hover_color "#000fff"
    insensitive_color "#8888"
    idle_outlines [(2, "#000c", 0, 0)]
    hover_outlines [(2, "#000c", 0, 0)]
    insensitive_outlines [(2, "#303030cc", 0, 0)]

style map_text:
    is default
    font "MarckScript-Regular.ttf"
    size 16
    line_leading 10
    # bold True
    color "#fff"
    outlines [(1, "#000c", 0, 0)]
