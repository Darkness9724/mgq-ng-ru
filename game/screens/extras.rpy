#############################################################################
# Меню со всякой всячиной
#
#
screen extras(enter="extras"):
    tag menu

    add Solid("#000")

    style_prefix "extras"

    frame:
        vbox:
            textbutton "Монстропедия":
                activate_sound "se/m_papier02.opus"
                action ShowMenu("pedia_%s" % persistent.pedia_view, enter)
            textbutton "Дневник" action ShowMenu("record", enter)
            textbutton "Музыка" action ShowMenu("music_room")
            textbutton "Побочные истории" action ShowMenu("choice_spinoff")
            if renpy.variant("pc") or (renpy.variant("web") and not renpy.variant("mobile")):
                textbutton "Помощь" action ShowMenu("help", enter)
            textbutton "Об игре" action ShowMenu("about")
            textbutton "Назад" action [Play("music", config.main_menu_music),
                                        Return()]

    key "game_menu" action [Play("music", config.main_menu_music),
                                        Return()]

style extras_frame:
    background Frame(persistent.win_bg, 5, 5)
    padding (20, 20)
    align (0.5, 0.5)

style extras_button:
    is default
    xalign .5
    background None

style extras_button_text:
    is default
    font "Arciform.otf"
    outlines [(2, "#001", 0, 0)]
    size 23
    idle_color "#fff"
    hover_color "#00f"
    insensitive_color "#8886"
    insensitive_outlines [(2, "#0008", 0, 0)]

style extras_button_text:
    variant "touch"
    size 28

#############################################################################
# Помощь
#
# Показывает управление клавиатурой, мышкой и геймпадом.
screen help(enter=None):
    tag menu

    default device = "keyboard"

    if main_menu:
        add Solid("#000")
    else:
        add renpy.get_screen("bg", layer='master')

    add gui.game_menu_background

    style_prefix "help"

    frame:
        has viewport:
            mousewheel True
            draggable True
            yinitial 1.0

        vbox:
            spacing 15
            if device == "keyboard":
                use keyboard_help
            elif device == "mouse":
                use mouse_help
            elif device == "gamepad":
                use gamepad_help
            elif device == "developer":
                use developer_help

    hbox:
        align (0.05, .97)
        spacing 15

        textbutton "Клавиатура":
            action SetScreenVariable("device", "keyboard")
            style "save_load_folder_button"
        textbutton "Мышка":
            action SetScreenVariable("device", "mouse")
            style "save_load_folder_button"

        if GamepadExists():
            textbutton "Геймпад":
                action SetScreenVariable("device", "gamepad")
                style "save_load_folder_button"

        if config.developer:
            textbutton "Отладка":
                action SetScreenVariable("device", "developer")
                style "save_load_folder_button"

    use return_button(enter)
    use title("Помощь")

screen keyboard_help():
    hbox:
        label "Enter"
        text "Перейти к следующиму диалогу и включить интерфейс."

    hbox:
        label "Space"
        text "Перейти к следующиму диалогу с пропуском выбора."

    hbox:
        label "Стрелочки"
        text "Навигация по интерфейсу."

    hbox:
        label "Escape"
        text "В главное меню."

    hbox:
        label "Ctrl"
        text "Пропустить диалоги при удержании."

    hbox:
        label "Tab"
        text "Включить/выключить пропуск диалогов."

    hbox:
        label "Page Up"
        text "Перелистнуть на более ранний диалог."

    hbox:
        label "Page Down"
        text "Перелистнуть на более поздний диалог."

    hbox:
        label "H"
        text "Спрятать интерфейса."

    hbox:
        label "S"
        text "Сделать скриншот."

    hbox:
        label "F1"
        text "Открыть эту помощь."

    hbox:
        label "F5"
        text "Быстрое сохранение."

    hbox:
        label "F6"
        text "Быстрая загрузка."

    hbox:
        label "F11"
        text "Полноэкранный/оконный режим."

screen mouse_help():
    hbox:
        label "Левая кнопка"
        text "Перейти к следующиму диалогу и включить интерфейс."

    hbox:
        label "Средняя кнопка"
        text "Спрятать интерфейса."

    hbox:
        label "Правая кнопка"
        text "В главное меню."

    hbox:
        label "Колёсико вверх"
        text "Перелистнуть на более ранний диалог."

    hbox:
        label "Колёсико вниз"
        text "Перелистнуть на более поздний диалог."


screen gamepad_help():

    hbox:
        label "Правый триггер\nA/кнопка"
        text "Перейти к следующиму диалогу и включить интерфейс."

    hbox:
        label ("Левый триггер\nЛевый Shoulder")
        text "Перелистнуть на более ранний диалог."

    hbox:
        label "Правый Shoulder"
        text "Перелистнуть на более поздний диалог."

    hbox:
        label "D-Pad, стик"
        text "Навигация по интерфейсу."

    hbox:
        label "Start, Guide"
        text "В главное меню."

    hbox:
        label "Y/Верхняя кнопка"
        text "Спрятать интерфейса."

    textbutton "Калибровка" action GamepadCalibrate()

screen developer_help():
    hbox:
        label "Shift + D"
        text "Инструменты разработчика."

    hbox:
        label "Shift + O"
        text "Консоль."

    hbox:
        label "Shift + R"
        text "Перекомпиляция скриптов и перезагрузка игры."

    hbox:
        label "Shift + I"
        text "Свойства открытых экранов."

    hbox:
        label "Shift + G"
        text "Настройка отрисовки."

    hbox:
        label "Shift + A"
        text "Настройка шрифтов и синтезатора."

    hbox:
        label "Shift + E"
        text "Открыть скрипт текущей точки останова в системном редакторе."

    hbox:
        label "Shift + С"
        text "Включить/выключить копирование всего текста на экране."

    hbox:
        label "Shift + ."
        text "Мгновенный пропуск всех диалогов до важного события."

    hbox:
        label "F3"
        text "Задержки отрисовки экранов."

    hbox:
        label "F4"
        text "Счётчик отрисовки и кэширования."

style help_button is gui_button
style help_button_text is gui_button_text
style help_label is gui_label
style help_label_text is gui_label_text
style help_text is gui_text

style help_button:
    properties gui.button_properties("help_button")
    xmargin 8

style help_button_text:
    properties gui.button_text_properties("help_button")

style help_label:
    xsize 250
    right_padding 20

style help_label_text:
    font "DejaVuSansMono.ttf"
    size gui.text_size
    xalign 1.0
    text_align 1.0

style help_frame:
    background Frame(persistent.win_bg, 5, 5)
    xfill True
    yfill True
    margin (50, 60)
    padding (10, 10)

style help_vscrollbar:
    is pref_vscrollbar

## Экран Об игре ###############################################################
##
## Этот экран показывает авторскую информацию об игре и Ren'Py.
##
## В этом экране нет ничего особенного, и он служит только примером того, каким
## можно сделать свой экран.

screen about():
    tag menu
    add gui.main_menu_background
    add gui.game_menu_background
    style_prefix "about"

    frame:
        vbox:
            label "[config.name!t]"
            if renpy.emscripten:
                $ renpy.browser_name = emscripten.run_script_string("navigator.userAgent.split(' ').pop()")

                text "Версия [config.version!t], {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only] на [renpy.browser_name], {a=https://python.org/}Python{/a} [sys.version_info.major].[sys.version_info.minor], [renpy.platform]\n"
            else:
                $ renpy.display_driver = pygame_sdl2.display.get_driver().decode("UTF-8")
                $ renpy.sdl_version = f"{pygame_sdl2.get_sdl_version()[0]}.{pygame_sdl2.get_sdl_version()[1]}.{pygame_sdl2.get_sdl_version()[2]}"
                $ mod_version = mod_api[config.version]

                text "Версия [config.version!t] (API v[mod_version]), {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only] на SDL [renpy.sdl_version] ([renpy.display_driver]), {a=https://python.org/}Python{/a} [sys.version_info.major].[sys.version_info.minor]\n"

            if gui.about:
                text "[gui.about!t]\n"

            if gui.translate_team:
                label "Команда переводчиков"
                text "[gui.translate_team]\n"

            if gui.help_team:
                label "Помогали"
                text "[gui.help_team]\n"

            text "[renpy.license!t]\n"

    use return_button("extras")
    use title("Об игре")

style about_label is gui_label
style about_text is gui_text

style about_label_text:
    is gui_label_text
    size gui.label_text_size

style about_frame:
    background Frame(persistent.win_bg, 5, 5)
    align (.5, .5)
    xfill True
    xmargin 10
    padding (10, 10)

style about_vscrollbar is vscrollbar:
    base_bar Frame(persistent.win_bg, Borders(5, 5, 5, 5))
    bar_vertical True
    thumb Composite((12, 25),
        (1, 0), Solid("#fff", xysize=(10, 25)))
    yfill True

style about_text:
    variant "touch"
    size 16
