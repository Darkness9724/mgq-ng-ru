init -9 python:
    class Chr(object):
        def __init__(self):
            self.name = None
            self.pedia_name = None
            self.unlock = False
            self.zukan_ko = None
            self.label = None
            self.label_cg = None
            self.facenum = 1
            self.posenum = 1
            self.bk_num = 0
            self.face = 1
            self.pose = 1
            self.bk = [False] * 4
            self.x = [0] * 2
            self.scope = None
            self.info = ""
            self.hp = 0
            self.lv = 0
            self.skills = None
            self.m_pose = None
            self.echi = [""]

    def get_size(d):
        d = renpy.easy.displayable(d)
        w, h = renpy.render(d, 0, 0, 0, 0).get_size()
        w, h = int(round(w)), int(round(h))
        return w, h

    def get_girls(page, max_page=2):
        girls = (
            # 1 глава
            get_alice_ng,
            get_granberia_ng,
            get_queenharpy_ng,
            get_tamamo_ng,
            # 2 глава
            get_alma_ng,
            get_sylph_ng,
            get_gnome_ng,
            get_erubetie_ng,
            get_undine_ng,
            get_salamander_ng,
            get_micaela_ng,
            # 3 глава
            get_bess_ng,
            get_sphinx_ng,
            get_sara_ng,
        )

        for _ in range(28):
            girls += (None,)

        start = page * max_page - max_page
        end = start + max_page
        s = []
        s0 = girls[start:end]

        for i in s0:
            if i is not None:
                s.append(i())
            else:
                s.append(None)

        s0 = []

        for girl in s:
            if girl is not None and girl.unlock:
                i_img, pos = girl.m_pose(girl, girl.x[0])[0]
                x, y = pos
                i_img = Crop((-x, -y, 400, 600), i_img)
                s0.append((girl.pedia_name, i_img,  girl))
            else:
                s0.append(("", Null(), False))

        return s0

    def pf():
        renpy.transition(Fade(.3, 0, .3, color="#fff"), layer="screens")

    def get_info_list(info):
        text = []
        line = ""
        info = info.replace("\n", " \t")
        info = info.split(" ")
        info.insert(0, " ")

        for word in info:
            # Не более 37 слов в ширину
            if "\t" in word or len("".join((line, " ", word))) > 37:
                text.append(line)
                # Красная строка в два пробела
                if "\t" in word:
                    word = word.expandtabs(2)
                line = word
            else:
                line = "".join((line, " ", word))

        text.append(line)

        return text

screen pedia_new(enter):
    default page = 1
    predict False
    tag menu

    $ p_girls = get_girls(page)

    add "overlay/book.webp"

    grid 2 1:

        spacing 0
        xfill True
        yfill True
        # at p_fade

        for nm, i_img, obj in p_girls:

            if nm:
                button:
                    background None
                    hover_foreground Solid("#f002")
                    padding (0, 0)
                    margin (0, 5)
                    activate_sound "se/m_papier02.opus"

                    action [Show("pedia_base_new", girl=obj), Function(pf)]

                    add i_img align (0, 0)
                    text nm:
                        style "pbn_name"
                        align (.5, 0)
                        yoffset 10
            else:
                text "???":
                    # at p_fade
                    size 70
                    align (.5, .5)
                    font "Skandal.ttf"
                    color "#808080"
                    outlines [(2, "#000")]

        if len(p_girls) < 2:
            null

    add "overlay/book_foreground.webp"

    imagebutton:
        auto "phone/button/back_%s.webp"
        align (1.0, 1.0)
        margin (5, 5)
        activate_sound "se/m_papier02.opus"

        if enter:
            action ShowMenu(enter)
        else:
            action Return()


    use pedia_pages(page, 20)

screen pedia_pages(page, max_page):
    $ start = page*2 - 2 + 1
    $ end = start + 1

    default ZOOM = 2.0

    style_prefix "ppages"

    imagebutton:
        at yzoom(ZOOM)
        align (0, .5)
        auto "button/left_%s.webp"
        margin (5, 5)
        activate_sound "se/m_papier02.opus"

        action [If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', max_page)), Function(pf)]

    imagebutton:
        at yzoom(ZOOM)
        align (1.0, .5)
        auto "button/right_%s.webp"
        margin (5, 5)
        activate_sound "se/m_papier02.opus"
        action [If(page < max_page, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1)), Function(pf)]

    label "- [start] -":
        align (.25, .94)

    label "- [end] -":
        align (.75, .94)

    key "K_PAGEDOWN" action If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', max_page))
    key "K_PAGEUP" action If(page < max_page, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1))

style ppages_label:
    xfill False
    background None

style ppages_label_text:
    color "#fffa"
    outlines [(2, "#000e", 0, 0)]
    size 22

screen pedia_base_new(girl):
    on "hide" action [
        SetField(girl, "bk[0]", False),
        SetField(girl, "bk[1]", False),
        SetField(girl, "bk[2]", False),
        SetField(girl, "bk[3]", False),
        SetField(girl, "pose", 1),
        SetField(girl, "face", 1),
        SetField(girl, "battle", 0),
        SetField(girl, "echi", "")]

    modal True
    default page = 0

    python:
        img_list = girl.m_pose(girl, girl.x[0])

        global world
        world = World()
        world.init()

        scope = {}
        scope["world"] = world
        scope["_game_menu_screen"] = "game_menu"
        for variable, value in girl.scope.items():
            dynamic_variable("%s" % variable, value)
        scope["_window_auto"] = True

    add "overlay/book.webp"
    style_prefix "pbn"

    if page == 0:
        for IMG, POS in img_list:
            add IMG pos POS
        add "overlay/book_f.webp" xalign 1.0

        frame:
            background None
            align (1.0, 1.0)
            xysize (400, 600)
            padding (20, 20)
            margin (0, 0)

            vbox:
                xfill True
                spacing 15

                text "[girl.name]":
                    style "pbn_name"
                    xalign .5

                textbutton "Информация" action SetScreenVariable('page', 1)
                textbutton "Характеристики" action SetScreenVariable('page', 2)
                textbutton "Позы" action Show("pose", girl=girl)
                # textbutton "CG" action If(girl.zukan_ko == None or persistent.defeat[girl.zukan_ko] != 0,
                #                             Function(renpy.call_in_new_context, girl.label_cg)
                #                             )
                textbutton "Вспомнить" action SetScreenVariable('page', 3)


    elif page == 1:
        frame:
            xfill True
            yfill True
            background None
            padding (0, 25, 0, 25)
            margin (0, 0)

            has hbox:
                spacing 20
            vbox:
                box_wrap True

                text "[girl.name]":
                    style "pbn_name"
                    xalign .5

                $ info_list = get_info_list(girl.info)

                for line in info_list:
                    label "[line]":
                        style "pbn_info"

    elif page == 2:
        grid 2 1:
            xfill True

            frame:
                margin (10, 0)
                padding (30, 15)
                background None

                has vbox:
                    xfill True
                text "[girl.name]":
                    style "pbn_name"
                    xalign .5

                text "Уровень: [girl.lv]":
                    size 22
                    font "MarckScript-Regular.ttf"
                    color '#000'
                    outlines [(1, '#f3ede180')]
                text 'Здоровье: [girl.hp]{#pedia_base}':
                    size 22
                    font "MarckScript-Regular.ttf"
                    color '#000'
                    outlines [(1, '#f3ede180')]

                if girl.zukan_ko:
                    $ var = persistent.defeat[girl.zukan_ko]

                    text "Проигрышей: [var]":
                        size 22
                        font "MarckScript-Regular.ttf"
                        color '#000'
                        outlines [(1, '#f3ede180')]
                else:
                    text "Побеждена: Нет":
                        size 22
                        font "MarckScript-Regular.ttf"
                        kerning -0.5
                        color '#000'
                        outlines [(1, '#f3ede180')]
            frame:
                padding (20, 15)
                background None

                has vbox:
                    xfill True

                if girl.skills is not None:
                    text "Умения:":
                        size 24
                        xalign .5
                        font "MarckScript-Regular.ttf"
                        color '#000'
                        outlines [(1, '#f3ede180')]

                    for num, sk_name in girl.skills:
                        text "[sk_name!t]{#pedia_base}:":
                            size 22
                            font "MarckScript-Regular.ttf"
                            color '#fffa'
                            outlines [(2, '#000a')]
                        text "    Получил: %d, Оргазмов: %d{#data}" % (persistent.skills[num][0], persistent.skills[num][1]):
                            size 22
                            font "MarckScript-Regular.ttf"
                            color '#000'
                            outlines [(2, '#f3ede180')]

    elif page == 3:
        grid 2 1:
            xfill True

            frame:
                padding (30, 15)
                background None
                has vbox:
                    xfill True

                if girl.zukan_ko:
                    textbutton "{b}Нормальная{/b} битва":
                        action Function("pedia_battle", 1)
                    if persistent.battle != 1:
                        textbutton "{b}Сложная{/b} битва":
                            action Function("pedia_battle", 2)
                        textbutton "{b}Адская{/b} битва":
                            action Function("pedia_battle", 3)

            frame:
                padding (20, 15)
                background None

                has vbox:
                    xfill True

                if len(girl.echi) == 1:
                    if girl.echi[0]:
                        $ hlb = girl.echi[0]
                    else:
                        $ hlb = "%s_h" % girl.label

                    textbutton "Сцена изнасилования":
                        action Replay(hlb, scope=scope)
                else:
                    for num, replay_lb in enumerate(girl.echi, 1):
                        textbutton "Сцен изнасилования [num]":
                            action Replay(replay_lb , scope=scope)

    imagebutton:
        auto "phone/button/back_%s.webp"
        align (1.0, 1.0)
        margin (5, 5)
        activate_sound "se/m_papier02.opus"

        if page != 0:
            action SetScreenVariable('page', 0)
        else:
            action Hide("pedia_base_new")

    key "game_menu" action Hide('pedia_base_new')

style pbn_name:
    size 24
    font "Skandal.ttf"
    color "#fff000"
    outlines [(2, "#000a", 0, 0)]

style pbn_info:
    is label
    margin (10, 0)
    padding (27, 0, 0, 0)
    xsize 400

style pbn_info_text:
    size 18
    font "MarckScript-Regular.ttf"
    color "#000"
    outlines [(1, "#fffa")]

style pbn_button:
    is button
    background None
    activate_sound "se/m_papier02.opus"

style pbn_button_text:
    size 24
    font "Skandal.ttf"
    color "#fff"
    hover_color "#f00"
    insensitive_color "#8888"
    outlines [(2, "#000a")]

screen pedia_old(enter):
    tag menu

    default page = 1
    default max_range = 26

    $ p_girls = get_girls(page, max_range)
    $ L = len(p_girls)

    if enter == "ex":
        add Solid("#000")
    else:
        add renpy.get_screen("bg", layer='master')

    add gui.game_menu_background

    if not renpy.android:
        textbutton "Топография":
            align (1.0, 0.02)
            style "ret_button"
            action ShowMenu("gallery", enter="pedia_%s" % persistent.pedia_view)

    frame:
        xmargin 20
        padding (5, 10, 5, 0)
        ysize 450
        align (.5, .5)
        background Frame(persistent.box, 5, 5)

        grid 2 13:
            transpose True
            xfill True
            spacing 5

            for nm, i_img, obj in p_girls:
                if obj:
                    textbutton "[nm!t]" style "pedia_tb":
                        action ShowMenu("pedia_base", girl=obj)
                else:
                    text "???" style "pedia_lock"

            if L < max_range:
                for i in range(max_range-L):
                    null


    use pages(page, 2, "pedia")
    use title("Монстропедия NG+")
    use return_button(enter)

style pedia_tb:
    is button
    background None
    xalign 0
    xmargin 0
    xpadding 0

style pedia_tb_text:
    size 22
    line_spacing -5
    line_leading -5
    outlines [(2, '#000c')]
    font "Arciform.otf"
    idle_color "#fff000cc"
    hover_color "#000fffcc"

style pedia_lock:
    size 22
    line_spacing -5
    line_leading -5
    font "Arciform.otf"
    color "#80808060"
    outlines [(2, "#0008")]


screen pedia_base(girl):
    modal True
    predict False

    default page = 1

    on "hide" action [
        SetField(girl, "bk[0]", False),
        SetField(girl, "bk[1]", False),
        SetField(girl, "bk[2]", False),
        SetField(girl, "bk[3]", False),
        SetField(girl, "pose", 1),
        SetField(girl, "face", 1),
        SetField(girl, "battle", 0),
        SetField(girl, "echi", "")]

    python:
        img_list = girl.m_pose(girl, girl.x[0])

        global world
        world = World()
        world.init(True)

        scope = {}
        scope["world"] = world
        scope["_game_menu_screen"] = "game_menu"
        for variable, value in girl.scope.items():
            dynamic_variable("%s" % variable, value)
        scope["_window_auto"] = True

    add "overlay/book.webp"

    for IMG, POS in img_list:
        add IMG pos POS

    add "overlay/book_f.webp" align (0, 0)

    fixed:
        style_prefix 'pedia_base'

        vbox:
            xfill True

            text "[girl.name]":
                xalign .5
                size 24
                kerning -0.5
                color '#ff0000'
                outlines [(1, '#f3ede180')]
                line_spacing -5
                line_leading -1

            # null height 5

            if page == 1:
                $ info_list = get_info_list(girl.info)

                if len(info_list) > 22:
                    frame:
                        background None

                        has viewport:
                            mousewheel True
                            draggable True
                            scrollbars "vertical"

                        vbox:
                            for line in info_list:
                                text "[line]":
                                    size 18
                                    kerning -0.5
                                    color '#000'
                                    outlines [(1, '#f3ede180')]
                                    line_spacing -2
                                    line_leading -1
                else:
                    vbox:
                        for line in info_list:
                            text "[line]":
                                size 18
                                kerning -0.5
                                color '#000'
                                outlines [(1, '#f3ede180')]
                                line_spacing -2
                                line_leading -1

            elif page == 2:
                text "Уровень: [girl.lv]":
                    size 20
                    color '#000'
                    outlines [(1, '#f3ede180')]
                text 'Hp: [girl.hp]{#pedia_base}':
                    size 20
                    color '#000'
                    outlines [(1, '#f3ede180')]

                if girl.zukan_ko:
                    $ var = persistent.defeat[girl.zukan_ko]

                    text "Проигрышей: [var]":
                        size 20
                        color '#000'
                        outlines [(1, '#f3ede180')]
                else:
                    text "Проигрышей: Нет":
                        size 20
                        kerning -0.5
                        color '#000'
                        outlines [(1, '#f3ede180')]

                # null height 10

                if girl.skills is not None:
                    text "Умения:":
                        size 20
                        color '#000'
                        outlines [(1, '#f3ede180')]

                    for num, sk_name in girl.skills:
                        textbutton "[sk_name!t]{#pedia_base}":
                            xalign 0
                            left_margin 5
                            text_kerning -1
                            text_hover_color "#f00"
                            text_size 21
                            # text_outlines [(1, '#000a')]

                            action If(persistent.skills[num][0], ShowMenu("data2", c=persistent.skills[num]))

            elif page == 3:
                if not main_menu:
                    text "Вы не можете использовать воспоминания, проходя сюжет.":
                        size 20
                        color '#000'
                        outlines [(1, '#f3ede180')]
                else:
                    if girl.zukan_ko:
                        textbutton "{b}Нормальная{/b} битва":
                            action Function(pedia_battle, 1, girl.label, scope)
                        if persistent.battle != 1:
                            textbutton "{b}Сложная{/b} битва":
                                action Function(pedia_battle, 2, girl.label, scope)
                            textbutton "{b}Адская{/b} битва":
                                action Function(pedia_battle, 3, girl.label, scope)

                    if len(girl.echi) == 1:
                        if girl.echi[0]:
                            $ hlb = girl.echi[0]
                        else:
                            $ hlb = "%s_h" % girl.label

                        textbutton "Сцена изнасилования":
                            action [SetDict(scope, "replay_h", True),
                                    SetDict(scope, "monster_name", girl.name),
                                    Replay(hlb, scope=scope)]
                    else:
                        for num, replay_lb in enumerate(girl.echi, 1):
                            textbutton "Сцен изнасилования [num]":
                                left_margin 0
                                action [SetDict(scope, "replay_h", True),
                                        SetDict(scope, "monster_name", girl.name),
                                        Replay(replay_lb , scope=scope)]


    frame:
        style_prefix 'pedia_nav'
        background None
        xsize 400
        align (1.0, 1.0)

        hbox:
            xalign .5
            textbutton "Описание" action If(page != 1, SetScreenVariable("page", 1))
            textbutton "Инфо" action If(page != 2, SetScreenVariable("page", 2))
            textbutton "Позы" action ShowMenu('pose', girl=girl)
            # textbutton "CG" action If(girl.zukan_ko == None or persistent.defeat[girl.zukan_ko] != 0,
            #                                 Function(renpy.call_in_new_context, girl.label_cg)
            #                                 )
            textbutton "Вспомнить" action If(page != 3, true=SetScreenVariable("page", 3))

    imagebutton:
        auto "phone/button/back_%s.webp"
        align (0.0, 1.0)
        margin (5, 5)
        activate_sound "se/m_papier02.opus"

        action Hide("pedia_base")

    key "game_menu" action Hide('pedia_base')

style pedia_base_fixed:
    anchor (0, 0)
    pos (425, 10)
    xysize (350, 550)

style pedia_base_text:
    font "MarckScript-Regular.ttf"

style pedia_base_button:
    is button
    xalign 0
    background None
    xpadding 5
    xmargin 0

style pedia_base2_button:
    is pedia_base_button
    xalign 0.5

style pedia_nav_button:
    is pedia_base_button
    xalign 0.5

style pedia_base2_button_text:
    is pedia_base_button_text

style pedia_nav_button_text:
    is text
    xalign 0.0
    size 20
    line_spacing -2
    line_leading -2
    outlines [(2, '#000c', 0, 0)]
    font "Skandal.ttf"
    color "#fff000cc"
    hover_color "#000fffcc"
    insensitive_color "#888c"

style pedia_base_button_text:
    is text
    xalign 0.0
    font "Skandal.ttf"
    size 20
    outlines [(2, '#000c')]
    color "#fff000cc"
    hover_color "#000fffcc"
    insensitive_color "#888c"

style pedia_base_vscrollbar:
    is pref_vscrollbar

############################################################
#
# Skills Data
#
############################################################
screen data2(c):
    modal True

    frame:
        style_prefix "data2"

        has vbox

        text "Taken: [c[0]]"
        text "Orgasms: [c[1]]"

    key "dismiss" action Hide("data2")
    key "game_menu" action Hide("data2")

style data2_frame:
    background Frame(persistent.box, 5, 5)
    # xsize (200, 100)
    align (.5, .5)
    padding (10, 10)

style data2_vbox:
    spacing 10

style data2_text:
    size 20

############################################################
#
# POSE
#
############################################################
screen pose(girl):
    default rm = 1

    modal True

    python:
        img_list = girl.m_pose(girl, girl.x[1])

    add "overlay/book.webp"

    for IMG, POS in img_list:
        add IMG pos POS

    if rm == 1:
        frame:
            background Solid("8a01bd70")
            style_prefix "pose"
            align (1.0, 0)
            xysize (195, 600)
            ypadding 20

            frame:
                background None
                yalign 0
                xmargin 0
                xpadding 0
                has vbox
                textbutton "Лица" action If(girl.facenum > 1, SetField(girl, "face", girl.face+1))
                textbutton "Позы" action If(girl.posenum > 1, SetField(girl, "pose", girl.pose+1))

            frame:
                background None
                yalign .5
                xmargin 0
                xpadding 0
                has vbox

                if girl.bk_num > 0:
                    for i in range(1, girl.bk_num+1):
                        textbutton "Буккакэ [i]" action ToggleField(persistent, "bk" + str(i), true_value=True, false_value=False)
            frame:
                background None
                yalign 1.0
                xmargin 0
                xpadding 0
                has vbox
                textbutton "Скрыть" action SetScreenVariable("rm", 0) yalign 1.0
                textbutton "Назад" action Hide('pose')

        key "game_menu" action Hide('pose')
    else:
        key "dismiss" action SetScreenVariable("rm", 1)
        key "game_menu" action SetScreenVariable("rm", 1)

style pose_button:
    is button
    background None

style pose_button_text:
    is text
    size 26
    font "MarckScript-Regular.ttf"
    idle_color "#fff"
    outlines [(1, "#000a")]
    hover_color "#f00"
    selected_color "#3ccd7a"
    insensitive_color "#8888"
    insensitive_outlines [(1, "#303030")]
