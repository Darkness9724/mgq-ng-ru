init python:
    class ProportionalScale(im.ImageBase):
        """
        Resizes a renpy image to fit into the specified width and height.
        The aspect ratio of the image will be conserved.
        """
        def __init__(self, img, maxwidth, maxheight, bilinear=True, **properties):
            self.image = renpy.easy.displayable(img)
            super(ProportionalScale, self).__init__(self.image, maxwidth, maxheight, bilinear, **properties)
            self.maxwidth = int(maxwidth)
            self.maxheight = int(maxheight)
            self.bilinear = bilinear

        def load(self):
            child = im.cache.get(self.image)
            width, height = child.get_size()

            ratio = min(self.maxwidth/float(width), self.maxheight/float(height))
            width = ratio * width
            height = ratio * height

            if self.bilinear:
                try:
                    renpy.display.render.blit_lock.acquire()
                    rv = renpy.display.scale.smoothscale(child, (width, height))
                finally:
                    renpy.display.render.blit_lock.release()
            else:
                try:
                    renpy.display.render.blit_lock.acquire()
                    rv = renpy.display.pgrender.transform_scale(child, (newwidth, newheight))
                finally:
                    renpy.display.render.blit_lock.release()
            return rv

        def true_size(self):
            """
            I use this for the BE. Will do the callulations but not render anything.
            """
            child = im.cache.get(self.image)
            width, height = child.get_size()

            ratio = min(self.maxwidth/float(width), self.maxheight/float(height))
            width = int(round(ratio * width))
            height = int(round(ratio * height))
            return width, height

        def get_image_name(self):
            """
            Returns the name of an image bound to the ProportionalScale.
            """
            path = self.image.filename
            image_name = path.split("/")[-1]
            return image_name

        def get_image_tags(self):
            """
            Returns a list of tags bound to the image.
            """
            image_name = self.get_image_name()
            image_name_base = image_name.split(".")[0]
            obfuscated_tags = image_name_base.split("-")[1:]
            return [tags_dict[tag] for tag in obfuscated_tags]

        def predict_files(self):
            return self.image.predict_files()

    def ProportionalScale(img, maxwidth, maxheight):
        currentwidth, currentheight = renpy.image_size(img)
        xscale = float(maxwidth) / float(currentwidth)
        yscale = float(maxheight) / float(currentheight)

        if xscale < yscale:
            minscale = xscale
        else:
            minscale = yscale

        return im.FactorScale(img,minscale,minscale)

    from collections import OrderedDict

    gallery_items = OrderedDict() # maintain dictionary order so you can assign custom key names

    # Grid values
    gallery_xgrid = 3
    gallery_ygrid = 2

    gallery_page_margin = 30 #for things like buttons at the top
    gallery_thumb_margin = 50 #for things like thumbnail heading and spacing

    # Work out the thumbnail bounding size
    gallery_xmin = ((config.screen_width - gallery_page_margin) / gallery_xgrid) - gallery_thumb_margin
    gallery_ymin = ((config.screen_height - gallery_page_margin) / gallery_ygrid) - gallery_thumb_margin

    # Locked image thumbnail
    gallery_locked_thumb = "overlay/saveload_dummy.webp"

    if not renpy.android:
        for i, files in enumerate(sorted(renpy.loader.game_files)):
            scene = tuple(renpy.re.sub('(.*)/', '', files[1]).replace('.webp','').split())
            if scene[0] == "bg":
                gallery_items[f"scene {scene[1]}"] = {
                    "name": f"Местность {scene[1]}",
                    "thumb": files[1],
                    "images": [f"bg {scene[1]}"]
                }
    gallery = Gallery()

    for key, gallery_item in gallery_items.items():
        gallery.button(key)

        if persistent.unlock_gallery: # Cheat code variable
            gallery.condition("True")
            for p_image in gallery_item["images"]:
                gallery.image(p_image)
        else:
            for p_image in gallery_item["images"]:
                gallery.unlock_image(p_image)

        gallery.transform(truecenter) # center image if smaller than full screen

        if not "locked" in gallery_item:
            gallery_items[key]["locked"] = gallery_locked_thumb

    gallery.transition = dissolve # choose your transition type

screen gallery(enter=None):
    tag menu
    if main_menu:
        add Solid("#000")
    else:
        add renpy.get_screen("bg", layer="master")
    add gui.game_menu_background
    default page = 1

    use title("Топография")

    style_prefix "gallery"

    grid gallery_xgrid gallery_ygrid:
        xfill True
        yfill True

        $ gallery_item_start = (page - 1)*gallery_xgrid*gallery_ygrid
        $ gallery_item_end = gallery_item_start + gallery_xgrid*gallery_ygrid - 1
        $ gallery_item_pointer = 0

        for key, gallery_item in gallery_items.items():
            if gallery_item_start <= gallery_item_pointer <= gallery_item_end:
                frame:
                    xalign 0.5
                    yalign 0.5
                    vbox:
                        spacing 2

                        add gallery.make_button(key, ProportionalScale(gallery_item["thumb"], gallery_xmin, gallery_ymin), ProportionalScale(gallery_item["locked"], gallery_xmin, gallery_ymin), xalign=0.5, yalign = 0.5)
                        text gallery_item["name"] xalign 0.5
            $ gallery_item_pointer += 1

        if gallery_item_pointer < gallery_item_end:
            for i in range(0, gallery_item_end - gallery_item_pointer + 1):
                null

    use pages(page, int(len(gallery_items) / (gallery_xgrid*gallery_ygrid)))
    use return_button()
