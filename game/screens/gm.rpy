## Экран игрового меню #########################################################
##
## Всё это показывает основную, обобщённую структуру экрана игрового меню. Он
## вызывается с экраном заголовка и показывает фон, заголовок и навигацию.
##
## Параметр scroll может быть None, или "viewport", или "vpgrid", когда этот
## экран предназначается для использования с более чем одним дочерним экраном,
## включённым в него.
screen game_menu(enter="game_menu"):
    $ gm, stats = world.party.player.get_stats()
    $ equip = world.party.player.get_equip()

    tag menu

    add renpy.get_screen("bg", layer='master')
    add gui.game_menu_background
    add "gm char"

    style_prefix "gm"

    frame:
        pos (10, 20)
        ypadding 5

        vbox:
            spacing 5

            textbutton "Предметы" action ShowMenu("menu_skit", var="item")
            textbutton "Навыки" action ShowMenu("menu_skit", var="skill")
            textbutton "Дневник" action If(not _in_replay, ShowMenu("record"))
            textbutton "Монстропедия" action If(_in_replay, true=EndReplay(confirm=False), false=ShowMenu(f"pedia_{persistent.pedia_view}", enter))
            textbutton "Журнал" action ShowMenu("history", enter)
            textbutton "Настройки" action ShowMenu("preferences", enter)
            textbutton "Сохранить" action If(store._rollback, ShowMenu("save", enter))
            textbutton "Загрузить" action ShowMenu("load", enter)
            textbutton "В главное меню" action MainMenu()
            if renpy.variant("pc"):
                textbutton "Выйти из игры" action Quit()
            textbutton "\nВерсия перевода: [config.version]" action ShowMenu("pearl") style "hidden_button"

    frame:
        align (.5, 1.0)
        yoffset -20
        xoffset -50
        xpadding 10

        has vbox
        text "[gm!t]" xalign (.5) size 25

        grid 2 5:
            transpose True

            text "Уровень:"
            text "Макс. HP:"
            text "Макс. SP:"
            text "Атака:"
            text "Защита:"

            for t in stats:
                text "[t]" xalign 1.0

    frame:
        align (1.0, 1.0)
        xoffset -20
        yoffset -100
        xpadding 10
        xminimum 250

        vbox:
            text "Экипировка" xalign (.5) size 25
            for e in equip:
                text "[e]"

    use return_button()


style gm_frame:
    is frame
    background Frame(persistent.win_bg, Borders(10, 10, 10, 10))

style gm_button:
    is button
    background None
    xalign .5
    ypadding 0
    ymargin 0

style gm_button_text:
    is default
    xalign .5
    size 25
    line_spacing -2
    line_leading -2
    outlines [(2, '#000c')]
    insensitive_outlines [(2, '#8888')]
    insensitive_color '#808080'
    font "Arciform.otf"
    color "#fff000cc"
    hover_color "#000fffcc"

style hidden_button:
    is button
    background None
    ypadding 0
    ymargin 0

style hidden_button_text:
    is default
    size 18
    line_spacing -2
    line_leading -2
    outlines [(2, '#000c')]
    insensitive_outlines [(2, '#8888')]
    insensitive_color '#808080'
    font "Arciform.otf"
    color "#fffacd"
    hover_color "#fffacd"

style gm_text:
    is default
    size 22
    outlines [(2, '#000c')]
    font "Arciform.otf"
    color "#fffacd"

###############################################################################
## Дневник
###############################################################################
screen record(enter="game_menu"):
    default page = 1

    tag menu

    if main_menu:
        add Solid("#000")
    else:
        add renpy.get_screen("bg", layer='master')

    add gui.game_menu_background

    frame:
        style_prefix 'record'
        has vbox
        if page == 1:
            text "Врагов побеждено: [persistent.count_wins]"
            text "Оргазмов: [persistent.count_end]"
            text "Капитуляций: [persistent.count_surrenders]"
            text "Запросов: [persistent.count_requests]"
            text "Получено наставлений: [persistent.count_advices]"
            text "HP поглощено при атаке: [persistent.count_drainhp]"
            text "Уровней поглощено: [persistent.count_drainlv]"
            text "Количество спонтанных оргазмов: [persistent.count_early_end]"

            if not persistent.count_mostname:
                text "Больше всего проигрышей: Никому"
            else:
                text "Больше всего проигрышей: [persistent.count_mostname] ([persistent.count_most])"

            if persistent.count_wind > 0:
                text "Сильфа призвана: [persistent.count_wind] раз"

            if persistent.count_earth > 0:
                text "Гнома призвана: [persistent.count_earth] раз"

            if persistent.count_aqua > 0:
                text "Ундина призвана: [persistent.count_aqua] раз"

            if persistent.count_fire > 0:
                text "Саламандра призвана: [persistent.count_fire] раз"

        elif page == 2:
            text "Вагинальных эякуляций: [persistent.count_vaginal_sex]"
            text "Эякуляций во время особых услуг ротиком: [persistent.count_blowjob_special]"
            text "Эякуляций меж бёдрами: [persistent.count_assjob]"
            text "Эякуляций от минета: [persistent.count_blowjob]"
            text "Эякуляций от пайзури: [persistent.count_titsjob]"
            text "Эякуляций от ласк ручками: [persistent.count_arm_rubs]"
            text "Эякуляций от ласк ножками: [persistent.count_leg_rubs]"
            text "Эякуляций от ласк волосами: [persistent.count_hair_rubs]"
            text "Эякуляций от ласк телом: [persistent.count_body_rubs]"
            text "Эякуляций от поцелуев: [persistent.count_kiss]"
            text "Эякуляций от анального секса: [persistent.count_anal_sex]"
            text "Эякуляций от пыток и издевательств над сосками: [persistent.count_nipple_torture]"
            text "Эякуляций от массажа простаты: [persistent.count_anal_massage]"

        elif page == 3:
            text "Эякуляций от массажа всего тела: [persistent.count_body_massage]"
            text "Эякуляций от щекотки: [persistent.count_tickles]"
            text "Эякуляций от покатушек на лице: [persistent.count_mounts]"
            text "Эякуляций от удушья: [persistent.count_asphyxia]"
            text "Эякуляций от принудительной мастурбации: [persistent.count_onanism]"
            text "Эякуляций от атак щупальцами: [persistent.count_tentacles]"
            text "Эякуляций от атак слизью: [persistent.count_slime]"
            text "Эякуляций от атак слизистых мембран: [persistent.count_slug]"
            text "Эякуляций от игрищ и дразнений хвостиками: [persistent.count_tails]"
            text "Эякуляций от атак верёвками: [persistent.count_ropes]"
            text "Эякуляций от атак сковывания: [persistent.count_binds]"
            text "Эякуляций от атак всякой растительностью: [persistent.count_plant]"
            text "Эякуляций от атак крыльями: [persistent.count_wing]"

        elif page == 4:
            text "Эякуляций от атак одеждой: [persistent.count_clothes]"
            text "Эякуляций от атак ветром: [persistent.count_wind]"
            text "Эякуляций от атак пожирания и поглощения: [persistent.count_vore]"
            text "Количество поражений от физических атак: [persistent.count_physical]"
            text "Количество самопоражений в замешательстве: [persistent.count_suicide]"
            text "Эякуляций от иных атак: [persistent.count_other]"
            text "Количество оргазмов, будучи скованным: [persistent.count_bind_end]"
            text "Количество оргазмов в трансе: [persistent.count_hypnosis]"
            text "Количество оргазмов, будучи парализованным: [persistent.count_paralyze]"
            text "Количество оргазмов во время искушений: [persistent.count_charm]"
            text "Количество оргазмов во время принуждения к капитуляции: [persistent.count_surrender]"
            text "Количество оргазмов во время окаменения: [persistent.count_stone]"
            text "Количество оргазмов во время замешательства: [persistent.count_confuse]"

        elif page == 5:
            text "Количество раз, когда оставлен как партнёр для размножения: [persistent.count_b001]"
            text "Количество спариваний для размножения и смертей после этого: [persistent.count_b002]"
            text "Количество раз, когда оставался служить бесконечной кормёжкой: [persistent.count_b003]"
            text "Количество смертей от высасывания досуха: [persistent.count_b004]"
            text "Количество превращений в игрушку: [persistent.count_b005]"
            text "Количество игрушек-веселушек и следующих за ними смертей: [persistent.count_b006]"
            text "Количество превращений в групповую игрушку: [persistent.count_b007]"
            text "Количество смертей от группового иссушения до костей: [persistent.count_b008]"
            text "Количество превращений в раба для монстра: [persistent.count_b009]"
            text "Количество принудительных свадеб: [persistent.count_b010]"
            text "Количество пожираний монстром: [persistent.count_b011]"
            text "Количество превращений в подопытного кролика: [persistent.count_b013]"
            text "Количество принятых приглашений к смерти: [persistent.count_b014]"

        elif page == 6:
            text "Количество помещений в Монстролечебницу: [persistent.count_b012]"
            text "Количество перерождений в совершенно новое бытие: [persistent.count_b015]"
            text "Количество наказаний в виде беспрестанного высасывания спермы: [persistent.count_b016]"
            text "Количество спермовыжимающих наказаний и смертей после этого: [persistent.count_b017]"

        elif page == 7:
            text "Завершить первую главу":
                if persistent.game_clear == 0:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Завершить вторую главу":
                if persistent.game_clear < 2:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Завершить третью главу":
                if persistent.game_clear < 3:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Заполнить первую главу монстропедии":
                if not persistent.count_pedia[0]:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Заполнить вторую главу монстропедии":
                if not persistent.count_pedia[1]:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Заполнить третью главу монстропедии":
                if not persistent.count_pedia[2]:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Получил удар всеми запрашиваемыми приёмами первой главы":
                if persistent.count_oskill[0] != 262:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Получил удар всеми запрашиваемыми приёмами второй главы":
                if persistent.count_oskill[1] != 402:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Получил удар всеми запрашиваемыми приёмами третьей главы":
                if persistent.count_oskill[2] != 443:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Монстропедия полностью заполнена":
                if persistent.count_pedia[0] != 1 or persistent.count_pedia[1] != 1 or persistent.count_pedia[2] == 1 or persistent.count_oskill[0] + persistent.count_oskill[1] + persistent.count_oskill[2] <= 1107:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Победил, не получив урона":
                if not persistent.count_hpfull:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Побеждён в первый же ход":
                if not persistent.count_onekill:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Победил, будучи в это время в замешательстве":
                if not persistent.count_vickonran:
                    color "#8886"
                    outlines [(2, "#0008")]

        elif page == 8:
            text "Победил в битве на АДСКОМ режиме сложности":
                if not persistent.count_hellvic:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Иссушён до самого 1-го уровня":
                if not persistent.count_mylv0:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Заставил противника вкусить всю мощь Четырёхкратного Гига-удара":
                if persistent.count_giga:
                    color "#8886"
                    outlines [(2, "#0008")]


            if persistent.g_sylph:
                text "Сильфа призвана уставшей"
            elif not persistent.g_sylph and persistent.game_clear == 0:
                text "?????? призвана уставшей":
                    color "#8886"
                    outlines [(2, "#0008")]
            elif not persistent.g_sylph and persistent.game_clear > 0:
                text "Сильфа призвана уставшей":
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.z_gnome:
                text "Гнома призвана спящей"
            elif not persistent.z_gnome and persistent.game_clear == 0:
                text "?????? призвана спящей":
                    color "#8886"
                    outlines [(2, "#0008")]
            elif not persistent.z_gnome and persistent.game_clear > 0:
                text "Гнома призвана спящей":
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Побеждён противником, усиленным Зильфой.":
                if not persistent.count_enemy_wind:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Побеждён противником, усиленным Гномарен.":
                if not persistent.count_enemy_earth:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Побеждён противником, усиленным Грандиной.":
                if not persistent.count_enemy_aqua:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Побеждён противником, усиленным Гигамандрой.":
                if not persistent.count_enemy_fire:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Победил противника контратакой.":
                if not persistent.counterk:
                    color "#8886"
                    outlines [(2, "#0008")]

        elif page == 9:
            text "Победил Гранберию на адской сложности.":
                if not persistent.granberia_hell:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Победил Королеву Гарпий на адской сложности.":
                if not persistent.queenhapy_hell:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Победил Тамамо на адской сложности.":
                if not persistent.tamamo_hell:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Отношения с Гранберией на максимуме (Конец 1 части)":
                if not persistent.granberia_maxrep:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Отношения с Тамамо на максимуме (Конец 1 части)":
                if not persistent.tamamo_maxrep:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Отношения с Гранберией на минимуме (Конец 1 части)":
                if not persistent.granberia_minrep:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Словил Звёздный Клинок Гранберии и выжил":
                if not persistent.starblade:
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Словил Девять Лун Тамамо и победил":
                if not persistent.nine_moons_won:
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.tamamo_knows == 1:
                text "Тамамо что-то знает..."
            elif persistent.tamamo_knows == 0:
                text "????? что-то знает...":
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Получил все достижения 1 главы NG+":
                if not persistent.ng1_achi:
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.lilith_hell == 1:
                text "Победил Лилит и Лилим на адской сложности"
            elif persistent.lilith_hell == 0:
                text "????? на адской сложности":
                    color "#8886"
                    outlines [(2, "#0008")]

            text "Дебьют купидона-суккуба!":
                if not persistent.cupidsuccubus_debut:
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.lily_generator == 1:
                text "Перегрузил генератор барьера Лили"
            elif persistent.lily_generator == 0:
                text "Перегрузил генератор ????? ?????":
                    color "#8886"
                    outlines [(2, "#0008")]

            if persistent.koakgigacount == 1:
                text "Заставить Коакуму вкусить всю мощь Четырёхкратного Гига-удара"
            elif persistent.koakgigacount == 0:
                text "Заставить ????? вкусить всю мощь Четырёхкратного Гига-удара":
                    color "#8886"
                    outlines [(2, "#0008")]

        elif page == 10:
            if world.party.member[1] is not None:
                text "Отношения с Алисой: [world.party.member[1].rep]"
            else:
                text "Отношения с ?????"


            if world.party.member[2] is not None:
                text "Отношения с [world.party.member[2].name[4]]: [world.party.member[2].rep]"
            else:
                text "Отношения с ?????"

            if world.party.member[0].skill[1]:
                text "Отношения с Сильфой: [world.party.sylph_rep]"
            else:
                text "Отношения с ?????"
            if world.party.member[0].skill[2]:
                text "Отношения с Гномой: [world.party.gnome_rep]"
            else:
                text "Отношения с ?????"
            if world.party.member[0].skill[3]:
                text "Отношения с Ундиной: [world.party.undine_rep]"
            else:
                text "Отношения с ?????"
            if world.party.member[0].skill[4]:
                text "Отношения с Саламандрой: [world.party.salamander_rep]"
            else:
                text "Отношения с ?????"

            if world.party.member[3] is not None:
                text "Отношения с [world.party.member[3].name[4]]: [world.party.member[3].rep]"
            else:
                text "Отношения с ?????"

            if world.party.member[4] is not None:
                text "Отношения с [world.party.member[4].name[4]]: [world.party.member[4].rep]"
            else:
                text "Отношения с ?????"

    if main_menu:
        $ max_page = 9
    else:
        $ max_page = 10
    use pages(page, max_page)
    use title("Походный дневник")
    use return_button(enter)

style record_button:
    is default
    background None

style record_frame:
    background Frame(persistent.win_bg, 5, 5)
    xfill True
    yfill True
    margin (30, 60)
    padding (5, 20)

style record_frame:
    variant "touch"
    xmargin 50

style record_text:
    is item_text
    font "Arciform.otf"
    outlines [(2, '#000a')]
    size 19

style record_text:
    variant "touch"
    size 18

###############################################################################
## Книга пёрлов
###############################################################################
screen pearl():
    default yvalue = 1.0
    tag menu

    add renpy.get_screen("bg", layer='master')
    add gui.game_menu_background

    default pearls = [
        "Синии Ангела. (Видимо вместе с синим Лукой).",
        "Перенёссе. (Указывающее на место проживания переводчика).",
        "Рубашка, которая на мне надета сейчас. (Шедевр который не нуждается в пояснении).",
        "Провернь. (Это как прорубь, но на проверку. Так же это французкая провинция).",
        "Заикающийся Лука. (НГ+ воистину страшная игра).",
        "Что я мне делать? (Гранберия весьма озадачена тем, что проиграла Луке).",
        "Покинем из город. (Илиасбург стал оградой).",
        "Обижжать. (Совмещение обиды и самосожжения).",
        "Посмотри смотри за мной! (За Лукой нужен глаз да глаз!).",
        "ПОЛУЧАЙ СУКА! (Лучшая фраза при крите).",
        "Бессмысленная сайдквесты. (Жи есть, брат).",
        "Полнуешь. (Крайняя степень волнения. aka полное волнение).",
        "Оружейный магазиг. (Берегитесь вооруженных магических зиг!).",
        "Тамаом. (Закон Тамаома - эмпирический физический закон, определяющий связь электродвижущей силы источника с силой тока, протекающего в проводнике, и сопротивлением проводника. Открыт ученой Тамаомо но Маэ в 14хх году по календарю Йохана).",
        "Шторм начинался еще в прошлой когда. (Удивительная аномалия, пришедшая к нам из чертзнаетоткуда. Этот перл так-же легендарен как и рубашка, которая на мне надета сейчас).",
        "Я... Я правда не хочешь говорить об этом... (Лучок говорит о себе в третьем лице. Опять).",
        "Если собираешься завтра утром рано уходить. (Еще один легендарный перл, подобный рубашке).",
        "Азазазазавтраки. (Алиса и еда. Классика!).",
        "Сильфа Сильфы. (Безумие и Чи-Па-Па в чистом виде).",
        "Спасибо, Ундина. (Лука заглянул в будущее и поблагодарил Ундину заранее).",
        "Очень жально. (Жалко, но жалко у пчелки :с).",
        "Явно он не так сражался! (Гарпия крайне впечатлена умениями Луки).",
        "Как бы сильно я не взглядывался...",
        "Лучшая отсылка всех времен и народов! (Какой-то порт, какой-то персонаж)."
    ]

    frame:
        style_prefix "history"
        frame:
            background None
            margin (0, 0)

            has viewport:
                mousewheel True
                draggable True
                yinitial yvalue
                scrollbars "vertical"

            vbox:
                spacing 10

                for i, pearl in enumerate(pearls, 1):
                    text "[i]. [pearl]" size 20

    use title("Книга перлов перевода", ShowMenu("_developer"))
    use return_button("game_menu")

screen history(enter=None):
    default yvalue = 1.0
    tag menu

    add renpy.get_screen("bg", layer='master')
    add gui.game_menu_background

    ## Не предсказывать этот экран, он слишком большой.
    predict False

    frame:
        style_prefix "history"

        if not _history_list:
            label "Нет истории диалогов.":
                align(.5, .5)
        else:
            frame:
                background None
                margin (0, 0)

                has viewport:
                    mousewheel True
                    draggable True
                    yinitial yvalue
                    scrollbars "vertical"

                vbox:
                    spacing 10

                    for h in _history_list:
                        text (h.who or "Мысли") style "history_who"

                        hbox:
                            box_wrap True
                            spacing 10

                            if h.voice.filename:
                                imagebutton:
                                    auto "button/%s_backlog_voice_butt.webp"
                                    action Play("voice", h.voice.filename)

                            text h.what size 20

                        null height 10

    use title("Журнал")
    use return_button(enter)

style history_frame:
    background Frame(persistent.win_bg, 5, 5)
    xfill True
    yfill True
    margin (50, 60)

style history_who:
    text_align 0.0
    color '#98FB98'
    font "Arciform.otf"
    outlines [(2, '#000a')]
    size 28

style history_text:
    outlines [(2, '#000a')]
    font "Arciform.otf"
    size 25

style history_label:
    xfill True
    align (.5, .5)

style history_label_text:
    font "Arciform.otf"
    size 28

style history_vscrollbar:
    is pref_vscrollbar

screen setup():
    tag menu

    add renpy.get_screen("bg", layer='master')

    default chapter = 1
    default knight = 0
    default kitsune = False
    default harpy = False
    default kuromu = False
    default sara = False

    if chapter == 1:
        $ knight = 0
        $ kitsune = False
        $ harpy = False
        $ kuromu = False
        $ sara = False

    $ tooltip = GetTooltip()

    frame:
        background None
        xfill True
        yfill True
        margin (30, 60)

        frame:
            style_prefix "prefs"

            grid 2 7:
                style_prefix "pref"
                transpose True

                vbox:
                    label "Сложность"
                    hbox:
                        textbutton "Нормальная":
                            xalign .5
                            action SetField(persistent, "difficulty", 1)

                        textbutton "Сложная":
                            xalign .5
                            action SetField(persistent, "difficulty", 2)
                            tooltip "Враги на ±25% сильнее по сравнению с базовой сложностью."

                        textbutton "Адская":
                            xalign .5
                            action SetField(persistent, "difficulty", 3)
                            tooltip "Враги на ±50% сильнее по сравнению с базовой сложностью.\nОчень сложно!"
                vbox:
                    label "Сцены пожирания"
                    hbox:
                        textbutton "Отключены":
                            xalign .5
                            action SetField(persistent, "vore", False)
                        textbutton "Включены":
                            xalign .5
                            action SetField(persistent, "vore", True)

                vbox:
                    textbutton "Режим истории":
                        style "pref_label"
                        action NullAction()
                        tooltip "Включает пропуск битв.\nНельзя менять после начала игры."
                    hbox:
                        textbutton "Отключен":
                            xalign .5
                            action SetField(world, "battle_skip", False)
                        textbutton "Включен":
                            xalign .5
                            action SetField(world, "battle_skip", True)

                vbox:
                    label "Музыка"
                    hbox:
                        textbutton "Оригинальная":
                            xalign .5
                            action SetField(persistent, "music", False)
                            tooltip "Свободная музыка без копирайта.\nПолезно, если хотите залить на YouTube."
                        textbutton "NG+ музыка":
                            xalign .5
                            action SetField(persistent, "music", True)
                            tooltip "Понатыренная из разных игр авторская музыка."

                null

                null

                null

                # Вторая колонка

                if persistent.game_clear:
                    vbox:
                        textbutton "Глава":
                            style "pref_label"
                            action NullAction()
                            tooltip "С какой главы начать игру.\nНовые главы открываются по мере их достижения в сюжете."
                        hbox:
                            textbutton "Первая":
                                xalign .5
                                action SetScreenVariable("chapter", 1)
                            textbutton "Вторая":
                                xalign .5
                                action SetScreenVariable("chapter", 2)
                            if persistent.game_clear > 1:
                                textbutton "Третья":
                                    xalign .5
                                    action SetScreenVariable("chapter", 3)

                if chapter > 1:
                    vbox:
                        label "Небесный рыцарь"
                        hbox:
                            textbutton "Нет":
                                xalign .5
                                action SetScreenVariable("knight", 0)
                            textbutton "Гранберия":
                                xalign .5
                                action SetScreenVariable("knight", 2)
                            textbutton "Тамамо":
                                xalign .5
                                action SetScreenVariable("knight", 3)
                            if chapter > 2:
                                textbutton "Альма":
                                    xalign .5
                                    action SetScreenVariable("knight", 4)
                            if chapter > 3:
                                textbutton "Эрубети":
                                    xalign .5
                                    action SetScreenVariable("knight", 5)

                    vbox:
                        label "Компаньоны"
                        hbox:
                            textbutton "Кицунэ":
                                xalign .5
                                action ToggleScreenVariable("kitsune")
                            textbutton "Гарпия":
                                xalign .5
                                action ToggleScreenVariable("harpy")
                            if chapter > 3:
                                textbutton "Курому":
                                    xalign .5
                                    action ToggleScreenVariable("kuromu")
                                textbutton "Сара":
                                    xalign .5
                                    action ToggleScreenVariable("sara")

    label "Игровые настройки":
        text_font "Skandal.ttf"
        text_size 40
        xalign 0.5
        yalign 0.02

    grid 2 1:
        yalign 0.98
        xfill True

    if tooltip is not None:
        text "[tooltip]" align (.03, .98)

    textbutton "Продолжить":
        align (.97, .98)
        text_size 24

        action Return([
            chapter,
            knight,
            kitsune,
            harpy,
            kuromu,
            sara
        ])

    key "game_menu" action Return([
            chapter,
            knight,
            kitsune,
            harpy,
            kuromu,
            sara
        ])
    key "spacebar" action Return([
            chapter,
            knight,
            kitsune,
            harpy,
            kuromu,
            sara
        ])
