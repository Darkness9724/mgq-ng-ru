#########################################################################
#
# Меню городов и лагерей
#
screen city():
    predict False

    frame:
        style_prefix 'city'

        grid gui.city_cols gui.city_rows:
            xfill True
            transpose True

            for key, value in world.city.button.items():
                if value is not None:
                    textbutton "%s" % value:
                        action If(world.city.action[key] < 1, Return(key))
                else:
                    text ""

    textbutton world.city.exit:
        background Frame(persistent.textbox,
                        15, 15, tile=True
                    )
        action If(world.city.exit is not None, Return(19))
        align (.5, .85)
        xminimum 300
        yminimum 50
        text_font "Blogger-Sans.otf"
        text_outlines [(2, "#000", 0, 0)]
        text_size 21
        text_idle_color "#fff"
        text_hover_color "#00f"
        text_insensitive_color "#80808060"
        text_insensitive_outlines [(2, "#00000080", 0, 0)]

    if renpy.variant("touch"):
        hbox:
            align (.5, 1.0)

            imagebutton:
                at xyzoom(.9)
                auto "phone/button/back_%s.webp"
                margin (5, 3)
                action Rollback()
            imagebutton:
                at xyzoom(.9)
                auto "phone/button/save_%s.webp"
                margin (5, 3)
                action ShowMenu("save")
            imagebutton:
                at xyzoom(.9)
                auto "phone/button/load_%s.webp"
                margin (5, 3)
                action ShowMenu("load")
            imagebutton:
                at xyzoom(.9)
                auto "phone/button/menu_%s.webp"
                margin (5, 3)
                action ShowMenu()


style city_frame:
    background Frame(persistent.textbox,
                        15, 15
                    )
    align (.5, .45)
    xysize (700, 300)
    padding (20, 20)

style city_frame:
    variant "touch"
    align (.5, .25)
    xysize (740, 400)

style city_grid:
    variant "touch"
    spacing 10

style city_button:
    is default
    background None

style city_button_text:
    is default
    font "Blogger-Sans.otf"
    outlines [(2, "#000")]
    size 21
    kerning .2
    line_leading 2
    line_spacing 2
    idle_color "#fff"
    hover_color "#00f"
    insensitive_color "#80808060"
    insensitive_outlines [(2, "#00000080")]
