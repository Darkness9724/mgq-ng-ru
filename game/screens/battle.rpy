init python:
    class Cmd():
        def __init__(self):
            self.clear()

        def clear(self):
            for idx in range(1, 9):
                setattr(self, "button%d_text" % idx, "")
                setattr(self, "button%d_action" % idx, None)

            self.switch_buttons = False
            self.switch_buttons_action = None

        def attack(self):
            self.clear()

            self.button1_text = "Атака"
            self.button1_action = 1 if world.battle.cmd[0] else None

            self.button2_text = "Навыки"
            self.button2_action = 7 if world.battle.cmd[6] and world.party.player.bind == 0 else None

            self.button3_text = "Вырываться"
            self.button3_action = 2 if world.battle.cmd[1] and world.party.player.bind > 0 else None

            self.button4_text = "Защита"
            self.button4_action = 3 if world.battle.cmd[2] and world.party.player.bind == 0 else None

            self.button5_text = "Подождать"
            self.button5_action = 4 if world.battle.cmd[3] else None

            self.button6_text = "Сдаться"
            self.button6_action = 5 if world.battle.cmd[4] else None

            self.button7_text = "Попросить об атаке" if not world.battle.ng else ""
            self.button7_action = 6 if world.battle.cmd[5] and not world.battle.ng else None

            self.button8_text = "Компаньоны" if any([(member.support) for member in world.party.member if member is not None]) else ""
            self.button8_action = 8 if world.battle.cmd[7] and any([(member.support) for member in world.party.member if member is not None]) and not world.party.support_turn else None

        def onedari_clear(self):
            for idx in range(1, 17):
                setattr(store, "list%d" % idx, "")
                setattr(store, "list%d_unlock" % idx, 0)

        def onedari(self):
            self.clear()

            if world.battle.enemy[0].list[8]:
                self.switch_buttons = True
                self.switch_buttons_action = self.onedari2

            for idx in range(1, 9):
                setattr(self, "button%d_text" % idx, getattr(store, "list%d" % idx))
                if getattr(store, "list%d_unlock" % idx):
                    setattr(self, "button%d_action" % idx, idx)

        def onedari2(self):
            self.clear()
            self.switch_buttons = True
            self.switch_buttons_action = self.onedari

            for idx in range(1, 9):
                setattr(self, "button%d_text" % idx, getattr(store, "list%d" % (idx+8)))
                if getattr(store, "list%d_unlock" % idx):
                    setattr(self, "button%d_action" % (idx+8), (idx+8))

        def skill_sylph(self):
            self.clear()

            self.button1_text = "Повторить призыв - 1" if world.party.player.wind else "Призвать Сильфу - 1"
            self.button1_action = 11 if world.party.player.mp > 0 else None

            self.button5_text = "Белые ветра - 2" if world.party.player.sylph_skill[0] else ""
            self.button5_action = 31 if world.party.player.mp > 1 else None

            self.button2_text = "Чи-па-па - 1" if world.party.player.sylph_skill[1] else ""
            self.button2_action = 32 if world.party.player.mp > 0 else None

            self.button3_text = "Подавление земли - 6" if world.party.player.sylph_skill[2] else ""
            self.button3_action = 33 if world.party.player.mp > 5 else None

        def skill_gnome(self):
            self.clear()

            self.button1_text = "Повторить призыв - 1"
            self.button1_action = 12 if world.party.player.mp > 0 else None

            self.button5_text = "Укоренение - 4" if world.party.player.gnome_skill[0] else ""
            self.button5_action = 41 if world.party.player.mp > 3 else None

            self.button2_text = "Мощь земли - 3" if world.party.player.gnome_skill[1] else ""
            self.button2_action = 42 if world.party.player.mp > 2 else None

            self.button3_text = "Подавление ветра - 6" if world.party.player.gnome_skill[2] else ""
            self.button3_action = 43 if world.party.player.mp > 5 else None

        def skill_undine(self):
            self.clear()

            self.button1_text = "Уровень 2 - ВСЕ"
            self.button1_action = 13 if world.party.player.mp > 3 else None

            if world.party.player.aqua == 2:
                self.button5_text = "Уровень 3 - 0"
                self.button5_action = 16 if world.party.player.aqua == 2 else None
            elif world.party.player.aqua != 2:
                self.button5_text = "Уровень 3 - 2"
                self.button5_action = 16 if world.party.player.mp > 1 else None

            self.button2_text = "Очищение - 2" if world.party.player.undine_skill[1] else ""
            self.button2_action = 51 if world.party.player.mp > 1 else None

            self.button3_text = "Аква-пентаграмма - 3" if world.party.player.undine_skill[2] else ""
            self.button3_action = 52 if world.party.player.mp > 2 else None

            self.button4_text = "Подавление огня - 6" if world.party.player.undine_skill[3] else ""
            self.button4_action = 53 if world.party.player.mp > 5 else None

        def skill_salamander(self):
            self.clear()

            self.button1_text = "Повторить призыв - 4"
            self.button1_action = None

            self.button5_text = "Берсерк - 5" if world.party.player.salamander_skill[0] else ""
            self.button5_action = 61 if world.party.player.mp > 4 else None

            self.button2_text = "Сгори! - 4" if world.party.player.salamander_skill[1] else ""
            self.button2_action = 62 if world.party.player.mp > 3 else None

            self.button3_text = "Подавление воды - 6" if world.party.player.salamander_skill[2] else ""
            self.button3_action = 63 if world.party.player.mp > 5 else None

        def companions(self):
            self.clear()

            if world.party.member[1].support:
                self.button1_text = world.party.member[1].name[0].name
                self.button1_action = 1

            if world.party.member[2].support:
                self.button2_text = world.party.member[2].name[0].name
                self.button2_action = world.actor.index(world.party.member[2])

        def granberia_skills(self):
            self.clear()

            self.button1_text = "Проклятый клинок: Обезглавливание - 2"
            self.button1_action = 201 if world.party.player.mp > 1 else None

            self.button2_text = "Громовой выпад: Торнадо - 2"
            self.button2_action = 202 if world.party.player.mp > 1 else None

            self.button3_text = "Удар дракона-мясника - 3"
            self.button3_action = 203 if world.party.player.mp > 2 else None

            self.button4_text = "Крушитель черепов: Очищение - 3"
            self.button4_action = 204 if world.party.player.mp > 2 else None

            self.button5_text = "Гибельный клинок звезды хаоса - 3"
            self.button5_action = 205 if world.party.player.mp > 2 else None

            self.button6_text = "Неудержимый испепеляющий клинок - 4"
            self.button6_action = 206 if world.party.player.mp > 3 else None

            self.button7_text = "Звёздный клинок - 10 (Лука: 8)"
            self.button7_action = 207 if world.party.player.mp > 9 and world.actor[0].mp > 7 else None

        def alma_skills(self):
            self.clear()

            self.button1_text = "Шамшир - 3"
            self.button1_action = 301 if world.party.player.mp > 2 else None

            self.button2_text = "Ангельский поцелуй - 6"
            self.button2_action = 302 if world.party.player.mp > 5 else None

            self.button3_text = "Соблазнение - 8"
            self.button3_action = 303 if world.party.player.mp > 7 else None

            self.button5_text = "Девятиликий Ракшаса: Пожар - 5 (Лука: 6)"
            self.button5_action = 304 if world.party.player.mp > 4 and world.actor[0].mp > 5 else None

        def luka_skill1(self):
            self.clear()

            if world.party.player.skill[0] > 18 and world.party.player.skillset > 1:
                self.luka_skillx1()
                return

            if world.party.player.skill[0] > 25 or world.party.player.skill[1] or world.party.player.skill[2] or world.party.player.skill[3] or world.party.player.skill[4]:
                self.switch_buttons = True
                self.switch_buttons_action = self.luka_skill2

            if world.party.player.skill[0] > 13 or world.party.player.skillset == 1:
                self.button1_text = "Безмятежный Демонический Клинок - 2"
                self.button1_action = 8 if world.party.player.mp > 1 else None
            elif world.party.player.skill[0]:
                self.button1_text = "Демоническое Обезглавливание - 2"
                self.button1_action = 1 if world.party.player.mp > 1 else None

            if world.party.player.skill[0] > 10 or world.party.player.skillset == 1:
                self.button2_text = "Молниеносный Удар Клинка - 2"
                self.button2_action = 6 if world.party.player.mp > 1 else None
            elif world.party.player.skill[0] > 2:
                self.button2_text = "Громовой Выпад - 2"
                self.button2_action = 3 if world.party.player.mp > 1 else None

            if world.party.player.skill[0] > 11 or world.party.player.skillset == 1:
                self.button3_text = "Сотрясающее Землю Обезглавливание - 3"
                self.button3_action = 7 if world.party.player.mp > 2 else None
            elif world.party.player.skill[0] > 4:
                self.button3_text = "Демонический Крушитель Черепов - 3"
                self.button3_action = 5 if world.party.player.mp > 2 else None

            if world.party.player.skill[0] > 12 or world.party.player.skillset == 1:
                self.button4_text = "Неудержимый Испепеляющий Клинок - 4"
                self.button4_action = 10 if world.party.player.mp > 3 else None
            elif world.party.player.skill[0] > 5:
                self.button4_text = "Гибельный Клинок Звезды Хаоса - 4"
                self.button4_action = 2 if world.party.player.mp > 3 else None
            elif world.party.player.skill[0] > 1:
                self.button4_text = "Беспорядочные удары - 2"
                self.button4_action = 2 if world.party.player.mp > 1 else None

            if world.party.player.skill[0] > 3:
                self.button5_text = "{color=#FF8000}Медитация - 3{/color}" if world.party.player.guard_on and world.party.player.skill[0] > 26 else "Медитация - 3"
                self.button5_action = 4 if world.party.player.mp > 2 and (world.party.player.fire == 0 or world.party.player.fire > 2) else None

            if world.party.player.skill[0] > 25:
                self.button6_text = "{color=#FF8000}Разгон - 0{/color}" if world.party.player.guard_on and world.party.player.skill[0] > 26 else "Разгон - 0"
                self.button6_action = 26 if world.party.player.life > 0 and world.party.player.fire == 0 and world.party.player.aqua != 2 else None
            else:
                self.button6_text = "Крайность - 0"
                self.button6_action = 15 if world.party.player.life > 0 else None

            if world.party.player.skill[0] > 20 and world.party.player.skill[1] and world.party.player.skill[2] and world.party.player.skill[3] and world.party.player.skill[4]:
                self.button8_text = "Четырёхкратный Гига-удар - 1"
                self.button8_action = 9 if world.party.player.mp > 0 else None
            elif not world.party.player.skill[3] and world.party.player.skill[0] > 17:
                self.button8_text = "{color=#00BFFF}Безмятежный разум - 2{/color}" if world.party.player.aqua == 4 else "Безмятежный разум - 2"
                self.button8_action = 16 if world.party.player.mp > 1 else None

        def luka_skillx1(self):
            self.clear()

            if world.party.player.skill[0] > 25 or world.party.player.skill[1] or world.party.player.skill[2] or world.party.player.skill[3] or world.party.player.skill[4]:
                self.switch_buttons = True
                self.switch_buttons_action = self.luka_skill2

            self.button1_text = "Мгновенное Убийство - 2"
            self.button1_action = 17 if world.party.player.mp > 1 else None

            self.button2_text = "Возрождение Небесного Демона - 4"
            self.button2_action = 19 if world.party.player.mp > 3 else None

            self.button3_text = "Девятиликий Ракшаса - 6"
            self.button3_action = 18 if world.party.player.mp > 5 else None

            if not world.party.player.daystar:
                self.button4_text = "Денница - 8"
            elif world.party.player.daystar:
                self.button4_text = "{color=#00FFFF}Денница (заряжена){/color}"
            self.button4_action = 24 if world.party.player.mp > 7 and world.party.player.daystar < 1 else None

            if world.party.player.skill[0] > 3:
                self.button5_text = "{color=#FF8000}Медитация - 3{/color}" if world.party.player.guard_on and world.party.player.skill[0] > 26 else "Медитация - 3"
                self.button5_action = 4 if world.party.player.mp > 2 and (world.party.player.fire == 0 or world.party.player.fire > 2) else None

            if world.party.player.skill[0] > 25:
                self.button6_text = "{color=#FF8000}Разгон - 0{/color}" if world.party.player.guard_on and world.party.player.skill[0] > 26 else "Разгон - 0"
                self.button6_action = 26 if world.party.player.life > 0 and world.party.player.fire == 0 and world.party.player.aqua != 2 else None
            else:
                self.button6_text = "Крайность - 0"
                self.button6_action = 15 if world.party.player.life > 0 else None

            if world.party.player.holy:
                self.button7_text = "{color=#006400}Танец Падшего Ангела - 2{/color}"
            else:
                self.button7_text = "Танец Падшего Ангела - 2"
            self.button7_action = 25 if world.party.player.mp > 1 else None

            if world.party.player.skill[3]:
                self.button8_text = "Безмятежный Демонический Клинок - 2"
                self.button8_action = 8 if world.party.player.mp > 1 else None
            elif not world.party.player.skill[3] and world.party.player.skill[0] > 17:
                self.button8_text = "{color=#00BFFF}Безмятежный разум - 2{/color}" if world.party.player.aqua == 4 else "Безмятежный разум - 2"
                self.button8_action = 16 if world.party.player.mp > 1 else None

        def luka_skill2(self):
            self.clear()

            self.switch_buttons = True
            self.switch_buttons_action = self.luka_skill1

            if 0 < world.party.player.skill[1] < 2:
                self.button1_text = "Сильфа - 2"
                self.button1_action = 11 if world.party.player.mp > 1 and world.party.player.wind == 0 and world.party.player.aqua < 2 else None
            elif world.party.player.skill[1] == 2:
                self.button1_text = "Сильфа - 2"
                self.button1_action = 11 if world.party.player.mp > 1 and world.party.player.wind == 0 and world.party.player.aqua < 2 and world.party.player.fire == 0 else None
            elif world.party.player.skill[1] == 3:
                if (world.party.player.wind or world.party.player.holy):
                    self.button1_text = "{color=00C957}Сильфа{/color}"
                    if world.party.player.aqua != 2:
                        self.button1_action = 101 if world.party.player.mp > 0 and world.party.player.fire != 1 and world.party.player.fire != 2 else None
                    else:
                        self.button1_action = 101 if world.party.player.mp > 0 and (world.party.player.aqua == 0 or world.party.player.aqua == 3) and (world.party.player.fire == 0 or world.party.player.fire == 3) else None
                else:
                    self.button1_text = "Сильфа - 1"
                    self.button1_action = 11 if world.party.player.mp > 0 and ((world.party.player.aqua == 0 or world.party.player.aqua > 2) or (world.party.player.aqua == 2 and world.party.player.skill[3] > 5)) and (world.party.player.fire == 0 or world.party.player.fire == 3) else None

            if 0 < world.party.player.skill[2] < 2:
                self.button2_text = "Гнома - 2"
                self.button2_action = 12 if world.party.player.mp > 1 and world.party.player.earth == 0 and world.party.player.aqua < 2 else None
            elif world.party.player.skill[2] == 2:
                self.button2_text = "Гнома - 2"
                self.button2_action = 12 if world.party.player.mp > 1 and world.party.player.earth == 0 and world.party.player.aqua < 2 and world.party.player.fire == 0 else None
            elif world.party.player.skill[2] == 3:
                if world.party.player.earth:
                    self.button2_text = "{color=F4A460}Гнома{/color}"
                    if world.party.player.aqua != 2:
                        self.button2_action = 102 if world.party.player.mp > 0 and world.party.player.fire != 1 and world.party.player.fire != 2 else None
                    else:
                        self.button1_action = 102 if world.party.player.mp > 0 and (world.party.player.aqua == 0 or world.party.player.aqua == 3) and (world.party.player.fire == 0 or world.party.player.fire == 3) else None
                else:
                    self.button2_text = "Гнома - 1"
                    self.button2_action = 12 if world.party.player.mp > 0 and ((world.party.player.aqua == 0 or world.party.player.aqua > 2) and (world.party.player.fire == 0 or world.party.player.fire == 3)) or world.party.player.aqua != 2 else None

            if world.party.player.skill[3] > 1:
                self.button3_text = "{color=00BFFF}Ундина{/color}" if world.party.player.aqua else "Ундина"
                self.button3_action = 103 if world.party.player.mp > 1 else None

            if 0 < world.party.player.skill[4] < 2:
                self.button4_text = "{color=FF6347}Саламандра{/color}"
                self.button4_action = 14 if world.party.player.mp > 1 and world.party.player.fire == 0 and world.party.player.aqua < 2 else None
            elif world.party.player.skill[4] == 2:
                self.button4_text = "{color=FF6347}Саламандра{/color}"
                self.button4_action = 14 if world.party.player.mp > 1 and world.party.player.fire == 0 and world.party.player.aqua < 2 else None
            elif world.party.player.skill[4] == 3:
                if world.party.player.fire:
                    self.button4_text = "{color=FF6347}Саламандра{/color}"
                    self.button4_action = 104 if world.party.player.mp > 3 and world.party.player.aqua != 2 else None
                else:
                    self.button4_text = "Саламандра - 4"
                    self.button4_action = 14 if world.party.player.mp > 3 and (world.party.player.aqua > 2 or world.party.player.aqua < 2) else None

            if world.party.player.skill[0] > 21 and world.party.player.skill[1] and world.party.player.skill[2] and world.party.player.skill[3] and world.party.player.skill[4]:
                self.button5_text = "Вызов четырёх духов - 6"
                self.button5_action = 23 if world.party.player.mp > 5 else None

            if world.party.player.skill[1] and world.party.player.skill[2] and world.party.player.skill[3] and world.party.player.skill[4]:
                self.button6_text = "Элементальная Спика - 10"
                self.button6_action = 22 if world.party.player.mp > 9 and (world.party.player.wind or world.party.player.holy) and world.party.player.earth and world.party.player.aqua and world.party.player.fire else None
            if world.party.player.skill[0] > 20 and world.party.player.skill[1] and world.party.player.skill[2] and world.party.player.skill[3] and world.party.player.skill[4] and world.party.player.skillset == 2:
                self.button7_text = "Четырёхкратный Гига-удар - 1"
                self.button7_action = 9 if world.party.player.mp > 0 else None
            if world.party.player.skill[0] > 25:
                self.button8_text = "Крайность - 0"
                self.button8_action = 15 if world.party.player.life > 0 else None

    cmd = Cmd()

screen cmd(act):
    predict False
    $ LM = 5
    $ RM = 5
    default var=getattr(cmd, act)()

    if world.actor[0].wind or world.actor[0].aqua or world.actor[0].fire or world.actor[0].earth or world.actor[0].holy:
        $ LM = 25
        use elm

    if world.battle.enemy[0].wind or world.battle.enemy[0].aqua or world.battle.enemy[0].fire or world.battle.enemy[0].earth or world.battle.enemy[0].holy:
        $ RM = 25
        use enemy_elm

    vbox:
        yalign 1.0
        yoffset -75

        if renpy.variant("touch"):

            if act != "attack":
                imagebutton:
                    at xyzoom(2.0)
                    auto "button/left_%s.webp"
                    margin (5, 3)
                    xalign 1.0

                    action Return(-1)

            if act == "attack":
                imagebutton:
                    at xyzoom(.9)
                    auto "phone/button/menu_%s.webp"
                    margin (5, 3)
                    xalign 1.0
                    action ShowMenu()

            if cmd.switch_buttons:
                hbox:
                    xfill True

                    imagebutton:
                        xalign 0
                        at xyzoom(2.0)
                        auto "button/left_%s.webp"
                        margin (5, 3)
                        action Function(cmd.switch_buttons_action)

                    imagebutton:
                        xalign 1.0
                        at xyzoom(2.0)
                        auto "button/right_%s.webp"
                        margin (5, 3)
                        action Function(cmd.switch_buttons_action)

        if not renpy.variant("touch") and cmd.switch_buttons:
            if cmd.switch_buttons_action:
                frame:
                    background Frame(persistent.textbox, Borders(5, 5, 5, 5))

                    xmargin 0
                    xfill True
                    padding (0, 5)
                    align (.5, 1.0)
                    left_margin LM
                    right_margin RM

                    has grid 2 1:
                        xfill True

                    imagebutton:
                        xmargin 2
                        align (0.05, 0.5)
                        auto "button/left_%s.webp"

                        action Function(cmd.switch_buttons_action)
                    imagebutton:
                        xmargin 2
                        align (0.95, 0.5)
                        auto "button/right_%s.webp"

                        action Function(cmd.switch_buttons_action)

        frame:
            background Frame(persistent.textbox, Borders(15, 15, 15, 15))
            style_prefix "attack"

            left_margin LM
            right_margin RM

            xmargin 0
            xfill True
            padding (0, 5)
            align (.5, 1.0)

            has grid 2 4:
                transpose True
                xfill True

            for idx in range(1, 9):
                textbutton getattr(cmd, "button%d_text" % idx):
                    if getattr(cmd, "button%d_action" % idx):
                        action Return(getattr(cmd, "button%d_action" % idx))

    if act != "attack":
        key "game_menu" action Return(-1)

screen hp():
    if world.battle is not None and world.battle.element_turn:
        use turn_scorer
    if not renpy.get_screen("choice"):
        window:
            id 'window'
            background Frame(persistent.textbox, Borders(15, 15, 15, 15))
            ysize 70
            ypadding 0
            margin (5, 0, 5, 5)

            has hbox

            add "face" xoffset 5 yalign .5

            grid 2 1:
                yalign 1.0
                xfill True
                spacing 5
                style_prefix "hp"

                vbox:
                    xfill True
                    spacing 0

                    text "[world.party.player.name[0]]" align (0, 0)

                    frame:
                        if world.party.player == world.actor[0]:
                            bar:
                                align (1.0, 1.0)
                                value AnimatedValue(range=world.party.player.max_life, value=world.party.player.life, delay=.2)
                                right_bar Frame("battle/bar_full.webp", Borders(5, 5, 5, 5))
                                left_bar Frame("battle/bar_empty.webp", Borders(5, 5, 5, 5))
                                bar_invert True

                            text f"{world.party.player.life}/{world.party.player.max_life}":
                                align (.52, 0.5)
                                size 16
                                outlines [(2, "#000")]
                        else:
                            bar:
                                align (1.0, 1.0)
                                value AnimatedValue(range=world.party.player.max_mp, value=world.party.player.mp, delay=.2)
                                right_bar Frame(im.MatrixColor("battle/bar_full.webp", im.matrix.colorize("#960000", "#ff0000")), Borders(5, 5, 5, 5))
                                left_bar Frame("battle/bar_empty.webp", Borders(5, 5, 5, 5))
                                bar_invert True

                            text f"SP: {world.party.player.mp}/{world.party.player.max_mp}":
                                align (.52, 0.5)
                                size 16
                                outlines [(2, "#000")]

                    if world.party.player == world.actor[0]:
                        hbox:
                            xoffset 20

                            text 'SP: [world.party.player.mp]'

                vbox:
                    xfill True
                    spacing 0

                    text '[world.battle.name]' align (0.98, 0)

                    frame:
                        bar:
                            yalign .49
                            value AnimatedValue(world.battle.get_life(), range=world.battle.get_life("max"), delay=.2)

                            left_bar Frame("battle/bar_full.webp", Borders(5, 5, 5, 5))
                            right_bar Frame("battle/bar_empty.webp", Borders(5, 5, 5, 5))

                        # if config.developer:
                        #     text f"{world.battle.get_life()}/{world.battle.get_life('max')}":
                        #         align (.52, 0.5)
                        #         size 16
                        #         outlines [(2, "#000")]

style hp_text:
    is default
    size 14
    font "Blogger-Sans.otf"
    outlines [(1, "#000")]
    bold True

style hp_bar:
    is bar
    right_gutter 0
    left_gutter 0
    top_gutter 0
    bottom_gutter 0
    thumb None
    thumb_offset 0
    xsize 330
    ysize 20

style hp_frame:
    is frame
    background None
    padding (0, 0)
    margin (0, 0)
    ysize 20

style skillname:
    font "anime_ace.ttf"
    size 28
    italic True
    bold True
    color '#fff000'
    outlines [(2, "#00a", 2, 2),(2, "#000")]

style continents:
    font "MarckScript-Regular.ttf"
    size 23
    color '#fff'
    outlines [(2, "#000")]

style counter:
    size 90
    bold True
    italic True
    color "#cc0000"
    outlines [(4, "#000", 0, 0)]
    font "Skandal.ttf"

############################################################################
# Attack screen
#
#

style attack_window:
    yminimum 115
    xpadding 4

style attack_grid:
    is default
    align (.5, 5)
    xfill True

style attack_button:
    is default
    xalign .5
    padding (0, 0)
    margin (0, 0)
    background None

style attack_button_text:
    is text
    font "Blogger-Sans.otf"
    size 21
    kerning -0.5
    line_leading 1
    line_spacing 2
    idle_color "#fff"
    hover_color "#f00"
    insensitive_color "#8888"
    idle_outlines [(2, "#000c")]
    hover_outlines [(2, "#000c")]
    insensitive_outlines [(2, "#303030cc")]

style attack_button_text:
    variant "touch"
    line_leading 5
    line_spacing 5

screen turn_scorer():
    style_prefix "turn_scorer"

    frame:
        if renpy.variant("touch"):
            pos (4, 220)
        else:
            pos (4, 270)
        xsize 100

        grid 2 4:
            yalign 1.0
            spacing 1

            if world.actor[0].holy:
                text "Свет:" color "#f0f0f0"
                text "[world.actor[0].holy_turn]" color "#f0f0f0" xalign 0.0
            else:
                text "Ветер:" color "#008000"
                text "[world.actor[0].wind_turn]" color "#008000" xalign 0.0
            text "Земля:" color "#ad806b"
            text "[world.actor[0].earth_turn]" color "#A0522D" xalign 0.0
            text "Вода:" color "#00FFFF"

            if world.actor[0].aqua != 2:
                text "[world.actor[0].aqua_turn]" color "#00FFFF" xalign 0.0
            else:
                text "[world.actor[0].mp]" color "#00FFFF" xalign 0.0

            text "Огонь:" color "#FF0000"
            text "[world.actor[0].fire_turn]" color "#FF0000" xalign 0.0

style turn_scorer_frame:
    is frame
    background Frame(persistent.namebox, Borders(10, 10, 10, 10))
    padding Borders(10, 10, 10, 10).padding

style turn_scorer_text:
    is default
    size 14
    outlines [(2, '#000c')]
    font "Arciform.otf"
    xalign 1.0

screen elm():
    vbox:
        if not renpy.get_screen("hp"):
            pos (2, 450)
        else:
            pos (2, 420)

        spacing 4


        if world.actor[0].wind:
            add 'elm_wind'
        elif world.actor[0].holy:
            add 'elm_holy'
        else:
            null height 20

        if world.actor[0].earth:
            add 'elm_earth'
        else:
            null height 20

        if world.actor[0].aqua:
            add 'elm_aqua'
        else:
            null height 20

        if world.actor[0].fire:
            add 'elm_fire'
        else:
            null height 20

screen enemy_elm():
    vbox:
        if not renpy.get_screen("hp"):
            pos (778, 450)
        else:
            pos (778, 420)

        spacing 4

        if world.battle.enemy[0].wind:
            add "battle/elm_wind.webp"
        else:
            null height 20

        if world.battle.enemy[0].earth:
            add "battle/elm_earth.webp"
        else:
            null height 20

        if world.actor[0].poison > 0:
            add "battle/elm_poison.webp"
        elif world.battle.enemy[0].aqua:
            add "battle/elm_aqua.webp"
        else:
            null height 20

        if world.battle.enemy[0].fire:
            add "battle/elm_fire.webp"
        else:
            null height 20

screen badend(img="", bad1="", bad2=""):
    add img yalign 0.5

    style_prefix "bad_end"

    frame:
        align (0.8, 0.5)

        vbox:
            text "Проигрышей: [persistent.count_end]" size 22 color "#f00"
            null height 20

            text "[bad1]"
            text "[bad2]"


style bad_end_frame:
    background Frame(persistent.win_bg, 5, 5)
    xsize 420
    padding (20, 20)

style bad_end_text:
    size 19
    color "#ffff00"
    outlines [(2, "#000")]
    font "Arciform.otf"
    xoffset 10

screen kitsune():
    imagebutton:
        idle "youko st02"
        hover "youko st02 hover"
        focus_mask True
        pos (-230, 0)
        action Return(1)

    imagebutton:
        idle "youko st01"
        hover "youko st01 hover"
        focus_mask True
        pos (0, 0)
        action Return(2)

    imagebutton:
        idle "youko st01"
        hover "youko st01 hover"
        focus_mask True
        pos (230, 0)
        action Return(3)


screen kitsune():
    variant "touch"

    default k = 2

    if k == 1:
        add "youko st02 hover" pos (-230, 0)
    elif k == 2:
        add "youko st01 hover" pos (0, 0)
    elif k == 3:
        add "youko st01 hover" pos (230, 0)

    imagebutton:
        align (0, .5)
        auto "left_%s_n"
        margin (2, 5)
        action [If(k > 1, true=SetScreenVariable('k', k-1), false=SetScreenVariable('k', 3))]

    imagebutton:
        align (1.0, .5)
        auto "right_%s_n"
        margin (2, 5)
        action [If(k < 3, true=SetScreenVariable('k', k+1), false=SetScreenVariable('k', 1))]

    textbutton "Confirm":
        action Return(k)
        style "gm_button"
        background Frame(persistent.win_bg, Borders(10, 10, 10, 10))
        text_size 30
        align (.5, .9)
