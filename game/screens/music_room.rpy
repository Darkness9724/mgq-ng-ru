init python:
    def add_files_to_mr():
        music_files = (
            "title", "irias", "mura1", "kiki1", "battle",
            "ero1", "zukan", "hutuu1", "kiki2", "sinpi1",
            "alice1", "maou", "comi1", "field1", "yaei",
            "boss0", "sitenno", "city1", "amira", "dungeon1",
            "mura2", "boss1", "dance", "epilogue", "enrika",
            "tamamo", "hutuu2", "umi", "field3", "castle1",
            "dungeon2", "umi2", "obakeyasiki", "seireinomori", "yonseirei2",
            "yonseirei", "boss2", "sabaku", "sabasa", "castle2",
            "dungeon3", "dance2", "hutuu3", "mura3", "madou1",
            "hutuu4", "safar", "ending", "colosseum", "castle3",
            "hubuki", "yamatai", "plansect", "ruka", "izumi",
            "castle4", "kazan", "gordport", "maou2", "aqua",
            "alice2", "galda", "manotairiku", "remina", "maouzyou",
            "boss3", "boss4", "stein", "labo1", "kanasimi1",
            "kanasimi2", "kiki3", "field4", "epilogue2", "battle2",
            "maouzyou2", "sabasa2", "labo2", "labo3", "heso",
            "tower", "tenkai", "eden", "stein2", "maou3",
            "maou4", "irias2", "irias3", "epilogue3", "ending2"
        )

        for i in music_files:
            mr.add("bgm/%s.ogg" % i, always_unlocked=True)

    def add_files_to_mr_ng():
        music_files = (
            "gn_night", "zukan", "granberia", "granberia2", "tamamo",
            "alma", "erubetie", "boss", "boss0", "boss2", "boss3", "boss4",
            "boss5", "field5", "field6", "field7", "field8",
            "field9", "field10", "erubetie", "comi2", "colosseum",
            "ruka2", "ruka3", "ruka4", "spirits", "spirits2",
            "iliasbattle", "hutuu5", "alice8th1", "hutuu5", "hutuu6",
            "mystery1", "shirome", "pyramid", "umi3", "porttown",
            "succubusvillage", "volcano", "volcano2", "sisters1",
            "irias4"
        )

        for i in music_files:
            mr.add("ngdata/bgm/%s.ogg" % i, always_unlocked=True)

    # Инициализация музыкальной комнаты
    mr = MusicRoom(fadeout=1.0)

    # Добавление музыки в плеер
    add_files_to_mr()
    add_files_to_mr_ng()

# Музыкальная комната
screen music_room():
    if renpy.variant('touch'):
        $ zoom_factor = 2.0
        $ YALIGN = 1.0
        $ BUTTON_L = "button/left_%s.webp"
        $ BUTTON_R = "button/right_%s.webp"
    else:
        $ zoom_factor = 1.0
        $ YALIGN = 0.5
        $ BUTTON_L = "button/left_%s.webp"
        $ BUTTON_R = "button/right_%s.webp"

    tag menu

    default ng = True

    if not ng:
        $ BUTTON_NAME = "NG+ музыка"
        add "bg 140"
    else:
        $ BUTTON_NAME = "Музыка"
        add "bg 164"

    add gui.game_menu_background

    frame:
        style_prefix "pos_duration"
        bar value AudioPositionValue() style "pref_scrollbar"

    frame:
        style_prefix "mr"

        has vpgrid:
            cols 1
            draggable True
            mousewheel True
            scrollbars "vertical"

        # Кнопки проигрывания
        if not ng:
            textbutton "Главная тема" action mr.Play("bgm/title.ogg")
            textbutton "Илиас" action mr.Play("bgm/irias.ogg")
            textbutton "Деревня Илиас" action mr.Play("bgm/mura1.ogg")
            textbutton "Бедствие 1" action mr.Play("bgm/kiki1.ogg")
            textbutton "Обычная битва" action mr.Play("bgm/battle.ogg")
            textbutton "H-сцена" action mr.Play("bgm/ero1.ogg")
            textbutton "Монстропедия" action mr.Play("bgm/zukan.ogg")
            textbutton "Мир 1" action mr.Play("bgm/hutuu1.ogg")
            textbutton "Бедствие 2" action mr.Play("bgm/kiki2.ogg")
            textbutton "Алиса 1" action mr.Play("bgm/sinpi1.ogg")
            textbutton "Алиса 2" action mr.Play("bgm/alice1.ogg")
            textbutton "Битва с владыкой монстров" action mr.Play("bgm/maou.ogg")
            textbutton "Курьёз" action mr.Play("bgm/comi1.ogg")
            textbutton "Приключение 1" action mr.Play("bgm/field1.ogg")
            textbutton "Лагерь" action mr.Play("bgm/yaei.ogg")
            textbutton "Перед Боссом" action mr.Play("bgm/boss0.ogg")
            textbutton "Четвёрка Небесных Рыцарей" action mr.Play("bgm/sitenno.ogg")
            textbutton "Городок" action mr.Play("bgm/city1.ogg")
            textbutton "Амира" action mr.Play("bgm/amira.ogg")
            textbutton "Подземелье 1" action mr.Play("bgm/dungeon1.ogg")
            textbutton "Деревня" action mr.Play("bgm/mura2.ogg")
            textbutton "Босс 1" action mr.Play("bgm/boss1.ogg")
            textbutton "Танец 1" action mr.Play("bgm/dance.ogg")
            textbutton "Эпилог" action mr.Play("bgm/epilogue.ogg")
            textbutton "Энрика" action mr.Play("bgm/enrika.ogg")
            textbutton "Тамамо" action mr.Play("bgm/tamamo.ogg")
            textbutton "Мир 2" action mr.Play("bgm/hutuu2.ogg")
            textbutton "Море" action mr.Play("bgm/umi.ogg")
            textbutton "Приключение 2" action mr.Play("bgm/field3.ogg")
            textbutton "Замок Сан-Илии" action mr.Play("bgm/castle1.ogg")
            textbutton "Библиотека" action mr.Play("bgm/dungeon2.ogg")
            textbutton "Под водой" action mr.Play("bgm/umi2.ogg")
            textbutton "Особняк с призраками" action mr.Play("bgm/obakeyasiki.ogg")
            textbutton "Лес духов" action mr.Play("bgm/seireinomori.ogg")
            textbutton "Четвёрка духов" action mr.Play("bgm/yonseirei2.ogg")
            textbutton "Битва с духами" action mr.Play("bgm/yonseirei.ogg")
            textbutton "Босс 2" action mr.Play("bgm/boss2.ogg")
            textbutton "Пустыня" action mr.Play("bgm/sabaku.ogg")
            textbutton "Сабаса" action mr.Play("bgm/sabasa.ogg")
            textbutton "Замок Сабасы" action mr.Play("bgm/castle2.ogg")
            textbutton "Пирамида" action mr.Play("bgm/dungeon3.ogg")
            textbutton "Танец 2" action mr.Play("bgm/dance2.ogg")
            textbutton "Мир 3" action mr.Play("bgm/hutuu3.ogg")
            textbutton "Деревня охотников на ведьм" action mr.Play("bgm/mura3.ogg")
            textbutton "Особняк Лили" action mr.Play("bgm/madou1.ogg")
            textbutton "Печалька" action mr.Play("bgm/hutuu4.ogg")
            textbutton "Руины Сафару" action mr.Play("bgm/safar.ogg")


            textbutton "Титры" action mr.Play("bgm/ending.ogg")
            textbutton "Колизей" action mr.Play("bgm/colosseum.ogg")
            textbutton "Замок Гранд Ноя" action mr.Play("bgm/castle3.ogg")
            textbutton "Метель" action mr.Play("bgm/hubuki.ogg")
            textbutton "Деревня Яматай" action mr.Play("bgm/yamatai.ogg")
            textbutton "Деревня Плансект" action mr.Play("bgm/plansect.ogg")
            textbutton "Лука (в ярости)" action mr.Play("bgm/ruka.ogg")
            textbutton "Источник Ундины" action mr.Play("bgm/izumi.ogg")
            textbutton "Замок Грангольда" action mr.Play("bgm/castle4.ogg")
            textbutton "Золотой вулкан" action mr.Play("bgm/kazan.ogg")
            textbutton "Голдпорт" action mr.Play("bgm/gordport.ogg")
            textbutton "Чёрная Алиса" action mr.Play("bgm/maou2.ogg")
            textbutton "Безмятежность разума" action mr.Play("bgm/aqua.ogg")
            textbutton "Алиса 3" action mr.Play("bgm/alice2.ogg")
            textbutton "Галда" action mr.Play("bgm/galda.ogg")
            textbutton "Хеллгондо" action mr.Play("bgm/manotairiku.ogg")
            textbutton "Ремина" action mr.Play("bgm/remina.ogg")
            textbutton "Замок владыки монстров" action mr.Play("bgm/maouzyou.ogg")
            textbutton "Босс 3" action mr.Play("bgm/boss3.ogg")
            textbutton "Битва с ангелами" action mr.Play("bgm/boss4.ogg")


            textbutton "Промештейн 1" action mr.Play("bgm/stein.ogg")
            textbutton "Лаборатория Промештейн" action mr.Play("bgm/labo1.ogg")
            textbutton "Печалька 2" action mr.Play("bgm/kanasimi1.ogg")
            textbutton "Печалька 3" action mr.Play("bgm/kanasimi2.ogg")
            textbutton "В атаку!" action mr.Play("bgm/kiki3.ogg")
            textbutton "Приключение 3" action mr.Play("bgm/field4.ogg")
            textbutton "Безмятежность" action mr.Play("bgm/epilogue2.ogg")
            textbutton "Контратака" action mr.Play("bgm/battle2.ogg")
            textbutton "Замок владыки монстров 2" action mr.Play("bgm/maouzyou2.ogg")
            textbutton "Осада Сабасы" action mr.Play("bgm/sabasa2.ogg")
            textbutton "Лаборатория высасывания" action mr.Play("bgm/labo2.ogg")
            textbutton "Биолаборатория" action mr.Play("bgm/labo3.ogg")
            textbutton "Сердце мира" action mr.Play("bgm/heso.ogg")
            textbutton "Вышки Предела" action mr.Play("bgm/tower.ogg")
            textbutton "Рай" action mr.Play("bgm/tenkai.ogg")
            textbutton "Эдем" action mr.Play("bgm/eden.ogg")
            textbutton "Промештейн 2" action mr.Play("bgm/stein2.ogg")
            textbutton "Чёрная Алиса 2" action mr.Play("bgm/maou3.ogg")
            textbutton "Чёрная Алиса 3" action mr.Play("bgm/maou4.ogg")
            textbutton "Илиас" action mr.Play("bgm/irias2.ogg")
            textbutton "Финальная битва" action mr.Play("bgm/irias3.ogg")
            textbutton "Пробуждение" action mr.Play("bgm/epilogue3.ogg")
            textbutton "Титры 2" action mr.Play("bgm/ending2.ogg")
        else:
            textbutton "Главная тема" action mr.Play("ngdata/bgm/gn_night.ogg")
            textbutton "Монстропедия" action mr.Play("ngdata/bgm/zukan.ogg")
            textbutton "Смертельная битва" action mr.Play("ngdata/bgm/granberia.ogg")
            textbutton "Спарринг" action mr.Play("ngdata/bgm/granberia2.ogg")
            textbutton "Девятихвостый предок" action mr.Play("ngdata/bgm/tamamo.ogg")
            textbutton "Суккуб ветра" action mr.Play("ngdata/bgm/alma.ogg")
            textbutton "Королева слизей" action mr.Play("ngdata/bgm/erubetie.ogg")
            textbutton "Новые враги" action mr.Play("ngdata/bgm/boss.ogg")
            textbutton "Босс 2" action mr.Play("ngdata/bgm/boss0.ogg")
            textbutton "Химера" action mr.Play("ngdata/bgm/boss2.ogg")
            textbutton "Босс 4" action mr.Play("ngdata/bgm/boss3.ogg")
            textbutton "С новой силой" action mr.Play("ngdata/bgm/boss4.ogg")
            textbutton "Сёстры Лилит" action mr.Play("ngdata/bgm/boss5.ogg")
            textbutton "Второй круг" action mr.Play("ngdata/bgm/field5.ogg")
            textbutton "Безмятежность" action mr.Play("ngdata/bgm/field7.ogg")
            textbutton "Юг" action mr.Play("ngdata/bgm/field6.ogg")
            textbutton "Запад" action mr.Play("ngdata/bgm/field8.ogg")
            textbutton "Восток" action mr.Play("ngdata/bgm/field9.ogg")
            textbutton "Север" action mr.Play("ngdata/bgm/field10.ogg")
            textbutton "Гарпия" action mr.Play("ngdata/bgm/comi2.ogg")
            textbutton "Колизей" action mr.Play("ngdata/bgm/colosseum.ogg")
            textbutton "Эпилог" action mr.Play("ngdata/bgm/ruka2.ogg")
            textbutton "Эрозия" action mr.Play("ngdata/bgm/ruka3.ogg")
            textbutton "Лука" action mr.Play("ngdata/bgm/ruka4.ogg")
            textbutton "Битва с Духами" action mr.Play("ngdata/bgm/spirits.ogg")
            textbutton "Духи Стихий" action mr.Play("ngdata/bgm/spirits2.ogg")
            textbutton "Городок у моря" action mr.Play("ngdata/bgm/porttown.ogg")
            textbutton "Подводное царство" action mr.Play("ngdata/bgm/umi3.ogg")
            textbutton "Страх" action mr.Play("ngdata/bgm/shirome.ogg")
            textbutton "Пирамида" action mr.Play("ngdata/bgm/pyramid.ogg")
            textbutton "Сфинкс" action mr.Play("ngdata/bgm/hutuu5.ogg")
            textbutton "Деревня суккубов" action mr.Play("ngdata/bgm/succubusvillage.ogg")
            textbutton "Вулкан" action mr.Play("ngdata/bgm/volcano.ogg")
            textbutton "Битва в Вулкане" action mr.Play("ngdata/bgm/volcano2.ogg")
            textbutton "Болезненные воспоминания" action mr.Play("ngdata/bgm/hutuu6.ogg")
            textbutton "Таинственность" action mr.Play("ngdata/bgm/mystery1.ogg")
            textbutton "Первородные сёстры" action mr.Play("ngdata/bgm/sisters1.ogg")
            textbutton "Алифиса Восьмая" action mr.Play("ngdata/bgm/alice8th1.ogg")
            textbutton "Илиас" action mr.Play("ngdata/bgm/irias4.ogg")
            textbutton "Финальная битва" action mr.Play("ngdata/bgm/iliasbattle.ogg")

    frame:
        style_prefix "vol"
        has vbox

        label "Громкость музыки"
        bar value Preference("music volume") style "pref_scrollbar"

    # Управление плеером
    frame:
        style_prefix 'pages'

        side "l c r":
            xfill True

            imagebutton:
                yalign YALIGN
                auto BUTTON_L
                at xyzoom(zoom_factor)

                action mr.Previous()

            textbutton BUTTON_NAME:
                align (.5, 1.0)
                text_size 22
                action ToggleScreenVariable("ng", True, False)
                style "mr_button"

            imagebutton:
                yalign YALIGN
                auto BUTTON_R
                at xyzoom(zoom_factor)

                action mr.Next()

    # Начать проигрывание музыки при открытии экрана
    on "replace" action mr.Play()

    use title("Музыкальная комната")
    use return_button("extras")

style pos_duration_frame:
    is default
    align (.5, 0.13)
    margin (30, 20)
    padding (10, 10)
    background Frame(persistent.win_bg, 5, 5)

style vol_frame:
    is default
    align (.5, 0.9)
    margin (30, 20)
    padding (10, 10)
    background Frame(persistent.win_bg, 5, 5)

style mr_frame:
    is default
    align (.5, 0.3)
    margin (30, 120)
    padding (10, 10)
    background Frame(persistent.win_bg, 5, 5)

style mr_vpgrid:
    is default
    xfill True
    spacing 5
    align (.5, .5)

style mr_button:
    is default
    xalign .5
    background None

style mr_button_text:
    is default
    font "Arciform.otf"
    outlines [(2, "#001", 0, 0)]
    size 25
    idle_color "#fff"
    selected_color "#f00"
    hover_color "#00f"
    insensitive_color "#8886"
    insensitive_outlines [(2, "#0008", 0, 0)]

style mr_button_text:
    variant "touch"
    size 28

style mr_vscrollbar:
    is pref_vscrollbar
