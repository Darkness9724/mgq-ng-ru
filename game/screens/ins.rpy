init python:
    def get_battle_item():
        s = []
        item_text = "Боевые"

        if world.party.item[0] > 0 or world.party.item[1] == 1:
            s.append(("Железный меч",
                "Легкий меч, выкованный у деревенского кузнеца.\nСпроектирован специально для невысокого роста Луки."))
        if world.party.item[0] > 4 or world.party.item[1] == 1:
            s.append(("Сияние Ангела",
                "Жуткий меч, полученный от Алисы.\nСозданный из 666 поглощённых Ангелов, он способен запечатывать монстров и превращать в пыль ангелов."))

        if world.party.item[0] > 0:
            s.append(("Памятное кольцо",
                "Памятный подарок моей матери.\nОно является дорогой памятью о моей матери, а также печатью, ограничивающую мою ангельскую силу."))

        if world.party.item[0] < 4 and world.party.item[1] != 1:
            s.append(("Тканевая рубашка",
                "Одежда, сшитая Бетти.\nСделана добротно, но для боя не подходит."))
        elif world.party.item[0] > 3 or world.party.item[1] == 1:
            s.append(("Энрикийская рубашка",
                "Защитная одежда из Энрики, зачарованная эльфийскими техниками.\nНесмотря на свою лёгкость она весьма прочная."))
        if world.party.item[2] > 0:
            s.append(("Запасная энрикийская рубашка",
                "Защитная одежда из Энрики, зачарованная эльфийскими техниками.\nНесмотря на то, что такая у меня уже есть, сменная одежда никогда не повредит."))

        return s

    def get_valuables_item():
        s = []
        item_text = "Ключевые"

        if world.party.orb[0]:
            s.append(("Красная сфера",
                "Красная Сфера, необходимая для воскрешения Святых Крыльев.\nОна воскреснет, когда будут собраны все шесть сфер."))

        if world.party.orb[1]:
            s.append(("Зелёная сфера",
                    "Зелёная Сфера, необходимая для воскрешения Святых Крыльев.\nОна воскреснет, когда будут собраны все шесть сфер."))

        if world.party.orb[2]:
            s.append(("Серебрянная сфера",
                "Серебрянная Сфера, необходимая для воскрешения Святых Крыльев.\nОна воскреснет, когда будут собраны все шесть сфер."))

        if world.party.item[0] > 5:
            s.append(("Колокольчик Посейдона",
                "Сокровище Капитана Селины.\nПока оно находится на носу корабля, нам не страшен серый шторм."))

        if world.party.item[4] == 1:
            s.append(("Письменное обещание",
                "Клятва от Меи и её мужа.\nПравда, не понимаю я этих их церемоний..."))
        elif world.party.item[4] == 2:
            s.append(("Клятвенное кольцо",
                "Парные кольца, подаренные Кракеном.\nЭто их доказательство женитьбы."))

        if world.party.orb[3]:
            s.append(("Жёлтая сфера",
                "Жёлтая Сфера, необходимая для воскрешения Святых Крыльев.\nПохоже, они воскреснут, когда будут собраны все шесть сфер."))

        if world.party.orb[4]:
            s.append(("Синяя сфера",
                "Синяя Сфера, необходимая для воскрешения Святых Крыльев.\nПохоже, они воскреснут, когда будут собраны все шесть сфер."))

        if world.party.orb[5]:
            s.append(("Фиолетовая сфера",
                "Фиолетовая Сфера, необходимая для воскрешения Святых Крыльев.\nПохоже, они воскреснут, когда будут собраны все шесть сфер."))

        if world.party.item[4] in (1, 2, 4):
            s.append(("Путеводный шар",
                "Драгоценность, полученная от Меи.\nБлагодаря ей я могу спустится в Морской храм."))

        return s

    def get_other_item():
        s = []
        item_text = "Остальные"

        if world.party.item[0] > 1:
            s.append(("Всемирный путеводитель",
            "Лучшая книга для искателей приключений и гурманов.\nОднако ей уже более 500 лет, так что сведения в ней несколько... устарели.\n... И почему я за неё таскаю её книгу?"))

        if world.party.item[3] > 0:
            s.append(("Мёд Счастья",
            "Сладкий мёд из Деревни Счастья.\nЕго можно есть как с хлебом, так и просто так, из банки.\nКак и ожидалось, Алиса заставила меня тащить и его."))

        # if world.party.item[0] > 7:
        #     s.append(("Четыре анимиста и их источники",
        #     "Книга, {b}позаимствованная{/b} в замке Сан Илия.\nЯ не смог вернуть её вовремя, поэтому мне приходится носить её с собой."))

        if world.party.item[6] == 1:
            s.append(("Жареная актиния",
            "Совершенно несъедобная еда, которую я был вынужден купить в Порту Наталия.\nОна выглядит ещё хуже, чем Жаренная Морская звезда.\nАлиса от неё в ужасе."))

        if world.party.item[0] > 8:
            s.append(("Корзина скорпионов",
            "Алиса вновь наловила целую корзину скорпионов.\n... Почему я до сих пор держу это?"))

        if world.party.item[9] == 1:
            s.append(("Цветок Алрауны",
            "Странный цветок, купленный у Алрауны-цветочницы.\nУже начал увядать...\nПохоже, из-за большого отпечатка зубов Алисы на нём..."))

        if world.party.item[11] == 1:
            s.append(("Тамамо-шар",
            "Такой пуши-и-истый..."))

        if world.party.item[13] == 1:
            s.append(("Драгоценность Победы",
            "Драгоценность, полученная от Амиры.\nРаньше она даровала силу победы, но эта сила практически покинула этот камень.\nВыглядит бесполезным."))

        if world.party.item[0] > 2:
            s.append(("Ама-ама-данго",
            "Настоящая роскошь из {b}Гостиницы Сазерленд{/b}.\nОни приготовлены с Мёдом Счастья и невероятно сладкие.\nУдивлён, что Алиса до сих пор не съела их прямо у меня из рук."))

        if world.party.item[0] > 6:
            s.append(("Жареная морская звезда",
            "Совершенно несъедобная еда, которую я был вынужден купить в Порту Наталия.\nДаже Алиса не хочет её пробовать..."))

        if world.party.item[5] == 1:
            s.append(("Святая антипризрачная вода",
            "Противопризрачная вода, купленная в Сан Илия.\nПо-моему, это просто обычная вода."))

        if world.party.item[7] == 1:
            s.append(("Жёлудь Фей",
            "Знак дружбы от милой Феи.\nКогда я держу его, я чувствую силу, текущую сквозь меня.\nОно является дорогим для меня сокровищем."))
        elif world.party.item[7] == 2:
            s.append(("Jewel of Friendship",
            "Created by combining the Fairy Acorn and Nekomata's Bell\nHolding the power of friendship, I can see the Fairies and Nekomata\nIt's an important treasure to me."))

        if world.party.item[8] == 1:
            s.append(("Genie's Lamp",
            "A lamp containing a Genie Girl.\nIt was dangerous to leave her there, so I kept it.\nI can't destroy it or dispose of it... How annoying."))
        elif world.party.item[8] == 2:
            s.append(("Lottery Ticket",
            "A lottery ticket given with gold for the lamp.\nLooks like I can use it in Gold Port."))
        elif world.party.item[8] == 3:
            s.append(("Jewel of Fortune",
            "The first prize from the lottery.\nIt used to have the power of luck, but it's mostly gone now.\nSeems worthless."))

        if world.party.item[10] == 1:
            s.append(("Nekomata's Bell",
            "A bell from a Nekomata.\nThere was some power in it at first, but not any longer.\nI had to give food to get it. Alice might get angry if she finds out."))

        if world.party.item[12] == 1 or world.party.item[1] == 1:
            s.append(("Медаль Героя",
            "Красивая медаль, которую может получить только настоящий герой.\nАбсолютно бесполезная вещица.\nВсё, что я могу - так это просто смотреть на неё и ухмыляться."))

        return s

screen menu_skit(var=None):
    tag menu

    default desc = ""
    default page = 1

    if var == "item":
        $ T = "Предметы"

        if page == 1:
            $ ITEMS = get_battle_item()
            $ item_text = "Боевые"
        elif page == 2:
            $ ITEMS = get_valuables_item()
            $ item_text = "Ключевые"
        elif page == 3:
            $ ITEMS = get_other_item()
            $ item_text = "Остальные"

        $ LEN = len(ITEMS)
        $ ROWS = len(ITEMS)

        if ROWS % 2:
            $ ROWS += 1

    else:
        $ T = "Навыки"

        if page == 1:
            $ item_text = "Проклятый Меч"
        elif page == 2:
            $ item_text = "Ангельские техники"
        elif page == 3:
            $ item_text = "Силы стихий"

    if renpy.variant('touch'):
        $ zoom_factor = 2.0
        $ YALIGN = 1.0
        $ BUTTON_L = "button/left_%s.webp"
        $ BUTTON_R = "button/right_%s.webp"
    else:
        $ zoom_factor = 1.0
        $ YALIGN = 0.5
        $ BUTTON_L = "button/left_%s.webp"
        $ BUTTON_R = "button/right_%s.webp"

    add renpy.get_screen("bg", layer='master')
    add gui.game_menu_background

    frame:
        style_prefix 'skit_but'

        if var == "item":
            grid 2 ROWS/2:
                xfill True
                spacing 15

                for nm, val in ITEMS:
                    textbutton "[nm!t]" action SetScreenVariable("desc", val) style "item_button"

                if LEN % 2:
                    null

        if var == "skill":
            grid 2 6:
                xfill True
                yfill True
                spacing 10

                if page == 1:
                    textbutton "Безмятежный Демонический Клинок (SP: 2)":
                        action SetScreenVariable("desc", "Приём Иайдо, используемый в состоянии безмятежности.\nНаносит гигантский урон, пока призвана Ундина.\nК тому же он может принудительно оборвать призыв Ундины.")
                        style "item_button"
                    textbutton "Молниеносный Удар Клинка (SP: 2)":
                        action SetScreenVariable("desc", "Искусство, использующее движение ветра как усиление для нанесения удара молниеносной скорости.\nНаносит дополнительный урон, если воспользоваться этим приёмом на первом ходу.\nСтановится ещё сильнее, если при этом вызвана Сильфа.")
                        style "item_button"
                    textbutton "Сотрясающее Землю Обезглавливание (SP: 3)":
                        action SetScreenVariable("desc", "Приём, использующий силу земли для нанесения гибельного удара сверху.\nБезумно мощный.\nК тому же приём становится ещё мощнее, если при его использовании вызвана Гнома.")
                        style "item_button"
                    textbutton "Неудержимый Испепеляющий Клинок (SP: 4)":
                        action SetScreenVariable("desc", "Бесчисленное количество ударов, ещё и вдобавок усиленных очищающим пламенем.\nНевероятно могущественный приём, созданный Гранберией.\nЕго сила увеличивается ещё больше, если во время его выполнения вызвана Саламандра.")
                        style "item_button"

                    null
                    null

                    textbutton "Крайность (SP: 0)":
                        action SetScreenVariable("desc", "Рискованный и отчаянный приём, резко снижающий количество очков здоровья Луки.\nЛегчайшего касания может оказаться достаточно, чтобы вывести Луку из себя, когда он на волоске от поражения.") style "item_button"
                    if world.party.member[0].skill[0] > 25:
                        textbutton "Разгон (SP: 0)":
                            action SetScreenVariable("desc", "Демоническая техника, заключающаяся в концентрации своей темной энергии и извлечении из неё духовной энергии.{w}\nПри первом использовании даёт 2 SP. Прирост возрастает на единицу при каждом последовательном использовании.")
                            style "item_button"
                    else:
                        null

                    null
                    null

                    if world.party.member[0].skill[1] and world.party.member[0].skill[2] and world.party.member[0].skill[3] and world.party.member[0].skill[4]:
                        textbutton "Элементальная Спика (SP: 10)":
                            action SetScreenVariable("desc", "Концентрация мощи всех четырёх духов в правой руке.\nПриём опустошительной мощи, изученный с помощью Генриха.")
                            style "item_button"
                        textbutton "Четырёхкратный Гига-удар (SP: 1)":
                            action SetScreenVariable("desc", "Ультимативный приём абсолютной, опустошающей мощи, использующий всех четырёх духов для уничтожения противника.\nЕсли во время четырёх ходов зарядки противник наносит удар, то приём проваливается.\nБолее того, во время начала исполнения приёма все вызванные духи исчезают.")
                            style "item_button"
                    else:
                        null
                        null

                elif page == 2:
                    textbutton "Мгновенное Убийство (SP: 2)":
                        action SetScreenVariable("desc", "Тайный приём падшего ангела, прорубающий насквозь само время и пространство.\nОчень большой урон за сравнительно малое количество очков навыков на его использование.\nПри определённых условиях может бить дважды.\nПохоже, что он может сочетаться с силой ветра...")
                        style "item_button"
                    textbutton "Возрождение Небесного Демона (SP: 4)":
                        action SetScreenVariable("desc", "Таинственный приём, являющий собой концентрацию магической энергии для уничтожения противников.\nНикому из тех, кто его видел, не посчастливилось дожить до момента, чтобы его описать.\nПохоже, что этот приём может сочетаться с силой земли...") style "item_button"
                    textbutton "Девятиликий Ракшаса (SP: 6)":
                        action SetScreenVariable("desc", "Целый шквал атак мечом, бушующий в танце смерти.\nЛегендарный приём, которым могут пользоваться лишь сильнейшие из ангелов.\nИ похоже, что он может сочетаться с силой огня...") style "item_button"
                    textbutton "Денница (SP: 8)":
                        action SetScreenVariable("desc", "Приём абсолютной мощи, созданный первородным падшим ангелом - Люцифиной.\nСлужит контратакой в ответ на удар противника, аннигилируя его.\nПохоже, что он, к тому же, может сочетаться с силой воды...") style "item_button"

                    null
                    null

                    textbutton "Медитация (SP: 3)":
                        action SetScreenVariable("desc", "Лука концентрирует силу своего духа, восстанавливая очки здоровья без использования магии.\nВосстанавливает половину максимального количества очков здоровья Луки.{w}\nЛегендарный навык, используемый падшими ангелами.") style "item_button"
                    if world.party.member[0].skill[1]:
                        textbutton "Танец Падшего Ангела: Буря (SP: 2)":
                            action SetScreenVariable("desc", "Наделение себя могуществом Сильфы для максимального усиления Танца Падшего Ангела.\nСпособность уклоняться от большинства атак и атаковать дважды.\nНаибольший эффект очевиден при встрече с ангелами.") style "item_button"
                    else:
                        textbutton "Танец Падшего Ангела (SP: 2)":
                            action SetScreenVariable("desc", "Тайный приём, заключающийся в единении со святым течением и уклонении от атак противника.\nПохоже, что он довольно эффективен в битвах с ангелами и против статус-атак.") style "item_button"

                    null
                    null
                    null
                    null

                elif page == 3:
                    if world.party.member[0].skill[1]:
                        textbutton "Сильфа (ур. 3) (SP: 1)":
                            action SetScreenVariable("desc", "Наделение себя могуществом опустошающего урагана для увеличения скорости движения.\nСпособность уклоняться от большинства атак и атаковать дважды.") style "item_button"
                    else:
                        null
                    if world.party.member[0].skill[2]:
                        textbutton "Гнома (ур. 3) (SP: 1)":
                            action SetScreenVariable("desc", "Способность наполнить всё своё собственное тело могуществом Земли.\nПочти полная неуязвимость к физическому урону и ослабление урона от секс-приёмов.\nВсе атаки наносят критический урон, а вырваться из сковываний становится ещё легче.") style "item_button"
                    else:
                        null

                    if world.party.member[0].skill[3]:
                        textbutton "Ундина (ур. 3) (SP: 2)":
                            action SetScreenVariable("desc", "Доверие себя течению и достижение состояния безмятежности.\nУсиливает атаку и шанс на уклонение от атак противника.\nНаибольший эффект проявляется при использовании против монстров.") style "item_button"
                    else:
                        textbutton "Безмятежный разум (SP: 2)":
                            action SetScreenVariable("desc", "Состояние Безмятежности, достижимое даже без Ундины.\nПовышается сила атаки и шанс на уклонение.\nПохоже, что оно довольно-таки эффективно против монстров и кровожадных противников.") style "item_button"

                    if world.party.member[0].skill[4]:
                        textbutton "Саламандра (ур. 3) (SP: 4)":
                            action SetScreenVariable("desc", "Наделение себя пламенем искупительного очищения.\nМгновенное восполнение очков навыков до максимума и значительное увеличение мощности атаки.\nСпособность при этом пользоваться Медитацией, и, когда спадает эффект, количество очков навыков не опускается до нуля.") style "item_button"
                    else:
                        null

                    null
                    null

                    if world.party.member[0].undine_skill[0]:
                        textbutton "Ундина (ур. 2) (SP: ВСЕ)":
                            action SetScreenVariable("desc", "Доверие себя течению и достижение состояния абсолютной безмятежности.\nУсиление атаки и уклонение почти от всех атак противника.\nКаждый ход отнимает одно очко навыков до тех пор, пока их количество не достигнет нуля.") style "item_button"
                    else:
                        null

                    if world.party.member[0].undine_skill[1]:
                        textbutton "Очищение (SP: 2)":
                            action SetScreenVariable("desc", "Техника очищения организма, выученная от Ундины и работающая только при её призыве.\nВосстанавливает немного здоровья и выводит токсины из тела.") style "item_button"
                    else:
                        null

                    if world.party.member[0].skill[1] and world.party.member[0].skill[2] and world.party.member[0].skill[3] and world.party.member[0].skill[4]:
                        textbutton "Призыв четырёх духов (SP: 6)":
                            action SetScreenVariable("desc", "Требует много очков навыков на исполнение, но способен призывать сразу всех духов одновременно.\nСтоит меньше очков, чем поочерёдный призыв духов, однако от призыва Саламандры SP не восстанавливается.") style "item_button"
                    else:
                        null

                    null
                    null
                    null

    label '[desc]' style 'skit_desc'

    if var == "item":
        frame:
            style_prefix 'pages'
            xsize 300

            side "l c r":
                xfill True

                imagebutton:
                    auto BUTTON_L
                    at xyzoom(zoom_factor)
                    yalign YALIGN

                    action [If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', 3)), SetScreenVariable("desc", "")]

                label "[item_text!t]"

                imagebutton:
                    auto BUTTON_R
                    at xyzoom(zoom_factor)
                    yalign YALIGN

                    action [If(page < 3, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1)), SetScreenVariable("desc", "")]

        key "K_PAGEDOWN" action [If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', 3)), SetScreenVariable("desc", "")]
        key "K_PAGEUP" action [If(page < 3, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1)), SetScreenVariable("desc", "")]

    if var == "skill":
        frame:
            style_prefix 'pages'
            xsize 300

            side "l c r":
                xfill True

                imagebutton:
                    auto BUTTON_L
                    at xyzoom(zoom_factor)
                    yalign YALIGN

                    action [If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', 3)), SetScreenVariable("desc", "")]

                label "[item_text!t]"

                imagebutton:
                    auto BUTTON_R
                    at xyzoom(zoom_factor)
                    yalign YALIGN

                    action [If(page < 3, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1)), SetScreenVariable("desc", "")]

        key "K_PAGEDOWN" action [If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', 3)), SetScreenVariable("desc", "")]
        key "K_PAGEUP" action [If(page < 3, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1)), SetScreenVariable("desc", "")]

    use title(T)
    use return_button("game_menu")

style skit_but_frame:
    background Frame(persistent.win_bg, 5, 5)
    xfill True
    yfill True
    margin (5, 60, 5, 195)
    padding (5, 10)

style skit_desc:
    is label
    background Frame(persistent.win_bg, 5, 5)
    xfill True
    yfill True
    margin (5, 405, 5, 60)
    padding (5, 10)

style skit_desc_text:
    is label_text
    size 19
    outlines [(2, '#000c')]
    font "Arciform.otf"
    color "#fffacd"
    line_spacing 4

style inv_but_frame:
    background Frame(persistent.win_bg, 5, 5)
    xfill True
    yfill True
    margin (20, 60, 295, 60)
    padding (5, 10)

style inv_desc:
    is label
    background Frame(persistent.win_bg, 5, 5)
    xfill True
    yfill True
    margin (515, 60, 20, 60)
    padding (5, 10)

style inv_desc_text:
    is label_text
    size 19
    outlines [(2, '#000c')]
    font "Arciform.otf"
    color "#fffacd"
    line_spacing 4

style item_button:
    is default
    background None

style item_button_text:
    is gm_button_text
    size 18
    kerning 0

style skit_but_vscrollbar:
    is pref_vscrollbar
