################################################################################
## Инициализация
################################################################################
init offset = -2

################################################################################
## Стили
################################################################################
style default:
    properties gui.text_properties()
    language gui.language

style input:
    properties gui.text_properties("input", accent=True)
    adjust_spacing False

style hyperlink_text:
    properties gui.text_properties("hyperlink", accent=True)
    hover_underline True
    color gui.hyperlink_color

style gui_text:
    properties gui.text_properties("interface")


style button:
    properties gui.button_properties("button")

style button_text is gui_text:
    properties gui.text_properties("button")
    yalign 0.5


style label_text is gui_text:
    properties gui.text_properties("label", accent=True)

style prompt_text is gui_text:
    properties gui.text_properties("prompt")


style bar:
    ysize gui.bar_size
    left_bar Frame("bar/left.webp", gui.bar_borders, tile=gui.bar_tile)
    right_bar Frame("bar/right.webp", gui.bar_borders, tile=gui.bar_tile)

style vbar:
    xsize gui.bar_size
    top_bar Frame("bar/top.webp", gui.vbar_borders, tile=gui.bar_tile)
    bottom_bar Frame("bar/bottom.webp", gui.vbar_borders, tile=gui.bar_tile)

style scrollbar:
    ysize gui.scrollbar_size
    base_bar Frame("scrollbar/horizontal_[prefix_]bar.webp", gui.scrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("scrollbar/horizontal_[prefix_]thumb.webp", gui.scrollbar_borders, tile=gui.scrollbar_tile)

style vscrollbar:
    xsize gui.scrollbar_size
    base_bar Frame("scrollbar/vertical_[prefix_]bar.webp", gui.vscrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("scrollbar/vertical_[prefix_]thumb.webp", gui.vscrollbar_borders, tile=gui.scrollbar_tile)

style slider:
    ysize gui.slider_size
    base_bar Frame("slider/horizontal_[prefix_]bar.webp", gui.slider_borders, tile=gui.slider_tile)
    thumb "slider/horizontal_[prefix_]thumb.webp"

style vslider:
    xsize gui.slider_size
    base_bar Frame("slider/vertical_[prefix_]bar.webp", gui.vslider_borders, tile=gui.slider_tile)
    thumb "slider/vertical_[prefix_]thumb.webp"


style frame:
    padding gui.frame_borders.padding
    background Frame(persistent.win_bg, gui.frame_borders, tile=gui.frame_tile)



################################################################################
## Внутриигровые экраны
################################################################################


## Экран разговора #############################################################
##
## Экран разговора используется для показа диалога игроку. Он использует два
## параметра — who и what — что, соответственно, имя говорящего персонажа и
## показываемый текст. (Параметр who может быть None, если имя не задано.)
##
## Этот экран должен создать текст с id "what", чтобы Ren'Py могла показать
## текст. Здесь также можно создать наложения с id "who" и id "window", чтобы
## применить к ним настройки стиля.
##
## https://www.renpy.org/doc/html/screen_special.html#say
screen say(who, what):
    # Приподнимает диалоговое окно в битве
    if renpy.get_screen("hp"):
        $ SAYW_YOFFSET = -75
        $ SAYW_YSIZE = 110
    else:
        $ SAYW_YOFFSET = -3
        $ SAYW_YSIZE = 160

    $ LM = 5
    $ RM = 5

    if world.battle is not None:
        if world.actor[0].wind or world.actor[0].aqua or world.actor[0].fire or world.actor[0].earth or world.actor[0].holy:
            $ LM = 25
            use elm

        if world.battle.enemy[0].wind or world.battle.enemy[0].aqua or world.battle.enemy[0].fire or world.battle.enemy[0].earth or world.battle.enemy[0].holy:
            $ RM = 25
            use enemy_elm

    vbox:
        yalign 1.0

        hbox:
            yoffset SAYW_YOFFSET
            xfill True
            spacing 0

            if who is not None:
                window:
                    id "namebox"
                    style "namebox"
                    align (0, 1.0)

                    text who id "who" outlines [(2, "#000", 0, 0), (2, "#0003", 2, 2)]
            else:
                text ""

            use quick_menu

        window:
            id "window"
            background Frame(persistent.textbox,
                            Borders(30, 30, 30, 30)
                        )

            yoffset SAYW_YOFFSET
            ysize SAYW_YSIZE
            left_margin LM
            right_margin RM
            top_margin 0
            xfill True
            padding (10, 10)

            has vbox:
                style "say_vbox"

            text what id "what" outlines [(2, "#000", 0, 0), (2, "#0003", 2, 2)]

    ## Если есть боковое изображение ("голова"), показывает её поверх текста.
    ## По стандарту не показывается на варианте для мобильных устройств — мало
    ## места.
    if not renpy.variant("small"):
        add SideImage() xalign 0.0 yalign 1.0

    if not store._rollback:
        key "rollback" action ShowMenu('history')


## Делает namebox доступным для стилизации через объект Character.
init python:
    config.character_id_prefixes.append('namebox')

style window is default
style say_label is default
style say_dialogue is default
style say_thought is say_dialogue

style namebox is default
style namebox_label is say_label


style window:
    xalign 0.5
    xfill True
    yalign gui.textbox_yalign
    ysize gui.textbox_height

    background Image(persistent.textbox, xalign=0.5, yalign=1.0)

style namebox:
    xpos gui.name_xpos
    xanchor gui.name_xalign
    xsize gui.namebox_width
    ypos gui.name_ypos
    ysize gui.namebox_height

    background Frame(persistent.namebox, gui.namebox_borders, tile=gui.namebox_tile, xalign=gui.name_xalign)
    padding gui.namebox_borders.padding
    xminimum 200
    margin (5, 5)

style say_label:
    properties gui.text_properties("name", accent=True)
    xalign gui.name_xalign
    yalign 0.5

# style say_dialogue:
#     properties gui.text_properties("dialogue")

#     xpos gui.dialogue_xpos
#     xsize gui.dialogue_width
#     ypos gui.dialogue_ypos


## Экран ввода #################################################################
##
## Этот экран используется, чтобы показывать renpy.input. Это параметр запроса,
## используемый для того, чтобы дать игроку ввести в него текст.
##
## Этот экран должен создать наложение ввода с id "input", чтобы принять
## различные вводимые параметры.
##
## https://www.renpy.org/doc/html/screen_special.html#input

screen input(prompt):
    style_prefix "input"

    window:

        vbox:
            xalign gui.dialogue_text_xalign
            xpos gui.dialogue_xpos
            xsize gui.dialogue_width
            ypos gui.dialogue_ypos

            text prompt style "input_prompt"
            input id "input"

style input_prompt is default

style input_prompt:
    xalign gui.dialogue_text_xalign
    properties gui.text_properties("input_prompt")

style input:
    xalign gui.dialogue_text_xalign
    xmaximum gui.dialogue_width


## Экран выбора ################################################################
##
## Этот экран используется, чтобы показывать внутриигровые выборы,
## представленные оператором menu. Один параметр, вложения, список объектов,
## каждый с заголовком и полями действия.
##
## https://www.renpy.org/doc/html/screen_special.html#choice

screen choice(items, title=None):
    style_prefix "choice"

    vbox:
        if title:
            text title id "menu_title"
        for i in items:
            textbutton i.caption action i.action


## Когда этот параметр True, заголовки меню будут проговариваться рассказчиком.
## Когда False, заголовки меню будут показаны как пустые кнопки.
define config.narrator_menu = True


style choice_vbox is vbox
style choice_button is button
style choice_button_text is button_text

style choice_vbox:
    xalign 0.5
    ypos 270
    yanchor 0.5

    spacing gui.choice_spacing

style choice_text is default:
    properties gui.text_properties("name")
    outlines [(2, "#000e")]
    xalign 0.5
    yalign 0.1

style choice_button is default:
    properties gui.button_properties("choice_button")

style choice_button_text is default:
    properties gui.button_text_properties("choice_button")


## Экран быстрого меню #########################################################
##
## Быстрое меню показывается внутри игры, чтобы обеспечить лёгкий доступ к
## внеигровым меню.

screen quick_menu():
    ## Гарантирует, что оно появляется поверх других экранов.
    zorder 100

    hbox:
        style_prefix "quick"

        align(1.0, 1.0)

        frame:
            if store._rollback:
                textbutton "Назад" action Rollback()
            else:
                textbutton "История" action ShowMenu('history')
        frame:
            textbutton "Пропуск" action Skip() alternate Skip(fast=True, confirm=True)
        frame:
            textbutton "Авто" action Preference("auto-forward", "toggle")
        frame:
            textbutton "Сохранить" action If(store._save, ShowMenu("save"))
        frame:
            textbutton "Б.Сохр" action If(store._save, QuickSave())
        frame:
            textbutton "Б.Загр" action QuickLoad()
        frame:
            textbutton "Меню" action ShowMenu()

screen quick_menu():
    variant "touch"

    zorder 100

    hbox:
        style_prefix "quick"

        align(1.0, 1.0)

        if store._rollback:
            textbutton "Назад" action Rollback()
        else:
            textbutton "История" action ShowMenu('history')
        textbutton "Пропуск" action Skip() alternate Skip(fast=True, confirm=True)
        textbutton "Авто" action Preference("auto-forward", "toggle")
        textbutton "Сохранить" action If(store._save, ShowMenu("save"))
        textbutton "Меню" action ShowMenu()

style quick_button is default
style quick_button_text is button_text

style quick_button:
    properties gui.button_properties("quick_button")

style quick_button_text:
    properties gui.button_text_properties("quick_button")
    outlines [(2, "#000e")]


style quick_frame:
    background Frame("boxes/n_box.webp", 15, 15, tile=True)

## Экран главного меню #########################################################
##
## Используется, чтобы показать главное меню после запуска игры.
##
## https://www.renpy.org/doc/html/screen_special.html#main-menu

screen main_menu():

    ## Этот тег гарантирует, что любой другой экран с тем же тегом будет
    ## заменять этот.
    tag menu

    style_prefix "main_menu"

    add gui.main_menu_background
    add "logo" xalign .5 yalign .35

    # Для праздничных сборок
    if date.today().month == 12 and date.today().day in range(25, 31) or date.today().month == 1:
        add "snow"

    frame:
        style_prefix "mm"

        use choice_box:
            textbutton "Новая игра +" at appear(1.0) action Start()
            textbutton "Продолжить" at appear(1.0) action ShowMenu("load")
            textbutton "Настройки" at appear(1.0) action ShowMenu("preferences")
            textbutton "Экстра" at appear(1.0) action [Play("music", "ngdata/bgm/zukan.ogg"), ShowMenu("extras")]
            if renpy.variant("pc"):
                textbutton "Выйти" at appear(1.0) action Quit(confirm=False)

    if gui.show_name:
        vbox:
            text "[config.version]":
                style "main_menu_version"

screen choice_box():
    if renpy.variant("touch"):
        hbox:
            transclude
    else:
        vbox:
            transclude

# Make all the main menu buttons be the same size.
style mm_frame:
    background None
    align (.5, .9)

style mm_frame:
    variant "touch"
    align (.5, .75)

style mm_button:
    size_group "mm"
    align (.5, .5)
    idle_background Solid("#000")
    hover_background Solid('#888')

style mm_button:
    variant "touch"
    xysize (140, 100)

style mm_vbox:
    spacing 5

style mm_hbox:
    variant "touch"
    spacing 20

style mm_button_text:
    font "Arciform.otf"
    xalign 0.5
    size 20
    idle_color "#e6e6fa"
    hover_color "#330000"
    idle_outlines [(1, "#330000")]
    hover_outlines [(1, "#888")]

style mm_button_text:
    variant "touch"
    align (0.5, 0.5)

style main_menu_frame is empty
style main_menu_vbox is vbox
style main_menu_text is gui_text
style main_menu_title is main_menu_text
style main_menu_version is main_menu_text

style main_menu_frame:
    xsize 280
    yfill True

    background "overlay/main_menu.webp"

style main_menu_vbox:
    xalign 1.0
    xoffset -20
    xmaximum 800
    yalign 1.0
    yoffset -20

style main_menu_text:
    properties gui.text_properties("main_menu", accent=True)

style main_menu_title:
    properties gui.text_properties("title")

style main_menu_version:
    insensitive_outlines [(2, "#303030cc", 0, 0)]
    properties gui.text_properties("version")

style game_menu_outer_frame:
    bottom_padding 30
    top_padding 120

    background "overlay/game_menu.webp"

style game_menu_navigation_frame:
    xsize 280
    yfill True

style game_menu_content_frame:
    left_margin 40
    right_margin 20
    top_margin 10

style game_menu_viewport:
    xsize 920

style game_menu_vscrollbar:
    unscrollable gui.unscrollable

style game_menu_side:
    spacing 10

style game_menu_label:
    xpos 50
    ysize 120

style game_menu_label_text:
    size gui.title_text_size
    color gui.accent_color
    yalign 0.5

style return_button:
    xpos gui.navigation_xpos
    yalign 1.0
    yoffset -30

style page_label is gui_label
style page_label_text is gui_label_text
style page_button is gui_button
style page_button_text is gui_button_text

style slot_button is gui_button
style slot_button_text is gui_button_text
style slot_time_text is slot_button_text
style slot_name_text is slot_button_text

style page_label:
    xpadding 50
    ypadding 3

style page_label_text:
    text_align 0.5
    layout "subtitle"
    hover_color gui.hover_color

style page_button:
    properties gui.button_properties("page_button")

style page_button_text:
    properties gui.button_text_properties("page_button")

style slot_button:
    properties gui.button_properties("slot_button")

style slot_button_text:
    properties gui.button_text_properties("slot_button")

style pref_label is gui_label
style pref_label_text is gui_label_text
style pref_vbox is vbox

style radio_label is pref_label
style radio_label_text is pref_label_text
style radio_button is gui_button
style radio_button_text is gui_button_text
style radio_vbox is pref_vbox

style check_label is pref_label
style check_label_text is pref_label_text
style check_button is gui_button
style check_button_text is gui_button_text
style check_vbox is pref_vbox

style slider_label is pref_label
style slider_label_text is pref_label_text
style slider_slider is gui_slider
style slider_button is gui_button
style slider_button_text is gui_button_text
style slider_pref_vbox is pref_vbox

style mute_all_button is check_button
style mute_all_button_text is check_button_text

style pref_label:
    top_margin gui.pref_spacing
    bottom_margin 2

style pref_label_text:
    yalign 1.0

style pref_vbox:
    xsize 225

style radio_vbox:
    spacing gui.pref_button_spacing

style radio_button:
    properties gui.button_properties("radio_button")
    foreground "button/radio_[prefix_]foreground.webp"

style radio_button_text:
    properties gui.button_text_properties("radio_button")

style check_vbox:
    spacing gui.pref_button_spacing

style check_button:
    properties gui.button_properties("check_button")
    foreground "button/check_[prefix_]foreground.webp"

style check_button_text:
    properties gui.button_text_properties("check_button")

style slider_slider:
    xsize 350

style slider_button:
    properties gui.button_properties("slider_button")
    yalign 0.5
    left_margin 10

style slider_button_text:
    properties gui.button_text_properties("slider_button")

style slider_vbox:
    xsize 450

################################################################################
## Дополнительные экраны
################################################################################


## Экран подтверждения #########################################################
##
## Экран подтверждения вызывается, когда Ren'Py хочет спросить у игрока вопрос
## Да или Нет.
##
## https://www.renpy.org/doc/html/screen_special.html#confirm
screen confirm(message, yes_action, no_action):
    modal True

    style_prefix "yesno"

    add renpy.get_screen("bg", layer='master')

    if message == layout.QUIT:
        $ message = "Вы уже хотите покинуть девушек-монстров?"

        if world.party.check(2):
            add "granberia st81" align(.5, .5) xzoom(-1)
        elif world.party.check(3):
            add "tamamo st21" align(.5, .5)
        else:
            add "alice st94" align(.5, .5)

    label message:
        xalign 0.5

    hbox:
        align (.5, .8)
        spacing 100

        textbutton "Да" action yes_action
        textbutton "Нет" action no_action

    # Правый клик и ESC равны "нет".
    key "game_menu" action no_action

style yesno_label:
    background Frame(persistent.win_bg, 5, 5)
    xmargin 30
    xfill True
    yminimum 100
    align (.5, .1)
    padding (10, 10)

style yesno_label_text:
    text_align 0.5
    layout "subtitle"
    font "Arciform.otf"
    align (.5, .5)
    size 22

style yesno_button:
    background Frame(persistent.win_bg, 5, 5)
    xysize (150, 90)

style yesno_button_text:
    font "Arciform.otf"
    align (.5, .5)
    size 22
    color "#fff"

## Экран индикатора пропуска ###################################################
##
## Экран индикатора пропуска появляется для того, чтобы показать, что идёт
## пропуск.
##
## https://www.renpy.org/doc/html/screen_special.html#skip-indicator

screen skip_indicator():

    zorder 100
    style_prefix "skip"

    frame:

        hbox:
            spacing 6

            text "Пропускаю"

            text "▸" at delayed_blink(0.0, 1.0) style "skip_triangle"
            text "▸" at delayed_blink(0.2, 1.0) style "skip_triangle"
            text "▸" at delayed_blink(0.4, 1.0) style "skip_triangle"


## Эта трансформация используется, чтобы мигать стрелками одна за другой.
transform delayed_blink(delay, cycle):
    alpha .5

    pause delay

    block:
        linear .2 alpha 1.0
        pause .2
        linear .2 alpha 0.5
        pause (cycle - .4)
        repeat


style skip_frame is empty
style skip_text is gui_text
style skip_triangle is skip_text

style skip_frame:
    ypos gui.skip_ypos
    background Frame("skip.webp", gui.skip_frame_borders, tile=gui.frame_tile)
    padding gui.skip_frame_borders.padding

style skip_text:
    size gui.notify_text_size

style skip_triangle:
    ## Нам надо использовать шрифт, имеющий в себе символ U+25B8 (стрелку выше).
    font "DejaVuSans.ttf"


## Экран уведомлений ###########################################################
##
## Экран уведомлений используется, чтобы показать игроку оповещение. (Например,
## когда игра автосохранилась, или был сделан скриншот)
##
## https://www.renpy.org/doc/html/screen_special.html#notify-screen

screen notify(message):

    zorder 100
    style_prefix "notify"

    frame at notify_appear:
        text "[message!tq]"

    timer 3.25 action Hide('notify')


transform notify_appear:
    on show:
        alpha 0
        linear .25 alpha 1.0
    on hide:
        linear .5 alpha 0.0


style notify_frame is empty
style notify_text is gui_text

style notify_frame:
    ypos gui.notify_ypos

    background Frame("notify.webp", gui.notify_frame_borders, tile=gui.frame_tile)
    padding gui.notify_frame_borders.padding

style notify_text:
    properties gui.text_properties("notify")


################################################################################
## Мобильные варианты
################################################################################

style pref_vbox:
    variant "medium"
    xsize 450

## Раз мышь может не использоваться, мы заменили быстрое меню версией,
## использующей меньше кнопок, но больших по размеру, чтобы их было легче
## касаться.


style window:
    variant "small"
    background "phone/textbox.webp"

style radio_button:
    variant "small"
    foreground "phone/button/radio_[prefix_]foreground.webp"

style check_button:
    variant "small"
    foreground "phone/button/check_[prefix_]foreground.webp"

style main_menu_frame:
    variant "small"
    background "phone/overlay/main_menu.webp"

style game_menu_outer_frame:
    variant "small"
    background "phone/overlay/game_menu.webp"

style game_menu_navigation_frame:
    variant "small"
    xsize 340

style game_menu_content_frame:
    variant "small"
    top_margin 0

style pref_vbox:
    variant "small"
    xsize 400

style bar:
    variant "small"
    ysize gui.bar_size
    left_bar Frame("phone/bar/left.webp", gui.bar_borders, tile=gui.bar_tile)
    right_bar Frame("phone/bar/right.webp", gui.bar_borders, tile=gui.bar_tile)

style vbar:
    variant "small"
    xsize gui.bar_size
    top_bar Frame("phone/bar/top.webp", gui.vbar_borders, tile=gui.bar_tile)
    bottom_bar Frame("phone/bar/bottom.webp", gui.vbar_borders, tile=gui.bar_tile)

style scrollbar:
    variant "small"
    ysize gui.scrollbar_size
    base_bar Frame("phone/scrollbar/horizontal_[prefix_]bar.webp", gui.scrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("phone/scrollbar/horizontal_[prefix_]thumb.webp", gui.scrollbar_borders, tile=gui.scrollbar_tile)

style vscrollbar:
    variant "small"
    xsize gui.scrollbar_size
    base_bar Frame("phone/scrollbar/vertical_[prefix_]bar.webp", gui.vscrollbar_borders, tile=gui.scrollbar_tile)
    thumb Frame("phone/scrollbar/vertical_[prefix_]thumb.webp", gui.vscrollbar_borders, tile=gui.scrollbar_tile)

style slider:
    variant "small"
    ysize gui.slider_size
    base_bar Frame("phone/slider/horizontal_[prefix_]bar.webp", gui.slider_borders, tile=gui.slider_tile)
    thumb "phone/slider/horizontal_[prefix_]thumb.webp"

style vslider:
    variant "small"
    xsize gui.slider_size
    base_bar Frame("phone/slider/vertical_[prefix_]bar.webp", gui.vslider_borders, tile=gui.slider_tile)
    thumb "phone/slider/vertical_[prefix_]thumb.webp"

style slider_pref_vbox:
    variant "small"
    xsize None

style slider_pref_slider:
    variant "small"
    xsize 600

##############################################################################
#
# Title Menu
#

screen title(title, act=None):
    if act:
        textbutton title action act style "title"
    else:
        label title style "title"

style title:
    is label
    background "#330000"
    padding (0, 0, -10, 0)
    margin (0, 5)
    anchor (0, 0)
    pos (20, 10)

style title_text:
    size 40
    align (.5, .5)
    color "#fff000"
    font "Skandal.ttf"
    line_leading -10
    line_spacing -10
    outlines [(1, "#330000"), (2, "#cc0000")]
    bold True

screen pages(page, max_page, enter=None):
    if renpy.variant('touch'):
        $ zoom_factor = 2.0
        $ YALIGN = 1.0
        $ BUTTON_L = "button/left_%s.webp"
        $ BUTTON_R = "button/right_%s.webp"
    else:
        $ zoom_factor = 1.0
        $ YALIGN = 0.5
        $ BUTTON_L = "button/left_%s.webp"
        $ BUTTON_R = "button/right_%s.webp"

    if enter == "saveload":
        frame:
            style_prefix 'pages'

            side "l c r":
                xfill True

                imagebutton:
                    yalign YALIGN
                    auto BUTTON_L
                    at xyzoom(zoom_factor)

                    action [FilePagePrevious(max=max_page, wrap=True, auto=False, quick=False), If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', max_page))]

                label "[page]/[max_page]"

                imagebutton:
                    yalign YALIGN
                    auto BUTTON_R
                    at xyzoom(zoom_factor)

                    action [FilePageNext(max=max_page, wrap=True, auto=False, quick=False), If(page < max_page, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1))]

        key "K_PAGEDOWN" action If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', max_page))
        key "K_PAGEUP" action If(page < max_page, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1))
    else:
        frame:
            style_prefix 'pages'

            side "l c r":
                xfill True

                imagebutton:
                    yalign YALIGN
                    auto BUTTON_L
                    at xyzoom(zoom_factor)
                    if enter == "pedia":
                        activate_sound "se/m_papier02.opus"

                    action If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', max_page))

                label "[page]/[max_page]"

                imagebutton:
                    yalign YALIGN
                    auto BUTTON_R
                    at xyzoom(zoom_factor)
                    if enter == "pedia":
                        activate_sound "se/m_papier02.opus"

                    action If(page < max_page, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1))

        key "K_PAGEDOWN" action If(page > 1, true=SetScreenVariable('page', page-1), false=SetScreenVariable('page', max_page))
        key "K_PAGEUP" action If(page < max_page, true=SetScreenVariable('page', page+1), false=SetScreenVariable('page', 1))


style pages_frame:
    background Solid("#330000")
    xsize 200
    margin (3, 0)
    align (0.5, 0.98)

style pages_frame:
    variant "touch"
    xsize 250

style pages_label:
    background Solid("#330000")
    xfill True
    align (.5, 1.0)

style pages_label_text:
    align (.5, .5)
    color "#fffa"
    outlines [(2, "#000e")]
    size 24
    font "Arciform.otf"

##############################################################################
# Return Button
##############################################################################
screen return_button(enter=None):
    textbutton "Назад":
        align (.97, .98)
        style "ret_button"

        if enter:
            action ShowMenu(enter)
        else:
            action Return()

    if enter:
        key "game_menu" action ShowMenu(enter)
    else:
        key "game_menu" action Return()


screen return_button(enter=None):
    variant "touch"

    imagebutton:
        align (1.0, 1.0)
        at xyzoom(2.0)
        auto "button/right_%s.webp"
        margin (5, 3)

        if enter:
            action ShowMenu(enter)
        else:
            action Return()

    if enter:
        key "game_menu" action ShowMenu(enter)
    else:
        key "game_menu" action Return()

style ret_button:
    is default
    background "#330000"
    padding (1, 0)

style ret_button_text:
    is default
    size 34
    line_spacing -6
    line_leading -6
    font "Arciform.otf"
    idle_color "#fff000cc"
    hover_color "#000fffcc"

##############################################################################
# Save, Load
#
# Screens that allow the user to save and load the game.
screen save(enter=None):
    if is_integer(FilePageName()):
        default page = int(FilePageName())
    else:
        default page = 1

    tag menu

    use file_slots(page)
    use title("Сохранить")
    if FilePageName() != 'q':
        use pages(page, 50, "saveload")
    hbox:
        align (0.05, .97)
        spacing 10

        if config.has_quicksave:
            textbutton _("{#quick_page}Быстр"):
                style "save_load_folder_button"
                action FilePage("quick")

            textbutton _("{#quick_page}Обыч"):
                style "save_load_folder_button"
                action FilePage(page)

    use return_button(enter)

screen load(enter=None):
    if is_integer(FilePageName()):
        default page = int(FilePageName())
    else:
        default page = 1

    tag menu

    use file_slots(page, enter)
    use title("Загрузить")
    if FilePageName() != 'a' and FilePageName() != 'q':
        use pages(page, 50, "saveload")
    hbox:
        align (0.05, .97)
        spacing 10

        if config.has_autosave:
            textbutton _("{#auto_page}Авто"):
                style "save_load_folder_button"
                action FilePage("auto")

        if config.has_quicksave:
            textbutton _("{#quick_page}Быстр"):
                style "save_load_folder_button"
                action FilePage("quick")

        if config.has_autosave or config.has_quicksave:
            textbutton _("{#quick_page}Обыч"):
                style "save_load_folder_button"
                action FilePage(page)

    use return_button(enter)

style save_load_folder_button:
    is ret_button

style save_load_folder_button_text:
    is default
    size 24
    line_spacing -6
    line_leading -6
    font "Skandal.ttf"
    idle_color "#fff000cc"
    hover_color "#000fffcc"

screen file_slots(page, enter=None):
    if main_menu:
        add gui.main_menu_background
    else:
        add renpy.get_screen("bg", layer='master')

    add Solid("#000a", ysize=500) yalign .5
    add gui.game_menu_background

    frame:
        style_prefix "slots"

        grid gui.file_slot_cols gui.file_slot_rows:

            for i in range(gui.file_slot_cols * gui.file_slot_rows):
                $ slot = i + 1
                $ slot_img = FileScreenshot(slot)
                python:
                    if FileLoadable(slot, page=None):
                        surf = im.cache.get(slot_img)
                        x, y = surf.get_size()

                        if (x, y) != (217, 158):
                            slot_img = At(slot_img, xyzoom(1.97))

                button:
                    style 'slot'

                    action FileAction(slot)

                    text "[slot]" style "slot_time_text" size 17 align (.5, 0)

                    add Composite((219, 160),
                        (0, 0), Solid("#fffc"),
                        (0, 0), "overlay/saveload_dummy.webp",
                        (1, 1), slot_img):
                        align (.5, .5)

                    text FileTime(slot, format=_("{#file_time}%H:%M %A,\n %d %B %Y"), empty="Пустой слот"):
                        style "slot_time_text"

                    text FileSaveName(slot):
                        style "slot_name_text"

                    key "save_delete" action FileDelete(slot)

style slots_frame:
    background None
    xfill True
    yfill True
    margin (40, 80)
    padding (0, 0)

style slots_grid:
    spacing 15
    xfill True
    yfill True

style slot:
    background Frame("boxes/slot_bg.webp", Borders(6, 6, 6, 6))
    hover_background Frame("boxes/slot_bg_hover.webp", Borders(6, 6, 6, 6))
    padding (5, 5)

style slot_time_text:
    is text
    size 15
    font "Arciform.otf"
    color "#fff"
    hover_color "#f00"
    outlines [(1, "#000e")]
    hover_outlines [(1, "#000e")]
    align (.5, 1.0)

style slot_name_text:
    is text
    size 15
    font "Arciform.otf"
    color "#fff"
    hover_color "#f00"
    outlines [(1, "#000e")]
    align (.5, .5)

## Экран настроек ##############################################################
##
## Экран настроек позволяет игроку настраивать игру под себя.
##
## https://www.renpy.org/doc/html/screen_special.html#preferences
screen preferences(enter=None):
    tag menu

    if main_menu:
        add gui.main_menu_background
    else:
        add renpy.get_screen("bg", layer='master')

    add gui.game_menu_background

    frame:
        background None
        xfill True
        yfill True
        margin (30, 50)

        grid 2 1:
            style_prefix "prefs"

            frame:
                has vbox
                style_prefix "pref"

                frame:
                    has vbox

                    label "Режим экрана"
                    hbox:
                        textbutton "Оконный":
                            xalign .5
                            action Preference("display", "window")
                        textbutton "Полноэкранный":
                            xalign .5
                            action Preference("display", "fullscreen")

                frame:
                    has vbox
                    label "Эффекты"

                    hbox:
                        textbutton "Включить":
                            xalign .5
                            action Preference("transitions", "all")
                        textbutton "Отключить":
                            xalign .5
                            action Preference("transitions", "none")

                frame:
                    has vbox

                    label "Пропуск текста"
                    hbox:
                        textbutton "Уже увиденного":
                            xalign .5
                            action Preference("skip", "seen")
                        textbutton "Всего":
                            xalign .5
                            action Preference("skip", "all")
                        textbutton "После выборов":
                            xalign .5
                            action Preference("after choices", "toggle")

                frame:
                    has vbox

                    label "Монстропедия"
                    hbox:
                        textbutton "Оригинальная":
                            xalign .5
                            action SetField(persistent, "pedia_view", "old")
                        textbutton "Книжная":
                            xalign .5
                            action SetField(persistent, "pedia_view", "new")

                if not main_menu and world.battle is None:
                    frame:
                        has vbox

                        label "Сложность"
                        hbox:
                            textbutton "Нормальная":
                                xalign .5
                                action SetField(persistent, "difficulty", 1)
                            textbutton "Сложная":
                                xalign .5
                                action SetField(persistent, "difficulty", 2)
                            textbutton "Адская":
                                xalign .5
                                action SetField(persistent, "difficulty", 3)

                    frame:
                        has vbox

                        label "Сцены пожирания"
                        hbox:
                            textbutton "Отключены":
                                xalign .5
                                action SetField(persistent, "vore", False)
                            textbutton "Включены":
                                xalign .5
                                action SetField(persistent, "vore", True)

                frame:
                    has vbox

                    hbox:
                        textbutton "Геймпад" action GamepadCalibrate()
                        textbutton "Джойстик" action Preference("joystick")

            frame:
                has vbox
                style_prefix "pref"

                frame:
                    has vbox

                    label "Скорость текста"
                    bar value Preference("text speed") style "pref_scrollbar"

                frame:
                    has vbox

                    label "Авточтение"
                    bar value Preference("auto-forward time") style "pref_scrollbar"

                    if config.has_voice:
                        textbutton "Ждать голос" action Preference("wait for voice", "toggle")


                frame:
                    has vbox

                    label "Громкость музыки"
                    bar value Preference("music volume") style "pref_scrollbar"

                frame:
                    has vbox

                    label "Громкость звуков"
                    hbox:
                        bar value Preference("sound volume") style "pref_scrollbar"
                        if config.sample_voice:
                            textbutton "Тест" action Play("sound", config.sample_sound)

                frame:
                    has vbox

                    label "Громкость голоса"
                    hbox:
                        bar value Preference("voice volume") style "pref_scrollbar"
                        if config.sample_voice:
                            textbutton "Тест" action Play("voice", config.sample_voice)

                    if config.has_music or config.has_sound or config.has_voice:
                        null height gui.pref_spacing

                        textbutton "Без звука" action Preference("all mute", "toggle")
                frame:
                    has vbox

                    label "Музыка"
                    grid 2 1:
                        xalign .5

                        textbutton "Оригинальная (свободная)":
                            xalign .5
                            action SetField(persistent, "music", False)
                        textbutton "NG+ музыка":
                            xalign .5
                            action SetField(persistent, "music", True)

    use title("Настройки")
    use return_button(enter)

screen error(msg, code):
    vbox:
        align (0.5, 0.5)

        text "[msg]"
        text "Код ошибки: [code]"

        textbutton "Выйти":
            ypos 1.0
            style_prefix "pref"
            action Return()

# Android
screen preferences(enter=None):
    tag menu
    variant "touch"

    if main_menu:
        add gui.main_menu_background
    else:
        add renpy.get_screen("bg", layer='master')

    add gui.game_menu_background

    frame:
        background Frame(persistent.win_bg, Borders(5, 5, 5, 5))
        xfill True
        yfill True
        margin (50, 60)

        vpgrid:
            xfill True
            cols 1
            scrollbars "vertical"
            draggable True
            mousewheel True
            spacing 5
            style_prefix "pref"

            frame:
                has hbox
                label "Эффекты"

                hbox:
                    textbutton "Включить":
                        action Preference("transitions", "all")
                    textbutton "Отключить":
                        action Preference("transitions", "none")

            frame:
                has hbox

                label "Пропуск текста"
                hbox:
                    textbutton "Уже увиденного":
                        action Preference("skip", "seen")
                    textbutton "Всего":
                        action Preference("skip", "all")
                    textbutton "После выборов":
                        action Preference("after choices", "toggle")

            frame:
                has vbox

                label "Монстропедия"
                hbox:
                    textbutton "Оригинальная":
                        action SetField(persistent, "pedia_view", "old")
                    textbutton "Книжная":
                        action SetField(persistent, "pedia_view", "new")

            if not main_menu:
                frame:
                    has vbox

                    label "Сложность"
                    hbox:
                        textbutton "Нормальная":
                            action SetField(persistent, "difficulty", 1)
                        textbutton "Сложная":
                            action SetField(persistent, "difficulty", 2)
                        textbutton "Адская":
                            action SetField(persistent, "difficulty", 3)

                frame:
                    has vbox

                    label "Сцены пожирания"
                    hbox:
                        textbutton "Отключены":
                            action SetField(persistent, "vore", False)
                        textbutton "Включены":
                            action SetField(persistent, "vore", True)

            frame:
                has vbox

                label "Скорость текста"
                bar value Preference("text speed") style "pref_scrollbar"

            frame:
                has vbox

                label "Авточтение"
                hbox:
                    bar value Preference("auto-forward time") style "pref_scrollbar"
                    if config.has_voice:
                        textbutton "Ждать голос" action Preference("wait for voice", "toggle") xalign 0.1

            frame:
                has hbox

                label "Громкость музыки"
                bar value Preference("music volume") style "pref_scrollbar"

            frame:
                has hbox

                label "Громкость звука"
                vbox:
                    bar value Preference("sound volume") style "pref_scrollbar"
                    if config.sample_sound:
                        textbutton "Тест" action Play("sound", config.sample_sound)

            frame:
                has hbox

                label "Громкость голоса"
                vbox:
                    bar value Preference("voice volume") style "pref_scrollbar"
                    if config.sample_voice:
                        textbutton "Тест" action Play("voice", config.sample_voice)
            frame:
                has vbox

                label "Музыка"
                hbox:
                    textbutton "Оригинальная (свободная)":
                        action SetField(persistent, "music", False)
                    textbutton "NG+ музыка":
                        action SetField(persistent, "music", True)

            null

    use title("Настройки")
    use return_button(enter)

style prefs_frame:
    is default
    background Frame(persistent.win_bg, 5, 5)
    xfill True
    yfill True
    padding (10, 10)

style prefs_grid:
    xfill True
    spacing 5

style pref_grid:
    xfill True
    xalign .5

style pref_grid:
    variant "touch"
    xsize 400
    xalign 1.0

style pref_frame:
    background None
    xfill True
    margin (0, 0)

style pref_frame:
    variant "touch"
    background None
    xfill True
    padding (10, 10)

style pref_label:
    variant "touch"
    xalign 0

style pref_label_text:
    color '#ffcc33'
    outlines [(2, "#000c")]
    font "Arciform.otf"
    size 24

style pref_label:
    variant "touch"
    size 32

style pref_hbox:
    variant "touch"
    xfill True

style pref_button:
    xpadding 5
    xmargin 0
    background None

style pref_button:
    variant "touch"
    xalign .5
    size_group "pref"

style pref_button_text:
    outlines [(2, "#000c")]
    selected_color "#3ccd7a"
    hover_color "#000fff"
    font "Arciform.otf"
    size 18
    insensitive_color "#8888"
    insensitive_outlines [(2, "#303030")]

style pref_button_text:
    variant "touch"
    xalign .5

style pref_scrollbar is scrollbar:
    base_bar Frame(persistent.win_bg, Borders(5, 5, 5, 5))
    thumb Composite((25, 12),
        (0, 1), Solid("#fff", xysize=(25, 10)))
    top_gutter 3
    bottom_gutter 3
    left_gutter 3
    right_gutter 3
    thumb_offset 0
    xfill True
    xalign .5

style pref_scrollbar:
    variant "touch"
    thumb Composite((25, 14),
        (0, 1), Solid("#fff", xysize=(25, 13)))
    align (1.0, .5)
    xsize 400
    ysize 15

style pref_vscrollbar is vscrollbar:
    base_bar Frame(persistent.win_bg, Borders(5, 5, 5, 5))
    bar_vertical True
    thumb Composite((12, 25),
        (1, 0), Solid("#fff", xysize=(10, 25)))
    yfill True
