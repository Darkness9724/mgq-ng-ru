# одноцветные изображения
image bg black = Solid("#000")
image bg white = Solid("#fff")
image color black = Solid("#000")
image color white = Solid("#fff")

image ctc_wait:
    Composite((24, 23),
        (8, 6), Transform("ctc_wait.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_out_color, persistent.what_out_color)),
        (7, 4), Transform("ctc_wait.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_color, persistent.what_color)))
    0.2

    Composite((24, 23),
        (8, 8), Transform("ctc_wait.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_out_color, persistent.what_out_color)),
        (7, 6), Transform("ctc_wait.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_color, persistent.what_color)))
    0.2
    repeat

image ctc:
    Composite((24, 23),
        (8, 6), Transform("ctc.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_out_color, persistent.what_out_color)),
        (7, 4), Transform("ctc.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_color, persistent.what_color)))
    0.2

    Composite((24, 23),
        (8, 8), Transform("ctc.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_out_color, persistent.what_out_color)),
        (7, 6), Transform("ctc.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_color, persistent.what_color)))
    0.2
    repeat

image face = TransitionConditionSwitch(Dissolve(0.5, alpha=True),
    ["world.party.player == world.actor[1]",
        "battle/faces/face_a0[world.party.player.status].webp"],
    ["world.party.player == world.actor[2]",
        "battle/faces/face_e0[world.party.player.status].webp"],
    ["world.party.player == world.actor[3]",
        "battle/faces/face_d0[world.party.player.status].webp"],
    ["world.party.player == world.actor[4]",
        "battle/faces/face_b0[world.party.player.status].webp"],
    ["world.party.player == world.actor[5]",
        "battle/faces/face_c0[world.party.player.status].webp"],
    ["world.party.player.bind == 0",
        "battle/faces/face_0[world.party.player.status].webp"],
    ["world.party.player.bind > 0",
        "battle/faces/face_1[world.party.player.status].webp"]
    )

image elm_wind = "battle/elm_wind.webp"

image elm_holy = TransitionConditionSwitch(Dissolve(0.5, alpha=True),
    ["world.party.player.holy and not world.party.player.skill[1]",
        "battle/elm_wind2.webp"],
    ["world.party.player.holy and world.party.player.skill[1]",
        "battle/elm_wind3.webp"]
    )

image elm_earth = "battle/elm_earth.webp"

image elm_aqua = TransitionConditionSwitch(Dissolve(0.5, alpha=True),
    ["(world.party.player.aqua == 4 and not world.party.player.skill[3])",
        "battle/elm_aqua2.webp"],
    ["world.party.player.aqua > 0 and world.party.player.skill[3]",
        "battle/elm_aqua.webp"]
    )

image elm_fire = "battle/elm_fire.webp"
image elm_poison = "battle/elm_poison.webp"

# персонаж в главном меню
image gm char = ConditionSwitch(
    "world.party.player.skillset == 1",
        Image("images/Characters/luka/luka st02.webp", align=(0, 0), pos=(130, 0)),
    "world.party.player.skillset != 1",
        Image("images/Characters/luka/luka st03.webp", align=(0, 0), pos=(20, 0)))

# лого
image logo = Image("logo.webp", align=(.5, .25))
image snow = SnowBlossom("snowflake.webp", count=100)
# изображение нотки
image note:
    Composite((20, 19),
        (0, 0), Transform("note.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_out_color, persistent.what_out_color)),
        (2, 2), Transform("note.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_out_color, persistent.what_out_color)),
        (0, 2), Transform("note.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_out_color, persistent.what_out_color)),
        (2, 0), Transform("note.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_out_color, persistent.what_out_color)),
        (1, 1), Transform("note.webp", matrixcolor=
                                ColorizeMatrix(persistent.what_color, persistent.what_color)))
