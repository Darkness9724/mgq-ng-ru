init python:
    def pedia_battle(difficulty, label, scope):
        prev_difficulty = persistent.difficulty
        persistent.difficulty = difficulty
        renpy.call_replay(f"{label}_start", scope)
        persistent.difficulty = prev_difficulty

    class Troop:
        def __init__(self, world):
            self.world = world
            self.enemy = [None] * 3
            self.battle = Battle(self.enemy, self.world)

    class Enemy:
        def __init__(self, world):
            self.world = world
            self.party = world.party
            self.battle = world.battle
            self.name = Character(None)
            self.max_life = 0
            self.life = 0
            self.sprite = [""] * 3
            self.position = center
            self.defense = 0
            self.evasion = 0
            self.evasion_el = 0
            self.power = 0
            self.wind = 0
            self.wind_turn = 0
            self.earth = 0
            self.earth_turn = 0
            self.aqua = 0
            self.aqua_turn = 0
            self.fire = 0
            self.fire_turn = 0
            self.holy = 0
            self.holy_turn = 0
            self.damage = 0
            self.skill_sub = 0
            self.is_damaged = False
            self.is_attack = False
            self.counter = 0
            self.lose = ""
            self.sealed = ""
            self.level = 0
            self.tmp = [""] * 3
            self.half = [""] * 2
            self.kiki = [""] * 2
            self.power_desc = [""] * 4
            self.power_cry = [""] * 5
            self.power_escaped = [""] * 2
            self.henkahp = [0] * 3
            self.list = [""] * 15
            self.skip = False
            self.bind_maxhp = 0
            self.bind_hp = 0
            self.charge = 0
            self.countered = False

        # Функция вычисления урона для противника
        def life_calc(self, line=0):
            if self.battle.no_hit:
                self.no_hit()
                return

            if self.wind == 4 and self.party.player.aqua != 2:
                self.wind_guard()
                return

            if self.aqua and self.party.player.skill[3] and self.party.player.wind != 3 and self.party.player.holy != 3 and not self.party.player.support_attack and self.battle.result != 17:
                random = rand().randint(1, 100)

                if self.aqua == 2 and random <= 75:
                    self.aqua_guard()
                    return
                elif self.aqua == 3 and random <= 37:
                    self.aqua_guard()
                    return
                elif self.aqua == 4 and not self.party.player.aqua == 2:
                    self.aqua_guard()
                    return
                elif self.aqua == 4 and self.party.player.aqua == 2 and random <= 37:
                    self.aqua_guard()
                    return

                if self.wind == 2 and random <= 50:
                    self.wind_guard()
                    return
                elif self.wind == 3 and random <= 75:
                    self.wind_guard()
                    return

                if self.holy:
                    self.holy_guard()
                    return

            if not renpy.get_skipping():
                renpy.show(self.sprite[0].split()[0], at_list=[slash, alpha_off])

            if self.party.player.support_attack:
                self.party.player.damage = self.party.player.damage * self.party.member[0].level * self.defense // 2000
            else:
                self.party.player.damage = self.party.player.damage * self.party.player.level * self.defense // 2000

            if self.party.player.enrage and self.party.player.enrage_turn != 0:
                self.party.player.damage = self.party.player.damage * 2

            self.party.player.damage = rand().randint(self.party.player.damage * 9 // 10, self.party.player.damage * 11 // 10)

            if self.party.check(2):
                if self.world.actor[2].rep > 9:
                    self.party.player.damage = self.life_gran(self.party.player.damage)
            if self.party.player.fire > 2:
                self.party.player.damage = self.life_sala(self.party.player.damage)

            if line == 0:
                renpy.pause(0.5)

                narrator(f"{self.party.player.name[0]} наносит {self.party.player.damage} урона!")
            else:
                self.tmp[line-1] = renpy.substitute(f"{self.party.player.name[0]} наносит {self.party.player.damage} урона!")

                if line == 1:
                    extend_say(narrator, f"{self.tmp[line-1]}")
                else:
                    extend_say(extend, f"\n{self.tmp[line-1]}")

            if self.skill_sub != 2:
                if self.life - self.party.player.damage < 0:
                    self.life = 0
                else:
                    self.life -= self.party.player.damage

            self.is_damaged = True

        # Для катс-сцен, не меняет урон
        def raw_life_calc(self, line=0):
            if not renpy.get_skipping():
                renpy.show(self.sprite[0].split()[0], at_list=[slash, alpha_off])

            if line == 0:
                renpy.pause(0.5)

                narrator(f"{self.party.player.name[0]} наносит {self.party.player.damage} урона!")
            else:
                self.tmp[line-1] = renpy.substitute(f"{self.party.player.name[0]} наносит {self.party.player.damage} урона!")

                if line == 1:
                    extend_say(narrator, f"{self.tmp[line-1]}")
                else:
                    extend_say(extend, f"\n{self.tmp[line-1]}")

            if self.life - self.party.player.damage < 0:
                self.life = 0
            else:
                self.life -= self.party.player.damage

        def life_gran(self, damage):
            if 9 < self.world.actor[2].rep < 20:
                damage = damage * 105//100
            elif 19 < self.world.actor[2].rep < 30:
                damage = damage * 108//100
            elif 29 < self.world.actor[2].rep < 40:
                damage = damage * 112//100
            elif 39 < self.world.actor[2].rep < 50:
                damage = damage * 118//100
            elif self.world.actor[2].rep > 49:
                damage = damage * 125//100

            return damage

        # Усиление урона от Саламандры
        def life_sala(self, damage):
            if self.party.salamander_rep < -19:
                damage = damage * 75//100
            elif -20 < self.party.salamander_rep < -14:
                damage = damage * 80//100
            elif -15 < self.party.salamander_rep < -9:
                damage = damage * 85//100
            elif -10 < self.party.salamander_rep < -4:
                damage = damage * 90//100
            elif -5 < self.party.salamander_rep < 0:
                damage = damage * 95//100
            elif 9 < self.party.salamander_rep < 20:
                damage = damage * 110//100
            elif 19 < self.party.salamander_rep < 30:
                damage = damage * 115//100
            elif 29 < self.party.salamander_rep < 40:
                damage = damage * 120//100
            elif 39 < self.party.salamander_rep < 50:
                damage = damage * 125//100
            elif 49 < self.party.salamander_rep:
                damage = damage * 130//100

            if self.world.fire == 1:
                damage = damage * 120//100
            elif self.world.fire == 2:
                damage = damage * 135//100
            elif self.world.fire == 3:
                damage = damage * 150//100

            if self.world.aqua == 1:
                damage = damage * 90//100
            elif self.world.aqua == 2:
                damage = damage * 85//100
            elif self.world.aqua == 3:
                damage = damage * 80//100

            return damage

        # Атака противника
        def attack(self):
            renpy.call(f"{self.battle.tag}_a")

        # Урон противника
        def deal(self, d, effect=0, line=0, poison_damage=False, raw_damage=False, drain=False, direct=False):
            DELAY = 0.5

            # Исключает уворот или контратаку против яда
            if not poison_damage and not direct:
                # И если персонаж схвачен
                if not self.party.player.bind:
                    # Если битва против обычных противников, даёт 100% шанс уворота
                    if not self.battle.ng:
                        if self.party.player.aqua == 2 and self.party.player.wind == 5:
                            self.party.player.serenegale_guard()
                            self.battle.main()
                        if self.party.player.aqua:
                            self.party.player.aqua_guard()
                        if self.party.player.wind or self.party.player.holy:
                            self.party.player.wind_guard()
                            self.battle.main()

                    if self.party.player.daystar and (self.party.player.status == 0 or self.party.player.status == 9):
                        self.party.player.skill22x()

                if self.party.player.aqua == 2:
                    self.party.player.aqua_end = True

            # Если в аргумент функции поступил массив из двух чисел (d[0] и d[1]), вычисляет случайное число между ними.
            if len(d) == 2:
                damage = rand().randint(d[0], d[1])
            # В противном случае передаёт аргумент d переменной damage без изменений.
            else:
                damage = d[0]

            # Прорыв защиты
            if self.battle.guard_break_on and self.party.player.guard:
                self.battle.guard_break()

            # Нормальный эффект удара
            if effect == 0:
                    renpy.sound.play("se/damage.ogg")
                    renpy.transition(hpunch, "master")
                    renpy.transition(hpunch, "screens")
                    DELAY = 0.5
            # Эффект сильной тряски
            elif effect == 1:
                    renpy.sound.play("se/damage2.ogg")
                    renpy.transition(Quake((0, 0, 0 ,0), 2.0, dist=30), "master")
                    renpy.transition(Quake((0, 0, 0 ,0), 2.0, dist=30), "screens")
                    DELAY = 2.5
            # Эффект слабой тряски
            elif effect == 2:
                    renpy.sound.play("se/ikazuti.ogg")
                    renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=20), "master")
                    renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=20), "screens")
                    DELAY = 1.5

            # Если эта атака ядом или атака raw_damage, урон не будет увеличиваться или ослабляться
            if not raw_damage or not poison_damage:
                # Увеличение силы атаки от поглощённого уровня
                if self.level:
                    damage += self.level * 3

                if self.fire == 1:
                    damage = damage * 15 // 10
                elif self.fire == 2:
                    damage = damage * 18 // 10
                elif self.fire == 3:
                    damage = damage * 22 // 10

                if self.party.player.guard == 1:
                    damage = damage // 2
                elif self.party.player.guard == 2:
                    damage = damage * 2 // 3

                if self.party.player.earth:
                    damage = damage * (100 - self.party.player.earth_keigen) // 100

                if self.party.player.aqua == 1:
                    damage = damage * 4 // 5

                # Уменьшение урона в зависимости от отношений с Тамамо. :3
                if self.party.check(3):
                    if self.world.actor[3].rep > 9:
                        damage = damage * (100 - (self.world.actor[3].rep*2//3)) // 100

                if self.party.player.blackwind and self.party.player.blackwind_turn != 0:
                    damage = damage * 2 // 3

                if self.party.pentagram and self.party.pentagram_turn != 0 or self.party.player.pentagram and self.party.player.pentagram_turn != 0:
                    damage = damage * 20 // 100

                if self.party.player.titanguard and self.party.player.titanguard_turn != 0 or self.party.player.gnomaren and self.party.player.gnomaren_turn != 0:
                    damage = damage * 1 // 100

                if self.party.player.potion == 2:
                    damage = damage * 65 // 100
                if self.party.player.potion == 5:
                    damage = 0

            if damage == 0:
                damage = 1

            # Если удар единичный, выводит одну строку.
            if line == 0:
                renpy.pause(DELAY)
                narrator(f"{self.party.player.name[0].name} получает {damage} урона!")
            # В противном случае выводит от 1 до 3 строк.
            else:
                print(line)
                self.tmp[line-1] = renpy.substitute(f"{self.party.player.name[0]} получает {damage} урона!")

                if line == 1:
                    extend_say(narrator, f"{self.tmp[line-1]}", line-1)
                else:
                    extend_say(extend, f"\n{self.tmp[line-1]}")

            if drain:
                persistent.count_drainhp += damage
                self.life_regen(damage)

            # Если урон больше текущего HP, не позволяет здоровью уйти в минус
            if self.party.player.life - damage < 0:
                self.party.player.life = 0
            else:
                self.party.player.life -= damage

            self.party.player.giga_damage = 1

        # Ветрянной уворот противника
        def wind_guard(self):
            self.battle.hide_skillname()
            renpy.sound.play("se/wind2.ogg")
            renpy.hide(self.battle.tag)
            renpy.pause(0.01)
            self.battle.face(2)
            narrator(f"Но {self.battle.enemy[0].name} двигается подобно ветру и уклоняется от атаки!")
            self.battle.face(1, True)

        # Водяной уворот противника
        def aqua_guard(self):
            renpy.pause(0.5)
            self.battle.show_skillname("Безмятежное движение")
            renpy.sound.play("se/miss_aqua.ogg")
            renpy.hide(self.sprite[0].split()[0])
            renpy.pause(0.01)
            self.battle.face(2)
            narrator(f"Но {self.battle.enemy[0].name} уклоняется, словно поток воды, обегающий препятствие!")
            self.battle.face(1, True)

        # Исцеление противника
        def life_regen(self, life):
            renpy.sound.play("se/kaihuku.ogg")

            narrator(f"{self.battle.enemy[0].name} восстанавливает {life} HP!")

            if self.life + life > self.max_life:
                self.life = self.max_life
            else:
                self.life += life

        # Уворот противника
        def evade(self):
            if not renpy.get_skipping():
                renpy.show(self.sprite[0], at_list=[miss])
            renpy.pause(1.0)

        def element_end(self):
            if self.wind and not self.wind_turn:
                self.wind = 0
                narrator(f"{self.name.name} теряет стремительность движения!")

            if self.earth and not self.earth_turn:
                self.earth = 0
                narrator(f"Тело {self.name.name} возвращается в норму!")

            if self.aqua and not self.aqua_turn:
                self.aqua = 0
                narrator(f"{self.name.name} теряет связь с потоками!")

            if self.fire and not self.fire_turn:
                self.fire = 0
                narrator(f"{self.name.name} успокаивается!")

        # Изменение лица противника по переданному аргументу
        def face(self, param, force):
            if param in (1, 2):
                if self.party.player.bind and not force:
                    return

                if param == 1:
                    if self.party.player.bind or (self.party.player.max_life > self.party.player.life*5 and self.party.player.kiki):
                        param = 2

            elif param == 3:
                self.hide_ct()

            renpy.show(self.sprite[param-1], at_list=[self.position], zorder=5)
            renpy.transition(dissolve, layer="master")

    class Battle:
        def __init__(self, enemy, world):
            self.world = world
            self.party = world.party
            self.tag = None
            self.name = Character(None)
            self.enemy = enemy
            self.background = ""
            self.music = 0
            self.ng = False
            self.result = 0
            self.turn = 0
            self.element_turn = True
            self.used_serenegale = 0
            self.used_daystar = 0
            self.plural_name = 0
            self.progress = 0
            self.cmd = [True] * 8
            self.exp = 0
            self.exit = ""
            self.guard_break_on = False
            self.ren = 0
            self.nostar = 0
            self.nocharge = 0
            self.bad = [""] * 2
            self.no_hit = False
            self.stage = [0] * 20
            self.first = [False] * 4
            self.skill_num = 0
            self.delay = [0] * 10

        # Расчёт значений в зависимости от сложности
        def difficulty(self, easy, medium, hard):
            if persistent.difficulty == 1:
                return easy
            elif persistent.difficulty == 2:
                return medium
            elif persistent.difficulty == 3:
                return hard
            else:
                return 1

        # Проигрывает музыку в зависимости от битвы
        def musicplay(self, music):
            mus_dic = {
                1: "battle",
                2: "boss1",
                3: "boss2",
                4: "sitenno",
                5: "maou",
                6: "comi1",
                7: "ruka",
                8: "boss3",
                9: "boss4",
                10: "yonseirei",
                11: "yonseirei2",
                12: "tamamo",
                13: "boss0",
                14: "eden",
                15: "stein2",
                16: "maou3",
                17: "maou4",
                18: "irias2",
                19: "irias3",
                20: "granberia",
                21: "tamamo",
                22: "granberia2",
                23: "spirits",
                24: "alma",
                25: "erubetie",
                26: "boss",
                27: "boss2",
                28: "boss3",
                29: "boss4",
                30: "spirits2",
                31: "boss5",
                32: "ruka4"
            }

            if not is_integer(music):
                renpy.music.play(music)
            elif music > 19:
                renpy.music.play("ngdata/bgm/%s.ogg" % mus_dic[music])
            elif music < 20:
                renpy.music.play("bgm/%s.ogg" % mus_dic[music])

        # Обработка хода битвы
        def main(self):
            if self.world.battle_skip or not self.get_life():
                renpy.jump(f"{self.tag}_v")

            for enemy in self.enemy:
                if enemy is not None:
                    enemy.countered = False

            self.party.player.guard_on = False

            if self.party.support_turn:
                self.party.support_turn -= 1

            if self.party.player.before_action != 91:
                self.party.player.mp_charge = 0

            if self.party.player.guard == 1:
                self.party.player.guard_on = True
            elif self.party.player.guard == 2 and self.party.player.before_action == 42 and self.party.player.skill[0] > 26:
                self.party.player.guard_on = True
            elif self.party.player.guard == 2 and self.party.player.before_action == 91 and self.party.player.skill[0] > 26:
                self.party.player.guard_on = True

            if not self.party.player.guard_on:
                self.party.player.guard = 0
            if self.party.player.guard == 1:
                self.party.player.guard = 0

            if self.party.player.turn > self.party.player.max_turn:
                self.party.player.turn = 0

            self.turn += 1

            # Отложенные атаки
            self.delay = [idx+1 for idx in self.delay]

            if persistent.difficulty == 3:
                self.delay = [100] * 10

            if self.party.player.max_life != self.party.player.life:
                self.party.player.nodamage = False

            if self.party.player.max_life > self.party.player.life*5 and not self.party.player.bind and self.party.player.kiki:
                self.face(2)

            if not self.party.player.bind:
                if self.party.player.wind and self.party.player.wind_turn != -1:
                    self.party.player.wind_turn -= 1
                    if self.party.player.wind_turn == 0:
                        self.party.player.element_end()

                if self.party.player.earth and self.party.player.earth_turn != -1:
                    self.party.player.earth_turn -= 1
                    if self.party.player.earth_turn == 0:
                        self.party.player.element_end()

                if self.party.player.aqua and self.party.player.aqua_turn != -1:
                    if self.party.player.aqua == 2 and self.party.player.aqua_end:
                        self.party.player.serene_end()

                    elif self.party.player.aqua == 2 and not self.party.player.aqua_end:
                        if self.party.player.wind != 5:
                            self.party.player.mp -= 1
                        elif self.party.player.wind == 5:
                            self.party.player.mp -= 2

                        if self.party.player.mp < 0:
                            self.party.player.mp = 0

                        self.party.player.aqua_turn = self.party.player.mp

                        if self.party.player.aqua_turn == 0:
                            self.party.player.serene_end()
                    else:
                        self.party.player.aqua_turn -= 1

                        if self.party.player.aqua_turn == 0:
                            self.party.player.element_end()

                if self.party.player.aqua == 2 and self.party.player.wind == 5:
                    self.party.player.wind_turn = self.party.player.mp

                if self.party.player.fire and self.party.player.fire_turn != -1:
                    self.party.player.fire_turn -= 1
                    if self.party.player.fire_turn == 0:
                        self.party.player.element_end()

                if self.party.player.holy and self.party.player.holy_turn != -1:
                    self.party.player.holy_turn -= 1
                    if self.party.player.holy_turn == 0:
                        self.party.player.element_end()

            else:
                if self.party.player.aqua == 2:
                    self.party.player.serene_end()

            self.party.player.aqua_end = False

            if self.party.player.poison and self.party.player.poison_turn:
                self.party.player.poison_turn -= 1
                if not self.party.player.poison_turn:
                    self.party.player.poisonfade()
                else:
                    self.party.player.poison_damage()

            if not self.party.player.bind:
                for enemy in self.enemy:
                    if enemy is not None:
                        if enemy.wind and enemy.wind_turn != -1:
                            enemy.wind_turn -= 1
                            if enemy.wind_turn == 0:
                                enemy.element_end()
                        if enemy.earth and enemy.earth_turn != -1:
                            enemy.earth_turn -= 1
                            if enemy.earth_turn == 0:
                                enemy.element_end()
                        if enemy.aqua and enemy.aqua_turn != -1:
                            enemy.aqua_turn -= 1
                            if enemy.aqua_turn == 0:
                                enemy.element_end()
                        if enemy.fire and enemy.fire_turn != -1:
                            enemy.fire_turn -= 1
                            if enemy.fire_turn == 0:
                                enemy.element_end()
                        if enemy.holy and enemy.holy_turn != -1:
                            enemy.holy_turn -= 1
                            if enemy.holy_turn == 0:
                                enemy.element_end()

            if self.party.player.status:
                self.party.player.status_turn -= 1
                if self.party.player.status in (1, 2, 4, 5, 7) and self.party.player.status_turn or self.party.player.status == 8:
                    self.party.player.status_sel(1)
                elif self.party.player.status in (1, 2, 4, 5, 7) and not self.party.player.status_turn:
                    self.party.player.status_sel(2)
                elif self.party.player.status == 6:
                    self.party.player.status_sel(3)

            if self.party.player.giga:
                if self.party.player.giga_damage or self.party.player.bind or self.party.player.status:
                    self.party.player.giga_damage = 0
                    self.party.player.giga = 0

                narrator("Сила, наполняющая меч Луки, исчезает!")

                if self.party.player.giga == 1:
                    self.party.player.skill15a()
                elif self.party.player.giga == 2:
                    self.party.player.skill15b()
                elif self.party.player.giga == 3:
                    self.party.player.skill15c()
                elif self.party.player.giga == 4:
                    self.party.player.skill15d()

            if self.party.player.surrendered:
                for enemy in self.enemy:
                    if enemy is not None:
                        enemy.attack()

            renpy.jump(f"{self.tag}_main")

        # Атака
        def common_attack(self):
            if self.party.player.aqua:
                self.party.player.attack_aqua()
                self.common_attack_end()

            if (self.party.player.wind == 3 or self.party.player.holy == 3) and self.party.player.earth == 0:
                random = rand().randint(1, 100)

                if random == 1:
                    self.party.player.attack_critical(False)
                    self.party.player.attack_critical(True)
                    self.common_attack_end()
                elif random <= 10:
                    self.party.player.attack(False)
                    self.party.player.attack_critical(True)
                    self.common_attack_end()
                elif random <= 20:
                    self.party.player.attack_critical(False)
                    self.party.player.attack(True)
                    self.common_attack_end()
                else:
                    self.party.player.attack(False)
                    self.party.player.attack(True)
                    self.common_attack_end()

            elif (self.party.player.wind == 3 or self.party.player.holy == 3) and self.party.player.earth == 2:
                random = rand().randint(1, 25)

                if random == 1:
                    self.party.player.attack_critical(False)
                    self.party.player.attack_critical(True)
                    self.common_attack_end()
                elif random <= 5:
                    self.party.player.attack(False)
                    self.party.player.attack_critical(True)
                    self.common_attack_end()
                elif random <= 10:
                    self.party.player.attack_critical(False)
                    self.party.player.attack(True)
                    self.common_attack_end()
                else:
                    self.party.player.attack(False)
                    self.party.player.attack(True)
                    self.common_attack_end()

            elif (self.party.player.wind == 3 or self.party.player.holy == 3) and self.party.player.earth == 3:
                self.party.player.attack_critical(False)
                self.party.player.attack_critical(True)
                self.common_attack_end()

            random = rand().randint(1, 100)

            for enemy in self.enemy:
                if enemy is not None:
                    if random > enemy.evasion and self.party.player.wind < 2:
                        self.party.player.attack_miss()
                        self.common_attack_end()
                    elif random > enemy.evasion and self.party.player.holy < 3 and not self.party.player.skill[1]:
                        self.party.player.attack_miss()
                        self.common_attack_end()
                    elif random > enemy.evasion_el and enemy.wind:
                        self.party.player.attack_miss()
                        self.common_attack_end()
                    elif self.party.player.earth == 3:
                        self.party.player.attack_critical()
                        self.common_attack_end()

            random = rand().randint(1, 10)

            if random == 9 and self.party.player.earth == 2:
                self.party.player.attack_critical()
                self.common_attack_end()
            elif random > 7 and self.party.player.max_life < self.party.player.level*5:
                self.party.player.attack_critical()
                self.common_attack_end()
            elif random == 10:
                self.party.player.attack_critical()
                self.common_attack_end()

            self.party.player.attack()
            self.common_attack_end()

        # Конец атаки
        def common_attack_end(self):
            self.hide_skillname()

            for enemy in self.enemy:
                if enemy is not None:
                    if enemy.aqua and enemy.aqua != 3 and enemy.aqua != 4 and self.result == 37:
                        enemy.aqua_turn = 0
                        enemy.element_end()

            self.party.player.guard_on = False
            self.party.player.turn += 1

            for enemy in self.enemy:
                if enemy is not None:
                    if enemy.life == 0:
                        renpy.jump(f"{self.tag}_v")

            if self.party.player.before_action != 3 or self.party.player.before_action != 59:
                for enemy in self.enemy:
                    if enemy is not None:
                        if enemy.counter:
                            self.counter()
                            self.main()

            for enemy in self.enemy:
                if enemy is not None:
                    if self.party.player.turn < self.party.player.max_turn:
                            enemy.main()

                    enemy.attack()

        def common_struggled_attack(self, damage):
            self.party.player.attack_struggle(damage)
            self.common_attack_end()

        def common_struggle(self, escaped=False):
            if not self.ng and self.result == 1:
                self.common_struggled_attack(100)
            if escaped:
                self.party.player.escape()
            else:
                self.party.player.struggle()

            self.attack()

        def attack(self):
            for enemy in self.enemy:
                if enemy is not None:
                    enemy.attack()

        # Вызов меню боя
        def call_cmd(self, action="attack"):
            renpy.start_predict_screen("cmd")
            _window_hide()
            self.result = renpy.call_screen("cmd", action)
            _window_show(auto=True)
            renpy.stop_predict_screen("cmd")

        # Навыки
        def common_skill(self):
            if self.party.player == self.world.actor[0]:
                self.call_cmd("luka_skill1")
            self.party.player.skill_result()

        def common_guard(self):
            self.party.player.before_action = 51
            self.party.player.guard = 1
            self.party.player.mp_charge = 0

            narrator(f"{self.party.player.name[0].name} принимает защитную стойку!")

            renpy.jump(f"{self.tag}_a")

        def common_wait(self):
            self.party.player.before_action = 52

            narrator(f"{self.party.player.name[0].name} остаётся неподвижным.")

            renpy.jump(f"{self.tag}_a")

        def common_surrender(self):
            self.party.player.before_action = 53
            self.party.player.surrendered = True
            persistent.count_surrenders += 1

            if self.party.player.aqua == 2:
                    renpy.show(self.background)
                    renpy.with_statement(Dissolve(1.5))
                    self.musicplay(self.music)

            self.party.player.wind_turn = 0
            self.party.player.earth_turn = 0
            self.party.player.aqua_turn = 0
            self.party.player.fire_turn = 0
            self.party.player.holy_turn = 0
            self.party.player.element_end()

        # Компаньоны
        def common_support(self):
            self.call_cmd("companions")

            if self.result == 1:
                self.party.player = world.actor[1]
                if not self.party.player.used_support:
                    self.party.player.help()
                self.call_cmd("alice_skills")

            elif self.result == 2:
                self.party.player = world.actor[2]
                if not self.party.player.used_support:
                    self.party.player.help()
                self.call_cmd("granberia_skills")

            elif self.result == 3:
                self.party.player = world.actor[3]
                if not self.party.player.used_support:
                    self.party.player.help()
                self.call_cmd("tamamo_skills")

            elif self.result == 4:
                self.party.player = world.actor[4]
                if not self.party.player.used_support:
                    self.party.player.help()
                self.call_cmd("alma_skills")

            elif self.result == 5:
                self.party.player = world.actor[5]
                if not self.party.player.used_support:
                    self.party.player.help()
                self.call_cmd("erubetie_skills")

            self.party.player.support_attack = True
            self.party.player.skill_result()
            self.party.player.support_attack = False
            self.party.player = world.actor[0]

            renpy.hide("alice")
            renpy.hide("granberia")
            renpy.hide("tamamo")
            renpy.hide("alma_elma")
            renpy.hide("erubetie")

            renpy.jump(f"{self.world.battle.tag}_main")

        # Суммарное здоровье всех противников
        def get_life(self, string="current"):
            life = 0

            for enemy in self.enemy:
                if enemy is not None:
                    if string == "current":
                        life += enemy.life
                    elif string == "max":
                        life += enemy.max_life

            return life

        # Победа
        def victory(self, viclose=0):
            # if viclose == 1:
            #     self.face(3)

            #     enemy.name("[self.battle.enemy[0].lose]")

            #     renpy.sound.play("se/syometu.ogg")
            #     renpy.hide("slug")
            #     renpy.with_statement(crash)

            #     if self.plural_name:
            #         narrator("[self.battle.enemy[0].name] была запечатаны в [self.battle.enemy[0].sealed]!")
            #     else:
            #         narrator("[self.battle.enemy[0].name] была запечатана в [self.battle.enemy[0].sealed]!")

            if self.party.player.aqua == 2:
                renpy.show(self.background)
                renpy.transition(Dissolve(1.5), layer="master")

            renpy.music.play("bgm/fanfale.ogg", loop=False)

            self.element_turn = False
            renpy.hide_screen("hp")

            if self.plural_name == 1:
                if viclose == 1:
                    narrator(f"{self.name.name} побеждены!")
                elif viclose == 2:
                    narrator(f"{self.name.name} убегают прочь!")
            else:
                if viclose == 1:
                    narrator(f"{self.name.name} побеждена!")
                elif viclose == 2:
                    narrator(f"{self.name.name} убегает прочь!")

            self.party.player.wind = 0
            self.party.player.earth = 0
            self.party.player.aqua = 0
            self.party.player.fire = 0
            self.party.player.holy = 0

            for enemy in self.enemy:
                if enemy is not None:
                    enemy.wind = 0
                    enemy.earth = 0
                    enemy.aqua = 0
                    enemy.fire = 0
                    enemy.holy = 0

            self.nocharge = 0
            self.plural_name = 0

            persistent.count_wins += 1

            if self.party.player.life == self.party.player.max_life and self.party.player.nodamage:
                persistent.count_hpfull = True

            if persistent.difficulty == 3:
                persistent.count_hellvic = True

            if self.party.player.status == 7:
                persistent.count_vickonran = True

            for enemy in self.enemy:
                if enemy is not None:
                    if enemy.countered:
                        persistent.counterk = True

            for member in self.party.member:
                if member is not None:
                    member.support = False

            renpy.end_replay()

            if self.party.player.status == 9:
                self.party.player.poisonfade()

            if self.party.player.level_sin != self.party.player.level:
                self.party.player.level = self.party.player.level_sin

                narrator("После победы над противником, уровень Луки возвращается!")

            for enemy in self.enemy:
                if enemy is not None:
                    for sprite in enemy.sprite:
                        renpy.stop_predict(sprite)
            renpy.stop_predict(self.background)
            renpy.stop_predict_screen("hp")
            renpy.save_persistent()
            renpy.free_memory()

            if viclose == 1:
                self.party.player.lvup(self.exp)

            dynamic_variable(f"persistent.{self.tag}_unlock", True)

            store._rollback = True
            store._save = True
            store._autosave = True

            self.party.player.skill_set(1)
            self.world.battle = None

            if exit == "return":
                renpy.return_statement()
                renpy.set_return_stack([])
            else:
                # Временный костыль, пока я не придумаю как лучше реорганизовать стек вызовов
                renpy.set_return_stack([])
                renpy.jump(self.exit)

        def lose(self, viclose=0):
            if self.party.player.aqua == 2:
                renpy.show(self.background)
                renpy.transition(Dissolve(1.5), layer="master")

            self.party.player.wind = 0
            self.party.player.earth = 0
            self.party.player.aqua = 0
            self.party.player.fire = 0
            self.party.player.holy = 0
            self.element_turn = False
            renpy.hide_screen("hp")

            if not viclose:
                narrator(f"{self.party.player.name[0].name} проигрывает...")
            else:
                narrator(f"{self.party.player.name[0].name} сдался...")

            renpy.music.play("audio/bgm/ero1.ogg", fadeout=1)

            if self.turn == 1 and not self.party.player.surrendered:
                persistent.count_onekill = True

            for idx in ("wind", "earth", "aqua", "fire"):
                for enemy in self.enemy:
                    if enemy is not None:
                        if getattr(enemy, "%s" % idx):
                            setattr(persistent, "count_enemy_%s" % idx, True)

            persistent.hsean = True

            self.count_defeat()

        def count_defeat(self):
            persistent.defeat[self.tag] += 1
            if persistent.defeat[self.tag] == 1:
                persistent.count_enemy_wins += 1

            if persistent.defeat[self.tag] > persistent.count_most:
                persistent.count_most = persistent.defeat[self.tag]
                persistent.count_mostname = self.name.name

        def show_skillname(self, skn=""):
            renpy.show("", at_list=[sk], what=Text(skn, style="skillname"), zorder=50, tag="skillname")
            renpy.pause(0.5)

        def hide_skillname(self):
            renpy.hide("skillname")
            renpy.pause(0.5)

        def hide_ct(self):
            renpy.hide("ct")

        # Изменение лиц противников по переданному аргументу
        def face(self, param, force=False):
            if param in (1, 2):
                if self.party.player.bind and not force:
                    return

                if param == 1:
                    if self.party.player.bind or (self.party.player.max_life > self.party.player.life*5 and self.party.player.kiki):
                        param = 2

            elif param == 3:
                self.hide_ct()

            for enemy in self.enemy:
                if enemy is not None:
                    renpy.show(enemy.sprite[param-1], at_list=[enemy.position], zorder=5)

            renpy.transition(dissolve, layer="master")

        # Случайная фраза или её продолжение
        def cry(self, who, what, extend=False):
            if not extend:
                while True:
                    random = rand().randint(0, len(what)-1)

                    if random != self.ren:
                        self.ren = random
                        break

            who(what[self.ren])

        # Контратака
        def counter(self):
            renpy.sound.play("se/counter.ogg")
            if not renpy.get_skipping():
                renpy.show("", at_list=[counter], what=Text("КОНТРАТАКА!!!", style="counter"), zorder=30, tag="txt", layer="overlay")
            else:
                renpy.show("", at_list=[align(0.5, 0.5)], what=Text("КОНТРАТАКА!!!", style="counter"), zorder=30, tag="txt", layer="overlay")
            renpy.pause(3.2, hard=True)
            renpy.hide("txt")

        # Инициализация битвы
        def init(self, alarm=1, continued=False):
            renpy.choice_for_skipping()
            self.world.battle = self
            for enemy in self.enemy:
                if enemy is not None:
                    enemy.battle = self
            self.party.player.status_reset()

            store._rollback = False
            store._save = False
            store._autosave = False

            for enemy in self.enemy:
                if enemy is not None:
                    for sprite in enemy.sprite:
                        renpy.start_predict(sprite)
            renpy.start_predict_screen("hp")

            if alarm:
                renpy.music.stop(fadeout=1)
                renpy.music.play("bgm/battlestart.ogg", loop=False)

            renpy.show(self.background)

            for enemy in self.enemy:
                if enemy is not None:
                    renpy.show(enemy.sprite[0], at_list=[enemy.position], zorder=5)
            renpy.with_statement(dissolve)

            self.party.player.level_sin = self.party.player.level

            for enemy in self.enemy:
                if enemy is not None:
                    enemy.life = enemy.max_life
                    enemy.tukix = 0.55
                    enemy.tukiy = 0.3
                    enemy.wind = 0
                    enemy.earth = 0
                    enemy.aqua = 0
                    enemy.fire = 0
                    enemy.holy = 0
                    enemy.wind_turn = 0
                    enemy.earth_turn = 0
                    enemy.aqua_turn = 0
                    enemy.fire_turn = 0
                    enemy.holy_turn = 0

            self.party.player.calc()

            if self.world.afterend or not continued:
                self.party.player.life = self.party.player.max_life
                self.party.player.mp = self.party.player.max_mp
                self.party.player.status = 0
                self.party.player.bind = 0
                self.party.player.wind = 0
                self.party.player.holy = 0
                self.party.player.earth = 0
                self.party.player.aqua = 0
                self.party.player.fire = 0
                self.party.player.wind_turn = 0
                self.party.player.earth_turn = 0
                self.party.player.aqua_turn = 0
                self.party.player.fire_turn = 0
                self.party.player.holy_turn = 0
                self.party.player.daystar = 0
                self.party.player.mp_charge = 0
                self.party.player.surrendered = False
                self.used_daystar = 0
                self.stage = [0] * 20

                if self.party.player.undine_skill[1] < 3:
                    self.party.player.undine_skill[1] = False

            elif continued:
                self.party.player = self.party.player_state

            self.world.afterend = False
            self.party.player.spirit_buff()

            self.support()

            renpy.show_screen("hp")

            if alarm:
                if self.plural_name == 1:
                    narrator("%s появляются!{w=4.5}{nw}" % self.name.name)
                else:
                    narrator("%s появляется!{w=4.5}{nw}" % self.name.name)

            self.musicplay(self.music)

        def support(self):
            if self.party.check(1):
                self.world.actor[1].max_mp = 20+(self.world.actor[1].rep//2)
                self.world.actor[1].mp = self.world.actor[1].max_mp

            if self.party.check(2):
                self.world.actor[2].max_mp = 13+(self.world.actor[2].rep//2)
                self.world.actor[2].mp = self.world.actor[2].max_mp
            elif self.party.check(3):
                self.world.actor[3].max_mp = 15+(self.world.actor[3].rep//2)
                self.world.actor[3].mp = self.world.actor[3].max_mp
            elif self.party.check(4):
                self.world.actor[4].max_mp = 13+(self.world.actor[4].rep//2)
                self.world.actor[4].mp = self.world.actor[4].max_mp
            elif self.party.check(5):
                self.world.actor[5].max_mp = 10+(self.world.actor[5].rep//2)
                self.world.actor[5].mp = self.world.actor[5].max_mp

            if self.party.check(6):
                self.world.actor[6].max_mp = 6+(self.world.actor[6].rep//2)
                self.world.actor[6].mp = self.world.actor[6].max_mp
            if self.party.check(7):
                self.world.actor[7].max_mp = 8+(self.world.actor[7].rep//2)
                self.world.actor[7].mp = self.world.actor[7].max_mp

        # Плохая концовка
        def badend(self, label=None):
            if label:
                renpy.call_in_new_context(f"{self.tag}_{label}")

            renpy.choice_for_skipping()
            persistent.hsean = 0
            store._rollback = True
            renpy.music.stop(fadeout=1)
            renpy.scene()
            renpy.show("bg black")
            renpy.hide_screen("hp")
            _window_hide(auto=True)
            renpy.with_statement(Dissolve(1.0))
            renpy.pause(1.0)

            if _in_replay:
                renpy.end_replay()

            renpy.show_screen("badend", self.enemy[0].sprite[1], self.bad[0], self.bad[1])
            renpy.pause()
            renpy.hide_screen("badend")

            renpy.scene()
            renpy.show("bg black")
            renpy.with_statement(Dissolve(1.0))

            if _in_replay:
                self.end_n = 100

            if self.enemy[0].level:
                self.party.player.level += self.enemy[0].level
                self.party.player.calc()

            # Опции после концовки
            choices = [("", None)] * 4
            choices[0] = ("Переиграть", "restart")
            choices[1] = ("Попросить совет", "advice")
            if not _in_replay:
                choices[2] = ("Изменить сложность", "difficulty")
            if not _in_replay:
                choices[3] = ("В главное меню", "main_menu")
            if _in_replay:
                choices[3] = ("Назад в Монстропедию", "pedia")

            result = renpy.display_menu(choices)

            if result == "restart":
                self.restart()
            elif result == "advice":
                self.advice()
            elif result == "difficulty":
                self.world.change_difficulty()
                self.restart()
            elif result == "main_menu":
                renpy.full_restart()
            elif result == "pedia":
                renpy.end_replay()

        def restart(self):
            self.world.afterend = True
            renpy.jump(f"{self.tag}_start")

        def advice(self):
            renpy.music.play("bgm/irias.ogg")
            renpy.scene()
            renpy.show("bg 001")
            renpy.with_statement(Dissolve(3.0))

            if self.world.micaela_evaluations > 14 and not self.world.rape["micaela"]:
                persistent.micaela_ng_unlock = True

            persistent.count_advices += 1

            if self.ng:
                renpy.call_in_new_context(f"{self.tag}_advice")
            else:
                renpy.call_in_new_context("generic_advice")

            self.restart()

        def skillcount(self, idx, skill):
            if persistent.skills[skill][0] == 0:
                persistent.count_oskill[idx] += 1

            persistent.skills[skill][0] += 1
            self.skill_num = skill

        def count(self, finish):
            persistent.count_end += 1

            if finish[0] == 1:
                persistent.count_vaginal_sex += 1
            elif finish[0] == 2:
                persistent.count_blowjob_special += 1
            elif finish[0] == 3:
                persistent.count_assjob += 1
            elif finish[0] == 4:
                persistent.count_blowjob += 1
            elif finish[0] == 5:
                persistent.count_arm_rubs += 1
            elif finish[0] == 6:
                persistent.count_leg_rubs += 1
            elif finish[0] == 7:
                persistent.count_titsjob += 1
            elif finish[0] == 8:
                persistent.count_anal_sex += 1
            elif finish[0] == 9:
                persistent.count_anal_massage += 1
            elif finish[0] == 10:
                persistent.count_tentacles += 1
            elif finish[0] == 11:
                persistent.count_slime += 1
            elif finish[0] == 12:
                persistent.count_plant += 1
            elif finish[0] == 13:
                persistent.count_wing += 1
            elif finish[0] == 14:
                persistent.count_physical += 1
            elif finish[0] == 15:
                persistent.count_slug += 1
            elif finish[0] == 16:
                persistent.count_binds += 1
            elif finish[0] == 17:
                persistent.count_mounts += 1
            elif finish[0] == 18:
                persistent.count_kiss += 1
            elif finish[0] == 19:
                persistent.count_tails += 1
            elif finish[0] == 20:
                persistent.count_ropes += 1
            elif finish[0] == 21:
                persistent.count_onanism += 1
            elif finish[0] == 22:
                persistent.count_hair_rubs += 1
            elif finish[0] == 23:
                persistent.count_nipple_torture += 1
            elif finish[0] == 24:
                persistent.count_body_massage += 1
            elif finish[0] == 25:
                persistent.count_tickles += 1
            elif finish[0] == 26:
                persistent.count_wind += 1
            elif finish[0] == 27:
                persistent.count_clothes += 1
            elif finish[0] == 28:
                persistent.count_body_massage += 1
            elif finish[0] == 29:
                persistent.count_hug += 1
            elif finish[0] == 30:
                persistent.count_asphyxia += 1
            elif finish[0] == 31:
                persistent.count_suicide += 1
            elif finish[0] == 32:
                persistent.count_other += 1
            elif finish[0] == 33:
                persistent.count_vore += 1
            elif finish[0] == 34:
                persistent.count_sirikoki += 1

            if self.party.player.bind:
                persistent.count_binds += 1

            if self.party.player.status == 1:
                persistent.count_bind_end += 1
            elif self.party.player.status == 2:
                persistent.count_paralyze += 1
            elif self.party.player.status == 4:
                persistent.count_surrender += 1
            elif self.party.player.status == 5:
                persistent.count_charm += 1
            elif self.party.player.status == 6:
                persistent.count_stone += 1
            elif self.party.player.status == 7:
                persistent.count_confuse += 1

            if finish[1] == 1:
                persistent.count_b001 += 1
            elif finish[1] == 2:
                persistent.count_b002 += 1
            elif finish[1] == 3:
                persistent.count_b003 += 1
            elif finish[1] == 4:
                persistent.count_b004 += 1
            elif finish[1] == 5:
                persistent.count_b005 += 1
            elif finish[1] == 6:
                persistent.count_b006 += 1
            elif finish[1] == 7:
                persistent.count_b007 += 1
            elif finish[1] == 8:
                persistent.count_b008 += 1
            elif finish[1] == 9:
                persistent.count_b009 += 1
            elif finish[1] == 10:
                persistent.count_b010 += 1
            elif finish[1] == 11:
                persistent.count_b011 += 1
            elif finish[1] == 12:
                persistent.count_b012 += 1
            elif finish[1] == 13:
                persistent.count_b013 += 1
            elif finish[1] == 14:
                persistent.count_b014 += 1
            elif finish[1] == 15:
                persistent.count_b015 += 1
            elif finish[1] == 16:
                persistent.count_b016 += 1
            elif finish[1] == 17:
                persistent.count_b017 += 1

            if self.skill_num:
                persistent.skills[self.skill_num][1] += 1
