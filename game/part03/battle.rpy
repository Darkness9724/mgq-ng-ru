label bess_ng_start:
    python:
        world.troops[42] = Troop(world)
        world.troops[42].enemy[0] = Enemy(world)

        world.troops[42].enemy[0].name = Character("Бэсс")
        world.troops[42].enemy[0].sprite = ["slimelord st04", "slimelord st02", "slimelord st03", "slimelord st02"]
        world.troops[42].enemy[0].position = center
        world.troops[42].enemy[0].defense = 90
        world.troops[42].enemy[0].evasion = 80
        world.troops[42].enemy[0].max_life = world.troops[42].battle.difficulty(32000, 34000, 36000)
        world.troops[42].enemy[0].henkahp[0] = world.troops[42].battle.difficulty(27000, 28500, 0)
        world.troops[42].enemy[0].henkahp[1] = world.troops[42].battle.difficulty(18000, 19000, 0)
        world.troops[42].enemy[0].henkahp[2] = world.troops[42].battle.difficulty(10000, 10000, 0)
        world.troops[42].enemy[0].power = world.troops[42].battle.difficulty(0, 1, 2)

        world.troops[42].battle.tag = "bess_ng"
        world.troops[42].battle.name = world.troops[42].enemy[0].name
        world.troops[42].battle.background = "bg 097"

        if persistent.difficulty < 3:
            world.party.player.earth_keigen = 15
        else:
            world.party.player.earth_keigen = 5

        if persistent.music:
            world.troops[42].battle.music = 26
        else:
            world.troops[42].battle.music = 3

        world.troops[42].battle.ng = True
        world.troops[42].battle.exp = 1500000
        world.troops[42].battle.exit = "erubetie_join2"

        world.troops[42].battle.init(0)

        world.party.player.skill_set(2)

    world.battle.enemy[0].name "П-подожди!{w}\nПослушай меня!"
    world.battle.enemy[0].name "Я не хотела помогать Чёрной Алисе!{w}\nНо у меня просто не было выбора!"
    l "Мне всё равно на твои оправдания.{w}\nТы не отравишь источник снова!"
    world.battle.enemy[0].name "Э-эрубети?!"

    show erubetie st01 at xy(0.2)
    show slimelord st04 at xy(0.7)

    e ".........."
    world.battle.enemy[0].name "Хмпф!{w} Кидалы..."

    $ world.battle.enemy[0].sprite[0] = "slimelord st01"

    world.battle.enemy[0].name "Ладно!{w}{nw}"

    hide erubetie
    $ world.battle.face(2)

    extend "\nТогда я просто поглощу тебя~{image=note}!"
    world.battle.enemy[0].name "Грандина, дай мне своей силы!"

    $ world.battle.show_skillname("Безмятежный разум")
    play sound "se/aqua2.ogg"
    $ world.summon("grandine st01")

    pause 0.5

    "Бэсс взывает к силе воды и соединяется с мировым потоком!"

    $ world.battle.hide_skillname()
    $ world.battle.enemy[0].aqua = 4
    $ world.battle.enemy[0].aqua_turn = -1

    l "Гхх..."
    "А она не так проста.{w}\nМне следует действовать с осторожностью!"

    $ world.battle.face(1)

    $ world.battle.main()

label bess_ng_main:
    if world.battle.used_daystar and not world.battle.stage[5]:
        call bess_ng_003

    # Изучение очищения
    if world.party.player.poison and not world.party.player.undine_skill[1]:
        $ world.battle.stage[4] += 1

        if world.battle.stage[4] > 2 and world.party.player.life < world.party.player.max_life // 10:
            call learn_refresh

    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()
        elif world.battle.result == 8:
            world.battle.common_support()

label bess_ng_v:
    if not world.battle.stage[3] and not world.battle_skip:
        jump bess_ng_heal

    $ world.event["spring"][1] = True
    $ world.battle.face(3)

    world.battle.enemy[0].name "Нет...{w}\nЯ не могу вот так проиграть..."
    world.battle.enemy[0].name "Я была так близка..."

    play sound "se/syometu.ogg"
    hide slimelord with crash

    "Бэсс запечатывается в безобидный розовую лужу!"

    play sound "se/ero_slime3.ogg"

    "Розовая лужица сбегает через щели в полу пещеры!"

    $ world.battle.victory(1)

label bess_ng_001:
    $ world.battle.stage[1] = 1
    $ world.battle.face(3)

    world.battle.enemy[0].name "Уф...{w}\nТак значит ты можешь разгонять свою магическую силу."
    world.battle.enemy[0].name "Хмммм..."

    $ world.battle.face(1)

    return

label bess_ng_002:
    $ world.battle.stage[2] = 1
    $ world.battle.face(2)

    world.battle.enemy[0].name "Эхе-хе-хе~{image=note}...{w}\nТы весьма силён~{image=note}!"
    l "Рррр..."
    "Она считает всё это лишь безумной игрой?"
    world.battle.enemy[0].name "Но у меня есть ещё способы ослабить тебя..."
    world.battle.enemy[0].name "Грандина, покажи свою истинную силу!"

    $ world.battle.show_skillname("Мор")
    play sound "se/ero_makituki5.ogg"
    $ world.summon("grandine st01")

    if world.party.player.aqua == 2 and world.party.player.wind == 5:
        l "............."

        $ world.battle.show_skillname("Безмятежный Ураган")

        "Меч Луки, сверкая, направляется вперёд и соединяется с токсичной слизью!"

        play sound "se/aqua4.ogg"

        "С порывом бушующего ветра, он сдувает всю слизь обратно!"

        $ world.battle.face(3)

        world.battle.enemy[0].name "А... что?{w}\nТак значит даже это не сработает?..."
        world.battle.enemy[0].name "(Думаю мне следует подождать, пока он не откроется...)"

        l "Тебе никогда не победить меня!{w}\nПросто сдайся, Бэсс!"

        $ world.battle.face(1)

        world.battle.enemy[0].name "Хмпф.{w} Никогда..."
    else:
        "Токсическая жидкость впитывается в Луку!"
        l "Гах!"
        "Только не снова...{w}\nМне нужно как можно быстрее победить её!"

        pause 0.5

        $ world.party.player.status = 9
        $ world.party.player.poison = 1
        $ world.party.player.poison_turn = -1
        $ world.party.player.status_print()

        play sound "se/aqua3.ogg"
        "Лука отравлен!"

    $ world.battle.hide_skillname()

    return

label bess_ng_003:
    $ world.battle.stage[5] = 1
    $ world.battle.face(3)

    world.battle.enemy[0].name "Хья!{w}\nЧто это только что было?!"
    world.battle.enemy[0].name "Хрммм...{w}\nКак бы мне это переконтрить?.."

    $ world.battle.face(1)

    return

label bess_ng_poison:
    world.battle.enemy[0].name "Грандина, покажи свою истинную силу!"

    $ world.battle.show_skillname("Мор")
    play sound "se/ero_makituki5.ogg"
    $ world.summon("grandine st01")

    if world.party.player.aqua == 2 and world.party.player.wind == 5:
        l "............."

        $ world.battle.show_skillname("Безмятежный Ураган")

        "Меч Луки, сверкая, направляется вперёд и соединяется с токсичной слизью!"

        play sound "se/aqua4.ogg"

        "С порывом бушующего ветра, он сдувает всю слизь обратно!"
    else:
        "Токсическая жидкость впитывается в Луку!"

        pause 0.5

        $ world.party.player.status = 9
        $ world.party.player.poison = 1
        $ world.party.player.poison_turn = -1
        $ world.party.player.status_print()
        play sound "se/aqua3.ogg"
        "Лука отравлен!"

    $ world.battle.hide_skillname()

    return

label bess_ng_nopoison:
    $ world.battle.stage[8] = 1
    $ world.battle.face(3)

    world.battle.enemy[0].name "Чёрт...{w} Он даже способен исцелить отравление..."
    world.battle.enemy[0].name "Придётся подождать, пока он не откроется..."
    l "А?"
    "Что она там себе бубнит под нос?{w}\nЯ слишком далеко, чтобы расслышать..."

    if world.party.player.life > world.party.player.life/5:
        $ world.battle.face(2)
    else:
        $ world.battle.face(1)

    return

label bess_ng_aqua:
    world.battle.enemy[0].name "Грандина, дай мне своей силы!"

    $ world.battle.show_skillname("Безмятежный разум")
    play sound "se/aqua2.ogg"
    $ world.summon("grandine st01")

    $ world.battle.enemy[0].aqua = 4
    $ world.battle.enemy[0].aqua_turn = -1

    "Используя силу Грандины, Бэсс успокаивает своё тело и разум!"

    $ world.battle.hide_skillname()

    return

label bess_ng_chargecancel:
    world.battle.enemy[0].name "А вот и не полу-у-у-учится~{image=note}!"

    $ world.battle.counter()
    $ world.battle.show_skillname("Отмена разгона")
    play sound "se/ero_slime2.ogg"

    "Слизь из тела Бэсс выстреливает в ноги Луки, сбивая его концентрацию!"

    # 1: (250, 300), 2: (350, 400), 3: (450, 500)
    $ world.battle.enemy[0].damage = {1: (260, 310), 2: (360, 410), 3: (460, 510)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], raw_damage=True)

    $ world.battle.face(1)

    $ world.battle.enemy[0].attack()

label bess_ng_nostar:
    world.battle.enemy[0].name "Эхе-хе-хе...{w}\nПлохое решение-е-е-е~{image=note}!"

    $ world.battle.counter()
    $ world.battle.show_skillname("Амёбное удержание")
    play sound "se/ero_slime3.ogg"

    "Бэсс сокращает расстояние, обволакивая тело Луки своей слизью!{w}{nw}"

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    extend "\nЛука схвачен Бэсс!"
    l "Мммф?!"
    world.battle.enemy[0].name "Ха-ха-ха...{w}\nЯ заберу тебя с собой~{image=note}!"

    $ world.battle.show_skillname("Денница")

    "Ослепительная звезда обрушивается в ад"

    python:
        world.party.player.skill22a()

        for line in range(3):
            renpy.sound.play("se/damage2.ogg")

            if world.party.player.aqua == 0 and world.party.player.fire == 0:
                world.party.player.damage = 450 + world.party.player.level * 9 / 2
            elif world.party.player.aqua > 0 and world.party.player.fire == 0:
                world.party.player.damage = 600 + world.party.player.level * 6
            elif world.party.player.aqua == 0 and world.party.player.fire > 0:
                world.party.player.damage = 600 + world.party.player.level * 6
            elif world.party.player.aqua > 0 and world.party.player.fire > 0:
                world.party.player.damage = 700 + world.party.player.level * 7
            elif world.party.player.aqua > 0 and world.party.player.skill[3] and world.party.player.fire == 0:
                world.party.player.damage = 750 + world.party.player.level * 6
            elif world.party.player.aqua > 0 and world.party.player.skill[3] and world.party.player.fire > 0:
                world.party.player.damage = 850 + world.party.player.level * 8

            if not line:
                _window_show(auto=True)
                renpy.show_screen("hp")

            world.battle.enemy[0].life_calc()
            world.battle.enemy[0].deal((1000, 1500))

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label bess_ng_a:
    # Реакция на очищение
    if world.party.player.before_action == 101 and not world.battle.stage[8]:
        call bess_ng_nopoison

    # Контра Денницы
    if world.party.player.daystar and world.battle.stage[5]:
        call bess_ng_nostar
        $ world.battle.main()

    # Эликсир
    if world.battle.enemy[0].life < world.battle.enemy[0].max_life // 2 and not world.battle.stage[3] and world.battle.stage[6]:
        call bess_ng_heal
        $ world.battle.main()
    elif world.battle.enemy[0].life < world.battle.enemy[0].max_life // 2 and not world.battle.stage[3] and not world.battle.stage[6]:
        call bess_ng_healcharge
        $ world.battle.main()

    elif world.battle.enemy[0].life < world.battle.enemy[0].max_life * 1//3 and not world.battle.stage[3] and world.battle.stage[6]:
        call bess_ng_heal
        $ world.battle.main()
    elif world.battle.enemy[0].life < world.battle.enemy[0].max_life * 1//3 and not world.battle.stage[3] and not world.battle.stage[6]:
        call bess_ng_healcharge
        $ world.battle.main()

    elif world.battle.enemy[0].life < world.battle.enemy[0].max_life * 2//10 and not world.battle.stage[3] and world.battle.stage[6]:
        call bess_ng_heal
        $ world.battle.main()
    elif world.battle.enemy[0].life < world.battle.enemy[0].max_life * 2//10 and not world.battle.stage[3] and not world.battle.stage[6]:
        call bess_ng_healcharge
        $ world.battle.main()

    # Мор
    if world.battle.enemy[0].life < world.battle.enemy[0].max_life * 2//3 and not world.battle.stage[2]:
        call bess_ng_002
    elif world.battle.stage[2] and not world.party.player.poison and (not world.battle.stage[8] or not world.party.player.aqua or world.party.player.mp < 2):
        call bess_ng_poison
        $ world.battle.main()

    # Реакция на Разгон
    if world.party.player.before_action == 91 and not world.battle.stage[1]:
        call bess_ng_001

    # Обычные атаки и призыв духа
    if world.party.player.bind:
        if world.party.player.bind == 2:
            call bess_ng_a7
            $ world.battle.main()

        $ random = rand().randint(1, 2)

        if random == 1:
            call bess_ng_a6
            $ world.battle.main()
        elif random == 2:
            call bess_ng_a7
            $ world.battle.main()
    else:
        $ world.battle.hide_ct()

    if world.battle.enemy[0].aqua:
        $ random = rand().randint(1, 6)
    else:
        $ random = rand().randint(1, 13)

    if random == 1:
        call bess_ng_a1
        $ world.battle.main()
    elif random == 2:
        call bess_ng_a2
        $ world.battle.main()
    elif random == 3:
        call bess_ng_a3
        $ world.battle.main()
    elif random == 4:
        call bess_ng_a4
        $ world.battle.main()
    elif 4 < random < 7:
        call bess_ng_a5
        $ world.battle.main()
    elif random > 6 and world.battle.delay[0] > 15:
        call bess_ng_aqua
        $ world.battle.main()

label bess_ng_healcharge:
    $ world.battle.stage[6] = 1
    $ world.battle.face(2)

    if world.battle.stage[3]:
        world.battle.enemy[0].name "Все твои действия бессмысленны~{image=note}..."
    else:
        world.battle.enemy[0].name "Я не позволю тебе так просто победить~{image=note}!"

    $ world.battle.show_skillname("Зарядка воды")
    play sound "se/power.ogg"
    "Бэсс заряжает свою магию!"

    $ world.battle.hide_skillname()

    if not world.battle.stage[7]:
        call bess_ng_healchargeb

    $ world.battle.face(1)

    return

label bess_ng_healchargeb:
    $ world.battle.stage[7] = 1

    l "Э?"
    "Что она только что зарядила?{w}\nМожет быть мне стоит уйти в защиту?.."

    return

label bess_ng_heal:
    $ world.battle.stage[3] += 1

    if not world.battle.stage[0]:
        $ world.battle.face(2)
        world.battle.enemy[0].name "Ну что ж~{image=note}!"
    else:
        $ world.battle.face(3)
        world.battle.enemy[0].name "Нет...{w} Я ещё могу сражаться!.."

    $ world.battle.show_skillname("Эликсир")
    play sound "se/aqua.ogg"

    "Бэсс со всей силы поглощает влагу из окружающего воздуха..."

    play sound "se/power.ogg"

    if world.battle.stage[0] and world.party.player.daystar:
        $ world.party.player.skill22x()

    $ world.battle.enemy[0].life_regen(world.battle.enemy[0].max_life // 2)
    $ world.battle.hide_skillname()

    $ world.battle.stage[4] = 2

    if world.battle.stage[0] == 0:
        call bess_ng_heal2
    elif world.battle.stage[0] == 1:
        call bess_ng_heal3

    $ world.battle.face(1)

    return

label bess_ng_heal2:
    $ world.battle.stage[0] = 1

    l "!?"
    "Она вылечилась?!"
    world.battle.enemy[0].name "Эхе-хе-хе-хе~{image=note}...{w}\nТы сражаешься с девушкой-слизью. Чего ты ещё ожидал?"
    l "Дерьмово..."
    "Ну а теперь что делать?{w}\nПродолжать её атаковать?"
    "Ей требуется зарядить этот приём, прежде чем использовать его.{w}\nМожет быть я как-то могу оборвать её зарядку..."

    return

label bess_ng_heal3:
    $ world.battle.stage[0] = 2

    l "Опять?!"
    "Чёрт возьми!{w}\nИ как же мне победить, если она может вот так просто вылечиться?"

    return

label bess_ng_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я подрочу тебе этой слизью~{image=note}.",
        "Только девушка-слизь может так дрочить~{image=note}.",
        "Только девушка-слизь может сжимать твой член таким образом~{image=note}!"
    ])

    $ world.battle.show_skillname("Желейные ручки")
    play sound "se/ero_koki1.ogg"

    "Бэсс хватает член Луки своими склизкими ручками!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.battle.enemy[0].aqua and world.party.player.aqua == 2 and random <= 65:
            world.party.player.aqua_guard()
        elif not world.battle.enemy[0].aqua and world.party.player.aqua == 2 and random <= 100:
            world.party.player.aqua_guard()

        elif world.battle.enemy[0].aqua and world.party.player.aqua > 2 and random <= 20:
            world.party.player.aqua_guard()
        elif not world.battle.enemy[0].aqua and world.party.player.aqua > 2 and random <= 50:
            world.party.player.aqua_guard()

        # 1: (290, 320), 2: (340, 370), 3: (390, 420)
        world.battle.enemy[0].damage = {1: (300, 330), 2: (350, 380), 3: (400, 430)}

        if world.battle.enemy[0].aqua:
            if (world.party.player.wind or world.party.player.holy) and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
                world.party.player.wind_guard()
                renpy.return_statement()
            else:
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
        else:
            if world.party.player.wind and world.party.player.wind_guard_calc({1: 30, 2: 30, 3: 30}):
                world.party.player.wind_guard()
                renpy.return_statement()
            elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
                world.party.player.wind_guard()
                renpy.return_statement()
            else:
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"

    "Пенис Луки утопает в её слизистых руках!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"

    "Член Луки входит-выходит из рук Бэсс, пока та яростно дрочит ему!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label bess_ng_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ну как? Твоему члену хорошо тут~{image=note}?",
        "Я изнасилую тебя своими слизистыми сиськами~{image=note}.",
        "Только девушка-слизь может доставить такие приятности~{image=note}!"
    ])

    $ world.battle.show_skillname("Растворяющие пайзури")
    play sound "se/ero_sigoki1.ogg"

    "Бэсс сжимает член Луки меж своих желейных сисек!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.battle.enemy[0].aqua and world.party.player.aqua == 2 and random <= 65:
            world.party.player.aqua_guard()
        elif not world.battle.enemy[0].aqua and world.party.player.aqua == 2 and random <= 100:
            world.party.player.aqua_guard()

        elif world.battle.enemy[0].aqua and world.party.player.aqua > 2 and random <= 20:
            world.party.player.aqua_guard()
        elif not world.battle.enemy[0].aqua and world.party.player.aqua > 2 and random <= 50:
            world.party.player.aqua_guard()

        # 1: (250, 280), 2: (300, 330), 3: (350, 380)
        world.battle.enemy[0].damage = {1: (260, 290), 2: (310, 340), 3: (360, 390)}

        if world.battle.enemy[0].aqua:
            if (world.party.player.wind or world.party.player.holy) and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
                world.party.player.wind_guard()
                renpy.return_statement()
            else:
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
        else:
            if world.party.player.wind and world.party.player.wind_guard_calc({1: 30, 2: 30, 3: 30}):
                world.party.player.wind_guard()
                renpy.return_statement()
            elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
                world.party.player.wind_guard()
                renpy.return_statement()
            else:
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_sigoki1.ogg"

    "Член Луки утопает в её слизистой груди!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_sigoki1.ogg"

    "Бэсс двигает своей грудью вверх-вниз, надрачивая зажатому в ней члену Луки!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label bess_ng_a3:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ха-ха, я сомну твой член свою задницей~{image=note}.",
        "Моя слизь поработит тебя~{image=note}.",
        "Вкуси моей слизистой задницы~{image=note}."
    ])

    $ world.battle.show_skillname("Слизистые бёдра")
    play sound "se/ero_sigoki1.ogg"

    "Бэсс зажимает член Луки в своей заднице!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.battle.enemy[0].aqua and world.party.player.aqua == 2 and random <= 65:
            world.party.player.aqua_guard()
        elif not world.battle.enemy[0].aqua and world.party.player.aqua == 2 and random <= 100:
            world.party.player.aqua_guard()

        elif world.battle.enemy[0].aqua and world.party.player.aqua > 2 and random <= 20:
            world.party.player.aqua_guard()
        elif not world.battle.enemy[0].aqua and world.party.player.aqua > 2 and random <= 50:
            world.party.player.aqua_guard()

        # 1: (260, 290), 2: (310, 340), 3: (360, 390)
        world.battle.enemy[0].damage = {1: (270, 300), 2: (320, 350), 3: (370, 400)}

        if world.battle.enemy[0].aqua:
            if (world.party.player.wind or world.party.player.holy) and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
                world.party.player.wind_guard()
                renpy.return_statement()
            else:
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
        else:
            if world.party.player.wind and world.party.player.wind_guard_calc({1: 30, 2: 30, 3: 30}):
                world.party.player.wind_guard()
                renpy.return_statement()
            elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
                world.party.player.wind_guard()
                renpy.return_statement()
            else:
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_sigoki1.ogg"

    "Член Луки утопает меж её бёдер!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_sigoki1.ogg"

    "Давление внутри её натирает зажатый член Луки!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label bess_ng_a4:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Выдержишь ли ты минет девушки-слизи?",
        "Я попробую на вкус твой пенис~{image=note}.",
        "Твой член растворится в моём рту~{image=note}."
    ])

    $ world.battle.show_skillname("Слизистые минет")
    play sound "se/ero_buchu2.ogg"

    "Бэсс засасывает член Луки в свой рот!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.battle.enemy[0].aqua and world.party.player.aqua == 2 and random <= 65:
            world.party.player.aqua_guard()
        elif not world.battle.enemy[0].aqua and world.party.player.aqua == 2 and random <= 100:
            world.party.player.aqua_guard()

        elif world.battle.enemy[0].aqua and world.party.player.aqua > 2 and random <= 20:
            world.party.player.aqua_guard()
        elif not world.battle.enemy[0].aqua and world.party.player.aqua > 2 and random <= 50:
            world.party.player.aqua_guard()

        # 1: (270, 300), 2: (320, 350), 3: (370, 400)
        world.battle.enemy[0].damage = {1: (280, 310), 2: (330, 360), 3: (380, 410)}

        if world.battle.enemy[0].aqua:
            if (world.party.player.wind or world.party.player.holy) and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
                world.party.player.wind_guard()
                renpy.return_statement()
            else:
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
        else:
            if world.party.player.wind and world.party.player.wind_guard_calc({1: 30, 2: 30, 3: 30}):
                world.party.player.wind_guard()
                renpy.return_statement()
            elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
                world.party.player.wind_guard()
                renpy.return_statement()
            else:
                world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_buchu3.ogg"

    "Её слизистый язык обвивается вокруг члена Луки!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_buchu2.ogg"

    "Мягкий желейный рот Бэсс сжимается вокруг члена Луки, пока она сосёт его!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label bess_ng_a5:
    if world.party.player.surrendered:
        if not world.battle.first[0]:
            world.battle.enemy[0].name "Я поймаю тебя~{image=note}.}"
        else:
            world.battle.enemy[0].name "Поймаю-ка я тебя снова~{image=note}."

    $ world.battle.show_skillname("Амёбное удержание")
    play sound "se/ero_slime3.ogg"

    "Бэсс сокращает расстояние, обволакивая тело Луки своей слизью!{w}{nw}"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.battle.enemy[0].aqua and world.party.player.aqua and random <= 60:
            world.party.player.aqua_guard()
        elif not world.battle.enemy[0].aqua and world.party.player.aqua and random <= 100:
            world.party.player.aqua_guard()

        elif world.battle.enemy[0].aqua and world.party.player.aqua > 2 and random <= 20:
            world.party.player.aqua_guard()
        elif not world.battle.enemy[0].aqua and world.party.player.aqua > 2 and random <= 50:
            world.party.player.aqua_guard()

        if world.party.player.daystar:
            world.party.player.skill22x()

        world.battle.enemy[0].sprite[3] = "slimelord ha1"
        world.party.player.bind = 1
        world.party.player.status_print()

    extend "\nЛука схвачен Бэсс!"

    $ world.battle.hide_skillname()

    if not world.battle.first[0]:
        $ world.battle.first[0] = True

        world.battle.enemy[0].name "Аха-ха-ха, ты застрял~{image=note}.{w}\nТеперь я выжму тебя досуха~{image=note}..."
    else:
        world.battle.enemy[0].name "Аха-ха-ха, ты снова застрял~{image=note}.{w}\nНа этот раз я точно выжму тебя~{image=note}..."

    if world.party.player.earth == 0:
        $ world.battle.enemy[0].power = world.battle.difficulty(3, 3, 4)

    elif world.party.player.earth > 0:
        $ world.battle.enemy[0].power = world.battle.difficulty(1, 1, 2)

    return

label bess_ng_a6:
    if not world.party.player.surrendered:
        "Лука застрял внутри Бэсс..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Время слизи~{image=note}!",
        "Я подразню тебя свою слизью~{image=note}.",
        "Эхе-хе-хе, ну разве моя слизь не хороша~{image=note}."
    ])

    $ world.battle.show_skillname("Амёбный водоворот")
    play sound "se/ero_slime1.ogg"

    "Слизь Бэсс завихряется внутри её тела, пробегаясь по Луке!"

    # 1: (190, 210), 2: (220, 240), 3: (290, 320)
    $ world.battle.enemy[0].damage = {1: (200, 220), 2: (230, 250), 3: (300, 330)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_slime2.ogg"

    "Её слизь зацепляется за член Луки!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_buchu2.ogg"

    "Плотная слизь сильно сжимает член Луки, пока тот находится внутри тела Бэсс!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label bess_ng_a7:
    if not world.party.player.surrendered:
        "Лука застрял внутри Бэсс..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я сожму его медленно и нежно~{image=note}...",
        "Я переварила многих мужчин таким способом~{image=note}...",
        "Хочешь, чтобы я высосала тебя досуха~{image=note}?.."
    ])

    $ world.battle.show_skillname("Амёбное высасывание")
    play sound "se/ero_chupa6.ogg"

    show slimelord hb1
    show slimelord ct01 as ct

    # 1: (190, 210), 2: (220, 240), 3: (290, 320)
    $ world.battle.enemy[0].damage = {1: (200, 220), 2: (230, 250), 3: (300, 330)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], drain=True)

    play sound "se/ero_chupa6.ogg"

    "Слизь вокруг члена Луки сосёт его!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], drain=True)

    play sound "se/ero_chupa6.ogg"

    "Из кожи Луки, где касается её слизь, выходит энергия!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], drain=True)
    $ world.battle.hide_skillname()

    $ world.battle.enemy[0].sprite[3] = "bess_ng_hb1"
    $ world.party.player.bind = 2

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label bess_ng_hpoison:
    jump badend_h

label bess_ng_kousan:
    $ world.battle.common_surrender()

    "Она тут же прибьёт меня, если я сдамся..."

    $ world.battle.main()

label mummy_start:
    python:
        world.troops[43] = Troop(world)
        world.troops[43].enemy[0] = Enemy(world)

        world.troops[43].enemy[0].name = Character("Девушка-мумия")
        world.troops[43].enemy[0].sprite = [
            "mummy st01",
            "mummy st02",
            "mummy st03",
            "mummy st11"
        ]
        world.troops[43].enemy[0].position = center
        world.troops[43].enemy[0].defense = 100
        world.troops[43].enemy[0].evasion = 95
        world.troops[43].enemy[0].max_life = world.troops[43].battle.difficulty(1300, 1625, 1950)
        world.troops[43].enemy[0].power = world.troops[43].battle.difficulty(0, 1, 2)

        world.troops[43].battle.tag = "mummy"
        world.troops[43].battle.name = world.troops[43].enemy[0].name
        world.troops[43].battle.background = "bg 068"
        world.troops[43].battle.music = 1
        world.troops[43].battle.exp = 2000
        world.troops[43].battle.exit = "lb_0069"

        world.troops[43].battle.init()

    world.battle.enemy[0].name "Кто-то проходит испытание...{w}\nЕсли хочешь продолжить, тебе придётся победить меня."
    l "Нет, вообще-то я не прохожу его..."
    world.battle.enemy[0].name "Раз ты не участник, значит — нарушитель.{w}\nВозвращайся сейчас же."
    l "Эм-м...{w}\nТак я тоже не могу."
    world.battle.enemy[0].name "Тогда ты мне не оставляешь выбора..."
    l "............."
    "Походу мне никоим образом не избежать этой битвы.{w}\nХотя она не похожа на плохого монстром..."
    "Но как она и сказала, у меня нет иного выбора!"

    $ world.battle.progress = 1
    $ world.battle.main()

label mummy_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()
        elif world.battle.result == 8:
            world.battle.common_support()

label mummy_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Великолепно... Продолжай..."

    play sound "se/syometu.ogg"
    hide mummy with crash

    "Мумия растворяется, не оставляя после себя ничего кроме бинтов!"

    $ world.battle.victory(1)

label mummy_a:
    if world.party.player.bind:
        $ random = rand().randint(1, 4)

        if random == 1:
            call mummy_a4
            call mummy_a4
            $ world.battle.main()
        elif random == 2:
            call mummy_a5
            call mummy_a5
            $ world.battle.main()
        elif random == 3:
            call mummy_a4
            call mummy_a5
            $ world.battle.main()

    while True:
        $ random = rand().randint(1, 3)

        if random == 1:
            call mummy_a1
            $ world.battle.main()
        elif random == 2:
            call mummy_a2
            $ world.battle.main()
        elif random == 3 and world.battle.delay[0] > 3:
            call mummy_a3
            $ world.battle.main()

label mummy_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я выжму тебя этой рукой...",
        "Пади от моей руки...",
        "Твой провал будет означать провал в испытании..."
    ])

    $ world.battle.show_skillname("Мастурбация мумии")
    play sound "se/ero_koki1.ogg"

    "Девушка-мумия протягивает руку к паху Луки!{w}\nОбвившись своими бинтами вокруг члена Луки, она начинает его поглаживать!"

    $ world.battle.skillcount(1, 3216)

    $ world.battle.enemy[0].damage = {1: (18, 22), 2: (27, 33), 3: (36, 44)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h1")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label mummy_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Твоё тело испытает невиданное доселе удовольствие...",
        "Я сожмусь вокруг тебя...",
        "Я попытаюсь быть нежной..."
    ])

    $ world.battle.show_skillname("Бондаж")
    play sound "se/umaru.ogg"

    "Один из бинтов девушки-мумии устремляется к Луке!{w}\nОн туго обвивается вокруг члена Луки!"

    $ world.battle.skillcount(1, 3217)

    $ world.battle.enemy[0].damage = {1: (20, 25), 2: (30, 37), 3: (40, 50)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h2")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label mummy_a3:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Я заберу твою свободу..."

    $ world.battle.show_skillname("Удержание мумии")
    play sound "se/umaru.ogg"

    "Бинты девушки-мумии кружат вокруг Луки!{w}{nw}"

    $ world.battle.enemy[0].damage = {1: (20, 25), 2: (30, 37), 3: (40, 50)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    extend "{w=.7}\nЛука был опутан бинтами!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h3")

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    if world.battle.first[0]:
        world.battle.enemy[0].name "На этот раз тебе не сбежать..."
    else:
        world.battle.enemy[0].name "Вот теперь я с тобой поиграю..."

    $ world.battle.first[0] = True

    if not world.party.player.earth:
        $ world.battle.enemy[0].power = world.battle.difficulty(1, 2, 3)
    else:
        $ world.battle.enemy[0].power = world.battle.difficulty(0, 1, 2)

    return

label mummy_a4:
    "Лука опутан бинтами!"

    $ world.battle.skillcount(1, 3218)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Выплесни же своё поражение...",
        "Это будет приятно...",
        "Это заставит тебя кончить..."
    ])

    $ world.battle.show_skillname("Удержание бинтами")
    play sound "se/umaru.ogg"

    "Бинты вокруг члена Луки начинают быстро елозить вверх-вниз!"

    $ world.battle.enemy[0].damage = {1: (15, 22), 2: (22, 33), 3: (30, 33)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h4")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label mummy_a5:
    "Лука опутан бинтами!"

    $ world.battle.skillcount(1, 3219)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я сожму каждую из частей твоего тела...",
        "Может быть больно...",
        "Если ты сдашься сейчас, провалишь испытание..."
    ])

    $ world.battle.show_skillname("Кошмарное удержание")
    play sound "se/ero_simetuke1.ogg"

    "Бинты начинают сжиматься сильнее вокруг тела Луки!{w}\nВ том числе вокруг члена!"

    $ world.battle.enemy[0].damage = {1: (17, 23), 2: (25, 34), 3: (34, 46)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h5")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label mummy_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Уже сдаёшься? Значит испытание ты провалил...{w}\nЗа это я высосу тебя досуха прямо здесь..."

    $ world.battle.enemy[0].attack()

label mummy_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Mummy Handjob"
    $ list2 = "Mummy Bandage"
    $ list3 = "Holding Bandage"
    $ list4 = "Holding Nightmare"
    $ list5 = "Mummy Package"

    if persistent.skills[3216][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3217][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3218][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3219][0] > 0:
        $ list4_unlock = 1

    if persistent.skills[3220][0] > 0:
        $ list5_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump mummy_onedari1
    elif world.battle.result == 2:
        jump mummy_onedari2
    elif world.battle.result == 3:
        jump mummy_onedari3
    elif world.battle.result == 4:
        jump mummy_onedari4
    elif world.battle.result == 5:
        jump mummy_onedari5
    elif world.battle.result == -1:
        jump mummy_main


label mummy_onedari1:
    call onedari_syori

    sara "Eh?{w} What!?{w}\nWhat the hell are you doing!?"
    world.battle.enemy[0].name "Be quiet, nuisance..."
    sara "Kya!"

    play sound "se/umaru.ogg"

    "Sara is rolled up in the Mummy Girl's bandages!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "You wish to be defeated by my hand?{w}\nThen I'll do as the fool wants..."

    while True:
        call mummy_a1


label mummy_onedari2:
    call onedari_syori

    sara "Eh?{w} What!?{w}\nWhat the hell are you doing!?"
    world.battle.enemy[0].name "Be quiet, nuisance..."
    sara "Kya!"

    play sound "se/umaru.ogg"

    "Sara is rolled up in the Mummy Girl's bandages!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "You wish to be defeated by my bandages?{w}\nThen I'll do as the fool wants..."

    while True:
        call mummy_a2


label mummy_onedari3:
    call onedari_syori_k

    sara "Eh?{w} What!?{w}\nWhat the hell are you doing!?"
    world.battle.enemy[0].name "Be quiet, nuisance..."
    sara "Kya!"

    play sound "se/umaru.ogg"

    "Sara is rolled up in the Mummy Girl's bandages!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "You wish to be squeezed while bound in my bandages?{w}\nThen I'll do as the fool wants..."

    if world.party.player.bind == 0:
        call mummy_a3

    while True:
        call mummy_a4


label mummy_onedari4:
    call onedari_syori_k

    sara "Eh?{w} What!?{w}\nWhat the hell are you doing!?"
    world.battle.enemy[0].name "Be quiet, nuisance..."
    sara "Kya!"

    play sound "se/umaru.ogg"

    "Sara is rolled up in the Mummy Girl's bandages!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "You wish to be squeezed while bound in my bandages?{w}\nThen I'll do as the fool wants..."

    if world.party.player.bind == 0:
        call mummy_a3

    while True:
        call mummy_a5


label mummy_onedari5:
    call onedari_syori_k

    sara "Eh?{w} What!?{w}\nWhat the hell are you doing!?"
    world.battle.enemy[0].name "Be quiet, nuisance..."
    sara "Kya!"

    play sound "se/umaru.ogg"

    "Sara is rolled up in the Mummy Girl's bandages!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "You wish to be defeated by my ultimate pleasure?{w}\nThen I'll squeeze the fool dry..."

    if world.party.player.bind == 0:
        call mummy_a3

    call mummy_a6


label mummy_h1:
    $ world.party.player.moans(1)
    with syasei1
    show mummy bk03 zorder 10 as bk
    $ world.ejaculation()

    "Stimulated by the Mummy Girl's bandage-covered hand, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([5, 4])
    $ bad1 = "Luka succumbed to the Mummy Girl's handjob."
    $ bad2 = "Luka was squeezed dry by the Mummy Girl's intense milking."
    $ world.battle.face(2)

    world.battle.enemy[0].name "...You shot it all on my hand.{w}\nYou aren't qualified to continue on..."

    jump mummy_h


label mummy_h2:
    $ world.party.player.moans(1)
    with syasei1
    show mummy bk04 zorder 10 as bk
    $ world.ejaculation()

    "With his whole body being played with by her bandages, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([27, 4])
    $ bad1 = "Luka succumbed to the Mummy Girl's bandages."
    $ bad2 = "Luka was squeezed dry by the Mummy Girl's intense milking."
    $ world.battle.face(2)

    world.battle.enemy[0].name "...You shot it all over my bandages.{w}\nYou aren't qualified to continue on..."

    jump mummy_h


label mummy_h3:
    $ world.battle.lose(3)
    with syasei1
    $ world.ejaculation()

    "While being rolled up in the Mummy Girl's bandages, the feeling of the bandages wrapping around Luka was too much for him as he ejaculates."

    $ world.battle.lose()

    $ persistent.count_bouhatu += 1
    $ world.battle.count([27, 4])
    $ bad1 = "Luka ejaculated as the Mummy Girl wrapped him up."
    $ bad2 = "Luka was squeezed dry by the Mummy Girl's intense milking."

    world.battle.enemy[0].name "Exhausted while being wrapped up... Pathetic...{w}\nYou aren't qualified to continue on..."

    jump mummy_h


label mummy_h4:
    $ world.party.player.moans(1)
    with syasei1
    $ world.ejaculation()

    "Played with by the Mummy Girl's bandages while bound, Luka ejaculates."

    $ world.battle.lose()

    $ world.battle.count([27, 4])
    $ bad1 = "Luka succumbed to the Mummy Girl's bandages while bound."
    $ bad2 = "Luka was squeezed dry by the Mummy Girl's intense milking."

    world.battle.enemy[0].name "...You shot it all out on my bandages.{w}\nYou aren't qualified to continue on..."

    jump mummy_h


label mummy_h5:
    $ world.party.player.moans(1)
    with syasei1
    $ world.ejaculation()

    "Luka shivers as he's strangled by the Mummy Girl's bandages, ejaculating all over her."

    $ world.battle.lose()

    $ world.battle.count([16, 4])
    $ bad1 = "Luka succumbed to the Mummy Girl's holding nightmare."
    $ bad2 = "Luka was squeezed dry by the Mummy Girl's intense milking."

    world.battle.enemy[0].name "...You gave in while being strangled?{w}\nYou aren't qualified to continue on..."

    jump mummy_h


label mummy_h6:
    $ world.battle.lose(3)
    with syasei1
    $ world.ejaculation()

    "The moment Luka was forced into the Mummy Girl's cold vagina, he accidentally ejaculates."

    $ world.battle.lose()

    $ persistent.count_bouhatu += 1
    $ world.battle.count([1, 4])
    $ bad1 = "Luka accidentally ejaculated as soon as he was forced into the Mummy Girl."
    $ bad2 = "Luka was squeezed dry by the Mummy Girl's intense milking."

    world.battle.enemy[0].name "...You're a quick shot."

    show mummy st02
    show mummy bk05 zorder 10 as bk
    with dissolve

    "The Mummy Girl separates from Luka as his semen slowly drips out of her."
    world.battle.enemy[0].name "This is the proof of your surrender...{w}\nA pathetic man like you will go no further..."

    jump mummy_h


label mummy_h7:
    $ world.battle.lose(2)
    with syasei1
    $ world.ejaculation()

    "Luka ejaculates in the Mummy Girl's chilly vagina, unable to endure the overpowering stimulation."

    $ world.battle.lose()

    $ world.battle.count([1, 4])
    $ bad1 = "Luka succumbed to the Mummy Girl's mummy package."
    $ bad2 = "Luka was squeezed dry by the Mummy Girl's intense milking."

    world.battle.enemy[0].name "...Were my insides too much for you?"

    show mummy st02
    show mummy bk05 zorder 10 as bk
    with dissolve

    "The Mummy Girl separates from Luka as his semen slowly drips out of her."
    world.battle.enemy[0].name "This is the proof of your surrender...{w}\nA pathetic man like you will go no further..."

    jump mummy_h


label mummy_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    play sound "se/umaru.ogg"

    "The Mummy Girl's bandages expand and wrap around my hands and feet."

    hide ct
    hide bk
    show mummy h1
    with dissolve

    "Unable to move, I'm suspended in midair."
    "The Mummy Girl grabs my dangling penis."
    l "Ah!"
    "I can feel the warmth of her hand through the bandage.{w}\nThe sudden stimulation from her grab makes me body shake."
    sara "Kya!{w} This bandage...!"

    play sound "se/umaru.ogg"

    "A bandage quickly wraps around Sara's body.{w}\nIn an instant, the Mummy Girl has bound both of us."
    sara "Luka!{w} We have to get away somehow!"
    world.battle.enemy[0].name "It's futile, girl.{w}\nI'll suck up all of this man's energy."
    world.battle.enemy[0].name "After that, I'll suck out all of yours."

    play hseanwave "se/hsean17_sigoki.ogg"

    "The Mummy Girl starts to slowly move her hand up and down."
    "Firmly holding on to my dangling penis, she grips me hard at the base and slowly moves down to the tip."
    "Back and forth, just like she was milking a cow..."
    l "Stop...!"
    "Suspended in midair, my writhing doesn't accomplish anything.{w}\nAll I do is sway back and forth."
    world.battle.enemy[0].name "I'll squeeze everything out of you like this, as if you were mere livestock."
    world.battle.enemy[0].name "I'll let your female companion watch you be milked."
    l "Ahh... Stop..."
    "I keep trying to shake my body as she milks me.{w}\nBut the bandages holding me up don't loosen at all."
    "At this rate, I won't be able to do anything..."
    sara "Luka!{w} You must not come!"
    "Sara stares straight at my pathetic appearance without averting her eyes."
    world.battle.enemy[0].name "It's useless even if you try to endure.{w}\nJust surrender to the stimulation..."
    l "Faaa..."
    "After continuing the milking stimulation for a while longer, she starts to slowly change her movements."
    "She makes a small circle with her thumb and forefinger around the tip of my penis, as her palm tightens around the shaft."
    "Holding me in a new way, she resumes the milking movements."
    "Additionally, Sara is watching everything that's happening...{w}\nThe disgrace causes a sick excitement to rise in me, as I feel an orgasm start to boil up."
    world.battle.enemy[0].name "It looks like you're about to come...{w}\nShow your companion the proof of your failure."
    l "Stop... I'm going to come..."
    "The rhythmic stimulation of her milking is too much for me..."
    sara "Wait, Luka!{w}\nDon't let her make you come!"
    l "I can't hold on...{w} Ahhh!"

    with syasei1
    show mummy h2
    $ world.ejaculation()

    "I finally come due to the Mummy Girl's milking...{w}\nStill hanging in the air, my semen shoots straight down onto the floor."
    "The humiliation of being milked like an animal fills my mind."
    l "Faa..."
    sara "Luka..."
    "Sara watched me surrender to her hand...{w}\nFilled with misery, I relax my body."
    world.battle.enemy[0].name "...The man gave up.{w}\nWell, girl. Isn't your companion pathetic?"
    sara "Wait, Luka!{w} Get a hold of yourself!"
    l "Auuu..."
    "The Mummy Girl grabs onto my penis again.{w}\nShe resumes her milking movements, forcing the leftover semen in my urethra to spill out."
    "In front of Sara, she makes sure that every drop of my disgrace pours onto the floor."
    world.battle.enemy[0].name "Now, I'll squeeze out even more.{w}\nWatch closely, girl... As I squeeze everything out of your companion."

    show mummy h3 with dissolve

    "A bandage expands and wraps around my penis.{w}\nIn an instant, it had tightly wrapped all the way around me."
    l "Aie!{w} What is this?.."
    "A strange sense attacks me everywhere the bandage touches.{w}\nA soft feeling of ecstasy slowly spreads through my waist from the bandages wrapped around my penis."
    world.battle.enemy[0].name "This bandage has energy extraction magic in it.{w}\nWhile giving unparalleled pleasure, it will take everything from you..."
    l "Ahhh!"
    "As the sweet euphoria spreads through my body, it isn't possible for me to resist against the rising desire."
    "Shaking my waist pointlessly in midair, I give in to the pleasure."

    with syasei1
    show mummy h4
    $ world.ejaculation()

    l "Faaa..."
    "Awash in pleasure, I give in to the orgasm.{w}\nBut my ejaculation doesn't feel like the last ones."
    "Instead of spurts, it feels like something is continually flowing out of me."
    "The accompanying pleasure was anything but ordinary, too."
    l "Faaa... This is amazing..."
    sara "Stop, Luka!{w}\nDon't give in!{w} Please fight against it!!"
    "Awash in the pleasure, I faintly hear Sara's voice.{w}\nShe's closely watching my miserable appearance..."
    "But even then, I can't stop it..."

    with syasei1
    $ world.ejaculation()

    l "Ahh... I came again..."
    sara "Stop!{w} You can't keep coming like that!{w}\nYou have to resist somehow!"
    world.battle.enemy[0].name "It's useless, girl.{w}\nHuman males are helpless when you stimulate them like this.{w}\nHaven't you already understood this from watching him?"
    l "Faaa..."

    with syasei1
    $ world.ejaculation()

    "Drowning in the pleasure from her strange bandage, I orgasm one after another."
    "Sara's eyes turn from horror into disgust at me."
    sara "Letting a monster do that to you!{w} You're the worst!"
    l "But this... Ahhh!"
    "Another sweet discharge feeling comes over me.{w}\nBut it isn't semen...{w}\nUnable to control myself because of the pleasure, I was peeing onto the floor."
    l "Auuu..."
    world.battle.enemy[0].name "...Unable to control yourself because of the pleasure.{w}\nYou've reached a new low, boy."
    sara "You're... the worst..."
    "Sara stays silent after muttering that.{w}\nNot saying anything more, she just stares at me with hate and contempt."
    l "Faaa..."

    with syasei1
    $ world.ejaculation()

    "But the pleasure doesn't stop.{w}\nMore semen shoots out of me onto the floor, mixing with the urine."
    "Fatigue starts to spread through my body.{w}\nThe more I ejaculate, the more my thoughts become hazy."
    l "Auuu..."
    world.battle.enemy[0].name "Are you feeling faint?..{w}\nI'm squeezing out your life itself, along with your semen..."
    world.battle.enemy[0].name "That's the fate of one who fails the trial.{w}\nHaving their life slowly squeezed out and spilled onto the Pyramid floor..."
    l "Faaa..."

    with syasei1
    $ world.ejaculation()

    "Unable to feel anything but the ecstasy, I start to lose consciousness.{w}\nSuspended in midair, it's as if I'm already just a spirit..."

    with syasei1
    $ world.ejaculation()
    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Squeezed dry, the proof of my life was all over the Pyramid's floor."
    "This is the end of one who failed the trial...{w}\nMy life, as well as my adventure, ends here."
    ".................."

    jump badend


label kobura_start:
    python:
        world.troops[44] = Troop(world)
        world.troops[44].enemy[0] = Enemy(world)

        world.troops[44].enemy[0].name = Character("Девушка-кобра")
        world.troops[44].enemy[0].sprite = [
            "kobura st01",
            "kobura st02",
            "kobura st03",
            "kobura h1"
        ]
        world.troops[44].enemy[0].position = center
        world.troops[44].enemy[0].defense = 100
        world.troops[44].enemy[0].evasion = 95
        world.troops[44].enemy[0].max_life = world.troops[44].battle.difficulty(1400, 1600, 1800)
        world.troops[44].enemy[0].power = world.troops[44].battle.difficulty(0, 1, 2)

        world.troops[44].battle.tag = "kobura"
        world.troops[44].battle.name = world.troops[44].enemy[0].name
        world.troops[44].battle.background = "bg 068"
        world.troops[44].battle.music = 1
        world.troops[44].battle.exp = 2200
        world.troops[44].battle.exit = "lb_0070"

        world.troops[44].battle.init()

    world.battle.enemy[0].name "Согласно приказу Сфинкса, я не могу пропустить слабаков дальше.{w}\nЯ сожру тебя."
    "Сожру?{w} Как ужасно!"
    l "Ух-х, вообще-то я..."
    "... Здесь не ради испытания.{w}\nНо не думаю, что девушку-кобру это заботит..."

    $ world.battle.main()

label kobura_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()
        elif world.battle.result == 8:
            world.battle.common_support()

label kobura_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Как я могла... вот так проиграть?!"

    play sound "se/syometu.ogg"
    hide kobura with crash

    "Девушка-кобра превращается в обычную кобру!"

    $ world.battle.victory(1)

label kobura_a:
    if world.party.player.bind == 2:
        call kobura_a8
        $ world.battle.main()

    elif world.party.player.bind == 1:
        $ random = rand().randint(1, 4)

        if random == 1:
            call kobura_a5
            call kobura_a5
            $ world.battle.main()
        elif random == 2:
            call kobura_a6
            call kobura_a6
            $ world.battle.main()
        elif random == 3:
            call kobura_a5
            call kobura_a6
            $ world.battle.main()
        elif random == 4:
            call kobura_a7
            $ world.battle.main()

    while True:
        $ random = rand().randint(1, 4)

        if random == 1:
            call kobura_a1
            $ world.battle.main()
        elif random == 2:
            call kobura_a2
            $ world.battle.main()
        elif random == 3 and world.party.player.status == 0 and world.battle.delay[0] > 3:
            call kobura_a3
            $ world.battle.main()
        elif random == 4 and world.battle.delay[1] > 4:
            call kobura_a4
            $ world.battle.main()


label kobura_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Это приятно, когда я сосу?..",
        "Сдайся моему рту...",
        "Я испью твои соки поражения...",
    ])

    $ world.battle.show_skillname("Минет кобры")
    play sound "se/ero_buchu3.ogg"

    "Девушка-кобра кладёт член Луки в рот!"

    $ world.battle.skillcount(1, 3222)

    $ world.battle.enemy[0].damage = {1: (20, 23), 2: (26, 30), 3: (36, 42)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h1")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label kobura_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Выплесни своё семя на мою грудь!",
        "Я победю тебя таким способом...",
        "Ни один мужчина не устоит от этого...",
    ])

    $ world.battle.show_skillname("Пайзури кобры")
    play sound "se/ero_koki1.ogg"

    "Девушка-кобра сжимает член Луки своей грудью!"

    $ world.battle.skillcount(1, 3223)

    $ world.battle.enemy[0].damage = {1: (22, 25), 2: (29, 34), 3: (40, 46)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h2")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label kobura_a3:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "I'll give you some venom..."

    $ world.battle.show_skillname("Cobra Bite")

    "Девушка-кобра кусает Луку!{w}{nw}"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    if world.party.player.wind:
        $ world.party.player.wind_guard()
        return

    extend "{w=.7}\nЯд распространяется по телу Луки!"

    $ world.battle.skillcount(1, 3224)

    $ world.party.player.status = 2
    $ world.party.player.status_print()

    "Лука парализован!"

    $ world.battle.hide_skillname()

    l "У-у-у... а-а-ах..."
    world.battle.enemy[0].name "Не волнуйся, он не убивает мою добычу.{w}\nТолько парализует на некоторое время.."
    world.battle.enemy[0].name "Хе-хе... и этого времени предостаточно, чтобы поиграть с жертвой."

    if persistent.difficulty == 1:
        $ world.party.player.status_turn = rand().randint(3, 4)
    elif persistent.difficulty == 2:
        $ world.party.player.status_turn = rand().randint(4, 5)
    elif persistent.difficulty == 3:
        $ world.party.player.status_turn = rand().randint(5, 6)

    return

label kobura_a4:
    if not world.party.player.surrendered:
        if not world.battle.first[0]:
            world.battle.enemy[0].name "Я обовью тебя..."
        else:
            world.battle.enemy[0].name "Я обовьюсь вокруг тебя снова..."

    $ world.battle.show_skillname("Окутывание кобры")
    play sound "se/ero_simetuke1.ogg"

    "Девушка-кобра обвивает Луку!{w}\nОна буквально душит его!"

    $ world.battle.enemy[0].damage = {1: (17, 21), 2: (21, 27), 3: (30, 38)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Девушка-кобра обвила Луку!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h3")

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    if not world.battle.first[0]:
        world.battle.enemy[0].name "Хе-хе... поймала.{w}\nЯ буду пытать тебя нежно, доставляя удовольствие..."
    else:
        world.battle.enemy[0].name "На этот раз я не позволю тебе сбежать...{w}\nЕщё ни одна добыча не сбегала от меня..."

    $ world.battle.first[0] = True

    if not world.party.player.earth:
        $ world.battle.enemy[0].power = world.battle.difficulty(
            rand().randint(3, 4),
            rand().randint(4, 5),
            rand().randint(5, 6)
        )
    else:
        $ world.battle.enemy[0].power = world.battle.difficulty(
            rand().randint(1, 2),
            rand().randint(2, 3),
            rand().randint(3, 4)
        )

    return

label kobura_a5:
    "Девушка-кобра обвивает Луку!"

    $ world.battle.skillcount(1, 3225)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я сожму тебя ещё сильнее...",
        "Тебе нравится боль?",
        "Хочу смотреть на агонию от того, как я душу тебя..."
    ])

    $ world.battle.show_skillname("Кошмарные тиски")
    play sound "se/ero_simetuke1.ogg"

    "Хвост девушки-кобры сдавливает органы Луки!"

    $ world.battle.enemy[0].damage = {1: (20, 23), 2: (26, 30), 3: (36, 42)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h4")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label kobura_a6:
    "Девушка-кобра обвивает Луку!"

    $ world.battle.skillcount(1, 3226)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хочешь, чтобы я сжала твой член ещё сильнее?",
        "Ха-ха... я поиграю и с ним тоже...",
        "Хе-хе... что-то уже вытекает."
    ])

    $ world.battle.show_skillname("Мягкий кончик")
    play sound "se/ero_koki1.ogg"

    "Кончик хвоста кобры опутывает член Луки!"

    $ world.battle.enemy[0].damage = {1: (22, 25), 2: (29, 34), 3: (40, 46)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h5")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label kobura_a7:
    if not world.party.player.surrendered:
        "Девушка-кобра обвивает Луку!"
        world.battle.enemy[0].name "Пришло время заканчивать...{w}\nСейчас будет сюрприз..."

    $ world.battle.skillcount(1, 3227)
    $ world.battle.show_skillname("Анальные игры кобры")
    play sound "se/ero_pyu4.ogg"

    "Кончик хвоста девушки-кобры подкрадывается к сфинктеру Луки!{w}{nw}"

    $ world.party.player.bind = 2
    $ world.party.player.status_print()

    extend "{w=.7}\nИ резко проникает прямиком внутрь!"

    $ world.battle.deal((35, 40), raw_damage=True)
    $ world.battle.hide_skillname()

    if world.battle.party.player == 0:
        $ world.battle.badend("h6")

    l "Ах-х-х!"
    world.battle.enemy[0].name "Хе-хе... тебе приятно, когда тебя насилуют?{w}\nПозволь мне дать тебе ещё больше этого чувства..."

    return

label kobura_a8:
    $ world.battle.skillcount(1, 3227)

    "Луку насилуют!"

    if world.party.player.surrendered:
        $ world.battle.cry(world.battle.enemy[0].name, [
            "Ха-ха... счастлив? Я делаю то, о чём ты так просил...",
            "Требовать, чтобы тебя так насиловали... да ты настоящий извращенец.",
            "Я заставлю тебя кончить, как ты и хотел...",
        ])
    else:
        $ world.battle.cry(world.battle.enemy[0].name, [
            "Выплесни всё прямо на меня...",
            "Хе-хе... это слабое место всех мужчин.",
            "Кончить из-за такого...",
        ])

    $ world.battle.show_skillname("Анальные игры кобры")
    play sound "se/ero_chupa3.ogg"

    $ world.battle.cry([
        "Хвост кобры стимулирует простату Луки!",
        "Хвост кобры щекочек анус Луки!",
        "Хвост кобры проникает вглубь ануса Луки!",
    ], True)

    $ world.battle.enemy[0].deal((35, 40), raw_damage=True)

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h7")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label kobura_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "I'll eat her later...{w}\nFirst I'll enjoy you..."

    jump kobura_a


label kobura_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Cobra Blowjob"
    $ list2 = "Cobra Tit Fuck"
    $ list3 = "Cobra Bite"
    $ list4 = "Nightmare Tightening"
    $ list5 = "Mellow Tail"
    $ list6 = "Cobra Anal"

    if persistent.skills[3222][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3223][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3224][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3225][0] > 0:
        $ list4_unlock = 1

    if persistent.skills[3226][0] > 0:
        $ list5_unlock = 1

    if persistent.skills[3227][0] > 0:
        $ list6_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump kobura_onedari1
    elif world.battle.result == 2:
        jump kobura_onedari2
    elif world.battle.result == 3:
        jump kobura_onedari3
    elif world.battle.result == 4:
        jump kobura_onedari4
    elif world.battle.result == 5:
        jump kobura_onedari5
    elif world.battle.result == 6:
        jump kobura_onedari6
    elif world.battle.result == -1:
        jump kobura_main


label kobura_onedari1:
    call onedari_syori

    sara "Wait, Luka!{w} What are you doing!?"
    world.battle.enemy[0].name "Hehe... The man gave up.{w}\nYou don't stand a chance little girl!"
    sara "Guh... Damn it... Kya!"

    play sound "se/ero_simetuke1.ogg"

    "The Cobra Girl's tail coils around Sara and strangles her!{w}\nAfter Sara faints, the Cobra Girl tosses her body away!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "I'll eat her later...{w}\nFirst I'll enjoy you..."
    world.battle.enemy[0].name "So you want me to play with you with my mouth?{w}\nThen let me drink it all..."

    while True:
        call kobura_a1


label kobura_onedari2:
    call onedari_syori

    sara "Wait, Luka!{w} What are you doing!?"
    world.battle.enemy[0].name "Hehe... The man gave up.{w}\nYou don't stand a chance little girl!"
    sara "Guh... Damn it... Kya!"

    play sound "se/ero_simetuke1.ogg"

    "The Cobra Girl's tail coils around Sara and strangles her!{w}\nAfter Sara faints, the Cobra Girl tosses her body away!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "I'll eat her later...{w}\nFirst I'll enjoy you..."
    world.battle.enemy[0].name "So you want me to play with you with my chest?{w}\nThen make sure you cover me in the proof of your surrender..."

    while True:
        call kobura_a2


label kobura_onedari3:
    call onedari_syori

    sara "Wait, Luka!{w} What are you doing!?"
    world.battle.enemy[0].name "Hehe... The man gave up.{w}\nYou don't stand a chance little girl!"
    sara "Guh... Damn it... Kya!"

    play sound "se/ero_simetuke1.ogg"

    "The Cobra Girl's tail coils around Sara and strangles her!{w}\nAfter Sara faints, the Cobra Girl tosses her body away!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "I'll eat her later...{w}\nFirst I'll enjoy you..."
    world.battle.enemy[0].name "Hehe... You want to be made numb by my venom?{w}\nPlayed with while unable to move... What a miserable request."

    call kobura_a3
    jump kobura_a


label kobura_onedari4:
    call onedari_syori_k

    sara "Wait, Luka!{w} What are you doing!?"
    world.battle.enemy[0].name "Hehe... The man gave up.{w}\nYou don't stand a chance little girl!"
    sara "Guh... Damn it... Kya!"

    play sound "se/ero_simetuke1.ogg"

    "The Cobra Girl's tail coils around Sara and strangles her!{w}\nAfter Sara faints, the Cobra Girl tosses her body away!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "I'll eat her later...{w}\nFirst I'll enjoy you..."
    world.battle.enemy[0].name "You want me to strangle you?{w}\nLike that girl, I'll squeeze you until you faint..."

    if world.party.player.bind == 0:
        call kobura_a4

    while True:
        call kobura_a5


label kobura_onedari5:
    call onedari_syori_k

    sara "Wait, Luka!{w} What are you doing!?"
    world.battle.enemy[0].name "Hehe... The man gave up.{w}\nYou don't stand a chance little girl!"
    sara "Guh... Damn it... Kya!"

    play sound "se/ero_simetuke1.ogg"

    "The Cobra Girl's tail coils around Sara and strangles her!{w}\nAfter Sara faints, the Cobra Girl tosses her body away!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "I'll eat her later...{w}\nFirst I'll enjoy you..."
    world.battle.enemy[0].name "Hehe... You want me to strangle your penis?{w}\nUntil you leak in surrender, I'll tighten around it..."

    if world.party.player.bind == 0:
        call kobura_a4

    while True:
        call kobura_a6


label kobura_onedari6:
    call onedari_syori_k

    sara "Wait, Luka!{w} What are you doing!?"
    world.battle.enemy[0].name "Hehe... The man gave up.{w}\nYou don't stand a chance little girl!"
    sara "Guh... Damn it... Kya!"

    play sound "se/ero_simetuke1.ogg"

    "The Cobra Girl's tail coils around Sara and strangles her!{w}\nAfter Sara faints, the Cobra Girl tosses her body away!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "I'll eat her later...{w}\nFirst I'll enjoy you..."
    world.battle.enemy[0].name "Even though you're a man, wanting something like that...{w}\nAlright... I'll make you writhe in the pleasure!"

    if world.party.player.bind == 0:
        call kobura_a4

    call kobura_a7

    while True:
        call kobura_a8


label kobura_h1:
    $ world.party.player.moans(1)
    with syasei1
    show kobura bk01 zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the Cobra Girl's mouth, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([4, 4])
    $ bad1 = "Luka succumbed to the Cobra Girl's blowjob."
    $ bad2 = "Raped like that, Luka was forced to ejaculate until death."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Mmm... You shot out so much in my mouth.{w}\nHow shameful... Haha."

    jump kobura_h


label kobura_h2:
    $ world.party.player.moans(1)
    with syasei1
    show kobura bk02 zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the Cobra Girl's breasts crushing his penis, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([7, 4])
    $ bad1 = "Luka succumbed to the Cobra Girl's tit fuck."
    $ bad2 = "Raped like that, Luka was forced to ejaculate until death."
    $ world.battle.face(2)

    world.battle.enemy[0].name "You shot out so much on my breasts...{w}\nHow shameful... Haha."

    jump kobura_h


label kobura_h3:
    $ world.battle.lose(3)
    with syasei1
    $ world.ejaculation()

    "While being coiled by the Cobra Girl, Luka ejaculates!"

    $ world.battle.lose()

    $ persistent.count_bouhatu += 1
    $ world.battle.count([16, 4])
    $ bad1 = "Luka ejaculated as the Cobra Girl coiled around him."
    $ bad2 = "Raped like that, Luka was forced to ejaculate until death."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Oh... Leaking just from being coiled.{w}\nSuch a pathetic man..."

    jump kobura_h


label kobura_h4:
    $ world.party.player.moans(1)
    with syasei1
    $ world.ejaculation()

    "Squeezed by the Cobra Girl, Luka ejaculates."

    $ world.battle.lose()

    $ world.battle.count([16, 4])
    $ bad1 = "Luka succumbed to the Cobra Girl's nightmare tightening."
    $ bad2 = "Raped like that, Luka was forced to ejaculate until death."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Haha... Having an orgasm just from being strangled.{w}\nIn fear for your life, was your sexual nature awakened?"

    jump kobura_h


label kobura_h5:
    $ world.party.player.moans(1)
    with syasei1
    $ world.ejaculation()

    "Unable to endure the Cobra Girl's tail tightening around his penis, Luka ejaculates."

    $ world.battle.lose()

    $ world.battle.count([19, 4])
    $ bad1 = "Luka succumbed to the Cobra Girl's tailjob."
    $ bad2 = "Raped like that, Luka was forced to ejaculate until death."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Shooting out your semen all over my tail...{w}\nSuch a shameful boy, Haha."

    jump kobura_h


label kobura_h6:
    l "Aiee! Ahhhh!!"

    with syasei1
    $ world.ejaculation()

    "The moment the Cobra Girl's tail went into Luka, he ejaculates!"

    $ world.battle.lose()

    $ persistent.count_bouhatu += 1
    $ world.battle.count([9, 4])
    $ bad1 = "Luka exploded on accident as soon as the tail went inside him."
    $ bad2 = "Raped like that, Luka was forced to ejaculate until death."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Oh... Already?{w}\nDid it feel too good just to be violated?"

    jump kobura_h


label kobura_h7:
    l "Aiee! Ahhhh!!"

    with syasei1
    $ world.ejaculation()

    "While violated by her tail, Luka ejaculates!"

    $ world.battle.lose()

    $ world.battle.count([9, 4])
    $ bad1 = "Luka succumbed to the Cobra Girl's anal violation."
    $ bad2 = "Raped like that, Luka was forced to ejaculate until death."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hehe... Feeling so good from anal.{w}\nEven though you're a man... How pathetic."

    jump kobura_h


label kobura_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if not world.party.player.bind > 0:
        play sound "se/ero_simetuke1.ogg"
        hide bk
        hide ct
        show kobura h1 with dissolve

        "The Cobra Girl's tail coils around my body.{w}\nLike that, she pulls me right next to her."
        l "Guh... Get off!"

    sara "L...Luka!{w} Kyaa!"

    play sound "se/ero_simetuke1.ogg"

    "The Cobra Girl's tail expands and grabs Sara as well.{w}\nLike that, both Sara and I are coiled in her long tail."
    sara "Ahhh!!"
    "The Cobra Girl's tail tightens around Sara as she screams.{w}\nIt seems as though she's trying to kill Sara!"
    l "Stop!!{w}\nIf you have to do that, do it to me!"
    world.battle.enemy[0].name "Oh... What a brave thing to say.{w}\nHehe... I just got an interesting idea for a brave man like you."
    "The Cobra Girl softly whispers in my ear as she brings me closer.{w}\nA cruel flash crosses her eyes."
    world.battle.enemy[0].name "I'll accept your kind offer of replacing the girl...{w}\nBut... If you can't endure it, I'll do it to the girl instead."
    world.battle.enemy[0].name "That is... The more you endure, the less pain the girl will take."
    l "I... I understand!"
    "Stuck like this, I don't have any choice but to agree...{w}\nAt the very least, I have to let Sara get out of this somehow!"
    world.battle.enemy[0].name "Then I'll start strangling you...{w}\nBut I'll do it with this instead..."
    l "Eh?.."
    "Her tail brings me right up next to her.{w}\nA dripping wet hole opens up right where her tail meets the human half of her."
    "The tail wrapped around me forces my penis into the Cobra Girl!"

    play hseanwave "se/hsean04_innerworks_a4.ogg"
    show kobura h2
    show kobura ct01 at left zorder 15 as ct
    with dissolve

    "In an instant, my penis is forced into the Cobra Girl!"
    l "Ahhh! {w} What are you doing!?"
    "Surprisingly hot, the narrow walls inside of her clamp down on me.{w}\nThe stimulation from that tightness was overwhelming."
    world.battle.enemy[0].name "I'll strangle your penis like this...{w}\nAren't you glad?{w} You get pleasure while she gets pain..."
    l "Auuu!"
    "My body shivers as the Cobra Girl's pussy slowly gets even tighter around me."
    world.battle.enemy[0].name "Don't you remember what I said?{w}\nIf you can't endure this, the girl will suffer."
    world.battle.enemy[0].name "If you ejaculate from this...{w}\nMy tail will get even tighter around this girl."
    "The part of the tail around Sara slightly moves, causing Sara to groan."
    l "Stop!"
    world.battle.enemy[0].name "If you want me to stop, you just need to endure this.{w}\nNow, I'll make it a little tighter..."
    "Saying that, the fleshy walls of her vagina get even narrower.{w}\nSqueezing my penis from all sides, the stimulation becomes even more overwhelming."
    l "Ahhhh!!"
    "I yell out at her renewed attack on me."
    world.battle.enemy[0].name "What a nice scream...{w}\nIt doesn't look like you'll be able to endure, does it?"
    l "Uaaa...{w} Sara..."
    "I clench my stomach as I try to hold on.{w}\nFocusing my mind on anything but what's happening to me, I try everything I can think of to endure."
    "But no matter what I do, the tingling feeling continues to spread through my body."
    world.battle.enemy[0].name "I can make it even tighter you know...{w}\nI wonder how loud you'll scream this time?"
    l "Auu..."
    "The Cobra Girl squeezes down on the base of my penis at the entrance, and slowly starts to tighten up to the tip."
    "Once the slow wave of tightness finished, the muscles inside of her start to rapidly tighten and loosen, as if vibrating."
    l "Uaaa! What is this!?"
    sara "Lu...ka..."
    "While being squeezed by the Cobra Girl's tail, Sara lets out a hoarse voice."
    "I have to hold on for her sake!"
    l "Urhg... I can't..."
    world.battle.enemy[0].name "Hehe... It looks like you're in so much pain.{w}\nDon't worry, I can help you release it."
    world.battle.enemy[0].name "Well? Don't you want me to make it even better?{w}\nAs long as you don't ejaculate, that girl will be fine."

    play hseanwave "se/hsean06_innerworks_a6.ogg"

    "The vibrating muscles inside of her start to move with even more power."
    "As if another snake was inside of her coiling around me, the desire to release flows through my body as my vision starts to go white."
    "The desperation of my body to release in her is slowly breaking down my mind."
    l "Auuuu...{w}\nI'm... Going to..."
    world.battle.enemy[0].name "Already admitting defeat? What a pathetic Hero.{w}\nNow, surrender to me!"
    l "Ahhhh!!"
    "As if giving me her finishing blow, she tightens around me one final time as her vibrating muscles intensity doubles."
    "Unable to hold on anymore, I finally surrender to the desire."

    with syasei1
    show kobura h3
    show kobura ct02 at left zorder 15 as ct
    $ world.ejaculation()

    l "Ahhh..."
    "With my resistance shattered, I explode in the Cobra Girl.{w}\nBecause I couldn't hold on, Sara is going to be..."
    world.battle.enemy[0].name "Hehe... Too bad.{w}\nThe pleasure you couldn't endure will be transferred to the girl as pain!"
    sara "Ahhh!"
    "As the tail tightens around Sara's thin body, she cries out."
    sara "Ahhhhhhhhhhhhh!!"
    "Her pain filled voice echoes around the narrow hall of the Pyramid.{w}\nBecause I couldn't hold on, she's in so much pain..."
    "The guilt hangs over me as I listen to her."
    world.battle.enemy[0].name "Hehe... She looks like she's in pain.{w}\nBut it isn't over yet."
    world.battle.enemy[0].name "I'll tighten around both of you at the same time.{w}\nYou get pleasure, she gets pain. Haha!"

    play hseanwave "se/hsean07_innerworks_a7.ogg"

    "As she says that, the tail around Sara constricts as her pussy tightens around me."
    l "Auuu!"
    sara "Kyaaaa!!"
    "As Sara screams in pain, I moan in pleasure."
    world.battle.enemy[0].name "Hehe..."
    "The Cobra Girl licks her lips as she watches the two of us writhing around."
    l "S..Sara...{w} Auuu!"
    "The sweet pleasure makes it impossible to focus on Sara."
    world.battle.enemy[0].name "The more you ejaculate, the tighter I'll get around the girl.{w}\nIf you don't want her to die, you better endure."
    l "Ahhh!"
    "Deep inside of the Cobra Girl, her wriggling muscles tighten around the tip of my penis."
    "While squeezing the tip, the rest of her vagina starts to contract and loosen around my penis in a rhythm."
    l "Auuu... S...Sara..."
    sara "Ahh... Luka... Help..."
    "With her voice dripping with agony, Sara begs me for help.{w}\nI have to hold on somehow..."
    "But no matter what I try, this pleasure isn't easily resisted."
    world.battle.enemy[0].name "Shall I force you to come again?{w}\nWallow in your inability to save your companion as you doom her."
    "After she said that, she continues to press the walls of her vagina around my penis, squeezing me even tighter."
    "There's no way I can endure this..."
    sara "It hurts...{w} Help me... Luka..."
    l "Auu... I'm sorry... Sara...{w}\nI'm coming!{w} Ahhhh!"

    with syasei1
    show kobura h4
    show kobura ct03 at left zorder 15 as ct
    $ world.ejaculation()

    "While apologizing to Sara, I yield to the pleasure again as I explode in the Cobra Girl."
    l "Ahhh..."
    "My mind starts to break as Sara's screams of agony mix with the pleasure from my orgasm."
    world.battle.enemy[0].name "Hehe... You came again.{w}\nAs compensation, this girl will be strangled even tighter."
    "I hear the Cobra Girl's tail squeezing even harder on Sara.{w}\nMore than before, the Cobra Girl is putting overwhelming strength into her tail to squeeze her."
    sara "...A...aa..."
    "With the tail so tight around her chest, she can't even raise her voice.{w}\nSara's mouth is agape in an almost silent scream."
    l "Sa...ra..."
    world.battle.enemy[0].name "Oh, she's in a bad state...{w}\nIf it gets any tighter, she may die."
    world.battle.enemy[0].name "In contrast, look at you.{w}\nWatching her die while in such a state of ecstasy."
    l "Auuu..."
    "The muscles in her vagina start to wriggle around me again."
    "If she tightens around Sara any more than that, she will be in serious danger..."
    "Even though I know that, I can't endure this pleasure..."
    l "Ahhhh...{w}\nI'm sorry... Sara... I'm coming!"

    with syasei1
    show kobura h5
    show kobura ct04 at left zorder 15 as ct
    $ world.ejaculation()

    "Quickly yielding again to the pleasure, my body relaxes as I shoot out a death sentence for Sara into the Cobra Girl."
    world.battle.enemy[0].name "Oh, were you even trying to endure that time?{w}\nThe pleasure you surrendered to will cost the girl..."
    sara "...A..."
    "With a final gasp, Sara's body finally goes limp.{w}\nDid she faint?{w} Or...{w} No!"
    world.battle.enemy[0].name "Haha... What happened to your companion?{w}\nWell... She doesn't matter anymore."
    world.battle.enemy[0].name "You already don't care about her, right?{w}\nSince you came so quickly last time, that is.{w}\nYou didn't even try to endure... You put your pleasure in front of her life."
    "Trying to forget about the responsibility for her death, I immerse myself in the ecstasy."
    l "Ahhhh..."
    "Already exhausted, I give my body completely to the pleasure."
    world.battle.enemy[0].name "Hehe... Look how you ended up.{w}\nNow I'll wring the rest out of you."
    world.battle.enemy[0].name "I'll suck everything out of you until there's nothing left!"
    l "Ahh!!"
    "The Cobra Girl's vagina tightens around me again as her muscles continue to vibrate."
    "Different from the tightening pressure before, it seems like now she's just trying to squeeze everything out of me."
    l "Ahhh... Incredible...{w} Ahhh!"

    with syasei1
    $ world.ejaculation()

    "Even as I ejaculate inside of her, the tortuous pleasure she's forcing into me doesn't stop."
    l "Auu... I'm going to come again..."
    world.battle.enemy[0].name "Shoot out more...{w}\nDrown yourself in the pleasure until you die."
    l "Ahhh!"

    with syasei1
    $ world.ejaculation()

    "Each orgasm just makes me feel even more sensitive, increasing the stimulation."
    "Strangled by her soft vagina, my penis rejoices at the pleasure."

    with syasei1
    $ world.ejaculation()

    l "Faaa..."
    "My consciousness starts to fade as I'm forced to orgasm one after another."

    with syasei1
    $ world.ejaculation()

    "Exhausted by the repeated orgasms, I fall into an eternal sleep while awash in ecstasy."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Forced to submit to the pleasure, another adventurer loses their life in the Pyramid."
    "Two adventurers attempted the trial.{w} Neither returned."
    "Thus ends two more lives in the Pyramid..."
    "..................."

    jump badend


label lamias_start:
    python:
        world.troops[45] = Troop(world)
        world.troops[45].enemy[0] = Enemy(world)

        world.troops[45].enemy[0].name = Character("Ламии-нефертити")
        world.troops[45].enemy[0].sprite = [
            "lamias st01",
            "lamias st02",
            "lamias st03",
            "lamias st02"
        ]
        world.troops[45].enemy[0].position = center
        world.troops[45].enemy[0].defense = 100
        world.troops[45].enemy[0].evasion = 95
        world.troops[45].enemy[0].max_life = world.troops[45].battle.difficulty(1700, 1900, 2100)
        world.troops[45].enemy[0].power = world.troops[45].battle.difficulty(0, 1, 2)

        world.troops[45].battle.tag = "lamias"
        world.troops[45].battle.name = world.troops[45].enemy[0].name
        world.troops[45].battle.background = "bg 068"
        world.troops[45].battle.music = 1
        world.troops[45].battle.exp = 2500
        world.troops[45].battle.exit = "lb_0071"
        world.troops[45].battle.plural_name = True

        world.troops[45].battle.init()

    oldest_lamia "Прости, но мы не можем тебя пропустить."
    second_lamia "Сфинкс сказала, что только сильным людям вход разрешён."
    third_lamia "А ещё она сказала, что мы можем полакомиться слабаками~{image=note}!"
    l "................"
    "Никак они не научатся..."

    $ world.battle.main()

label lamias_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()
        elif world.battle.result == 8:
            world.battle.common_support()

label lamias_v:
    $ world.battle.face(3)

    oldest_lamia "Кья!"
    second_lamia "Этот человек... силён!"

    play sound "se/syometu.ogg"
    hide lamias with crash

    "Ламии превращаются в обычных змеек!"

    $ world.battle.victory(1)

label lamias_a:
    if world.party.player.bind == 1:
        $ random = rand().randint(1, 2)

        if random == 1:
            call lamias_a5
            $ world.battle.main()
        elif random == 2:
            call lamias_a6
            $ world.battle.main()

    while True:
        $ random = rand().randint(1, 4)

        if random == 1:
            call lamias_a1
            $ world.battle.main()
        elif random == 2:
            call lamias_a2
            $ world.battle.main()
        elif random == 3:
            call lamias_a3
            $ world.battle.main()
        elif random == 4 and world.battle.delay[0] > 4:
            call lamias_a4
            $ world.battle.main()

label lamias_a1:
    $ random = rand().randint(1, 3)

    if random == 1:
        call lamias_a1a
    elif random == 2:
        call lamias_a1b
    elif random == 3:
        call lamias_a1c

    return

label lamias_a1a:
    oldest_lamia "Сёстры, давайте используем наши руки."
    second_lamia "Хе-хе, просто отдайся своим желаниям!"

    $ world.battle.show_skillname("Сафинский штурвал")

    "Все ламии устремляют свои руки к паху Луки!{w}{nw}"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    if world.party.player.wind:
        $ world.party.player.wind_guard()
        return

    play sound "se/ero_koki1.ogg"

    extend "\nЛамия-малышка нежно массирует мошонку Луки!"

    $ world.battle.skillcount(1, 3228)
    $ world.battle.enemy[0].damage = {1: (5, 7), 2: (7, 10), 3: (10, 14)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_koki1.ogg"

    "Старшая нежно ласкает основание члена!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_koki1.ogg"

    "Средняя ламия щекочет головку!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_koki1.ogg"

    "Младшая нежно сжимает член Луки!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h1")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label lamias_a1b:
    oldest_lamia "Давайте сдалем это одновременно, сёстры!"
    third_lamia "Ха-ха-ха, готовься~{image=note}!"

    $ world.battle.show_skillname("Сафинский штурвал")

    "Все ламии ринулись к члену Луки!"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    if world.party.player.wind:
        $ world.party.player.wind_guard()
        return

    "Старашая делает круг вокруг головки его члена!{w}\nВторая по старшинству берётся за основание!"
    "Третья хватает мошонку Луки!{w}\nА самая младшая — ствол!"
    oldest_lamia "А теперь, все вместе!{w}\nДавайте же, доставим ему удовольствие..."

    play sound "se/ero_koki1.ogg"

    "Четыре ладони начинают одновременно дрочить Луке!"

    $ world.battle.skillcount(1, 3228)

    $ world.battle.enemy[0].damage = {1: (20, 28), 2: (30, 42), 3: (40, 56)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h1")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label lamias_a1c:
    oldest_lamia "Посмотрим кто из нас лучшая..."
    second_lamia "Хе-хе... звучит весело."

    $ world.battle.show_skillname("Сафинский штурвал")

    "Ламии кинулись к члену Луки!"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    if world.party.player.wind:
        $ world.party.player.wind_guard()
        return

    play sound "se/ero_koki1.ogg"

    "Старшая начинает дрочить Луке!{w}{nw}"

    $ world.battle.skillcount(1, 3228)

    $ world.battle.enemy[0].damage = {1: (5, 7), 2: (7, 10), 3: (10, 14)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_koki1.ogg"

    extend "\nВторая играет с его членом!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_koki1.ogg"

    "Третья туго сжимает его!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_koki1.ogg"

    "А самая младшая нежно поглаживает!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h1")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label lamias_a2:
    $ random = rand().randint(1, 3)

    if random == 1:
        oldest_lamia "I want to taste your body..."
        second_lamia "Hahaha! He looks delicious."
    elif random == 2:
        oldest_lamia "I wonder if he can endure our licking?"
        second_lamia "Let's make him go crazy!"
    elif random == 3:
        oldest_lamia "Do you want us to lick all over?"
        second_lamia "I'll make sure to lick your most shameful spots."

    $ world.battle.show_skillname("Групповое облизывание")
    play sound "se/ero_pyu1.ogg"

    "The Lamia's close in on Luka with their tongues out!{w}{nw}"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    if world.party.player.wind:
        $ world.party.player.wind_guard()
        return

    $ random = rand().randint(1, 3)

    if random == 1:
        extend "{w=.7}\nThe oldest licks Luka's nipple!"
    elif random == 2:
        extend "{w=.7}\nThe oldest licks Luka's face!"
    elif random == 3:
        extend "{w=.7}\nThe oldest licks Luka's anus!"

    $ world.battle.skillcount(1, 3229)
    $ world.battle.enemy[0].damage = {1: (5, 7), 2: (6, 9), 3: (9, 13)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_pyu1.ogg"

    $ random = rand().randint(1, 3)

    if random == 1:
        "The second born licks Luka's chest!"
    elif random == 2:
        "The second born licks Luka's nipple!"
    elif random == 3:
        "The second born licks Luka's thighs!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_pyu1.ogg"

    $ random = rand().randint(1, 3)

    if random == 1:
        "The third born licks Luka's stomach!"
    elif random == 2:
        "The third born licks Luka's lips!"
    elif random == 3:
        "The third born licks Luka's face!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_pyu1.ogg"

    $ random = rand().randint(1, 3)

    if random == 1:
        "The youngest licks Luka's bellybutton!"
    elif random == 2:
        "The youngest licks Luka's armpit!"
    elif random == 3:
        "The youngest licks inside Luka's thigh!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h2")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label lamias_a3:
    $ random = rand().randint(1, 3)

    if random == 1:
        oldest_lamia "Do you want to feel our tails?"
        third_lamia "Attack his penis with our tails~{image=note}!"
    elif random == 2:
        oldest_lamia "We'll play with you with our tails."
        third_lamia "If we do it all at once, he doesn't stand a chance.{image=note}"
    elif random == 3:
        oldest_lamia "If we all tighten around it at once, I wonder if he can endure it?"
        third_lamia "Let's see, Hehe.{image=note}"

    $ world.battle.show_skillname("Поглаживание хвостами")
    play sound "se/ero_koki1.ogg"

    "The four Lamia's tails coil around Luka's penis!{w}{nw}"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    if world.party.player.wind:
        $ world.party.player.wind_guard()
        return

    extend "{w=.7}\nFrom the base to the tip, the tails coil around Luka's penis!{w}{nw}"

    $ world.battle.skillcount(1, 3230)

    $ world.battle.enemy[0].damage = {1: (6, 8), 2: (8, 11), 3: (11, 15)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_koki1.ogg"

    "The tails wriggle around!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_koki1.ogg"

    "The tails move up and down!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    "The tips of the tails all rub the tip of Luka's penis!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h3")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return


label lamias_a4:
    if not world.party.player.surrendered:
        if not world.battle.first[0]:
            oldest_lamia "Hehe... Coil around him."
            second_lamia "Aren't you looking forward to being wrapped by so many Lamias?"
        else:
            oldest_lamia "Wrap him again..."

    $ world.battle.show_skillname("Групповое опутывание")
    play sound "se/ero_makituki3.ogg"

    "The four Lamia's tails coil around Luka!{w}{nw}"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    if world.party.player.wind:
        $ world.party.player.wind_guard()
        return

    extend "{w=.7}\nTheir tails coil around his arms, legs and body!{w}{nw}"

    $ world.battle.enemy[0].damage = {1: (10, 12), 2: (15, 18), 3: (20, 24)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Luka's bound by the Lamia's tails!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h4")

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    if not world.battle.first[0]:
        oldest_lamia "Hehe... How does it feel having four Lamia's wrapped around you?"
        third_lamia "It must be unbearable.{image=note}"
    else:
        oldest_lamia "This time we won't let you go."

    $ world.battle.first[0] = True

    if not world.party.player.earth:
        $ world.battle.enemy[0].power = world.battle.difficulty(2, 2, 3)
    else:
        $ world.battle.enemy[0].power = world.battle.difficulty(1, 1, 2)

    return

label lamias_a5:
    $ world.battle.skillcount(1, 3231)

    $ random = rand().randint(1, 3)

    if random == 1:
        oldest_lamia "Everyone, tighten up!"
        second_lamia "I want to feel him writhe around!"
    elif random == 2:
        oldest_lamia "Do you want to be hurt by us?"
        second_lamia "I'll tighten up as hard as I can!"
    elif random == 3:
        oldest_lamia "Let's squeeze him..."
        second_lamia "How does it feel to have your whole body strangled?"

    $ world.battle.show_skillname("Групповое сжатие")
    play sound "se/ero_simetuke1.ogg"

    "The four Lamias tighten their tails around Luka!{w}{nw}"

    $ world.battle.enemy[0].damage = {1: (5, 7), 2: (7, 10), 3: (10, 14)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=2)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h5")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return


label lamias_a6:
    $ world.battle.skillcount(1, 3232)

    $ random = rand().randint(1, 3)

    if random == 1:
        jump lamias_a6a
    elif random == 2:
        jump lamias_a6b
    elif random == 3:
        jump lamias_a6c


label lamias_a6a:
    oldest_lamia "Sisters, use your mouth!"
    second_lamia "Hahaha... I'll suck him the hardest!"

    $ world.battle.show_skillname("Групповой минет")

    "The four Lamia's lean in on Luka's groin with their mouths open!{w}{nw}"

    if world.party.player.aqua:
        $ world.party.player.aqua_guard()

    if world.party.player.wind:
        $ world.party.player.wind_guard()
        return

    play sound "se/ero_buchu2.ogg"

    extend "\nThe fourth born Lamia's tongue coils around Luka's penis!{w}{nw}"

    $ world.battle.enemy[0].damage = {1: (6, 8), 2: (9, 12), 3: (12, 16)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_buchu2.ogg"

    "The third born Lamia's tongue licks the back of Luka's penis!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_buchu2.ogg"

    "The second born Lamia's tongue squeezes Luka's penis!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_buchu2.ogg"

    "The oldest born Lamia's tongue licks Luka's urethral opening!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h6")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return


label lamias_a6b:
    oldest_lamia "Let's see who can suck the best!"
    second_lamia "Hehe... I won't lose!"

    $ world.battle.show_skillname("Групповой минет")

    "The four Lamia's lean in on Luka's groin with their mouths open!{w}{nw}"

    if wind == 1 and Difficulty < 2:
        $ wind_kakuritu = 15
    elif wind == 1 and Difficulty == 2:
        $ wind_kakuritu = 15
    elif wind == 1 and Difficulty == 3:
        $ wind_kakuritu = 7

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind == 1:
        $ sel1 = "\nBut the wind pushes them away!"
        jump wind_guard

    play sound "se/ero_buchu2.ogg"

    extend "\nThe youngest Lamia hesitatingly sucks on Luka's penis!{w}{nw}"

    $ world.battle.enemy[0].damage = {1: (6, 8), 2: (9, 12), 3: (12, 16)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_buchu2.ogg"

    "The third born Lamia gently bites on Luka's penis as she sucks!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_buchu2.ogg"

    "The second born Lamia intensely sucks on Luka's penis!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "se/ero_buchu2.ogg"

    extend "\nThe oldest Lamia's tongue wraps around Luka's penis as she sucks on it!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h6")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return


label lamias_a6c:
    oldest_lamia "Shall we all suck at once?"
    second_lamia "Hehe... Teamwork.{image=note}"

    $ world.battle.show_skillname("Групповой минет")

    "The four Lamia's lean in on Luka's groin with their mouths open!{w}{nw}"

    if wind == 1 and Difficulty < 2:
        $ wind_kakuritu = 15
    elif wind == 1 and Difficulty == 2:
        $ wind_kakuritu = 15
    elif wind == 1 and Difficulty == 3:
        $ wind_kakuritu = 7

    call wind_guard_kakuritu

    if wind_guard_on != 0 and wind == 1:
        $ sel1 = "But the wind pushes them away!"
        jump wind_guard

    extend "{w=.7}\nThe oldest and second born Lamia's put their lips on the left and right side of the tip of Luka's penis!"
    "The youngest puts her mouth around the base of Luka's penis as the third born Lamia sucks Luka's scrotum into her mouth!"

    play sound "se/ero_buchu2.ogg"

    "All of the Lamia's start to suck on Luka's penis at the same time!{w}{nw}"

    $ world.battle.enemy[0].damage = {1: (24, 32), 2: (36, 48), 3: (48, 64)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ world.battle.badend("h6")

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label lamias_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    second_lamia "Ха-ха, какой же ты жалкий.{w}\nДавайте все вместе с ним поиграем!"

    $ world.battle.enemy[0].attack()

label lamias_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Group Handjob"
    $ list2 = "Group Licking"
    $ list3 = "Group Tailjob"
    $ list4 = "Group Tightening"
    $ list5 = "Group Blowjob"

    if persistent.skills[3228][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3229][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3230][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3231][0] > 0:
        $ list4_unlock = 1

    if persistent.skills[3232][0] > 0:
        $ list5_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump lamias_onedari1
    elif world.battle.result == 2:
        jump lamias_onedari2
    elif world.battle.result == 3:
        jump lamias_onedari3
    elif world.battle.result == 4:
        jump lamias_onedari4
    elif world.battle.result == 5:
        jump lamias_onedari5
    elif world.battle.result == -1:
        jump lamias_main


label lamias_onedari1:
    call onedari_syori

    sara "Wait, Luka!{w}\nWhat are you doing!?"

    $ world.battle.face(2)

    oldest_lamia "It's pointless even if you scream, girl.{w}\nThis man has given in to desire."
    second_lamia "You want us to squeeze out your semen with our hands?{w}\nAlright... We'll be sure to get it all!"

    while True:
        call lamias_a1


label lamias_onedari2:
    call onedari_syori

    sara "Wait, Luka!{w}\nWhat are you doing!?"

    $ world.battle.face(2)

    oldest_lamia "It's pointless even if you scream, girl.{w}\nThis man has given in to desire."
    third_lamia "You want us to all lick you at the same time?{w}\nWhat a lewd request.{image=note}"

    while True:
        call lamias_a2


label lamias_onedari3:
    call onedari_syori

    sara "Wait, Luka!{w}\nWhat are you doing!?"

    $ world.battle.face(2)

    oldest_lamia "It's pointless even if you scream, girl.{w}\nThis man has given in to desire."
    youngest_lamia "You want all of our tails to play with your penis?"

    while True:
        call lamias_a3


label lamias_onedari4:
    call onedari_syori

    sara "Wait, Luka!{w}\nWhat are you doing!?"

    $ world.battle.face(2)

    oldest_lamia "It's pointless even if you scream, girl.{w}\nThis man has given in to desire."
    third_lamia "You want us to coil around you and strangle you with our tails? Haha.{image=note}"

    if world.party.player.bind == 0:
        call lamias_a4

    while True:
        call lamias_a5


label lamias_onedari5:
    call onedari_syori

    sara "Wait, Luka!{w}\nWhat are you doing!?"

    $ world.battle.face(2)

    oldest_lamia "It's pointless even if you scream, girl.{w}\nThis man has given in to desire."
    second_lamia "Wanting all of us to suck on you... How lewd.{image=note}"

    if world.party.player.bind == 0:
        call lamias_a4

    while True:
        call lamias_a6


label lamias_h1:
    l "Faaa..."
    oldest_lamia "Take a good look, girl.{w}\nThis boy is going to surrender to our hands."
    sara "...................."

    $ world.party.player.moans(1)
    with syasei1
    show lamias bk05 zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the four Lamia's handjob, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([5, 7])
    $ bad1 = "Luka succumbed to the Lamias group handjob."
    $ bad2 = "The Lamias kept Luka as a sex slave, and took turns raping him for the rest of his life."

    sara "............."

    $ world.battle.face(2)

    oldest_lamia "Hehe... You shot it out.{w}\nHow does it feel to surrender in front of your companion?"
    "The Lamias laugh as they show the semen to Sara."

    jump lamias_h


label lamias_h2:
    l "Faaa..."
    oldest_lamia "Take a good look, girl.{w}\nThis boy is going to surrender to our tongues."
    sara "...................."

    $ world.party.player.moans(1)
    with syasei1
    show lamias bk02 zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the four Lamia's tongues, Luka finally ejaculates."
    "The Lamias quickly move back to catch Luka's semen on their chests."

    $ world.battle.lose()

    $ world.battle.count([28, 7])
    $ bad1 = "Luka succumbed to the Lamia's group licking."
    $ bad2 = "The Lamias kept Luka as a sex slave, and took turns raping him for the rest of his life."

    sara "............."

    $ world.battle.face(2)

    oldest_lamia "Hehe... You shot it out.{w}\nHow does it feel to surrender in front of your companion?"
    "The Lamias laugh as they show the semen to Sara."

    jump lamias_h


label lamias_h3:
    l "Faaa..."
    oldest_lamia "Take a good look, girl.{w}\nThis boy is going to surrender to our tails."
    sara "...................."

    $ world.party.player.moans(1)
    with syasei1
    show lamias bk04 zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the four Lamia's tails, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([19, 7])
    $ bad1 = "Luka succumbed to the four Lamia's tailjob."
    $ bad2 = "The Lamias kept Luka as a sex slave, and took turns raping him for the rest of his life."

    sara ".............."

    $ world.battle.face(2)

    oldest_lamia "Hehe... You shot it out.{w}\nHow does it feel to surrender in front of your companion?"
    "The Lamias laugh as they show the semen to Sara."

    jump lamias_h


label lamias_h4:
    $ world.battle.lose(3)

    oldest_lamia "Oh?{w} Already coming?"

    with syasei1
    show lamias bk03 zorder 10 as bk
    show lamias bk04 zorder 10 as bk2
    $ world.ejaculation()

    "As soon as the Lamia's coiled around Luka, he gives in to the desire to ejaculate."

    $ world.battle.lose()

    $ persistent.count_bouhatu += 1
    $ world.battle.count([16, 7])
    $ bad1 = "Luka ejaculated as soon as the four Lamia's coiled around him."
    $ bad2 = "The Lamias kept Luka as a sex slave, and took turns raping him for the rest of his life."

    sara ".........."

    $ world.battle.face(2)

    oldest_lamia "Hehe... You shot it out.{w}\nHow does it feel to surrender in front of your companion?"
    "The Lamias laugh as they show the semen to Sara."

    jump lamias_h


label lamias_h5:
    l "Urgh..."
    oldest_lamia "Take a good look, girl.{w}\nThis pervert is going to come just from being strangled."
    sara "...................."
    l "Ahhhh!"

    with syasei1
    show lamias bk03 zorder 10 as bk
    show lamias bk04 zorder 10 as bk2
    $ world.ejaculation()

    "Strangled by the Lamia's tails, Luka orgasms unexpectedly."

    $ world.battle.lose()

    $ world.battle.count([16, 7])
    $ bad1 = "Luka ejaculated while the Lamia's tightened around him."
    $ bad2 = "The Lamias kept Luka as a sex slave, and took turns raping him for the rest of his life."

    sara "............."

    $ world.battle.face(2)

    oldest_lamia "Haha... How pathetic.{w}\nShooting out from being strangled in front of your companion."
    "The Lamias laugh as they show the semen to Sara."

    jump lamias_h


label lamias_h6:
    l "Faaaa!"
    oldest_lamia "Take a good look, girl.{w}\nThis boy is going to surrender to our mouths!"
    sara "...................."

    $ world.party.player.moans(1)
    with syasei1
    show lamias bk01 zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the group blowjob, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([4, 7])
    $ bad1 = "Luka succumbed to the Lamia's group blowjob."
    $ bad2 = "The Lamias kept Luka as a sex slave, and took turns raping him for the rest of his life."

    sara ".........."

    $ world.battle.face(2)

    oldest_lamia "Haha... Look, girl.{w}\nYour pathetic companion made our faces so sticky..."
    "The Lamias laugh as they show the semen to Sara."

    jump lamias_h


label lamias_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    sara "............"
    "Sara coldly glares at me.{w}\nToo weak to stand, I fall to the ground."

    play sound "se/ero_simetuke1.ogg"
    hide bk
    hide bk2
    show lamias ha1
    with dissolve

    "Like that, all four of them tangle their bodies around mine.{w}\nWrapped in their tails, I can't move at all."
    l "Ahh..."
    "The four sisters wrap their soft, warm bodies around me."
    "With the weight of their tails on me, the disgrace of defeat is only amplified."
    "The oldest lamia moves her waist towards mine as her dripping wet vagina opens up."
    oldest_lamia "Hehe... We'll rape you in front of your companion.{w}\nMake sure you watch closely, girl."
    sara "............"
    "Sara quietly glares at me."

    play hseanwave "se/hsean02_innerworks_a2.ogg"
    show lamias ha2 with dissolve

    "While I look at her angry eyes, the Lamia suddenly forces me into her!"
    l "Ahhh!!"
    "The Oldest Lamia was surprisingly hot inside.{w}\nSmall ridges lined the walls of her pussy that rubbed me with every movement."
    "Forced all the way into her, the tip of my penis rubs against every single ridge.{w}\nThe surprising pleasure just from insertion causes a chill to go down my spine."
    l "Ah! I'm coming!"

    with syasei1
    show lamias ha3
    $ world.ejaculation()
    stop hseanwave fadeout 1

    oldest_lamia "Oh?.."
    "The Lamia looks surprised as I come just from being inserted into her.{w}\nBut her look slowly turns to a smile."
    oldest_lamia "...You already came?{w}\nWhat a pitiful man."
    second_lamia "Hahaha, he came right away, sister?{w}\nDo you really feel that good? Hahaha!"
    third_lamia "Pathetic~{image=note}!{w}\nWhat a quick shot~{image=note}!"
    youngest_lamia "Already letting it out?{w}\nHehe..."
    "All four of the Lamia's laugh at me.{w}\nRaped and insulted, my body twitches in humiliation."
    sara "............"
    "Sara's cold glare shows nothing but contempt."
    "Raped by a Lamia and coming almost instantly, she's completely disgusted."
    oldest_lamia "I'm sorry for forcing you to come so fast...{w}\nI just didn't think you were that pathetic."
    "The Oldest Lamia laughs in a triumphant manner as she looks down at me, with my penis still stuck inside her."
    oldest_lamia "Oh well... Let's try again.{w}\nDon't be so quick this time!"

    play hseanwave "se/hsean02_innerworks_a2.ogg"

    l "Ahhhh!!"
    "She squeezes her vagina tightly around me.{w}\nWith the heat and ridges from her walls, I would have come again with that if I hadn't just orgasmed."
    "In addition, she starts to slowly move her waist.{w}\nWith the movement, I'm forced in and out of her, rubbing my penis against the ridges inside of her."
    l "Aiee!{w} Ahhh!"
    "The overwhelming pleasure from her ribbed pussy feels like I'm stuck in heaven."
    "The lewd sounds from her waist slapping against mine fill the narrow hallway."
    "I've already started to become addicted to this Lamia's amazing vagina..."
    l "Faaa... Incredible..."
    oldest_lamia "Hehe... Are you already a slave to my pussy?{w}\nThat's not very fitting of a Hero..."
    second_lamia "Does her vagina really feel that good?"
    third_lamia "Aren't you glad you're being raped?{image=note}"
    "The other Lamias push their soft bodies into mine as they laugh at me."
    sara "You're the worst..."
    "Sara mutters that as she continues to glare at me, as if she's looking at filth."
    "Insulted from every direction while being raped, the disgrace boils over in me."
    "Trembling in misery, I can't do anything but be raped..."
    l "Auuu..."
    oldest_lamia "It's about time for me to make you come."
    l "Aiee!"
    "The Oldest Lamia's vagina suddenly contracts.{w}\nStill moving her hips, the stimulation from the ridges inside of her dug into me as the stimulation becomes too much to endure."
    l "Ahhh!!"
    oldest_lamia "You can't take it when I do this, right?{w}\nNow, shoot out even more inside me!"
    "Forced in and out of her, I'm quickly brought to the edge."
    "Helplessly raped with the Lamias all wrapped around me, I give in to the rising desire."
    l "Ah!{w} It's too good!{w}\nFaaaa!!"

    with syasei1
    show lamias ha4
    $ world.ejaculation()

    "The Oldest Lamia has a pleased smile on her face as I explode inside of her, filling her with my semen."

    stop hseanwave fadeout 1

    oldest_lamia "Hehe... That was fun.{w}\nNow, who's next?"
    second_lamia "Oh, me!"
    "As soon as the Oldest Lamia gets off of me, the second born Lamia jumps on me!"

    show lamias hb1 with dissolve

    "Her tail wraps around my body and forces me right next to her, as if in a tight embrace."
    l "Ahg!"
    "I let out a groan as her strong tail crushes my body.{w}\nUnable to do anything, the fact that I'm just prey for her to play with is forced into me."
    second_lamia "Let's see...{w}\nI think I'll try raping you as I strangle you."
    l "Ahhh!!"

    play hseanwave "se/hsean04_innerworks_a4.ogg"
    show lamias hb2 with dissolve

    "While her tail tightens around my body, she forces my penis into her surprisingly tight vagina."
    "Like her tail squeezing my body, her vagina is squeezing my penis."
    l "Auu!{w} It's too tight inside!"
    second_lamia "Hahaha!{w} It's not just the inside that will tighten..."
    l "Ahhh!!"
    "Her tail squeezes down on my body even tighter.{w}\nAs my body is slowly being crushed by her strong tail, I feel her vagina tightening around my penis at the same time."
    second_lamia "Haha... I'll rape your mouth too..."

    show lamias hb3 with dissolve

    "The Lamia forces her tongue into my mouth.{w}\nHer long tongue darts around my mouth and wraps itself around mine."
    second_lamia "Well?{w} How does it feel to have sex with a Lamia?{w}\nSqueezed all over... Is it unbearable?"
    l "Urhg!{w} Ahhh!"
    "As every part of my body is wrapped and crushed by the Lamia, the sense of oppression is overwhelming."
    "But the damage isn't just being done to my body... It feels as though my mind itself is being crushed by her."
    "Squeezed and raped with no escape, the sense of hopelessness fills every fiber of my being."
    "My body relaxes as the Lamia squeezes tighter around me.{w}\nWith my mind and body breaking, I give in and accept the rape."
    second_lamia "Hahaha... Already admitting defeat?{w}\nI love the moment when a man surrenders..."
    second_lamia "I'll finish you quickly for now...{w}\nBut I'll strangle you a lot in the future..."
    "Even more than before, the Lamia contracts her vagina around me."
    l "Aiee!"
    second_lamia "Here, I'll tighten it more...{w}\nSo make sure you come right away!"
    "The Lamia's vaginal muscles squeeze me without mercy.{w}\nWith her soft flesh crushing me, I quickly go over the edge."
    l "Auuu..."

    with syasei1
    show lamias hb4
    $ world.ejaculation()

    "Exhausted, my semen explodes into her tight vagina."

    stop hseanwave fadeout 1

    "But there was no time to rest.{w}\nThe tail of another Lamia starts to coil around my body."
    l "S...Stop... If you do any more..."
    third_lamia "What are you saying?{w}\nWe're gang-raping you.{image=note}"

    show lamias hc1 with dissolve

    "The next Lamia pushes her ass onto my groin.{w}\nMy penis slips between the soft cheeks of her ass."
    "With her tail tightly wrapped around my body, I can't get away.{w}\nDragged by her tail, my penis is poking the entrance of her ass."
    third_lamia "I'll rape you with my ass...{w}\nIt's good enough for someone like you.{image=note}"
    l "Ahhh!"
    "The Lamia skillfully uses her tail to slowly force me into her.{w}{nw}"

    play hseanwave "se/hsean05_innerworks_a5.ogg"

    extend "\nLike that, I'm forced all the way into the Lamia's ass!"
    l "Auuu!"
    "The feeling inside of her was completely different from the other Lamia's vaginas."
    third_lamia "How is it?{w}\nHere... I'll make it even better~{image=note}!"
    "The Lamia grins as she starts to clench her muscles, causing her anus to tighten around me."
    l "Aieee!"
    third_lamia "Haha... What a nice scream.{w}\nYour penis is twitching in my ass..."
    third_lamia "Just like that, shoot out all your semen into me.{w}\nNot even good enough to use my vagina... Hahaha.{image=note}"
    "The Lamia tightens her anus as she laughs.{w}\nTightened up by this new hole, a strange pleasure washes over me."
    "At the edge of an orgasm, I put the last of my strength into my waist to stop myself from exploding in her."
    l "Auu... Guh..."
    third_lamia "Haha... You held on.{w}\nYou're trying so hard... But it's useless in the end.{image=note}"
    l "Eh?..{w} Ahhh!"
    "The tail around my body starts to forcibly move me.{w}\nControlled by her tail, she forces me to start pumping her ass."
    l "Stop!!{w} Ahhh!"
    third_lamia "Hahaha, if I do it like this, it's like you're seeking pleasure on your own."
    third_lamia "Now you have no choice but to come in me.{image=note}"
    l "Hauu!{w} Ahhhh!!"
    "With movements against my will, I'm forced in and out of her.{w}\nAs she tightens her ass around me, I'm forced to move like a piston."
    "I let out a moan as the tip of my penis keeps entering in and out of her tight anus."
    "Forced into having anal sex with her, I finally break down and yield."
    l "Ahhhh!!"

    with syasei1
    show lamias hc2
    $ world.ejaculation()

    "I explode inside the Lamia as I feel humiliated at being forced to have anal sex with her."

    stop hseanwave fadeout 1

    "But even after that, they don't give me a moments rest.{w}\nAnother tail winds around me and forces me to the ground."
    youngest_lamia "I'm next...{w}\nThis boy looks really weak, so I should be ok."
    l "Ahhhh!"

    play hseanwave "se/hsean03_innerworks_a3.ogg"
    show lamias hd1 with dissolve

    "The Youngest Lamia drops her waist on me, as I screw into her tiny vagina."
    "Without even trying to squeeze me at all, her pussy was tighter than the other Lamias."
    l "Auuu!"
    youngest_lamia "It went in...{image=note}"
    second_lamia "Hahaha... How is she?{w}\nShe's still young, so her vagina isn't very soft yet..."
    l "Ahhh!"
    "Just from being inside her, my penis is crushed in her tiny pussy."
    "It doesn't feel like she has any muscles inside of her yet.{w}\nEven though she's narrow inside, the flesh is fairly rigid."
    "In addition, the hard creases and bumps inside of her rub against me.{w}\nThe unique stimulation gives a mix of pleasure and pain."
    l "Auu... It's hard..."
    second_lamia "It can't be helped... This child is still growing.{w}\nHer vagina is still small, and this is her first time..."
    youngest_lamia "But I can still make him come!"
    "The Lamia wraps her tail around my body as she starts to bounce up and down."
    "The rigid flesh of her vagina rubs against me with a mix of pain and pleasure."
    l "Ah!{w} Ahhh!"
    "My body writhes as it feels like my penis is being slightly tortured.{w}\nThe slight pain brings me to another orgasm surprisingly fast as the Youngest Lamia happily bounces up and down."
    l "Auu...{w} I'm going to come..."
    youngest_lamia "That's good... I want to feel it shoot out inside me!"
    l "Ahhh!!"

    with syasei1
    show lamias hd2
    $ world.ejaculation()

    "I explode inside of her small vagina, as my semen starts to overflow out of it."
    "The slight pain I felt while ejaculating causes a new strange pleasure to wash over me."
    youngest_lamia "Haha... It's so easy to make men come!"
    "The Youngest Lamia looks down at me with a triumphant face as she happily exclaims that."
    l "Auuu..."
    "Raped one after another, I'm left exhausted."

    stop hseanwave fadeout 1
    show lamias ha1 with dissolve

    oldest_lamia "It looked like everyone had their fun...{w}\nThen, it's my turn again."
    l "Ahhh!"
    "The gang rape by the four Lamias isn't over..."

    scene bg black with Dissolve(3.0)

    "Captured by the Lamias, they made me into their sex toy.{w}\nRaped one after another by the four of them, they don't allow me to escape."
    "Every day for the rest of my life, they'll continue to rape me..."
    "..................."

    jump badend

label sphinx_ng_start:
    python:
        world.troops[46] = Troop(world)
        world.troops[46].enemy[0] = Enemy(world)

        world.troops[46].enemy[0].name = Character("Сфинкс")
        world.troops[46].enemy[0].sprite = [
            "sphinx st01",
            "sphinx st02",
            "sphinx st03",
            "sphinx hb1"
        ]
        world.troops[46].enemy[0].position = center
        world.troops[46].enemy[0].defense = 80
        world.troops[46].enemy[0].evasion = 95
        world.troops[46].enemy[0].max_life = world.troops[46].battle.difficulty(20000, 24000, 28000)
        world.troops[46].enemy[0].power = world.troops[46].battle.difficulty(0, 1, 2)

        world.troops[46].battle.tag = "sphinx_ng"
        world.troops[46].battle.name = world.troops[46].enemy[0].name
        world.troops[46].battle.background = "bg 069"

        world.party.player.earth_keigen = 30

        if persistent.music:
            world.troops[46].battle.music = 26
        else:
            world.troops[46].battle.music = 3

        world.troops[46].battle.ng = True
        world.troops[46].battle.exp = 1800000
        world.troops[46].battle.exit = "lb_0076"

        world.troops[46].battle.init(0)

        if world.party.check(2) or world.party.check(4):
            world.party.member[2].support = True
        world.party.player.skill_set(2)

    l "Подожди!{w}\nЕсть веская причина — зачем она мне нужна!"

    $ world.battle.face(3)

    sphinx "Достаточно!"
    sphinx "Мой долг — оберегать эту сферу.{w}\nКоторый мне дала сама предок зверей..."
    sphinx "Я никогда не сдамся человеку!"
    l "Гх!"
    "Чёрт возьми!{w}\nМне и правда не хочется драться с ней!"
    "Но похоже у меня нет иного выбора..."

    if world.party.check(3):
        jump sphinx_ng_vt

    $ world.battle.main()

label sphinx_ng_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()
        elif world.battle.result == 8:
            world.battle.common_support()

label sphinx_ng_v:
    if world.battle.stage[0] == 1:
        jump sphinx_ng_v2
    elif world.battle.stage[0] == 2:
        jump sphinx_ng_v3

    $ world.battle.stage[0] = 1

    sphinx "О?{w} А ты хорош, пусть по тебе так и не скажешь."

    play sound "se/syometu.ogg"
    hide sphinx with crash

    "Сфинкс исчезает!"
    l "Получилось!{w} Э?.."

    play sound "se/syometu.ogg"
    show sphinx st01 with crash
    $ world.battle.enemy[0].life = world.battle.enemy[0].max_life

    "Сфинкс появляется вновь!"
    l "Подожди, что за?..{w}\nКаким образом, чёрт возьми?!"

    $ world.battle.face(2)

    sphinx "Я живу с незапамятных времен.{w}\nДаже если использовать против меня запечатывающие техники, я легко могу восстановить своё тело."
    sphinx "Как печально для тебя, мальчик.{w}\nТебе не победить меня, просто запечатав..."
    l "Гах... вот блин!"
    "Страницу я смог победить с помощью огня...{w}\nНо сейчас у меня есть только этот клинок!"
    sphinx "Твоя единственная надежда на победу — использовать другой меч..."
    l "А?.."
    sphinx "Другой меч, который покоится во вторых ножнах...{w}\nЭто же обычный клинок, да?"
    l "Ох..."
    "Она права.{w}\nСкорее всего я бы мог победить, если бы использовал его."
    l "Но...{w} я ведь могу тебя поранить им!"
    sphinx "Глупое дитя...{w}{nw}"

    $ world.battle.face(3)

    extend "\nПоказавший милосердие обречён на погибель!"

    call sphinx_ng_a9

    $ world.battle.enemy[0].attack()

label sphinx_ng_v2:
    $ world.battle.stage[0] = 2
    $ world.battle.face(3)

    sphinx "... Хмпф."

    play sound "se/syometu.ogg"
    hide sphinx with crash

    "Сфинкс исчезает!"

    pause 1.0

    play sound "se/syometu.ogg"
    show sphinx st01 with crash
    $ world.battle.enemy[0].life = world.battle.enemy[0].max_life

    "Сфинкс появляется вновь!"
    l "Да блин!.."
    sphinx "Я ведь сказала, таким способом тебе не победить."
    l "Грр!.."
    "Она ведь не может так постоянно возрождаться! Правда же?"

    $ world.battle.enemy[0].attack()

label sphinx_ng_v3:
    $ world.battle.face(3)

    sphinx "....................."
    "Внезапно Сфинкс остановилась на месте."
    l "Хах?.."
    "Что она делает?..{w}\nНеважно, главное не терять бдительности!"
    sphinx "Похоже мы зашли в тупик, дитя."
    "Сфинкс вздыхает."
    sphinx "... Я не могу победить тебя.{w}\nЭто уже очевидно."
    sphinx "А ты, в свою очередь, отказываешься сражаться серьёзно.{w}\nКак оскорбительно..."
    l "Что?{w}\nОскорбительно?{w} О чём ты вообще?"
    sphinx "Ты ведь можешь легко убить меня, если перестанешь использовать этот странный меч.{w}\nНо вместо этого ты пытаешься выбесить меня..."
    l "Не пытаюсь!{w}\nЯ просто...{w} не хочу навредить тебе."
    sphinx "... Однажды милосердие приведёт к погибели, человек."

    stop music fadeout 1.0
    pause 1.0

    sphinx "Хорошо.{w}\nСдаюсь."
    l "Серьёзно?{w}\nТогда!.."

    $ world.battle.victory(1)

label sphinx_ng_vt:
    stop music fadeout 2.0
    pause 2.0

    t "... Хватит."

    show tamamo st07 at xy(0.3) zorder 10
    show sphinx st03 at xy(0.7)

    l "Ч-что?!"
    "Без тени смущения Тамамо, стоя в сторонке, приказывает нам остановиться ещё до того, как начнётся настоящая битва."
    sphinx "Ты тоже здесь, чтобы украсть сферу?!"

    show tamamo st01

    t "Пха-ха-ха..."
    t "О, Сфинкс...{w}\nНеужели ты и правда до сих пор меня не узнала?"
    sphinx "О чём ты?"
    t "Хм-хм, ясно..."

    show tamamo st04

    t "Похоже пора освежить твою память!"

    play sound "se/flash.ogg"
    show bg white zorder 100 as flash

    "Белая вспышка света окутывают всю комнату!"
    "И когда она спала..."

    hide flash
    show tamamo st55

    "... Тамамо уже стояла перед нами в своей истинной форме."
    sphinx "Вы!.."
    t "Вспомнила наконец свою старую хозяйку?"
    l "Подожди...{w}\nТак ты тоже знаешь её?!"

    $ world.battle.exit = "lb_0076_ta"
    $ world.battle.exp = 0
    $ world.battle.victory(1)

label sphinx_ng_a:
    if world.battle.stage[0] == 2 and world.battle.enemy[0].life < world.battle.enemy[0].max_life/2:
        jump sphinx_ng_v3

    if world.battle.stage[1] == 1:
        call sphinx_ng_a10b
    elif world.battle.stage[1] > 1:
        call sphinx_ng_a10a

    if world.battle.enemy[0].earth:
        $ world.battle.enemy[0].defense = 40
    else:
        $ world.battle.enemy[0].defense = 80

    if world.party.player.surrendered:
        call sphinx_ng_a4b
        $ world.battle.main()

    #;if %hanyo2=1 gosub *sphinx_ng_a6:goto *common_main	;??...??...??

    while True:
        if not world.battle.stage[0] or world.battle.stage[1]:
            $ random = rand().randint(1, 8)
        else:
            $ random = rand().randint(1, 13)

        if random == 1:
            call sphinx_ng_a1
            $ world.battle.main()
        elif random == 2:
            call sphinx_ng_a2
            $ world.battle.main()
        elif random == 3:
            call sphinx_ng_a3
            $ world.battle.main()
        elif random == 4 and not world.party.player.surrendered and world.battle.delay[0] > 5:
            call sphinx_ng_a4a
            $ world.battle.main()
        elif random == 5 and not world.party.player.status and world.battle.delay[1] > 4:
            call sphinx_ng_a3
            $ world.battle.main()
        elif 5 < random < 9 and not world.battle.enemy[0].earth:
            call sphinx_ng_earth
            $ world.battle.main()
        elif random > 8 and not world.party.player.status:
            call sphinx_ng_a9
            $ world.battle.main()

label sphinx_ng_earth:
    sphinx "...................."

    $ world.battle.show_skillname("Дикие земли")
    play sound "se/earth2.ogg"

    "Сфинкс наполняет свое тело силой земли!"

    $ world.battle.enemy[0].earth = 1
    $ world.battle.enemy[0].earth_turn = 4
    $ world.battle.enemy[0].defense = 40

    $ world.battle.hide_skillname()

    return

label sphinx_ng_a1:
    world.battle.enemy[0].name "Я заставлю тебя осознать собственную глупость своими пальцами."

    $ world.battle.show_skillname("Приглашение пальцами")
    play sound "se/ero_koki1.ogg"

    "Сфинкс хватает за член Луки!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2 and persistent.difficulty == 1 and random <= 40:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random <= 30:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random <= 20:
            world.party.player.aqua_guard()

        elif world.party.player.aqua > 2 and persistent.difficulty == 1 and random <= 15:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random <= 10:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random <= 5:
            world.party.player.aqua_guard()

        # 1: (150, 180), 2: (190, 220), 3: (230, 260)
        world.battle.enemy[0].damage = {1: (160, 190), 2: (200, 230), 3: (240, 270)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 45, 2: 40, 3: 20}):
            world.party.player.wind_guard()
            renpy.return_statement()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"

    "Её руки поглаживают член Луки!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"

    "Сфинкс играется с головкой члена!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label sphinx_ng_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Мой удовлетворит тебя.",
        "Ты растаешь в моём рту.",
        "Я высосу тебя досуха...",
    ])

    $ world.battle.show_skillname("Приглашение ртом")
    play sound "se/ero_buchu2.ogg"

    "Сфинкс быстро засовывает член Луки в свой рот!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2 and persistent.difficulty == 1 and random <= 45:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random <= 35:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random <= 25:
            world.party.player.aqua_guard()

        elif world.party.player.aqua > 2 and persistent.difficulty == 1 and random <= 20:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random <= 15:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random <= 10:
            world.party.player.aqua_guard()

        # 1: (180, 210), 2: (230, 260), 3: (280, 310)
        world.battle.enemy[0].damage = {1: (190, 220), 2: (240, 270), 3: (290, 320)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 30, 2: 25, 3: 15}):
            world.party.player.wind_guard()
            renpy.return_statement()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_buchu2.ogg"

    "Она вылизывает языком заднюю часть члена Луки!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"

    "Сфинкс сужает рот, начиная сосать ещё сильнее!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label sphinx_ng_a3:
    world.battle.enemy[0].name "Я унижу тебя своей лапой."

    $ world.battle.show_skillname("Приглашение лапой")
    play sound "se/ero_koki1.ogg"

    "Сфинкс быстро засовывает член Луки в свой рот!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2 and persistent.difficulty == 1 and random <= 40:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random <= 30:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random <= 20:
            world.party.player.aqua_guard()

        elif world.party.player.aqua > 2 and persistent.difficulty == 1 and random <= 15:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random <= 10:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random <= 5:
            world.party.player.aqua_guard()

        # 1: (150, 180), 2: (190, 220), 3: (230, 260)
        world.battle.enemy[0].damage = {1: (160, 190), 2: (200, 230), 3: (240, 270)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 45, 2: 40, 3: 20}):
            world.party.player.wind_guard()
            renpy.return_statement()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"

    "Она потирает член Луки о свои мягкие подушечки!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"

    "Сфинкс играется с головкой члена!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label sphinx_ng_a4a:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я обездвижу тебя своими змеями.",
        "Даже не пытайся сбежать...",
        "Это удержит тебя в узде.",
    ])

    $ world.battle.show_skillname("Церемониальное опутывание")
    play sound "se/ero_koki1.ogg"

    "Змеи Сфинкса устремляются в сторону Луки!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            renpy.call(f"{world.battle.tag}_a4c")
            renpy.return_statement()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2 and persistent.difficulty == 1 and random <= 50:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random <= 40:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random <= 30:
            world.party.player.aqua_guard()

        elif world.party.player.aqua > 2 and persistent.difficulty == 1 and random <= 25:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random <= 20:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random <= 15:
            world.party.player.aqua_guard()

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 100, 2: 100, 3: 50}):
            world.party.player.wind_guard()
            renpy.return_statement()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Лука опутан змеями!"

    $ world.battle.hide_skillname()

    $ world.battle.face(2)

    world.battle.enemy[0].name "Теперь осталось лишь сломать твой разум.{w}\nТебя поглотит наслаждение и экстаз."

    if world.party.player.earth == 0:
        $ world.battle.enemy[0].power = 2

    elif world.party.player.earth > 0:
        $ world.battle.enemy[0].power = 0

    $ world.battle.face(1)

    return

label sphinx_ng_a4b:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Задохнись в моей груди.",
        "Моя древняя техника погрузит тебя в пучину наслаждения.",
        "Мужчин так легко контролировать...",
    ])

    $ world.battle.show_skillname("Любовь Исиды")
    play sound "se/ero_koki1.ogg"

    "Сфинкс зажимает член Луки меж своих сисек!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            renpy.call(f"{world.battle.tag}_a4c")
            renpy.return_statement()

        # 1: (300, 350), 2: (370, 420), 3: (440, 490)
        world.battle.enemy[0].damage = {1: (310, 360), 2: (380, 430), 3: (450, 500)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 100, 2: 100, 3: 50}):
            world.party.player.wind_guard()
            renpy.return_statement()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()

    show sphinx hb1
    play sound "se/ero_koki1.ogg"

    "Она быстро двигает своей грудь вверх-вниз!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"

    "Сфинкс буквально выжимает член Луки!"

    $ world.party.player.status = 1
    $ world.party.player.status_turn = world.battle.difficulty(
        rand().randint(2, 3),
        rand().randint(3, 4),
        rand().randint(4, 5)
    )
    $ world.party.player.status_print()

    if not world.party.player.status:
        "Лука очарован!"
    else:
        "Лука в трансе..."

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.surrendered:
        return

    if not world.battle.first[3]:
        "Уф-ф... а-а..."
        world.battle.enemy[0].name "Неужто моя грудь свела тебя с ума?{w}\nПосиди тогда, пока я с тобой поиграю."

        $ world.battle.first[3] = True

    $ world.battle.face(1)

    return

# TODO: sphinx_ng_a4c

label sphinx_ng_a5:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Этим поцелуем я закончу с тобой..."

    $ world.battle.show_skillname("Поцелуй смерти")
    play sound "se/power.ogg"

    "Магия собирается на губах Сфинкса!"

    $ world.battle.hide_skillname()

    if world.party.player.status == 1 or world.battle.first[0]:
        return

    l "Что за?!"
    "Опасная магия собирается у её губ...{w}\nПлохое предчувствие у меня..."

    return

label sphinx_ng_a6:
    if not world.party.player.surrendered:
        if world.battle.first[2]:
            "Защищаться бесполезно..."
        else:
            "Почувствуй поцелуй самой смерти..."

    $ world.battle.show_skillname("Поцелуй смерти")
    play sound "se/ero_chupa1.ogg"

    "Сфинкс целует Луку!{w}\nНеземное блаженство пронизывает его тело, когда её слюна смешивается с его слюной!"
    l "А-а-а-а-а..."
    "И вместе с этим поцелуем его тело покидают и последние силы..."

    $ world.party.player.life = 0

    "Поцелуй смерти обрекает Луку на смерть!"

    $ world.battle.badend("h5")

label sphinx_ng_a9:
    world.battle.enemy[0].name "Ты умрёшь за то, что посмел потревожить это священное место!"

    $ world.battle.show_skillname("Проклятие фараона")
    play sound "se/yami.ogg"

    "Сфинкс читает древнее заклинание!"

    $ world.battle.stage[1] = 6

    "Лука проклят!"

    if not world.battle.first[0]:
        call sphinx_ng_a9a

    $ world.battle.hide_skillname()

    return

label sphinx_ng_a9a:
    $ world.battle.first[0] = True

    l "Ч-что ты сделала?!"
    "Мурашки пробегают по моей спине после того, как она закончила читать заклинание.{w}\nНо... не похоже, чтобы после этого что-то произошло."
    world.battle.enemy[0].name "Через считанные минуты твоя душа покинет тело.{w}\nОт тебя ничего не останется, кроме пустой безжизненной оболочки."
    l "?!"
    "Она собирается убить меня?!{w}\nИ к-как мне этому помешать?!"

    return

label sphinx_ng_a10a:
    $ world.battle.stage[1] -= 1
    $ world.battle.show_skillname("Проклятие фараона")

    world.battle.enemy[0].name "[world.battle.stage[1]]..."

    if world.battle.stage[1] == 1 and not world.battle.first[1]:
        jump sphinx_ng_a10d

    $ world.battle.hide_skillname()

    return

label sphinx_ng_a10b:
    $ world.battle.stage[1] -= 1
    $ world.battle.show_skillname("Проклятие фараона")

    world.battle.enemy[0].name "[world.battle.stage[1]]..."
    world.battle.enemy[0].name "О проклятие, приведи эту душу к распятию.{w}\nА сейчас, умри же."

    play sound "se/yami.ogg"

    "Тёмные силы пытаются похитить душу Луки!"

    $ renpy.dynamic(save_keigen=world.party.player.earth_keigen)
    $ world.party.player.earth_keigen = 0

    $ world.battle.enemy[0].deal((world.party.player.max_life, world.party.player.max_life), 1, direct=True)

    $ world.party.player.earth_keigen = save_keigen

    $ world.battle.hide_skillname()

    if world.party.player.life == 0:
        jump badend_h

    if not world.battle.first[2]:
        call sphinx_ng_a10c

    $ world.battle.face(1)

    return

label sphinx_ng_a10c:
    l "Грхааа!"
    "Только на мгновение, я прям почувствовал, словно моё тело разорвало на куски.{w}\nНо... мне удалось пережить это."

    if world.party.player.earth:
        "И даже Гнома никак не смогла защитить меня..."

    "Если Сфинкс снова использует этот приём, я могу и умереть!"

    return

label sphinx_ng_a10d:
    l "Ой-ёй...{w}\nЧ-что просходит?"
    "Непреодолимое чувство страха окутывает меня."

    $ world.summon("salamander st03")

    salamander "Лука!{w}\nЗащищайся! Сейчас же!"
    l "А?!"
    salamander "Если ты не хочешь тут помереть, тебе же лучше это сделать!"
    "... Защищаться?{w}\nЭто и правда сработает?"

    return

label sphinx_ng_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(3)

    world.battle.enemy[0].name "Уже сдаёшься?{w}\nТогда позволь попробовать тебя..."

    $ world.battle.enemy[0].attack()

label sphinx_ng_alskill3:
    "Альма поворачивается к Сфинксу."

    show sphinx st01 at xy(0.3)
    show alma_elma st22 at xy(0.7)
    $ world.battle.face(3)

    alma "О-о-о, а ты мне нравишься...{w}\nЯ вдо-о-о-оволь повеселюсь с такой милашкой..."

    play sound "se/flash.ogg"
    with flash

    "Смотря прямо в глаза Сфинкса, оки Альмы вспыхивают в свете!"
    world.battle.enemy[0].name "................."
    alma "................."
    world.battle.enemy[0].name "................."

    show alma_elma st13

    alma "................."
    alma "... Ну как считаешь?"
    sphinx "Это что, была попытка... меня соблазнить?"

    play sound "se/lvdown.ogg"

    "Это не возымело никакого эффекта!"

    hide alma_elma
    show sphinx st01 at center

    return

label saraevil_ng_start:
    python:
        world.troops[47] = Troop(world)
        world.troops[47].enemy[0] = Enemy(world)

        world.troops[47].enemy[0].name = Character("Сара")
        world.troops[47].enemy[0].sprite = [
            "saraevil st01",
            "saraevil st02",
            "saraevil st03",
            "saraevil ha1"
        ]
        world.troops[47].enemy[0].position = center
        world.troops[47].enemy[0].defense = 100
        world.troops[47].enemy[0].evasion = 95
        world.troops[47].enemy[0].max_life = world.troops[47].battle.difficulty(25000, 28000, 32000)
        world.troops[47].enemy[0].power = world.troops[47].battle.difficulty(0, 1, 2)

        world.troops[47].battle.tag = "saraevil_ng"
        world.troops[47].battle.name = world.troops[47].enemy[0].name
        world.troops[47].battle.background = "bg 068"

        world.party.player.earth_keigen = 20

        if persistent.difficulty == 3:
            world.party.player.earth_keigen = 10

        if persistent.music:
            world.troops[47].battle.music = 26
        else:
            world.troops[47].battle.music = 2

        world.troops[47].battle.ng = True
        world.troops[47].battle.exp = 1250000
        world.troops[47].battle.exit = "lb_0080"

        world.troops[47].battle.init(0)


    $ world.battle.delay[0] = 8
    $ world.battle.delay[1] = 8
    $ world.battle.delay[2] = 8

    $ world.battle.main()

label saraevil_ng_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()
        elif world.battle.result == 8:
            world.battle.common_support()

label saraevil_ng_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Н-нет...{w}\nМоя сила..."

    play sound "se/syometu.ogg"
    hide saraevil with crash

    "Магическая сила покидает тело Сары, возвращая ей человеческий облик!"

    $ world.battle.victory(1)

label saraevil_ng_a:
    if world.party.player.status == 1:
        call saraevil_ng_a4
        $ world.battle.main()

    if world.party.player.bind == 1:
        call saraevil_ng_a6
        call saraevil_ng_a7
        $ world.battle.main()
    #elif world.party.player.bind == 2:
    #    call saraevil_ng_a9
    #    $ world.battle.main()

    while True:
        $ random = rand().randint(1, 9)

        if random < 4:
            call saraevil_ng_a1
            $ world.battle.main()
        elif random < 7:
            call saraevil_ng_a2
            $ world.battle.main()
        elif random == 7 and world.battle.delay[0] > 9:
            call saraevil_ng_a3
            $ world.battle.main()
        elif random == 8 and world.battle.delay[0] > 9:
            call saraevil_ng_a5
            $ world.battle.main()
        #elif random == 9:
        #    call saraevil_ng_a8
        #    $ world.battle.main()

label saraevil_ng_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Интересно, каков твой член...",
        "Я хочу выжать его своей рукой...",
        "Моя рука выжмет всё семя из тебя...",
    ])

    $ world.battle.show_skillname("Мастурбация Матушки Земли")
    play sound "se/ero_koki1.ogg"

    "Сара хватает член Луки и начинает медленно дрочить ему!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2:
            world.party.player.aqua_guard()

        elif world.party.player.aqua > 2 and persistent.difficulty == 1 and random <= 75:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random <= 65:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random <= 50:
            world.party.player.aqua_guard()

        # 1: (450, 500), 2: (500, 550), 3: (550, 600)
        world.battle.enemy[0].damage = {1: (460, 510), 2: (510, 560), 3: (560, 610)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 30, 2: 25, 3: 15}):
            world.party.player.wind_guard()
            renpy.return_statement()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"
    "Используя вторую руку, она трёт пальцами голову члена!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"
    "Сжимая кончик члена, она начинает дрочить ещё сильнее!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label saraevil_ng_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я сожму его своей грудью...",
        "Я выжму всё из тебя своей грудью...",
        "Я... хочу ощутить твоё семя на своей груди...",
    ])

    $ world.battle.show_skillname("Любовь Исиды")
    play sound "se/ero_koki1.ogg"

    "Сара зажимает член Луки меж своих сисек!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2:
            world.party.player.aqua_guard()

        elif world.party.player.aqua > 2 and persistent.difficulty == 1 and random <= 70:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random <= 60:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random <= 45:
            world.party.player.aqua_guard()

        # 1: (470, 520), 2: (520, 570), 3: (570, 620)
        world.battle.enemy[0].damage = {1: (480, 530), 2: (530, 580), 3: (580, 630)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 20, 2: 15, 3: 10}):
            world.party.player.wind_guard()
            renpy.return_statement()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()
        else:
            renpy.show("saraevil ctb01", tag="ct", zorder=10)
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"
    "Руками она сжимает их, увеличивая давление!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_koki1.ogg"
    "И после начинает водить вверх-вниз, дроча Луке!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if not world.party.player.surrendered:
        hide ct

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label saraevil_ng_a3:
    world.battle.enemy[0].name "Хочу сосать...{w}\nЯ высосу всё твое семя, так что готовься..."

    $ world.battle.show_skillname("Адский минет")
    play sound "se/ero_pyu2.ogg"

    "Мягкие губки Сары обволакивают член Луки!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2:
            world.party.player.aqua_guard()

        elif world.party.player.aqua > 2 and persistent.difficulty == 1 and random <= 70:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random <= 60:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random <= 45:
            world.party.player.aqua_guard()

        # 1: (470, 520), 2: (520, 570), 3: (570, 620)
        world.battle.enemy[0].damage = {1: (480, 530), 2: (530, 580), 3: (580, 630)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 20, 2: 15, 3: 10}):
            world.party.player.wind_guard()
            renpy.return_statement()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()
        else:
            renpy.show("saraevil cta01", tag="ct", zorder=10)
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_buchu2.ogg"
    "Её влажный язык лижет головку!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_buchu3.ogg"
    "Сжимая губы у основания члена, Сара начинает сосать словно пылесос!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.status = 1
    $ world.party.player.status_print()

    "Лука впадает в транс!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if not world.party.player.surrendered:
        hide ct
    else:
        return

    l "Фуа-а-а..."
    world.battle.enemy[0].name "Хе-хе, полагаю это самое приятное, что ты испытывал...{w}\nИнтересно, какова твоя сперма на вкус..."

    if persistent.difficulty < 3:
        $ world.party.player.status_turn = rand().randint(1, 2)
    elif persistent.difficulty == 3:
        $ world.party.player.status_turn = rand().randint(2, 3)

    return

label saraevil_ng_a4:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Кончи же в мой рот...",
        "Я высосу твою сперму...",
        "Ннн... я постараюсь сосать так сильно, как могу...",
    ])

    $ world.battle.show_skillname("Любовь Исиды")
    play sound "se/ero_pyu2.ogg"

    "Сара сосёт член Луки изо всех сил!"

    $ world.battle.enemy[0].damage = {1: (480, 530), 2: (530, 580), 3: (580, 630)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_buchu2.ogg"

    "Её язык нетерпеливо лижет кончик члена!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "se/ero_buchu3.ogg"

    "Туго сжимая губы у основания члена, Сара начинает сосать словно пылесос!"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return

label saraevil_ng_a5:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Я тоже хочу... вылижи меня..."

    $ world.battle.show_skillname("Райское насаждение")
    play sound "se/ero_makituki5.ogg"

    "Сара толкает Луку на землю и садится на его лицо!"

    python:
        if world.party.player.aqua == 2 and world.party.player.wind == 5:
            world.party.player.serenegale_guard()

        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2:
            world.party.player.aqua_guard()

        elif world.party.player.aqua > 2 and persistent.difficulty == 1 and random <= 80:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random <= 75:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random <= 60:
            world.party.player.aqua_guard()

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 15, 2: 10, 3: 7}):
            world.party.player.wind_guard()
            renpy.return_statement()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()

    "Вагина Сары прямо перед глазами Луки!"

    # 1: (600, 700), 2: (670, 770), 3: (740, 840)
    $ world.battle.enemy[0].damage = {1: (610, 710), 2: (680, 780), 3: (750, 850)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.face(4)
    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Сара оседалала Луку!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.surrendered:
        return

    if world.battle.first[0]:
        world.battle.enemy[0].name "Ах... я села своим сокровенным местом прямо тебе на лицо...{w}{nw}"
    else:
        world.battle.enemy[0].name "Я так хочу, чтобы ты полизал мне... не убегай в этот раз...{w}{nw}"

    extend "\nЭй, как тебе мой запах?.."

    $ world.battle.first[0] = True

    if world.party.player.earth == 0:
        $ world.battle.enemy[0].power = world.battle.difficulty(3, 4, 5)

    elif world.party.player.earth > 0:
        $ world.battle.enemy[0].power = world.battle.difficulty(1, 2, 3)

    return

label saraevil_ng_a6:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ха-ха... кончить от того, как лижут киску...",
        "Это первый раз, когда я сажусь на мужчину вот так... хе-хе.",
        "Ну как тебе моя киска на вкус?..",
    ])

    $ world.battle.show_skillname("Райское насаждение")
    play sound "se/ero_paizuri.ogg"

    "Сара запихивает свою вагину в рот Луки!{w}\nТепло и запах от неё на секунду затуманивают его разум!"

    # 1: (600, 700), 2: (670, 770), 3: (740, 840)
    $ world.battle.enemy[0].damage = {1: (610, 710), 2: (680, 780), 3: (750, 850)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

label saraevil_ng_a7:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Интересно, я правда могу использовать этот хвост, чтобы высасывать семя?..",
        "Давай-ка испытаем мой хвост...",
        "Это приятно, когда мой хвост сосёт тебе?..",
    ])

    $ world.battle.show_skillname("Высасывание хвостом")
    play sound "se/ero_chupa6.ogg"

    "Сара запихивает свою вагину в рот Луки!{w}\nТепло и запах от неё на секунду затуманивают его разум!"

    # 1: (800, 900), 2: (870, 970), 3: (940, 1040)
    $ world.battle.enemy[0].damage = {1: (810, 910), 2: (880, 980), 3: (950, 1050)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], drain=True)

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

#label saraevil_ng_a8:
#    world.battle.enemy[0].name "Хочу немного поиграть..."
#
#    $ world.battle.show_skillname("Рот")

label saraevil_ng_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Аха-ха... ты сдался...{w}\nЭто значит, я теперь могут делать с тобой всё, что захочу~{image=note}?"

    $ world.battle.enemy[0].attack()
