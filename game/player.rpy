init python:
    class Player:
        def __init__(self, world):
            self.world = world
            self.name = Character(None)
            self.level = 0
            self.max_life = 0
            self.life = 0
            self.max_mp = 0
            self.mp = 0
            self.exp = 0
            self.skillset = 0
            self.skill = [0] * 6
            self.sylph_skill = [False] * 3
            self.gnome_skill = [False] * 3
            self.undine_skill = [False] * 4
            self.salamander_skill = [False] * 3
            self.support = False
            self.support_attack = False
            self.used_support = False
            self.seal = False
            self.turn = 0
            self.max_turn = 0
            self.level_sin = 0
            self.wind = 0
            self.wind_turn = 0
            self.earth = 0
            self.earth_turn = 0
            self.aqua = 0
            self.aqua_turn = 0
            self.fire = 0
            self.fire_turn = 0
            self.holy = 0
            self.holy_turn = 0
            self.aqua_end = False
            self.daystar = 0
            self.before_action = 0
            self.sylph_buff = 0
            self.gnome_buff = 0
            self.undine_buff = 0
            self.salamander_buff = 0
            self.giga = 0
            self.giga_damage = 0
            self.surrendered = False
            self.damage = 0
            self.exp_sin = 0
            self.guard_on = True
            self.guard = 0
            self.poison = 0
            self.poison_turn = 0
            self.blackwind = False
            self.blackwind_turn = 0
            self.pentagram = False
            self.pentagram_turn = 0
            self.titanguard = False
            self.titanguard_turn = 0
            self.enrage = False
            self.enrage_turn = 0
            self.brace = False
            self.brace_turn = 0
            self.whitewind = False
            self.whitewind_turn = 0
            self.strength = False
            self.strength_turn = 0
            self.gnomaren = 0
            self.gnomaren_turn = 0
            self.zylphe = 0
            self.zylphe_turn = 0
            self.potion = 0
            self.potion_turn = 0
            self.target_all = 0
            self.hphalf = False
            self.kiki = False
            self.bind = 0
            self.status = 0
            self.status_turn = 0
            self.nodamage = True
            self.rep = 0
            self.mp_charge = 0

        def get_stats(self):
            stats = (
                self.level,
                self.max_life,
                self.max_mp,
                self.level*5,
                5 if self.world.party.item[0] < 4 and self.world.party.item[1] != 1 else 25
            )

            return self.name[0], stats

        def get_equip(self):
            equip = (
                "Оружие: Железный меч" if self.world.party.item[0] < 5 and self.world.party.item[1] != 1 else "Оружие: Сияние Ангела",
                "Тело: Тканевая рубашка" if self.world.party.item[0] < 4 and self.world.party.item[1] != 1 else "Тело: Энрикийская рубашка",
                "Прочее: Пусто" if self.skill[0] > 18 and self.skillset != 1 else "Прочее: Памятное кольцо"
            )

            return equip

        def lvup(self, exp):
            self.exp += exp
            level_before = self.level

            self.lvup_check()

            narrator(f"{self.name[0].name} получает {self.world.battle.exp} очков опыта!")

            if self.level != level_before:
                self.level_sin = self.level
                renpy.sound.play("se/lvup.ogg")

                narrator(f"{self.name[0].name} теперь {self.level} уровня!")

        # Проверка опыта для повышения уровня
        def lvup_check(self):
            if self.exp < 10:
                self.level = 1
            elif self.exp < 20:
                self.level = 2
            elif self.exp < 40:
                self.level = 3
            elif self.exp < 80:
                self.level = 4
            elif self.exp < 160:
                self.level = 5
            elif self.exp < 320:
                self.level = 6
            elif self.exp < 530:
                self.level = 7
            elif self.exp < 800:
                self.level = 8
            elif self.exp < 1200:
                self.level = 9
            elif self.exp < 1800:
                self.level = 10
            elif self.exp < 2800:
                self.level = 11
            elif self.exp < 4000:
                self.level = 12
            elif self.exp < 6000:
                self.level = 13
            elif self.exp < 9000:
                self.level = 14
            elif self.exp < 12000:
                self.level = 15
            elif self.exp < 15000:
                self.level = 16
            elif self.exp < 19000:
                self.level = 17
            elif self.exp < 24000:
                self.level = 18
            elif self.exp < 29000:
                self.level = 19
            elif self.exp < 35000:
                self.level = 20
            elif self.exp < 41000:
                self.level = 21
            elif self.exp < 47000:
                self.level = 22
            elif self.exp < 53000:
                self.level = 23
            elif self.exp < 61000:
                self.level = 24
            elif self.exp < 70000:
                self.level = 25
            elif self.exp < 81000:
                self.level = 26
            elif self.exp < 93000:
                self.level = 27
            elif self.exp < 105000:
                self.level = 28
            elif self.exp < 117000:
                self.level = 29
            elif self.exp < 130000:
                self.level = 30
            elif self.exp < 145000:
                self.level = 31
            elif self.exp < 160000:
                self.level = 32
            elif self.exp < 180000:
                self.level = 33
            elif self.exp < 210000:
                self.level = 34
            elif self.exp < 240000:
                self.level = 35
            elif self.exp < 270000:
                self.level = 36
            elif self.exp < 300000:
                self.level = 37
            elif self.exp < 340000:
                self.level = 38
            elif self.exp < 380000:
                self.level = 39
            elif self.exp < 430000:
                self.level = 40
            elif self.exp < 480000:
                self.level = 41
            elif self.exp < 540000:
                self.level = 42
            elif self.exp < 600000:
                self.level = 43
            elif self.exp < 670000:
                self.level = 44
            elif self.exp < 740000:
                self.level = 45
            elif self.exp < 810000:
                self.level = 46
            elif self.exp < 880000:
                self.level = 47
            elif self.exp < 960000:
                self.level = 48
            elif self.exp < 1040000:
                self.level = 49
            elif self.exp < 1120000:
                self.level = 50
            elif self.exp < 1200000:
                self.level = 51
            elif self.exp < 1300000:
                self.level = 52
            elif self.exp < 1400000:
                self.level = 53
            elif self.exp < 1500000:
                self.level = 54
            elif self.exp < 1650000:
                self.level = 55
            elif self.exp < 1800000:
                self.level = 56
            elif self.exp < 1950000:
                self.level = 57
            elif self.exp < 2100000:
                self.level = 58
            elif self.exp < 2300000:
                self.level = 59
            elif self.exp < 2500000:
                self.level = 60
            elif self.exp < 2700000:
                self.level = 61
            elif self.exp < 3000000:
                self.level = 62
            elif self.exp < 3400000:
                self.level = 63
            elif self.exp < 3800000:
                self.level = 64
            elif self.exp < 4200000:
                self.level = 65
            elif self.exp < 4600000:
                self.level = 66
            elif self.exp < 5000000:
                self.level = 67
            elif self.exp < 5500000:
                self.level = 68
            elif self.exp < 6100000:
                self.level = 69
            elif self.exp < 6800000:
                self.level = 70
            elif self.exp < 7600000:
                self.level = 71
            elif self.exp < 8500000:
                self.level = 72
            elif self.exp < 9500000:
                self.level = 73
            elif self.exp < 11000000:
                self.level = 74
            elif self.exp < 12500000:
                self.level = 75
            elif self.exp < 14000000:
                self.level = 76
            elif self.exp < 16000000:
                self.level = 77
            elif self.exp < 18000000:
                self.level = 78
            elif self.exp < 20000000:
                self.level = 79
            elif self.exp < 22000000:
                self.level = 80
            elif self.exp < 24000000:
                self.level = 81
            elif self.exp < 26000000:
                self.level = 82
            elif self.exp < 28000000:
                self.level = 83
            elif self.exp < 30000000:
                self.level = 84
            elif self.exp < 32000000:
                self.level = 85
            elif self.exp < 34000000:
                self.level = 86
            elif self.exp < 36000000:
                self.level = 87
            elif self.exp < 38000000:
                self.level = 88
            elif self.exp < 40000000:
                self.level = 89
            elif self.exp < 43000000:
                self.level = 90
            elif self.exp < 46000000:
                self.level = 91
            elif self.exp < 49000000:
                self.level = 92
            elif self.exp < 53000000:
                self.level = 93
            elif self.exp < 56000000:
                self.level = 94
            elif self.exp < 59000000:
                self.level = 95
            elif self.exp < 63000000:
                self.level = 96
            elif self.exp < 67000000:
                self.level = 97
            elif self.exp < 71000000:
                self.level = 98
            elif self.exp < 75000000:
                self.level = 99
            elif self.exp < 80000000:
                self.level = 100
                self.exp_sin = 80000000
            # Расчёт опыта для >100 уровней
            elif self.exp > self.exp_sin + (self.level ** 3 * 10 / 2):
                self.exp_sin += (self.level ** 3 * 10 / 2)
                self.level += 1

            self.calc()
            self.spirit_buff()

        def calc(self):
            self.maxhp_calc()
            self.maxmp_calc()

        def maxhp_calc(self):
            if self.level  == 1:
                self.max_life = 30
            elif self.level  == 2:
                self.max_life = 40
            elif self.level  == 3:
                self.max_life = 50
            elif self.level  == 4:
                self.max_life = 60
            elif self.level  == 5:
                self.max_life = 70
            elif self.level  == 6:
                self.max_life = 80
            elif self.level  == 7:
                self.max_life = 90
            elif self.level  == 8:
                self.max_life = 100
            elif self.level  == 9:
                self.max_life = 110
            elif self.level  == 10:
                self.max_life = 120
            elif self.level  == 11:
                self.max_life = 130
            elif self.level  == 12:
                self.max_life = 140
            elif self.level  == 13:
                self.max_life = 150
            elif self.level  == 14:
                self.max_life = 160
            elif self.level  == 15:
                self.max_life = 170
            elif self.level  == 16:
                self.max_life = 180
            elif self.level  == 17:
                self.max_life = 190
            elif self.level  == 18:
                self.max_life = 200
            elif self.level  == 19:
                self.max_life = 215
            elif self.level  == 20:
                self.max_life = 230
            elif self.level  == 21:
                self.max_life = 245
            elif self.level  == 22:
                self.max_life = 260
            elif self.level  == 23:
                self.max_life = 275
            elif self.level  == 24:
                self.max_life = 290
            elif self.level  == 25:
                self.max_life = 305
            elif self.level  == 26:
                self.max_life = 320
            elif self.level  == 27:
                self.max_life = 335
            elif self.level  == 28:
                self.max_life = 350
            elif self.level  == 29:
                self.max_life = 370
            elif self.level  == 30:
                self.max_life = 390
            elif self.level  == 31:
                self.max_life = 410
            elif self.level  == 32:
                self.max_life = 430
            elif self.level  == 33:
                self.max_life = 450
            elif self.level  == 34:
                self.max_life = 470
            elif self.level  == 35:
                self.max_life = 490
            elif self.level  == 36:
                self.max_life = 510
            elif self.level  == 37:
                self.max_life = 530
            elif self.level  == 38:
                self.max_life = 550
            elif self.level  == 39:
                self.max_life = 580
            elif self.level  == 40:
                self.max_life = 610
            elif self.level  == 41:
                self.max_life = 640
            elif self.level  == 42:
                self.max_life = 670
            elif self.level  == 43:
                self.max_life = 700
            elif self.level  == 44:
                self.max_life = 730
            elif self.level  == 45:
                self.max_life = 760
            elif self.level  == 46:
                self.max_life = 790
            elif self.level  == 47:
                self.max_life = 820
            elif self.level  == 48:
                self.max_life = 860
            elif self.level  == 49:
                self.max_life = 900
            elif self.level  == 50:
                self.max_life = 940
            elif self.level  == 51:
                self.max_life = 980
            elif self.level  == 52:
                self.max_life = 1020
            elif self.level  == 53:
                self.max_life = 1060
            elif self.level  == 54:
                self.max_life = 1100
            elif self.level  == 55:
                self.max_life = 1150
            elif self.level  == 56:
                self.max_life = 1200
            elif self.level  == 57:
                self.max_life = 1250
            elif self.level  == 58:
                self.max_life = 1300
            elif self.level  == 59:
                self.max_life = 1350
            elif self.level  == 60:
                self.max_life = 1400
            elif self.level  == 61:
                self.max_life = 1450
            elif self.level  == 62:
                self.max_life = 1500
            elif self.level  == 63:
                self.max_life = 1550
            elif self.level  == 64:
                self.max_life = 1600
            elif self.level  == 65:
                self.max_life = 1650
            elif self.level  == 66:
                self.max_life = 1700
            elif self.level  == 67:
                self.max_life = 1750
            elif self.level  == 68:
                self.max_life = 1800
            elif self.level  == 69:
                self.max_life = 1850
            elif self.level  == 70:
                self.max_life = 1900
            elif self.level  == 71:
                self.max_life = 1950
            elif self.level  == 72:
                self.max_life = 2000
            elif self.level  == 73:
                self.max_life = 2050
            elif self.level  == 74:
                self.max_life = 2100
            elif self.level  == 75:
                self.max_life = 2150
            elif self.level  == 76:
                self.max_life = 2200
            elif self.level  == 77:
                self.max_life = 2250
            elif self.level  == 78:
                self.max_life = 2300
            elif self.level  == 79:
                self.max_life = 2350
            elif self.level  == 80:
                self.max_life = 2400
            elif self.level  == 81:
                self.max_life = 2450
            elif self.level  == 82:
                self.max_life = 2500
            elif self.level  == 83:
                self.max_life = 2550
            elif self.level  == 84:
                self.max_life = 2600
            elif self.level  == 85:
                self.max_life = 2650
            elif self.level  == 86:
                self.max_life = 2700
            elif self.level  == 87:
                self.max_life = 2750
            elif self.level  == 88:
                self.max_life = 2800
            elif self.level  == 89:
                self.max_life = 2900
            elif self.level  == 90:
                self.max_life = 3000
            elif self.level  == 91:
                self.max_life = 3100
            elif self.level  == 92:
                self.max_life = 3200
            elif self.level  == 93:
                self.max_life = 3300
            elif self.level  == 94:
                self.max_life = 3400
            elif self.level  == 95:
                self.max_life = 3500
            elif self.level  == 96:
                self.max_life = 3600
            elif self.level  == 97:
                self.max_life = 3700
            elif self.level  == 98:
                self.max_life = 3800
            elif self.level  == 99:
                self.max_life = 3900
            elif self.level  == 100:
                self.max_life = 4000
            # Расчёт HP для >100 уровней
            else:
                self.max_life = (self.level-100) * 100 + 4000

        # Зависимость SP от уровня
        def maxmp_calc(self):
            if self.level < 8:
                self.max_mp = 2
            elif self.level < 15:
                self.max_mp = 3
            elif self.level < 23:
                self.max_mp = 4
            elif self.level < 30:
                self.max_mp = 5
            elif self.level < 37:
                self.max_mp = 6
            elif self.level < 44:
                self.max_mp = 7
            elif self.level < 51:
                self.max_mp = 8
            elif self.level < 58:
                self.max_mp = 9
            elif self.level < 65:
                self.max_mp = 10
            elif self.level < 72:
                self.max_mp = 11
            elif self.level < 79:
                self.max_mp = 12
            elif self.level < 81:
                self.max_mp = 13
            elif self.level < 84:
                self.max_mp = 14
            elif self.level < 86:
                self.max_mp = 15
            elif self.level < 88:
                self.max_mp = 16
            elif self.level < 90:
                self.max_mp = 17
            elif self.level < 92:
                self.max_mp = 18
            elif self.level < 94:
                self.max_mp = 19
            elif self.level < 96:
                self.max_mp = 20
            elif self.level < 98:
                self.max_mp = 21
            elif self.level < 99:
                self.max_mp = 22
            elif self.level == 100:
                self.max_mp = 23
            # Расчёт SP для >100 уровней
            else:
                self.max_mp = int(23+3/5*(self.level-100))

        # Регенерация SP
        def mp_regen(self):
            if not self.skill[0]:
                return

            self.mp += 1

            if self.mp > self.max_mp:
                self.mp = self.max_mp

        def attack(self, multi_hit=False):
            self.before_action = 1

            if not multi_hit:
                self.world.battle.cry(self.name[0], [
                    "Хаа!",
                    "Ораа!",
                    "Получай!"
                ])

            narrator(f"{self.name[0].name} атакует!")

            if not multi_hit:
                self.mp_regen()

            renpy.sound.play("se/slash.ogg")

            if self.fire == 0:
                self.damage = 100 + self.level
            elif self.fire == 1:
                self.damage = 120 + self.level * 6 / 5
            elif self.fire == 2:
                self.damage = 170 + self.level * 17 / 10
            elif self.fire == 3:
                self.damage = 200 + self.level * 2

            self.world.battle.enemy[0].life_calc()

        def attack_critical(self, multi_hit=False):
            self.before_action = 2
            self.world.battle.cry(self.name[0], [
                "Открылась!",
                "СУУУПЕР АТАКААА",
                "Сейчас ты у меня получишь!"
            ])

            narrator("Критическое попадание!")

            if not multi_hit:
                self.mp_regen()

            renpy.sound.play("se/slash3.ogg")

            if self.fire == 0:
                self.damage = 170 + self.level * 17 / 10
            elif self.fire == 1:
                self.damage = 200 + self.level * 2
            elif self.fire == 2:
                self.damage = 250 + self.level * 5 / 2
            elif self.fire == 3:
                self.damage = 300 + self.level * 3

            self.world.battle.enemy[0].life_calc()

        def attack_miss(self):
            self.before_action = 2
            self.world.battle.cry(self.name[0], [
                "Чёрт!",
                "Ух-ох!",
                "Дерьмо!"
            ])

            narrator(f"{self.name[0].name} атакует!")

            self.mp_regen()
            renpy.sound.play("se/miss.ogg")

            narrator("Промах!")

        def attack_aqua(self):
            self.before_action = 4
            self.world.battle.cry(self.name[0], [
                "..............",
                "Вижу!",
                "Вверяя свой меч потоку..."
            ])

            self.world.battle.show_skillname("Клинок Стоячей Воды")

            narrator("Меч Луки, сверкая, направляется к противнику!")

            if self.aqua > 2:
                self.mp_regen()

            if self.aqua > 2:
                self.damage = 140 + self.level * 7 / 5

            if self.aqua == 2:
                self.damage = 440 + self.level * 4

            if self.earth == 3:
                self.damage = self.damage * 125 / 100

            if self.fire == 3:
                self.damage = self.damage * 125 / 100

            renpy.sound.play("se/slash4.ogg")

            self.world.battle.enemy[0].life_calc()
            self.world.battle.hide_skillname()

            if not (self.wind == 3 or self.holy == 3):
                return

            self.world.battle.show_skillname("Клинок Стоячей Воды")

            narrator("Сверкая подобно молнии, меч Луки возвращается в ножны!")

            if self.aqua > 2:
                self.damage = 140 + self.level * 7 / 5

            if self.aqua == 2:
                self.damage = 440 + self.level * 4

            if self.earth == 3:
                self.damage = self.damage * 125 / 100

            if self.fire == 3:
                self.damage = self.damage * 125 / 100

            renpy.sound.play("se/slash4.ogg")

            self.world.battle.enemy[0].life_calc()
            self.world.battle.hide_skillname()

        def struggle(self):
            self.before_action = 7
            self.world.battle.cry(self.name[0], [
                "Угх...",
                "Грр... Чёрт возьми!",
                "Отпусти меня!",
                "Гх...",
                "Гх... отстань!",
                "Грр... отпусти!"
            ])

            if self.world.battle.result == 1:
                narrator(f"{self.name[0].name} пытается атаковать!")

                self.mp_regen()

            elif self.world.battle.result == 2:
                narrator(f"{self.name[0].name} начинает вырываться!")

            for enemy in self.world.battle.enemy:
                if enemy is not None:
                    if enemy.power != -1:
                        if self.world.battle.result == 2:
                            enemy.power -= 1

                            if enemy.power < 0:
                                enemy.power = 0

            if self.world.battle.result == 1:
                narrator(f"{self.world.battle.enemy[0].power_desc[2]}")
            elif self.world.battle.result == 2:
                if self.earth:
                    narrator(f"Тело {self.name[1].name} наполняет сила самой земли, давая тому небывалую мощь!")
                narrator(f"{self.world.battle.enemy[0].power_desc[0]}")

            self.world.battle.cry(self.world.battle.enemy[0].name, [
                self.world.battle.enemy[0].power_cry[0],
                self.world.battle.enemy[0].power_cry[1],
                self.world.battle.enemy[0].power_cry[2],
                self.world.battle.enemy[0].power_cry[3],
                self.world.battle.enemy[0].power_cry[4]
            ])

        def escape(self):
            self.before_action = 8

            if persistent.difficulty == 2:
                random = rand().randint(1, 10)
                if random < 2:
                    self.struggle()
            elif persistent.difficulty == 3:
                random = rand().randint(1, 2)
                if random < 2:
                    self.struggle()

            if self.earth:
                renpy.sound.play("se/power.ogg")

                self.world.battle.cry(self.name[0], [
                    "С этой силой!..",
                    "С силой Гномы!...",
                    "С самой силой земли!.."
                ])
            else:
                self.world.battle.cry(self.name[0], [
                    "Грр....!",
                    "Чёрт тебя побери...!",
                    "О-отпусти!"
                ])

            renpy.sound.play("se/dassyutu.ogg")

            self.world.battle.face(3)

            self.bind = 0
            self.status_print()

            narrator(f"{self.name[0].name} борется интенсивнее и вырывается на свободу!")

            if self.world.battle.enemy[0].power_escaped[0]:
                self.world.battle.enemy[0].name(f"{self.world.battle.enemy[0].power_escaped[0]}")

            if self.world.battle.enemy[0].power_escaped[1]:
                self.world.battle.enemy[0].name(f"{self.world.battle.enemy[0].power_escaped[1]}")

            self.world.battle.face(1)
            self.world.battle.main()

        def attack_struggle(self, damage, multi_hit=False):
            self.before_action = 9
            self.world.battle.cry(self.name[0], [
                "Хаа!",
                "Ораа!",
                "Получай!"
            ])

            narrator(f"{self.name[0].name} атакует!")

            if self.world.battle.enemy[0].power_desc[3]:
                narrator(f"{self.world.battle.enemy[0].power_desc[3]}")

            if not multi_hit:
                self.mp_regen()

            renpy.sound.play("se/slash.ogg")
            self.damage = damage
            self.world.battle.enemy[0].life_calc()
            self.world.battle.enemy[0].bind_hp -= damage

            if not self.world.battle.enemy[0].life:
                self.bind = 0
                self.status_print()

        def skill_set(self, s):
            self.skillset = s
            if s == 2:
                renpy.sound.play("se/fire4.ogg", "sound2")
                renpy.pause(0.6)
                renpy.sound.play("se/laser2.ogg", "sound2")
                renpy.pause(0.6)
                renpy.sound.play("se/bom3.ogg", "sound2")

        # Высасывание уровня
        def drain(self, drain):
            self.level -= drain

            if self.level < 1:
                self.level = 1

            if self.level == 1:
                persistent.count_mylv0 = True

            self.calc()

            if self.max_life < self.life:
                self.life = self.max_life

            renpy.sound.play("audio/se/lvdown.ogg")

            narrator(f"Уровень {self.name[1].name} уменьшился на {drain} единиц!")

            self.world.battle.enemy[0].level += drain
            persistent.count_drainlv += drain

            narrator(f"Уровень {self.world.battle.name.name} увеличился на {drain} единиц!")

        # Усиление духов от отношения
        def spirit_buff(self):
            if not self.skill[1]:
                self.sylph_buff = 0
            else:
                self.sylph_buff = self.world.party.sylph_rep / 2

            if not self.skill[2]:
                self.gnome_buff = 0
            else:
                self.gnome_buff = self.world.party.gnome_rep / 2

            if not self.skill[3]:
                self.undine_buff = 0
            else:
                self.undine_buff = self.world.party.undine_rep / 2

            if not self.skill[4]:
                self.salamander_buff = 0
            else:
                self.salamander_buff = self.world.party.salamander_rep / 2

            if not self.world.wind and not self.world.earth and not self.world.aqua and not self.world.fire:
                return

            self.world.party.buff()

        # Уклонение безмятежным разумом
        def aqua_guard(self):
            self.world.battle.hide_ct()
            self.world.battle.show_skillname("Безмятежное движение")
            renpy.sound.play("se/miss_aqua.ogg")
            if self.aqua != 2:
                renpy.show(self.world.battle.background, at_list=[miss2])

            narrator(f"Но {self.name[0].name} уклоняется, словно поток воды, обегающий препятствие!")

            self.world.battle.hide_skillname()
            self.world.battle.main()

        def wind_guard_calc(self, evasion):
            if self.status in (1, 2, 4, 5, 7):
                return False

            random = rand().randint(1, 100)

            if random < evasion[persistent.difficulty]+1:
                return True

            return False

        # Уклонение разными типами ветра
        def wind_guard(self):
            if self.wind == 1:
                self.world.battle.show_skillname("Ветрянная Защита")
                renpy.sound.play("se/wind2.ogg")
            elif self.wind == 2:
                self.world.battle.show_skillname("Танец Ветра")
                renpy.sound.play("se/miss.ogg")
            elif self.wind == 3:
                self.world.battle.show_skillname("Ураган")
                renpy.sound.play("se/miss_wind.ogg")
                line = "Но Лука движется словно торнадо и уклоняется!"
            elif self.holy < 3:
                self.world.battle.show_skillname("Танец Падшего Ангела")
                renpy.sound.play("se/bun.ogg")
                lightwave((self.world.battle.background, self.world.battle.enemy[0].sprite[0]))
                self.world.battle.face(1)
                line = "Но Лука, подхваченный священной волной, уходит от атаки!"
            elif self.holy > 2:
                self.world.battle.show_skillname("Танец Падшего Ангела: Буря")
                renpy.sound.play("se/bun.ogg")
                lightwave((self.world.battle.background, self.world.battle.enemy[0].sprite[0]))
                self.world.battle.face(1)
                line = "Но Лука, подхваченный святой волной и ветром, уходит от атаки!"

            self.world.battle.hide_ct()

            if line:
                narrator(f"{line}")

            self.world.battle.hide_skillname()

        # Фразы при достижении 1/2 HP
        def common_s1(self):
            if self.max_life > self.life * 5:
                self.hphalf = True
                self.common_s2()

            self.world.battle.cry(self.name[0], [
                "Гх...",
                "Ох...",
                "Угх..."
            ])

            self.world.battle.face(2)

            if self.world.battle.enemy[0].half[0]:
                self.world.battle.enemy[0].name(f"{self.world.battle.enemy[0].half[0]}")

            if self.world.battle.enemy[0].half[1]:
                self.world.battle.enemy[0].name(f"{self.world.battle.enemy[0].half[1]}")

            self.world.battle.face(1)
            self.hphalf = True

        # Фразы при достижении 1/5 HP
        def common_s2(self):
            self.world.battle.cry(self.name[0], [
                "Ахх...",
                "Ауу...",
                "Ууу..."
            ])

            self.world.battle.face(2)
            sel1 = self.world.battle.enemy[0].kiki[0]
            sel2 = self.world.battle.enemy[0].kiki[1]

            if self.world.battle.enemy[0].kiki[0]:
                self.world.battle.enemy[0].name(f"{self.world.battle.enemy[0].kiki[0]}")

            if self.world.battle.enemy[0].kiki[1]:
                self.world.battle.enemy[0].name(f"{self.world.battle.enemy[0].kiki[1]}")

            self.kiki = True

        def status_reset(self):
            self.poison = 0
            self.poison_turn = -1
            self.whitewind = False
            self.whitewind_turn = -1
            self.brace = False
            self.brace_turn = -1
            self.strength = False
            self.strength_turn = -1
            self.pentagram = False
            self.pentagram_turn = -1
            self.world.party.pentagram = False
            self.world.party.pentagram_turn = -1
            self.zylphe = 0
            self.zylphe_turn = -1
            self.gnomaren = 0
            self.gnomaren_turn = -1
            self.potion = 0
            self.potion_turn = -1
            self.immune = 0
            self.life_boost = 0
            self.max_turn = 1
            self.lift_turn = -1
            self.world.party.windbuff = 0
            self.world.party.windbuff_turn = -1
            self.enrage = False
            self.enrage_turn = -1
            self.blackwind = False
            self.blackwind_turn = -1
            self.titanguard = False
            self.titanguard_turn = -1

        def element_end(self):
            if self.wind and not self.wind_turn:
                self.wind = 0

                narrator("Сила ветра исчезает!")

            if self.holy and not self.holy_turn:
                self.holy = 0

                narrator("Танец Падшего Ангела заканчивается!")

            if self.earth and not self.earth_turn:
                self.earth = 0

                narrator("Сила земли исчезает!")

            if self.aqua and not self.aqua_turn:
                self.aqua = 0

                if self.skill[3]:
                    narrator("Сила воды исчезает!")
                else:
                    narrator("Состояние безмятежности проходит!")

            if self.fire and not self.fire_turn:
                self.fire = 0

                narrator("Сила огня исчезает!")

                if self.fire == 2:
                    self.mp = 0

        def serene_end(self):
            renpy.music.stop(fadeout=1)
            renpy.show(self.world.battle.background)
            renpy.with_statement(Dissolve(1.5))
            self.aqua = 0
            self.aqua_turn = 0
            self.aqua_end = False
            self.world.battle.musicplay(self.world.battle.music)

            self.element_end()

            renpy.pause(1.0)

        def guard_break(self, factor = 2):
            defense_break = (100 - self.world.battle.enemy[0].defense) // factor
            self.world.battle.enemy[0].defense = self.world.battle.enemy[0].defense + defense_break

            if self.world.battle.enemy[0].defense > 100:
                self.world.battle.enemy[0].defense = 100

        def skill_result(self):
            if self.world.battle.result == 1:
                self.skill1()                                         # Демоническое обезглавливание
            elif self.world.battle.result == 2 and self.skill[0] > 1 and self.skill[0] < 6:
                self.skill2()                                         # Беспорядочные взмахи
            elif self.world.battle.result == 2 and self.skill[0] > 5:
                self.skill6()                                         # Гибельный Клинок Звезды Хаоса
            elif self.world.battle.result == 3:
                self.skill3()                                         # Грозовой выпад
            elif self.world.battle.result == 4:
                self.skill4()                                         # Медитация
            elif self.world.battle.result == 5:
                self.skill5()                                         # Небесный Крушитель Черепов
            elif self.world.battle.result == 6:
                self.skill11()                                        # Молниеносный Мгновенный Клинок
            elif self.world.battle.result == 7:
                self.skill12()                                        # Сокрушающее Землю Обезглавливание
            elif self.world.battle.result == 8:
                self.skill13()                                        # Безмятежный Демонический Клинок
            elif self.world.battle.result == 9:
                self.skill15()                                        # Четырёхкратный Гига-удар
            elif self.world.battle.result == 10:
                self.skill16()                                        # Неудержимый Испепеляющий Клинок
            elif self.world.battle.result == 11 and self.skill[1] == 1:
                self.skill7()                                         # Сильфа 1 лвла
            elif self.world.battle.result == 11 and self.skill[1] == 2:
                self.skill7b()                                        # Сильфа 2 лвла
            elif self.world.battle.result == 11 and self.skill[1] == 3:
                self.skill7c()                                        # Сильфа 3 лвла
            elif self.world.battle.result == 12 and self.skill[2] == 1:
                self.skill8()                                         # Гнома 1 лвла
            elif self.world.battle.result == 12 and self.skill[2] == 2:
                self.skill8b()                                        # Гнома 2 лвла
            elif self.world.battle.result == 12 and self.skill[2] == 3:
                self.skill8c()                                        # Гнома 3 лвла
            elif self.world.battle.result == 12 and self.skill[2] == 4:
                self.skill8_ng4()                                     # Гнома 4 лвла NG+
            elif self.world.battle.result == 13 and self.skill[3] == 1:
                self.skill10()                                        # Дерьмовая Ундина
            elif self.world.battle.result == 13 and (self.skill[3] == 2 or self.undine_skill[0]):
                self.skill10b()                                       # Хорошая Ундина
            elif self.world.battle.result == 13 and self.skill[3] == 3:
                self.skill10c()                                       # Фиговая Ундина
            elif self.world.battle.result == 14 and self.skill[4] == 1:
                self.skill14()                                        # Саламандра лвл 1
            elif self.world.battle.result == 14 and self.skill[4] == 2:
                self.skill14b()                                       # Саламандра лвл 2
            elif self.world.battle.result == 14 and self.skill[4] == 3:
                self.skill14c()                                       # Саламандра лвл 3
            elif self.world.battle.result == 15:
                self.skill17()                                        # Крайность
            elif self.world.battle.result == 16:
                self.skill18()                                        # Безмятежный разум
            elif self.world.battle.result == 17:
                self.skill19()                                        # Мгновенное убийство
            elif self.world.battle.result == 18:
                self.skill20()                                        # Девятиликий Ракшаса (какого хуя это делает перед HDR?)
            elif self.world.battle.result == 19:
                self.skill21()                                        # Возрождение Небесного Демона
            elif self.world.battle.result == 24:
                self.skill22()                                        # Денница
            elif self.world.battle.result == 25 and self.skill[1] < 2:
                self.skill23()                                        # Танец Падшего Ангела
            elif self.world.battle.result == 25 and self.skill[1] == 2:
                self.skill7d()                                        # Танец Падшего Ангела с Сильфой
            elif self.world.battle.result == 25 and self.skill[1] == 3:
                self.skill7e()                                        # Танец Падшего Ангела: Буря
            elif self.world.battle.result == 22:
                self.skill24()                                        # Элементальная Спика
            elif self.world.battle.result == 23:
                self.skill25()                                        # Вызов четырёх духов
            elif self.world.battle.result == 26:
                self.skill26()                                        # Разгон

            elif self.world.battle.result == 31:
                self.syskill1()                                       # Белые ветра
            elif self.world.battle.result == 32:
                self.syskill2()                                       # Чи-па-па!
            elif self.world.battle.result == 33:
                self.syskill3()                                       # Отмена земли

            elif self.world.battle.result == 41:
                self.gnskill1()                                       # Захват
            elif self.world.battle.result == 42:
                self.gnskill2()                                       # Сила земли
            elif self.world.battle.result == 43:
                self.gnskill3()                                       # Отмена ветра

            elif self.world.battle.result == 51:
                self.unskill1()                                       # Очищение
            elif self.world.battle.result == 52:
                self.unskill2()                                       # Аква-пентаграмма
            elif self.world.battle.result == 53:
                self.unskill3()                                       # Фаер-резист

            elif self.world.battle.result == 61:
                self.saskill1()                                       # Берсерк
            elif self.world.battle.result == 62:
                self.saskill2()                                       # Сгори!
            elif self.world.battle.result == 63:
                self.saskill3()                                       # Отмена воды

            elif self.world.battle.result == 101:
                self.world.battle.call_cmd("skill_sylph")                        # Дисплей Сильфы
                self.skill_result()
            elif self.world.battle.result == 102:
                self.world.battle.call_cmd("skill_gnome")                        # Дисплей Гномы
                self.skill_result()
            elif self.world.battle.result == 103:
                self.world.battle.call_cmd("skill_undine")                       # Дисплей Ундины
                self.skill_result()
            elif self.world.battle.result == 104:
                self.world.battle.call_cmd("skill_salamander")                   # Дисплей Саламандры
                self.skill_result()

            elif self.world.battle.result == 201:
                self.grskill1()
            elif self.world.battle.result == 202:
                self.grskill2()
            elif self.world.battle.result == 203:
                self.grskill3()
            elif self.world.battle.result == 204:
                self.grskill4()
            elif self.world.battle.result == 205:
                self.grskill5()
            elif self.world.battle.result == 206:
                self.grskill6()
            elif self.world.battle.result == 207:
                self.grskill7()

            elif self.world.battle.result == 301:
                self.alskill1()
            elif self.world.battle.result == 302:
                self.alskill2()
            elif self.world.battle.result == 303:
                self.alskill3()
            elif self.world.battle.result == 304:
                self.alskill4()

            if self.world.battle.result == -1:
                #self.world.battle.main()
                if self.world.party.player == world.actor[0]:
                    renpy.jump(f"{self.world.battle.tag}_main")

        def skill1(self):
            self.before_action = 21
            self.world.battle.cry(self.name[0], [
                "Нападаю! Демоническое Обезглавливание!",
                "Ну всё, попалась! Демоническое Обезглавливание!",
                "Получи! Демоническое Обезглавливание!"
            ])

            self.mp -= 2
            self.world.battle.show_skillname("Демоническое обезглавливание")

            narrator(f"{self.name[0].name} прыгает к ней и бьёт мечом по её шее!")

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            renpy.sound.play("se/tuki.ogg")
            renpy.show("demon decapitation", at_list=[xy(self.world.battle.enemy[0].tukix, self.world.battle.enemy[0].tukiy)], zorder=30)
            renpy.pause(0.26)
            renpy.hide("demon")
            self.damage = 150 + self.level * 3 / 2
            self.world.battle.enemy[0].life_calc()
            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill2(self):
            self.before_action = 41
            self.world.battle.cry(self.name[0], [
                "Получи, и ещё, и ещё!",
                "Один точно попадёт!",
                "Аааах!!!"
            ])

            self.mp -= 2
            self.world.battle.show_skillname("Беспорядочные взмахи")

            narrator(f"{self.name[0].name} начинает крутиться вокруг своей оси, размахивая мечом во все стороны!")

            renpy.sound.play("se/miss.ogg")

            "Промах!"

            renpy.pause(0.5)
            renpy.sound.play("se/miss.ogg")

            "Промах!"

            renpy.pause(0.5)
            renpy.sound.play("se/miss.ogg")

            "Промах!"

            renpy.pause(0.5)
            renpy.sound.play("se/miss.ogg")

            "Промах!"
            ".... Похоже это не очень эффективно..."

            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            self.world.battle.common_attack_end()

        def skill3(self):
            self.before_action = 22
            self.world.battle.cry(self.name[0], [
                "Получай! Громовой Выпад!",
                "Ха! Громовой Выпад!",
                "Нападаю! Громовой Выпад!!!"
            ])

            self.mp -= 2
            self.world.battle.show_skillname("Громовой Выпад")

            narrator(f"{self.name[0].name} ступает вперёд, подобно грому, и делает резкий выпад!")

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            if self.world.battle.turn > 1:
                self.skill3a()
            elif self.world.battle.turn == 1:
                self.skill3b

            renpy.pause(0.26)
            renpy.hide("thunder")
            self.world.battle.enemy[0].life_calc()
            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill3a(self):
            renpy.sound.play("se/tuki.ogg")
            self.damage = 120 + self.level * 6 / 5
            renpy.show("thunder thrust a", at_list=[xy(self.world.battle.enemy[0].tukix, self.world.battle.enemy[0].tukiy)], zorder=30)

        def skill3b(self):
            "Ранняя атака наносит повышенный урон!"

            renpy.sound.play("se/slash3.ogg")
            self.damage = 200 + self.level * 2
            renpy.show("thunder thrust b", at_list=[xy(self.world.battle.enemy[0].tukix, self.world.battle.enemy[0].tukiy)], zorder=30)

        def skill4(self):
            self.before_action = 42

            l(".............")

            self.mp -= 3
            self.world.battle.show_skillname("Медитация")

            extend_say(narrator, f"{self.name[0].name} спокойно медитирует.")

            renpy.sound.play("se/kaihuku.ogg")
            self.damage = self.max_life // 2

            extend_say(extend, f"\n{self.name[0].name} восстанавливает половину своего здоровья!")

            if self.life + self.damage > self.max_life:
                self.life = self.max_life
            else:
                self.life += self.damage

            if self.guard_on and self.skill[0] > 26 and self.fire < 1:
                self.guard = 2

            self.world.battle.hide_skillname()

            if self.max_life < self.life * 5:
                self.world.battle.face(1)

            self.world.battle.enemy[0].attack()

        def skill5a(self):
            _window_hide()
            renpy.hide_screen("hp")
            renpy.sound.play("se/karaburi.ogg")
            renpy.show("effect crush", zorder=30)
            renpy.pause(0.3)
            renpy.hide("effect")
            renpy.with_statement(ImageDissolve("images/Transition/mask01.webp", 1.0))
            renpy.show_screen("hp")
            _window_show(auto=True)
            renpy.sound.play("se/bom2.ogg")
            renpy.with_statement(Quake((0, 0, 0, 0), 1.0, dist=25))

        def skill5(self):
            self.before_action = 23
            self.world.battle.cry(self.name[0], [
                "Уооо! Небесный Крушитель Черепов!",
                "Как насчёт этого?! Небесный Крушитель Черепов!",
                "Попалась! Небесный Крушитель Черепов!"
            ])

            self.mp -= 3
            self.world.battle.show_skillname("Небесный Крушитель Черепов")

            if self.world.environment == 0:
                narrator(f"{self.name[0].name} залезает на возвышенность и прыгает оттуда!")
            elif self.world.environment == 1:
                narrator(f"{self.name[0].name} залезает на мачту и прыгает оттуда!")
            elif self.world.environment == 2:
                narrator(f"{self.name[0].name} залезает на дерево и прыгает оттуда!")
            elif self.world.environment == 3:
                narrator(f"{self.name[0].name} залезает на стену и прыгает оттуда!")
            elif self.world.environment == 4:
                narrator(f"{self.name[0].name} залезает на колонну и прыгает оттуда!")
            elif self.world.environment == 5:
                narrator(f"{self.name[0].name} взбирается на здание и прыгает сверху вниз!")

            self.skill5a()

            narrator(f"{self.name[0].name} наносит сокрушительный удар по её голове!")

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            renpy.sound.play("se/damage2.ogg")
            self.damage = 300 + self.level * 3
            self.world.battle.enemy[0].life_calc()
            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill6(self):
            self.before_action = 24
            self.world.battle.cry(self.name[0], [
                "Гибельный Клинок Звезды Хаоса!",
                "Съешь это! Гибельный Клинок Звезды Хаоса!",
                "Нападаю! Гибельный Клинок Звезды Хаоса!"
            ])

            self.mp -= 4
            self.world.battle.show_skillname("Гибельный Клинок Звезды Хаоса")

            narrator("Меч Луки сверкает и блестит словно хаотично падающая звезда!")

            self.skill6a()

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            count = 0
            for _ in range(5):
                renpy.sound.play("se/slash.ogg")

                if self.fire < 2:
                    self.damage = rand().randint(50+self.level*1/2, 150+self.level*3/2)
                elif self.fire == 2:
                    self.damage = rand().randint(100+self.level, 225+self.level*9/4)

                self.world.battle.enemy[0].life_calc(count+1)

                count += 1
                if count > 2:
                    count = 0

            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill6a(self):
            _window_hide()
            renpy.hide_screen("hp")
            renpy.show("death sword chaos star", zorder=30)
            renpy.pause(1.5, hard=True)
            renpy.hide("death")
            renpy.show_screen("hp")
            _window_show(auto=True)

        def skill7(self):
            self.before_action = 43
            persistent.count_wind += 1

            l("Сильфа, одолжи мне свою силу!")

            self.mp -= 2
            self.world.battle.show_skillname("Ветряная защита")
            self.wind = 1
            self.wind_turn = 8
            self.holy = 0
            self.holy_turn = 0

            if self.skill[0] < 9:
                self.earth = 0
                self.earth_turn = 0

            self.aqua = 0
            self.aqua_turn = 0
            renpy.sound.play("se/wind2.ogg")
            random = rand().randint(1, 100)

            if random != 100:
                self.world.summon("sylph st12")

            elif random == 100:
                self.world.summon("sylph st07")
                persistent.g_sylph = True

            "Могучий ветер обвивает Луку!"

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill7b(self):
            self.before_action = 43
            persistent.count_wind += 1

            l("Отдаться ветру!..")

            self.mp -= 2
            self.world.battle.show_skillname("Игривый ветер")
            self.wind = 2
            self.wind_turn = 12
            self.holy = 0
            self.holy_turn = 0

            if self.skill[0] < 9 or self.skill[1] == 4:
                self.earth = 0
                self.earth_turn = 0

            self.aqua = 0
            self.aqua_turn = 0
            renpy.sound.play("se/wind2.ogg")
            random = rand().randint(1, 100)

            if random != 100:
                self.world.summon("sylph st12")

            elif random == 100:
                self.world.summon("sylph st07")
                persistent.g_sylph = True

            "Мощные ветра кружат вокруг тела Луки, резко увеличивая его скорость!"

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill7c(self):
            self.before_action = 43
            persistent.count_wind += 1

            l("Сильфа, бушуй и свирепствуй, как тебе захочется!")

            self.mp -= 1
            self.world.battle.show_skillname("Опустошающий Ураган")
            self.wind = 3
            self.wind_turn = 16
            self.holy = 0
            self.holy_turn = 0
            renpy.sound.play("se/wind3.ogg")
            random = rand().randint(1, 100)

            if random != 100:
                self.world.summon("sylph st12")

            elif random == 100:
                self.world.summon("sylph st07")
                persistent.g_sylph = True

            "Движения Луки становятся так же быстры и мощны, как опустошительный ураган!"

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill8(self):
            self.before_action = 44
            persistent.count_earth += 1

            l("Гнома, одолжи мне свою силу!")

            self.mp -= 2
            self.world.battle.show_skillname("Сила Земли") # Попов, перелогинься
            self.earth = 1
            self.earth_turn = 8

            if self.skill[0] < 9:
                self.wind = 0
                self.wind_turn = 0
                self.holy = 0
                self.holy_turn = 0

            self.aqua = 0
            self.aqua_turn = 0
            renpy.sound.play("se/earth4.ogg")
            random = rand().randint(1, 100)

            if random != 100:
                self.world.summon("gnome st21")

            elif random == 100:
                self.world.summon("gnome st12")
                persistent.z_gnome = True

            narrator("Сила Земли растекается по телу Луки!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill8b(self):
            self.before_action = 44
            persistent.count_earth += 1

            l("Наполнить дыханием земли само моё тело!..")

            self.mp -= 2
            self.world.battle.show_skillname("Дыхание Земли")
            self.earth = 2
            self.earth_turn = 12

            if self.skill[0] < 9 or self.skill[2] == 4:
                self.wind = 0
                self.wind_turn = 0
                self.holy = 0
                self.holy_turn = 0

            self.aqua = 0
            self.aqua_turn = 0
            renpy.sound.play("se/earth1.ogg")
            random = rand().randint(1, 100)

            if random != 100:
                self.world.summon("gnome st21")

            elif random == 100:
                self.world.summon("gnome st12")
                persistent.z_gnome = True

            narrator("Дыхание земли укрепляется в теле Луки!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill8c(self):
            self.before_action = 44
            persistent.count_earth += 1

            l("Гнома, наполни меня непоколебимой силой земли!")

            self.mp -= 1
            self.world.battle.show_skillname("Дикие Земли")
            self.earth = 3
            self.earth_turn = 16
            renpy.sound.play("se/earth2.ogg")
            random = rand().randint(1, 100)

            if random != 100:
                self.world.summon("gnome st21")

            elif random == 100:
                self.world.summon("gnome st12")
                persistent.z_gnome = True

            narrator("Непоколебимая и несокрушимая сила Земли укрепляется в теле Луки!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill10(self):
            self.before_action = 45
            persistent.count_aqua += 1

            l("Ундина, одолжи мне свою силу!")

            self.mp -= 2
            self.world.battle.show_skillname("Водный барьер")
            self.aqua = 1
            self.aqua_turn = 8
            self.wind = 0
            self.wind_turn = 0
            self.holy = 0
            self.holy_turn = 0
            self.earth = 0
            self.earth_turn = 0
            self.fire = 0
            self.fire_turn = 0
            renpy.sound.play("se/aqua2.ogg")
            self.world.summon("undine st11")

            narrator("Тонкая стенка воды окружает Луку!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill10b(self):
            self.before_action = 45
            persistent.count_aqua += 1

            l("Принять течение в сердце!..")

            self.world.battle.show_skillname("Абсолютный безмятежный разум")
            self.aqua = 2
            self.aqua_turn = self.mp
            self.wind = 0
            self.wind_turn = 0
            self.holy = 0
            self.holy_turn = 0
            self.earth = 0
            self.earth_turn = 0
            self.fire = 0
            self.fire_turn = 0
            world.battle.enemy[0].aqua = 0

            renpy.sound.play("se/aqua.ogg")
            self.world.summon("undine st11")

            renpy.music.stop(fadeout=1)
            renpy.show("bg black")
            renpy.with_statement(Dissolve(1.5))
            renpy.music.play("bgm/aqua.ogg")

            narrator(f"{self.name[0].name} доверяет своё сердце течению воды!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill10c(self):
            self.before_action = 45
            persistent.count_aqua += 1

            l("Успокоить своё сердце и разум...")

            self.world.battle.show_skillname("Безмятежный разум")

            if self.skill[0] < 17:
                self.mp -= 4
            elif self.skill[0] > 17:
                self.mp -= 2

            self.aqua = 3
            self.aqua_turn = 16
            renpy.sound.play("se/aqua.ogg")
            self.world.summon("undine st11")

            narrator(f"{self.name[0].name} очищает свой разум и объединяется с мировым течением!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill11(self):
            self.before_action = 25
            if self.skill[1]:
                self.world.battle.cry(self.name[0], [
                    "С ветром, направляющим мой меч... Молниеносный Удар Клинка!",
                    "С силой Сильфы!.. Молниеносный Удар Клинка!",
                    "Нападаю! Молниеносный Удар Клинка!"
                ])
            else:
                self.world.battle.cry(self.name[0], [
                    "С ветром, направляющим мой меч... Молниеносный Удар Клинка!",
                    "Нападаю! Молниеносный Удар Клинка!"
                ])

            self.mp -= 2

            if self.aqua == 2 and self.skill[3]:
                self.world.battle.show_skillname("Молниеносный Удар Клинка: Громовой раскат")
            elif self.aqua > 2 and self.skill[3]:
                self.world.battle.show_skillname("Молниеносный Удар Клинка: Циклон")
            else:
                self.world.battle.show_skillname("Молниеносный Удар Клинка")

            renpy.sound.play("se/wind2.ogg")

            if self.wind and self.skill[1]:
                renpy.show("cutin_ruka_wind", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(0.7)
                renpy.show("cutin_wind", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(0.7)
                renpy.hide("cutin_ruka_wind")
                renpy.pause(.1)
                renpy.hide("cutin_wind")
                renpy.pause(.1)

            if self.aqua and self.skill[3]:
                renpy.show("cutin_ruka_aqua", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(0.7)
                renpy.show("cutin_wind", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(0.7)
                renpy.hide("cutin_ruka_aqua")
                renpy.pause(.1)
                renpy.hide("cutin_wind")
                renpy.pause(.1)

            narrator(f"Двигаясь, словно мощный порыв ветра, {self.name[0].name} делает быстрый, словно молния, выпад вперёд!")

            if self.world.battle.turn > 1 and not self.wind:
                self.skill11a()
            elif self.world.battle.turn == 1:
                self.skill11b()
            elif 0 < self.wind < 5:
                self.skill11c()

            renpy.pause(0.1)
            renpy.hide("thunder")

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            self.world.battle.enemy[0].life_calc()
            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill11a(self):
            self.damage = 220 + self.level * 11 / 5

            if self.fire > 1:
                self.damage = damage * 3 / 2

            renpy.sound.play("se/tuki.ogg")
            renpy.show("effect wind", zorder=30)
            renpy.pause(0.05)
            renpy.show("thunder thrust a", at_list=[xy(self.world.battle.enemy[0].tukix, self.world.battle.enemy[0].tukiy)], zorder=30)
            renpy.pause(0.25)
            renpy.hide("effect")
            renpy.hide("thunder")

        def skill11b(self):
            narrator("Ранняя атака наносит повышенный урон!")

            self.damage = 350 + self.level * 7 / 2
            renpy.sound.play("se/slash3.ogg")
            renpy.show("effect wind", zorder=30)
            renpy.pause(0.05)
            renpy.show("thunder thrust b", at_list=[xy(self.world.battle.enemy[0].tukix, self.world.battle.enemy[0].tukiy)], zorder=30)
            renpy.pause(0.25)
            renpy.hide("effect")
            renpy.hide("thunder")

        def skill11c(self):
            self.damage = 350 + self.level * 7 / 2

            if self.aqua and self.skill[3]:
                self.damage = self.damage * 4 / 3

            if self.fire > 1:
                self.damage = self.damage * 3 / 2

            renpy.sound.play("se/slash3.ogg")
            renpy.show("effect wind", zorder=30)
            renpy.pause(0.05)
            renpy.show("thunder thrust b", at_list=[xy(self.world.battle.enemy[0].tukix, self.world.battle.enemy[0].tukiy)], zorder=30)
            renpy.pause(0.25)
            renpy.hide("effect")
            renpy.hide("thunder")

        def skill12a(self):
            renpy.sound.play("se/karaburi.ogg")
            renpy.show("effect earth", zorder=30)
            renpy.pause(0.3, hard=True)
            renpy.hide("effect")
            renpy.with_statement(ImageDissolve("images/Transition/mask01.webp", 0.5))
            renpy.sound.play("se/bom2.ogg")
            renpy.with_statement(Quake((0, 0, 0, 0), 1.0, dist=30))

        def skill12(self):
            self.before_action = 26
            if self.skill[2]:
                self.world.battle.cry(self.name[0], [
                    "О Земля, наполни же мой меч своей силой! Сотрясающее Землю Обезглавливание!",
                    "Гнома, придай мне своей силы! Сотрясающее Землю Обезглавливание!",
                    "Ощути всю тяжесть самой Земли! Сотрясающее Землю Обезглавливание!"
                ])
            else:
                self.world.battle.cry(self.name[0], [
                    "О Земля, наполни же мой меч своей силой! Сотрясающее Землю Обезглавливание!",
                    "Ощути всю тяжесть самой Земли! Сотрясающее Землю Обезглавливание!"
                ])

            self.mp -= 3

            if self.fire > 1 and self.skill[4]:
                self.world.battle.show_skillname("Сотрясающее Землю Обезглавливание: Магма")
            else:
                self.world.battle.show_skillname("Сотрясающее Землю Обезглавливание")

            renpy.sound.play("se/earth1.ogg")

            if self.earth and self.skill[2]:
                renpy.show("cutin_ruka_earth", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_earth", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_earth")
                renpy.pause(.1)
                renpy.hide("cutin_earth")
                renpy.pause(.1)

            if self.fire > 1:
                renpy.show("cutin_ruka_fire", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_fire", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_fire")
                renpy.pause(.1)
                renpy.hide("cutin_fire")
                renpy.pause(.1)

            self.skill12a()

            narrator("Сила Земли разливается и наполняет всё тело Луки когда он прыгает в воздух!")
            narrator(f"{self.name[0].name} обрушивается вниз, сотрясая саму Землю при приземлении!")

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            if self.aqua:
                if self.world.battle.enemy[0].wind and not self.wind:
                    self.world.battle.enemy[0].wind_guard()
                    self.world.battle.common_attack_end()

            renpy.sound.play("se/damage2.ogg")
            self.damage = 450 + self.level * 9 / 2

            if self.earth:
                self.damage = self.damage * 4/3

            if self.fire > 1 and self.earth:
                self.damage = self.damage * 180/100
            elif self.fire > 1 and not self.earth:
                self.damage = self.damage * 125/100

            if self.earth:
                save_defense = self.world.battle.enemy[0].defense
                self.guard_break()

            self.world.battle.enemy[0].life_calc()

            if self.wind == 5:
                narrator(f"Вслед за порывом ветра, остаточный образ {self.name[0].name} обрушивается на противника со всей силой!")

                if self.earth:
                    self.damage = self.damage * 4/3

                if self.fire > 1 and self.earth:
                    self.damage = self.damage * 180/100

                elif self.fire > 1 and not self.earth:
                    self.damage = self.damage * 125/100

                self.world.battle.enemy[0].life_calc()

            if self.earth:
                self.world.battle.enemy[0].defense = save_defense

            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill13a(self):
            renpy.sound.play("se/karaburi.ogg")
            renpy.show("effect serene demon sword", zorder=30)
            renpy.pause(0.3, hard=True)
            renpy.hide("effect")
            renpy.with_statement(ImageDissolve("images/Transition/mask01.webp", 0.5))

        def skill13(self):
            self.before_action = 27
            if self.skill[3]:
                self.world.battle.cry(self.name[0], [
                    "Вместе с потоком... Безмятежный Демонический Клинок!",
                    "Ундина, одолжи мне свою силу... Безмятежный Демонический Клинок!",
                    "Я рассеку всё на своём пути... Безмятежный Демонический Клинок!"
                ])
            else:
                self.world.battle.cry(self.name[0], [
                    "Вместе с потоком... Безмятежный Демонический Клинок!",
                    "Я рассеку всё на своём пути... Безмятежный Демонический Клинок!"
                ])

            self.aqua_end = True
            self.mp -= 2

            if self.earth and self.aqua and self.aqua != 4:
                self.world.battle.show_skillname("Безмятежный Демонический Клинок: Землетрясение")
            else:
                self.world.battle.show_skillname("Безмятежный Демонический Клинок")

            narrator(f"{self.name[0].name} вкладывает меч в ножны, и безмятежно держит рукой его рукоять!")

            renpy.sound.play("se/aqua2.ogg")

            if self.aqua and self.aqua != 4:
                renpy.show("cutin_ruka_aqua", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_aqua", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_aqua")
                renpy.pause(.1)
                renpy.hide("cutin_aqua")
                renpy.pause(.1)

            if self.earth and self.aqua and self.aqua != 4:
                renpy.show("cutin_ruka_earth", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_earth", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_earth")
                renpy.pause(.1)
                renpy.hide("cutin_earth")
                renpy.pause(.1)

            self.skill13a()

            narrator("Вынимая его в молниеносном, гладком, как течение воды, взмахе, он прорубается сквозь всё на своём пути!")

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            self.damage = 300 + self.level * 3

            if self.aqua == 2:
                self.damage = self.damage * 2

            elif self.aqua and self.aqua != 2:
                self.damage = self.damage * 3 / 2

            if self.fire > 1:
                self.damage = self.damage * 3 / 2

            if self.earth and self.aqua:
                self.damage = self.damage * 115 / 100

                "Лука прорывает защиту противника несокрушимой силой Гномы!"

                save_defense = self.world.battle.enemy[0].defense
                self.guard_break()

            renpy.sound.play("se/slash4.ogg")

            self.world.battle.enemy[0].life_calc()

            if self.earth and self.aqua:
                self.world.battle.enemy[0].defense = save_defense

            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill14(self):
            self.before_action = 46
            persistent.count_fire += 1

            l("Саламандра, одолжи мне свою силу!..")

            self.mp -= 2
            self.world.battle.show_skillname("Клинок Пламени")
            self.fire = 1
            self.fire_turn = 5
            renpy.sound.play("se/fire1.ogg")
            self.world.summon("salamander st02")

            narrator("Языки огня пробегают вверх и вниз по мечу Луки!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill14b(self):
            self.before_action = 46
            persistent.count_fire += 1

            l("Наполни мой меч пламенем самого ада!")

            self.mp = self.max_mp
            self.world.battle.show_skillname("Адский Испепелящий Клинок")
            self.wind = 0
            self.wind_turn = 0
            self.holy = 0
            self.holy_turn = 0
            self.earth = 0
            self.earth_turn = 0
            self.aqua = 0
            self.aqua_turn = 0
            self.fire = 2
            self.fire_turn = 5
            renpy.sound.play("se/fire2.ogg")
            self.world.summon("salamander st02")

            narrator("Языки огня пробегают вверх и вниз по мечу Луки, готовые воспламениться с его умениями меча!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill14c(self):
            self.before_action = 46
            persistent.count_fire += 1

            l("Саламандра, очисти всё в искупляющем пламени!")

            self.mp = self.max_mp
            self.world.battle.show_skillname("Очищающее Пламя")
            self.fire = 3
            self.fire_turn = 5
            renpy.sound.play("se/fire4.ogg")
            self.world.summon("salamander st02")

            "Языки огня пробегают вверх и вниз по мечу Луки готовые воспламениться с его умениями меча!"

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill15(self):
            self.before_action = 47
            self.giga = 1

            if self.aqua == 2:
                renpy.show(self.world.battle.background)
                renpy.with_statement(Dissolve(1.5))
                self.world.battle.musicplay(self.world.battle.music)

            self.wind = 0
            self.wind_turn = 0
            self.holy = 0
            self.holy_turn = 0
            self.earth = 0
            self.earth_turn = 0
            self.aqua = 0
            self.aqua_turn = 0
            self.fire = 0
            self.fire_turn = 0
            self.mp -= 1

            l("Приди, Сильфа")

            self.world.battle.show_skillname("Сильфа")
            renpy.sound.play("se/wind2.ogg")
            self.world.summon("sylph st12")

            narrator("Сила ветра вливается в меч Луки!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill15a(self):
            self.before_action = 47
            self.giga = 2

            l("Приди, Гнома!")

            self.world.battle.show_skillname("Гнома")
            renpy.sound.play("se/earth1.ogg")
            self.world.summon("gnome st21")

            narrator("Сила земли вливается в меч Луки!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill15b(self):
            self.before_action = 47
            self.giga = 3

            l("Приди, Ундина!")

            self.world.battle.show_skillname("Ундина")
            renpy.sound.play("se/aqua2.ogg")
            self.world.summon("undine st11")

            narrator("Сила воды вливается в меч Луки!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill15c(self):
            self.before_action = 47
            self.giga = 4

            l("Приди, Саламандра!")

            self.world.battle.show_skillname("Саламандра")
            renpy.sound.play("se/fire2.ogg")
            self.world.summon("sylph st02")

            narrator("Сила огня вливается в меч Луки!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill15e(self):
            renpy.sound.play("se/wind2.ogg")
            renpy.show("cutin_wind", zorder=30, at_list=[cutin(0, -200)])
            renpy.pause(.5)
            renpy.sound.play("se/earth1.ogg")
            renpy.show("cutin_earth", zorder=30, at_list=[cutin(200, -200)])
            renpy.pause(.5)
            renpy.sound.play("se/aqua2.ogg")
            renpy.show("cutin_aqua", zorder=30, at_list=[cutin(400, -200)])
            renpy.pause(.5)
            renpy.sound.play("se/fire2.ogg")
            renpy.show("cutin_fire", zorder=30, at_list=[cutin(600, -200)])
            renpy.pause(.5)
            renpy.hide("cutin_wind")
            renpy.hide("cutin_earth")
            renpy.hide("cutin_aqua")
            renpy.hide("cutin_fire")
            renpy.with_statement(Dissolve(1.0))

        def skill15d(self):
            self.before_action = 28
            giga = 0
            persistent.count_giga = 1

            l("Тебе пришёл конец! Четырёхкратный Гига-удар!")

            self.world.battle.show_skillname("Четырёхкратный Гига-удар")

            self.skill15e()

            narrator("Силы четырёх духов перекрывают и усиливают друг друга, взрываясь в силовом потоке!")

            renpy.pause(0.5)
            renpy.sound.play("se/karaburi.ogg")
            renpy.show("effect crush", zorder=30)
            renpy.pause(0.3)
            renpy.sound.play("se/earth1.ogg")
            renpy.show("effect earth", zorder=30)
            renpy.pause(0.1)
            renpy.sound.play("se/fire2.ogg")
            renpy.show("effect fire", zorder=30)
            renpy.pause(0.1)
            renpy.sound.play("se/aqua2.ogg")
            renpy.show("effect aqua", zorder=30)
            renpy.pause(0.1)
            renpy.sound.play("se/wind2.ogg")
            renpy.show("effect wind", zorder=30)
            renpy.pause(0.1)
            renpy.hide("effect")
            renpy.with_statement(ImageDissolve("images/Transition/mask01.webp", 0.1))
            renpy.sound.play("se/bom3.ogg")
            renpy.with_statement(Quake((0, 0, 0, 0), 2.0, dist=40))

            narrator("Всесокрушающая сила высвобождается из меча Луки!")

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            renpy.sound.play("se/damage2.ogg")
            self.damage = rand().randint(40000, 50000)
            self.world.battle.enemy[0].raw_life_calc(self.damage)
            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill16(self, say=True):
            self.before_action = 29
            if say:
                if self.skill[4]:
                    self.world.battle.cry(self.name[0], [
                        "Я очищу тебя в искупляющем пламени... Неудержимый Испепеляющий Клинок!",
                        "Сгори в этом адском пламени! Неудержимый Испепеляющий Клинок!",
                        "Одолжи мне свою силу, Саламандра! Неудержимый Испепеляющий Клинок!"
                    ])
                else:
                    self.world.battle.cry(self.name[0], [
                        "Я очищу тебя в искупляющем пламени... Неудержимый Испепеляющий Клинок!",
                        "Сгори в этом адском пламени! Неудержимый Испепеляющий Клинок!"
                    ])

            self.mp -= 4
            if (self.wind or self.holy > 2) and self.fire > 1:
                self.world.battle.show_skillname("Неудержимый Испепеляющий Клинок: Тайфун")

                renpy.sound.play("se/fire2.ogg")

                renpy.show("cutin_ruka_fire", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_fire", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_fire")
                renpy.pause(.1)
                renpy.hide("cutin_fire")
                renpy.pause(.1)

                renpy.sound.play("se/wind2.ogg")

                renpy.show("cutin_ruka_wind", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_wind", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_wind")
                renpy.pause(.1)
                renpy.hide("cutin_wind")
                renpy.pause(.1)

                self.skill16c()

            else:
                self.world.battle.show_skillname("Неудержимый Испепеляющий Клинок")

                renpy.sound.play("se/fire2.ogg")

                if self.fire != 0:
                    renpy.show("cutin_ruka_fire", zorder=30, at_list=[cutin(500, -200)])
                    renpy.pause(.7)
                    renpy.show("cutin_fire", zorder=30, at_list=[cutin(100, -200)])
                    renpy.pause(.7)
                    renpy.hide("cutin_ruka_fire")
                    renpy.pause(.1)
                    renpy.hide("cutin_fire")
                    renpy.pause(.1)

                self.skill16a()

            narrator(f"Воспламеняясь очищающим огнём, {self.name[0].name} атакует в вихре пламени!")

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            if self.world.battle.enemy[0].wind and self.wind < 2:
                self.world.battle.enemy[0].windguard()
                self.world.battle.common_attack_end()

            count = 0
            if self.wind > 2 and self.fire:
                for _ in range(8):
                    renpy.sound.play("se/slash.ogg")

                    self.damage = rand().randint(135+self.level*3//2, 190+self.level*2)

                    self.world.battle.enemy[0].life_calc(count+1)

                    count += 1
                    if count > 2:
                        count = 0

            else:
                for _ in range(5):
                    renpy.sound.play("se/slash.ogg")

                    if self.fire < 2:
                        self.damage = rand().randint(150+self.level*3//2, 200+self.level*2)
                    elif self.fire > 1:
                        self.damage = rand().randint(200+self.level*2, 300+self.level*3)

                    self.world.battle.enemy[0].life_calc(count+1)

                    count += 1
                    if count > 2:
                        count = 0

            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill16a(self):
            renpy.show("vaporizing rebellion sword", zorder=30)
            renpy.pause(3.0, hard=True)
            renpy.hide("vaporizing")

        def skill16c(self):
            renpy.show("effect wind", zorder=30)
            renpy.show("vaporizing rebellion sword typhoon", zorder=30)
            renpy.pause(3.0, hard=True)
            renpy.hide("vaporizing")
            renpy.hide("effect")

        def skill17(self):
            self.before_action = 55
            self.world.battle.cry(self.name[0], [
                "Загнать себя в угол!..",
                "Таков мой способ ведения боя!..",
                "Даже находясь на грани, я не дрогну!"
            ])

            self.world.battle.show_skillname("Крайность")

            extend_say(narrator, f"{self.name[0].name} спокойно сосредотачивается...")

            renpy.sound.play("se/down2.ogg")
            self.life = 1

            extend_say(extend, f"\nЖизнеспособность Луки резко падает!")

            self.world.battle.hide_skillname()
            self.world.battle.face(param=2)
            self.world.battle.main()

        def skill18(self):
            self.before_action = 45

            l("Успокоить своё сердце и разум...")

            self.world.battle.show_skillname("Безмятежный разум")

            if not self.aqua == 2:
                self.mp -= 2

            renpy.sound.play("se/aqua.ogg")

            self.aqua = 4

            if self.skill[3]:
                persistent.count_aqua += 1
                self.world.summon("undine st11")
                if self.aqua == 2:
                    renpy.show(self.world.battle.background)
                    renpy.with_statement(Dissolve(1.5))
                    self.world.battle.musicplay(self.world.battle.music)
                self.aqua_turn = 16
            else:
                self.aqua_turn = 12

                if self.holy:
                    self.holy = 0
                    self.holy_turn = 0

            narrator(f"{self.name[0].name} очищает свой разум и объединяется с мировым течением!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill19a(self):
            if self.wind and self.skill[1]:
                renpy.sound.play("se/wind2.ogg")
                renpy.show("cutin_ruka_wind", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_wind", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_wind")
                renpy.pause(.1)
                renpy.hide("cutin_wind")
                renpy.pause(.1)

            renpy.sound.play("se/tuki.ogg")
            renpy.show("effect flash kill", zorder=30)
            renpy.pause(0.1, hard=True) #2
            renpy.hide("effect")
            renpy.with_statement(ImageDissolve("images/Transition/mask01.webp", 0.5))

        def skill19(self):
            self.before_action = 30
            self.world.battle.cry(self.name[0], [
                "Пришёл за тобой судьбоносный клинок правосудия!",
                "Насквозь прорезая пространство, время и здравый смысл!"
            ])

            self.mp -= 2

            if (self.wind or self.holy > 2):
                self.world.battle.show_skillname("Мгновенное Убийство: Опустошающее Торнадо")
            else:
                self.world.battle.show_skillname("Мгновенное Убийство")

            self.skill19a()

            narrator("Клинок Луки прорывается сквозь само время и пространство!")

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            renpy.sound.play("se/slash4.ogg")

            if self.wind != 3 and not self.fire:
                self.damage = 250+self.level*5/2
            elif (self.wind == 3 or self.holy == 3) and not self.fire:
                self.damage = 400+self.level*7/2
            elif self.wind != 3 and self.fire:
                self.damage = 300+self.level*11/4
            elif (self.wind == 3 or self.holy == 3) and self.fire:
                self.damage = 450+self.level*9/2

            if (self.wind or self.holy > 2) and self.skill[1]:
                self.damage = self.damage * 11/10

            self.world.battle.enemy[0].life_calc()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            if (self.wind or self.holy > 2):
                narrator("Бушуя, подобно торнаду, меч Луки возвращается в ножны!")

                renpy.sound.play("se/slash4.ogg")

                if self.wind != 3 and not self.fire:
                    self.damage = 250+self.level*5/2
                elif (self.wind == 3 or self.holy == 3) and not self.fire:
                    self.damage = 400+self.level*7/2
                elif self.wind != 3 and self.fire:
                    self.damage = 300+self.level*11/4
                elif (self.wind == 3 or self.holy == 3) and self.fire:
                    self.damage = 450+self.level*9/2

                if (self.wind or self.holy > 2) and self.skill[1]:
                    self.damage = self.damage * 11/10

                self.world.battle.enemy[0].life_calc()

            self.world.battle.hide_skillname()
            self.world.battle.common_attack_end()

        def skill20a(self):
            _window_hide()
            renpy.hide_screen("hp")

            if not self.fire < 3:
                renpy.sound.play("se/fire2.ogg")
                renpy.show("cutin_ruka_fire", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_fire", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_fire")
                renpy.pause(.1)
                renpy.hide("cutin_fire")
                renpy.pause(.1)

            renpy.show("rakshasa", zorder=30)
            renpy.pause(1.5, hard=True)
            renpy.hide("rakshasa")
            renpy.show_screen("hp")
            _window_show(auto=True)

        def skill20(self):
            self.before_action = 31

            l("Челюсти Ракшасы да приведут грешников к расплате!")

            self.mp -= 6

            if self.fire == 3:
                self.world.battle.show_skillname("Девятиликий Ракшаса: Асура")
            elif self.fire < 3:
                self.world.battle.show_skillname("Девятиликий Ракшаса")

            self.skill20a()

            narrator(f"{self.name[0].name} атакует в смертельном танце!")

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            count = 0
            for _ in range(9):
                renpy.sound.play("se/slash.ogg")

                if self.fire == 0:
                    self.damage = rand().randint(100+self.level, 130+self.level*13//10)
                elif self.fire == 2:
                    self.damage = rand().randint(120+self.level*6//5, 150+self.level*3//2)
                elif self.fire == 3:
                    self.damage = rand().randint(140+self.level*7//5, 170+self.level*17//10)

                self.world.battle.enemy[0].life_calc(count+1)

                count += 1
                if count > 2:
                    count = 0

            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill21a(self):
            _window_hide()
            renpy.hide_screen("hp")

            if not self.earth < 3:
                renpy.sound.play("se/earth1.ogg")
                renpy.show("cutin_ruka_earth", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_earth", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_earth")
                renpy.pause(.1)
                renpy.hide("cutin_earth")
                renpy.pause(.1)

            renpy.pause(0.3, hard=True)
            renpy.sound.play("se/bom2.ogg")
            renpy.show("effect heavenly demon revival", zorder=30)
            renpy.pause(0.3, hard=True)
            renpy.hide("effect ")
            renpy.with_statement(ImageDissolve("images/Transition/mask02.webp", 0.5, ramplen=256, hard=True))
            renpy.show_screen("hp")
            _window_show(auto=True)

        def skill21(self):
            self.before_action = 32

            l("Вся жизнь возвращается к своей первоначальной форме!")

            self.mp -= 4

            if self.earth == 3:
                self.world.battle.show_skillname("Возрождение Небесного Демона: Гайя")
            elif self.earth < 3:
                self.world.battle.show_skillname("Возрождение Небесного Демона")

            self.skill21a()

            narrator(f"{self.name[0].name} концентрирует вокруг себя сгусток мощной магической энергии!")

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            renpy.sound.play("se/damage2.ogg")

            if self.earth < 3 and not self.fire:
                self.damage = 600 + self.level * 6
            elif self.earth == 3 and not self.fire:
                self.damage = 800 + self.level * 8
            elif self.earth < 3 and self.fire:
                self.damage = 600 + self.level * 8
            elif self.earth == 3 and self.fire:
                self.damage = 1000 + self.level * 10

            self.world.battle.enemy[0].life_calc()
            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill22(self):
            self.before_action = 59

            l("...........")

            self.mp -= 4
            self.world.battle.show_skillname("Концентрация магии")
            renpy.sound.play("se/power.ogg")

            narrator(f"{self.name[0].name} закрывает глаза, и атмосфера вокруг него наполняется магической энергией ошеломляющей мощи!")

            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.daystar = 1
            self.world.battle.common_attack_end()

        def skill22a(self):
            renpy.pause(0.3)
            _window_hide()
            renpy.hide_screen("hp")
            renpy.sound.play("se/bom3.ogg")
            renpy.show("effect daystar", zorder=30)
            renpy.with_statement(ImageDissolve("images/Transition/mask01.webp", 1.5, hard=True))
            renpy.show("effect daystar", zorder=30, at_list=[daystar])
            renpy.pause(1.6, hard=True)
            renpy.show("color white")
            renpy.hide("effect")
            renpy.with_statement(Dissolve(1.5))
            renpy.hide("color")
            renpy.with_statement(Dissolve(2.0))

        def skill22x(self):
            if len(self.world.battle.enemy) > 1:
                self.target_all = 1
            self.world.battle.guard_break_on = False
            self.world.battle.used_daystar += 1
            self.daystar = 0
            self.world.battle.hide_ct()
            self.world.battle.face(param=3)
            self.world.battle.counter()

            if self.aqua != 3:
                self.world.battle.show_skillname("Денница")
            elif self.aqua > 1 and self.skill[3]:
                self.world.battle.show_skillname("Непогрешимая Денница")

            l("Моя мать - Утренняя Звезда, дитя зари.{w}\nЗвезда, падшая на Землю, да обретёт победу!")

            if self.mp > 3:
                damage_reduce = 1
                self.mp -= 4
            elif self.mp < 4:
                damage_reduce = self.mp / 4
                self.mp = 0

            if self.aqua == 3:
                _window_hide()
                renpy.hide_screen("hp")
                renpy.sound.play("se/aqua2.ogg")
                renpy.show("cutin_ruka_aqua", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_aqua", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_aqua")
                renpy.pause(.1)
                renpy.hide("cutin_aqua")
                renpy.pause(.1)

                _window_show(auto=True)
                renpy.show_screen("hp")

            narrator("Ослепительная звезда обрушивается в ад!")

            self.skill22a()

            if self.world.battle.nostar:
                if self.world.battle.nostar == 1:
                    _window_show(auto=True)
                    renpy.show_screen("hp")
                    renpy.jump(f"{self.world.battle.tag}_nostar")

                self.world.battle.nostar -= 1

            for line in range(3):
                renpy.sound.play("se/damage2.ogg")

                if not self.aqua and not self.fire:
                    self.damage = 450 + self.level * 9 / 2
                elif self.aqua and not self.fire:
                    self.damage = 600 + self.level * 6
                elif not self.aqua and self.fire:
                    self.damage = 600 + self.level * 6
                elif self.aqua and self.fire:
                    self.damage = 700 + self.level * 7
                elif self.aqua and self.skill[3] and not self.fire:
                    self.damage = 750 + self.level * 6
                elif self.aqua and self.skill[3] and self.fire:
                    self.damage = 850 + self.level * 8

                self.damage = int(self.damage * damage_reduce)

                if not line:
                    _window_show(auto=True)
                    renpy.show_screen("hp")

                self.world.battle.enemy[0].life_calc(line+1)

            self.world.battle.hide_skillname()

            self.world.battle.face(1)
            self.target_all = 0
            self.world.battle.result = 44
            self.world.battle.enemy[0].countered = True

            self.world.battle.main()

        def skill23(self):
            self.before_action = 56

            l("Непогрешимый и неизменный... Танец Падшего Ангела!")

            self.world.battle.show_skillname("Танец Падшего Ангела")
            self.mp -= 2
            self.holy = 1
            self.holy_turn = 12
            self.wind = 0
            self.wind_turn = 0
            if self.skill[3] < 2:
                self.aqua = 0
                self.aqua_turn = 0
            renpy.sound.play("se/bun.ogg")

            lightwave((self.world.battle.background, self.world.battle.enemy[0].sprite[0]))
            self.world.battle.face(1)

            narrator(f"{self.name[0].name} объединяется со святым течением!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill7d(self):
            self.before_action = 43
            persistent.count_wind += 1

            l("Непогрешимый и неизменный... Танец Падшего Ангела!")

            self.mp -= 2
            self.world.battle.show_skillname("Танец Падшего Ангела")
            self.holy = 2
            self.holy_turn = 12
            self.wind = 0
            self.wind_turn = 0
            renpy.sound.play("se/wind2.ogg")
            random = rand().randint(1, 100)

            if random != 100:
                self.world.summon("sylph st12")
            elif random == 100:
                self.world.summon("sylph st07")
                persistent.g_sylph = True

            lightwave((self.world.battle.background, self.world.battle.enemy[0].sprite[0]))
            self.world.battle.face(1)

            narrator(f"{self.name[0].name} объединяется со святым течением!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill7e(self):
            self.before_action = 43
            persistent.count_wind += 1

            l("Непогрешимый и неизменный... Танец Падшего Ангела!")

            self.mp -= 1
            self.world.battle.show_skillname("Танец Падшего Ангела: Буря")
            self.holy = 3
            self.holy_turn = 16
            self.wind = 0
            self.wind_turn = 0
            renpy.sound.play("se/wind3.ogg")
            random = rand().randint(1, 100)

            if random != 100:
                self.world.summon("sylph st12")
            elif random == 100:
                self.world.summon("sylph st07")
                persistent.g_sylph = True

            lightwave((self.world.battle.background, self.world.battle.enemy[0].sprite[0]))
            self.world.battle.face(1)

            narrator(f"{self.name[0].name} объединяется со святым течением и ветром!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill24a(self):
            _window_hide()
            renpy.hide_screen("hp")

            renpy.sound.play("se/wind2.ogg")
            renpy.show("cutin_wind", zorder=30, at_list=[cutin(0, -200)])
            renpy.pause(.5)
            renpy.sound.play("se/earth1.ogg")
            renpy.show("cutin_earth", zorder=30, at_list=[cutin(200, -200)])
            renpy.pause(.5)
            renpy.sound.play("se/aqua2.ogg")
            renpy.show("cutin_aqua", zorder=30, at_list=[cutin(400, -200)])
            renpy.pause(.5)
            renpy.sound.play("se/fire2.ogg")
            renpy.show("cutin_fire", zorder=30, at_list=[cutin(600, -200)])
            renpy.pause(.5)
            renpy.hide("cutin_wind")
            renpy.hide("cutin_earth")
            renpy.hide("cutin_aqua")
            renpy.hide("cutin_fire")
            renpy.with_statement(Dissolve(1.0))

            renpy.show_screen("hp")
            _window_auto = True

        def skill24(self):
            self.before_action = 34

            l("Вся сила четырёх духов в моей руке!")

            self.mp -= 10

            self.world.battle.show_skillname("Элементальная Спика")
            self.skill24a()

            narrator("Сила четырёх духов вливается в правую руку Луки, после чего он взмахивает своим мечом с разрушительной силой!")

            renpy.pause(0.5)
            renpy.sound.play("se/karaburi.ogg")
            _window_hide()
            renpy.hide_screen("hp")
            renpy.show("spica", zorder=30)
            renpy.pause(1.2)
            renpy.hide("spica")

            renpy.show_screen("hp")
            _window_show(auto=True)

            if self.world.battle.enemy[0].skill_sub == 1:
                return

            if self.world.battle.enemy[0].earth or self.world.battle.enemy[0].wind or self.world.battle.enemy[0].aqua or self.world.battle.enemy[0].fire:
                narrator(f"Но {self.world.battle.enemy[0].name} имеет устойчивость к одному из элементов!")

            renpy.sound.play("se/damage2.ogg")
            self.damage = 3000 + self.level * 20
            if self.world.battle.enemy[0].wind:
                self.damage = self.damage // 2
            if self.world.battle.enemy[0].earth:
                self.damage = self.damage // 2
            if self.world.battle.enemy[0].aqua:
                self.damage = self.damage // 2
            if self.world.battle.enemy[0].fire:
                self.damage = self.damage // 2

            self.world.battle.enemy[0].life_calc()
            self.world.battle.hide_skillname()

            if self.world.battle.enemy[0].skill_sub > 1:
                return

            self.world.battle.common_attack_end()

        def skill25a(self):
            _window_hide()
            renpy.hide_screen("hp")

            renpy.sound.play("se/wind2.ogg")
            renpy.show("cutin_wind", zorder=30, at_list=[cutin(0, -200)])
            renpy.pause(.5)
            renpy.sound.play("se/earth1.ogg")
            renpy.show("cutin_earth", zorder=30, at_list=[cutin(200, -200)])
            renpy.pause(.5)
            renpy.sound.play("se/aqua2.ogg")
            renpy.show("cutin_aqua", zorder=30, at_list=[cutin(400, -200)])
            renpy.pause(.5)
            renpy.sound.play("se/fire2.ogg")
            renpy.show("cutin_fire", zorder=30, at_list=[cutin(600, -200)])
            renpy.pause(.5)
            renpy.hide("cutin_wind")
            renpy.hide("cutin_earth")
            renpy.hide("cutin_aqua")
            renpy.hide("cutin_fire")
            renpy.with_statement(Dissolve(1.0))

            renpy.show_screen("hp")
            _window_show(auto=True)

        def skill25(self):
            choice = 1

            if self.skillset == 2:
                renpy.pause(0.5)

                sylph("Лука!{w}\nТебе нужен мой ветер или ты хочешь его скомбинировать со своей святой силой?")

                _window_hide(auto=True)

                choice = renpy.display_menu([
                    ("Опустошающий Ураган", 1),
                    ("Танец Падшего Ангела", 2)
                ])


            self.before_action = 57
            persistent.count_wind += 1
            persistent.count_earth += 1
            persistent.count_aqua += 1
            persistent.count_fire += 1

            l("Вперёд, все вместе!")

            self.mp -= 6
            self.world.battle.show_skillname("Вызов четырёх духов")
            renpy.sound.play("se/wind2.ogg")

            self.skill25a()

            if choice == 1:
                self.wind = 3
                self.wind_turn = 16
                self.holy = 0
                self.holy_turn = 0
            elif choice == 2:
                self.holy = 3
                self.holy_turn = 16
                self.wind = 0
                self.wind_turn = 0
            self.earth = 3
            self.earth_turn = 16
            self.aqua = 3
            self.aqua_turn = 16
            self.fire = 3
            self.fire_turn = 5

            narrator("Лука призывает всех духов разом!")

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def skill26(self):
            self.before_action = 91

            l(".............")

            self.world.battle.show_skillname("Разгон")
            renpy.sound.play("audio/se/power.ogg")

            extend_say(narrator, f"{self.name[0].name} заряжает свою магию!")

            if self.world.battle.enemy[0].wind > 1:
                if self.world.battle.enemy[0].holy != 1 and self.world.battle.enemy[0].holy != 2 and self.guard < 1:
                    renpy.jump(f"{self.world.battle.tag}_chargecancel")
                elif self.world.battle.enemy[0].holy != 1 and self.skill[0] < 27:
                    renpy.jump(f"{self.world.battle.tag}_chargecancel")

            if world.battle.nocharge:
                renpy.jump(f"{self.world.battle.tag}_chargecancel")

            amount = 2 + self.mp_charge
            self.mp += amount
            self.mp_charge += 1

            if self.mp > self.max_mp:
                self.mp = self.max_mp

            extend_say(extend, f"\n{self.name[0].name} восстанавливает {amount} SP!")

            if self.guard_on == 1 and self.skill[0] > 26 and self.fire < 1:
                self.guard = 2

            self.world.battle.hide_skillname()
            self.world.battle.enemy[0].attack()

        def unskill1(self):
            self.before_action = 101

            if self.undine_skill[1] < 2:
                renpy.pause(0.5)

                narrator("Очистить своё тело?{w}\nИсцелить себя?..")
                narrator("И как же я должен это призвать?")
                narrator("Полностью уходя в себя, я представляю Ундину, исцеляющую меня.")

            l("Ундина, выведи токсины из моего тела!")

            self.world.battle.show_skillname("Очищение")
            renpy.pause(0.5)
            renpy.sound.play("se/aqua.ogg")
            self.mp -= 2
            self.world.summon("undine st11")

            "Лука фокусирует магию Ундины!"

            renpy.pause(0.5)

            if self.poison:
                renpy.sound.play("se/item.ogg")

                self.poison = 0
                if self.status == 9:
                    self.status = 0

                self.status_print()

                narrator(f"{self.name[0].name} исцеляется от яда!")

            renpy.sound.play("se/kaihuku.ogg")
            self.damage = self.max_life // 5

            narrator(f"{self.name[0].name} восстанавливает немного здоровья!")

            if self.life + self.damage > self.max_life:
                self.life = self.max_life
            else:
                self.life += self.damage

            self.world.battle.hide_skillname()

            if self.undine_skill[1] < 2:
                self.undine_skill[1] = 2

                l("Сработало!")
                narrator("Спасибо, Ундина...")

            self.world.battle.enemy[0].attack()

        def help(self):
            if self == world.actor[2]:
                renpy.show("granberia st02", at_list=[xy(0.7)], zorder=10)
                renpy.with_statement(dissolve)

                self.name[0]("О?{w}\nТеперь ты просишь меня о помощи, малец?")
                l("Да! Пожалуйста, помоги!")

                renpy.show("granberia st04")
                renpy.with_statement(dissolve)

                extend_say(self.name[0], "... Хмпф.")

                renpy.sound.play("se/karaburi.ogg")
                renpy.show("granberia st41")
                renpy.with_statement(dissolve)

                extend_say(extend, "\nХорошо, погнали!")

            elif self == world.actor[3]:
                renpy.sound.play("se/tamamo2.ogg")
                renpy.show("tamamo st06", at_list=[xy(0.7)], zorder=10)
                renpy.with_statement(dissolve)

                self.name[0]("Хм-м?{w}\nНеужто понадобилась помощь?")
                l("Т-ты не против?")

                renpy.show("tamamo st01")
                renpy.with_statement(dissolve)

                self.name[0]("Ну, раз владыка монстров приказала помочь, значит помогу...")

                renpy.show("tamamo st04")
                renpy.with_statement(dissolve)

                self.name[0]("Ладненько~{image=note}!{w}\nНебесный рыцарь Тамамо к вашим услугам~{image=note}!")

            elif self == world.actor[4]:
                renpy.show("alma_elma st01", at_list=[xy(0.7)], zorder=10)
                renpy.with_statement(dissolve)

                self.name[0]("Хм-хм-хм-хм...{w}\nПодсобить?")
                l("Поможешь мне?{w}\nП-пожалуйста?")

                renpy.show("alma_elma st03")
                renpy.with_statement(dissolve)

                self.name[0]("Что ж, раз ты сказал волшебное слово...")

            elif self == world.actor[5]:
                renpy.show("erubetie st01", at_list=[xy(0.7)], zorder=10)
                renpy.with_statement(dissolve)

                self.name[0]("Я?..{w}\nТы просишь моей помощи?")
                l("Ты ведь... не против, да?")
                self.name[0](".............")
                narrator("Эрубети вздыхает.")

                self.name[0]("Хорошо.{w}\nНа этот раз я тебе помогу.")

            self.used_support = True

        def grskill1(self):
            self.world.party.support_turn = 1

            renpy.show("granberia st41", at_list=[xy(0.7)], zorder=10)
            self.world.battle.face(3)

            self.world.battle.cry(self.name[0], [
                "А вот и я!",
                "Сможешь ли ты уклониться?!",
                "Посмотрим насколько ты сильна!"
            ])

            renpy.hide("granberia")
            renpy.with_statement(dissolve)

            self.mp -= 2
            self.world.battle.show_skillname("Проклятый клинок: Обезглавливание")

            narrator(f"{self.name[0]} быстро ступает вперёд, нацеливая свой клинок на шею противника!")

            renpy.show("demon decapitation", at_list=[xy(self.world.battle.enemy[0].tukix, self.world.battle.enemy[0].tukiy)], zorder=30)
            renpy.pause(0.26)
            renpy.hide("demon decapitation")

            self.damage = 799 + rand().randint(1, 301) // 2
            renpy.sound.play("se/damage.ogg")
            self.world.battle.enemy[0].life_calc()

            self.world.battle.hide_skillname()

            self.world.battle.result = -1

        def grskill2(self):
            self.world.party.support_turn = 1

            renpy.show("granberia st41", at_list=[xy(0.7)], zorder=10)
            self.world.battle.face(3)

            self.name[0]("Тебе не уйти!")

            renpy.hide("granberia")
            renpy.with_statement(dissolve)

            self.mp -= 2
            self.world.battle.show_skillname("Кровопускающий Громовой Выпад: Торнадо")

            narrator(f"{self.name[0]} ступает вперёд, подобно грому, и делает резкий выпад!")

            renpy.sound.play("se/tuki.ogg")
            renpy.show("thunder thrust b", at_list=[xy(self.world.battle.enemy[0].tukix, self.world.battle.enemy[0].tukiy)], zorder=30)
            renpy.pause(0.26)
            renpy.hide("thunder thrust b")

            if self.world.battle.turn == 1:
                self.damage = 699 + rand().randint(1, 201) // 2
            else:
                self.damage = 999 + rand().randint(1, 201) // 2

            renpy.sound.play("se/damage.ogg")
            self.world.battle.enemy[0].life_calc()

            self.world.battle.hide_skillname()

            self.world.battle.result = -1

        def grskill3(self):
            self.world.party.support_turn = 1

            renpy.show("granberia st41", at_list=[xy(0.7)], zorder=10)
            self.world.battle.face(3)

            self.world.battle.cry(self.name[0], [
                "С силой, способной прорубить горы!..",
                "Сожри-ка это!",
                "Получай!"
            ])

            if self.world.battle.enemy[0].earth:
                self.name[0]("Твоя защита бесполезна!")

            renpy.hide("granberia")
            renpy.with_statement(dissolve)

            self.mp -= 3
            self.world.battle.show_skillname("Удар дракона-мясника")

            narrator(f"{self.name[0]} обрушивает свой клинок, круша всё на своём пути!")

            _window_hide()
            renpy.hide_screen("hp")
            renpy.sound.play("se/karaburi.ogg")
            renpy.show("effect crush", zorder=30)
            renpy.pause(0.3)
            renpy.hide("effect")
            renpy.with_statement(ImageDissolve("images/Transition/mask01.webp", 1.0))
            renpy.show_screen("hp")
            _window_show(auto=True)
            renpy.with_statement(Quake((0, 0, 0, 0), 2.0, dist=30))

            self.damage = 899 + rand().randint(1, 201) // 2

            save_defense = self.world.battle.enemy[0].defense
            defense_break = (100 - self.world.battle.enemy[0].defense) // 2
            self.world.battle.enemy[0].defense = self.world.battle.enemy[0].defense+defense_break

            if self.world.battle.enemy[0].defense > 100:
                self.world.battle.enemy[0].defense = 100

            renpy.sound.play("se/damage2.ogg")
            self.world.battle.enemy[0].life_calc()

            self.world.battle.enemy[0].defense = save_defense
            self.world.battle.hide_skillname()

            self.world.battle.result = -1

        def grskill4(self):
            self.world.party.support_turn = 1

            renpy.show("granberia st41", at_list=[xy(0.7)], zorder=10)
            self.world.battle.face(3)

            self.name[0]("Я тебя раздавлю!")

            renpy.hide("granberia")
            renpy.with_statement(dissolve)

            self.mp -= 3
            self.world.battle.show_skillname("Демонический Крушитель Черепов: Очищение")

            narrator(f"{self.name[0]} подпрыгивает в воздух и обрушивается вниз!")

            self.skill5a()

            self.damage = 1199 + rand().randint(1, 301) // 2

            renpy.sound.play("se/damage.ogg")
            self.world.battle.enemy[0].life_calc()

            self.world.battle.hide_skillname()

            self.world.battle.result = -1

        def grskill5(self):
            self.world.party.support_turn = 1

            renpy.show("granberia st41", at_list=[xy(0.7)], zorder=10)
            self.world.battle.face(3)

            self.name[0]("Словно падающая звезда!..")

            renpy.hide("granberia")
            renpy.with_statement(dissolve)

            self.mp -= 3
            self.world.battle.show_skillname("Гибельный Клинок Звезды Хаоса")

            narrator(f"Меч {self.name[1]} сверкает и блестит словно хаотично падающая звезда!")

            self.skill6a()

            save_defense = self.world.battle.enemy[0].defense
            defense_break = (100 - self.world.battle.enemy[0].defense) // 2
            self.world.battle.enemy[0].defense = self.world.battle.enemy[0].defense+defense_break

            if self.world.battle.enemy[0].defense > 100:
                self.world.battle.enemy[0].defense = 100

            count = 0
            for _ in range(5):
                renpy.sound.play("se/slash.ogg")

                self.damage = 120 + rand().randint(80, 120)

                self.world.battle.enemy[0].life_calc(count+1)

                count += 1
                if count > 2:
                    count = 0

            self.world.battle.enemy[0].defense = save_defense
            self.world.battle.hide_skillname()

            self.world.battle.result = -1

        def grskill6(self):
            self.world.party.support_turn = 1

            renpy.show("granberia st41", at_list=[xy(0.7)], zorder=10)
            self.world.battle.face(3)

            self.name[0]("Да пусть всё сгорит на моём пути! Неудержимый Испепеляющий Клинок")

            self.mp -= 4
            self.world.battle.show_skillname("Неудержимый Испепеляющий Клинок")

            renpy.show("granberia st42")
            renpy.with_statement(dissolve)

            renpy.sound.play("se/fire2.ogg")
            renpy.pause(0.5)
            renpy.hide("granberia")
            renpy.with_statement(dissolve)
            self.skill16a()

            count = 0
            for _ in range(5):
                renpy.sound.play("se/slash.ogg")

                self.damage = 160 + rand().randint(150, 190)

                self.world.battle.enemy[0].life_calc(count+1)

                count += 1
                if count > 2:
                    count = 0

            self.world.battle.hide_skillname()

            self.world.battle.result = -1

        def grskill7(self):
            self.world.party.support_turn = 1

            renpy.show("granberia st41", at_list=[xy(0.7)], zorder=10)
            self.world.battle.face(3)

            self.name[0]("Лука!{w}\nИспользуй эту звёздную атаку!")
            l("Э?!")
            self.name[0]("Доверься мне...")
            l("Х-хорошо!")

            self = self.world.actor[0]
            self.world.party.player = self

            self.mp -= 8
            self.world.actor[2].mp -= 10

            if self.aqua:
                self.world.battle.show_skillname("Звёздный клинок: Цунами")
            else:
                self.world.battle.show_skillname("Звёздный клинок")

            if self.aqua == 3:
                _window_hide()
                renpy.hide_screen("hp")
                renpy.sound.play("se/aqua2.ogg")
                renpy.show("cutin_ruka_aqua", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_aqua", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_aqua")
                renpy.pause(.1)
                renpy.hide("cutin_aqua")
                renpy.pause(.1)

                _window_show(auto=True)
                renpy.show_screen("hp")

            narrator("Ослепительная звезда обрушивается в ад!")

            self.skill22a()
            renpy.show_screen("hp")

            self = self.world.actor[2]
            self.world.party.player = self

            self.name[0]("Поехали!")
            narrator("Гранберия подпрыгивает ввверх к звезде!")

            renpy.hide_screen("hp")
            renpy.sound.play("audio/se/karaburi.ogg")
            renpy.hide("granberia")
            renpy.pause(0.3)
            renpy.sound.play("audio/se/karaburi.ogg")
            renpy.show("effect serene demon sword", zorder=30)
            renpy.pause(0.3)
            renpy.show("effect daystar")
            renpy.with_statement(ImageDissolve("images/Transition/mask01.webp", 0.5))
            renpy.pause(0.3)
            renpy.sound.play("audio/se/karaburi.ogg")
            renpy.hide("effect")
            renpy.show_screen("hp")

            narrator("Она ударяет своим пылающим лезвием прямо в звезду!")

            renpy.sound.play("audio/se/bom2.ogg")
            renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=20), "master")
            renpy.transition(Quake((0, 0, 0 ,0), 1.0, dist=20), "screens")
            renpy.sound.play("audio/se/fire2.ogg")
            renpy.pause(0.3)
            renpy.sound.play("audio/se/power.ogg")

            narrator("Свет звезды резонирует с огнём на лезвиях!")
            narrator(f"Сила света и огня сливаются воедино в мече {self.name[1]}!")

            renpy.sound.play("audio/se/power.ogg")
            renpy.pause(1.0)
            renpy.sound.play("audio/se/karaburi.ogg")

            narrator("Наконец, она обрушивает клинок из чистого света на противника!")

            renpy.sound.play("audio/se/bom2.ogg")
            renpy.pause(0.4)
            renpy.sound.play("audio/se/bom3.ogg")
            renpy.with_statement(flash2)
            renpy.show("bg white", tag="flash", zorder=30)
            renpy.with_statement(dissolve)
            renpy.pause(0.3)
            renpy.transition(Quake((0, 0, 0 ,0), 2.0, dist=30), "master")
            renpy.transition(Quake((0, 0, 0 ,0), 2.0, dist=30), "screens")

            if not self.world.actor[0].aqua and not self.world.actor[0].fire:
                self.damage = 2800 + world.actor[0].level*7
            elif not self.world.actor[0].aqua and self.world.actor[0].fire:
                self.damage = 3000 + world.actor[0].level*8
            elif self.world.actor[0].aqua and not self.world.actor[0].fire:
                self.damage = 3200 + world.actor[0].level*9
            elif self.world.actor[0].aqua and self.world.actor[0].fire:
                self.damage = 3600 + world.actor[0].level*10

            save_defense = self.world.battle.enemy[0].defense
            defense_break = (100 - self.world.battle.enemy[0].defense) // 2
            self.world.battle.enemy[0].defense = self.world.battle.enemy[0].defense+defense_break

            if self.world.battle.enemy[0].defense > 100:
                self.world.battle.enemy[0].defense = 100

            self.world.battle.enemy[0].life_calc()

            renpy.hide("flash")
            renpy.with_statement(dissolve)

            self.world.battle.hide_skillname()

            self.world.battle.result = -1

        def alskill1(self):
            self.world.party.support_turn = 1

            renpy.show("alma_elma st09", at_list=[xy(0.7)], zorder=10)
            self.world.battle.face(3)

            self.name[0]("Ветра, бушуйте как вам заблагорассудится!")

            renpy.hide("alma_elma")

            self.mp -= 3

            self.world.battle.show_skillname("Шамшир")
            renpy.sound.play("se/wind3.ogg")

            narrator("Альма Эльма призывает шквал ветряных лезвий!")

            renpy.pause(0.2)
            renpy.sound.play("se/slash.ogg")
            renpy.show("shamshir", at_list=[xy(self.world.battle.enemy[0].tukix, self.world.battle.enemy[0].tukiy)], zorder=30)
            renpy.pause(0.6)
            renpy.hide("shamshir")

            self.damage = 550 + rand().randint(70, 90) * 5 // 2
            renpy.sound.play("se/slash3.ogg")
            self.world.battle.enemy[0].life_calc()

            self.world.battle.hide_skillname()

            return

        def alskill2(self):
            self.world.party.support_turn = 1

            renpy.show("alma_elma st03", at_list=[xy(0.7)], zorder=10)
            self.world.battle.face(3)

            self.name[0]("Ох...{w}{nw}")

            renpy.show("alma_elma st22")

            self.name[0]("Неужто малыш Лука хочет поцелуйчик~{image=note}?")

            renpy.show("alma_elma st21")

            self.name[0]("Просто закрой глазки, милый. Тебе станет намно-о-о-ого лучше~{image=note}.")

            self.mp -= 6
            self.world.battle.show_skillname("Ангельский поцелуй")
            renpy.pause(0.5)
            renpy.sound.play("se/ero_chupa4.ogg")

            l("М-м-м-м-хм...")

            renpy.sound.play("se/ero_chupa5.ogg")

            "Альма Эльма впускает свой язык в рот Луки, углубляя поцелуй!"

            renpy.pause(0.5)
            renpy.sound.play("se/ero_chupa5.ogg")
            renpy.pause(1.0)
            renpy.sound.play("se/ero_chupa5.ogg")
            renpy.pause(1.0)
            renpy.sound.play("se/ero_chupa2.ogg")

            "Он вынужден проглотить её слюну..."

            renpy.pause(0.5)

            renpy.sound.play("se/kaihuku.ogg")
            self.damage = self.max_life // 5

            narrator(f"{self.name[0].name} полностью восстанавливает здоровье Луки!")

            self = self.world.actor[0]
            self.world.party.player = self

            if self.life + self.world.actor[4].damage > self.max_life:
                self.life = self.max_life
            else:
                self.life += self.damage

            self.world.battle.hide_skillname()

        def alskill3(self):
            self.world.party.support_turn = 1

            renpy.show("alma_elma st03", at_list=[xy(0.7)], zorder=10)
            self.world.battle.face(3)

            self.name[0]("Уфу-фу-фу-фу~{image=note}...{w}\nПо-о-о-осмотрим~{image=note}")

            self.mp -= 8
            self.world.battle.show_skillname("Соблазнение")

            renpy.call(f"{world.battle.tag}_alskill3")

            self.world.battle.hide_skillname()

            self.world.battle.result = -1

        def alskill4a(self):
            _window_hide()
            renpy.hide_screen("hp")

            if not self.fire < 3:
                renpy.sound.play("se/fire2.ogg")
                renpy.show("cutin_ruka_fire", zorder=30, at_list=[cutin(500, -200)])
                renpy.pause(.7)
                renpy.show("cutin_fire", zorder=30, at_list=[cutin(100, -200)])
                renpy.pause(.7)
                renpy.hide("cutin_ruka_fire")
                renpy.pause(.1)
                renpy.hide("cutin_fire")
                renpy.pause(.1)

            renpy.show("rakshasa wildfire", zorder=30)
            renpy.pause(1.5, hard=True)
            renpy.hide("rakshasa")
            renpy.show_screen("hp")
            _window_show(auto=True)

        def alskill4(self):
            self.world.party.support_turn = 1

            renpy.show("alma_elma st01", at_list=[xy(0.7)], zorder=10)
            self.world.battle.face(3)

            self.name[0]("Эй, малыш Лука!{w}\nИспользуй эту свою огненную атаку!")
            l("А?")
            self.name[0]("Ну знаешь, это техника, где ты атакуешь противника кучу раз за атаку!{w}\nУ меня есть одна идейка~{image=note}...")
            l("Эм, ладно!{w}\nПоехали!")

            self = self.world.actor[0]
            self.world.party.player = self

            self.mp -= 6
            self.world.actor[4].mp -= 5

            if self.fire:
                self.world.battle.show_skillname("Девятиликий Ракшаса: Пожар Асуры")
            else:
                self.world.battle.show_skillname("Девятиликий Ракшаса: Пожар")

            renpy.sound.play("se/wind1.ogg")
            renpy.show("effect wind", zorder=30)
            renpy.with_statement(dissolve)

            narrator("Альма Эльма использует свою магию, чтобы наделить клинок Луки силой могучего ветра!")

            renpy.sound.play("se/fire2.ogg")
            renpy.show("effect fire", tag="effect2", zorder=20)
            renpy.with_statement(dissolve)

            narrator("С потоками ветра вокруг клинка, меч Луки начинает расколяться от обвившего его пламени!")

            renpy.show("alma_elma st09")

            alma("Давай же!")
            self.name[0]("Хорошо! Выкуси!")
            print(world.party.player.name)

            renpy.pause(0.5)
            renpy.sound.play("se/power.ogg")
            renpy.pause(1.5)
            renpy.hide("effect2")
            renpy.hide("alma_elma")
            renpy.with_statement(dissolve)
            self.alskill4a()
            renpy.hide("wind")
            renpy.with_statement(dissolve)

            count = 0
            for _ in range(9):
                renpy.sound.play("se/slash.ogg")

                if self.fire == 0:
                    self.damage = rand().randint(150+self.level*11//10, 180+self.level*14//10)
                elif self.fire == 2:
                    self.damage = rand().randint(190+self.level*7//5, 220+self.level*8//5)
                elif self.fire == 3:
                    self.damage = rand().randint(240+self.level*9//5, 270+self.level*20//10)

                self.world.battle.enemy[0].life_calc(count+1)

                count += 1
                if count > 2:
                    count = 0

            self.world.battle.hide_skillname()

            self.world.battle.result = -1

        def blaze(self):
            renpy.show("blaze")
            renpy.sound.play("se/bom1.ogg")
            renpy.pause(4.4, hard=True)
            renpy.hide("blaze")

        def status_sel(self, kind):
            if kind == 1:
                if self.status == 1:
                    narrator(f"{self.name[0].name} в трансе...")
                elif self.status == 2:
                    narrator(f"Тело {self.name[1].name} парализовано...")
                elif self.status == 4:
                    narrator(f"{self.name[0].name} не может сопртивляться воле монстра...")
                elif self.status == 5:
                    narrator(f"{self.name[0].name} загипнотизирован... ")

                    renpy.jump(f"{self.world.battle.tag}_yuwaku_a")
                elif self.status == 7:
                    narrator(f"{self.name[0].name} в замешательства...")

                    renpy.jump(f"{self.world.battle.tag}_main")
                elif self.status == 8:
                    narrator(f"Часть тела {self.name[1].name} окаменела...")

                    if not self.surrendered:
                        renpy.jump(f"{self.world.battle.tag}_main")
                    elif self.surrendered:
                        renpy.jump(f"{self.world.battle.tag}_a")

                renpy.jump(f"{self.world.battle.tag}_a")

            elif kind == 2:
                if self.surrendered:
                    self.status_sel(1)

                if persistent.difficulty == 3:
                    if rand().randint(1, 2) == 2:
                        self.status_sel(1)

                if self.status == 1:
                    "[ori_name!t] broke free of the trance!"
                elif self.status == 2:
                    "[ori_name!t] shook off the paralysis!"
                elif self.status == 4:
                    "[ori_name!t] regains his senses!"
                elif self.status == 5:
                    "[ori_name!t] broke the mesmerization!"
                elif self.status == 7:
                    "[ori_name!t] is no longer confused!"

                self.status = 0
                self.status_print()

                # if owaza_num2 < 2:
                #     $ owaza_count2a = 0
                # elif owaza_num2 == 2:
                #     $ owaza_count2b = 0
                # elif owaza_num2 == 3:
                #     $ owaza_count2c = 0
                # elif owaza_num2 == 4:
                #     $ owaza_count2d = 0

                # $ owaza_num2 = 0
                # $ tuika_owaza_count1a = 0
                # $ tuika_owaza_count1b = 0
                # $ tuika_owaza_count1c = 0
                # $ tuika_owaza_count1d = 0
                # $ tuika_owaza_count1e = 0
                # $ tuika_owaza_count1f = 0
                # $ tuika_owaza_count1g = 0
                # $ tuika_owaza_count1h = 0
                # $ tuika_owaza_count2a = 0
                # $ tuika_owaza_count2b = 0
                # $ tuika_owaza_count2c = 0
                # $ tuika_owaza_count2d = 0
                # $ tuika_owaza_count3a = 0
                # $ tuika_owaza_count3b = 0
                # $ tuika_owaza_count3c = 0
                # $ tuika_owaza_count3d = 0
                renpy.jump(f"{self.world.battle.tag}_main")

            elif kind == 3:
                narrator(f"{self.status_turn+1} ходов до полного окаменения!")

                if not self.status_turn:
                    "Остался всего один ход до окаменения!"

                if self.surrendered:
                    renpy.jump(f"{self.world.battle.tag}_a")

                renpy.jump(f"{self.world.battle.tag}_main")

        # Изменяет спрайт противника в соответствии с состоянием
        def status_print(self, face=0):
            self.guard = 0

            if not self.bind:
                renpy.hide("ct")
            elif not self.bind and not face:
                for enemy in self.world.battle.enemy:
                    if enemy is not None:
                        renpy.show(enemy.sprite[3], at_list=[enemy.position], zorder=5)

            renpy.transition(dissolve, layer="master")

        # Спад яда
        def poisonfade(self):
            self.poison = 0
            if self.status == 9:
                self.status = 0

            narrator("Яд выходит из тела Луки!")

            self.status_print()

        def poison_damage(self):
            renpy.sound.play("se/aqua3.ogg", channel="sound2")
            narrator(f"Яд медленно разрушает тело {self.name[1].name}!")

            if self.world.battle.tag == "erubetie_ng":
                self.world.battle.enemy[0].deal((150, 200), poison_damage=True)
            elif self.world.battle.tag == "lilith_ng":
                self.world.battle.enemy[0].deal((280, 350), poison_damage=True)
            else:
                self.world.battle.enemy[0].deal((self.max_life // 15, self.max_life // 9), poison_damage=True)

            if not self.life:
                renpy.jump(f"{self.world.battle.tag}_hpoison")

        def moans(self, moan):
            if moan != 4 and self.status == 1:
                self.world.battle.cry(self.name[0], [
                    "Фуаа...",
                    "Ааа...",
                    "Хааа...",
                    "Слишком... слишком хорошо..."
                ])
            elif moan < 2:
                self.world.battle.cry(self.name[0], [
                    "Ааа... кончаю!",
                    "Фуаааа!",
                    "Аааааа!",
                    "Уууууу!"
                ])
            elif moan == 2:
                self.world.battle.cry(self.name[0], [
                    "Аааахх!",
                    "Ааа!",
                    "Нет... я больше этого не выдержу!",
                    "Ах... слишком хорошо..."
                ])
            elif moan == 3:
                self.world.battle.cry(self.name[0], [
                    "Аргх! Я сейчас кончу!",
                    "К-кончаю!",
                    "Угх! Х-хватит!",
                    "Я... больше не выдержу!"
                ])
            elif moan == 4:
                self.world.battle.cry(self.name[0], [
                    "Ургх...!",
                    "Аргх...",
                    "Ах... ахх...",
                    "Угх... ахх..."
                ])
