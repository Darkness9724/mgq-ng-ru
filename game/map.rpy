init python:
    class Map:
        def __init__(self, world):
            self.world = world
            self.region = 0
            self.result = 0

        def __call__(self, id):
            exec("self.map%d()" % id)

        def move(self, field):
            renpy.sound.play("se/asioto2.ogg")
            renpy.scene()
            renpy.show("bg black")
            renpy.with_statement(blinds)
            renpy.music.play("bgm/field%s.ogg" % field)

        def map1(self):
            renpy.scene()
            renpy.show(self.world.background)

            while True:
                renpy.start_predict_screen("map1")
                _window_hide(auto=True)
                renpy.transition(slideleft)
                self.result = renpy.call_screen("map1")
                renpy.stop_predict_screen("map1")

                # Илиасбург
                if self.result == 1:
                    renpy.call("city03_main0", from_current=True)
                    self.move(1)
                # Илиаспорт
                elif self.result == 2 and not self.world.event["iliasport"]:
                    renpy.jump("lb_0023")
                elif self.result == 2 and self.world.event["iliasport"]:
                    renpy.jump("iliascontinent_end")
                # Горы Ирины
                elif self.result == 3 and not self.world.event["irina"]:
                    renpy.jump("lb_0015")
                elif self.result == 3 and self.world.event["irina"]:
                    renpy.jump("lb_0017")
                # Деревня Счастья
                elif self.result == 4 and not self.world.event["harpy"][0]:
                    renpy.jump("lb_0018")
                elif self.result == 4 and self.world.event["harpy"][0]:
                    renpy.call("city04_main0", from_current=True)
                    self.move(1)
                # Энрика
                elif self.result == 5 and not self.world.event["enrika"][0]:
                    renpy.jump("lb_0010")
                elif self.result == 5 and self.world.event["enrika"][0]:
                    renpy.jump("lb_0013")
                # Деревня Илиас
                elif self.result == 6:
                    renpy.jump("lb_0014")

            _window_show(auto=True)

        def map1_1(self):
            while True:
                renpy.start_predict_screen("map1_1")
                _window_hide(auto=True)
                renpy.transition(slideleft)
                self.result = renpy.call_screen("map1_1")
                renpy.stop_predict_screen("map1_1")

                if self.result == 7:
                    renpy.jump("lb_0026a")

            _window_show(auto=True)

        def map0(self):
            renpy.scene()
            renpy.show(self.world.background)

            while True:
                renpy.start_predict_screen("map0")
                _window_hide(auto=True)
                renpy.transition(slideleft)
                self.result = renpy.call_screen("map0")
                renpy.stop_predict_screen("map0")

                if self.result == 1:
                    self.map2()
                elif self.result == 2:
                    self.map3()
                elif self.result == 3:
                    self.map4()
                elif self.result == 4:
                    self.map5()

            _window_show(auto=True)

        def map2(self):
            renpy.scene()
            renpy.show(self.world.background)

            if self.region != 2:
                renpy.music.stop(fadeout=1.0)
                renpy.music.play("ngdata/bgm/field6.ogg")
                self.region = 2

            while True:
                renpy.start_predict_screen("map2")
                _window_hide(auto=True)
                renpy.transition(slideleft)
                self.result = renpy.call_screen("map2")
                renpy.stop_predict_screen("map2")

                if self.result == 1:
                    if not self.world.party.player.skill[1]:
                        renpy.jump("lb_0036")
                    else:
                        renpy.jump("sylph_complete")
                elif self.result == 9:
                    self.map0()

            _window_show(auto=True)

        def map3(self):
            renpy.scene()
            renpy.show(self.world.background)

            if self.region != 3:
                renpy.music.stop(fadeout=1.0)
                renpy.music.play("ngdata/bgm/field8.ogg")
                self.region = 3

            while True:
                renpy.start_predict_screen("map3")
                _window_hide(auto=True)
                renpy.transition(slideleft)
                self.result = renpy.call_screen("map3")
                renpy.stop_predict_screen("map3")

                if self.result == 1:
                    if not self.world.party.player.skill[2]:
                        renpy.jump("lb_0044")
                    else:
                        renpy.jump("gnome_complete")

                elif self.result == 2:
                    if not self.world.event["pyramid"]:
                        renpy.jump("lb_0068")
                    else:
                        renpy.jump("pyramid_complete")

                elif self.result == 9:
                    self.map0()

            _window_show(auto=True)

        def map4(self):
            renpy.scene()
            renpy.show(self.world.background)

            if self.region != 8:
                renpy.music.stop(fadeout=1.0)
                renpy.music.play("ngdata/bgm/field9.ogg")
                self.region = 4

            while True:
                renpy.start_predict_screen("map4")
                _window_hide(auto=True)
                renpy.transition(slideleft)
                self.result = renpy.call_screen("map4")
                renpy.stop_predict_screen("map4")

                if self.result == 1:
                    if not self.world.party.player.skill[3]:
                        renpy.jump("lb_0050")
                    elif self.world.party.player.skill[1:5].count(3) == 4 and not self.world.event["spring"][0]:
                        renpy.jump("lb_0066")
                    else:
                        renpy.jump("undine_complete")
                elif self.result == 9:
                    self.map0()

            _window_show(auto=True)

        def map5(self):
            renpy.scene()
            renpy.show(self.world.background)

            if self.region != 9:
                renpy.music.stop(fadeout=1.0)
                renpy.music.play("ngdata/bgm/field10.ogg")
                self.region = 5

            while True:
                renpy.start_predict_screen("map5")
                _window_hide(auto=True)
                renpy.transition(slideleft)
                self.result = renpy.call_screen("map5")
                renpy.stop_predict_screen("map5")

                if self.result == 1:
                    if not self.world.party.player.skill[4]:
                        renpy.jump("lb_0058")
                    else:
                        renpy.jump("salamander_complete")
                elif self.result == 9:
                    self.map0()

            _window_show(auto=True)

label region_complete:
    "Я вновь открываю свою карту."

    $ world.map(0)

label sylph_complete:
    $ renpy.show(world.background)
    show sylph st21 at xy(0.5, 0.4)

    sylph "А?{w}\nЗачем ты хочешь вернуться в лес духов, Лука?"
    sylph "Я уже с тобой!"
    l "Ну..."
    "Я в любом случае ничего не смогу сделать с лабораторией Сироме без Курому.{w}\nДумаю Сильфа права."

    jump region_complete

label gnome_complete:
    $ renpy.show(world.background)

    l "......"
    "Я взглянул на руины Сафару на карте.{w}\nГнома уже со мной, так что у меня нет никаких причин возвращаться туда сейчас."

    jump region_complete

label undine_complete:
    $ renpy.show(world.background)

    l "............"
    "Я взглянул на источник Ундины на карте."

    show undine st23 at xy(0.5, 0.5)

    undine "Нет."
    l "А?"

    show undine st21

    if world.event["spring"][0]:
        undine "Тебе и так было позволено его осмотреть."
        if world.party.check(5):
            undine "Если ты сейчас вернёшься туда, кто знает как отреагирует Эрубети.{w}\nТак что, пожалуйста, давай как-нибудь в другой раз."
        else:
            undine "Если ты сейчас вернёшься туда, кто знает что учудит Эрубети.{w}\nТак что, пожалуйста, просто оставь её в покое."
    else:
        undine "Ты уже достаточно натворил дел в моём источнике."
        undine "Ты уже получил мою силу как и хотел.{w}\nТак что больше тебе там нечего делать."
        undine "Оставь его в покое."

    "Резко указывает мне Ундина."
    l "Ну, ладно...{w}\nПрости..."

    jump region_complete

label salamander_complete:
    $ renpy.show(world.background)

    l "Угх, Золотой вулкан..."
    "Вот ни за что в жизни больше туда не вернусь."
    show salamander st27 at xy(0.5, 0.5)
    salamander "..............."
    "Внутри моего сознания появляется Саламандра, недовольно поглядывая."
    l "Люди не выносят таких температур.{w}\nТы стихийный дух самого огня, конечно же тебе легче."
    show salamander st28
    salamander "Слабак...{w}\nВот я останусь с тобой даже несмотря на сильный холод, знаешь ли."
    show salamander st27
    l "Но у нас всё равно нет причин туда возвращаться!"

    jump region_complete

label pyramid_complete:
    $ renpy.show(world.background)

    show alice st01b

    a "Собираешься вернуться в пирамиду?{w}\nНам же больше нечего там делать..."
    l "Это так..."
    "Всё никак не могу выбросить Сфинкса из головы, она осталась там совсем одна...{w}\nНо ничего не изменится, если я вернусь."
    "Поэтому, нашей следующей остановкой будет..."

    jump region_complete
