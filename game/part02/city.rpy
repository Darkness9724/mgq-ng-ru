label city06_main:
    python:
        world.city.id = "city06"
        world.city.background = "bg 037"
        world.city.button = {
            1: youth,
            2: man,
            3: woman,
            4: soldier,
            5: mermaid_a,
            6: mermaid_b,
            7: mermaid_girl,
            8: mermaid_merchant,
            9: "",
            10: "Оружейный магазин",
            11: "Лавка",
            12: "Церковь",
            13: "Русалочий бар",
            14: "",
            15: "",
            16: "",
            17: "",
            18: ""
        }
        world.city.exit = "Покинуть город"
        world.city.call_cmd()

    return

label city06_01:
    show mob seinen1

    youth "Почему «Бремя Илиас» так сильно ненавидят монстров...{w}\nМожет это потому что женщины так ревнуют к девушкам-монстрам?"
    youth "Ну, так говорит мне моя милая русалка..."

    return

label city06_02:
    show mob ozisan1

    man "Ненавижу беспорядочный терроризм «Бремя Илиас»."
    man "Если они взорвут бар русалок...{w}\nВозможно, тогда я сам стану террористом для террористов."

    return

label city06_03:
    show mob musume2

    woman "Я не очень люблю русалок...{w}\nНо напав на их школу, они зашли слишком далеко."

    return

label city06_04:
    show mob sensi2

    soldier "Я слышал, Гранберия напала на Илиасбург, но одинокий герой смог победить её."
    soldier "Если так подумать, то для человека одолеть её...{w}\nМир действительно удивителен, да?"

    return

label city06_05:
    show mob mermaid2

    mermaid_a "Мы, русалки, намного сильнее людей...{w}\nДаже если они взорвут наши здания, людей погибнет больше чем монстров."
    mermaid_a "... Мы обеспокоены тем, что наше присутствие приносит вред местным жителям."

    return

label city06_06:
    show mob mermaid1

    mermaid_b "Эти негодяи из «Бремя Илиас»...{w}\nЕсли они что-то имеют против нас, почему бы просто об этом не сказать?"
    mermaid_b "Нападать на школу, полную детей...{w}\nНепростительно!"

    return

label city06_07:
    show mob mermaid3

    mermaid_girl "Спасибо больше, что спас нашу школу.{w}\nМои друзья могли пострадать!"

    show mob mermaid3 at xy(0.75)
    show alma_elma st02 at xy(0.25)

    alma "А разве ты сама не должна была быть в школе, юная леди?"

    show alma_elma st01

    mermaid_girl "Эмм, я прогуляла..."
    "Кстати, зачем русалкам своя собственная школа?{w}\nРазве они не могут учиться совместо с людьми?"

    return

label city06_08:
    show meia st21 at xy(0.5625, 0.58)

    mermaid_merchant "Почему же моя жареная морская звезда не продаётся...{w}\n... Может, это плохой товар?"
    mermaid_merchant "Эй, а ты что думаешь?"
    l "Она... не очень аппетитно выглядит."
    mermaid_merchant "Понятно...{w}\nТогда поищу что-нибудь новенькое..."

    return

label city06_10:
    scene bg 150 with blinds
    show mob ozisan3

    shopkeeper "Добро пожаловать!{w}\nУ нас собрано лучшее оружие со всего света."
    l "Хмм... не думаю, что мне нужен другой меч."

    show alice st11b at xy(0.3)
    show mob ozisan3 at xy(0.7)

    shopkeeper "Возможно, этой прекрасной девушке будет интересен нож для самозащиты?"
    a "Мои кулаки — сильнейшее оружие в мире."
    shopkeeper "П-понятно..."

    scene bg 037 with blinds

    return

label city06_11:
    scene bg 154 with blinds
    show mob seinen1

    shopkeeper "Добро пожаловать!"
    l "... Эта женская одежда очень откровенная..."
    shopkeeper "Это новый писк моды.{w}\nОткровенную одежду сейчас хотят все."
    shopkeeper "Взять это, к примеру..."
    l "Да тут совсем нет ткани!"
    shopkeeper "... Разве не потрясающе?{w}\nА как насчёт этого?!"
    l "Аааа!{w}\nОденься ты так в моей деревне, солдаты из храма арестовали бы тебя!"
    shopkeeper "Хо-хо-хо... А как насчёт ЭТОГО!"

    show alma_elma st01 at xy(0.3)
    show mob seinen1 at xy(0.7)

    alma "Оооо!{w}\nМне оно нравится!"
    l "... Оно даже не прикроет все твои места!{w}\nПо сути это тоже самое, что ходить голой..."

    show alma_elma st03

    alma ".............."
    l "Ух, з-забудь!"

    hide mob
    show alice st11b at xy(0.3)
    show alma_elma st04 at xy(0.6875)

    a "........."
    l "Хья! Алиса?!"
    a "..................... Идиоты."

    scene bg 037 with blinds

    return

label city06_12:
    scene bg 156 with blinds
    show mob sinkan

    priest "Я немного понимаю позицию «Бремя Илиас».{w}\nНельзя иметь сексуальных отношений с монстрами..."
    priest "Однако в городе много людей, нарушающих этот закон.{w}\nЭто очень серьёзная проблема..."

    scene bg 037 with blinds

    return

label city06_13:
    scene bg 155 with blinds
    show mob mermaid2

    mermaid "Добро пожаловать в знаменитый русалочий бар порта Наталия!"
    mermaid "Оооуу... какой милый мальчик.{w}\nХочешь поиграть со старшей сестрёнкой?"
    "Русалка кладёт руку Луки на свою грудь!{w}\nРусалка соблазнительно дует Луке в ухо!"
    l "... Нет"
    mermaid "Ээээ?!"

    play sound "se/lvdown.ogg"

    "Лука отвергает распутную русалку!"

    scene bg 037 with blinds

    return
