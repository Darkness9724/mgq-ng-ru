init python:
    def get_alma_ng():
        girl = Chr()
        girl.unlock = persistent.alma_ng_unlock
        girl.name = "Альма Эльма"
        girl.pedia_name = "Альма Эльма"
        girl.info = """Грозная королева суккубов и небесный рыцарь ветра. За счёт своей нечеловеческой скорости и несравненными способностями как в бою, так и в постеле, Альму Эльму практически нереально победить в битве. Но несмотря на всю свою опасность, у Альмы игривый и свободолюбивый характер. Хоть она и является королевой суккубов, не похоже, чтобы её сильно заботила своя раса.
Из-за её переменчивого поведения нельзя сказать точно о чём она думает. Скорее всего она что-то недоговаривает о себе, учитывая её нелюбовь к физическому насилию при отточенных боевых искусств.
Альма изъявляет желание присоединиться к Луке после того, как он побеждает её на пляже Наталии. Непонятно, каковы её мотивы в этом, и есть ли они вообще."""
        girl.zukan_ko = "alma_ng"
        girl.label = "alma_ng"
        girl.label_cg = "alma_ng"
        girl.m_pose = alma_ng_pose
        girl.facenum = 10
        girl.posenum = 3
        girl.bk_num = 0
        girl.x[0] = -200
        girl.lv = 125
        girl.hp = 25000
        girl.scope = {
            "world.party.player.level": 83,
            "world.party.player.skill[0]": 27,
            "world.party.item[0]": 6
        }

        return girl

    def get_sylph_ng():
        girl = Chr()
        girl.unlock = persistent.sylph_ng_unlock
        girl.name = "Сильфа"
        girl.pedia_name = "Сильфа"
        girl.info = """Сильфа — одна из четырёх стихийных духов — отождествляется с ветром. Она проживает в таинственном лесу духов, что в регионе Наталия. Сильфа была первым духом, который присоединился к Луке в его первом приключении. Хоть она и похожа на фею, её истинная форма, в которой она предстала в пустыне, мало чем отличается от вида остальных духов. До сих пор не понятно, почему она предпочитает форму феи. Из-за своей болтливости Сильфа часто конфликтует с остальными тремя духами. По своему её можно назвать изгоем среди них. Впрочем, остальные духи не ненавидят её, а Гнома и вовсе проявляла привязанность в прошлом.
Хоть время от времени Сильфа и надоедлива, в остальном она и мухи не обидит, не говоря уже о монстрах с людьми. Сильфа мила со всеми, кто хочет подружиться с ней, к тому же у ней нет боевого опыта. Чтобы на равных сражаться с Лукой у ней не было иного выбора, кроме как украсть его силы и попытаться его изнасиловать. Её действия никогда не содержат в себе злого умысла, но игривая натура Сильфы в сочетании с похотливостью может доставить немало проблем.
Как воплощение ветра, даруемая ею сила хорошо синергирует с врождённой ловкостью Луки, ещё сильнее увеличивая его скорость и проворность. Сильфа с радостью согласилась заключить с ним контракт после того, как оставила свои попытки изнасиловать его."""
        girl.zukan_ko = "sylph_ng"
        girl.label = "sylph_ng"
        girl.label_cg = "sylph_ng"
        girl.m_pose = sylph_ng_pose
        girl.facenum = 8
        girl.posenum = 4
        girl.bk_num = 0
        girl.x[0] = -200
        girl.lv = 65
        girl.hp = 8000
        girl.scope = {
            "world.party.player.level": 85,
            "world.party.player.skill[0]": 27,
            "world.party.item[0]": 7
        }

        return girl

    def get_gnome_ng():
        girl = Chr()
        girl.unlock = persistent.gnome_ng_unlock
        girl.name = "Гнома"
        girl.pedia_name = "Гнома"
        girl.info = """Тихий и загадочный дух земли. Как и остальная четвёрка духов, она имеет полный контроль над своим элементом и может сделать с ним что угодно. Управляя стихией земли, она способна насылать могучие песчаные бури и наполнять своё тело силой земли без всяких проблем.
В отличии от остальных, она немногословна. Настолько, что порой кажется, что она немая, из-за чего трудно сказать о чём она думает. Никому не известна причина по которой она предпочитает не говорить. Судя по всему она вобрала в себя частички характеров остальных духов: добрая и дружелюбная как Сильфа, спокойная и отстранённая как Ундина, а иногда и проявляющая раздражение в духе Саламандры. Впрочем, любая попытка определить её личность это как попасть пальцем в небо. Единственное, что можно сказать с полной увереностью: она очень замкнутая. Но даже несмотря на это, Гнома всё же решает присоединиться к Луке вновь.
Конечно же, как и все остальные монстры, Гнома питается спермой. Своей киской она может заставить мужчину кончить множество раз за короткое время. Многие бы не отказались от этого, но даже так ей приходится использовать своих грязевых кукол для удержания добычи. Возможно она просто не любит, когда касаются её тела. Коричневая кожа и густая жидкость внутри её вагины могут говорить о том, что тело Гномы сделано из глины, но, опять же, сложно сказать правда это или нет."""
        girl.zukan_ko = "gnome_ng"
        girl.label = "gnome_ng"
        girl.label_cg = "gnome_ng"
        girl.m_pose = gnome_ng_pose
        girl.facenum = 7
        girl.posenum = 3
        girl.bk_num = 0
        girl.x[0] = -200
        girl.lv = 80
        girl.hp = 14000
        girl.scope = {
            "world.party.player.level": 85,
            "world.party.player.skill[0]": 27,
            "world.party.item[0]": 9
        }

        return girl

    def get_erubetie_ng():
        girl = Chr()
        girl.unlock = persistent.erubetie_ng_unlock
        girl.name = "Эрубети"
        girl.pedia_name = "Эрубети"
        girl.info = """Последня из четвёрки небесных рыцарей, которая встала на пути у Луки — Эрубети, рыцарь воды. Королева слизей, способная управлять жидкостями, в том числе из которой состоит её тело. В отличии от остальных рыцарей, она всем сердцем ненавидит человечество и убьёт любого, кто посмеет покуситься на источник Ундины, который является её домом. Если это мужчина, то сначала Эрубети высосет всю сперму из него, а затем раствоит заживо в своём теле. Фактически это противоречит приказу владыки монстров, но похоже ей всё равно. Будучи защитницей источника и заодно королевой слизи, она близка к Ундине, которая, как и Алиса, закрывает глаза на её поведение.
К слову, одна важная деталь — в прошлом Эрубети поглотила и ассимилировала тысячи слизей, чтобы стать сильней. Но как итог теперь её разум соткан из десятков тысяч разрозненных сознаний. Скорее всего это объясняет её нестабильную психику и поведение, хотя однозначно утверждать нельзя."""
        girl.zukan_ko = "erubetie_ng"
        girl.label = "erubetie_ng"
        girl.label_cg = "erubetie_ng"
        girl.m_pose = erubetie_ng_pose
        girl.facenum = 1
        girl.posenum = 1
        girl.bk_num = 0
        girl.x[0] = 25
        girl.x[1] = 225
        girl.lv = 125
        girl.hp = 36000
        girl.scope = {
            "world.party.player.level": 85,
            "world.party.player.skill[0]": 27,
            "world.party.item[0]": 9
        }

        return girl

    def get_undine_ng():
        girl = Chr()
        girl.unlock = persistent.undine_ng_unlock
        girl.name = "Ундина"
        girl.pedia_name = "Ундина"
        girl.info = """Безмятежная и холодная, Ундина является одной из четырёх духов, олицетворяя элемент воды. Когда-то давно, несколько сотен лет назад, её почитали как богиню наравне с остальными духами, но многое с тех пор изменилось. Как только люди стали загрязнять воду по всему миру, она возненавидала их и уединилась в своём источнике, убивая любого человека, посмеевшего потревожить её. Она близка к королеве слизей и небесному рыцарю — Эрубети, и если сравнивать их, то Ундина будет чуть более милосердной, хотя это и мало что меняет.
Обычно она убивает и поглощает любого нарушителя, но если она находится в хорошем настроении или мужчина ей понравится, она оставит его как свою игрушку. Используя своё прохладное тело из густой слизи, ровно как и стихию воды, Ундина всегда сможет доставить особое наслаждение своим жертвам. У ней сильная садистская натура, своих жертв она будет истязать удовольствием и жестокостью до тех пор, пока те не потеряют рассудок и не станут её личным секс-рабами.
Интересно, что во время первого приключения Лука она проявляла к нему симпатию и охотно стала его третьим духом. Сейчас же Ундина, видя насколько он пугающе силён, сражалась до последнего, и даже проиграв присоединилась с нежеланием. Непонятно, толи это потому что Лука запечатал её слизей и ранил Эрубети, толи есть иная причина, которая её беспокоит..."""
        girl.zukan_ko = "undine_ng"
        girl.label = "undine_ng"
        girl.label_cg = "undine_ng"
        girl.m_pose = undine_ng_pose
        girl.facenum = 7
        girl.posenum = 3
        girl.bk_num = 0
        girl.x[0] = -200
        girl.lv = 96
        girl.hp = 18000
        girl.scope = {
            "world.party.player.level": 85,
            "world.party.player.skill[0]": 27,
            "world.party.player.skill[4]": 3,
            "world.party.item[0]": 9
        }

        return girl

    def get_salamander_ng():
        girl = Chr()
        girl.unlock = persistent.salamander_ng_unlock
        girl.name = "Саламандра"
        girl.pedia_name = "Саламандра"
        girl.info = """Одна из четырёх стихийных духов, свирепое воплощение огня, созданное из тёмной энергии. Саламандра была четвёртым и последним духом, которую Лука заполучил в своём первом приключении. В отличии от остальных духов, она до сих пор известна среди монстров. Многие мечники проклятого клинка приходят к ней за советом или тренировкой, притом что её навыки фехтования никто не видел. Она также вырастила Гранберию, самого грозного пользователя этих техник.
Хоть обычно она ведёт себя спокойно и больше выполняет роль наставника для Луки, Саламандра, как дух пламени, всё ещё известна за свою вспыльчивую и хаотичную сторону, что сильно контрастирует с характером Ундины. Нехарактерно то, что в отличии от многих монстров она занимается сексом не столько ради пропитания или размножения, сколько ради удовольствия. Что характерно, она может даже разозлится, если во время него партнёр или она не будут получать наслаждения.
Саламандра может контролировать огонь и температуру своего тела по желанию, но из-за своей возбудимости её тело склонно нагреваться при приближении оргазма. Как ни странно, при прикосновении к телу это тепло расслабляет, и если даже этого недостаточно для удовлетворения её избранника, феромоны сделают оставшееся дело. Конечно же, Саламандра не была бы Саламандрой, если бы по ходу дела не теряла рассудок и не начинала насиловать своего партнера, пока у того не останется сил."""
        girl.zukan_ko = "salamander_ng"
        girl.label = "salamander_ng"
        girl.label_cg = "salamander_ng"
        girl.m_pose = salamander_ng_pose
        girl.facenum = 10
        girl.posenum = 3
        girl.bk_num = 0
        girl.x[0] = -200
        girl.lv = 99
        girl.hp = 20000
        girl.scope = {
            "world.party.player.level": 85,
            "world.party.player.skill[0]": 27,
            "world.party.item[0]": 9
        }

        return girl


    def get_micaela_ng():
        girl = Chr()
        girl.unlock = persistent.micaela_ng_unlock
        girl.name = "Микаэла"
        girl.pedia_name = "Падший ангел Микаэла"
        girl.info = """Сестра Люцифины, тётушка Луки и самый первый ангел, которого создала Илиас. Сейчас падший ангел, проживающий в Энрике.
Хоть она и служила Илиас верой и правдой, и была даже более лояльна нежели Люцифина, в конце концов она тоже увидела безумие своей богини. Но в отличии от Люцифины Микаэла сначала пыталась наставить Илиас на путь истинный, но поняв бесполезность этого занятия, она бежала на поверхность вслед за своей младшей сестрой и таким образом стала падшей. Сейчас, потеряв и Илиас, и Люцифину, Микаэла скорее всего чувствует себя одиноко, пусть и пытается это скрыть.
Прожив миллионы лет, мораль смертных для неё чужда, и она с удовольствием будет мучить свою жертву, пока та не сойдёт с ума. Но если её не злить, она добра и даже мухи не обидит, хотя на дружелюбие тоже рассчитвать не стоит. Микаэла подпускает к себе лишь тех, кого уважает.
Когда Лука пришёл к ней за помощью в Энрику, Микаэла тут же бросилась помогать ему. Возможно это потому, что она чувствует вину за его ужасную жизнь и смерть Люцифины. Впрочем, видимо она всё же кое-что недоговаривает по поводу всей этой ситуации..."""
        girl.zukan_ko = None
        girl.label = "micaela_ng"
        girl.label_cg = "micaela_ng"
        girl.m_pose = micaela_ng_pose
        girl.facenum = 7
        girl.posenum = 3
        girl.bk_num = 0
        girl.x[0] = -200
        girl.lv = "500?"
        girl.hp = 60000
        girl.scope = {
            "world.party.player.level": 85,
            "world.party.player.skill[0]": 27,
            "world.party.item[0]": 9
        }

        return girl


    def alma_ng_pose(girl, x):
        imgs = []

        if girl.pose == 1 and girl.face == 4:
            girl.face = 1
        elif girl.pose == 2 and girl.face == 4:
            girl.face = 1
        elif girl.pose == 3 and girl.face == 10:
            girl.face = 1
        elif girl.pose == 4:
            girl.pose = 1

        if girl.pose == 1:
            if girl.face == 1:
                imgs.append(["alma_elma st01", (x, 0)])
            elif girl.face == 2:
                imgs.append(["alma_elma st02", (x, 0)])
            elif girl.face == 3:
                imgs.append(["alma_elma st03", (x, 0)])
            elif girl.face == 4:
                imgs.append(["alma_elma st04", (x, 0)])
            elif girl.face == 5:
                imgs.append(["alma_elma st05", (x, 0)])
            elif girl.face == 6:
                imgs.append(["alma_elma st06", (x, 0)])
            elif girl.face == 7:
                imgs.append(["alma_elma st07", (x, 0)])
            elif girl.face == 8:
                imgs.append(["alma_elma st08", (x, 0)])
            elif girl.face == 9:
                imgs.append(["alma_elma st09", (x, 0)])

        elif girl.pose == 2:
            if girl.face == 1:
                imgs.append(["alma_elma st21", (x, 0)])
            elif girl.face == 2:
                imgs.append(["alma_elma st22", (x, 0)])
            elif girl.face == 3:
                imgs.append(["alma_elma st23", (x, 0)])

        elif girl.pose == 3:
            if girl.face == 1:
                imgs.append(["alma_elma st51", (x, 0)])
            elif girl.face == 2:
                imgs.append(["alma_elma st52", (x, 0)])
            elif girl.face == 3:
                imgs.append(["alma_elma st53", (x, 0)])

            if girl.bk[0]:
                imgs.append(["alma_elma bk01", (x, 0)])
            if girl.bk[1]:
                imgs.append(["alma_elma bk02", (x, 0)])
            if girl.bk[2]:
                imgs.append(["alma_elma bk03", (x, 0)])

        return imgs

    def sylph_ng_pose(girl, x):
        imgs = []

        if girl.pose == 1 and girl.face == 9:
            girl.face = 1
        elif girl.pose == 2 and girl.face == 3:
            girl.face = 1
        elif girl.pose == 3 and girl.face == 4:
            girl.face = 1
        elif girl.pose == 4 and girl.face == 5:
            girl.face = 1
        elif girl.pose == 5:
            girl.pose = 1

        if girl.pose == 1:
            if girl.face == 1:
                imgs.append(["sylph st01", (x, 0)])
            elif girl.face == 2:
                imgs.append(["sylph st02", (x, 0)])
            elif girl.face == 3:
                imgs.append(["sylph st03", (x, 0)])
            elif girl.face == 4:
                imgs.append(["sylph st04", (x, 0)])
            elif girl.face == 5:
                imgs.append(["sylph st05", (x, 0)])
            elif girl.face == 6:
                imgs.append(["sylph st06", (x, 0)])
            elif girl.face == 7:
                imgs.append(["sylph st07", (x, 0)])
            elif girl.face == 8:
                imgs.append(["sylph st08", (x, 0)])

            if girl.bk[0]:
                imgs.append(["sylph bk01", (x, 0)])
            if girl.bk[1]:
                imgs.append(["sylph bk02", (x, 0)])
            if girl.bk[2]:
                imgs.append(["sylph bk03", (x, 0)])

        elif girl.pose == 2:
            if girl.face == 1:
                imgs.append(["sylph st11", (x, 0)])
            elif girl.face == 2:
                imgs.append(["sylph st12", (x, 0)])

        elif girl.pose == 3:
            if girl.face == 1:
                imgs.append(["sylph st21", (x, 0)])
            elif girl.face == 2:
                imgs.append(["sylph st21b", (x, 0)])
            elif girl.face == 3:
                imgs.append(["sylph st21c", (x, 0)])

        elif girl.pose == 4:
            if girl.face == 1:
                imgs.append(["sylph st51", (x, 0)])
            elif girl.face == 2:
                imgs.append(["sylph st53", (x, 0)])
            elif girl.face == 3:
                imgs.append(["sylph st54", (x, 0)])
            elif girl.face == 4:
                imgs.append(["sylph st55", (x, 0)])

        return imgs

    def gnome_ng_pose(girl, x):
        imgs = []

        if girl.pose == 1 and girl.face == 3:
            girl.face = 1
        elif girl.pose == 2 and girl.face == 5:
            girl.face = 1
        elif girl.pose == 3 and girl.face == 4:
            girl.face = 1
        elif girl.pose == 4 and girl.face == 5:
            girl.face = 1
        elif girl.pose == 5:
            girl.pose = 1

        if girl.pose == 1:
            if girl.face == 1:
                imgs.append(["gnome st01", (x, 0)])
            elif girl.face == 2:
                imgs.append(["gnome st02", (x, 0)])

            if girl.bk[0]:
                imgs.append(["gnome bk01", (x, 0)])
            if girl.bk[1]:
                imgs.append(["gnome bk02", (x, 0)])
            if girl.bk[2]:
                imgs.append(["gnome bk03", (x, 0)])

        elif girl.pose == 2:
            if girl.face == 1:
                imgs.append(["gnome st11", (x, 0)])
            elif girl.face == 2:
                imgs.append(["gnome st12", (x, 0)])
            elif girl.face == 3:
                imgs.append(["gnome st13", (x, 0)])
            elif girl.face == 4:
                imgs.append(["gnome st14", (x, 0)])

        elif girl.pose == 3:
            if girl.face == 1:
                imgs.append(["gnome st21", (x, 0)])
            elif girl.face == 2:
                imgs.append(["gnome st23", (x, 0)])
            elif girl.face == 3:
                imgs.append(["gnome st24", (x, 0)])

        elif girl.pose == 4:
            if girl.face == 1:
                imgs.append(["gnome st31", (x-40, 0)])
            elif girl.face == 2:
                imgs.append(["gnome st32", (x-40, 0)])
            elif girl.face == 3:
                imgs.append(["gnome st33", (x-40, 0)])
            elif girl.face == 4:
                imgs.append(["gnome st34", (x-40, 0)])

        return imgs

    def erubetie_ng_pose(girl, x):
        imgs = []

        if girl.pose == 1 and girl.face == 2:
            girl.face = 1
        elif girl.pose == 2:
            girl.pose = 1

        if girl.pose == 1 and girl.face == 1:
            imgs.append(["erubetie st01", (x, 0)])

        if girl.pose == 1:
            if girl.bk[0]:
                imgs.append(["erubetie bk01", (x, 0)])
            if girl.bk[1]:
                imgs.append(["erubetie bk02", (x, 0)])
            if girl.bk[2]:
                imgs.append(["erubetie bk03", (x, 0)])

        return imgs

    def undine_ng_pose(girl, x):
        imgs = []

        if girl.pose == 1 and girl.face == 8:
            girl.face = 1
        elif girl.pose == 2 and girl.face == 5:
            girl.face = 1
        elif girl.pose == 3 and girl.face == 2:
            girl.face = 1
        elif girl.pose == 4:
            girl.pose = 1

        if girl.pose == 1:
            if girl.face == 1:
                imgs.append(["undine st01", (x, 0)])
            elif girl.face == 2:
                imgs.append(["undine st02", (x, 0)])
            elif girl.face == 3:
                imgs.append(["undine st03", (x, 0)])
            elif girl.face == 4:
                imgs.append(["undine st04", (x, 0)])
            elif girl.face == 5:
                imgs.append(["undine st05", (x, 0)])
            elif girl.face == 6:
                imgs.append(["undine st06", (x, 0)])
            elif girl.face == 7:
                imgs.append(["undine st07", (x, 0)])

            if girl.bk[0]:
                imgs.append(["undine bk01", (x, 0)])
            if girl.bk[1]:
                imgs.append(["undine bk02", (x, 0)])
            if girl.bk[2]:
                imgs.append(["undine bk03", (x, 0)])

        elif girl.pose == 2:
            x += 226

            if girl.face == 1:
                imgs.append(["undine st21", (x, 145)])
            elif girl.face == 2:
                imgs.append(["undine st22", (x, 145)])
            elif girl.face == 3:
                imgs.append(["undine st23", (x, 145)])
            elif girl.face == 4:
                imgs.append(["undine st24", (x, 145)])

        elif girl.pose == 3 and girl.face == 1:
            imgs.append(["undine st11", (x, 0)])

        return imgs

    def salamander_ng_pose(girl, x):
        imgs = []

        if girl.pose == 1 and girl.face == 8:
            girl.face = 1
        elif girl.pose == 2 and girl.face == 8:
            girl.face = 1
        elif girl.pose == 3 and girl.face == 11:
            girl.face = 1
        elif girl.pose == 4:
            girl.pose = 1

        if girl.pose == 1:
            if girl.face == 1:
                imgs.append(["salamander st01", (x, 0)])
            elif girl.face == 2:
                imgs.append(["salamander st02", (x, 0)])
            elif girl.face == 3:
                imgs.append(["salamander st03", (x, 0)])
            elif girl.face == 4:
                imgs.append(["salamander st05", (x, 0)])
            elif girl.face == 5:
                imgs.append(["salamander st06", (x, 0)])
            elif girl.face == 6:
                imgs.append(["salamander st07", (x, 0)])
            elif girl.face == 7:
                imgs.append(["salamander st08", (x, 0)])

            if girl.bk[0]:
                imgs.append(["salamander bk01", (x, 0)])
            if girl.bk[1]:
                imgs.append(["salamander bk02", (x, 0)])
            if girl.bk[2]:
                imgs.append(["salamander bk03", (x, 0)])

        elif girl.pose == 2:
            if girl.face == 1:
                imgs.append(["salamander st11", (x, 0)])
            elif girl.face == 2:
                imgs.append(["salamander st12", (x, 0)])
            elif girl.face == 3:
                imgs.append(["salamander st13", (x, 0)])
            elif girl.face == 4:
                imgs.append(["salamander st15", (x, 0)])
            elif girl.face == 5:
                imgs.append(["salamander st16", (x, 0)])
            elif girl.face == 6:
                imgs.append(["salamander st17", (x, 0)])
            elif girl.face == 7:
                imgs.append(["salamander st17", (x, 0)])

        elif girl.pose == 3:
            x += 226

            if girl.face == 1:
                imgs.append(["salamander st21", (x, 145)])
            elif girl.face == 2:
                imgs.append(["salamander st22", (x, 145)])
            elif girl.face == 3:
                imgs.append(["salamander st23", (x, 145)])
            elif girl.face == 4:
                imgs.append(["salamander st24", (x, 145)])
            elif girl.face == 5:
                imgs.append(["salamander st25", (x, 145)])
            elif girl.face == 6:
                imgs.append(["salamander st26", (x, 145)])
            elif girl.face == 7:
                imgs.append(["salamander st27", (x, 145)])
            elif girl.face == 8:
                imgs.append(["salamander st28", (x, 145)])
            elif girl.face == 9:
                imgs.append(["salamander st29", (x, 145)])
            elif girl.face == 10:
                imgs.append(["salamander cold", (x, 145)])

        return imgs

    def micaela_ng_pose(girl, x):
        imgs = []

        if girl.pose == 1 and girl.face == 8:
            girl.face = 1
        if girl.pose == 2 and girl.face == 5:
            girl.face = 1
        elif girl.pose == 3 and girl.face == 4:
            girl.face = 1
        elif girl.pose == 4:
            girl.pose = 1

        if girl.pose == 1:
            if girl.face == 1:
                imgs.append(["micaela st21", (x, 0)])
            elif girl.face == 2:
                imgs.append(["micaela st22", (x, 0)])
            elif girl.face == 3:
                imgs.append(["micaela st23", (x, 0)])
            elif girl.face == 4:
                imgs.append(["micaela st24", (x, 0)])
            elif girl.face == 5:
                imgs.append(["micaela st25", (x, 0)])
            elif girl.face == 6:
                imgs.append(["micaela st26", (x, 0)])
            elif girl.face == 7:
                imgs.append(["micaela st27", (x, 0)])

        elif girl.pose == 2:
            x += 50

            if girl.face == 1:
                imgs.append(["micaela st11", (x, 0)])
            elif girl.face == 2:
                imgs.append(["micaela st12", (x, 0)])
            elif girl.face == 3:
                imgs.append(["micaela st13", (x, 0)])
            elif girl.face == 4:
                imgs.append(["micaela st14", (x, 0)])

        elif girl.pose == 3:
            if girl.face == 1:
                imgs.append(["micaela st01", (x, 0)])
            elif girl.face == 2:
                imgs.append(["micaela st02", (x, 0)])
            elif girl.face == 3:
                imgs.append(["micaela st03", (x, 0)])

        return imgs
