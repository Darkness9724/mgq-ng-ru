label alma_ng_start:
    python:
        world.troops[18] = Troop(world)
        world.troops[18].enemy[0] = Enemy(world)

        world.troops[18].enemy[0].name = Character("Альма Эльма")
        world.troops[18].enemy[0].sprite = ["alma_elma st51", "alma_elma st52", "alma_elma st53", "alma_elma st54"]
        world.troops[18].enemy[0].position = center
        world.troops[18].enemy[0].defense = 85
        world.troops[18].enemy[0].evasion = 80

        if world.party.player.level > 81:
            world.troops[18].enemy[0].max_life = world.troops[18].battle.difficulty(28000, 32000, 50000)
        elif world.party.player.level < 82:
            world.troops[18].enemy[0].max_life = world.troops[18].battle.difficulty(24000, 26000, 40000)

        world.troops[18].enemy[0].power = world.troops[18].battle.difficulty(0, 1, 2)

        world.troops[18].battle.tag = "alma_ng"
        world.troops[18].battle.name = world.troops[18].enemy[0].name
        world.troops[18].battle.background = "bg darkbeach"

        if persistent.music:
            world.troops[18].battle.music = 24
        else:
            world.troops[18].battle.music = 4

        world.troops[18].battle.ng = True
        world.troops[18].battle.exp = 1750000
        world.troops[18].battle.exit = "lb_0035"

        world.troops[18].battle.init(0)

    world.battle.enemy[0].name "Уфу-фу-фу...{w}\nЯ собираюсь изнасиловать тебя, малыш Лука..."
    "На лице Альмы застыла непристойная ухмылка.{w}\nЕё явно забавляет вся эта ситуация..."
    world.battle.enemy[0].name "Ау...{w}\nНе смотри на меня так серьёзно, Лука..."
    world.battle.enemy[0].name "Если ты выиграешь, я позволю тебе пройти..{w}{nw}"

    $ world.battle.face(2)

    extend "\n... Но если ты проиграешь, будешь высушен досуха."

    $ world.battle.face(1)

    world.battle.enemy[0].name "Без разницы, что в итоге будет, все останутся в выигрыше."
    l "..................."
    "Ну, так и так, но мне пришлось бы драться с ней.{w}\nОна одна из Небесных Рыцарей, которых я должен победить!"

    $ world.party.player.skill_set(2)
    $ world.battle.enemy[0].wind = 1
    $ world.battle.enemy[0].wind_turn = -1

    $ world.battle.main()

label alma_ng_main:
    python:
        if world.battle.result == 15:
            renpy.jump(f"{world.battle.tag}_edging")

        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1 and world.party.player.aqua:
            world.battle.common_attack()
        elif world.battle.result == 1 and not world.party.player.aqua:
            renpy.jump(f"{world.battle.tag}_miss")
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label alma_ng_edging:
    $ world.battle.face(2)
    world.battle.enemy[0].name "Ох, так ты не хочешь сражаться?{w}\nХочешь просто сдаться?"
    world.battle.enemy[0].name "Что ж, это тоже неплохо...{w}\n В таком случае я покажу тебе бонус..."
    if not world.party.player.bind:
        call alma_ng_a9
    call alma_ng_a10
    call alma_ng_vaginalhell
    $ world.battle.bad = [
        "Лука использовал крайность против Альмы, став её секс-рабом.",
        "... Чёртов изврат."
    ]

    $ world.battle.badend()

label alma_ng_miss:
    $ world.party.player.before_action = 3
    $ world.battle.cry(world.party.player.name[0], [
        "Хаа!",
        "Ораа!",
        "Получай!"
    ])

    "Лука атакует!"
    $ world.party.player.mp_regen()
    play sound "se/wind2.ogg"
    hide alma_elma
    pause .5
    $ world.battle.face(1)

    "Но Альма Эльма легко уворачивается!"

    world.battle.enemy[0].name "Слишком медленно, малыш Лука..."

    "Дерьмо...{w}\nНе думаю, что в таком духе я по ней попаду..."

    $ world.battle.enemy[0].attack()

label alma_ng_v:
    $ world.battle.face(3)

    if world.party.player.life == world.party.player.max_life and not world.party.player.nodamage:
        $ persistent.count_hpfull = True

    if persistent.difficulty == 3:
        $ persistent.count_hellvic = True

    if world.party.player.status == 7:
        $ persistent.count_vickonran = True

    $ renpy.end_replay()

    world.battle.enemy[0].name "... Ауч."
    world.battle.enemy[0].name "Похоже что... ты победил, малыш Лука."
    play sound "se/down.ogg"
    "Альма Эльма падает на землю без сознания!"
    hide alma_elma

    $ world.battle.victory(1)

label alma_ng_a:
    $ world.battle.stage[4] -= 1
    $ world.battle.stage[3] += 1
    if not world.battle.enemy[0].life:
        jump alma_ng_v

    if world.party.player.bind and world.battle.stage[4] < 1:
        call alma_ng_a10
        $ world.battle.main()
    elif world.party.player.bind and world.battle.stage[4]:
        call alma_ng_a11
        $ world.battle.main()

    if world.party.player.mp > 7:
        $ random = rand().randint(1,100)

        if random < 31:
            call alma_ng_a1
            $ world.battle.main()

    if world.party.player.mp > 4 and world.battle.enemy[0].life < (world.battle.enemy[0].max_life / 2):
        $ random = rand().randint(1,100)

        if random < 41:
            call alma_ng_a1
            $ world.battle.main()

    while True:
        $ random = rand().randint(1,10)

        if random == 1 and world.party.player.mp > 2:
            call alma_ng_a1
            $ world.battle.main()

        elif random == 2:
            call alma_ng_a2
            $ world.battle.main()

        elif random == 3 and world.battle.stage[3] > 1:
            call alma_ng_a3
            $ world.battle.main()

        elif random == 4:
            call alma_ng_a4
            $ world.battle.main()

        elif random == 5:
            call alma_ng_a5
            $ world.battle.main()

        elif random == 7:
            call alma_ng_a7
            $ world.battle.main()

        elif random == 8 and world.battle.stage[4] < 1:
            call alma_ng_wind
            $ world.battle.main()

        elif random == 9 and world.battle.enemy[0].life < (world.battle.enemy[0].max_life / 2):
            call alma_ng_wind2
            $ world.battle.main()

        elif random == 10 and not world.party.player.bind and world.party.player.status == 0:
            call alma_ng_a9
            $ world.battle.main()

label alma_ng_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ты станешь моей едой...",
        "Я могу использовать хвост...",
        "Это просто прелюдия..."
    ])

    $ world.battle.show_skillname("Хвост Суккуба: Поглощение Магии")
    play sound "audio/se/tuki.ogg"

    "Альма Эльма трясёт своим хвостом перед Лукой!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and persistent.difficulty == 1 and random < 10:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 2 and random < 3:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 3 and random < 1:
            world.party.player.aqua_guard()

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()

        if world.party.player.daystar:
            world.party.player.skill22x()

    "Её хвост засасывает член Луки внутрь!{w}\nТаинственная сила истощает его!"

    if persistent.difficulty == 1:
        $ random = rand().randint(2,4)

    elif persistent.difficulty == 2:
        $ random = rand().randint(3,5)

    elif persistent.difficulty == 3:
        $ random = rand().randint(6,8)

    play sound "audio/se/down2.ogg"

    $ world.party.player.mp -= random
    if world.party.player.mp < 0:
        $ world.party.player.mp = 0

    "Лука теряет [random] SP!"

    python:
        world.battle.enemy[0].damage = (100,150)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label alma_ng_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Как насчёт этого?",
        "А вот и я!"
    ])

    $ world.battle.show_skillname("Хвост Суккуба: Поглощение Здоровья")
    play sound "audio/se/tuki.ogg"

    "Альма Эльма трясёт своим хвостом перед Лукой!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and persistent.difficulty == 1 and random < 10:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 2 and random < 5:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 3 and random < 3:
            world.party.player.aqua_guard()

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 15, 2: 10, 3: 5}):
            world.party.player.wind_guard()
            renpy.return_statement()

    "Её хвост начинает сильно сосать член Луки!"

    python:
        world.battle.enemy[0].damage = {1: (370, 400), 2: (410, 450), 3: (460, 510)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], drain=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label alma_ng_wind:
    $ world.battle.stage[4] = 1

    world.battle.enemy[0].name "Я ослаблю тебя своим хвостом..."
    play sound "audio/se/tuki.ogg"

    $ world.battle.show_skillname("Хвост Суккуба: Поглощение Уровня")

    "Альма Эльма трясёт своим хвостом перед Лукой!"

    "Хвост начинает сосать член Луки!{w}\nУровень Луки был поглащён!"

    play sound "audio/se/down2.ogg"

    if world.party.player.daystar:
        $ world.party.player.skill22x()

    if persistent.difficulty == 1:
        $ world.party.player.drain(3)

    elif persistent.difficulty == 2:
        $ world.party.player.drain(4)

    elif persistent.difficulty == 3:
        $ world.party.player.drain(5)

    $ world.battle.hide_skillname()

    if not world.party.player.life or world.party.player.level == 1:
        $ renpy.jump("badend_h")

    return

label alma_ng_wind2:
    $ world.battle.stage[0] = 2

    world.battle.enemy[0].name "Не хочешь немного поиграть?.."

    $ world.battle.show_skillname("Феромоны Суккуба")
    play sound "audio/se/wind1.ogg"

    "Опьяняющий аромат воздействует на сознание Луки!"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 100, 2: 100, 3: 100}):
            world.party.player.wind_guard()
            renpy.return_statement()

    if world.party.player.daystar:
        $ world.party.player.skill22x()

    $ world.party.player.status = 1
    $ world.party.player.status_turn = 2

    "Лука поддаётся искушнию Альмы!"

    return

label alma_ng_a3:
    $ world.battle.skillcount(2, 3027)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хе-хе... ты собираешься испачкать мою руку своей спермой?",
        "Я не могу ничего поделать с желанием подразнить его~{image=note}.",
        "Хе-хе... приятно, правда?"
    ])

    $ world.battle.show_skillname("Очумелые ручки")
    play sound "audio/se/ero_koki1.ogg"

    "Альма Эльма дотрагивается своими руками до члена Луки!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and persistent.difficulty == 1 and random < 10:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 2 and random < 5:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 3 and random < 3:
            world.party.player.aqua_guard()

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()

        world.battle.enemy[0].damage = {1: (450, 470), 2: (500, 520), 3: (550, 570)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label alma_ng_a4:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Приёмы моих рук заставят тебя корчиться и биться в экстазе...",
        "Ни один человек не может стерпеть моих пальчиков...",
        "Что ты об этом думаешь, а? Это ручное мастерство Королевы Суккубов..."
    ])

    $ world.battle.show_skillname("Древние Техники Суккубов: Змея")
    play sound "audio/se/ero_koki1.ogg"

    "Все десять пальчиков Альмы пробегаются вверх-вниз по члену Луки!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and persistent.difficulty == 1 and random < 10:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 2 and random < 5:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 3 and random < 3:
            world.party.player.aqua_guard()

    "Её пальчики трогают ползают по члену Луки будто змея!{w}{nw}"

    python:
        world.battle.enemy[0].damage = {1: (170, 190), 2: (200, 230), 3: (260, 290)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    "Её десять пальцев извиваются по всему члену!{w}{nw}"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    "После чего она начинает выжимать его!"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label alma_ng_a5:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хорошенько испробуй мастерство ротика самой Королевы Суккубов...",
        "Я высосу из тебя всё, хочешь ты этого или нет...",
        "Я усосу тебя в подчинение..."
    ])

    $ world.battle.show_skillname("Дух Романус Тери")
    play sound "audio/se/ero_buchu3.ogg"

    "Альма Эльма берёт член Луки в рот и начинает сосать!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and persistent.difficulty == 1 and random < 10:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 2 and random < 5:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 3 and random < 3:
            world.party.player.aqua_guard()

    "Её язык начинает вылизывать весь член Луки!{w}{nw}"

    python:
        world.battle.enemy[0].damage = {1: (170, 190), 2: (200, 230), 3: (260, 290)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    "Мягкий язык Альмы сдавливает член Луки!{w}{nw}"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    "Обвив его своим мокрым языком, Альма Эльма начинает сильно сосать член Луки!"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label alma_ng_a6:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Даже мои сиськи - оружие...",
        "Моя грудь приносят сказочное наслаждение...",
        "Я утоплю тебя в своих сиськах..."
    ])

    $ world.battle.show_skillname("Мело Софи Теллус")
    play sound "audio/se/ero_koki1.ogg"

    "Альма Эльма сдавливает член Луки между своими большими сиськами!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and persistent.difficulty == 1 and random < 10:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 2 and random < 5:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 3 and random < 3:
            world.party.player.aqua_guard()

    "Зефирные прикосновения окружают Луку со всех сторон!{w}{nw}"

    python:
        world.battle.enemy[0].damage = {1: (170, 190), 2: (200, 230), 3: (260, 290)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    "Альма Эльма медленно сжимает свои сиськи вокруг члена Луки!{w}{nw}"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    "После чего она начинает двигать своей грудью вверх-вниз!"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label alma_ng_a7:
    $ world.battle.skillcount(2, 3636)

    world.battle.enemy[0].name "Погрузись в агонию удовольствия от моего секретного навыка!"

    $ world.battle.show_skillname("Церемония Суккуба")

    "Альма Эльма хватает Луку сзади!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and persistent.difficulty == 1 and random < 10:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 2 and random < 5:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 3 and random < 3:
            world.party.player.aqua_guard()

    play sound "audio/se/ero_koki1.ogg"

    "Обхватив Луку сзади, она начинает поглаживать его член!{w}{nw}"

    python:
        world.battle.enemy[0].damage = {1: (90, 110), 2: (93, 113), 3: (96, 116)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/ero_paizuri.ogg"

    "Быстро сбивая Луку с ног, она обхватывает его член своими сиськами и начинает сдавливать его!{w}{nw}"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/ero_sigoki1.ogg"

    "Когда Лука пытается встать, Альма Эльма быстро вскакивает и, наступая на его член, прижимает вновь к земле!{w}{nw}"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/ero_koki1.ogg"

    "После чего, она нажимает сильнее своей ногой!{w}{nw}"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/ero_simetuke2.ogg"

    "Наклонившись над ним, она сжимает член Луки тыльной стороной своего бедра!{w}{nw}"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/ero_sigoki1.ogg"

    "Тряся ими из стороны в сторону, она продолжает сжимать член Луки между своих бёдер!{w}{nw}"

    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label alma_ng_a8:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Растай от моего поцелуя...",
        "Я украду твоё сердце...",
        "Твоё сердце отныне принадлежит мне..."
    ])

    $ world.battle.show_skillname("Совращающий поцелуй")
    play sound "audio/se/ero_chupa2.ogg"

    "Альма Эльма соприкасается своими губами с губами Луки!{w}{nw}"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and persistent.difficulty == 1 and random < 10:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 2 and random < 5:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 3 and random < 3:
            world.party.player.aqua_guard()

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()

    extend "\nПосле чего, её влажный язык проскальзывает в рот Луки!{w}{nw}"

    if world.party.player.daystar:
        $ world.party.player.skill22x()

    "После поцелуя развратное ощущение охватывает тело Луки!"

    $ world.battle.skillcount(2, 3637)

    if world.party.player.status == 0:
        "Лука очарован Альмой!"
    elif world.party.player.status == 1:
        "Лука загипнотизирован..."

    $ world.party.player.status = 5
    $ world.party.player.status_print()

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    if world.party.player.surrendered:
        return

    l "Хаа..."
    world.battle.enemy[0].name "Хорошие ощущение, не правда ли?{w}\nПозволь мне сделать тебе ещё лучше..."

    if persistent.difficulty == 1:
        $ world.party.player.status_turn = rand().randint(3,4)
    elif persistent.difficulty == 2:
        $ world.party.player.status_turn = rand().randint(4,5)
    elif persistent.difficulty == 3:
        $ world.party.player.status_turn = rand().randint(5,6)

    return

label alma_ng_a9:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Хочешь стать моей добычей?.."

    $ world.battle.show_skillname("Удержание Суккуба")

    "Альма Эльма опрокидывает Луку на землю и напрыгивает на него!"

    if world.party.player.daystar:
        $ world.party.player.skill22x()

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua and persistent.difficulty == 1 and random < 101:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 2 and random < 101:
            world.party.player.aqua_guard()
        elif world.party.player.aqua and persistent.difficulty == 3 and random < 98:
            world.party.player.aqua_guard()

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()

    $ world.battle.skillcount(2, 3636)

    $ world.battle.enemy[0].sprite[3] = "alma_elma hd0"
    $ world.party.player.bind = 1
    $ world.battle.enemy[0].power = 2

    "Альма Эльма крепко прижимается к Луке!"

    $ world.battle.hide_skillname()

    if world.battle.first[0]:
        if not world.party.player.surrendered:
            world.battle.enemy[0].name "А теперь пора изнасиловать тебя...{w}{nw}"

        elif world.party.player.surrendered:
            world.battle.enemy[0].name "А теперь пора изнасиловать тебя..."
            call alma_ng_a10

        extend "\nЕсли ты не будешь сопротивляться, я покажу тебе рай удовольствий."
        world.battle.enemy[0].name "Ну а если будешь, я исушу тебя без капли жалости.{w}\nИтак... что же ты будешь делать?"
    else:
        world.battle.enemy[0].name "Ты вновь собираешься вырываться?..{w}\nИли решил погрузиться в удовольствие?"

    $ world.battle.first[0] = True

    return

label alma_ng_a10:
    "Альма Эльма восседает верхом на Луке..."
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Так ты сдаёшься... в таком случае я погружу тебя в пучину наслаждений.{w}\nОщущения изменяться в зависимости от того, насколько глубоко ты войдёшь в мою киску."
        world.battle.enemy[0].name "Чем глубже, тем быстрее ты сойдёшь с ума...{w}\nЭто ультимативная техника, называемая - Пять Кругов Вагинального Ада..."
        world.battle.enemy[0].name "Ты будешь одним из немногих, кто ощутит это...{w}\nХе-хе... интересно, на каком из этапов ты сойдёшь с ума?"

    $ world.battle.show_skillname("Пять Кругов Вагинального Ада: Первая Тюрьма")
    play sound "audio/se/ero_pyu1.ogg"
    show alma_elma hd1
    "Альма Эльма медленно опускается на член Луки, вводя его кончик в свою вагину!{w}{nw}"

    $ world.battle.enemy[0].damage = (400,450)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()
    "Лука изнасилован Альмой!"
    if not world.party.player.life:
        $ renpy.jump("badend_h")
    # l "Аххх!"
    # world.battle.enemy[0].name "Хе-хе... это только начало.{w}\nЧем дальше, тем более сильное наслаждение тебя ждёт."
    # world.battle.enemy[0].name ""
    $ world.battle.show_skillname("Пять Кругов Вагинального Ада: Вторая Тюрьма")
    play sound "audio/se/ero_slime1.ogg"

    $ world.battle.enemy[0].damage = (400,450)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()
    if not world.party.player.life:
        $ renpy.jump("badend_h")
    $ world.battle.show_skillname("Пять Кругов Вагинального Ада: Третья Тюрьма")
    play sound "audio/se/ero_name2.ogg"
    if not world.party.player.life:
        $ renpy.jump("badend_h")

    $ world.battle.enemy[0].damage = (400,450)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")
    $ world.battle.show_skillname("Пять Кругов Вагинального Ада: Четвёртая Тюрьма")
    play sound "audio/se/ero_chupa5.ogg"

    $ world.battle.enemy[0].damage = (400,450)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()
    if not world.party.player.life:
        $ renpy.jump("badend_h")

label alma_ng_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Уфу-фу-фу, не хочешь сражаться со мной?{w}\nХорошо, тогда я дам тебе кое-что особенное~{image=note}!"

    call alma_ng_a9

    $ world.battle.enemy[0].attack()

label fairy_start:
    python:
        world.troops[19] = Troop(world)
        world.troops[19].enemy[0] = Enemy(world)

        world.troops[19].enemy[0].name = Character("Фея")
        world.troops[19].enemy[0].sprite = ["fairy st01", "fairy st02", "fairy st03", "fairy st02"]
        world.troops[19].enemy[0].position = xy(0.5, 0.5)
        world.troops[19].enemy[0].defense = 100
        world.troops[19].enemy[0].evasion = 75
        world.troops[19].enemy[0].max_life = world.troops[19].battle.difficulty(600, 750, 900)
        world.troops[19].enemy[0].power = world.troops[19].battle.difficulty(0, 1, 2)

        world.troops[19].battle.tag = "fairy"
        world.troops[19].battle.name = world.troops[19].enemy[0].name
        world.troops[19].battle.background = "bg 061"
        world.troops[19].battle.music = 6
        world.troops[19].battle.exp = 950
        world.troops[19].battle.exit = "lb_0038"

        world.troops[19].battle.init()

    world.battle.enemy[0].name "Цветочек~{image=note}!{w} Цветочки любят водичку-у-у-у~{image=note}!"
    "Фея мурлычет песенку, поливая поляну с цветами.{w}\nМутная, напоминающая мёд, жидкость капает из цветка в её ладонь."
    l "..........."
    "Похоже что она не заметила меня. Я не хочу испугать её.{w}\nМожет быть хоть в этот раз мне удасться прокрасться незамеченным..."
    world.battle.enemy[0].name "А!{w} Человек!"
    "Ну, я попытался."
    l "Ах... Привет..."
    world.battle.enemy[0].name "Эй... Давай поиграем!"
    world.battle.enemy[0].name "Я хочу подшутить с членом старшего братика, используя этот Цветок Суккуба...{w}\nЕсли я сделаю так, я смогу увидеть ту белую штуку!"
    world.battle.enemy[0].name "Этот цветок может выпить всю белую штуку...{w}\nИ тогда цветочек будет очень рад~~{image=note}!"
    l "............"

    play music "bgm/battle.ogg"

    "Мне правда не хочется этого делать..."

    $ world.battle.main()


label fairy_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()


label fairy_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Ой-ой?"

    play sound "se/syometu.ogg"
    hide fairy with crash

    "Фея превратилась в бабочку!"

    $ world.battle.victory(1)

label fairy_a:
    if world.party.player.bind:
        $ random = rand().randint(1,3)

        if random == 1:
            call fairy_a4
            $ world.battle.main()
        elif random == 2:
            call fairy_a5
            $ world.battle.main()
        elif random == 3:
            call fairy_a6
            $ world.battle.main()

    while True:
        $ random = rand().randint(1,3)

        if random == 1:
            call fairy_a1
            $ world.battle.main()
        elif random == 2:
            call fairy_a2
            $ world.battle.main()
        elif random == 3 and world.battle.delay[0] > 2:
            call fairy_a3
            $ world.battle.main()


label fairy_a1:
    $ world.battle.skillcount(1, 3150)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Выплесни свою белую штуку в этот цветочек~{image=note}!",
        "Эхе-хе... я поиздеваюсь над тобой этим цветочком~{image=note}.",
        "Этот цветочек хочет твоей белой жижи~{image=note}!"
    ])

    $ world.battle.show_skillname("Суккубий цветок")
    show fairy ct01 at topleft zorder 15 as ct
    play sound "audio/se/ero_buchu1.ogg"

    "Фея накрывает член Луки цветком!{w}\nПокрытые мёдом лепестки ласкают его!"

    python:
        world.battle.enemy[0].damage = {1: (20, 25), 2: (25, 30), 3: (40, 50)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label fairy_a2:
    $ world.battle.skillcount(1, 3151)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я пощекочу кончить, хе-хе~{image=note}.",
        "Если я так сделаю, оттуда же потечёт белая штука~{image=note}?",
        "Щекочу-щекочу-щекочу~{image=note}."
    ])


    $ world.battle.show_skillname("Щекотка крыльями")
    play sound "audio/se/umaru.ogg"

    "Фея кружит вокруг члена Луки, щекоча его своими крылышками!"

    python:
        world.battle.enemy[0].damage = {1: (15, 20), 2: (20, 25), 3: (30, 40)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label fairy_a3:
    $ world.battle.face(2)

    if world.battle.first[0]:
        world.battle.enemy[0].name "Эхе-хе-хе... я снова тебя поймала~{image=note}."
    else:
        world.battle.enemy[0].name "Эхе-хе-хе... я поймала твой пенис~{image=note}."

    $ world.battle.show_skillname("Зацеп")

    "Фея крепко-накрепко цепляется к члену Луки!{w}{nw}"

    show fairy ct11 at topleft zorder 15 as ct
    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    extend "{w=.7}\nФея держится за член Луки!"

    $ world.battle.hide_skillname()

    if world.party.player.surrendered:
        return

    if not world.battle.first[0]:
        l "Ва-ва-ва!{w} Отстань!"
        "Фея крепко схватилась за мой член. И я не могу её отцепить даже прикладывая усилия!"
        "Теперь весьма рискованно размахивать мечом."

    $ world.battle.face(2)

    if world.battle.first[0] == 1:
        world.battle.enemy[0].name "Эхе-хе, я снова обнимаю твой пенис~{image=note}.{w}\nВ этот раз я точно увижу твою белую штуку...{image=note}"
    else:
        world.battle.enemy[0].name "Эхе-хе, я поиграюсь с ним вот так~{image=note}.{w}\nСкоро я увижу как отсюда потечёт эта белая штука~{image=note}."

    $ world.battle.first[0] = True

    if persistent.difficulty == 1:
        $ world.battle.enemy[0].power = 2
    elif persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 2
    elif persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return


label fairy_a4:
    $ world.battle.skillcount(1, 3152)

    "Фея крепко держится за член Луки..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хе-хе-хе... я помну твой кончик~{image=note}.",
        "Мне нравятся ощущения от твоей крайней плоти!~{image=note}",
        "Хе-хе... посмотрите какая дырочка~{image=note}.",
        "Она выглядит прямо как зонтик~{image=note}!"
    ])

    $ world.battle.show_skillname("Розагрыш феи")
    play sound "audio/se/ero_koki1.ogg"

    $ world.battle.cry(narrator, [
        "Фея шлёпает обоими ручками по головке члена!",
        "Фея тянет крайнюю плоть члена Луки!",
        "Язык феи проскальзывает в уретру Луки!",
        "Фея обхватывает ручками головку члена!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (15, 20), 2: (20, 25), 3: (30, 40)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label fairy_a5:
    $ world.battle.skillcount(1, 3153)

    "Фея крепко держится за член Луки..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я подарю тебе много обнимашек~{image=note}!",
        "Эхе-хе, она такая мягкая~{image=note}!",
        "Хе-хе-хе.",
        "Если я его сожму, оттуда что-нибудь потечёт?"
    ])

    $ world.battle.show_skillname("Объятия феи")
    play sound "audio/se/ero_koki1.ogg"

    "Фея сжимает член Луки в своих объятиях!"

    python:
        world.battle.enemy[0].damage = {1: (20, 25), 2: (25, 30), 3: (40, 50)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label fairy_a6:
    $ world.battle.skillcount(1, 3154)

    "Фея крепко держится за член Луки..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Хе-хе... позволь мне исполнить танец~{image=note}!",
        "Хе-хе-хе... Кончай! Кончай~{image=note}!",
        "Я уделю внимание верхушке~{image=note}!",
        "Эхе-хе-хе... приятно?"
    ])

    $ world.battle.show_skillname("Танец феи")
    play sound "audio/se/ero_koki1.ogg"

    $ world.battle.cry(narrator, [
        "Фея проходится своими крылышками по каждому сантиметру члена Луки!",
        "Фея раскачивается туда-сюда, удерживаясь за член Луки!",
        "Фея мнёт головку члена Луки своими ручками и ножками!",
        "Фея трётся своим телом о член Луки, крепко держась за него!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (25, 30), 2: (30, 35), 3: (50, 60)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label fairy_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Уря-я-я! Теперь я смогу оторваться на тебе от души~{image=note}!"

    $ world.battle.enemy[0].attack()

label fairy_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Succubus Flower"
    $ list2 = "Wing Tickle"
    $ list3 = "Fairy Pranks"
    $ list4 = "Fairy Hug"
    $ list5 = "Fairy Dance"

    if persistent.skills[3150][0]:
        $ list1_unlock = 1

    if persistent.skills[3151][0]:
        $ list2_unlock = 1

    if persistent.skills[3152][0]:
        $ list3_unlock = 1

    if persistent.skills[3153][0]:
        $ list4_unlock = 1

    if persistent.skills[3154][0]:
        $ list5_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump fairy_onedari1
    elif world.battle.result == 2:
        jump fairy_onedari2
    elif world.battle.result == 3:
        jump fairy_onedari3
    elif world.battle.result == 4:
        jump fairy_onedari4
    elif world.battle.result == 5:
        jump fairy_onedari5
    elif world.battle.result == -1:
        jump fairy_main


label fairy_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want this flower to absorb your white stuff?"

    while True:
        call fairy_a1


label fairy_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want me to tickle you with my wings?{image=note}"

    while True:
        call fairy_a2


label fairy_onedari3:
    call onedari_syori_k

    world.battle.enemy[0].name "Hehehe... You want me to play pranks on your penis?{image=note}"

    if not world.party.player.bind:
        call fairy_a3

    while True:
        call fairy_a6


label fairy_onedari4:
    call onedari_syori_k

    world.battle.enemy[0].name "You want me to hug your penis?"

    if not world.party.player.bind:
        call fairy_a3

    while True:
        call fairy_a7


label fairy_onedari5:
    call onedari_syori_k

    world.battle.enemy[0].name "You want me to rub my body all over your penis?{image=note}"

    if not world.party.player.bind:
        call fairy_a3

    while True:
        call fairy_a8


label fairy_h1:
    $ world.party.player.moans(1)
    with syasei1
    show fairy ct02 at topleft zorder 15 as ct
    show fairy bk03 at xy(X=270, Y=130) zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the pleasure from the strange flower, Luka ejaculates.{w}\nThe flower mysteriously sucks up all of Luka's semen."

    $ world.battle.lose()

    $ world.battle.count([12, 5])
    $ bad1 = "Luka succumbed to the strange Succubus Flower."
    $ bad2 = "Caught by the Fairy, Luka was played with as a toy for the rest of his life."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hahaha... It came out~{image=note}!{w}\nDid this flower really feel that good?"
    world.battle.enemy[0].name "I'll play with you even more with this flower~{image=note}!"

    $ world.battle.stage[0] = 1
    jump fairy_h


label fairy_h2:
    $ world.party.player.moans(1)
    with syasei1
    show fairy bk02 at xy(X=270, Y=130) zorder 10 as bk
    $ world.ejaculation()

    "Tickled by the Fairy's wings, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([13, 5])
    $ bad1 = "Luka succumbed to the Fairy's wings tickling him."
    $ bad2 = "Caught by the Fairy, Luka was played with as a toy for the rest of his life."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hahaha... It came out~{image=note}!{w}\nYou made my wings all sticky~{image=note}!"

    jump fairy_h


label fairy_h3:
    $ world.party.player.moans(1)
    with syasei1
    show fairy bk01 at xy(X=270, Y=130) zorder 10 as bk
    show fairy ct12 at topleft zorder 15 as ct
    $ world.ejaculation()

    "As the Fairy plays with his penis, Luka finally ejaculates, covering her tiny body with his semen."

    $ world.battle.lose()

    $ world.battle.count([24, 5])
    $ bad1 = "While the Fairy clung to his penis, Luka succumbed to her pranks."
    $ bad2 = "Caught by the Fairy, Luka was played with as a toy for the rest of his life."
    show fairy ct13 at topleft zorder 15 as ct

    world.battle.enemy[0].name "Hahaha... It came out~{image=note}!{w}\nYou made my body all sticky~{image=note}!"

    hide ct
    jump fairy_h


label fairy_h4:
    $ world.party.player.moans(1)
    with syasei1
    show fairy bk01 at xy(X=270, Y=130) zorder 10 as bk
    show fairy ct12 at topleft zorder 15 as ct
    $ world.ejaculation()

    "As the Fairy tightly hugs his penis, Luka finally ejaculates, covering her tiny body with his semen."

    $ world.battle.lose()

    $ world.battle.count([24, 5])
    $ bad1 = "While the Fairy clung to his penis, Luka succumbed to her hug."
    $ bad2 = "Caught by the Fairy, Luka was played with as a toy for the rest of his life."
    show fairy ct13 at topleft zorder 15 as ct

    world.battle.enemy[0].name "Hahaha... It came out~{image=note}!{w}\nYou made my body all sticky~{image=note}!"

    hide ct
    jump fairy_h


label fairy_h5:
    $ world.party.player.moans(2)
    with syasei1
    show fairy bk01 at xy(X=270, Y=130) zorder 10 as bk
    show fairy ct12 at topleft zorder 15 as ct
    $ world.ejaculation()

    "As the Fairy dances around all over his penis, Luka finally ejaculates, covering her tiny body with his semen."

    $ world.battle.lose()

    $ world.battle.count([24, 5])
    $ bad1 = "While the Fairy clung to his penis, Luka succumbed to her dance."
    $ bad2 = "Caught by the Fairy, Luka was played with as a toy for the rest of his life."
    show fairy ct13 at topleft zorder 15 as ct

    world.battle.enemy[0].name "Hahaha... It came out~{image=note}!{w}\nYou made my body all sticky~{image=note}!"

    hide ct
    jump fairy_h


label fairy_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.battle.stage[0] == 1:
        play hseanwave "audio/se/hsean21_tentacle2.ogg"
    else:

        l "Auuu..."
        "Weak from the orgasm, I fall back onto the ground.{w}\nThe Fairy flies towards me, and puts the strange flower next to my groin."
        "The strange golden liquid drips from the petals of the flower.{w}\nAs I focus on the flower, all I can imagine is what it might feel like."
        world.battle.enemy[0].name "Hehehe... I'll play with you even more with this flower."
        l "Aaa... Stop... Stop!"
        "Even if I resist with my voice, my body can't..."

        play hseanwave "audio/se/hsean20_tentacle1.ogg"
        show fairy ct01 at topleft zorder 15 as ct

        "The petals wrap around my penis!"

    l "Ahh!!"
    "The flower's petals vibrate and move as if it was a living creature.{w}\nWrapped inside the mouth of the flower, a peculiar pleasure attacks me."
    "Somehow, the flower tightens down over the tip of my penis.{w}\nFinally, the petals have completely wrapped around every inch of my penis."
    world.battle.enemy[0].name "Hehehe... The flower says it loves your penis.{w}\nBut it likes the white liquid that comes out even more."
    world.battle.enemy[0].name "It looked like you liked to let out the liquid too~{image=note}!"
    l "Auuu..."
    "Honey starts to flow out from the flower, making my penis sticky.{w}\nIn addition, each petal of the flower starts to slowly move back and forth."
    "The sticky petals massage and stroke my penis as the flower tightens around me."
    l "Aaa... This flower... Incredible..."
    "This flower evolved to force men to ejaculate for some reason..."
    "The petals wrap around me and gently massage me.{w}\nThe sticky honey covers every inch of my penis as the friction between my penis and the soft petals increases."
    "As the pleasure attacks me, I feel my waist start to go numb.{w}\nSlowly giving in to the pleasure, I feel my pre-come start to leak out."
    l "No!{w} Stop this...!"
    "I somehow manage to hold back the desire to release.{w}\nI can't allow this strange flower to absorb my semen..."
    l "Get it off...{w} Auuuu..."
    "But it's too late.{w}\nThe stimulation that this flower forces on a man won't allow them to resist."
    world.battle.enemy[0].name "It's pointless to resist~{image=note}!{w}\nThere isn't a single man alive that can endure this flower~{image=note}!"
    l "Auuuu!"
    "Even though I know it's useless, my body writhes as I use the last of my strength to hold on."
    "Being forced to feel this good by some strange flower...!"
    world.battle.enemy[0].name "Hehehe... Your penis is twitching.{image=note}{w}\nThe flower wants your white pee~{image=note}!"
    l "Ahhh!!"
    "Despite my best effort, I can't take it any longer.{w}\nAs the pleasure races through my body like electricity, I give in to desire as the orgasm explodes throughout my body."

    with syasei1
    show fairy bk03 at xy(X=270, Y=130) zorder 10 as bk
    show fairy ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    world.battle.enemy[0].name "Hehe.{image=note}{w} The flower is sucking it all up.{image=note}"
    l "Ahhh..."
    "As my semen gushes out into the flower, it slowly absorbs it.{w}\nMy semen is just like fertilizer for this flower..."
    world.battle.enemy[0].name "Haha... Let out more~{image=note}!"
    "The Fairy keeps the flower on my penis as she happily frolics around."
    l "Auuu..."
    "Disgrace fills me as this Fairy toys with me by raping me with a flower."
    "But slowly pushing aside that humiliation is the strange pleasure from the flower."
    "As those two feelings mix in my body, all I can do is pitifully writhe on the forest floor."
    l "Ah... Ahhh!"
    world.battle.enemy[0].name "Hehehe... Isn't it fun?{image=note}{w}\nLet's play even more~{image=note}!"

    play hseanwave "audio/se/hsean21_tentacle2.ogg"

    "The petals around my penis get even tighter.{w}\nFrom the base of my penis up to the tip, the soft petals gently move and stroke me."
    "I'm no longer able to deny the pleasure this Succubus Flower can bring me..."
    l "Ah... I'm coming!"

    with syasei1
    $ world.ejaculation()

    "The flower wrapped around my penis sucks up all the semen as I shoot it out."
    "I'm only feeling this pleasure so that the flower can feed...{w}\nThe Fairy giggles at me as I pant after my orgasm."
    "Trampling on the real purpose for a man's penis, I'm just a toy to her.{w}\nToo weak to escape, I can't do anything but accept this..."
    world.battle.enemy[0].name "Hehehe... Let's play even more~{image=note}!"
    l "Stop already...{w} Ahhh!!"

    with syasei1
    $ world.ejaculation()

    "My penis is only a toy to amuse the Fairy...{w}\nUsing this flower to play with me, it will continue to suck up all of my semen..."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Thus I was made into a toy of the Fairy.{w}\nNow her favorite object, she'll never let me go."
    "Stuck in the Forest of Spirits, I'll be toyed with by the Fairy for the rest of my life..."
    "..............."

    jump badend

label elf_start:
    python:
        world.troops[20] = Troop(world)
        world.troops[20].enemy[0] = Enemy(world)

        world.troops[20].enemy[0].name = Character("Эльфийка")
        world.troops[20].enemy[0].sprite = ["elf st01", "elf st02", "elf st03", "elf h1"]
        world.troops[20].enemy[0].position = xy(0.5, 0.5)
        world.troops[20].enemy[0].defense = 100
        world.troops[20].enemy[0].evasion = 95
        world.troops[20].enemy[0].max_life = world.troops[20].battle.difficulty(700, 800, 800)
        world.troops[20].enemy[0].power = world.troops[20].battle.difficulty(0, 1, 2)

        world.troops[20].battle.tag = "elf"
        world.troops[20].battle.name = world.troops[20].enemy[0].name
        world.troops[20].battle.background = "bg 061"
        world.troops[20].battle.music = 1
        world.troops[20].battle.exp = 1000
        world.troops[20].battle.exit = "lb_0039"

        world.troops[20].battle.init()

    world.battle.enemy[0].name "Я должна защищать этот лес!{w}\nЯ не позволю бандиту, подобному тебе, атаковать фей!"
    world.battle.enemy[0].name "Как ты мог, напасть на невинную фею!"
    l "Она попыталась изнасиловать меня этим цветком!"
    sealed_fairy "Уряяя! Я бабочка!{w}\nКак весело!"
    world.battle.enemy[0].name ".............."
    l ".............."
    world.battle.enemy[0].name "Как бы там ни было... Я не позволю тебе нарушать покой этого леса!"
    l "Ты можешь хоть на пару секунд замолчать и выслушать меня?"
    "Полагаю у меня нет иного выбора."

    $ world.battle.main()

label elf_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind == 2:
            world.battle.power()
        elif world.battle.result == 1 and world.party.player.bind == 1:
            world.battle.miss3()
        elif world.battle.result == 1 and not world.battle.stage[0]:
            world.battle.attack()
        elif world.battle.result == 1 and world.battle.stage[0]:
            renpy.jump("elf002")
        elif world.battle.result == 2 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.party.player.bind:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label elf002:
    $ world.battle.cry(world.party.player.name[0], [
        "Хаа!",
        "Ораа!",
        "Получай!"
    ])

    "Лука атакует!"
    $ world.party.player.mp_regen()
    play sound "se/karaburi.ogg"

    extend "\nНо он не может достать до эльфийки, стоящей на расстоянии!"

    if world.battle.stage[1] != 1:
        l "Гх..."
        "Я не могу попасть по ней, пока она так далеко.{w}\nСмогу ли я сократить дистанцию умением, или мне нужно подождать, пока она подберется ближе?.."
        $ world.battle.stage[1] = 1

    jump elf_a

label elf_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Не только монстры, но и люди теперь тоже вторгаются к нам?!{w}\nНеужели для этого леса не будет больше мира?!"

    play sound "se/syometu.ogg"
    show elf st11 at xy(0.5, 0.6) with crash

    "Эльфийка уменьшается в размерах!"

    $ world.battle.victory(1)

label elf_a:
    if world.party.player.bind == 2:
        $ random = rand().randint(1,2)

        if random == 1:
            call elf_a11
            $ world.battle.main()
        elif random == 2:
            call elf_a12
            $ world.battle.main()

    elif world.party.player.bind == 1:
        call elf_a9
        $ world.battle.main()

    if world.party.player.status == 2:
        while True:
            $ random = rand().randint(1,5)

            if random == 1:
                call elf_a5
                $ world.battle.main()
            elif random == 2:
                call elf_a6
                $ world.battle.main()
            elif random == 3:
                call elf_a7
                $ world.battle.main()
            elif random > 3 and world.party.player.status_turn == 0:
                call elf_a8
                $ world.battle.main()

    if world.battle.stage[0] == 0:
        call elf_a1
        $ world.battle.main()
    elif world.battle.stage[0] == 1:
        call elf_a2
        $ world.battle.main()


label elf_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я не могу выстоять против мечника в ближнем бою!",
        "Вот как сражаются эльфийки!",
        "Бей и беги. Вот основы боя с луком!"
    ])

    play sound "audio/se/step.ogg"

    "Эльфийка отпрыгнула назад и подняла свой лук!"

    $ world.battle.stage[0] = 1
    return


label elf_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Эльфы не любят кровопролития, потому я не оставлю ран!",
        "Не бойся, больно не будет..."
        "Эта стрела не убьет тебя..."
    ])

    $ world.battle.show_skillname("Парализующая стрела")
    play sound "audio/se/yumi.ogg"

    "Эльфийка спустила стрелу с тетивы!{w}\nОна летит быстрее, чем смог бы увернуться Лука, и попадает ему точно в грудь!"

    if not world.battle.first[0]:
        l "Ух-х...{w} Что?.."
        "Хотя меня и не ранило, моё тело мгновенно застыло.{w}{nw}"

    $ world.party.player.status = 2
    $ world.party.player.status_print()

    extend "{w=.7}\nЛука парализован!"

    $ world.battle.hide_skillname

    if not world.battle.first[0]:
        l "Ух-х!.."
        world.battle.enemy[0].name "Теперь, ты не сможешь дать отпор...{w}\nПриготовься, я заставлю тебя заплатить за твои преступления."

        $ world.battle.first[0] = True

    "Эльфийка быстро подошла ближе к Луке!"

    if persistent.difficulty == 1:
        $ world.party.player.status_turn = rand().randint(2,3)
    elif persistent.difficulty == 2:
        $ world.party.player.status_turn = rand().randint(2,4)
    elif persistent.difficulty == 3:
        $ world.party.player.status_turn = rand().randint(2,5)

    $ world.battle.stage[0] = 0
    return


label elf_a5:
    $ world.battle.skillcount(1, 3155)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Как тебе нравятся ощущения, от стимуляции моей рукой?",
        "Ха-ха... приятно?",
        "Я помассирую тебя рукой..."
    ])

    $ world.battle.show_skillname("Эльфийская стимуляция рукой")
    play sound "audio/se/ero_sigoki1.ogg"

    "Эльфийка взяла член Луки в руку и нежно ласкает его!"

    python:
        world.battle.enemy[0].damage = {1: (13, 16), 2: (16, 20), 3: (23, 28)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label elf_a6:
    $ world.battle.skillcount(1, 3156)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "*Сосёт* *Лижет*",
        "Выплесни это в мой рот...",
        "Я высосу всё, что у тебя есть..."
    ])

    $ world.battle.show_skillname("Эльфийский минет")
    play sound "audio/se/ero_buchu3.ogg"

    "Эльфийка нежно сосёт член Луки!"

    python:
        world.battle.enemy[0].damage = {1: (16, 19), 2: (21, 24), 3: (29, 34)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label elf_a7:
    $ world.battle.skillcount(1, 3157)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Выплесни это на мою грудь...",
        "Разве это не самая любимая мужчинами часть тела?..",
        "Я выдавлю жидкость твоей сдачи своей грудью..."
    ])

    $ world.battle.show_skillname("Эльфийская стимуляция грудью")
    play sound "audio/se/ero_koki1.ogg"

    "Эльфийка вкладывает член Луки между своими грудями, сдавливая их!"

    python:
        world.battle.enemy[0].damage = {1: (19, 22), 2: (25, 29), 3: (35, 40)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label elf_a8:
    world.battle.enemy[0].name "А теперь, я изнасилую тебя..."

    $ world.battle.show_skillname("Оседлание")
    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Эльфийка уселась верхом на Луку!"

    $ world.battle.hide_skillname()

    if world.party.player.surrendered or world.battle.first[1]:
        return

    world.battle.enemy[0].name "Лежи смирно, пока я вставлю тебя внутрь...{w}\nРазве тебе не интересно, какие ощущения принесёт тебе невероятное влагалище Эльфийки?"
    world.battle.enemy[0].name "Я удовлетворю тебя тем, перед чем не устоит ни один мужчина...{w}\nИнтересно, сколько секунд ты продержишься?"

    $ world.battle.first[1] = 1
    return


label elf_a9:
    "Эльфийка сидит верхом на Луке!"
    world.battle.enemy[0].name "Ха-ха... Я ввожу его внутрь.{w}\nДавай посмотрим, как долго ты продержишься..."

    $ world.party.player.bind = 2
    $ world.battle.show_skillname("Насилующая эльфийка")
    $ world.battle.enemy[0].sprite[3] = "elf h2"
    show elf h2
    play sound "audio/se/ero_pyu5.ogg"

    "Эльфийка с силой опускает свои бёдра, втискивая член Луки глубоко внутрь себя!"

    $ world.battle.enemy[0].damage = (40, 45)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    "Эльфийка насилует Луку!"
    l "Aх-х-х!!!"
    "Её узкое влагалище сжимается вокруг моего члена, как только я вхожу в неё."
    "Как будто стенки её влагалища состоят из мягкого желе, они сжимаются и массируют каждый дюйм моего члена!"
    world.battle.enemy[0].name "Каково это, быть внутри меня?{w}\nНепохоже, что ты сможешь вынести это..."

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h4")

    $ mogaku_anno1 = "But Luka is forcibly held down!"
    $ mogaku_anno3 = "But Luka is forcibly held down!"
    $ mogaku_sel1 = "Pointless... You have no choice but to come in me."
    $ mogaku_sel2 = "Struggling while panting in pleasure..."
    $ mogaku_sel3 = "Struggling while being raped... How pitiful."
    $ mogaku_sel4 = "It's pointless to resist... Your fate was sealed as soon as I forced you into me."
    $ mogaku_sel5 = "Every man is left helpless when I rape them..."
    return


label elf_a11:
    $ world.battle.skillcount(1, 3158)

    "Эльфийка насилует Луку!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ха-ха-ха... ну как? Ты сможешь вытерпеть это?",
        "Это приятно, не правда ли?",
        "Хе-хе... ни один мужчина не в силах сопротивляться этому.",
        "Я заставлю тебя кончить!",
        "Я заставлю тебя выплеснуть всё, до последней капли..."
    ])

    $ world.battle.show_skillname("Эльфийское владение бёдрами")
    play sound "audio/se/ero_chupa3.ogg"

    $ world.battle.cry(narrator, [
        "Эльфийка трётся своими бёдрами о Луку!",
        "Эльфийка покручивает своими бёдрами!",
        "Эльфийка очень быстро двигает своими бёдрами, вверх и вниз!",
        "Эльфийка яростно двигает своими бёдрами!",
        "Эльфийка быстро двигает нижней частью своего тела!"
    ], True)

    python:
        world.battle.enemy[0].damage = (40, 45)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h5")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label elf_a12:
    $ world.battle.skillcount(1, 3159)

    "Эльфийка насилует Луку!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Haha... I can make it tighter...",
        "Does it feel good inside of me?",
        "Haha... What will you do when I do this?",
        "The tip is your weak point, isn't it?",
        "Haha... You won't last at this rate."
    ])

    $ world.battle.show_skillname("Elf Tightening")
    play sound "audio/se/ero_chupa5.ogg"

    $ world.battle.cry(narrator, [
        "The Elf's elastic pussy tightens around Luka!",
        "An overpowering pleasure is forced into Luka as the Elf tightens her pussy!",
        "The Elf's pussy contracts in waves, as if milking Luka's penis!",
        "The deepest part of the Elf's vagina tightens around the tip of Luka's penis!",
        "The Elf's vagina tightens around Luka!"
    ], True)

    python:
        world.battle.enemy[0].damage = (40, 45)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h6")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label elf_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Ты сдаёшься.{w}\nРаз так, я накажу тебя за твоё преступление."

    $ world.battle.enemy[0].attack()

label elf_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Elf Handjob"
    $ list2 = "Elf Blowjob"
    $ list3 = "Elf Tit Fuck"
    $ list4 = "Elf Waist Shake"
    $ list5 = "Elf Tightening"

    if persistent.skills[3155][0]:
        $ list1_unlock = 1

    if persistent.skills[3156][0]:
        $ list2_unlock = 1

    if persistent.skills[3157][0]:
        $ list3_unlock = 1

    if persistent.skills[3158][0]:
        $ list4_unlock = 1

    if persistent.skills[3159][0]:
        $ list5_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump elf_onedari1
    elif world.battle.result == 2:
        jump elf_onedari2
    elif world.battle.result == 3:
        jump elf_onedari3
    elif world.battle.result == 4:
        jump elf_onedari4
    elif world.battle.result == 5:
        jump elf_onedari5
    elif world.battle.result == -1:
        jump elf_main


label elf_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want to be punished by my hand?{w}\nAlright, I'll punish you like that..."

    while True:
        call elf_a5


label elf_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want to be punished by my mouth?{w}\nAlright, I'll punish you like that..."

    while True:
        call elf_a6


label elf_onedari3:
    call onedari_syori

    world.battle.enemy[0].name "You want to be punished by my breasts?{w}\nAlright, I'll punish you like that..."

    while True:
        call elf_a7


label elf_onedari4:
    call onedari_syori_k

    world.battle.enemy[0].name "You want me to rape you?{w}\nAlright, I'll shake my waist until you can't take it anymore..."

    if not world.party.player.bind:
        $ monster_pos = center
        $ world.party.player.bind = 1
        $ world.party.player.status_print()

        "The Elf mounts Luka!"

    if world.party.player.bind == 1:
        call elf_a9

    while True:
        call elf_a11


label elf_onedari5:
    call onedari_syori_k

    world.battle.enemy[0].name "You want me to rape you?{w}\nAlright, I'll tighten around you until you can't take it anymore..."

    if not world.party.player.bind:
        $ monster_pos = center
        $ world.party.player.bind = 1
        $ world.party.player.status_print()

        "The Elf mounts Luka!"

    if world.party.player.bind == 1:
        call elf_a9

    while True:
        call elf_a12


label elf_h1:
    $ world.party.player.moans(1)
    with syasei1
    show elf bk03 at xy(X=100, Y=-100) zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the pleasure from her hand, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([5, 9])
    $ bad1 = "Luka succumbed to the Elf's handjob."
    $ bad2 = "Robbed of free will by her pussy, Luka spent the rest of his life as the Elf's pet."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Haha... You covered my hand with your semen...{w}\nSince you disrupted the peace of this forest, you won't be let off with just that..."

    jump elf_h


label elf_h2:
    $ world.party.player.moans(1)
    with syasei1
    show elf bk01 at xy(X=100, Y=-100) zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the pleasure from her mouth, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([4, 9])
    $ bad1 = "Luka succumbed to the Elf's blowjob."
    $ bad2 = "Robbed of free will by her pussy, Luka spent the rest of his life as the Elf's pet."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Haha... You filled my mouth with your semen...{w}\nSince you disrupted the peace of this forest, you won't be let off with just that..."

    jump elf_h


label elf_h3:
    $ world.party.player.moans(1)
    with syasei1
    show elf bk02 at xy(X=100, Y=-100) zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the pleasure from her breasts, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([7, 9])
    $ bad1 = "Luka succumbed to the Elf's titty fuck."
    $ bad2 = "Robbed of free will by her pussy, Luka spent the rest of his life as the Elf's pet."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Haha... You covered my chest with your semen...{w}\nSince you disrupted the peace of this forest, you won't be let off with just that..."

    jump elf_h


label elf_h4:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Eh?{w} Already?"

    with syasei1
    show elf h3
    $ world.ejaculation()

    "As soon as Luka is forced into the Elf, he accidentally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([1, 9])
    $ persistent.count_bouhatu += 1
    $ bad1 = "As soon as Luka was forced into the Elf, he accidentally ejaculated."
    $ bad2 = "Robbed of free will by her pussy, Luka spent the rest of his life as the Elf's pet."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Coming just from insertion...{w} How pathetic!"
    world.battle.enemy[0].name "Since you disrupted the peace of this forest, you won't be let off with just that..."
    "Even after I was defeated, the Elf continues to insult me..."

    jump elf_h


label elf_h5:
    $ world.party.player.moans(2)
    with syasei1
    show elf h3
    $ world.ejaculation()

    "Unable to endure the shaking of her amazing pussy, Luka ejaculates."

    $ world.battle.lose()

    $ world.battle.count([1, 9])
    $ bad1 = "Raped by the Elf, Luka succumbed to her shaking waist."
    $ bad2 = "Robbed of free will by her pussy, Luka spent the rest of his life as the Elf's pet."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Haha... You let it all out inside of me.{w}\nSince you disrupted the peace of this forest, you won't be let off with just that..."
    "Even after I was defeated, the Elf continues to insult me..."

    jump elf_h


label elf_h6:
    $ world.party.player.moans(2)
    with syasei1
    show elf h3
    $ world.ejaculation()

    "Unable to endure the intense tightness of the Elf's amazing pussy, Luka ejaculates."

    $ world.battle.lose()

    $ world.battle.count([1, 9])
    $ bad1 = "Raped by the Elf, Luka succumbed to her tightness."
    $ bad2 = "Robbed of free will by her pussy, Luka spent the rest of his life as the Elf's pet."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Haha... You let it all out inside of me.{w}\nSince you disrupted the peace of this forest, you won't be let off with just that..."
    "Even after I was defeated, the Elf continues to insult me..."

    jump elf_h


label elf_h:
    $ monster_y = -100

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.party.player.bind:
        play hseanwave "audio/se/hsean02_innerworks_a2.ogg"
        "Warm, sticky liquid starts to flow out from inside of her."
    else:
        l "Uuuu..."

        hide bk
        show elf h1 at center


        "Too weak to resist her, the Elf throws me to the ground.{w}\nSitting on top of me, she puts the entrance of her vagina right next to my penis."
        world.battle.enemy[0].name "I'm going to violate you in a few seconds...{w}\nTo make sure you never return, I'll force you to go crazy."
        l "Ahh...{w} Stop! Stop!"
        world.battle.enemy[0].name "Haha... Raping a man who resists makes me a little happy."

        play hseanwave "audio/se/hsean02_innerworks_a2.ogg"
        show elf h2

        "Saying that, the Elf forces me all the way inside her!{w}\nAs soon as I'm inside her, I'm instantly attacked by the feeling of her soft walls!"
        "Just like being forced into jelly, her soft walls tighten around me!{w}\nWarm, sticky liquid starts to flow out from inside of her."

    l "Auuu!!"
    world.battle.enemy[0].name "Haha... A female elf's genitals are unmatched."
    "Letting out a sadistic laugh, the Elf looks down at me.{w}{nw}"

    play hseanwave "audio/se/hsean08_innerworks_a8.ogg"

    extend "\nAnd starts to grind her waist back and forth!"
    l "Ahhh!!"
    "Erotic sounds fill the clearing of the forest as I'm forced in and out of her."
    world.battle.enemy[0].name "Hehe... I just moved a little bit and you're already moaning that loudly?"
    "While bouncing her waist up and down, she looks at me with a triumphant face."
    "Underneath her, I bite my lip as I try to endure the pleasure.{w}\nMercilessly raping me, I have no way to stop her...{w}\nAll I can do is endure..."
    world.battle.enemy[0].name "Hehe... Are you humiliated?{w}\nIt's pointless to try to endure... There's no escape."
    "The Elf speeds up her pumping as she states that.{w}\nStrangled by her amazing pussy, I finally break down and give in."
    l "Haaaaaa!{w}\nI'm going to come!"
    world.battle.enemy[0].name "So pathetic! Let it out you wimp!{w}\nI'll squeeze out every drop of your defeat!"
    "As if doing a killing move, the Elf twists her hips around in a circle.{w}\nAs the obscene noise echoes through the forest, my vision goes white."
    l "Ahhh!!"

    with syasei1
    show elf h3 at center
    $ world.ejaculation()

    "I explode inside of the Elf, as if shooting out a white flag of surrender."
    l "Auuu..."
    "I tremble in pleasure as I ejaculate.{w}\nIn the end, I wasn't able to endure..."
    world.battle.enemy[0].name "Hehe... Was it good?"
    "The Elf looks down at me with a sadistic smile as I bask in the feelings of the humiliating orgasm."
    "While being forced to look at her triumphant face, my ejaculation ends."
    world.battle.enemy[0].name "How pathetic.{w} Raped by a female as you come helplessly.{w}\nEven though you're a man, you don't even try to fight back!"
    "As the Elf laughs at me, she starts to move her waist again.{w}\nNow moving around in a circle, the stimulation is twice as powerful as moments ago."
    l "Ahhh!!"
    world.battle.enemy[0].name "Is it really that good?{w}\nHahaha... Crying out in pleasure while being raped! How pathetic!"
    l "Auuu..."
    "Even if I try to endure this, her movements are too intense.{w}\nToo weak to move, I can't do anything but be flooded by the pleasure."
    world.battle.enemy[0].name "Hehe... So miserable."
    "My penis is being mercilessly tortured by pleasure inside of her.{w}\nWhenever she stops moving her waist, she tightens around my penis instead."
    "As her jelly-like walls close tighter around my penis, I can feel every inch of her soft walls."
    l "Faaaaa..."
    world.battle.enemy[0].name "It looks like you're going to come again."
    "While squeezing me even tighter, the Elf starts to make small movements."
    "Squeezed tightly by her pussy, the small movements force pleasure into every inch of my penis."
    world.battle.enemy[0].name "You look like you're about to come...{w}\nSuch a shameless Hero!"
    "The Elf starts to rhythmically contract her pussy.{w}\nAs if milking me, she starts from the entrance of her pussy and moves all the way to the deepest point."
    "Instantly cornered by this change in stimulation, I'm forced over the edge."
    l "Auu... I'm coming again!"
    world.battle.enemy[0].name "Then I'll finish you...{w}\nNow... Come!"
    "Finishing me off just like she said, she clamps down around every inch of my penis, squeezing me with all her power."
    "Squeezed tighter than ever before, a powerful orgasm blows through my body."

    with syasei1
    show elf h4
    $ world.ejaculation()

    l "Faaa!"
    "Flooded in ecstasy, I've finally surrendered my body and mind to the Elf."
    "Broken by the pleasure, I can't think of anything but wanting to feel more of her..."
    world.battle.enemy[0].name "Oh? Did you give in?{w}\nAre you already a slave to my pussy?"
    "The Elf shakes her waist as a triumphant smile flashes on her face."
    l "Ahhh!"
    "I tremble in joy at the merciless pleasure.{w}\nRejoicing inside of her pussy, I accept my new role."
    world.battle.enemy[0].name "It can't be helped, I guess.{w}\nIf you've become like this, I have no choice but to take care of you."
    world.battle.enemy[0].name "I'll keep you in this forest for the rest of your life as my pet."
    l "Auu... Incredible...{w}\nI'm coming again!"

    with syasei1
    show elf h5
    $ world.ejaculation()

    "I come again quickly after my last orgasm.{w}\nAccepting the pleasure of this humiliation, I immerse myself in the sensations."
    world.battle.enemy[0].name "You aren't allowed to rest yet.{w}\nYou don't have your own will anymore... You're just my pet."
    l "Ahhhh... Too good..."

    with syasei1
    $ world.ejaculation()
    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Robbed of my free will by her amazing pussy, it's not possible for me to return."
    "Unable to leave the forest, I spend every day at the Elf's side.{w}\nBut since she violates me every day, I'm happy..."
    "..................."

    jump badend

label tfairy_start:
    python:
        world.troops[21] = Troop(world)
        world.troops[21].enemy[0] = Enemy(world)

        world.troops[21].enemy[0].name = Character("Феи-близняшки")
        world.troops[21].enemy[0].sprite = ["tfairy st01", "tfairy st02", "tfairy st03", "tfairy st02"]
        world.troops[21].enemy[0].position = xy(0.5, 0.5)
        world.troops[21].enemy[0].defense = 100
        world.troops[21].enemy[0].evasion = 80
        world.troops[21].enemy[0].max_life = world.troops[21].battle.difficulty(700, 800, 900)
        world.troops[21].enemy[0].power = world.troops[21].battle.difficulty(0, 1, 2)

        world.troops[21].battle.tag = "tfairy"
        world.troops[21].battle.name = world.troops[21].enemy[0].name
        world.troops[21].battle.background = "bg 061"
        world.troops[21].battle.music = 6
        world.troops[21].battle.plural_name = True
        world.troops[21].battle.exp = 1000
        world.troops[21].battle.exit = "lb_0040"

        world.troops[21].battle.init()

    fairy_a "Уа!{w} Человек!"
    fairy_b "Человек!"

    show tfairy st01 at xy(0.7)
    show elf st11 at xy(0.3, 0.6)

    elf "Эй, вы, глупышки!{w} Я же сказала вам не показываться!{w}\nВокруг бродит ужасный монстр!{w} Вы же не хотите быть съеденными, так ведь?!"
    fairy_a "О?{w}\nСтаршая сестричка стала такой маленькой!"
    fairy_b "Крохотулькой-крохотулечкой!"
    elf "Это не важно!{w}\nБыстрее, возвращайтесь домой!"
    fairy_a "Эй...{w}\nПочему бы тебе не подойти на шажок?"
    fairy_b "Да, почему бы тебе не попробовать?"
    elf "Э?.."

    show elf st11:
        anchor (0.5, 0.5)
        linear 0.25 xpos 0.4
    pause 0.5

    "Эльфийка сделала шаг вперед."

    play sound "se/fall2.ogg"
    show elf st11:
        anchor (0.5, 0.5)
        linear 0.6 ypos 1.0
    pause 1.0
    hide elf

    "И внезапно, она проваливается в маленькую яму!"
    elf "Ай!{w} Что это такое?!"

    show tfairy st02

    fairy_a "Она упала~~{image=note}!"
    fairy_b "Плюхнулась~~{image=note}!"
    elf "Выпустите меня отсюда!{w} Я не прощу вас обеих за это!"
    "Будучи теперь маленькой и бессильной, эльфийка застряла в яме в земле."

    play sound "se/lvup.ogg"

    "[world.battle.enemy[0].name] победили свою старшую сестру!"
    "... Ой-ёй.{w}\nЭти девочки довольно жестоки..."
    fairy_a "Человек~~{image=note}!"
    fairy_b "Человек~~{image=note}!"
    "Непослушные маленькие феи перевели взгляд на меня.{w}\nКаждая их них сжимает по Цветку Суккуба, такому же, как и у предыдущей феи..."

    play music "bgm/battle.ogg"
    show tfairy st01 at xy(0.5, 0.5)

    fairy_a "Давай поиграем!{w} Посмотрим, кто высосет больше белой штуки~{image=note}!"
    fairy_b "Играть-играть~{image=note}!"
    l "Гх..."
    "Похоже, что мне придется воспитывать этих вредных фей..."

    $ world.battle.main()

label tfairy_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label tfairy_v:
    $ world.battle.face(3)

    fairy_a "Ой!"
    fairy_b "Ай!"

    play sound "se/syometu.ogg"
    hide tfairy with crash

    "Феи-близняшки превратились в двух бабочек!"

    $ world.battle.victory(1)

label tfairy_a:
    if world.party.player.bind == 1:
        $ random = rand().randint(1,3)

        if random == 1:
            call tfairy_a6
            $ world.battle.main()
        elif random == 2:
            call tfairy_a7
            $ world.battle.main()
        elif random == 3:
            call tfairy_a8
            $ world.battle.main()

    while True:
        $ random = rand().randint(1,3)

        if random == 1:
            call tfairy_a1
            $ world.battle.main()
        elif random == 2:
            call tfairy_a2
            $ world.battle.main()
        elif random == 3 and world.battle.delay[0] > 2:
            call tfairy_a3
            $ world.battle.main()


label tfairy_a1:
    $ world.battle.skillcount(1, 3160)

    $ world.battle.cry(fairy_a, [
        "Этот цветочек очень хорошо~{image=note}!",
        "Отдай моему цветочку свою белую штуку~{image=note}!",
        "Атака цветком!"
    ])

    $ world.battle.show_skillname("Суккубий цветок")
    show tfairy ct01 at topleft zorder 15 as ct
    show tfairy ct02 at topleft zorder 15 as ct
    play sound "audio/se/ero_buchu1.ogg"

    "Фея А насильно засовывает член Луки в цветок!{w}\nЛепестки цветка обхватывают головку члена Луки!"

    python:
        world.battle.enemy[0].damage = {1: (15, 20), 2: (18, 23), 3: (26, 33)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

    $ world.battle.cry(fairy_b, [
        "Нет, мой лучше~{image=note}!",
        "Полей и мой цветочек тоже~{image=note}!",
        "Атакую!"
    ], True)

    $ world.battle.show_skillname("Суккубий цветок")
    show tfairy ct11 at topleft zorder 15 as ct
    show tfairy ct12 at topleft zorder 15 as ct
    play sound "audio/se/ero_buchu1.ogg"

    "Fairy B forces the flower onto Luka's penis!{w}\nThe sticky honey covers Luka as the petals stroke him!"

    python:
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

    hide ct

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    return


label tfairy_a2:
    $ world.battle.skillcount(1, 3161)

    $ world.battle.cry(fairy_a, [
        "Хе-хе-хе... щекочу-щекочу~{image=note}!",
        "Я хочу увидеть белую штуку~{image=note}!",
        "Мягко-мягко!"
    ])
    $ world.battle.cry(fairy_b, [
        "Щекотка~{image=note}!",
        "Белую штуку из твоего пениса~{image=note}!",
        "Щекочу-щекочу!"
    ])

    $ world.battle.show_skillname("Щекотка двумя крыльями")
    play sound "audio/se/umaru.ogg"

    "Феи-близняшки кружат вокруг члена Луки, щекоча его своими крылышками!"

    python:
        world.battle.enemy[0].damage = {1: (10, 15), 2: (13, 18), 3: (18, 26)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label tfairy_a3:
    $ world.battle.face(2)

    if world.battle.first[0] == 1:
        fairy_a "Аха-ха-ха... снова поймала тебя~{image=note}!"
        fairy_b "Попался снова~{image=note}!"
    else:
        fairy_a "Ха-ха-ха... поймала~{image=note}!"
        fairy_b "Попался~{image=note}!"

    $ world.battle.show_skillname("Зацеп")

    "Феи-близняшки крепко-накрепко цепляются за член Луки!{w}{nw}"

    show tfairy ct21 at topright zorder 15 as ct

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    extend "{w=.7}\nФеи-близняшки держатся за член Луки!"

    $ world.battle.hide_skillname()

    if world.party.player.surrendered:
        return

    if not world.battle.first[0]:
        l "Гх... отстаньте!"
        "Я не могу их от себя отцепить... и я не могу размахивать своим мечом в таком положении..."

    $ world.battle.face(2)

    if world.battle.first[0] == 1:
        fairy_a "Больше веселья~{image=note}!"
    else:
        fairy_a "Мы будем с тобой играть, пока ты не кончишь~{image=note}!"

    fairy_b "Хочу увидеть эту липкую штуковину~{image=note}!"

    $ world.battle.first[0] = 1

    if persistent.difficulty == 1:
        $ world.battle.enemy[0].power = 1
    elif persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 2
    elif persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return


label tfairy_a6:
    $ world.battle.skillcount(1, 3162)

    "Феи-близняшки крепко держатся за член Луки..."

    $ world.battle.cry(fairy_a, [
        "Кончик... *хлоп* *хлоп*",
        "Кожа тут такая тянущаяся~{image=note}!",
        "Я засуну язык в эту дырочку~{image=note}!",
        "Похоже на грибочек~{image=note}!"
    ])
    $ world.battle.cry(fairy_b, [
        "Верхушка такая мягонькая~{image=note}!",
        "Тянучка~{image=note}!",
        "Я тоже~{image=note}!",
        "Членогриб~{image=note}!"
    ], True)

    $ world.battle.show_skillname("Розыгрыш фей-близняшек")
    play sound "audio/se/ero_koki1.ogg"

    $ world.battle.cry(narrator, [
        "Феи-близняшки тыкают и мнут головку члена члена Луки!",
        "Феи-близняшки тянут и играют с крайней плотью члена Луки!",
        "Одна из фей открывает уретру, тогда как вторая проскальзывает туда языком!",
        "Феи-близняшки тянутся к головке члена Луки и мнут её!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (10, 15), 2: (13, 18), 3: (18, 26)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label tfairy_a7:
    $ world.battle.skillcount(1, 3163)

    "Феи-близняшки крепко держатся за член Луки..."

    $ world.battle.cry(fairy_a, [
        "Я обниму его~{image=note}!",
        "Обнимашки-обнимашки~{image=note}!",
        "Член... руки... нападаю~{image=note}!",
        "Я выжму из него эту белую штуку~{image=note}!"
    ])
    $ world.battle.cry(fairy_a, [
        "А я сожму~{image=note}!"
        "Обнимашки-обнимашки~{image=note}!",
        "Атакуем пенис~{image=note}!",
        "Выплесни её~{image=note}!"
    ], True)

    $ world.battle.show_skillname("Объятия фей-близняшек")
    play sound "audio/se/ero_koki1.ogg"

    "Феи-близняшки туго сжимают член Луки!"

    python:
        world.battle.enemy[0].damage = {1: (15, 20), 2: (17, 23), 3: (25, 33)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h5")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label tfairy_a8:
    $ world.battle.skillcount(1, 3164)

    "Феи-близняшки крепко держатся за член Луки..."

    $ world.battle.cry(fairy_a, [
        "Давай танцевать~{image=note}!",
        "Выстрельни уже~{image=note}!",
        "Издеваемся над кончиком~{image=note}!",
        "Эхе-хе-хе... как тебе?"
    ])
    $ world.battle.cry(fairy_a, [
        "Танцевать~{image=note}!",
        "Выстрельни~{image=note}!",
        "Издеваемся~{image=note}!",
        "Хе-хе... он уже потёк?"
    ], True)

    $ world.battle.show_skillname("Танец фей-близняшек")
    play sound "audio/se/ero_koki1.ogg"

    $ world.battle.cry(narrator, [
        "Феи-близняшки щекотят каждый сантиметр члена Луки своими крылышками!",
        "Феи-близняшки крутятся на члене Луки, удерживаясь на нём!",
        "Феи-близняшки трут головку члена Луки своими ручками и ножками!",
        "Феи-близняшки ползают по члену Луки, попутно сжимая его!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (20, 25), 2: (27, 34), 3: (37, 47)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h6")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label tfairy_mogaku:
    $ random = rand().randint(1,3)

    if world.battle.result == 1 and random == 1:
        l "Uhg!"
    elif world.battle.result == 1 and random == 2:
        l "Guh... Damn..."
    elif world.battle.result == 1 and random == 3:
        l "Stop!"
    elif world.battle.result == 2 and random == 1:
        l "Guh!"
    elif world.battle.result == 2 and random == 2:
        l "Let go!"
    elif world.battle.result == 2 and random == 3:
        l "Grr... Stop!"

    if world.battle.result == 1:
        $ world.party.player.mp_regen()

        "Luka attacks!"
        "But Luka hesitates at swinging his sword!"
    elif world.battle.result == 2:
        $ world.battle.enemy[0].power -= 1

        if world.battle.enemy[0].power < 0:
            $ world.battle.enemy[0].power = 0

        "Luka tries to peel the Twin Fairies off!"
        "But Luka is unable to peel them off!"


    $ random = rand().randint(1,5)

    if random == 1:
        fairy_a "More pranks~{image=note}!"
        fairy_b "Keep playing until you leak~{image=note}!"
    elif random == 2:
        fairy_a "I'll never let gooo~{image=note}!"
        fairy_b "Neeever~{image=note}!"
    elif random == 3:
        fairy_a "Whee! He's moving around so much~{image=note}!"
        fairy_b "Wheee~{image=note}!"
    elif random == 4:
        fairy_a "More playing~{image=note}!"
        fairy_b "Play~{image=note}!"
    elif random == 5:
        fairy_a "No! No! More pranks!"
        fairy_b "More!"

    jump tfairy_a


label tfairy_mogaku2:
    if persistent.difficulty == 3:
        $ random = rand().randint(1,2)

        if random == 2:
            jump tfairy_mogaku

    l "You two...!"

    play sound "audio/se/dassyutu.ogg"
    $ world.party.player.bind = 0
    $ world.party.player.status_print()

    "Luka managed to peel the Twin Fairies off of him!"

    fairy_a "Aww... I wanted to play more!"
    fairy_b "Meanie!"

    $ world.battle.face(1)
    $ world.battle.main()

label tfairy_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    fairy_a "Ура! Он хочет, чтобы мы с ним поиграли"
    fairy_b "Играть-играть!"

    $ world.battle.enemy[0].attack()

label tfairy_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Succubus Flower"
    $ list2 = "Twin Wing Tickle"
    $ list3 = "Twin Fairy Pranks"
    $ list4 = "Twin Fairy Hug"
    $ list5 = "Twin Fairy Dance"

    if persistent.skills[3160][0]:
        $ list1_unlock = 1

    if persistent.skills[3161][0]:
        $ list2_unlock = 1

    if persistent.skills[3162][0]:
        $ list3_unlock = 1

    if persistent.skills[3163][0]:
        $ list4_unlock = 1

    if persistent.skills[3164][0]:
        $ list5_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump tfairy_onedari1
    elif world.battle.result == 2:
        jump tfairy_onedari2
    elif world.battle.result == 3:
        jump tfairy_onedari3
    elif world.battle.result == 4:
        jump tfairy_onedari4
    elif world.battle.result == 5:
        jump tfairy_onedari5
    elif world.battle.result == -1:
        jump tfairy_main


label tfairy_onedari1:
    call onedari_syori

    fairy_a "You want your white stuff to be absorbed by this flower?"
    fairy_b "Hehehe... Give a lot to the flower~{image=note}!"

    while True:
        call tfairy_a1


label tfairy_onedari2:
    call onedari_syori

    fairy_a "You want to be pranked by our wings?"
    fairy_b "Hehe, we'll play lots of tricks~{image=note}!"

    while True:
        call tfairy_a2


label tfairy_onedari3:
    call onedari_syori_k

    fairy_a "You want us to play pranks on your penis?{image=note}"
    fairy_b "Pranks~{image=note}!"

    if not world.party.player.bind:
        call tfairy_a3

    while True:
        call tfairy_a6


label tfairy_onedari4:
    call onedari_syori_k

    fairy_a "You want us to hug your penis?"
    fairy_b "I'll hug it tighter than you can~{image=note}!"

    if not world.party.player.bind:
        call tfairy_a3

    while True:
        call tfairy_a7


label tfairy_onedari5:
    call onedari_syori_k

    fairy_a "You want us to rub our bodies all over your penis? Hehehe.{image=note}"
    fairy_b "I can move faster than yoouu~{image=note}!"

    if not world.party.player.bind:
        call tfairy_a3

    while True:
        call tfairy_a8


label tfairy_s1:
    $ random = rand().randint(1,3)

    if random == 1:
        l "Guh..."
    elif random == 2:
        l "Uuu..."
    elif random == 3:
        l "Uhg..."

    $ world.battle.face(2)

    fairy_a "Is it coming?!"
    fairy_b "Is it?!"

    $ world.battle.face(1)
    $ world.party.player.hphalf = True
    return


label tfairy_s2:
    $ random = rand().randint(1,3)

    if random == 1:
        l "Auu..."
    elif random == 2:
        l "Aaa..."
    elif random == 3:
        l "Faa..."

    $ world.battle.face(2)

    fairy_a "Shoot it out~{image=note}!"
    fairy_b "Shoot it~{image=note}!"

    $ world.party.player.kiki = True
    return


label tfairy_h1:
    $ world.party.player.moans(1)
    with syasei1
    show tfairy ct03 at topleft zorder 15 as ct
    show tfairy bk04 at xy(X=220, Y=130) zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the stimulation from the Succubus Flower, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([12, 5])
    $ bad1 = "Luka succumbed to the Twin Fairies Succubus Flowers."
    $ bad2 = "Caught by the Twin Fairies, they toyed with him for the rest of his life."
    $ world.battle.face(2)

    fairy_a "Haha, I won~{image=note}!{w}\nHe put it all in my flower~{image=note}!"
    fairy_b "Awww!"

    jump tfairy_h


label tfairy_h2:
    $ world.party.player.moans(1)
    with syasei1
    show tfairy ct13 at topleft zorder 15 as ct
    show tfairy bk05 at xy(X=220, Y=130) zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the stimulation from the Succubus Flower, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([12, 5])
    $ bad1 = "Luka succumbed to the Twin Fairies Succubus Flowers."
    $ bad2 = "Caught by the Twin Fairies, they toyed with him for the rest of his life."
    $ world.battle.face(2)

    fairy_a "Aww! Why did you put it in her flower?!"
    fairy_b "Hehehe... My flower is better than yours~{image=note}!"

    jump tfairy_h


label tfairy_h3:
    $ world.party.player.moans(1)
    with syasei1
    show tfairy bk03 at xy(X=220, Y=130) zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the tickling sensation from the Twin Fairies wings, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([13, 5])
    $ bad1 = "Luka succumbed to the Twin Fairies wing tickling."
    $ bad2 = "Caught by the Twin Fairies, they toyed with him for the rest of his life."
    $ world.battle.face(2)

    fairy_a "Hahah! It shot out~{image=note}!"
    fairy_b "Yay! We defeated the penis~{image=note}!"

    jump tfairy_h


label tfairy_h4:
    $ world.party.player.moans(1)
    with syasei1
    show tfairy ct22 at topright zorder 15 as ct
    show tfairy bk01 at xy(X=220, Y=130) zorder 10 as bk
    show tfairy bk02 at xy(X=220, Y=130) zorder 10 as bk2
    $ world.ejaculation()

    "Unable to endure the Twin Fairies playing with his penis, Luka finally ejaculates, covering their tiny bodies in semen."

    $ world.battle.lose()

    $ world.battle.count([24, 5])
    $ bad1 = "Luka succumbed to the Twin Fairies pranks as they clung to his penis."
    $ bad2 = "Caught by the Twin Fairies, they toyed with him for the rest of his life."
    show tfairy ct23 at topright zorder 15 as ct


    fairy_a "Hahah! It shot out~{image=note}!"
    fairy_b "Yay! We defeated the penis~{image=note}!"

    jump tfairy_h


label tfairy_h5:
    $ world.party.player.moans(1)
    with syasei1
    show tfairy ct22 at topright zorder 15 as ct
    show tfairy bk01 at xy(X=220, Y=130) zorder 10 as bk
    show tfairy bk02 at xy(X=220, Y=130) zorder 10 as bk2
    $ world.ejaculation()

    "Unable to endure the Twin Fairies hugging his penis, Luka finally ejaculates, covering their tiny bodies in semen."

    $ world.battle.lose()

    $ world.battle.count([24, 5])
    $ bad1 = "Luka succumbed to the Twin Fairies hugs as they clung to his penis."
    $ bad2 = "Caught by the Twin Fairies, they toyed with him for the rest of his life."
    show tfairy ct23 at topright zorder 15 as ct


    fairy_a "Hahah! It shot out~{image=note}!"
    fairy_b "Yay! We defeated the penis~{image=note}!"

    jump tfairy_h


label tfairy_h6:
    $ world.party.player.moans(2)
    with syasei1
    show tfairy ct22 at topright zorder 15 as ct
    show tfairy bk01 at xy(X=220, Y=130) zorder 10 as bk
    show tfairy bk02 at xy(X=220, Y=130) zorder 10 as bk2
    $ world.ejaculation()

    "Unable to endure the Twin Fairies rubbing their bodies on his penis, Luka finally ejaculates, covering their tiny bodies in semen."

    $ world.battle.lose()

    $ world.battle.count([24, 5])
    $ bad1 = "Luka succumbed to the Twin Fairies dancing as they clung to his penis."
    $ bad2 = "Caught by the Twin Fairies, they toyed with him for the rest of his life."
    show tfairy ct23 at topright zorder 15 as ct


    fairy_a "Hahah! It shot out~{image=note}!"
    fairy_b "Yay! We defeated the penis~{image=note}!"

    jump tfairy_h


label tfairy_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.party.player.bind == 1:
        play hseanwave "audio/se/hsean01_innerworks_a1.ogg"
    else:
        l "Ahh..."
        "Drained of power, I fall onto the ground as the Twin Fairies approach.{w}{nw}"

        play hseanwave "audio/se/hsean01_innerworks_a1.ogg"
        hide bk
        hide bk2
        show tfairy ct21 at topright zorder 15 as ct


        extend "{w=.7}\nSuddenly, they both grab onto my penis!"

    l "Ahh!"
    "Clinging to my penis, the two fairies tightly hug it.{w}\nUnder the soft pressure from their tiny arms, I let out a low moan."
    fairy_a "Hehehe... So sticky~{image=note}!"
    fairy_b "Sticky~{image=note}!"
    "The two of them start to lick the tip of my penis with their tiny tongues."
    "The sudden feeling of a tongue smaller than a finger licking the sensitive tip of my penis causes me to let out a surprised yell."
    l "Auu!!"
    "Suddenly, one of the fairies sticks her tongue inside my urethra.{w}\nThe direct stimulation on the most sensitive parts of my body causes my back to arch up."
    "The Twin Fairies tightly hug my penis as they continue licking off all the semen from my last orgasm."
    fairy_a "*Лижет*{image=note}"
    fairy_b "*Лижет*{w} *Лижет*{image=note}"
    "Putting strength into their arms, the fairies squeeze tighter around me.{w}\nWith their tiny bodies pushed up against me, I could feel every part of them on my penis."
    "These innocent acting fairies are doing such lewd things...{w}\nAre they really just playing?.. Or have they done this before?.."
    l "Auuu!!"
    "My body writhes as the Twin Fairies toy with my penis.{w}\nPre-come starts to leak out as the Twin Fairies watch the liquid slowly flow out with a look of curiosity."
    fairy_a "Oh! The white stuff is coming out~{image=note}!"
    fairy_b "Sticky soup~{image=note}!"
    "The Twin Fairies stick out their tongues and start to lap up the pre-come from my hole."
    "My waist trembles as their tiny tongues quickly lick up all of the semen."
    "Making sure they lick up every drop of it, they move their tongues all over the tip of my penis."
    l "Auu!"
    "As they strain to lap up the remaining pre-come, they squeeze my penis even tighter."
    fairy_a "The penis is twitching~{image=note}!"
    fairy_b "Is more white stuff coming?!{image=note}"
    "The disgrace of having my penis treated like a toy rises up through me.{w}\nWhile humiliated by the Twin Fairies, I finally give in to the pleasure."
    l "Ah...Ahhh!!"

    with syasei1
    show tfairy ct22 at topright zorder 15 as ct
    show tfairy bk01 at xy(X=220, Y=130) zorder 10 as bk
    show tfairy bk02 at xy(X=220, Y=130) zorder 10 as bk2
    $ world.ejaculation()

    "Unable to hold on, I ejaculate while being attacked by their tiny bodies.{w}{nw}"

    show tfairy ct23 at topright zorder 15 as ct
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nShooting out a large amount of semen, it covers the two of them."
    fairy_a "Hehehe... Did you see it explode?!{image=note}"
    fairy_b "A lot shot out~{image=note}!"
    "Even though the Twin Fairies laugh happily, it doesn't seem like their \"prank\" is about to end."
    "Even after ejaculation, they're still treating my penis like a toy.{w}\nThey start to move their tiny arms up and down while hugging me.{w}\nOne of the fairies starts to poke and massage the back muscle."
    l "Auuu..."
    "My body twists as their four hands play with my penis.{w}\nAs soon as more pre-come leaks out, they instantly start to lick it all up."
    fairy_a "Soup!{w} *Лижет*{image=note}"
    fairy_b "Yay!{w} *Лижет*{image=note}"
    "Their tiny tongues lap up the semen as soon as it comes out.{w}\nThe more they lick and stroke me, the more it comes out..."
    l "Auuu... Don't lick... there...!"
    "As if stuck in some kind of cruel loop, the pleasure from the two of them licking up my pre-come causes more to come out."
    "Too exhausted to do anything, all I can do is lay back and feel the pleasure as they play with my penis."
    fairy_a "Haha! How interesting~{image=note}!{w}\nLet's tease it more~{image=note}!"
    fairy_b "Tease it~{image=note}!"
    "I tremble as the fairies attack my penis with renewed interest.{w}\nDoing whatever they want, the Twin Fairies view me only as a toy."
    l "Ahhh...{w} I'm going to come again..."
    fairy_a "More white stuff~{image=note}!"
    fairy_b "More~{image=note}!"
    "The tiny arms of the Twin Fairies hug me tighter after my moan.{w}\nTheir four small hands stroke and rub every corner of my penis."
    "The sensation causes my head to go white as I'm forced over the edge."
    l "Ahhh!!"

    with syasei1
    $ world.ejaculation()

    "Exploding again, I cover their bodies with more semen.{w}\nBeing showered in the white rain, the Twin Fairies happily laugh."
    "While still joyfully laughing, the Twin Fairies look over at me.{w}\nIt really does feel like I've just become a toy..."
    fairy_a "Hehe... You're fun.{image=note}"
    fairy_b "We're going to keep you~{image=note}!"
    l "No way..."
    "The innocent way they said that makes it all the more frightening.{w}\nBut with them giving so much pleasure to me, I can't raise another word of protest."
    fairy_a "Let's play more~{image=note}!"
    fairy_b "We'll play a lot~{image=note}!"
    l "Ahhh... That's not..."
    "Cutting short my voice, they hug my penis tighter.{w}\nFeeling their tiny soft bodies squeezing me sends me to another height."
    "So soon after my last orgasm, I'm already brought to another..."

    with syasei1
    $ world.ejaculation()

    l "Ahhh... Stop... Already..."
    fairy_a "Not a chance~{image=note}!"
    fairy_b "We're going to play a lot more than this~{image=note}!"

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "The innocent fairies don't stop their mischief.{w}\nThey feed me honey from the flowers every so often so I won't die, but keep me too weak to run away."
    "I'm kept just as a toy for these two fairies to play with...{w}\nAgain today, they're playing with me..."

    if world.party.player.bind == 1:
        play hseanwave "audio/se/hsean01_innerworks_a1.ogg"

    scene bg 061 with blinds2
    show tfairy st01 at xy(X=220, Y=130)
    show tfairy ct21 at topright zorder 15 as ct


    l "Ahh... Help me...{w} Let me go... Please..."
    fairy_a "No!{w} Big bro is already just our toy~{image=note}!"
    fairy_b "We're going to keep you forever~{image=note}!"
    l "Auuu..."

    with syasei1
    show tfairy ct22 at topright zorder 15 as ct
    show tfairy bk01 at xy(X=220, Y=130) zorder 10 as bk
    show tfairy bk02 at xy(X=220, Y=130) zorder 10 as bk2
    $ world.ejaculation()
    show tfairy ct23 at topright zorder 15 as ct

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Thus I'm kept in the forest for the rest of my life as a toy.{w}\nMy adventure ends as the Twin Fairies plaything..."
    "..................."

    jump badend

label sylph_ng_start:
    python:
        world.troops[22] = Troop(world)
        world.troops[22].enemy[0] = Enemy(world)

        world.troops[22].enemy[0].name = Character("Сильфа")
        world.troops[22].enemy[0].sprite = ["sylph st51", "sylph st54", "sylph st53", "sylph st52"]
        world.troops[22].enemy[0].position = center
        world.troops[22].enemy[0].defense = 20
        world.troops[22].enemy[0].evasion = 80
        world.troops[22].enemy[0].max_life = world.troops[22].battle.difficulty(8000, 10000, 12000)
        world.troops[22].enemy[0].power = world.troops[22].battle.difficulty(0, 1, 2)
        world.troops[22].enemy[0].half[0] = "Хе-хе-хе..."
        world.troops[22].enemy[0].half[1] = "Ты уже почти проигра-а-ал, Лука~{image=note}!"
        world.troops[22].enemy[0].kiki[0] = "Ты уже всё?!"
        world.troops[22].enemy[0].kiki[1] = "Т-а-а-ак быстро~{image=note}!"

        world.troops[22].battle.tag = "sylph_ng"
        world.troops[22].battle.name = world.troops[22].enemy[0].name
        world.troops[22].battle.background = "bg 061"

        if persistent.music:
            world.troops[22].battle.music = 23
        else:
            world.troops[22].battle.music = 10

        world.troops[22].battle.exp = 900000
        world.troops[22].battle.ng = True
        world.troops[22].battle.exit = "lb_0042"

        world.troops[22].battle.init(0)

        if world.party.player.skill[3]:
            world.party.player.skill[3] = 3

        world.party.player.earth_keigen = 40

    l "Сильфа?!{w}\n... Что за херня?!"
    "Она выглядит как-то по другому... прямо как в пустыне, когда её сила была запечатана.{w}\n... Но в отличии от того раза, сейчас я чувствую куда большую силу, исходящую от неё."
    "Подождите-ка минутку...{w}\nНеужели?.."
    "Она уменьшила меня?!"
    l "Что за фигню ты творишь?!"
    show sylph st54
    sylph "Ха-ха...{w}\nЯ запечатала часть твоей магической энергии и передала её себе."
    l "... Ты...{w}\nЧТО?!"
    show sylph st51
    sylph "Не волнуйся! Это легко отменить!{w}\n... Если ты победишь меня, конечно же."
    l "Ты не можешь так делать!{w}\nЭто...{w} это читерство!"
    show sylph st54
    sylph "Хе-хе-хе...{w}\nТогда давай, останови меня!"
    show sylph st51
    l ".............."
    "Дерьмово.{w}\nПолагаю это её способ проверить мои силы в этот раз."
    "... Хотя я более чем уверен, что она просто хочет изнасиловать меня."
    l "Хорошо...{w}\nТогда я сражусь с тобой!"
    sylph "Ну тогда погна-а-али~{image=note}!"
    play sound "se/wind2.ogg"
    "Сильфа призывает силу ветра!"
    show effect wind
    pause 1.0
    hide effect
    $ world.battle.enemy[0].wind = 1
    $ world.battle.enemy[0].wind_turn = -1
    l "Уоа..."
    "Вокруг нас задул сильный ветер!"
    $ world.party.player.skill_set(2)
    $ world.battle.main()

label sylph_ng_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.aqua and world.battle.stage[0]:
            world.battle.common_attack()
        elif world.battle.result == 1 and world.party.player.aqua and not world.battle.stage[0]:
            renpy.jump(f"{world.battle.tag}_001")
        elif world.battle.result == 1 and not world.party.player.aqua:
            renpy.jump(f"{world.battle.tag}_002")
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label sylph_ng_001:
    $ world.party.player.attack()
    l "Что?..{w}\nЧто это?!"
    l "Это весь урон, который я способен нанести?!"
    sylph "Хе-хе-хе...{w}\nТеперь ты не сможешь сильно атаковать!"
    sylph "Но так как я не лучший боец по сравнению с другими духами, мы уравнены в силах!"
    l "... Хмпф."
    "Какой же геморр."
    $ world.battle.stage[0] = 1
    $ world.battle.common_attack_end()

label sylph_ng_002:
    l "Хаа!"
    "Лука атакует!"
    $ world.party.player.mp_regen()
    pause .5
    play sound "se/wind2.ogg"
    hide sylph
    pause .1
    $ world.battle.face(2)
    "Но Сильфа двигается подобно ветру и уклоняется!"
    if world.battle.stage[1] < 1:
        call sylph_ng003

    $ world.battle.face(1)
    $ world.battle.common_attack_end()

label sylph_ng003:
    l "Вот чёрт!"
    sylph "Ты правда думал, что сможешь таким ударом попасть по мне?!{w}\nЭто было та-а-ак медленно~{image=note}!"
    $ world.battle.face(1)
    sylph "... Но я чувствую, что ты владеешь техникой безмятежного разума.{w}\nПочему бы тебе не попробовать воспользоваться им?"
    $ world.battle.stage[1] = 1
    return

label sylph_ng_v:
    "... Я победил её?!"
    world.battle.enemy[0].name "............."
    $ world.battle.face(3)
    world.battle.enemy[0].name "Уааааааааа!{w} Ты такой жестокий!"
    "Лука заставил Сильфу плакать!"
    l "Что?! Я?!{w}\nЭто ты всё это начала!"
    world.battle.enemy[0].name "Но тебе не нужно было так сильно бить меня!{w}\nЯ просто хотела немного тебя изнасиловать!"
    l "?!"
    "\"Просто\"?{w}\n\"Немного\"?{w}\nЧто?!"
    l "Так теперь это моя вина?!{w}\nЯ даже не хочу сражаться с тобой!"
    world.battle.enemy[0].name "{i}Сопит{/i}{w}\nУааааааа..."
    world.battle.enemy[0].name "Я извиняюсь...{w}\nЯ не знала, что ещё делать..."
    world.battle.enemy[0].name "Вот.{w}\nТы можешь забрать свою силу назад..."
    play sound "se/power.ogg"
    "Сильфа начинает излучать слабую энергию!"
    play sound "se/flash.ogg"
    scene color white
    l "Ах!"
    "Внезапно, мой взор застилает пелена!"
    show bg 061
    "Сработало?{w}\nДа! Теперь я снова нормально размера!"

    $ world.battle.victory(1)

label sylph_ng_earth:
    $ world.battle.stage[1] = 1
    if not world.battle.stage[4]:
        sylph "Эй, ты уже получил Гноми!{w}\nХотя это тебе не поможе-е-ет~{image=note}!"
    elif world.battle.stage[4] == 1:
        sylph "Э-э-э..."
    elif world.battle.stage[4] == 2:
        sylph "Г-Гноми?.."
    $ world.battle.stage[4] += 1
    return

label sylph_ng_aqua:
    $ world.battle.stage[2] = 1
    if not world.battle.stage[4]:
        sylph "Эй, Дини вместе с тобой!{w}\nПриветики, Дини!"
    elif world.battle.stage[4] == 1:
        sylph "Ты получил и Дини?{w}\nХмм..."
    elif world.battle.stage[4] == 2:
        sylph "Д-Дини?.."
    $ world.battle.stage[4] += 1
    return

label sylph_ng_fire:
    $ world.battle.stage[3] = 1
    if not world.battle.stage[4]:
        sylph "Ты уже получил Мэнди?{w}\nПриветики, Мэнди!"
    elif world.battle.stage[4] == 1:
        sylph "Ещё и Мэнди?{w}\nСерьёзно?.."
    elif world.battle.stage[4] == 2:
        sylph "М-Мэнди?.."
    $ world.battle.stage[4] += 1
    return

label sylph_ng_v2:
    $ world.party.sylph_rep -= 2
    show sylph st55
    stop music fadeout 1.0
    world.battle.enemy[0].name "............."
    show sylph st56
    world.battle.enemy[0].name "В-а-а-а-а-а!"
    "............."
    play music "bgm/comi1.ogg"
    "Лука заставил Сильфу плакать!"
    l "А, что?{w}\nЧто я сделал?!"
    world.battle.enemy[0].name "Я ведь тебе даже не нужна!{w}\nТы сначала собрал остальных духов и только потом пришёл за мной!"
    l "Э-э-э?!{w}\nНо... это вовсе не так!"
    sylph "{i}сопит{/i}{w}\nУ-а-а-а..."
    "Сильфа потеряла боевой дух!"
    world.battle.enemy[0].name "Вот.{w}\nТы можешь забрать свою силу назад..."
    play sound "se/power.ogg"
    "Сильфа начинает излучать слабую энергию!"
    play sound "se/flash.ogg"
    scene color white
    l "Ах!"
    "Внезапно, мой взор застилает пелена!"
    show bg 061
    "Сработало?{w}\nДа! Теперь я снова нормального размера!"
    $ world.battle.exit = "lb_0042b"
    $ world.battle.victory(1)

label sylph_ng_a:
    if not world.battle.stage[1] and world.party.player.earth and world.party.player.skill[2]:
        call sylph_ng_earth
    elif not world.battle.stage[2] and world.party.player.aqua and world.party.player.skill[3]:
        call sylph_ng_aqua
    elif not world.battle.stage[3] and world.party.player.fire and world.party.player.skill[4]:
        call sylph_ng_fire
    if world.battle.stage[4] == 3 and world.party.player.skill[4] and world.party.player.skill[3] and world.party.player.skill[2]:
        jump sylph_ng_v2

    while True:
        if world.party.player.surrendered:
            jump sylph_ng_a5
        if not world.party.player.bind:
            $ world.battle.progress = 0

        $ random = rand().randint(1,5)

        if random == 1 and (world.battle.enemy[0].life < world.battle.enemy[0].max_life / 2) and not world.battle.stage[5]:
            call sylph_ng_a5
            $ world.battle.main()
        if random == 2:
            call sylph_ng_a1
            $ world.battle.main()
        if random == 3:
            call sylph_ng_a2
            $ world.battle.main()
        if random == 4:
            call sylph_ng_a3
            $ world.battle.main()
        if random == 5:
            call sylph_ng_a4
            $ world.battle.main()

label sylph_ng_a1:
    $ world.battle.skillcount(1, 3025)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ты серьёзно кончишь от этого?!",
        "Люди такие странные~{image=note}!",
        "Тебе нравиться, когда на тебя наступа-а-ают~{image=note}?"
    ])

    $ world.battle.show_skillname("Нога ветреного духа")
    play sound "audio/se/ero_koki1.ogg"
    "Сильфа наступает на пах Луки!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 76:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 66:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 36:
            world.party.player.aqua_guard()
        if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 21:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 16:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 11:
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (230, 250), 2: (270, 290), 3: (520, 540)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

        if not world.battle.enemy[0].wind:
            world.battle.hide_skillname()
            renpy.return_statement()

    play sound "audio/se/ero_koki1.ogg"
    "Она хватает член Луки своими пальцами!"
    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    play sound "audio/se/ero_koki1.ogg"
    "После чего, сжимает его своими мягкими ступнями!"
    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label sylph_ng_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я буду сосать его~{image=note}!",
        "Я попробую твою смерму~{image=note}!",
        "Сосём-сосём-сосём~{image=note}!"
    ])

    $ world.battle.show_skillname("Минет ветреного духа")
    play sound "audio/se/ero_buchu3.ogg"
    "Сильфа берёт член Луки в свой рот и начинает сосать его!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 76:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 66:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 36:
            world.party.player.aqua_guard()
        if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 21:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 16:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 11:
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (180, 200), 2: (220, 240), 3: (450, 470)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

        if not world.battle.enemy[0].wind:
            world.battle.hide_skillname()
            renpy.return_statement()

    "Она заглатывает его член глубже и начинает неистово сосать!"
    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    "Её влажный язык начинает лизать яички Луки!"
    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label sylph_ng_a3:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Скачем, скачем, скачем~{image=note}!",
        "Мои бёдра очень мягкие...",
        "Разве теперь мои бёдра не восхитительны~{image=note}?"
    ])

    $ world.battle.show_skillname("Сжатие ветреного духа")
    play sound "audio/se/ero_paizuri.ogg"
    "Сильфа зажимает член Луки меж своих мягких бёдер!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 76:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 66:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 36:
            world.party.player.aqua_guard()
        if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 21:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 16:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 11:
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (230, 250), 2: (270, 290), 3: (520, 540)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

        if not world.battle.enemy[0].wind:
            world.battle.hide_skillname()
            renpy.return_statement()

    "После чего, она начинает двигать ими вверх и вниз!"
    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    "Наконец, она сжимает свои бёдра так сильно, насколько может!"
    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label sylph_ng_a4:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Как насчёт моей груди~{image=note}?",
        "Погляди, они стали больше~{image=note}!",
        "Это заставит тебя кончить~{image=note}!"
    ])

    $ world.battle.show_skillname("Пайзури ветреного духа")
    play sound "audio/se/ero_koki1.ogg"
    "Сильфа зажимает член Луки меж своих сисек!"

    python:
        random = rand().randint(1, 100)+world.party.player.undine_buff

        if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 76:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 66:
            world.party.player.aqua_guard()
        elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 36:
            world.party.player.aqua_guard()
        if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 21:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 16:
            world.party.player.aqua_guard()
        elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 11:
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (230, 250), 2: (270, 290), 3: (520, 540)}

        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

        if not world.battle.enemy[0].wind:
            world.battle.hide_skillname()
            renpy.return_statement()

    play sound "audio/se/ero_koki1.ogg"
    "После чего, она сдавливает свою грудь вокруг его члена!"
    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    play sound "audio/se/ero_koki1.ogg"
    "После чего начинает двигать своей грудью вверх-вниз вместе с его членом!"
    python:
        if world.party.player.holy and world.party.player.wind_guard_calc({1: 1, 2: 1, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            world.rape["sylph"] += 1
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label sylph_ng_a5:
    $ world.battle.stage[5] = 1
    if world.party.player.surrendered:
        world.battle.enemy[0].name "Я собираюсь покончить с этим, Лука~{image=note}!"

    $ world.battle.show_skillname("Удержание ветреного духа")
    "Сильфа берёт Луку в захват!"
    if world.party.player.aqua:
        $ world.party.player.aqua_guard()
    if world.party.player.daystar:
        $ world.party.player.skill22x()
        $ world.battle.main()
    $ world.party.player.bind = 1
    $ world.party.player.status_print()
    "Сильфа прижимает его к земле!"

    sylph "Попался~{image=note}!"
    l "Гах..."
    sylph "Я не позволю тебе вырваться~{image=note}!"
    $ world.party.player.bind = 1
    $ world.battle.enemy[0].power = 1
    jump sylph_ng_a6

label sylph_ng_a6:
    $ world.battle.progress += 1
    if world.party.player.surrendered:
        world.battle.enemy[0].name "Я победила..."

    $ world.battle.show_skillname("Киска ветреного духа")

    play sound "audio/se/ero_pyu5.ogg"
    show sylph hb1

    "Сильфа насильно вставляет член Луки в себя!"

    $ world.party.player.bind = 2

    $ world.battle.enemy[0].damage = (900, 950)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()
    if not world.party.player.life:
        $ world.rape["sylph"] += 1
        $ renpy.jump("badend_h")

    "Лука изнасилован Сильфой!"
    l "Аххх!"
    world.battle.enemy[0].name "Мммм...{w}\nТак приятно..."
    world.battle.enemy[0].name "Я хочу ещё..."
    jump sylph_ng_a8

label sylph_ng_a8:
    $ world.battle.progress += 1

    $ world.battle.show_skillname("Киска ветреного духа")

    play sound "audio/se/ero_chupa4.ogg"
    show sylph hb1

    "Сильфа двигает своими бёдрами ввер-вниз!"

    $ world.party.player.bind = 2

    $ world.battle.enemy[0].damage = (950, 1000)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()
    if not world.party.player.life:
        $ world.rape["sylph"] += 1
        $ renpy.jump("badend_h")

    "Лука изнасилован Сильфой!"
    world.battle.enemy[0].name "Хе-хе-хе...{w}\nТы ведь уже скоро кончишь, так?"
    world.battle.enemy[0].name "Последний рывок!"
    jump sylph_ng_a9

label sylph_ng_a9:
    $ world.battle.progress += 1

    $ world.battle.show_skillname("Киска ветреного духа")
    play sound "audio/se/ero_chupa3.ogg"
    show sylph hb1
    "Сильфа прыгает на члене Луки со всей силы!"

    $ world.party.player.bind = 2

    $ world.battle.enemy[0].damage = (850, 900)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()
    if not world.party.player.life:
        $ world.rape["sylph"] += 1
        $ renpy.jump("badend_h")

    "Лука изнасилован Сильфой!"
    world.battle.enemy[0].name "Хмм?{w}\nТы всё ещё держишься?"
    world.battle.enemy[0].name "Я не позволю тебе вырва-а-аться~{image=note}!"
    jump sylph_ng_a9

label sylph_ng_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "А?{w}\nТак ты всё-таки {b}хочешь{/b} заняться со мной сексом!?{w}\nТы такой извращенец~{image=note}!"

    $ world.battle.enemy[0].attack()

label c_dryad_start:
    python:
        world.troops[23] = Troop(world)
        world.troops[23].enemy[0] = Enemy(world)

        world.troops[23].enemy[0].name = Character("Химера-дриада")
        world.troops[23].enemy[0].sprite = ["c_dryad st01", "c_dryad st01", "c_dryad st01", "c_dryad st11"]
        world.troops[23].enemy[0].position = center
        world.troops[23].enemy[0].defense = 100
        world.troops[23].enemy[0].evasion = 99
        world.troops[23].enemy[0].max_life = 2500
        world.troops[23].enemy[0].henkahp[0] = 1500
        world.troops[23].enemy[0].power = world.troops[23].battle.difficulty(0, 1, 2)

        world.troops[23].battle.tag = "c_dryad"
        world.troops[23].battle.name = world.troops[23].enemy[0].name
        world.troops[23].battle.background = "bg 061"

        if persistent.music:
            world.troops[23].battle.music = 27
        else:
            world.troops[23].battle.music = 3

        world.troops[23].battle.exp = 2000
        world.troops[23].battle.exit = "lb_0043"

        world.troops[23].battle.init(0)

    l "Гх..."

    play sound "se/miss.ogg"

    "Я отпрыгиваю назад и едва избегаю удара."

    play sound "se/karaburi.ogg"

    "Алиса же отражает атаку своим хвостом."
    a "Что это значит?!{w}\nАтаковать меня?"
    a "Да ты знаешь, что значит атаковать владыку монстров?!"

    $ world.battle.show_skillname("Омега Пламя")

    "Алиса создаёт огненный шторм!{w}{nw}"

    play sound "se/bom1.ogg"
    window hide
    hide screen hp
    show blaze zorder 100
    $ renpy.pause(4.5, hard=True)
    hide blaze
    show screen hp
    window auto

    extend "{w=.7}\nИспепеляющий огненный вихрь обдаёт Химеру-Дриаду!"

    play sound "se/damage2.ogg"

    $ narrator(f"Алиса наносит {rand().randint(7000, 7100)} ед. урона!")

    $ world.battle.hide_skillname()

    world.battle.enemy[0].name "..................."
    a "Что?!{w}\nОно и не дрогнуло от моей атаки?!"
    l "Алиса, ты не сможешь нанести урон этой штуке.{w}\nХотя я и не знаю почему."
    "Возможно она наполнена святой энергией..."
    l "Просто оставь это на меня!{w}\nЯ смогу легко победить этого монстра!"
    a "... Хорошо.{w}\nУверена, это не составит тебе трудностей."
    play sound "se/escape.ogg"
    pause .5
    "Алиса исчезает."
    "Как только она уходит, я разворачиваюсь к монстру."
    "Испепеляющее пламя уже угасло.{w}\nОно повернуло свой бесчувственный взгляд ко мне."
    "Этот монстр нарушает лесной покой...{w}\nЯ устраню его!"

    $ world.battle.main()

label c_dryad_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind == 2:
            renpy.jump("c_dryad_dasyutu")
        elif world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.party.player.bind == 2:
            world.battle.common_struggle()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()


label c_dryad_dasyutu:
    $ world.battle.before_action = 9

    l "Гах... отстань!"
    "Лука атакует!{w}\nИз-за опутавшихся вокруг плетей сила атаки Луки падает!"

    $ world.party.player.mp_regen()
    play sound "se/slash.ogg"

    $ world.party.player.damage = 50
    $ world.battle.enemy[0].life_calc()

    play sound "se/dassyutu.ogg"
    $ renpy.show(world.battle.enemy[0].sprite[2], at_list=[world.battle.enemy[0].position])
    $ world.party.player.bind = 0
    $ world.party.player.status_print()

    if not world.battle.enemy[0].life:
        $ world.party.player.bind = 0
        $ world.battlestatus_print()
        $ renpy.jump(f"{world.battle.tag}_v")

    "Лука вырвался из хватки Химеры-Дриады!"
    world.battle.enemy[0].name "...................."

    $ world.battle.main()

label c_dryad_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "!...."

    play sound "se/syometu.ogg"
    hide c_dryad with crash

    "Тело Химеры-Дриады распыляется на тысячи частиц.{w}\nОстатки листвы и лепестков разлетаются вокруг!"

    $ world.battle.victory(1)

label c_dryad_a:
    if world.party.player.bind == 2:
        call c_dryad_a10
        $ world.battle.main()

    if world.battle.stage[0] == 1 and not world.party.player.guard:
        jump c_dryad_hc
    elif world.battle.stage[0] == 1 and world.party.player.guard:
        call c_dryad_a5
        $ world.battle.main()

    if world.party.player.bind == 1:
        while True:
            $ random = rand().randint(1,3)

            if random == 1:
                call c_dryad_a1
                $ world.battle.main()
            elif random == 2:
                call c_dryad_a8
                $ world.battle.main()
            elif random == 3:
                call c_dryad_a9
                $ world.battle.main()

    while True:
        $ random = rand().randint(1,6)

        if random == 1:
            call c_dryad_a1
            $ world.battle.main()
        elif random == 2:
            call c_dryad_a2
            $ world.battle.main()
        elif random == 3:
            call c_dryad_a3
            $ world.battle.main()
        elif random == 4 and world.party.player.status == 0 and not world.party.player.surrendered and world.battle.delay[0] > 3:
            call c_dryad_a4
            $ world.battle.main()
        elif random == 5 and world.party.player.status == 0 and world.battle.delay[1] > 3:
            call c_dryad_a6
            $ world.battle.main()
        elif random == 6 and world.battle.delay[2] > 3:
            call c_dryad_a7
            $ world.battle.main()


label c_dryad_a1:
    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Поглаживание плющём")
    play sound "audio/se/ero_makituki1.ogg"

    "Покрытый слизью плющ вытягивается в сторону Луки!"

    $ world.battle.cry(narrator, [
        "Плющ опутывает член Луки!",
        "Плющ гладит член Луки!"
    ])

    $ world.battle.skillcount(1, 3174)
    python:
        world.battle.enemy[0].damage = {1: (25, 30), 2: (32, 40), 3: (38, 55)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label c_dryad_a2:
    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Спермоядная лилия")
    play sound "audio/se/ero_makituki2.ogg"

    "Лилия настигает Луку, с её лепестков капает слизь!"

    $ world.battle.cry(narrator, [
        "A lily wraps around Luka's penis, creating a suction!",
        "A lily wraps around Luka's penis, and starts to suck as if it was a mouth!",
        "A lily wraps around Luka's penis, and starts to intensely suck!"
    ])

    $ world.battle.skillcount(1, 3175)

    python:
        world.battle.enemy[0].damage = {1: (32, 36), 2: (43, 47), 3: (59, 67)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label c_dryad_a3:
    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Подсолнух-пожиратель")
    play sound "audio/se/ero_makituki2.ogg"

    "Мягкий подсолнух накрывает пах Луки!"

    $ world.battle.cry(narrator, [
        "Each tiny seed wriggles around and brings a strange pleasure to Luka!",
        "Each tiny seed wriggles and tickles Luka's penis!",
        "The soft sunflower stimulates Luka's penis!"
    ])

    $ world.battle.skillcount(1, 3194)

    python:
        world.battle.enemy[0].damage = {1: (38, 48), 2: (52, 67), 3: (71, 93)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label c_dryad_a4:
    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Роза удушения")
    $ world.battle.enemy[0].sprite = ["c_dryad st21"] * 4
    show c_dryad st21
    $ world.battle.stage[0] = 1

    "Яркая красная роза раскрывается на теле химера-дриады!"

    $ world.battle.hide_skillname()

    return


label c_dryad_a5:
    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Роза удушения")

    "Ярко-красная роза устремляется к Луке!"

    play sound "audio/se/escape.ogg"

    "Но Лука уворачивается!"

    $ world.battle.hide_skillname()
    $ world.battle.enemy[0].sprite = ["c_dryad st01"] * 4
    $ world.battle.enemy[0].sprite[3] = "c_dryad st11"
    show c_dryad st01

    $ world.battle.stage[0] = 0

    return

label c_dryad_a6:
    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Отравленные феромоны")
    play sound "audio/se/gas.ogg"

    "Химера-дриада выпускает отравленные феромоны из своих цветов!"

    $ world.battle.skillcount(1, 3178)

    if world.party.player.wind:
        play sound "se/miss_wind.ogg"
        "Но порыв ветра сдувает облако!"
        return
    else:
        $ world.party.player.status = 1
        $ world.party.player.status_print()

    "Лука очарован!"

    $ world.battle.hide_skillname()

    if persistent.difficulty == 1:
        $ world.party.player.status_turn = 2
    elif persistent.difficulty == 2:
        $ world.party.player.status_turn = 2
    elif persistent.difficulty == 3:
        $ world.party.player.status_turn = rand().randint(3,4)

    return


label c_dryad_a7:
    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Magic Flower Hug")
    play sound "audio/se/ero_makituki3.ogg"

    "Countless flowers and ivy stretch out and wrap around Luka's body!"

    $ world.battle.enemy[0].damage = {1: (15, 20), 2: (20, 28), 3: (28, 38)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Luka's body has been wrapped by the ivy!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("c_dryad_h4")

    if world.party.player.surrendered:
        return

    l "Uguh...."
    "The ivy wraps around my feet as the petals of the flowers cover my body."
    "If I don't get out of this quickly, I don't know what's going to happen!"

    if persistent.difficulty == 1:
        $ world.battle.enemy[0].power = 2
    elif persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 2
    elif persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return


label c_dryad_a8:
    "Luka's limbs are bound by the ivy, as the flowers cover his body!"

    $ world.battle.skillcount(1, 3179)

    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Flower Energy Suck")
    play sound "audio/se/ero_chupa6.ogg"

    "A flower starts to suck on Luka's penis!{w}{nw}"

    $ world.battle.cry(extend, [
        "\nThe flower wriggles seductively as Luka's energy is sucked out!",
        "\nThe petals of the flowers vibrate as they suck out Luka's energy!",
        "\nThe flower sucks Luka's energy!"
    ])

    $ world.battle.enemy[0].damage = {1: (22, 27), 2: (28, 35), 3: (39, 49)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], drain=True)

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("c_dryad_h5")
    return


label c_dryad_a9:
    "Luka's limbs are bound by the ivy, as the flowers cover his body!"
    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Floral Funeral Preparations")
    $ world.party.player.bind = 2

    "All of the plants on the Chimera Dryad start to vibrate excitedly!"

    $ world.battle.hide_skillname()

    l "What's happening?!"
    "This is a strange atmosphere...{w}\nI have to break free quickly!!"

    return


label c_dryad_a10:
    "Luka's limbs are bound by the ivy, as the flowers cover his body!"

    $ world.battle.skillcount(1, 3180)

    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Floral Funeral")
    play sound "audio/se/ero_makituki3.ogg"
    show c_dryad st13 at xy(X=140)

    "The flowers and ivy cover Luka's body!{w}\nThe ivy tightly coils around Luka's body as a flower starts to suck on his penis!"

    $ world.battle.enemy[0].damage = (25, 30)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    show c_dryad st15

    "A huge flower wraps around Luka's body!{w}\nFrom head to toe, Luka has been swallowed by a giant flower!"

    $ world.battle.enemy[0].damage = (55, 65)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("c_dryad_h6")

    l "Ahhh!!"
    "My whole body is surrounded by a giant flower.{w}\nAs if being eaten, the walls of the flower start to pull me deeper inside of it."
    "At the same time, another flower is violently sucking on my penis."

    jump c_dryad_a11


label c_dryad_a11:
    $ world.battle.skillcount(1, 3180)

    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Floral Funeral")
    play sound "audio/se/ero_chupa6.ogg"

    "Countless flowers are attacking every corner of Luka's body!{w}{nw}"

    $ world.battle.cry(extend, [
        "\nCountless flowers suck on Luka's groin!",
        "\nThe petals wrapped around Luka's body start to clamp down on him, as if chewing!",
        "\nThe flowers start to suck on every part of Luka's body!",
        "\nThe flower around Luka's body tightens!",
        "\nThe flower sucking on Luka's penis sucks even harder!"
    ])

    $ world.battle.enemy[0].damage = (55, 65)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("c_dryad_h6")

    jump c_dryad_a11


label c_dryad_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.enemy[0].attack()

label c_dryad_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Ivy Stroking"
    $ list2 = "Semen Slurping Lily"
    $ list3 = "Maneating Sunflower"
    $ list4 = "Rose of Strangulation"
    $ list5 = "Intoxicating Incense"
    $ list6 = "Flower Energy Suck"
    $ list7 = "Floral Funeral"

    if persistent.skills[3174][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3175][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3194][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3177][0] > 0:
        $ list4_unlock = 1

    if persistent.skills[3178][0] > 0:
        $ list5_unlock = 1

    if persistent.skills[3179][0] > 0:
        $ list6_unlock = 1

    if persistent.skills[3180][0] > 0:
        $ list7_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump c_dryad_onedari1
    elif world.battle.result == 2:
        jump c_dryad_onedari2
    elif world.battle.result == 3:
        jump c_dryad_onedari3
    elif world.battle.result == 4:
        jump c_dryad_onedari4
    elif world.battle.result == 5:
        jump c_dryad_onedari5
    elif world.battle.result == 6:
        jump c_dryad_onedari6
    elif world.battle.result == 7:
        jump c_dryad_onedari7
    elif world.battle.result == -1:
        jump c_dryad_main


label c_dryad_onedari1:
    call onedari_syori

    while True:
        call c_dryad_a1


label c_dryad_onedari2:
    call onedari_syori

    while True:
        call c_dryad_a2


label c_dryad_onedari3:
    call onedari_syori

    while True:
        call c_dryad_a3


label c_dryad_onedari4:
    call onedari_syori

    if world.battle.stage[0] == 0:
        call c_dryad_a4

    jump c_dryad_hc


label c_dryad_onedari5:
    call onedari_syori
    call c_dryad_a6

    while True:
        $ random = rand().randint(1,3)

        if random == 1:
            call c_dryad_a1
        elif random == 2:
            call c_dryad_a2
        elif random == 3:
            call c_dryad_a3


label c_dryad_onedari6:
    call onedari_syori_k

    if not world.party.player.bind:
        call c_dryad_a7

    while True:
        call c_dryad_a8


label c_dryad_onedari7:
    call onedari_syori_k

    if not world.party.player.bind:
        call c_dryad_a7

    call c_dryad_a10


label c_dryad_h1:
    $ world.party.player.moans(1)
    with syasei1
    show c_dryad bk01 zorder 10 as bk
    $ world.ejaculation()

    "With the ivy stroking his penis, Luka finally succumbs to the pleasure forced into him."

    $ world.battle.lose()

    $ world.battle.count([12, 11])
    $ bad1 = "Luka succumbed to the ivy's stroking."
    $ bad2 = "Lusted after by the flowers, Luka was digested by the Chimera Dryad."

    world.battle.enemy[0].name "...................."
    "The Chimera Dryad doesn't seem to notice the semen staining her body."

    jump c_dryad_ha


label c_dryad_h2:
    $ world.party.player.moans(1)
    with syasei1
    show c_dryad bk02 zorder 10 as bk
    $ world.ejaculation()

    "With the lily sucking on his penis, Luka finally succumbs to the pleasure forced into him."

    $ world.battle.lose()

    $ world.battle.count([12, 11])
    $ bad1 = "Luka succumbed to the Chimera Dryad's lily sucking on him."
    $ bad2 = "Lusted after by the flowers, Luka was digested by the Chimera Dryad."

    world.battle.enemy[0].name "...................."
    "The Chimera Dryad doesn't seem to notice the semen staining her body."

    jump c_dryad_ha


label c_dryad_h3:
    $ world.party.player.moans(1)
    with syasei1
    show c_dryad bk04 zorder 10 as bk
    $ world.ejaculation()

    "With the sunflower massaging his penis, Luka finally succumbs to the pleasure forced into him."

    $ world.battle.lose()

    $ world.battle.count([12, 11])
    $ bad1 = "Luka succumbed to the Chimera Dryad's sunflower massaging him."
    $ bad2 = "Lusted after by the flowers, Luka was digested by the Chimera Dryad."

    world.battle.enemy[0].name "...................."
    "The Chimera Dryad doesn't seem to notice the semen staining her body."

    jump c_dryad_ha


label c_dryad_h4:
    $ world.party.player.moans(3)
    with syasei1
    show c_dryad bk01 zorder 10 as bk
    $ world.ejaculation()

    "As soon as Luka is bound by the ivy, he accidentally ejaculates."

    $ world.battle.lose()

    $ persistent.count_bouhatu += 1
    $ world.battle.count([12, 11])
    $ bad1 = "While being wrapped by the ivy, Luka accidentally ejaculated."
    $ bad2 = "Lusted after by the flowers, Luka was digested by the Chimera Dryad."

    world.battle.enemy[0].name "...................."
    "The Chimera Dryad doesn't seem to notice the semen staining her body."

    jump c_dryad_ha


label c_dryad_h5:
    $ world.party.player.moans(1)
    with syasei1
    $ world.ejaculation()

    "As the flower sucks out Luka's energy through his penis, he gives in to desire."

    $ world.battle.lose()

    $ world.battle.count([12, 11])
    $ bad1 = "While having his energy sucked by the flower, Luka ejaculated."
    $ bad2 = "Lusted after by the flowers, Luka was digested by the Chimera Dryad."

    world.battle.enemy[0].name "...................."
    "The Chimera Dryad doesn't seem to notice the semen staining her body."

    jump c_dryad_ha


label c_dryad_h6:
    $ world.party.player.moans(2)
    with syasei1
    $ world.ejaculation()

    "Unable to endure all of the flowers attacking him at once, Luka quickly ejaculates."

    $ world.battle.lose()

    $ world.battle.count([12, 11])
    $ bad1 = "Luka succumbed to the Chimera Dryad's floral funeral."
    $ bad2 = "Lusted after by the flowers, Luka was digested by the Chimera Dryad."

    world.battle.enemy[0].name "...................."

    $ skill01 = 6

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    jump c_dryad_hb


label c_dryad_ha:
    $ skill01 = 6

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.party.player.bind:
        play hseanwave "audio/se/hsean21_tentacle2.ogg"
    else:
        l "Uaaa..."
        "Forced to come, I'm left powerless..."

        play hseanwave "audio/se/hsean21_tentacle2.ogg"
        hide bk
        show c_dryad st11


        "Ivy shoots out from the Chimera Dryad, as my body is wrapped up!"

    l "Uaaa... Stop!"

    show c_dryad st12

    "The mucus dripping from the flowers melt my clothes.{w}\nI twist my body to try to escape from the strange feeling of the soft flowers on my bare skin."
    l "Uuu..."
    "The plants crawl all over my body with an eerie sensation."
    world.battle.enemy[0].name "...................."
    "A flower slowly started to move toward my groin.{w}\nWith mucus dripping from the petals, it's incredibly strange looking."
    l "What is that?.."

    play hseanwave "audio/se/hsean22_tentacle3.ogg"
    show c_dryad st13

    l "Auuu!"
    "Suddenly, the flower bites down on my penis!{w}\nThe flower starts to suck on me as if it was a mouth."
    "The sticky mucus starts to cover my penis as the petals rub me at the same time."
    "The unexpected pleasure from the flower causes me to relax my body."
    world.battle.enemy[0].name "...................."
    l "Faaa... That feels good..."
    "The strange flower gives an irresistible pleasure.{w}\nSkillfully stroking and sucking on me, the flower starts to tighten around me at the same time."
    "This mysterious flower is forcing pleasure into me..."
    l "Auuu..."
    "I feel the pre-come start to leak out.{w}\nAs soon as it starts to leak out, the flowers petals start to rub against my urethral opening, sucking up every drop as it appears."
    "The suction from the flower causes my head to go white at once."
    l "Ahh!!"

    with syasei1
    $ world.ejaculation()

    "Exploding as the flower sucks on me, it doesn't allow a single drop to escape from its suction."
    l "Aaa..."
    "Forced to fertilize this strange flower, the immoral feelings start to build up in me."

    show c_dryad st14

    l "Hrmf!"
    "In the next instant, a flower covers my mouth.{w}\nIt starts to slowly force a strange honey-like liquid into my mouth."
    l "Mmmfff..."
    "With no choice but to drink the sweet tasting liquid, a strange numbness starts to come over my body."
    "As if becoming drunk, I feel myself start to enter some sort of trance."
    world.battle.enemy[0].name "...................."
    l "Mmm... Mmmm..."
    "Like the flower covering my groin, the one on my mouth starts to suck."
    "I feel my energy drain as the flower covering my mouth continues its strange sucking movement."
    "Slowly, a comfortable feeling of exhaustion starts to come over me."
    l "Mm...mmm..."
    "The same feeling was being done to my lower body.{w}\nWhile sexually stimulating me, the flower is sucking out my energy."
    "As the feeling of exhaustion grows, I surrender my body to the dual suction."
    l "Faa..."
    "I realize that this thing is going to eat me...{w}\nThis unknown monster is going to suck out my life..."
    "Even though I know my fate, I can't fight against it.{w}\nSurrendered to the pleasure, it's not possible to run any longer."
    "I feel myself reaching another height as the beautiful flower sucks on me."
    l "Ah... Ahhhh!"
    "My lower back arches up as I don't even try to endure the pleasure."

    with syasei1
    $ world.ejaculation()

    "As I spurt out my seed into the flower, it sucks it all up.{w}\nI feel nothing but pleasure as this flower sucks up all of my semen."
    l "Faa..."
    "Drunk on pleasure, my body and mind have both surrendered.{w}\nPerhaps in a desperate attempt to ignore my fate, I drown myself in the ecstasy."
    l "Auu... That's incredible...{w}\nMore... Suck more..."
    "Already drowning in pleasure, I can't do anything but ask for more.{w}\nThe Chimera Dryad looks at me with her apathetic eyes."
    world.battle.enemy[0].name "...................."
    "...No, not apathetic. It's more robotic...{w}\nAs if devoid of thoughts and feelings, it's just mechanically sucking out my semen..."
    l "Aiie... Too good..."
    "Not giving me a chance to rest, it continues to suck out my semen.{w}\nFrom the base to the tip, the sweet pleasure is given to every inch of my penis."
    "As if giving in to the desires of the flower, I give it more of my semen."

    with syasei1
    $ world.ejaculation()

    l "Aaaa..."
    "I don't even know how many times I've ejaculated...{w}{nw}"

    show c_dryad st15
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nSuddenly, all of the other flowers wrap around my body!"

    jump c_dryad_hb


label c_dryad_hb:
    l "Mmmf!"

    play hseanwave "audio/se/hsean22_tentacle3.ogg"

    "Countless flowers wriggle and move all over my body.{w}\nAs if using my body as fertilizer, the flowers desperately suck every inch of my body."
    "My body writhes around as the suction brings an overpowering pleasure."

    if persistent.vore == 1:
        jump vore

    l "Mmmmfffff!!"

    with syasei1
    $ world.ejaculation()

    "My body goes into convulsions as the tremendous pleasure blows through my body."
    "The flower wrapped around my penis greedily sucks up everything as it's shot out."
    "As if spurred on by feeding it, the flowers only increase the force of their suction."
    l "Mmff!{w} Mmmmffff!"
    world.battle.enemy[0].name "...................."
    "The mucus from the petals covers my body as the flowers move and massage me."
    "As more of the mucus covers my skin, it feels like more of my energy is leaking out."
    "As the flowers leech out the energy from every corner of my body, it feels like my entire body is one giant erogenous zone."
    "This is a coffin made of flowers...{w}\nA funeral of flowers... Once they wrap a man, they suck out his life..."
    l "Ммммммfff!!"

    with syasei1
    $ world.ejaculation()

    "Wrapped in the flowers, I feed them.{w}\nStuck in this coffin of death, even my seed of life is eaten."
    "Instead of creating new life, it's only being sucked out as nourishment..."
    l "Mmmm..."
    "But my body is already addicted to this coffin.{w}\nSurrendered to the pleasure, I rejoice at giving it my energy."
    l "Mm...mmmm..."

    with syasei1
    $ world.ejaculation()

    "Stuck in this coffin, the only thing left for me is more pleasure.{w}\nMy long life until now will be given to this monster as food, just to sustain it for a couple of days."
    l "M...mm..."

    with syasei1
    $ world.ejaculation()

    "I give more of my semen to the flowers.{w}\nThe energy leaks out of my body."
    "With no energy left, the exhaustion overtakes my body.{w}\nWith my entire body stuck in a sweet paralysis, I feel my consciousness begin to melt away."

    with syasei1
    $ world.ejaculation()

    "A soft feeling of euphoria fills my mind as I'm able to orgasm a final time."
    l "...................."
    "Stuck in that sweet, sweet intoxication, my consciousness finally fades."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Like that, my body and mind was sucked out by the Chimera Dryad.{w}\nDevoured by the unknown plant monster, I reach a tragic end."
    ".................."

    jump badend


label c_dryad_hc:
    $ world.battle.skillcount(1, 3177)
    $ skill01 = 6

    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Rose of Strangulation")

    "The bright red rose shoots out from the Chimera Dryad's body!{w}\nThe rose fastens on to Luka's penis!"
    l "Ahhhh!!"

    play sound "audio/se/ero_chupa6.ogg"

    "The soft petals of the flower wrap around Luka's penis!{w}\nIt starts to violently suck!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    l "Aieee!"
    "The overpowering stimulation forces me to my knees instantly.{w}\nWith all of my strength drained in one blow, I fall to the ground."

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "Something inside the rose wraps around my penis as the suction continues!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    l "Haaaa!!"

    with syasei1
    show c_dryad bk05 zorder 10 as bk
    $ world.ejaculation()
    play music "audio/bgm/ero1.ogg"

    "The overwhelming sucking sensation forces me to ejaculate surprisingly fast."
    l "Ahh! Stop!"

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "The rose sucks out every drop of semen still in my urethra!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    l "Ahhh..."
    "The rose wraps tighter around Luka!"

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "The petals close around the base of Luka's penis!{w}\nLike that, the suction increases!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    l "Stop..."

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "The rose contracts tighter!{w}\nFrom the base to the tip, Luka's penis is violently sucked!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    l "Ahhh!!"

    with syasei1
    $ world.ejaculation()

    "It already forced me to come again...{w}\nBut it doesn't stop attacking me even while I orgasm."
    "It doesn't seem like it will stop until there's nothing left..."
    l "Stop already... Help..."
    world.battle.enemy[0].name "...................."
    "I'm not sure if she's ignoring my pleas, or if she is even capable of understanding."
    "With an expressionless face, the Chimera Dryad's rose continues to suck on Luka!"

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "The slimy insides of the rose close around Luka's penis!{w}\nDrawn deeper inside the flower, Luka's penis held tight!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    l "Faaa... Too much..."
    "I writhe helplessly as the rose sucks on me.{w}\nBut no matter what I do, the rose doesn't lose its grip."

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "The rose continues to suck on Luka's penis!{w}\nCreating a vacuum around him, it forcibly squeezes out his semen!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()
    with syasei1
    show c_dryad bk06 zorder 10 as bk
    $ world.ejaculation()

    "The white semen bleeds out of me into the red rose.{w}\nIs it going to suck on me until it turns pure white?.."
    l "Stop...{w}\nIf this keeps going..."

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "The rose tightens around Luka's penis!{w}\nOblivious to his cries, the Chimera Dryad sucks out more semen!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()
    with syasei1
    $ world.ejaculation()

    l "Stop...{w} I'll die..."

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "The rose tightens around Luka's penis!{w}\nOblivious to his cries, the Chimera Dryad sucks out more semen!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()
    with syasei1
    $ world.ejaculation()

    l "Faa...a..."
    "Every time my penis loses its erection after an orgasm, the rose tightens around it, forcing it to be hard again."
    "Like that, it continues to force me to ejaculate."
    "Over, and over, and over..."

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "The rose tightens around Luka's penis!{w}\nIt sucks out more semen!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()
    with syasei1
    $ world.ejaculation()

    l "Aaa..."
    "My body has lost all of its strength.{w}\nI've already realized that I won't escape from this alive."
    "Like this, she's going to kill me by sucking out my life itself..."
    l "Faa.. Stop..."
    "The only thing I can do is weakly beg her."

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "The rose tightens around Luka's penis!{w}\nAs if trying to suck out the semen straight from his balls, the rose sucks on Luka's penis!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()
    with syasei1
    $ world.ejaculation()

    l "A...a..."

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "The rose tightens around Luka's penis!{w}\nWith unfeeling mechanical motions, it continues to milk Luka!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()
    with syasei1
    $ world.ejaculation()

    l "...................."
    "I can't even keep my eyes open...{w}\nThe rose is sucking everything from me..."

    $ world.battle.show_skillname("Rose of Strangulation")
    play sound "audio/se/ero_chupa6.ogg"

    "The rose sucks Luka in even deeper!{w}\nWith forced stimulation, it sucks out Luka's semen!"

    $ world.battle.enemy[0].damage = (120, 150)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()
    with syasei1
    $ world.ejaculation()

    l "...U...a..."
    "Orgasm after orgasm, it doesn't stop for a moment.{w}{nw}"

    with syasei1
    $ world.ejaculation()

    extend "{w=.7}\nNo matter how much I give it, it's never enough.{w}{nw}"

    with syasei1
    $ world.ejaculation()

    extend "{w=.7}\nEndlessly squeezed, until finally..."

    with syasei1
    show c_dryad st22
    $ world.ejaculation()

    "The rose has been completely dyed white.{w}\nMy last view of the world is of that white flower of despair, as I lose consciousness."

    hide screen hp
    scene bg black with Dissolve(3.0)

    "................"

    $ world.battle.count([12, 4])
    $ bad1 = "Luka was exhausted by the Chimera Dryad's rose."
    $ bad2 = "Luka was continually squeezed like that until his death."
    jump badend

label taran_start:
    python:
        world.troops[24] = Troop(world)
        world.troops[24].enemy[0] = Enemy(world)

        world.troops[24].enemy[0].name = Character("Девушка-тарантул")
        world.troops[24].enemy[0].sprite = ["taran st01", "taran st02", "taran st03", "taran h1"]
        world.troops[24].enemy[0].position = center
        world.troops[24].enemy[0].defense = 100
        world.troops[24].enemy[0].evasion = 95
        world.troops[24].enemy[0].max_life = world.troops[24].battle.difficulty(900, 1125, 1350)
        world.troops[24].enemy[0].power = world.troops[24].battle.difficulty(0, 1, 2)

        world.troops[24].battle.tag = "taran"
        world.troops[24].battle.name = world.troops[24].enemy[0].name
        world.troops[24].battle.background = "bg 030"
        world.troops[24].battle.music = 1
        world.troops[24].battle.exp = 1500
        world.troops[24].battle.exit = "lb_0045"

        world.troops[24].battle.init()

    $ world.battle.face(2)

    world.battle.enemy[0].name "Хе-хе... Какой восхитительный на вид человек.{w}\nЯ заверну тебя в свой шёлк и сделаю тебя своей добычей..."
    l "................."
    "Я не могу этого допустить.{w}\nПридётся её запечатать!"

    $ world.battle.face(1)
    $ world.battle.main()

label taran_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()


label taran_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Как я могла... проиграть человеку?!"

    play sound "se/syometu.ogg"
    hide taran with crash

    "Девушка-тарантул превращается в маленького паучка!"

    $ world.battle.victory(1)

label taran_a:
    if world.battle.stage[0] == 1:
        call taran_a7
        $ world.battle.main()

    if world.party.player.bind == 1:
        $ random = rand().randint(1,2)

        if random == 1:
            call taran_a4
            $ world.battle.main()
        elif random == 2:
            call taran_a5
            $ world.battle.main()

    while True:
        $ random = rand().randint(1,5)

        if random == 1:
            call taran_a1
            $ world.battle.main()
        elif random == 2:
            call taran_a2
            $ world.battle.main()
        elif random == 3:
            call taran_a9
            $ world.battle.main()
        elif random == 4 and world.battle.delay[0] > 3:
            call taran_a6
            $ world.battle.main()
        elif random == 5 and world.battle.delay[1] > 3:
            call taran_a3
            $ world.battle.main()


label taran_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "I wonder if you can endure it when I do this...",
        "Let out a scream...",
        "I'll force you to scream..."
    ])

    $ world.battle.show_skillname("Tarantula Caress")
    play sound "audio/se/umaru.ogg"

    "The Tarantula Girl's eight legs caress Luka!{w}{nw}"
    extend "{w=.7}\nHer legs relentlessly attack Luka's groin!"

    $ world.battle.skillcount(1, 3196)

    python:
        world.battle.enemy[0].damage = {1: (20, 24), 2: (30, 36), 3: (40, 48)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label taran_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll defeat you like this...",
        "If I do this, can I suck out your semen right away?..",
        "It's fine if you want to surrender in my mouth..."
    ])

    $ world.battle.show_skillname("Tarantula Blowjob")
    play sound "audio/se/ero_buchu2.ogg"

    "The Tarantula Girl forces her face into Luka's groin, and starts to suck on Luka's penis!"

    $ world.battle.skillcount(1, 3197)

    python:
        world.battle.enemy[0].damage = {1: (22, 26), 2: (33, 39), 3: (44, 52)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label taran_a3:
    if not world.party.player.surrendered:
        if world.battle.first[0] == 1:
            world.battle.enemy[0].name "I'll get on top again..."
        else:
            world.battle.enemy[0].name "I'll take the top..."

    $ world.battle.show_skillname("Tarantula Hold")

    "The Tarantula Girl crawls over Luka, and holds him down with her eight legs!"

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Luka's body is held down by the Tarantula Girl!"

    $ world.battle.hide_skillname()

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    if world.battle.first[0] == 1:
        world.battle.enemy[0].name "I won't let you go this time.{w}\nI'll weaken you by forcing you to come before I eat you..."
    else:
        world.battle.enemy[0].name "Now, I'll play with you...{w}\nEnjoy it... No matter what you do, I'll make you my food..."

    $ world.battle.first[0] = 1

    if persistent.difficulty == 1:
        $ world.battle.enemy[0].power = 2
    elif persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 2
    elif persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return


label taran_a4:
    "The Tarantula Girl is on top of Luka!"

    $ world.battle.skillcount(1, 3198)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "How does it feel to be played with while unable to do anything?",
        "I'll make you a little more... Comfortable.",
        "Hahaha... Does it feel good?"
    ])

    $ world.battle.show_skillname("Restrained Tarantula Caress")
    play sound "audio/se/umaru.ogg"

    "The eight legs of the Tarantula Girl rub Luka's penis!"

    python:
        world.battle.enemy[0].damage = {1: (20, 24), 2: (30, 36), 3: (40, 48)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label taran_a5:
    "The Tarantula Girl is on top of Luka!"

    $ world.battle.skillcount(1, 3199)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll torture your genitals...",
        "Succumb to the torture and become my food...",
        "Aren't you humiliated by being toyed with Нравится?",
        "Are you feeling good while being tortured? How miserable.",
        "I can even torture you with my silk..."
    ])

    $ world.battle.show_skillname("Tarantula Torture")
    play sound "audio/se/ero_ito.ogg"
    show kumo ct01 at topleft zorder 15 as ct
    show kumo ct02 at topleft zorder 15 as ct

    "The Tarantula Girl shoots spider silk on Luka! {w}{nw}"


    $ world.battle.cry(extend, [
        "\nSpider silk wraps around Luka's penis!",
        "\nThe sticky spider silk gives a strange pleasure to Luka!",
        "\nThe spider silk wraps tightly around Luka's penis!",
        "\nThe sticky silk gives a strange pleasure to Luka!",
        "\nA strange pleasure fills Luka as the silk covers his penis!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (27, 31), 2: (40, 46), 3: (54, 62)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        renpy.hide("ct")

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label taran_a6:
    world.battle.enemy[0].name "I'll give you some of my exceptionally thick silk..."

    $ world.battle.show_skillname("Viscous Silk Preparation")

    "The Tarantula Girl's spinneret takes careful aim at Luka!"

    $ world.battle.stage[0] = 1
    $ world.battle.hide_skillname()

    if world.party.player.wind == 1:
        return

    l "The world.party.player.wind feels strange..."
    sylph "Luka... Call me..."
    "I faintly hear Sylph's voice on the world.party.player.wind."

    return


label taran_a7:
    $ world.battle.stage[0] = 0

    if not world.party.player.surrendered:
        world.battle.enemy[0].name "I'll cover you with my best silk, then flood you with pleasure..."

    $ world.battle.show_skillname("Viscous Silk Spray")
    play sound "audio/se/ero_ito.ogg"

    "Spider silk shoots out of the Tarantula Girl's spinneret!{w}{nw}"

    $ world.battle.enemy[0].sprite[3] = "taran h2"
    $ world.party.player.bind = 2
    $ world.party.player.status_print()

    extend "{w=.7}\nLuka's body is covered in sticky spider silk!"

    $ world.battle.enemy[0].damage = (15, 19)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h5")

    l "Ahhhh!"
    "The viscous silk covers my body in an instant.{w}\nWith it all over me, I'm unable to move even a finger."
    world.battle.enemy[0].name "Haha... How do you like my silk?{w}\nSqueezing you nice and tight while stopping your movements..."
    world.battle.enemy[0].name "Now, shall I violate you?{w}\nYou don't have a choice...{w} I'll wring everything from you...{w} Hahaha!"

    $ world.battle.skillcount(1, 3200)
    $ world.battle.show_skillname("Tarantula Rape")
    play sound "audio/se/ero_pyu4.ogg"
    show taran ct01 at right zorder 15 as ct
    $ world.party.player.bind = 3

    "The Tarantula Girl slowly forces Luka's penis into her spinneret!"

    $ world.battle.enemy[0].damage = (15, 19)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    "Luka is being raped by the Tarantula Girl!"

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h6")

    l "Ahhhh!!"
    "I'm instantly attacked by her sticky insides, with small bumps and creases wrapping around me."
    "But it wasn't just her flesh wrapping around my penis, I was forced into a giant ball of her sticky silk inside of her."
    world.battle.enemy[0].name "Well? How is my spinneret?{w}\nIt isn't used for mating, but it still should feel good..."
    world.battle.enemy[0].name "It's more than enough for a pathetic man like you."

    jump taran_a8


label taran_a8:
    "Luka is being raped by the Tarantula Girl!"

    $ world.battle.skillcount(1, 3200)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Surrender to the pleasure...",
        "I'll tighten, and force it out...",
        "You'll love my silk...",
        "I'll play with the weak point...",
        "Can you endure it if I do this?.."
    ])

    $ world.battle.show_skillname("Tarantula Rape")
    play sound "audio/se/ero_pyu4.ogg"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "The Tarantula Girl moves, forcing the bumps inside of her to rub against Luka!",
        "The Tarantula Girl tightens around Luka's penis!",
        "The sticky silk inside the Tarantula Girl gives a strange pleasure to Luka!",
        "The flesh around the tip of Luka's penis tightens!",
        "The Tarantula Girl starts to contract around Luka's penis in waves!"
    ])

    python:
        world.battle.enemy[0].damage = (35, 41)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        renpy.hide("ct")

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h7")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    jump taran_a8


label taran_a9:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Do you want to be crushed by my breasts?..",
        "I'll crush your penis with my breasts...",
        "I'll crush you with my breasts..."
    ])

    $ world.battle.show_skillname("Breast Pressure Hell")
    play sound "audio/se/ero_makituki5.ogg"

    "Luka's entire body is pressed under the Tarantula Girl's breasts!{w}\nLuka's penis is crushed between her breasts!"

    $ world.battle.skillcount(1, 3176)

    python:
        world.battle.enemy[0].damage = {1: (24, 28), 2: (35, 41), 3: (47, 55)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        renpy.hide("ct")

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h7")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label taran_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Так ты хочешь стать моим обедом?{w}\nХорошо... но для начала я вдоволь наиграюсь с тобой..."

    $ world.battle.enemy[0].attack()

label taran_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Tarantula Caress"
    $ list2 = "Tarantula Blowjob"
    $ list3 = "Breast Pressure Hell"
    $ list4 = "Restrained Tarantula Caress"
    $ list5 = "Tarantula Torture"
    $ list6 = "Tarantula Rape"

    if persistent.skills[3196][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3197][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3176][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3198][0] > 0:
        $ list4_unlock = 1

    if persistent.skills[3199][0] > 0:
        $ list5_unlock = 1

    if persistent.skills[3200][0] > 0:
        $ list6_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump taran_onedari1
    elif world.battle.result == 2:
        jump taran_onedari2
    elif world.battle.result == 3:
        jump taran_onedari6
    elif world.battle.result == 4:
        jump taran_onedari3
    elif world.battle.result == 5:
        jump taran_onedari4
    elif world.battle.result == 6:
        jump taran_onedari5
    elif world.battle.result == -1:
        jump taran_main


label taran_onedari1:
    call onedari_syori
    $ world.battle.face(2)

    world.battle.enemy[0].name "You want to be exhausted by my legs?{w}\nThen I'll let you reach your end as you desire..."

    while True:
        call taran_a1


label taran_onedari2:
    call onedari_syori
    $ world.battle.face(2)

    world.battle.enemy[0].name "You want to be exhausted by my mouth?{w}\nThen I'll let you reach your end as you desire..."

    while True:
        call taran_a2


label taran_onedari3:
    call onedari_syori_k
    $ world.battle.face(2)

    world.battle.enemy[0].name "You want me to play with you after restraining you?{w}\nYou're quite a masochist..."

    if not world.party.player.bind:
        call taran_a3

    while True:
        call taran_a4


label taran_onedari4:
    call onedari_syori_k
    $ world.battle.face(2)

    world.battle.enemy[0].name "You want my silk to torture your penis?{w}\nDesiring something like that... You're quite a pervert."

    if not world.party.player.bind:
        call taran_a3

    while True:
        call taran_a5


label taran_onedari5:
    call onedari_syori_k
    $ world.battle.face(2)

    world.battle.enemy[0].name "You want to be violated by me?..{w}\nThen I'll squeeze out your semen before I eat you..."

    call taran_a7


label taran_onedari6:
    call onedari_syori
    $ world.battle.face(2)

    world.battle.enemy[0].name "You want to be crushed by my breasts?{w}\nThen I'll let you reach your end as you desire..."

    while True:
        call taran_a9


label taran_h1:
    $ world.party.player.moans(1)
    with syasei1
    show taran bk04 zorder 10 as bk
    $ world.ejaculation()

    "Caressed by all eight of her legs, Luka finally ejaculated."

    $ world.battle.lose()

    $ world.battle.count([28, 4])
    $ bad1 = "Luka succumbed to the Tarantula Girl's eight-legged caress."
    $ bad2 = "Luka's semen was continually squeezed out until his death."
    $ world.battle.face(2)

    world.battle.enemy[0].name "You shot it out...{w}\nWas my caress too much for you?"

    jump taran_h


label taran_h2:
    $ world.party.player.moans(1)
    with syasei1
    show taran bk01 zorder 10 as bk
    $ world.ejaculation()

    "Sucked by the Tarantula Girl, Luka finally ejaculated."

    $ world.battle.lose()

    $ world.battle.count([4, 4])
    $ bad1 = "Luka succumbed to the Tarantula Girl's blowjob."
    $ bad2 = "Luka's semen was continually squeezed out until his death."
    $ world.battle.face(2)

    world.battle.enemy[0].name "You shot it out...{w}\nWas my mouth too much for you?"

    jump taran_h


label taran_h3:
    $ world.party.player.moans(1)
    with syasei1
    $ world.ejaculation()

    "Caressed by all eight of her legs, Luka finally ejaculated."

    $ world.battle.lose()

    $ world.battle.count([28, 4])
    $ bad1 = "Bound by the Tarantula Girl, Luka was forced to come by her eight-legged caress."
    $ bad2 = "Luka's semen was continually squeezed out until his death."

    world.battle.enemy[0].name "Haha... While unable to move, you still came.{w}\nHow miserable..."

    jump taran_h


label taran_h4:
    $ world.party.player.moans(1)
    with syasei1
    show kumo ct03 at topleft zorder 15 as ct
    $ world.ejaculation()

    "With the sticky silk being sprayed on his penis, Luka was forced to come."

    $ world.battle.lose()

    $ world.battle.count([20, 4])
    $ bad1 = "Luka succumbed to the Tarantula Girl's torture."
    $ bad2 = "Luka's semen was continually squeezed out until his death."
    show kumo ct04 at topleft zorder 15 as ct

    world.battle.enemy[0].name "Letting it out while I cover your penis with silk...{w}\nHow miserable..."

    hide ct
    jump taran_h


label taran_h5:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Oh?.."

    with syasei1
    $ world.ejaculation()

    "As soon as the viscous silk covered Luka's body, he accidentally ejaculated."

    $ world.battle.lose()

    $ persistent.count_bouhatu += 1
    $ world.battle.count([20, 4])
    $ bad1 = "As soon as the silk touched him, Luka accidentally ejaculated."
    $ bad2 = "Luka's semen was continually squeezed out until his death."

    world.battle.enemy[0].name "Letting it out as soon as the silk touches you...{w}\nYou're pathetic."

    jump taran_h


label taran_h6:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Oh?.. How fast..."

    with syasei1
    show taran h3
    show taran ct02 at right zorder 15 as ct
    $ world.ejaculation()

    "As soon as Luka was inserted into the Tarantula Girl's spinneret, he accidentally ejaculated."

    $ world.battle.lose()

    $ persistent.count_bouhatu += 1
    $ world.battle.count([2, 4])
    $ bad1 = "Luka accidentally ejaculated as soon as he was inserted into the Tarantula Girl's spinneret."
    $ bad2 = "Luka's semen was continually squeezed out until his death."

    world.battle.enemy[0].name "Coming just from insertion...{w}\nYou're pathetic."
    world.battle.enemy[0].name "But the real thing starts now..."
    "The Tarantula Girl forces me all the way inside of her!"

    jump taran_h


label taran_h7:
    $ world.party.player.moans(2)
    with syasei1
    show taran h3
    show taran ct02 at right zorder 15 as ct
    $ world.ejaculation()

    "While being raped by the Tarantula Girl, Luka was forced to come."

    $ world.battle.lose()

    $ world.battle.count([2, 4])
    $ bad1 = "While being raped by the Tarantula Girl, Luka was forced to come."
    $ bad2 = "Luka's semen was continually squeezed out until his death."

    world.battle.enemy[0].name "You let everything out inside of me...{w}\nDid you enjoy it?"
    world.battle.enemy[0].name "More... I'll drown you in more pleasure..."
    "The Tarantula Girl forces me all the way inside of her!"

    jump taran_h


label taran_h8:
    $ world.party.player.moans(1)
    with syasei1
    show taran bk05 zorder 10 as bk
    $ world.ejaculation()

    "While being crushed by her breasts, Luka is forced to ejaculate!"

    $ world.battle.lose()

    $ world.battle.count([7, 4])
    $ bad1 = "With his whole body crushed by her breasts, Luka was forced to come."
    $ bad2 = "Luka's semen was continually squeezed out until his death."
    $ world.battle.face(2)

    world.battle.enemy[0].name "You shot it out...{w}\nWere my breasts too much for you?"

    jump taran_h


label taran_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.party.player.bind == 3:
        play hseanwave "audio/se/hsean12_innerworks_c2.ogg"
    else:
        "While giggling, the Tarantula Girl turns her spinneret towards me...{w}{nw}"

        play sound "audio/se/ero_ito.ogg"

        extend "{w=.7}\n...And shoots her sticky spider silk all over my body!"
        l "Uwaa!"
        "The sticky silk quickly entangles my entire body.{w}\nThe frighteningly powerful silk doesn't even let me move a finger."
        "Even more, the sticky silk felt slightly soft and wet.{w}\nIt's as if I was stuck inside a giant tongue, tightly squeezing me."
        world.battle.enemy[0].name "How does my silk feel?{w}\nAre you struggling in bliss at the pleasure of being completely bound?"
        world.battle.enemy[0].name "Stuck like that, I'll violate you...{w}\nI'll suck out everything from you..."

        hide ct
        hide bk
        show taran h2

        "The Tarantula Girl presses her abdomen into me."
        l "Aaaa..."

        play hseanwave "audio/se/hsean12_innerworks_c2.ogg"
        show taran ct01 at right zorder 15 as ct

        "She slowly forces me into her spinneret."
        "At first, the tightness inside of her was too much.{w}\nBut as I'm forced deeper inside, the feelings slowly started to change."

    l "Ahhh!!"
    "Entangled inside of her, it felt surprisingly cool.{w}\nThe silk inside of her started to move around as it stuck to my penis."
    "More and more of the silk is wrapped tighter around me."
    world.battle.enemy[0].name "Does my spinneret feel good?{w}\nInside of it, I can move the silk as I want..."
    "The Tarantula Girl starts to move as she sneers at me."
    "I can feel the bumps and creases of her flesh through the silk rubbing against me with every movement."
    l "Auuuu!!"
    "A muscle is poking me like a finger right on the back of my penis.{w}\nEvery movement forces it to poke into me through the silk, rubbing me up and down."
    "In addition, tiny bumps cover the walls of her spinneret, each one pushing through the spider silk, forcing a strange pleasure into me."
    "The strange stimulation forced into me by this strange hole causes my body to shiver."
    "The complex combinations of stimulations forces me to the edge of another orgasm."
    l "Stop... Stop this...{w}\nOr... I'm going to come again...!"
    world.battle.enemy[0].name "Every man that I catch like this gives in eventually...{w}\nI'll suck you dry as well, just like the others..."
    "This combination of bumps, creases and sticky silk being forced into me by this insect monster is bringing me a pleasure unable to be duplicated by a human female."
    "Finally surrendering to this bizarre stimulation, I give in to the desire to release."
    l "Ahhh!!"

    with syasei1
    show taran h3
    show taran ct02 at right zorder 15 as ct
    $ world.ejaculation()

    "Wrapped in her silk inside of her spinneret, I ejaculate.{w}\nAs the sweet feelings of the orgasm come over me, they mix with the disgrace of being forced to give in like this."
    "My body trembles as my semen shoots out into this insect..."
    world.battle.enemy[0].name "Ah... You put out quite a lot.{w}\nEnjoying being raped by a spider so much...{w} Shall I let you taste more?"
    l "Eh? Ah!{w} Ahhh!!"
    "The entrance to her hole sharply tightens up around the base of my penis.{w}\nNow fixed at the root, I can't even move inside of her."
    "Stuck like that, her insides start to slowly shrink."
    l "Auu... What are you doing?.."
    world.battle.enemy[0].name "Didn't I just say it?..{w}\nYou're enjoying being raped by a spider so much, that I'll let you taste more..."
    "The next moment after saying that...{w}{nw}"

    play hseanwave "audio/se/hsean13_innerworks_c3.ogg"

    extend "\nShe sprays more sticky silk all over my penis!"
    l "Aiiee!"
    "The moist, sticky silk coils tightly around me.{w}\nMy waist shakes at the unexpected pleasure attacking me."
    "But completely stuck inside of her, there's no escape from it."
    world.battle.enemy[0].name "Every piece of silk that covers you leads you closer to death...{w}\nI'll spray more... Once you're covered too much, you won't even be able to pull out of me even if I loosened up..."
    l "Haa!!"
    "She sprays out more sticky silk over me, wrapping me even tighter.{w}\nMy body shakes pathetically as I'm attacked by the stimulation."
    "The silk tightens around me as I feel more and more of it pile on.{w}\nIt's as if the soft silk is making my penis even larger inside of her, as the creases and bumps from her fleshy walls push even harder into me."
    world.battle.enemy[0].name "How is it?{w} Having your proof of manhood covered in my silk?{w}\nIs it miserable?{w} Or did you stop caring because it feels too good?"
    l "Auu...{w} Please... Stop..."
    world.battle.enemy[0].name "No.{w}\nJust for that, I'll give you even more..."
    "More of her silk is sprayed on my penis, as the squeezing sensation only increases."
    l "Hauuu!"
    "She pauses for a few seconds, then starts again and sprays even more.{w}\nRolling up my penis tighter and tighter, I can already feel that there's no way to get out of her."
    l "Auuu... The silk... Too good..."
    world.battle.enemy[0].name "...I see, so it does feel good.{w}\nThen, shall I force you to come now?"
    "Aiming right at the tip of my penis, more silk sprays out inside of her.{w}\nThe moist sticky silk wraps tighter around my tip.{w}\nUnable to take it any longer, it finally sends me over the limit."
    l "Ahhh!!"

    with syasei1
    show taran h4
    show taran ct03 at right zorder 15 as ct
    $ world.ejaculation()

    "The semen forces a hole through the silk as all of it pours into the Tarantula Girl's spinneret."
    world.battle.enemy[0].name "Mmm... Delicious male extract...{w}\nBut play time is over."
    world.battle.enemy[0].name "I'm hungry, so I'm going to suck you dry now..."
    "In the next moment, her spinneret starts to violently wriggle!"
    l "Ahhh!!"
    "As if in a whirlpool, the silk swirls and rotates around me.{w}\nShrinking around me, the bumps and creases inside of her are massaging me in a circle."
    world.battle.enemy[0].name "If I do this, there's no way you can hold out...{w}\nJust like this, I'll squeeze every drop out of you!"
    l "Ah! Auu!{w} Auuu!!"
    "With all of the features of her spinneret amplified by the silk covering me, I'm quickly brought to another height in her rotating whirlpool."
    l "Ah...Ahhh!!"

    with syasei1
    show taran h5
    show taran ct04 at right zorder 15 as ct
    $ world.ejaculation()

    "I yield right away to the building desire to release, as I give all of my semen to her mixer..."
    l "Stop... This..."
    world.battle.enemy[0].name "I won't stop.{w}\nI'm going to suck you dry in here."
    world.battle.enemy[0].name "Are you scared?{w} Are you mortified?{w}\nOr does it feel too good for you to care?"
    l "Ahhh!!"

    with syasei1
    $ world.ejaculation()

    "No matter how much I try, I can't endure the stimulation.{w}\nStuck in her spinneret, I'm forced to orgasm."
    "Completely bound by her spider silk, I can't even move my body.{w}\nCompletely at her mercy, there's no escape from this..."
    l "Auu... I'm coming again..."

    with syasei1
    $ world.ejaculation()

    "Inside of her pleasure mixer, more of my semen is forced out.{w}\nEvery time I ejaculate in her, I can feel my body weaken."
    l "Stop please...{w} Any more than this and..."
    world.battle.enemy[0].name "If you don't want to die, all you have to do is endure it.{w}\nBut that might be impossible..."
    l "Ahhhh..."

    with syasei1
    $ world.ejaculation()

    "While the Tarantula Girl laughs at me, I'm forced to come again.{w}\nWith it already as if I was out of semen, it's like my energy is shooting out instead now."
    l "Auuu..."
    "With my strength rapidly vanishing, my vision starts to go hazy.{w}\nThe pleasure being forced into me is making my mind and body melt..."

    with syasei1
    $ world.ejaculation()

    world.battle.enemy[0].name "More... Shoot out more...{w}\nWhile tasting heaven, shrivel up until nothing is left..."
    l "Ahh..."

    with syasei1
    $ world.ejaculation()

    "Stuck in pleasure, my life is sucked out.{w}\nSqueezing out everything, my body is left dry."

    with syasei1
    $ world.ejaculation()

    "Feeling my body get colder, a final orgasm comes over me.{w}\nFinally, my consciousness slowly fades into nothingness."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    world.battle.enemy[0].name "...There's nothing left.{w}\nYou were delicious."
    "With all of my body fluids sucked up, my shriveled body was left there.{w}\nThus another victim of the Tarantula Girl met their end..."
    "..................."

    jump badend

label mino_start:
    python:
        world.troops[25] = Troop(world)
        world.troops[25].enemy[0] = Enemy(world)

        world.troops[25].enemy[0].name = Character("Девушка-минотавр")
        world.troops[25].enemy[0].sprite = ["mino st01", "mino st02", "mino st03", "mino st02"]
        world.troops[25].enemy[0].position = center
        world.troops[25].enemy[0].defense = 100
        world.troops[25].enemy[0].evasion = 95
        world.troops[25].enemy[0].max_life = world.troops[25].battle.difficulty(1200, 1500, 1800)
        world.troops[25].enemy[0].power = world.troops[25].battle.difficulty(0, 1, 2)

        world.troops[25].battle.tag = "mino"
        world.troops[25].battle.name = world.troops[25].enemy[0].name
        world.troops[25].battle.background = "bg 019"
        world.troops[25].battle.music = 1
        world.troops[25].battle.exp = 1600
        world.troops[25].battle.exit = "lb_0046"

        world.troops[25].battle.init()

    world.battle.enemy[0].name "Ох-хо... Здоровый мужчинка.{w}\nЯ тебя изнасилую."
    l "... Сгинь."
    $ world.party.player.skill16(say=False)

label mino_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Что за?!.."

    play sound "se/syometu.ogg"
    hide mino with crash

    "Девушка-минотавр превратилась в телёнка!"

    $ world.battle.victory(1)

label sasori_start:
    python:
        world.troops[26] = Troop(world)
        world.troops[26].enemy[0] = Enemy(world)

        world.troops[26].enemy[0].name = Character("Девушка-скорпион")
        world.troops[26].enemy[0].sprite = ["sasori st01", "sasori st02", "sasori st03", "sasori h1"]
        world.troops[26].enemy[0].position = center
        world.troops[26].enemy[0].defense = 70
        world.troops[26].enemy[0].evasion = 95
        world.troops[26].enemy[0].max_life = world.troops[26].battle.difficulty(1100, 1100, 1500)
        world.troops[26].enemy[0].power = world.troops[26].battle.difficulty(0, 1, 2)

        world.troops[26].battle.tag = "sasori"
        world.troops[26].battle.name = world.troops[26].enemy[0].name
        world.troops[26].battle.background = "bg 062"
        world.troops[26].battle.music = 1
        world.troops[26].battle.exp = 1700
        world.troops[26].battle.exit = "lb_0047"

        world.troops[26].battle.init()

    world.battle.enemy[0].name "Мужчина проходящий мимо этого места?.. Как необычно.{w}\nПохоже, что у меня сегодня будет перекус человеком..."
    l "Просто уходи... Я не позволю этому произойти."
    "Игнорируя мои слова, она встала в боевую стойку.{w}\nПохоже, что она уже настроилась на бой..."

    $ world.battle.main()

label sasori_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()


label sasori_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Я проиграла... человеку?!"

    play sound "se/syometu.ogg"
    hide sasori with crash

    "Девушка-скорпион превратилась в крошечного скорпиона!"

    $ world.battle.victory(1)

label sasori_a:
    if world.party.player.bind == 1:
        call sasori_a5
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,4)

        if random == 1 and world.party.player.status == 0 and world.battle.delay[0] > 3:
            call sasori_a3
            $ world.battle.main()
        elif random == 2:
            call sasori_a1
            $ world.battle.main()
        elif random == 3:
            call sasori_a2
            $ world.battle.main()
        elif random == 4 and world.battle.delay[1] > 3:
            call sasori_a4
            $ world.battle.main()


label sasori_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll make you feel good with my mouth...",
        "I'll slowly suck everything out...",
        "I'll suck out your male extract..."
    ])

    $ world.battle.show_skillname("Scorpion Blowjob")
    play sound "audio/se/ero_buchu2.ogg"

    "The Scorpion Girl intensely sucks on Luka's penis!{w}{nw}"

    $ world.battle.skillcount(1, 3206)

    python:
        world.battle.enemy[0].damage = {1: (25, 30), 2: (33, 41), 3: (46, 56)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label sasori_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll love you with my breasts.",
        "Hehe... Aren't my breasts soft?",
        "Pour out your extract on my chest..."
    ])

    $ world.battle.show_skillname("Scorpion Tit Fuck")
    play sound "audio/se/ero_koki1.ogg"

    "The Scorpion Girl crushes Luka's penis between her breasts!{w}{nw}"

    $ world.battle.skillcount(1, 3207)

    python:
        world.battle.enemy[0].damage = {1: (28, 32), 2: (38, 44), 3: (52, 60)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label sasori_a3:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "I'll make you numb with my venom..."

    $ world.battle.show_skillname("Venom Tail")

    "The Scorpion Girl's tail thrusts toward Luka!{w}{nw}"

    $ world.battle.skillcount(1, 3208)

    extend "{w=.7}\nVenom is injected into Luka, causing him to go numb!"

    $ world.party.player.status = 2
    $ world.party.player.status_print()

    "Luka has been paralyzed!"

    $ world.battle.hide_skillname()

    l "Uuu..."
    world.battle.enemy[0].name "You can't move anymore...{w}\nNow, let me squeeze out your extract..."

    $ world.party.player.status_turn = rand().randint(2,3)

    if persistent.difficulty == 2:
        $ world.party.player.status_turn = rand().randint(3,4)
    elif persistent.difficulty == 3:
        $ world.party.player.status_turn = rand().randint(4,5)

    return


label sasori_a4:
    play sound "audio/se/ero_pyu2.ogg"
    show sasori st11

    world.battle.enemy[0].name "I'll suck you into this tail."
    world.battle.enemy[0].name "Doesn't it look like it will feel good?{w}\nDon't you want it to suck up everything till the last drop?"

    $ world.battle.show_skillname("Climb")

    "The Scorpion Girl climbs on top of Luka's body!{w}{nw}"

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    extend "{w=.7}\nLuka's pinned under the Scorpion Girl!"

    $ world.battle.hide_skillname()
    $ world.battle.face(2)

    if world.battle.first[0] == 1:
        world.battle.enemy[0].name "This time I won't let you escape..."
    else:
        world.battle.enemy[0].name "It will feel amazing to be sucked dry by this tail...{w}\nYou'll die in a state of bliss."

    $ world.battle.first[0] = 1
    $ world.battle.enemy[0].power = 0
    return


label sasori_a5:
    "Luka is being held down by the Scorpion Girl..."
    world.battle.enemy[0].name "Give up, and just enjoy your final moments..."

    $ world.battle.skillcount(1, 3209)
    $ world.battle.show_skillname("Petra Scorpio")
    play sound "audio/se/ero_pyu4.ogg"
    show sasori ct01 at topleft zorder 15 as ct

    "The Scorpion Girl's tail sucks in Luka's penis!"

    $ world.party.player.bind = 2
    $ world.battle.enemy[0].damage = (40, 47)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    "Luka is being raped by the Scorpion Girl's tail!"
    l "Ahh!!"
    "My body writhes as her tail's soft meat sucks in my penis.{w}\nBut with the Scorpion Girl on top of me, escape isn't possible..."

    jump sasori_a6


label sasori_a6:
    $ world.battle.skillcount(1, 3209)

    "Luka is being raped by the Scorpion Girl's tail!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Give me your extract...",
        "It feels good, right? Just surrender to it...",
        "I'll suck you dry like this...",
        "I'll even move a little...",
        "Hehe... If I stimulate the tip, are you going to leak?"
    ])

    $ world.battle.show_skillname("Petra Scorpio")
    play sound "audio/se/ero_chupa4.ogg"

    $ world.battle.cry(narrator, [
        "The Scorpion Girl's tail contracts around Luka!",
        "Mucus from the tail covers Luka's penis!",
        "The Scorpion Girl's tail squeezes Luka's penis!",
        "The tail quickly pumps Luka's penis!",
        "The flesh around the tip of Luka's penis tightens!"
    ], True)

    python:
        world.battle.enemy[0].damage = (40, 47)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    jump sasori_a6


label sasori_kousan:

    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Сдаёшься?{w}\nТогда ты будешь не против, если я тебя высосу досуха..."

    $ world.battle.enemy[0].attack()

label sasori_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Scorpion Blowjob"
    $ list2 = "Scorpion Tit Fuck"
    $ list3 = "Venom Tail"
    $ list4 = "Petra Scorpio"

    if persistent.skills[3206][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3207][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3208][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3209][0] > 0:
        $ list4_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump sasori_onedari1
    elif world.battle.result == 2:
        jump sasori_onedari2
    elif world.battle.result == 3:
        jump sasori_onedari3
    elif world.battle.result == 4:
        jump sasori_onedari4
    elif world.battle.result == -1:
        jump sasori_main


label sasori_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want to be defeated in my mouth?{w}\nAlright... I'll make sure to suck everything out."

    while True:
        call sasori_a1


label sasori_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want to be defeated by my breasts?{w}\nAlright... I'll make sure to squeeze everything out."

    while True:
        call sasori_a2


label sasori_onedari3:
    call onedari_syori

    world.battle.enemy[0].name "You want to give in to my venom?{w}\nWhat an unusual desire..."

    call sasori_a3
    jump sasori_a


label sasori_onedari4:
    call onedari_syori_k

    world.battle.enemy[0].name "You want me to squeeze everything out with my tail?{w}\nAlright... I'll let you enjoy it until your death."

    if not world.party.player.bind:
        call sasori_a4

    call sasori_a5


label sasori_h1:
    $ world.party.player.moans(1)
    with syasei1
    show sasori bk01 zorder 10 as bk
    $ world.ejaculation()

    "Unable to take the Scorpion Girl's blowjob any longer, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([4, 4])
    $ bad1 = "Luka succumbed to the Scorpion Girl's blowjob."
    $ bad2 = "After that, Luka was sucked dry by her tail."
    $ world.battle.face(2)

    world.battle.enemy[0].name "You put out a lot...{w}\nHehe... It's delicious."

    jump sasori_h


label sasori_h2:
    $ world.party.player.moans(1)
    with syasei1
    show sasori bk02 zorder 10 as bk
    $ world.ejaculation()

    "Unable to take the Scorpion Girl's tit fuck any longer, Luka finally ejaculates."

    $ world.battle.lose()

    $ world.battle.count([7, 4])
    $ bad1 = "Luka succumbed to the Scorpion Girl's tit fuck."
    $ bad2 = "After that, Luka was sucked dry by her tail."
    $ world.battle.face(2)

    world.battle.enemy[0].name "You put out a lot...{w}\nHehe... It's delicious."

    jump sasori_h


label sasori_h3:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Already?..{w} Just from insertion?"

    with syasei1
    show sasori ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    "As soon as Luka was sucked into her tail, the pleasure causes him to instantly ejaculate."

    $ world.battle.lose()

    $ persistent.count_bouhatu += 1
    $ world.battle.count([19, 4])
    $ bad1 = "Luka accidentally ejaculated as soon as he was sucked in by the Scorpion Girl's tail."
    $ bad2 = "After that, Luka was sucked dry by her tail."

    world.battle.enemy[0].name "You're so quick...{w}\nOrgasming as soon as I put it inside... How pathetic!"
    "The Scorpion Girl laughs at me as her tail continues to suck on me."


    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut
    play hseanwave "audio/se/hsean21_tentacle2.ogg"
    jump sasori_hb


label sasori_h4:
    $ world.party.player.moans(2)
    with syasei1
    show sasori ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Raped by the Scorpion Girl's tail, Luka finally succumbed to the overpowering pleasure forced into him."

    $ world.battle.lose()

    $ world.battle.count([19, 4])
    $ bad1 = "Luka succumbed to the Scorpion Girl's petra scorpio."
    $ bad2 = "After that, Luka was sucked dry by her tail."

    world.battle.enemy[0].name "You put out a lot...{w}\nHehe... It's delicious."
    "While flashing a seductive smile at me, the Scorpion Girl continues to suck on me with her tail."


    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut
    play hseanwave "audio/se/hsean21_tentacle2.ogg"
    jump sasori_hb


label sasori_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    world.battle.enemy[0].name "Next I want your semen in here..."

    play sound "audio/se/ero_pyu2.ogg"
    hide ct
    hide bk
    show sasori st11

    "The tip of the Scorpion Girl's tail turns into another shape.{w}\nIt doesn't look anything like the needle it did before..."
    "That shape... It's pretty obvious what she's going to do..."
    world.battle.enemy[0].name "I'm going to suck your penis into my tail..."
    world.battle.enemy[0].name "Doesn't it look like it will feel incredible?{w}\nDon't you want it to suck out every drop of your extract?"
    l "St...Stop..."
    "I tremble with horror as I see the flesh inside of her tail wriggle around."
    "The fear and expectation of having my penis in there boils up in me."
    world.battle.enemy[0].name "Hehe... Just give in to the pleasure as I suck you dry."

    show sasori h1

    "While flashing a seductive smile at me, the Scorpion Girl's tail slowly moves closer to me."
    "Finally, her wriggling tail-hole is right next to my penis."
    l "No!{w} Stop!"

    play hseanwave "audio/se/hsean21_tentacle2.ogg"
    show sasori ct01 at topleft zorder 15 as ct

    "In an instant, her tail sucks in my penis!{w}\nEncased in her soft flesh, my penis is sucked all the way into her tail."
    l "Ahhh!"
    world.battle.enemy[0].name "Haha... Incredible, isn't it?{w}\nI'll force you to put all of your extract in here..."


label sasori_hb:
    "Her warm flesh wriggles around my penis.{w}\nSucked all the way inside of her tail, the sucking pleasure attacks every inch of my penis."
    "Something that felt like tongues start to rub and lick the tip of my penis."
    "The amazing stimulation inside of her quickly forces me to the edge of another orgasm."
    l "Incredible...{w} Inside... Too good...{w}\nAhhh!!"
    "When the tongues inside of her tail gently lick the tip, I let out a yell.{w}\nSucked, licked, and squeezed inside of her tail, I feel myself quickly giving in to the desire to release."
    world.battle.enemy[0].name "Shoot out a lot...{w}\nPour all of your male extract into me..."
    "Her tail starts to tighten up around me.{w}\nAs the pressure increases, I can feel the details of her tail even clearer."
    "The feel of her insect shell through her soft flesh rubs and massages against my penis with every movement."
    "Unable to hold on, I quickly give in to the overpowering stimulation."
    l "Ahhh!!"

    with syasei1
    show sasori h2
    show sasori ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Forced to orgasm, I explode inside of her, filling the Scorpion Girl's tail with my semen."
    "As if urged on by that, a powerful suction attacks me inside her tail, forcing out every drop still inside my urethra."
    l "Faaa..."
    world.battle.enemy[0].name "Giving me so much... Was it that good?{w}\nBut don't worry, I'll let you feel more... Until you're shriveled and dry, I won't stop."
    "The flesh inside of her tail keeps wriggling around me."
    "The tail tightens up around me, then slowly loosens.{w}\nFrom the base to the tip, the rhythmic stimulation attacks me."
    "At the same time, the tongues inside of her continue to lick the tip of my penis."
    l "Auu... Incredible..."
    "This tail must have evolved to suck out a man's semen...{w}\nOnce I was forced inside, there was no way to fight against it."
    "Caught by the Scorpion Girl, I can't do anything but be slowly sucked dry..."
    world.battle.enemy[0].name "Hehe... I'll make it even better.{w}\nThis is what you men desire, isn't it?{w}\nSticking your penis into something, and being sucked dry..."
    "Her tail tightens up around me, with her soft flesh squeezing tightly around me."
    "There's no resistance in the face of this tight, wriggling tail...{w}\nAs if rejoicing at the pleasure from her tail, my dick starts to twitch."

    with syasei1
    show sasori h3
    show sasori ct03 at topleft zorder 15 as ct
    $ world.ejaculation()

    l "Auuu..."
    "While laying here helpless, more semen is sucked out of me.{w}\nPowerless to stop her, an overwhelming sense of defeat comes over me as she sucks out my semen for food."
    "But this one-sided feeding isn't about to end...{w}\nRight after my last ejaculation finished, she's already working towards the next."
    l "Aaa... Stop... Please...!"
    world.battle.enemy[0].name "I won't stop... Not until there's nothing left."
    l "No way... Ahhh!"
    "The entrance to her tail narrows as she tightens around me again.{w}\nWith her soft wriggling flesh tightly wrapped around me, I'm forced to another height."
    l "Aaaa..."

    with syasei1
    show sasori h4
    show sasori ct04 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Before I can voice another word of protest, she had already forced me to another orgasm."
    "Filled with mixing feelings of pleasure and disgrace, my body shakes."
    world.battle.enemy[0].name "Hahahaha... It's too good for you, isn't it?{w}\nBut there's no escape from my tail.{w}\nAll that's left of your short life is to be sucked dry by me..."
    l "Auu...{w} No... Help me..."
    "But no matter how much I plead, her tail doesn't stop.{w}\nThis hateful tail is going to suck the life out of me..."
    l "Help... Stop..."
    world.battle.enemy[0].name "Hehe... Pitiful prey.{w}\nNo matter how much you cry, I'm going to suck everything up."
    world.battle.enemy[0].name "Why don't you just give in to the pleasure, so you can at least die happy?"
    l "Ahhh..."

    with syasei1
    $ world.ejaculation()

    "I ejaculate again from the pleasure being forced into me.{w}\nMy semen is being squeezed out only for the purpose of feeding her..."
    "Exhausted from my orgasms, I'm too weak to even attempt to move my body."
    world.battle.enemy[0].name "Shoot out more...{w}\nI'll send you to heaven while my tail sucks everything from you..."
    l "A...aa..."

    with syasei1
    $ world.ejaculation()

    "Each orgasm causes my thoughts to become even more muddled.{w}\nUnable to think clearly, I finally give in to the pleasure."
    world.battle.enemy[0].name "It's about time to end this...{w}\nYou were very delicious... Hahaha!"
    l "Aaa..."

    with syasei1
    $ world.ejaculation()

    "With a final orgasm, I finally lose consciousness.{w}\nSucked dry, I fall into an endless sleep."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Thus, I was sucked dry by the Scorpion Girl.{w}\nThe only proof of my existence is another dried up corpse in the desert."
    "................"

    jump badend

label sandw_start:
    python:
        world.troops[27] = Troop(world)
        world.troops[27].enemy[0] = Enemy(world)

        world.troops[27].enemy[0].name = Character("Песчаный червь")
        world.troops[27].enemy[0].sprite = ["sandw st01", "sandw st02", "sandw st03", "sandw h3"]
        world.troops[27].enemy[0].position = center
        world.troops[27].enemy[0].defense = 100
        world.troops[27].enemy[0].evasion = 100
        world.troops[27].enemy[0].max_life = world.troops[27].battle.difficulty(2500, 3125, 3750)
        world.troops[27].enemy[0].bind_maxhp = world.troops[27].battle.difficulty(280, 300, 560)
        world.troops[27].enemy[0].power = world.troops[27].battle.difficulty(0, 1, 2)

        world.troops[27].battle.tag = "sandw"
        world.troops[27].battle.name = world.troops[27].enemy[0].name
        world.troops[27].battle.background = "bg 076"
        world.troops[27].battle.music = 1
        world.troops[27].battle.exp = 4000
        world.troops[27].battle.exit = "lb_0048"

        world.troops[27].battle.init()

    l "Эта хрень..."
    "Даже сейчас, она выглядит ну очень странно.{w}\nКак она вообще размножается?.."
    world.battle.enemy[0].name "Человек...{w}\nПозволь мне тебя засосать..."
    l "................"

    $ world.battle.main()

label sandw_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggled_attack(80)
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2:
            world.battle.common_struggle()
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label sandw_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Так... больно..."

    play sound "se/syometu.ogg"
    hide sandw with crash

    "Гигантская девушка-червь превращается в маленького песчаного червя!"

    $ world.battle.victory(1)

label sandw_kaihou:
    world.battle.enemy[0].name "Гах..."

    play sound "audio/se/dassyutu.ogg"
    $ renpy.show(world.battle.enemy[0].sprite[2], at_list=[world.battle.enemy[0].position])
    $ world.party.player.bind = 0
    $ world.party.player.status_print()

    "Шквал атак заставляет Песчаного Червя выплюнуть Луку!"

    $ world.battle.face(1)

    $ world.battle.main()

label sandw_a:
    if world.party.player.bind == 1 and world.battle.enemy[0].bind_hp < 0:
        jump sandw_kaihou

    if world.party.player.bind == 1:
        $ random = rand().randint(1,3)

        if random == 1:
            call sandw_a3
            $ world.battle.main()
        elif random == 2:
            call sandw_a4
            $ world.battle.main()
        elif random == 3:
            call sandw_a5
            $ world.battle.main()

    while True:
        $ random = rand().randint(1,2)

        if random == 1:
            call sandw_a1
            $ world.battle.main()
        elif random == 2 and world.battle.delay[0] > 3:
            call sandw_a2
            $ world.battle.main()


label sandw_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "..........",
        ".........?"
    ])

    $ world.battle.cry(world.battle.enemy[0].name, [
        "The Sandworm stares at nothing in particular!",
        "The Sandworm looks around in confusion!",
        "The Sandworm is in a daze!"
    ])

    return


label sandw_a2:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "I want to suck on you..."

    $ world.battle.show_skillname("Mouth Hold")
    play sound "audio/se/ero_pyu2.ogg"

    "The Sandworm opens her gigantic mouth and leans toward Luka!{w}{nw}"

    extend "{w=.7}\nShe clamps down her mouth over Luka's body!"

    $ world.battle.enemy[0].damage = {1: (20, 30), 2: (30, 45), 3: (40, 60)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Luka's stuck in the Sandworm's mouth!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h1")

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    world.battle.enemy[0].name "Shoot it out..."

    $ world.battle.enemy[0].bind_hp = world.battle.enemy[0].bind_maxhp
    return


label sandw_a3:
    "Luka's stuck in the Sandworm's mouth!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "I want to taste you...",
        "Delicious...",
        "*Лижет*"
    ])

    $ world.battle.show_skillname("Lick")
    play sound "audio/se/ero_pyu1.ogg"
    show sandw ct01 at topleft zorder 15 as ct

    "The Sandworm's giant tongue licks Luka body!{w}{nw}"

    $ world.battle.skillcount(1, 3255)

    python:
        world.battle.enemy[0].damage = {1: (30, 40), 2: (45, 60), 3: (60, 80)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label sandw_a4:
    extend "{w=.7}\nLuka's stuck in the Sandworm's mouth!"

    $ world.battle.cry(world.battle.enemy[0].name, [
        "You taste good...",
        "I'll suck... So give me your semen...",
        "*Сосёт* *Сосёт*"
    ])

    $ world.battle.show_skillname("Suck")
    play sound "audio/se/ero_kyuin1.ogg"
    show sandw ct01 at topleft zorder 15 as ct

    "The Sandworm sucks on Luka's body like he's a piece of candy!{w}{nw}"

    $ world.battle.skillcount(1, 3256)

    python:
        world.battle.enemy[0].damage = {1: (35, 45), 2: (52, 67), 3: (70, 90)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label sandw_a5:
    extend "{w=.7}\nLuka's stuck in the Sandworm's mouth!"

    $ world.battle.skillcount(1, 3257)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll kiss it... So put it out...",
        "Delicious...",
        "*Целует*"
    ])

    $ world.battle.show_skillname("Kiss")
    play sound "audio/se/ero_chupa1.ogg"
    show sandw ct01 at topleft zorder 15 as ct

    "The Sandworm narrows her mouth as she makes a kissing motion on Luka's lower body!"

    python:
        world.battle.enemy[0].damage = {1: (45, 50), 2: (67, 75), 3: (90, 100)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label sandw_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Ты сдался?{w}\nТогда позволь тебя высосать..."

    if not world.party.player.bind:
        call sandw_a2

    $ world.battle.enemy[0].attack()

label sandw_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Lick"
    $ list2 = "Suck"
    $ list3 = "Kiss"

    if persistent.skills[3255][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3256][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3257][0] > 0:
        $ list3_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump sandw_onedari1
    elif world.battle.result == 2:
        jump sandw_onedari2
    elif world.battle.result == 3:
        jump sandw_onedari3
    elif world.battle.result == -1:
        jump sandw_main


label sandw_onedari1:
    call onedari_syori_k

    world.battle.enemy[0].name "You want me to lick?.."

    if not world.party.player.bind:
        call sandw_a2

    while True:
        call sandw_a3


label sandw_onedari2:
    call onedari_syori_k

    world.battle.enemy[0].name "You want me to suck?.."

    if not world.party.player.bind:
        call sandw_a2

    while True:
        call sandw_a4


label sandw_onedari3:
    call onedari_syori_k

    world.battle.enemy[0].name "You want me to kiss?.."

    if not world.party.player.bind:
        call sandw_a2

    while True:
        call sandw_a5


label sandw_h1:
    $ world.party.player.moans(3)
    with syasei1
    show sandw h3
    $ world.ejaculation()

    "The moment Luka was sucked into the Sandworm's giant mouth, he was forced to ejaculate."

    $ world.battle.lose()

    $ world.battle.count([4, 4])
    $ persistent.count_bouhatu += 1
    $ bad1 = "The moment Luka was sucked into the Sandworm's mouth, he ejaculated."
    $ bad2 = "While stuck in her mouth, Luka was sucked completely dry."
    jump sandw_h


label sandw_h2:
    $ world.party.player.moans(1)
    with syasei1
    show sandw h3
    show sandw ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Stuck in her mouth as she licks and sucks on him, Luka is forced to ejaculate."

    $ world.battle.lose()

    $ world.battle.count([4, 4])
    $ bad1 = "Licked by the Sandworm, Luka was forced to ejaculate."
    $ bad2 = "While stuck in her mouth, Luka was sucked completely dry."
    jump sandw_h


label sandw_h3:
    $ world.party.player.moans(1)
    with syasei1
    show sandw h3
    show sandw ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Stuck in her mouth as she licks and sucks on him, Luka is forced to ejaculate."

    $ world.battle.lose()

    $ world.battle.count([4, 4])
    $ bad1 = "Sucked by the Sandworm, Luka was forced to ejaculate."
    $ bad2 = "While stuck in her mouth, Luka was sucked completely dry."
    jump sandw_h


label sandw_h4:
    $ world.party.player.moans(1)
    with syasei1
    show sandw h3
    show sandw ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Stuck in her mouth as she licks and sucks on him, Luka is forced to ejaculate."

    $ world.battle.lose()

    $ world.battle.count([4, 4])
    $ bad1 = "Kissed by the Sandworm with her powerful lips, Luka was forced to ejaculate."
    $ bad2 = "While stuck in her mouth, Luka was sucked completely dry."
    jump sandw_h


label sandw_h:
    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    world.battle.enemy[0].name "Semen..."

    play hseanvoice "audio/voice/fera_zyuru2.ogg"
    hide ct
    show sandw h1

    "The Sandworm picks me out of her mouth and starts to lick the semen off my groin."
    l "Auuuu..."
    "Her slimy, gigantic tongue keeps licking me with long, slow strokes.{w}\nHer saliva covers my body as she slowly licks up all the semen."
    world.battle.enemy[0].name "Delicious..."
    l "Auuu..."
    "My body shivers as her warm tongue runs up my body."
    "Her gigantic tongue pushes into my body, as the tip keeps flicking and licking my penis."
    "Dangling helplessly in midair, I can't do anything but allow her tongue to taste my body."
    l "Ahhh..."
    world.battle.enemy[0].name "*Лижет*{w} *Лижет*"
    "Her tongue gets even more forceful as she strains to get the last drops of semen off of my penis."
    "She creates a funnel with the tip of her tip around my penis, and starts to forcefully suck on it to get out the semen still inside."
    l "Faaa... Your tongue..."
    world.battle.enemy[0].name "Mmm...{w} More semen is coming out..."
    "More pre-come is leaking out due to her pleasurable tongue.{w}\nHappy at the additional semen, she continues sucking it out with increased force."
    l "Aiee!{w} Ahhhh!!"
    world.battle.enemy[0].name "*Лижет*{w} *Сосёт*"
    "Her saliva drenches the lower half of my body as her thick tongue continues to suck on me."
    "The harshness of her tongue mixes with her sticky saliva, washing me with a sweet ecstasy."
    "In almost no time since my last orgasm, I already feel another one boiling up."
    l "Auuu...{w} I'm going to come again..."
    world.battle.enemy[0].name "*Лижет*{w} *Сосёт*"
    "As my body starts to tremble at the coming orgasm, the Sandworm's tongue starts to suck even harder in preparation."
    "With her relentless tongue attacking me, my head goes white."
    l "Ahhhh!!"

    with syasei1
    show sandw h2
    $ world.ejaculation()

    "I shoot out my semen all over her waiting tongue.{w}\nHeld in the air and licked like this, she forced me to come again..."
    l "Auuu..."
    world.battle.enemy[0].name "Mmm...{w} More came out..."
    "The Sandworm's tongue renews her licking as she makes sure to collect every drop."

    stop hseanvoice fadeout 1

    world.battle.enemy[0].name "More...{w}{nw}"

    play hseanvoice "audio/voice/fera_gutyu.ogg"
    show sandw h3
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nMmmm{w} *Сосёт*"
    l "Ah!{w} Aiee!"
    "The Sandworm gently sucked the lower half of my body into her mouth.{w}\nEverything from my waist down is stuck inside of her."
    l "Ahhhh!!"
    "My penis is forced against her slimy tongue.{w}\nThe bizarre feeling was unlike anything I could have ever imagined."
    world.battle.enemy[0].name "*Сосёт*{w} *Лижет*"

    show sandw ct01 at topleft zorder 15 as ct

    "Saliva starts to secrete inside her mouth, soaking my body in it."
    l "Auu... It's so warm..."
    world.battle.enemy[0].name "*Сосёт*{w} *Лижет*"
    "She narrows her cheeks as she starts to suck on my body.{w}\nHer sucking motions start off slow, and her tongue starts to move around the lower half of my body."
    "Unable to even see what's going on inside her mouth, the pleasure starts to put me into a trance."
    l "Aaa... That feels good..."
    world.battle.enemy[0].name "*Сосёт*{w} *Лижет*"
    "Her sucking motions cause her lips to move on my body.{w}\nMoving from my chest to my belly button, the friction brings a warming pleasure to my body as the saliva covers more of my body."
    l "Faaa... Incredible..."
    world.battle.enemy[0].name "Your penis...{w} Delicious..."
    "Her thick tongue starts to play with my penis.{w}\nShe uses her tongue to force my body onto the roof of her mouth, as she gently flicks my penis."
    l "Auu... I'm going to come again..."
    "I yell out as it feels like my lower body is melting in pleasure.{w}\nWrapped in her mouth and attacked by her tongue, there's no way I can hold on any longer..."
    world.battle.enemy[0].name "Mmmm...{w} *Лижет*{w}\nI want more of your delicious semen..."
    l "Ahhhh!!"
    "Wrapped in her tongue, I explode inside of her mouth."

    with syasei1
    show sandw h4
    show sandw ct02 at topleft zorder 15 as ct
    $ world.ejaculation()

    world.battle.enemy[0].name "Mmm...{w} *Slurrrrrp*"
    "A strong suction pulls violently at my body as the Sandworm sucks down my semen as I shoot it out."
    l "Ahh... Your sucking..."
    world.battle.enemy[0].name "*Сосёт*{w} *Сосёт*"
    "Even after she finished drinking down the last of my semen, she continues sucking on me."
    l "Aiee!{w} Ahhhh!!"
    "The upper half of my body writhes in futility as her lips tighten around my body."
    "With my lower half inside her mouth and suspended so far above the ground, there's no escape..."
    l "Haaa!{w} Ahhh!"
    "Her mouth narrows as the suction increases.{w}\nIt feels as though the entire lower half of my body is being squeezed between the roof of her mouth and her tongue."
    "As if sucking everything directly out of me by sheer force, my body relaxes as I give in again."

    with syasei1
    show sandw h5
    show sandw ct03 at topleft zorder 15 as ct
    $ world.ejaculation()

    l "Ahhh..."
    world.battle.enemy[0].name "*Сосёт*{w} *Сосёт*"
    "She fiercely sucks on my after every spurt of semen, swallowing it down as fast as it comes out."
    "Not stopping for a single moment, she quickly forces me to another ejaculation right after my last."

    with syasei1
    $ world.ejaculation()

    l "Ahhh..."
    "Unable to do anything, I finally surrender to her mouth.{w}\nWith all of my power drained, I accept being sucked by her."
    world.battle.enemy[0].name "*Сосёт*{w} *Сосёт*"
    l "Faa..."

    with syasei1
    show sandw ct04 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Each orgasm makes me feel even weaker.{w}\nAfter the repeated orgasms, I'm too exhausted to even move my head."
    "Even though I know that if she keeps sucking on me, I'll die...{w}\nI can't do anything but drown in the pleasure."
    "Even though I know it's killing me, I want to feel even more..."
    l "Aaaa..."

    with syasei1
    $ world.ejaculation()

    "A white fog envelopes me as my consciousness starts to fade.{w}\nThe Sandworm is sucking everything out of me..."
    "Until nothing's left, I'll be sucked in her mouth..."

    with syasei1
    $ world.ejaculation()
    scene bg black with Dissolve(3.0)
    stop hseanvoice fadeout 1

    world.battle.enemy[0].name "...He stopped moving..."
    "The Sandworm sucked every drop out of Luka, leaving only a shriveled up corpse."
    "On the outskirts of the desert, his discarded body is covered by the sand..."
    "....................."

    jump badend

label gnome_ng_start:
    python:
        world.troops[28] = Troop(world)
        world.troops[28].enemy[0] = Enemy(world)

        world.troops[28].enemy[0].name = Character("Гнома")
        world.troops[28].enemy[0].sprite = ["gnome st11", "gnome st13", "gnome st12", "gnome st23"]
        world.troops[28].enemy[0].position = center
        world.troops[28].enemy[0].defense = 30
        world.troops[28].enemy[0].evasion = 90
        world.troops[28].enemy[0].max_life = world.troops[28].battle.difficulty(14000, 14500, 15500)
        world.troops[28].enemy[0].henkahp[0] = 13500
        world.troops[28].enemy[0].power = world.troops[28].battle.difficulty(0, 1, 2)

        if persistent.difficulty == 3:
            world.party.player.earth_keigen = 150
        else:
            world.party.player.earth_keigen = 0

        world.troops[28].battle.tag = "gnome_ng"
        world.troops[28].battle.name = world.troops[28].enemy[0].name
        world.troops[28].battle.background = "bg 076"

        if persistent.music:
            world.troops[28].battle.music = 23
        else:
            world.troops[28].battle.music = 10

        world.troops[28].battle.ng = True
        world.troops[28].battle.exp = 1100000
        world.troops[28].battle.exit = "lb_0049"

        world.troops[28].battle.init(1)

    "Похоже, мне всё равно было не избежать этого боя."
    "Как и у Сильфа, у Гномы не должно быть реального боевого опыта.{w}{nw}"
    if world.party.player.skill[1]:
        "... Но Сильфа сыграла со мной злую шутку, запечатав силу. Что же планирует Гнома?.."
    else:
        "Это не должно быть слишком сложным!"
    world.battle.enemy[0].name "............................"
    play sound "se/earth2.ogg"
    $ world.battle.enemy[0].earth = 1
    $ world.battle.enemy[0].earth_turn = -1
    "Гнома взывает к силе земли!{w}\nЗащита Гномы сильно увеличена!"
    $ world.party.player.skill_set(2)
    $ world.battle.main()

label gnome_ng_main:
    python:
        if not world.party.player.bind:
            renpy.hide("doll1")
            renpy.hide("doll2")
            renpy.hide("doll3")
            renpy.hide("doll4")

        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1 and world.battle.stage[0]:
            world.battle.common_attack()
        elif world.battle.result == 1 and world.battle.stage[0] < 1:
            renpy.jump(f"{world.battle.tag}_001")
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label gnome_ng_001:
    if world.party.player.wind == 3:
        $ world.party.player.attack(False)
        $ world.party.player.attack(True)
    else:
        $ world.party.player.attack()

    if world.battle.enemy[0].life < 1:
        jump gnome_ng_v

    l "... Чёрт."
    "Гнома использовала силу земли, поэтому её тело намного прочнее, чем обычно.{w}\nДолжен быть какой-то другой способ нанести ей урон..."
    $ world.battle.stage[0] = 1

    jump gnome_ng_a

label gnome_ng_v:
    hide doll1
    hide doll2
    hide doll3
    hide doll4
    show gnome st14
    gnome "!!!"
    hide gnome
    play sound "se/down.ogg"
    "Гнома падает на песок.{w}\nПохоже она всё ещё в сознании..."
    "Отлично!{w}\nДумаю, я победил!"
    $ world.battle.victory(1)

label gnome_ng_kousan:
    $ world.battle.common_surrender()

    world.battle.enemy[0].name "...?"

    $ world.battle.face(2)

    world.battle.enemy[0].name ".............."

    call gnome_ng_a6

    $ world.battle.enemy[0].attack()

label gnome_ng_a:
    if world.battle.enemy[0].defense < 30:
        $ world.battle.enemy[0].defense = 30
        call gnome_ng_earthguard2
    elif world.battle.enemy[0].defense > 30:
        $ world.battle.enemy[0].defense = 30

    if world.party.player.bind:
        $ world.battle.stage[1] += 1
    elif not world.party.player.bind:
        $ world.battle.stage[1] = 0
    if world.battle.stage[1] > 2:
        jump gnome_ng_a6

    if world.battle.used_daystar and world.party.player.daystar:
        call gnome_ng_earthguard
        $ world.battle.main()

    if world.battle.enemy[0].charge:
        $ random = rand().randint(1,2)

        if world.party.player.bind:
            call gnome_ng_a1
            $ world.battle.main()
        elif random == 1:
            call gnome_ng_a1
            $ world.battle.main()
        elif random == 2:
            call gnome_ng_a2
            $ world.battle.main()

    $ random = rand().randint(1,6)

    if random < 4:
        call gnome_ng_a0
        $ world.battle.main()
    elif random == 4:
        call gnome_ng_a3
        $ world.battle.main()
    elif random == 5:
        call gnome_ng_a4
        $ world.battle.main()
    elif random == 6 and not world.party.player.bind:
        call gnome_ng_a5
        $ world.battle.main()
    elif random == 6 and world.party.player.bind:
        call gnome_ng_a4
        $ world.battle.main()

label gnome_ng_earthguard:
    world.battle.enemy[0].name "................."
    $ world.battle.show_skillname("Титановая броня")
    play sound "audio/se/earth2.ogg"
    gnome "Гнома концентрирует силу земли и значительно усиливает свою защиту!"
    $ world.battle.enemy[0].defense = 2
    if world.party.player.daystar:
        $ world.party.player.skill22x()
        $ world.battle.main()
    $ world.battle.hide_skillname()
    return

label gnome_ng_earthguard2:
    "Титановая броня Гномы спадает!"
    return

label gnome_ng_a0:
    world.battle.enemy[0].name "................."
    $ world.battle.show_skillname("Разгон")
    play sound "audio/se/power.ogg"
    "Гнома заряжает свою магию!"
    $ world.battle.enemy[0].charge += 1
    $ world.battle.hide_skillname()
    return

label gnome_ng_a1:
    $ world.battle.enemy[0].charge = 0

    world.battle.enemy[0].name "...................."

    $ world.battle.show_skillname("Удар Гномы")
    play sound "audio/se/earth2.ogg"

    "Гнома наделяет свой кулак силой земли!{w}{nw}"

    play sound "audio/se/dageki.ogg"

    extend "\nПосле чего, бьёт Луку!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 51:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 46:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 41:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 76:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 71:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 66:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 41:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 36:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 31:
                    world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (950, 1000), 2: (1090, 1150), 3: (1230, 1300)}
        world.battle.enemy[0].guard_break_on = True

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 80, 2: 80, 3: 75}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], 1)

        world.battle.hide_skillname()
        world.battle.enemy[0].guard_break_on = False

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label gnome_ng_a2:
    $ world.battle.enemy[0].charge = 0
    world.battle.enemy[0].name "...................."
    $ world.battle.show_skillname("Разлом")
    play sound "audio/se/earth2.ogg"
    "Гнома наделяет свой кулак силой земли!{w}\nПосле чего, земля под ногами Луки раскалывается!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 51:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 46:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 41:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 61:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 56:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 51:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 41:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 36:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 31:
                    world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (220, 240), 2: (260, 280), 3: (450, 470)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 80, 2: 80, 3: 75}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

        world.party.player.bind = 1
        world.party.player.status_print()
        world.battle.enemy[0].power = 2

    "Лука застрял в земле!"

    $ world.battle.hide_skillname()

    return

label gnome_ng_a3:
    world.battle.enemy[0].name "...................."
    $ world.battle.show_skillname("Грязевые ручки")
    play sound "audio/se/ero_koki1.ogg"
    "Гнома хватает член Луки неистово доит его!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 11:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 6:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 1:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 21:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 16:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 11:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 11:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 6:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 1:
                    world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (370, 430), 2: (410, 460), 3: (490, 550)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 40, 2: 35, 3: 30}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label gnome_ng_a4:
    world.battle.enemy[0].name "........"
    $ world.battle.show_skillname("Минет Земляного Духа")
    play sound "ngdata/se/lewdsuck.ogg"
    "Гнома берёт член Луки в рот и начинает сильно сосать!"
    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 16:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 11:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 6:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 26:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 21:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 16:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 11:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 6:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 1:
                    world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (410, 440), 2: (450, 490), 3: (500, 550)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 35, 2: 30, 3: 25}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label gnome_ng_a5:
    $ world.battle.stage[4] = 1
    if not world.party.player.surrendered:
        world.battle.enemy[0].name ".................."
    show doro st03 at xy(0.25) as doll1
    show doro st03 at xy(0.8) as doll2
    show doro st01 at xy(0.7) as doll3
    show doro st02 at xy(0.35) as doll4
    with dissolve
    $ world.battle.show_skillname("Заключение Грязевыми Куклами")
    "Грязевые куклы появляются вокруг Луки!"
    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 51:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 46:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 41:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 76:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 71:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 66:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 46:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 41:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 36:
                    world.party.player.aqua_guard()

            if world.party.player.wind and world.party.player.wind_guard_calc({1: 35, 2: 30, 3: 25}):
                world.party.player.wind_guard()
                renpy.return_statement()
            elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
                world.party.player.wind_guard()
                renpy.return_statement()

        if world.party.player.daystar:
            world.party.player.bind = 0
            world.party.player.skill22x()

        world.party.player.bind = 1
        world.party.player.status_print()
        world.battle.enemy[0].power = 1
        world.battle.stage[2] = 1

    "Они хватают Луку и прижимают к земле!"
    world.battle.enemy[0].name ".............."

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    return

label gnome_ng_a6:
    if world.party.player.surrendered and world.battle.stage[2] < 1:
        call gnome_ng_a5
    if world.battle.stage[2] < 1:
        call gnome_ng_a5
    if not world.party.player.bind:
        jump gnome_ng_a

    world.battle.enemy[0].name "...!"

    play sound "audio/se/escape.ogg"
    hide doll1
    hide doll2
    hide doll3
    hide doll4
    with dissolve

    "Гнома срывает с себя всю одежду!"

    pause .5
    $ world.battle.show_skillname("Грязевая киска")
    play sound "audio/se/ero_pyu1.ogg"
    show gnome hb7 with dissolve

    "И насильно входит в Луку!"

    $ world.party.player.bind = 2
    $ world.battle.enemy[0].damage = (1000, 1030)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        jump badend_h

    "Лука был изнасилован Гномой!"
    world.battle.enemy[0].name "................"

    return

label kaeru_start:
    python:
        world.troops[29] = Troop(world)
        world.troops[29].enemy[0] = Enemy(world)

        world.troops[29].enemy[0].name = Character("Девушки-лягушки")
        world.troops[29].enemy[0].sprite = ["kaerua st01", "kaerua st02", "kaerua st03", "kaerua st02"]
        world.troops[29].enemy[0].sprite_b = ["kaerub st01", "kaerub st02", "kaerub st03", "kaerub st02"]
        world.troops[29].enemy[0].position = xy(0.3)
        world.troops[29].enemy[0].position_b = xy(0.6)
        world.troops[29].enemy[0].defense = 100
        world.troops[29].enemy[0].evasion = 95
        world.troops[29].enemy[0].max_life = world.troops[29].battle.difficulty(2600, 2800, 3000)
        world.troops[29].enemy[0].henkahp[0] = 1000
        world.troops[29].enemy[0].power = world.troops[29].battle.difficulty(0, 1, 2)

        world.troops[29].battle.tag = "kaeru"
        world.troops[29].battle.name = world.troops[29].enemy[0].name
        world.troops[29].battle.background = "bg 079"
        world.troops[29].battle.music = 1
        world.troops[29].battle.exp = 5000
        world.troops[29].battle.exit = "lb_0051"
        world.troops[29].battle.plural_name = True

        world.troops[29].battle.init()

    frog_girl_a "Аха-ха, это человек~{image=note}!"
    frog_girl_b "Источник вкусной спермы...{w} Нам нужно лишь поиграть с его штучкой, чтобы покушать её."
    "Два скользких тела выползли из реки, помчавшись ко мне, чтобы напасть.{w}\nНет... Их не только двое."
    "Похоже, что в реке их гораздо больше..."
    l "Я не хочу драться, но если вы нападёте на меня, то вы не оставите мне иного выбора!"
    "Я обнажил меч перед девушками-лягушками, готовясь защищаться."

    $ world.battle.main()

label kaeru_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label kaeru_v:
    $ world.battle.face(3)

    frog_girl_a "Кьяя!{w} Что происходит?!"

    play sound "se/syometu.ogg"
    hide kaerua with crash

    "Девушка-лягушка А превращается в маленькую лягушку!"
    frog_girl_b "Ахх! П-помогите мне!"

    play sound "se/escape.ogg"
    hide kaerub

    "Девушка-лягушка Б убегает прочь!"

    $ world.battle.victory(1)

label kaeru_a:
    if world.battle.enemy[0].life < world.battle.enemy[0].henkahp[0]:
        jump kaeru_v

    if world.party.player.bind == 1:
        $ random = rand().randint(1,3)

        if random == 1:
            call kaeru_a5
            call kaeru_a5
            $ world.battle.main()
        elif random == 2:
            call kaeru_a6
            call kaeru_a6
            $ world.battle.main()
        elif random == 3:
            call kaeru_a5
            call kaeru_a6
            $ world.battle.main()

    while True:
        $ random = rand().randint(1,5)

        if random == 1:
            call kaeru_a1
            $ world.battle.main()
        elif random == 2:
            call kaeru_a2
            $ world.battle.main()
        elif random == 3:
            call kaeru_a3
            $ world.battle.main()
        elif random > 3 and world.battle.delay[0] > 5:
            call kaeru_a4
            $ world.battle.main()


label kaeru_a1:
    $ world.battle.cry(frog_girl_a, [
        "I'll give you a treat with my webbed hands.{image=note}",
        "I'll give some service for my food.{image=note}",
        "I'll play with you a lot, so make sure you give us a lot.{image=note}"
    ])
    $ world.battle.cry(frog_girl_b, [
        "Don't try to endure it, just feed us...",
        "Do you like our slimy palms?",
        "It's fine if you want to give up..."
    ], True)

    $ world.battle.show_skillname("Frog Handjob")

    "The Frog Girls reach out toward Luka's penis!"

    "Their slimy, webbed hands rub all over Luka's penis!"

    $ world.battle.skillcount(2, 3267)

    python:
        renpy.sound.play("audio/se/ero_koki1.ogg")

        world.battle.enemy[0].damage = {1: (15, 20), 2: (20, 25), 3: (25, 30)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        renpy.sound.play("audio/se/ero_koki1.ogg")

        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label kaeru_a2:
    $ world.battle.cry(frog_girl_a, [
        "My webbed feet are nice and soft.{image=note}",
        "How do webbed feet feel?{image=note}",
        "If you give yourself to us, all of us Frog Girls can step on you like this...{image=note}"
    ])
    $ world.battle.cry(frog_girl_b, [
        "Our feet are nice and slimy...",
        "Our legs are really nimble...",
        "Cover our slimy legs with your slimy semen..."
    ], True)

    $ world.battle.show_skillname("Frog Footjob")

    "The Frog Girl's nimble feet stretch out toward Luka!"

    "They step on Luka's penis with their slimy webbed feet!"

    $ world.battle.skillcount(2, 3268)

    python:
        renpy.sound.play("audio/se/ero_koki1.ogg")

        world.battle.enemy[0].damage = {1: (16, 22), 2: (21, 27), 3: (26, 32)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        renpy.sound.play("audio/se/ero_koki1.ogg")
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label kaeru_a3:
    $ world.battle.cry(frog_girl_a, [
        "Our breasts are nice and slimy.{image=note}",
        "Here, let us crush your dick...{image=note}",
        "Our squishy breasts feel good, don't they?{image=note}"
    ])
    $ world.battle.cry(frog_girl_b, [
        "Your dick will get all slimy, too...",
        "We'll crush it, hehe.",
        "Cover my breasts in semen..."
    ])

    $ world.battle.show_skillname("Frog Tit Fuck")

    "The two Frog Girls sandwich Luka's penis between them!"

    "Their soft, slimy breasts come together and put pressure on Luka's penis!"

    $ world.battle.skillcount(2, 3269)

    python:
        renpy.sound.play("audio/se/ero_koki1.ogg")

        world.battle.enemy[0].damage = {1: (18, 23), 2: (23, 28), 3: (28, 33)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        renpy.sound.play("audio/se/ero_koki1.ogg")
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label kaeru_a4:
    if not world.party.player.surrendered:
        frog_girl_a "Aha, caught you.{image=note}"

    $ world.battle.show_skillname("Group Binding")

    "Frog Girl A grabs hold of Luka's right arm, and Frog Girl B grabs hold of Luka's left arm!"

    "With both arms bound, Luka can't move!"

    show kaeru ha1 zorder 15 as ct
    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "The Frog Girls are holding Luka down!"

    $ world.battle.hide_skillname()

    if world.party.player.surrendered:
        return

    if world.battle.first[0] != 1:
        l "Guh...!"
        "With both of them holding me down, this is difficult...{w}\nBut I think I can still shake them off!"

    if world.battle.first[0] == 1:
        frog_girl_b "This time you won't get free.{w}\nThis tongue will finish you off..."
    else:
        frog_girl_b "Hehe... We'll play with you now.{w}\nOur long tongues can still reach you like this..."

    $ world.battle.first[0] = 1

    if world.party.player.earth == 0 and persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 2
    elif world.party.player.earth == 0 and persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 3
    elif world.party.player.earth == 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 4
    elif world.party.player.earth > 0 and persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 0
    elif world.party.player.earth > 0 and persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 1
    elif world.party.player.earth > 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 2
    return


label kaeru_a5:
    "The two Frog Girls are holding Luka down..."

    $ world.battle.skillcount(2, 3270)

    $ world.battle.cry(frog_girl_a, [
        "It's time for some tongue.{image=note}",
        "Does it feel good here?..",
        "Our tongues can reach everywhere...{image=note}",
        "Hehe, we can make you slimy everywhere.{image=note}",
        "I wonder how you taste here?.."
    ])
    $ world.battle.cry(frog_girl_b, [
        "Are you happy when we lick your body?",
        "I'll try here, too...",
        "I'll wrap my tongue around this part...",
        "We'll smear our drool on you...",
        "We'll take our time in tasting you..."
    ], True)

    $ world.battle.show_skillname("Full-Body Licking Attack")
    show kaeru ha1 zorder 15 as ct

    "The Frog Girl's long tongues crawl all over Luka's body!"

    $ world.battle.cry(narrator, [
        "Luka's licked all over his body!",
        "Their tongues wrap around Luka's nipples!",
        "The Frog Girl's tongues world.party.player.wind around Luka's body, licking him all over!",
        "Their slimy tongues lick Luka all over!",
        "The Frog Girl's tongues taste every inch of Luka's body!"
    ], True)

    python:
        renpy.sound.play("audio/se/ero_makituki1.ogg")

        world.battle.enemy[0].damage = {1: (10, 14), 2: (10, 14), 3: (16, 20)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)

        renpy.sound.play("audio/se/ero_makituki1.ogg")
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=2)

        renpy.sound.play("audio/se/ero_makituki1.ogg")
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label kaeru_a6:
    "The two Frog Girls are holding Luka down..."

    $ world.battle.skillcount(2, 3271)

    $ world.battle.cry(frog_girl_a, [
        "Here, we'll even tease your penis.{image=note}",
        "If we do this, are you going to leak?",
        "I'll lick the tip nice and carefully.{image=note}",
        "Hehe... Your penis is delicious.{image=note}",
        "We'll soak your penis in our drool.{image=note}"
    ])
    $ world.battle.cry(frog_girl_a, [
        "I'll wrap my tongue around it, so let it out...",
        "At this rate, I'll get a taste in no time...",
        "This licking will leave you in agony...",
        "Give me a good taste...",
        "Our slimy tongues feel good, don't they?"
    ], True)

    $ world.battle.show_skillname("Groin Licking Attack")
    show kaeru ha4

    "The Frog Girl's tongues shoot out toward Luka's groin!"

    $ world.battle.cry(narrator, [
        "The Frog Girl's tongues world.party.player.wind around Luka's penis!",
        "The Frog Girl's tongues wrap around Luka's penis, and start to vibrate!",
        "The multiple tongues lick up and down Luka's penis!",
        "While one tongue licks the tip of Luka's penis, the other massages his back muscle!",
        "The Frog Girl's tongues lick Luka's penis up and down, covering it in saliva!"
    ], True)

    python:
        renpy.sound.play("audio/se/ero_makituki2.ogg")

        world.battle.enemy[0].damage = {1: (12, 15), 2: (12, 15), 3: (18, 21)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)

        renpy.sound.play("audio/se/ero_makituki2.ogg")
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=2)

        renpy.sound.play("audio/se/ero_makituki2.ogg")
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h5")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label kaeru_mogaku:
    $ random = rand().randint(1,3)

    if world.battle.result == 1 and random == 1:
        l "Urhg...!"
    elif world.battle.result == 1 and random == 2:
        l "Guh... Damn it!"
    elif world.battle.result == 1 and random == 3:
        l "Stop!"
    elif world.battle.result == 2 and random == 1:
        l "Uhhg...!"
    elif world.battle.result == 2 and random == 2:
        l "L...Let go!"
    elif world.battle.result == 2 and random == 3:
        l "Guh... Stop it!"

    if world.battle.result == 1:
        $ world.party.player.mp_regen()

        "Luka attacks!{w}\nBut still bound, Luka can't move!"
    elif world.battle.result == 2:
        $ world.battle.enemy[0].power -= 1

        if world.battle.enemy[0].power < 0:
            $ world.battle.enemy[0].power = 0

        "Luka struggles with all his strength!{w}\nBut Luka can't escape from their grasp!"

    $ random = rand().randint(1,5)

    if random == 1:
        frog_girl_a "Ahaha, we'll never let go.{image=note}"
        frog_girl_b "We'll keep using our tongues to play with you..."
    elif random == 2:
        frog_girl_a "Useless!{image=note}"
        frog_girl_b "You won't escape..."
    elif random == 3:
        frog_girl_a "Ahaha, weeaaak!{image=note}"
        frog_girl_b "Such feeble resistance, hehe."
    elif random == 4:
        frog_girl_a "What are you doing? Aren't you embarrassed?"
        frog_girl_b "Just drown in the pleasure..."
    elif random == 5:
        frog_girl_a "What wasted effort.{image=note}"
        frog_girl_b "You'll be too weak to resist soon..."

    jump kaeru_a


label kaeru_mogaku2:
    if persistent.difficulty == 3:
        $ random = rand().randint(1,2)

        if random == 2:
            jump kaeru_mogaku

    $ random = rand().randint(1,3)

    if random == 1:
        l "Uwaaa!"
    elif random == 2:
        l "Damn you!"
    elif random == 3:
        l "Get off!"

    hide kaeru
    play sound "audio/se/dassyutu.ogg"
    $ world.party.player.bind = 0
    $ world.party.player.status_print()
    $ world.battle.face(3)

    "Luka struggles with all his might, and breaks free!"
    frog_girl_a "Arara, he fled."
    frog_girl_b "You didn't like our tongues?"

    $ world.battle.face(1)

    $ world.battle.main()

label kaeru_earth1:
    $ random = rand().randint(1,3)

    if random == 1:
        l "Guuh!"
    elif random == 2:
        l "G...Get off!"
    elif random == 3:
        l "Damn you!"

    play sound "audio/se/power.ogg"

    "Filled with the power of world.party.player.earth, Luka summons incredible strength!"

    $ world.battle.enemy[0].power -= 1

    if world.battle.enemy[0].power < 0:
        $ world.battle.enemy[0].power = 0

    "But he can't break from their grasp!"

    $ random = rand().randint(1,5)

    if random == 1:
        frog_girl_a "Ahaha, we'll never let go.{image=note}"
        frog_girl_b "We'll keep using our tongues to play with you..."
    elif random == 2:
        frog_girl_a "Useless!{image=note}"
        frog_girl_b "You won't escape..."
    elif random == 3:
        frog_girl_a "Ahaha, weeaaak!{image=note}"
        frog_girl_b "Such feeble resistance, hehe."
    elif random == 4:
        frog_girl_a "What are you doing? Aren't you embarrassed?"
        frog_girl_b "Just drown in the pleasure..."
    elif random == 5:
        frog_girl_a "What wasted effort.{image=note}"
        frog_girl_b "You'll be too weak to resist soon..."

    jump kaeru_a


label kaeru_earth2:
    if persistent.difficulty == 3:
        $ random = rand().randint(1,2)

        if random == 2:
            jump kaeru_earth1

    $ random = rand().randint(1,3)

    if random == 1:
        l "With this strength..."
    elif random == 2:
        l "With Gnome's power...!"
    elif random == 3:
        l "With the power of the world.party.player.earth...!"

    play sound "audio/se/power.ogg"

    "Filled with the power of world.party.player.earth, Luka summons incredible strength!"

    hide kaeru
    play sound "audio/se/dassyutu.ogg"
    $ world.party.player.bind = 0
    $ world.party.player.status_print()
    $ world.battle.face(3)

    "Luka struggles with all his might, and breaks free of their grasp!"
    frog_girl_a "What's with that strength?!"
    frog_girl_b "Was that some kind of strange magic?.."

    $ world.battle.face(1)

    $ world.battle.main()

label kaeru_s1:
    if world.party.player.max_life > world.party.player.life*5:
        $ world.party.player.hphalf = True
        jump kaeru_s2

    $ random = rand().randint(1,3)

    if random == 1:
        l "Guh..."
    elif random == 2:
        l "Uhh..."
    elif random == 3:
        l "Urhg..."

    $ world.battle.face(2)

    frog_girl_a "Aha, it's good isn't it?{image=note}"
    frog_girl_b "It's fine to give up and come..."

    $ world.battle.face(1)
    $ world.party.player.hphalf = True
    return


label kaeru_s2:
    $ random = rand().randint(1,3)

    if random == 1:
        l "Ahh..."
    elif random == 2:
        l "Ahhhh..."
    elif random == 3:
        l "Haa..."

    $ world.battle.face(2)

    frog_girl_a "You can't stand it anymore, can you?"
    frog_girl_b "Come on, give up and feed us..."

    $ world.party.player.kiki = True
    return


label kaeru_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    frog_girl_a "Уже сдаёшься?{w}\nКак жалко, аха-ха-ха!{image=note}"
    frog_girl_b "Тогда мы вдоволь с тобой наиграемся..."

    $ world.battle.enemy[0].attack()

label kaeru_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Frog Handjob"
    $ list2 = "Frog Footjob"
    $ list3 = "Frog Tit Fuck"
    $ list4 = "Full-Body Licking Attack"
    $ list5 = "Groin Licking Attack"

    if persistent.skills[3267][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3268][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3269][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3270][0] > 0:
        $ list4_unlock = 1

    if persistent.skills[3271][0] > 0:
        $ list5_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump kaeru_onedari1
    elif world.battle.result == 2:
        jump kaeru_onedari2
    elif world.battle.result == 3:
        jump kaeru_onedari3
    elif world.battle.result == 4:
        jump kaeru_onedari4
    elif world.battle.result == 5:
        jump kaeru_onedari5
    elif world.battle.result == -1:
        jump kaeru_main


label kaeru_onedari1:
    call onedari_syori

    frog_girl_a "With these webbed hands, you want us to make you come?"
    frog_girl_b "So that's how you like it... Hehe."

    while True:
        call kaeru_a1


label kaeru_onedari2:
    call onedari_syori

    frog_girl_a "You want to be teased by our webbed feet?"
    frog_girl_b "So that's how you like it... Hehe."

    while True:
        call kaeru_a2


label kaeru_onedari3:
    call onedari_syori

    frog_girl_a "You want to be crushed between our slimy breasts?"
    frog_girl_b "So that's how you like it... Hehe."

    while True:
        call kaeru_a3


label kaeru_onedari4:
    call onedari_syori_k

    frog_girl_a "You want our slimy tongues all over your body?.."
    frog_girl_b "So that's how you like it... Hehe."

    if not world.party.player.bind:
        call kaeru_a4

    while True:
        call kaeru_a5


label kaeru_onedari5:
    call onedari_syori_k

    frog_girl_a "You want our long tongues to play with your penis?.."
    frog_girl_b "So that's how you like it... Hehe."

    if not world.party.player.bind:
        call kaeru_a4

    while True:
        call kaeru_a6


label kaeru_h1:
    $ world.party.player.moans(1)
    with syasei1
    show kaerua bk03 at xy(Y=50) zorder 10 as bk
    show kaerub bk03 at xy(X=360, Y=50) zorder 10 as bk2
    $ world.ejaculation()

    "Unable to take the pleasure of their strange webbed hands any longer, Luka finally comes."

    $ world.battle.lose()
    $ world.battle.count([5, 1])
    $ bad1 = "Luka was forced to come by the Frog Girl's handjob."
    $ world.battle.face(2)

    frog_girl_a "Ahaha, he came.{image=note}"
    frog_girl_b "Our hands are all covered in semen... Hehe."

    jump kaeru_hb


label kaeru_h2:
    $ world.party.player.moans(1)
    with syasei1
    show kaerua bk04 at xy(Y=50) zorder 10 as bk
    show kaerub bk04 at xy(X=360, Y=50) zorder 10 as bk2
    $ world.ejaculation()

    "Unable to take the pleasure of their strange webbed feet any longer, Luka finally comes."

    $ world.battle.lose()
    $ world.battle.count([6, 1])
    $ bad1 = "Luka was forced to come by the Frog Girl's footjob."
    $ world.battle.face(2)

    frog_girl_a "Ahaha, he came.{image=note}"
    frog_girl_b "Our feet are all covered in semen... Hehe."

    jump kaeru_hb


label kaeru_h3:
    $ world.party.player.moans(1)
    with syasei1
    show kaerua bk02 at xy(Y=50) zorder 10 as bk
    show kaerub bk02 at xy(X=360, Y=50) zorder 10 as bk2
    $ world.ejaculation()

    "Unable to take the pleasure of their soft, slimy breasts any longer, Luka finally comes."

    $ world.battle.lose()
    $ world.battle.count([7, 1])
    $ bad1 = "Luka was forced to come by the Frog Girl's tit fuck."
    $ world.battle.face(2)

    frog_girl_a "Ahaha, he came.{image=note}"
    frog_girl_b "Our breasts are all covered in semen... Hehe."

    jump kaeru_hb


label kaeru_h4:
    $ world.party.player.moans(1)
    with syasei1
    show kaeru ha2 as ct zorder 15
    $ world.ejaculation()

    "With their slimy, long tongues running all over his body, Luka finally orgasms from the constant stimulation all over his body."

    $ world.battle.lose()
    $ world.battle.count([28, 7])
    $ bad1 = "With the Frog Girl's tongues licking Luka all over, he finally orgasms."
    $ world.battle.face(2)

    frog_girl_a "Ahaha, he came.{image=note}"
    frog_girl_b "Our tongues felt so good that you couldn't control yourself?"

    jump kaeru_ha


label kaeru_h5:
    $ world.party.player.moans(2)
    with syasei1
    show kaeru ha5 as ct zorder 15
    $ world.ejaculation()

    "With the Frog Girl's tongues licking every inch of his penis, Luka finally gives in to the rising orgasm."

    $ world.battle.lose()
    $ world.battle.count([4, 7])
    $ bad1 = "With the Frog Girl's tongues licking Luka's penis, he finally orgasms."
    $ world.battle.face(2)

    frog_girl_a "Ahaha, he came.{image=note}{w}\nOur tongues are all covered in semen.{image=note}"
    frog_girl_b "Our tongues feel really good, don't they?"

    jump kaeru_ha


label kaeru_ha:
    $ bad2 = "Caught by a swarm of Frog Girls, Luka was forced into being their semen slave."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    l "Ahhh..."
    "weakened after my orgasm, I feel my strength start to fade."

    hide bk
    hide bk2
    hide kaerua
    hide kaerub
    show kaeru ha1

    "Their long tongues run all over my body like snakes, until reaching my groin."
    "With saliva dripping off their long tongues, they turn toward my erect penis."
    l "D...Don't... Stop..."
    frog_girl_a "No way!{image=note}{w} I love your taste... I don't think I'll ever get tired of licking you.{image=note}"
    frog_girl_b "We can hold you down while we play with you with our tongues, so writhe around as much as you want."

    play hseanvoice "audio/voice/fera_zyuru1.ogg"
    show kaeru ha3

    "Multiple tongues wrap around my penis, slowly coiling until the tips of their tongues reach the tip of my penis."
    l "Ah... Ahhh..."
    "The combination of their rough tongues with their slimy saliva makes me moan out in surprise at the sudden stimulation."
    "Their pink tongues coil and wrap around my penis, as if trying to hide away the spoils of their victory from the world..."
    l "S...Stop it... Let me go..."
    frog_girl_a "Ahaha, we won't let you go.{image=note}{w}\nWe haven't even started the licking yet!{image=note}"
    frog_girl_c "Your penis tastes really good...{image=note}"
    "Another Frog Girl's long tongue begins to coil around the base of my penis.{w}{nw}"

    show kaeru ha4
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nEvery visible part of my penis is suddenly covered in multiple tongues!"
    l "Ahhhh!!"
    "The multiple tongues all move in random directions, each trying to beat the other for control.{w}\nThe random, powerful stimulation coming from every direction is too much to take."
    l "Ahh..."
    "I bite my lips as I can do nothing but feel what's happening under the pile of long tongues.{w}\nWith a small jump, I let out a sharp cry when I feel the tip of one of their tongues poking my urethral opening."
    frog_girl_a "Ehehe... Your penis now belongs to our tongues."
    frog_girl_b "We'll play with it a lot, so make sure you give us lots of delicious food..."
    "Their slimy tongues begin to tighten around me.{w}\nWith each increased constriction, the tongue poking me in the urethra continues to circle around the opening, adding to the unbearable stimulation."
    l "Ahh! Ahhhh!"
    frog_girl_a "Hahaha, look at him writhing around.{image=note}{w}\nHe's so happy that his penis was caught by our tongues, he doesn't know what to do with himself.{image=note}"
    frog_girl_b "Let's make it tighter, so he can give us more semen..."
    "Completely entombed in their pink tongues, their brutal attack continues on my penis."
    "The tight pressure being applied by the slimy tongues, with the rough texture as they move and lick every inch causes my back to arch as I moan."
    l "Ahh! Haaaa!!"
    frog_girl_c "Ahaha, he looks so happy.{image=note}{w}\nHow pathetic!{image=note}"
    "With my dick completely at the mercy of their tongues, the Frog Girls start to insult and ridicule me."
    "I can already feel myself nearing an orgasm, but I can't bring myself to admit to such a humiliating defeat.{w}\nI clench my teeth and bite my lip, trying to do everything in my power to reject the pleasure being forced into me."
    frog_girl_a "Aha, he's trying to hold on!{w}\nHow cute... But it won't work.{image=note}"
    frog_girl_b "Lick him more...{w}\nThere's no escaping the pitiful result of your feeble endurance..."
    "Wrapped in the slimy coil of pleasure, my resistance is being quickly chipped away."
    "The pleasure too powerful to resist any longer, my body overpowers my mind in desiring release."
    l "Ahh... I'm going to come...!{w}\nAhhhh!"

    with syasei1
    show kaeru ha5
    $ world.ejaculation()

    "Wrapped in the tower of tongues, I let out a powerful orgasm, as my semen sprays all over their coiled tongues."
    frog_girl_a "Aha, he came!{image=note}{w}\nHe tried so hard, but let out so much anyway!{image=note}"
    frog_girl_b "It's time for a feast..."

    play hseanvoice "audio/voice/fera_zyuru2.ogg"
    show kaeru ha6

    "The Frog Girl's tongues run all over me, fighting each other to get a taste of the semen I just let out."
    "Their rough tongues both rub against me and each other, as if in a competition to see who can get the most."
    l "Ah... Ahhh!{w}\nAhhhhhh!!"
    "Because of that, the stimulation seemingly doubles as they all rub me with almost crazed desire."
    "One of their tongues pokes at my urethral opening, lapping up the last of the semen that didn't manage to get shot out."
    l "Ahh! D...Don't lick the tip like that...!"
    frog_girl_a "But the tip is where the delicious semen comes from...{image=note}"
    frog_girl_b "Until we can't taste even a hint of it left, we won't stop licking."
    l "Ahhhhh!"
    "I writhe around pathetically on the ground as I'm stuck in their hell of frenzied tongues."
    frog_girl_a "Don't leave a single spot open on him.{image=note}"
    frog_girl_c "This human's sperm is really, really delicious.{image=note}"
    l "Ahhh!{w} Ahhhh!!"
    "Right as it seems as though they got the last taste of semen, I recoil in horror as I feel another orgasm rising through me."
    "I feebly try to hold on to stop the feeding frenzy again, but it's useless."

    with syasei1
    show kaeru ha7
    $ world.ejaculation()

    "As soon as I come again, the tongue frenzy begins anew.{w}\nEven while ejaculating, their tongues rub, lick, and squeeze my sensitive penis without mercy."
    frog_girl_a "Aha, he came again.{image=note}"
    frog_girl_b "Giving us another feast so quickly, what a thoughtful human..."
    "The Frog Girls all move in closer, eager to get another taste.{w}\nStuck in a hell of pleasure, there's no reprieve from their slimy tongues."
    l "Ahhh! Ahhh!!"
    "Captured by the Frog Girl's tongues, I writhe around on the ground trying to get away.{w}\nBut no matter what I do, their long, agile tongues never lose their grip on my penis."
    frog_girl_a "More... Come more.{image=note}"
    frog_girl_b "We'll keep playing with you, so you just keep feeding us."
    l "Ahhh!{w} Stop already...!"
    l "Ahhhhh!!"

    with syasei1
    $ world.ejaculation()

    "As if my body was obeying her commands, I orgasm again, shooting off more semen while my body writhes in pleasure."
    frog_girl_a "Aha, he did it again.{image=note}{w}\nI'll be able to get a good taste...{image=note}"
    frog_girl_b "Just drown yourself in our tongues..."
    frog_girl_c "Hey, come on. I'll lick you even more, so give me more!"
    l "Ahh!"

    with syasei1
    $ world.ejaculation()

    "While writhing helplessly, more and more Frog Girls show up, eager to force me toward another ejaculation so they can get a taste..."

    scene bg black with Dissolve(3.0)
    stop hseanvoice fadeout 1

    "Thus, I was caught by a swarm of Frog Girls.{w}\nKept as a domestic animal, my only purpose is to feed them."
    "With no chance to escape from the swarm, I had to accept my new life.{w}\nMy adventure ends here..."
    ".............."

    jump badend


label kaeru_hb:
    $ bad2 = "Caught by the swarm of Frog Girls, Luka was kept as a breeding slave."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut


    l "Ahh..."
    "weakened after my orgasm, I feel my body go limp.{w}{nw}"

    hide bk
    hide bk2
    hide kaerua
    hide kaerub
    show kaeru hb0
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nAs soon as I fall to the ground, a Frog Girl clings to me!"
    frog_girl_a "Now, let's mate.{image=note}"
    frog_girl_b "We've been waiting for this for a while, hehe."
    "The two Frog Girls sandwich me between them, rubbing their slimy skin directly on my own.{w}\nThat strange feeling alone causes me to gasp out loud."
    l "Ahh..."
    frog_girl_a "Hahaha, something like this was enough to make you moan like that?"
    frog_girl_b "Our slimy skin feels good on the outside, but it feels incredible inside..."
    "The Frog Girl clinging to my front parts her legs, revealing something that looks similar to a human female's vagina."
    "But the texture is completely different...{w} Drips of mucus are coming out of the opening, and the pink inside looks puffy and moist."
    "Just the thought of being inserted into that dripping hole causes a shiver to run through my body."
    l "S...Stop it..."
    frog_girl_a "You can't say \"No\" before we've even done anything.{w}\nIt feels really good in here, so you won't be able to say \"No\" for long.{image=note}"
    frog_girl_b "Mating with the both of us will be like you're in heaven."
    l "Ahhh..."
    "She spreads her legs open wider, and positions the tip of my penis right next to her opening.{w}{nw}"

    play sound "audio/se/ero_pyu1.ogg"

    extend "\nA small drip of her mucus comes out of her pink hole, and it slowly runs down my erect penis."
    "As the slimy liquid drips down onto my thigh, she slowly forces me into her soft, wet hole."

    play hseanwave "audio/se/hsean04_innerworks_a4.ogg"
    show kaeru hb1

    "The Frog Girl slowly forces me all the way inside her, with her puffy mucus covered vaginal walls rubbing against me the entire length in."
    l "Ah..."
    frog_girl_a "Alright, you're all the way in.{image=note}{w}\nWell? It's heavenly inside, isn't it?"
    "The giggling Frog Girl stares right into my face, loose and slack jawed at the stimulation she's forcing on me."
    "As she said, it feels better than anything I've ever felt before...{w} Soft, slimy, and warm... As if I could melt inside of her."
    "Her walls are puffy, and seemingly mold to my form, gently pushing and embracing my penis."
    "In addition, the slimy secretions inside of her form a lubricant that makes me slip in and out of her quickly, rubbing against all of her folds and interior bumps with no friction."
    "Just the act of being inside of her is draining me of any remaining power..."
    l "Haaa...{w}\nI...It feels amazing..."
    frog_girl_a "Don't you love being inside, all the way in?{w}\nSo enjoy it, and mate with us forever.{image=note}"
    l "M...Mate?.."
    "Right before almost losing myself in the pleasure, that word triggers a final warning in my brain."
    l "Th...That... No... I can't do that..."
    frog_girl_a "Eh?{w} You don't want to mate?{w}\nEven though it feels so nice inside?{image=note}"
    "To emphasize her point, the Frog Girl hugs me a little tighter, and quickly squeezes around my penis.{w}\nI jump back at the sudden stimulation, almost forced to an instant orgasm."
    l "Ahhh... N...No..."
    "But still, I bite my lip and hold on.{w}\nIn response to my feeble protests, the Frog Girls giggle and ridicule me."
    frog_girl_b "It's pointless to resist at this point.{w}\nJust surrender, and give yourself to us..."
    "The Frog Girl clinging to my back taunts me with a smile.{w}\nShe rubs her slimy body against me, as if impatient for her turn."
    "With the two Frog Girls on both sides of me, it feels as though my body itself doesn't even belong to me."
    frog_girl_a "Here, I'll do this to make it even better.{w}\nThere's no reason at all to resist this pleasure.{image=note}"
    "The Frog Girl raping me starts to move her waist up and down.{w}\nUp and down, her powerful legs and waist usually used for jumping are being used to quickly snap up and down in a piston movement."
    "The mucus inside of her serves as a powerful lubricant, enabling her to move faster than friction would seemingly allow."
    l "Ahhh...{w}\nS...Stop... I'm going to come..."
    frog_girl_a "Here, here!{image=note}{w}  I can already feel a little leaking!{image=note}"
    frog_girl_b "Stop trying to hold out, just enjoy mating with us."
    "The Frog Girls giggle at my pained face.{w}\nBrought to my limit, I can't hold on any longer."
    "Unable to endure it any longer, I feel my body breaking down all of my mental barriers and attempts at holding in my orgasm."
    l "Ahhh..."
    "The last of my resistance crumbles as I give myself to the Frog Girl's amazing pussy."

    with syasei1
    show kaeru hb2
    $ world.ejaculation()

    "My penis twitches inside her soft interior, as I release my semen inside her.{w}\nWith a smile on her face, the Frog Girl stares into my eyes as I ejaculate inside her."
    frog_girl_a "Ahaha, you came.{image=note}{w}\nYou let out so much, too.{image=note}"

    show kaeru hb3

    "The Frog Girl's long tongue shoots out and forces itself into my mouth, quickly darting around inside."
    frog_girl_b "I think I'll rape your mouth for a while too..."
    "The Frog Girl clinging to my back also extends her tongue, forcing it into my mouth."
    "Her slimy tongue darts around inside my mouth, as the Frog Girl in front of me doesn't let me pull out of her vagina."
    l "Nnnn... Nnngg..."
    frog_girl_a "Ehehe, I'll make you feel even better.{image=note}"

    play hseanwave "audio/se/hsean08_innerworks_a8.ogg"

    "While the Frog Girl behind me uses her tongue to rape my mouth, the one in front of me starts to slowly move her waist again."
    "She slightly changes the angle, causing the tip of my penis to rub against her puffy walls with every movement."
    "The soft texture of her slimy walls makes me moan in pleasure."
    l "Nnnnn..."
    "It truly is like being in heaven...{w}\nSandwiched and toyed with by the Frog Girls, I can't do anything but be embraced by the pleasure..."
    frog_girl_b "Hehe... He isn't resisting at all any more."
    frog_girl_a "Giving in after just this little... How pathetic.{image=note}"
    "The Frog Girl raping me moves in a slow circle, giving a constant flow of pleasure as I rub against her slimy walls."
    l "Ahhh... This is... Incredible...{w}\nAhhhh!"

    with syasei1
    show kaeru hb4
    $ world.ejaculation()

    "Having already given in to the pleasure, I don't even attempt to hold back."
    l "Ahhh..."
    "Completely exhausted, I've given myself to the Frog Girls.{w}\nAll I want is to feel more of this ecstasy..."
    frog_girl_a "Ahaha, he completely gave in.{image=note}"
    frog_girl_b "We'll keep you as our pet breeding slave."
    "The Frog Girls giggle as they casually tell me my fate.{w}\nBut at this point, I'm not even paying attention to their words... I just want to feel more pleasure."
    l "Ahhh... This is amazing...{w}\nAhhhh!"

    with syasei1
    $ world.ejaculation()

    "My only answer to them is my repeated orgasms."
    frog_girl_a "Ah, you already came again!{w}\nGood boy.{image=note}{w} You can mate with us as much as you want.{image=note}"
    frog_girl_b "We'll be able to give birth to a lot of healthy babies.{w}\nIt looks like our family will be secure for a long time..."
    l "Ahhh..."

    with syasei1
    $ world.ejaculation()

    "The Frog Girls continue to ridicule me as I slowly start to faint.{w}\nDrained in both body and spirit, I pass out."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "After that, I was never released.{w}\nKept as a breeding slave by the Frog Girls, I'm used as a tool to create more offspring."
    "But even so, I'm happy.{w}\nBecause I get to taste their amazing pussies every day..."
    "..........."

    jump badend


label alraune_start:
    python:
        world.troops[30] = Troop(world)
        world.troops[30].enemy[0] = Enemy(world)

        world.troops[30].enemy[0].name = Character("Альрауна")
        world.troops[30].enemy[0].sprite =  ["alraune st01", "alraune st02", "alraune st03", "alraune st02"]
        world.troops[30].enemy[0].position = center
        world.troops[30].enemy[0].defense = 100
        world.troops[30].enemy[0].evasion = 95
        world.troops[30].enemy[0].max_life = world.troops[30].battle.difficulty(1700, 2300, 3000)
        world.troops[30].enemy[0].power = world.troops[30].battle.difficulty(0, 1, 2)

        world.troops[30].battle.tag = "alraune"
        world.troops[30].battle.name = world.troops[30].enemy[0].name
        world.troops[30].battle.background = "bg 078"
        world.troops[30].battle.music = 1
        world.troops[30].battle.exp = 5500
        world.troops[30].battle.exit = "lb_0052"

        world.troops[30].battle.init()

    $ world.battle.face(2)

    world.battle.enemy[0].name "Ха-ха, а вот и вкусненький человек{image=note}.{w}\nВозрадуйся же, ведь я собираюсь высосать твою сперму для своего пропитания{image=note}."
    l "Я не позволю этому случиться!"
    "Прежде чем она сказала ещё что-либо, я обнажил меч и приготовился защищать себя!"

    $ world.battle.face(1)
    $ world.battle.main()


label alraune_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label alraune_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Уааа..."

    play sound "se/syometu.ogg"
    hide alraune with crash

    "Альрауна превращается в цветок!"

    $ world.battle.victory(1)

label alraune_a:
    if world.party.player.bind == 1:
        call alraune_a5
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,4)

        if random == 1:
            call alraune_a1
            $ world.battle.main()
        elif random == 2:
            call alraune_a2
            $ world.battle.main()
        elif random == 3 and world.party.player.status == 0 and world.battle.delay[0] > 3:
            call alraune_a3
            $ world.battle.main()
        elif random == 4 and world.battle.delay[1] > 3:
            call alraune_a4
            $ world.battle.main()


label alraune_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Let me have a taste of your penis...",
        "Delicious... Let me have a taste in my mouth.",
        "Does my mouth feel good?.."
    ])

    $ world.battle.show_skillname("Alraune's Blowjob")
    play sound "audio/se/ero_buchu2.ogg"

    "Alraune sucks Luka's penis into her mouth, and starts to gently suck on it!"

    $ world.battle.skillcount(2, 3272)

    python:
        world.battle.enemy[0].damage = {1: (35, 45), 2: (45, 55), 3: (55, 65)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label alraune_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Please come on my breasts.{image=note}",
        "I'll put your thing between my breasts, and make you come...",
        "Mmmpf... Mmpf... Does that feel good?"
    ])

    $ world.battle.show_skillname("Alraune's Tit Fuck")
    play sound "audio/se/ero_koki1.ogg"

    "Alraune sandwiches Luka's penis between her breasts, and starts to squeeze them together!"

    $ world.battle.skillcount(2, 3273)

    python:
        world.battle.enemy[0].damage = {1: (45, 55), 2: (55, 65), 3: (65, 75)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label alraune_a3:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "My breath is veeeery sweet!",
        "With this aroma, you'll be violated from the inside...{image=note}",
        "Take a deeeep breath, and let yourself go.{image=note}"
    ])

    $ world.battle.show_skillname("Alraune's Fragrance")
    play sound "audio/se/ero_pyu3.ogg"

    "Alraune kisses Luka, with a sweet smell emanating from her mouth..."

    "The sweet smell flows through Luka's body, drowning him in ecstasy."

    $ world.battle.skillcount(2, 3274)
    $ world.battle.enemy[0].damage = {1: (15, 20), 2: (25, 30), 3: (35, 40)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    if world.party.player.status == 0:
        $ world.party.player.status = 1
        $ world.party.player.status_print()
        "Luka has been immersed in ecstasy!"
    elif world.party.player.status == 1:
        $ world.party.player.status_print()
        "Luka is immersed in ecstasy..."

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    if world.party.player.surrendered:
        return

    l "Ahhh..."
    world.battle.enemy[0].name "Aha, it feels really nice, doesn't it?{w}\nBut now I can make it feel even more amazing.{image=note}"

    if persistent.difficulty == 1:
        $ world.party.player.status_turn = rand().randint(3,4)
    elif persistent.difficulty == 2:
        $ world.party.player.status_turn = rand().randint(4,5)
    elif persistent.difficulty == 3:
        $ world.party.player.status_turn = rand().randint(5,6)

    return


label alraune_a4:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "I'm going to catch you, then suck out all your semen!{image=note}"

    $ world.battle.show_skillname("Ivy Restraint")
    play sound "audio/se/ero_makituki3.ogg"

    "The Alraune's ivy wraps around Luka's body!"

    "Luka's body is wrapped by Alraune's ivy!"

    $ world.battle.enemy[0].damage = {1: (20, 30), 2: (30, 40), 3: (40, 50)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Luka's being restrained by Alraune's ivy!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h4")

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    world.battle.enemy[0].name "Ehehe... Caught you.{image=note}{w}{nw}"

    if world.battle.first[0] == 1:
        extend "{w=.7}\nThis time, I'll suck it all out.{image=note}"
    else:
        extend "{w=.7}\nNow, I'll suck it all out with this amazing flower.{image=note}"

    $ world.battle.first[0] = 1

    if world.party.player.earth == 0 and persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 2
    elif world.party.player.earth == 0 and persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 3
    elif world.party.player.earth == 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 4
    elif world.party.player.earth > 0 and persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 1
    elif world.party.player.earth > 0 and persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 2
    elif world.party.player.earth > 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return


label alraune_a5:
    "Luka's being restrained by Alraune's ivy!"

    $ world.battle.skillcount(2, 3275)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ehehe... I'm going to suck out all your semen.{image=note}",
        "Come on... Let out a lot of semen into this flower.{image=note}",
        "The honey will cover your penis, and make it feel even better.{image=note}",
        "Come on, all you have to do is come in here.{image=note}",
        "Please fill up my flower with all of your white nutrients.{image=note}"
    ])

    $ world.battle.show_skillname("Succubus Flower")
    play sound "audio/se/ero_chupa6.ogg"

    if world.battle.stage[4] == 0:
        show alraune ct01 at topleft zorder 15 as ct

    show alraune ct02 at topleft zorder 15 as ct

    if world.party.player.surrendered:
        $ world.battle.stage[4] = 1

    $ world.battle.cry(narrator, [
        "The Succubus Flower closes around Luka's penis, and intensely sucks on it!",
        "The Succubus Flower violently sucks on Luka's penis!",
        "The Succubus Flower closes around Luka's penis, its warm honey covering him!",
        "The Succubus Flower wraps around Luka's penis, and tightens!",
        "The Succubus Flower tightens around Luka's penis!"
    ], True)

    python:
        world.battle.enemy[0].damage = {1: (75, 100), 2: (85, 110), 3: (95, 120)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], drain=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if not world.party.player.surrendered:
            renpy.hide("ct")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label alraune_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Ура!{w} Я ведь могу делать что захочу?{w}\nЭхе-хе-хе... тогда я высосу всю сперму из тебя.{image=note}"

    $ world.battle.enemy[0].attack()

label alraune_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Alraune's Blowjob"
    $ list2 = "Alraune's Tit Fuck"
    $ list3 = "Alraune's Fragrance"
    $ list4 = "Succubus Flower"

    if persistent.skills[3272][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3273][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3274][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3275][0] > 0:
        $ list4_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump alraune_onedari1
    elif world.battle.result == 2:
        jump alraune_onedari2
    elif world.battle.result == 3:
        jump alraune_onedari3
    elif world.battle.result == 4:
        jump alraune_onedari4
    elif world.battle.result == -1:
        jump alraune_main


label alraune_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want to come inside my mouth?.."

    while True:
        call alraune_a1


label alraune_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want to come between my breasts?.."

    while True:
        call alraune_a2


label alraune_onedari3:
    call onedari_syori

    world.battle.enemy[0].name "You want my sweet smell to force you to come?"

    while True:
        call alraune_a3


label alraune_onedari4:
    call onedari_syori_k

    world.battle.enemy[0].name "You want to be sucked dry by my flower?.."

    if not world.party.player.bind:
        call alraune_a4

    while True:
        call alraune_a5


label alraune_h1:
    $ world.party.player.moans(1)
    with syasei1
    show alraune bk01 at xy(X=147) zorder 10 as bk
    $ world.ejaculation()

    "Sucked by Alraune's small mouth, Luka was finally forced to orgasm."

    $ world.battle.lose()
    $ world.battle.count([4, 3])
    $ bad1 = "Luka succumbed to Alraune's blowjob."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Nnnn... *Suck*"
    world.battle.enemy[0].name "Ehehe... You came in my mouth.{image=note}{w}\nDid it feel too good for you to take?"

    jump alraune_h


label alraune_h2:
    $ world.party.player.moans(1)
    with syasei1
    show alraune bk02 at xy(X=147) zorder 10 as bk
    $ world.ejaculation()

    "Sandwiched between Alraune's breasts, Luka was finally forced to orgasm."

    $ world.battle.lose()
    $ world.battle.count([7, 3])
    $ bad1 = "Luka succumbed to Alraune's tit fuck."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Ehehe, you made my breasts get all covered in semen.{image=note}{w}\nDid it feel too good for you to take?"

    jump alraune_h


label alraune_h3:
    $ world.party.player.moans(1)
    with syasei1
    show alraune bk02 at xy(X=147) zorder 10 as bk
    $ world.ejaculation()

    "As soon as Luka smelled Alraune's sweet fragrance, the overpowering ecstasy forces him to come."

    $ world.battle.lose()
    $ world.battle.count([18, 3])
    $ bad1 = "Just the smell of Alraune's sweet fragrance caused Luka to come."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Ehehe, I smelled so good, you just had to come right away.{image=note}{w}\nIt smelled so good, you came on accident?{image=note}"

    jump alraune_h


label alraune_h4:
    $ world.party.player.moans(3)
    with syasei1
    show alraune bk02 at xy(X=147) zorder 10 as bk
    $ world.ejaculation()

    "Entangled by Alraune's ivy, Luka accidentally climaxes..."

    $ world.battle.lose()
    $ world.battle.count([12, 3])
    $ persistent.count_bouhatu += 1
    $ bad1 = "The simple act of being bound by Alraune's ivy forced Luka to come."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Eh? You already came?{w}\nI guess that must have been too much for a weak boy like you. Hahaha.{image=note}"

    jump alraune_h


label alraune_h5:
    $ world.party.player.moans(2)
    with syasei1
    show alraune bk03 at xy(X=147) zorder 10 as bk
    show alraune ct03 at topleft zorder 15 as ct
    $ world.ejaculation()

    "Sucked intensely by the Succubus Flower, Luka finally gives in to the rising desire for release."

    $ world.battle.lose()
    $ world.battle.count([12, 3])
    $ bad1 = "Sucked by Alraune's Succubus Flower, Luka was forced to come."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Ehehe, my flower sucked in a lot.{image=note}{w}\nMy flower felt really good, didn't it?"

    hide ct
    jump alraune_h


label alraune_h:
    $ bad2 = "After that, Luka was raped by Alraune continually for the rest of his life..."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    l "Ahhh..."
    "Stuck in the afterglow of the orgasm, my body relaxes.{w}\nAs soon as I do, Alraune's petals start to spread out over my waist."

    play sound "audio/se/ero_makituki3.ogg"

    "In only a few seconds, I'm completely covered by her flowers and petals."
    world.battle.enemy[0].name "Now, next I'm going to use this to suck you dry...{w}\nThis hole down here is the best way to extract nutrients, after all.{image=note}"
    "Alraune moves some flowers to reveal a pink opening into her body."
    "It almost looks like a human female's, but inside the pink opening, something is wriggling and squirming inside."
    l "St...Stop!"
    world.battle.enemy[0].name "Eh? But your penis is already big.{w}\nDoesn't that mean it wants to be inserted in here?{w}\nIt wants to be sucked dry, until it's shriveled and wilts..."
    "Alraune leans on top of me, and aims my penis toward her small pink slit.{w}\nAlready erect, she easily forces me into her in one smooth movement."

    play hseanwave "audio/se/hsean01_innerworks_a1.ogg"
    hide ct
    hide bk
    show alraune h1

    world.battle.enemy[0].name "Ehehe. I swallowed your penis.{image=note}{w}\nNow, all of your sperm is officially mine!{image=note}"
    l "Ahhh...!!"
    "Nectar drips out of both her small pink opening and some of her flowers, giving off a sweet scent."
    "Wrapped in the pleasant stimulation from insertion, the sweet smells cause my body to relax."
    "But the feeling after insertion suddenly changes, as it feels like her vaginal walls themselves are coming alive."
    "From her entrance, to all the way inside, it feels like every bump, crease, and fold inside of her has a mind of its own, moving and rubbing every direction at once."
    l "Ahhh!!"
    "My body shakes involuntarily as I let out a pathetic moan at the sudden unexpected pleasure."
    world.battle.enemy[0].name "Hehehe... What a pitiful face.{w}\nAs soon as a man feels this, they always go insane with ecstasy.{image=note}"
    l "Haaa..."
    "It's as if I was forced into a massage, specially tailored for maximum stimulation."
    "Her soft, wriggling walls gently rub and massage me, urging me toward a quick orgasm."
    l "Ahhh... I... I'm going to come...!"
    world.battle.enemy[0].name "Hehe, that's good.{w}\nI need to suck out alllll of your semen, after all.{image=note}"
    "As if encouraged by my words, the speed of the massage increases, almost turning into a milking movement."
    "Unable to take it any more, I give in to the rising climax."
    l "Coming...!{w}\nAhhhh!"

    with syasei1
    show alraune h2
    $ world.ejaculation()

    "As soon as the first spurt of semen shoots into Alraune's vagina, her walls increase in force and speed into a strong milking movement, making sure every single drop is squeezed out of me into her."
    world.battle.enemy[0].name "Aha, you're coming, you're coming.{w}\nI'll milk you to the last drop.{image=note}"
    l "Ahhh... Y...You're wringing it all out..."
    "Designed just for extracting semen from men, her pussy squeezes out every drop of my last orgasm."
    l "Ahhh..."
    world.battle.enemy[0].name "Ehehe, your face looks so happy.{image=note}{w}\nIt makes me want to tease you a little.{image=note}"
    "Two long flowers come out from Alraune's lower body.{w}{nw}"

    show alraune h3
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nSuddenly, both of them latch onto my nipples!"
    "The unexpected touch on my chest causes me to shiver, and my penis gets hard again inside of Alraune's vagina."
    l "Wh...What are you doing?.."
    world.battle.enemy[0].name "These flowers are for using on girls.{w}\nThey latch onto their breasts, and are able to suck out milk."
    world.battle.enemy[0].name "I've never used them on a boy before...{w}\nI want to see what happens!"
    l "N...No way... Ahhh!"
    "The flowers on my left and right nipple latch on, and start to suck on them."
    "The soft, honey covered flowers put pressure on me, while her pussy starts its massaging motions again."
    "The three different sources of milking motions make it feel like my entire body is being exploited."
    l "Ahh! Ahhhh!!"
    "My body shivers as the sucking motions get more intense.{w}\nIf I were a women, would milk be coming out?.."
    "But of course, nothing comes out.{w}\nEven so, the flowers latched onto me don't give up, and continue to suck with ever increasing pressure."
    l "Ahhh! Stop!{w}\nAhhhhh!"
    world.battle.enemy[0].name "Milk really won't come out...{w}\nBut it looks like it feels good anyway.{image=note}"
    l "Ahhh!"
    "The flowers on my nipples continue to uselessly milk me, while her pussy continues to actually milk me."
    l "Stop it!{w}\nStop this!!"
    "The bizarre new stimulation on my breasts causes me to reach another orgasm, shooting milk out of my penis instead of my breasts..."

    with syasei1
    show alraune h4
    $ world.ejaculation()

    world.battle.enemy[0].name "Arara... So the milk came out down here.{w}\nI guess there's no harm in milking both places at once, then.{image=note}"
    l "Ahh! Stop!{w}\nAhhhh!!"
    "Thus began a nightmare of being sucked from three places at once..."
    "The flowers suck pointlessly on my nipples, while her pussy milks my penis..."

    show alraune h5

    "But then, more flowers come up to start sucking on my entire body!"
    l "Ahhhh!"
    world.battle.enemy[0].name "I'm milking your breasts like a girl...{w}\nBut just look at that reaction when I embrace your whole body. You're so shy.{image=note}"
    "Fine hairs inside the flowers start to tickle my body, sending a new stimulation racing throughout me."
    "Then, as if there were two tiny mouths inside, both of my nipples are sucked into a small opening."
    l "Ahhh! Ahhhhh!!"
    world.battle.enemy[0].name "Ahaha, you look so frantic all of a sudden.{w}\nDon't worry, you can shoot out your milk whenever it's too much to hold in.{image=note}"
    "Her pussy tightens after her words, as if emphasizing that it's hungry for more..."
    "Way too late to do anything about it, I realize that her whole body has evolved into an efficient way to milk men for their semen..."
    l "No more...! I'm going to come!"
    "While being sucked all over, I quickly given in again."

    with syasei1
    show alraune h6
    $ world.ejaculation()

    "The semen, the proof of my submission to her, is quickly sucked up by her waiting pussy."
    l "Ahh... Stop this... Someone help me..."
    "Overcome by the intensity of it all, I beg for Alraune to stop with tears in my eyes.{w}\nBut she just looks back at me with a smile on her face, not stopping the milking."
    world.battle.enemy[0].name "Not until you're empty.{w}\nI'll stop right before you would die, so don't worry.{image=note}"
    world.battle.enemy[0].name "I wouldn't waste a delicious source of milk like you."
    l "Ahhh... Help me..."

    with syasei1
    $ world.ejaculation()

    "Milked a final time, I feel myself about to faint.{w}\nThe endless barrage of pleasure finally starts to fade, as I slip into sleep."
    l "Ah... Ahhh..."
    "Wrapped in an endless ecstasy, I finally lose consciousness."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    world.battle.enemy[0].name "Arere?.. He fainted while I'm still sucking on him?{w}\nBut that's fine... I'll be milking him for forever from now on.{image=note}"
    "I hear those horrifying words right as my world fades to black."
    "Sleep will only be a short respite from this endless pleasure..."
    "............."

    jump badend


label jelly_start:
    python:
        world.troops[31] = Troop(world)
        world.troops[31].enemy[0] = Enemy(world)

        world.troops[31].enemy[0].name = Character("Девушка-желе")
        world.troops[31].enemy[0].sprite =  ["jelly st01", "jelly st02", "jelly st03", "jelly h1"]
        world.troops[31].enemy[0].position = center
        world.troops[31].enemy[0].defense = 100
        world.troops[31].enemy[0].evasion = 95
        world.troops[31].enemy[0].max_life = world.troops[31].battle.difficulty(2500, 2700, 2900)
        world.troops[31].enemy[0].power = world.troops[31].battle.difficulty(0, 1, 2)

        if persistent.difficulty < 3:
            world.party.player.earth_keigen = 40
        else:
            world.party.player.earth_keigen = 20

        world.troops[31].battle.tag = "jelly"
        world.troops[31].battle.name = world.troops[31].enemy[0].name
        world.troops[31].battle.background = "bg 097"
        world.troops[31].battle.music = 1
        world.troops[31].battle.exp = 10000
        world.troops[31].battle.exit = "lb_0053"

        world.troops[31].battle.init()

    world.battle.enemy[0].name "Человек, почему ты здесь?..{w}\nУ меня к тебе предложение: позволь мне испить немного твоего семени..."
    "Девушка-желе медленно приближается ко мне."
    "Прямо как и самый первый монстр, с которым я сражался, эта девушка - слизь...{w}\nЕё тело гибкое и способно по собственной воле изменять форму и вид..."
    l "Я бы хотел встретиться с Ундиной."

    $ world.battle.face(2)

    world.battle.enemy[0].name "Я поглощу твоё тело...{w}\nИ буду играть с тобой до тех пор, пока ты не кончишь..."
    l "........................"
    "Как и всегда, меня не слушают...{w}\nЧто ж, тогда пришла пора драться!"

    $ world.battle.face(1)
    $ world.battle.main()

label jelly_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label jelly_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Что... это?"

    play sound "se/syometu.ogg"
    hide jelly with crash

    "Девушка-желе растекается в лужу!"

    $ world.battle.victory(1)

label jelly_a:
    if world.party.player.bind == 1:
        call jelly_a4
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,3)

        if random == 1:
            call jelly_a1
            $ world.battle.main()
        elif random == 2:
            call jelly_a2
            $ world.battle.main()
        elif random == 3 and world.battle.delay[0] > 5:
            call jelly_a3
            $ world.battle.main()


label jelly_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Drown in my slime...",
        "I'll make you feel good inside my body...",
        "I feel nice and slimy..."
    ])

    $ world.battle.show_skillname("Jelly Press")
    play sound "audio/se/ero_slime1.ogg"

    "The Jelly Girl spreads her slime all over Luka's body!"

    "The wriggling slime massages Luka all over his body!"

    $ world.battle.skillcount(2, 3371)

    python:
        world.battle.enemy[0].damage = {1: (75, 85), 2: (85, 95), 3: (95, 105)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label jelly_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll make your penis all slimy...",
        "Leak your semen into my slime...",
        "Is it good?.. Are you going to come?.."
    ])

    $ world.battle.show_skillname("Jelly Draw")
    play sound "audio/se/ero_slime1.ogg"

    "The Jelly Girl's slime covers Luka's penis!"

    $ world.battle.skillcount(2, 3372)

    python:
        world.battle.enemy[0].damage = {1: (80, 90), 2: (90, 100), 3: (100, 110)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label jelly_a3:
    world.battle.enemy[0].name "I'll show you heaven..."

    $ world.battle.show_skillname("Jelly Heaven")
    play sound "audio/se/ero_slime3.ogg"

    "The Jelly Girl falls on Luka, absorbing his body into her own!"

    $ world.battle.skillcount(2, 3373)

    $ world.battle.enemy[0].damage = {1: (80, 90), 2: (90, 100), 3: (100, 110)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Luka's stuck inside the Jelly Girl!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    if world.battle.first[0] == 1:
        world.battle.enemy[0].name "This time you won't escape...{w}\nNow let me play with you..."
    else:
        world.battle.enemy[0].name "It feels good inside me, doesn't it?..{w}\nNow let me play with you..."

    $ world.battle.first[0] = 1

    if world.party.player.earth == 0 and persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 3
    elif world.party.player.earth == 0 and persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 4
    elif world.party.player.earth == 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 5
    elif world.party.player.earth > 0 and persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 1
    elif world.party.player.earth > 0 and persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 2
    elif world.party.player.earth > 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return


label jelly_a4:
    "Luka's stuck inside the Jelly Girl..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "I can control my body...",
        "I'll tease your penis...",
        "If I do this, will you come?..",
        "Slime can even go inside your ass...",
        "I can spin it around..."
    ])

    $ world.battle.show_skillname("Jelly Heaven")
    play sound "audio/se/ero_slime3.ogg"

    $ world.battle.cry(narrator, [
        "The soft slime tightens around Luka's body!",
        "The jelly-like slime tightens around Luka's penis!",
        "The soft slime tightens around Luka's penis!",
        "The soft slime slowly seeps into Luka's ass!",
        "The slime around Luka's body starts to slowly spin around him!"
    ], True)

    $ world.battle.skillcount(2, 3373)

    python:
        world.battle.enemy[0].damage = {1: (80, 90), 2: (90, 100), 3: (100, 110)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label jelly_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Значит я могу выжать и поглотить всю твою сперму?"

    $ world.battle.enemy[0].attack()

label jelly_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Jelly Press"
    $ list2 = "Jelly Draw"
    $ list3 = "Jelly Heaven"

    if persistent.skills[3371][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3372][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3373][0] > 0:
        $ list3_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump jelly_onedari1
    elif world.battle.result == 2:
        jump jelly_onedari2
    elif world.battle.result == 3:
        jump jelly_onedari3
    elif world.battle.result == -1:
        jump jelly_main


label jelly_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want my slime all over you?"

    while True:
        call jelly_a1


label jelly_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want my slime all over your penis?"

    while True:
        call jelly_a2


label jelly_onedari3:
    call onedari_syori_k

    world.battle.enemy[0].name "You want to be squeezed inside me?"

    if not world.party.player.bind:
        call jelly_a3

    while True:
        call jelly_a4


label jelly_h1:
    $ world.party.player.moans(1)
    with syasei1
    show jelly bk02 zorder 10 as bk
    $ world.ejaculation()

    "As the Jelly Girl's slime massages him all over, Luka comes, covering her with semen."

    $ world.battle.lose()
    $ world.battle.count([11, 3])
    $ bad1 = "Luka succumbed to the Jelly Girl's slime."
    $ world.battle.face(2)

    world.battle.enemy[0].name "You came a lot...{w}\nDid it feel good?"

    jump jelly_h


label jelly_h2:
    $ world.party.player.moans(1)
    with syasei1
    show jelly bk03 zorder 10 as bk
    $ world.ejaculation()

    "With the slime wriggling around his penis, Luka comes, covering her with semen."

    $ world.battle.lose()
    $ world.battle.count([11, 3])
    $ bad1 = "Luka succumbed to the Jelly Girl's slime."
    $ world.battle.face(2)

    world.battle.enemy[0].name "You came a lot...{w}\nDid it feel good?"

    jump jelly_h


label jelly_h3:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Already?.."

    with syasei1
    show jelly h2
    $ world.ejaculation()

    "As soon as his body is wrapped in the Jelly Girl's slime, Luka comes, filling her body with semen."

    $ world.battle.lose()
    $ world.battle.count([11, 3])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka was forced to come as soon as he was sucked into the Jelly Girl's body."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Already coming...{w}\nDid it feel that good?"

    jump jelly_h


label jelly_h4:
    $ world.party.player.moans(2)
    with syasei1
    show jelly h2
    $ world.ejaculation()

    "With the slime wriggling and tightening all around his body, Luka is forced to come, filling her body with semen."

    $ world.battle.lose()
    $ world.battle.count([11, 3])
    $ bad1 = "Luka was forced to come while being held inside the Jelly Girl's body."
    $ world.battle.face(2)

    world.battle.enemy[0].name "So much semen...{w}\nIt looks like I felt too good for you..."

    jump jelly_h


label jelly_h:
    $ bad2 = "Encased in her slime, Luka's semen is squeezed out endlessly."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.party.player.bind:
        play hseanwave "audio/se/hsean18_slime1.ogg"
        hide bk
        show jelly h3

        jump jelly_hb

    l "Ahh..."
    "Completely exhausted, I look on helplessly as her giant looming slimy body slowly moves into my own.{w}{nw}"

    play hseanwave "audio/se/hsean18_slime1.ogg"
    hide bk
    show jelly h3
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nJust like that, my body is absorbed by the Jelly Girl's!"
    l "Uaa!"


label jelly_hb:
    "The Jelly Girl's soft slimy body slowly tightens around me, holding me in place inside her."
    world.battle.enemy[0].name "Your penis... It's still big..."
    "The soft slime around my penis starts to compress.{w}\nIt slowly thickens until my penis is surrounded by and pushed on all sides by her elastic slime."
    world.battle.enemy[0].name "Does my slime... Feel good?..{w}\nI'll make it... Even better..."
    l "Ahhhh!"
    "The slime around my penis starts to wriggle, making wet sounds as the soft jelly pulses around me."
    "The soft slime gently tightens around me, as it freely flows all directions around my penis."
    l "Ahh...! That feels good...!"
    "This stimulation is enough to make any man surrender...{w}\nI too can't bring myself to resist the slime squirming around my lower body."
    world.battle.enemy[0].name "When you can't take it any longer, just come..."
    l "Haaa..."
    "At times tightening, and at times swirling around and massaging me, the Jelly Girl freely controls her own body to play with me."
    "The liquid nature of the slime even lets it poke inside the tip of my penis, gently rubbing against the most sensitive spot on my body."
    l "Haaa!{w} Ahhh!"
    world.battle.enemy[0].name "I'll make your penis feel good..."
    "The slime around my body continues to freely change its shape as it attacks me however the Jelly Girl wants."
    "With nothing but her warm slime body around me, I can only shake my head helplessly as I'm forced to a quick orgasm."
    l "Ahhh... I'm coming..."
    world.battle.enemy[0].name "Fill my blue body with your white semen..."
    "Her body movements get even faster, trying to force me to come right away."
    "Enveloped in her warm body, I don't have any choice but to give in to the stimulation."

    with syasei1
    show jelly h4
    $ world.ejaculation()

    "Semen explodes from the tip of my penis, only able to come out an inch into her body before it stops."
    "All during my ejaculation, her soft slime continues to wriggle and squeeze me, forcing every drop of semen out into her waiting body."
    l "Haa..."
    world.battle.enemy[0].name "You can come more, right?..{w} Come more..."

    play hseanwave "audio/se/hsean19_slime2.ogg"

    "As I helplessly float in her body, her slime starts massaging my body even more violently than before."
    "Overpowered by the warm, comfortable slime, just trying to struggle a little bit only makes it feel like I'm thrusting inside her."
    "With my entire body encased in her springy slime, there's no escape.{w}\nI can't break free of her body any longer..."
    l "Ahhh!"
    "The soft slime around my penis gets a little bit tighter as it swirls around my penis, forcing me to another quick orgasm."
    l "Haaaa... Again... Coming again..."

    with syasei1
    show jelly h5
    $ world.ejaculation()

    "I let out a soft moan as I come inside her again.{w}\nLooking down in shame, I watch the semen slowly bubble up in her transparent body."
    "Her blue body moves around inside, milking out the last drop of semen from my orgasm, all while I look on, watching it mix with her body."
    l "Haaa..."
    world.battle.enemy[0].name "You can come more, right?..{w}\nI'll squeeze it all out, don't worry..."
    l "Stop it already..."
    "But her merciless squeezing continues.{w}\nWith my body encased in hers, I float here helplessly as she skillfully manipulates her free flowing body to squeeze and milk out my semen."
    l "Ahhh..."
    "And before I was even aware of it, I had given up.{w}\nWith no way to escape, I relax inside her warm body as her slime fills me with pleasure."

    with syasei1
    show jelly h6
    $ world.ejaculation()

    "Relaxing inside her body, I quickly come again.{w}\nI look on in ecstasy as my white semen mixes with her blue slime."
    l "Ahhh..."
    "Weakened after my repeated orgasms, I don't even have the strength left to move inside her soft body.{w}\nGiven complete control, her slime freely plays and toys with my body."
    world.battle.enemy[0].name "Come on... Come more...{w}\nFill me with everything..."
    l "Ahhh..."

    with syasei1
    $ world.ejaculation()

    "Coming a final time, I lose consciousness.{w}\nThe only sensation I can feel as I drift off into sleep is her warm slime wriggling around my body."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Caught by the Jelly Girl, I'll be squeezed like this for the rest of my life...{w}\nNot killed or released, I'm stuck inside her soft body forever."
    "Stuck inside her body, I've become little more than prey for her to feed on all day.{w}\nI quickly went insane on the pleasure, and became little more than a slave to pleasure."
    "Today too, the Jelly Girl is squeezing me dry inside her body in the underground cave.{w}\nStuck inside the cave nobody visits with the Jelly Girl, my adventure ends here."
    "............."

    jump badend


label blob_start:
    python:
        world.troops[32] = Troop(world)
        world.troops[32].enemy[0] = Enemy(world)

        world.troops[32].enemy[0].name = Character("Девушка-капля")
        world.troops[32].enemy[0].sprite =  ["blob st01", "blob st02", "blob st03", "blob h1"]
        world.troops[32].enemy[0].position = center
        world.troops[32].enemy[0].defense = 100
        world.troops[32].enemy[0].evasion = 95
        world.troops[32].enemy[0].max_life = world.troops[32].battle.difficulty(2700, 2950, 3100)
        world.troops[32].enemy[0].power = world.troops[32].battle.difficulty(0, 1, 2)

        if persistent.difficulty < 3:
            world.party.player.earth_keigen = 40
        else:
            world.party.player.earth_keigen = 20

        world.troops[32].battle.tag = "blob"
        world.troops[32].battle.name = world.troops[32].enemy[0].name
        world.troops[32].battle.background = "bg 097"
        world.troops[32].battle.music = 1
        world.troops[32].battle.exp = 12000
        world.troops[32].battle.exit = "lb_0054"

        world.troops[32].battle.init()

    $ world.battle.face(2)

    world.battle.enemy[0].name "Аха-ха, какой вкусный на вид человек~{image=note}.{w}\nЭто уже второй за месяц~{image=note}."
    "Она слегка похожа на девушку-желе, которую я встретил до этого. Но всё же немного отличается..."
    "Консистенция её тела больше походит на липкий плавленый сыр, чем на желе...{w}\nСтрашно."
    world.battle.enemy[0].name "Я поглощу и растворю тебя тоже.{w}\nТы выглядишь очень аппетитно~{image=note}."
    "Даже если бы она на меня не напала, пожирать других людей непростительно.{w}\nЯ вынимаю свой меч, пока ужасающая на вид девушка-капля приближается ко мне!"

    $ world.battle.face(1)
    $ world.battle.main()

label blob_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label blob_v:
    $ world.battle.face(3)

    world.battle.enemy[0].name "Как я могла проиграть человеку?!.."

    play sound "se/syometu.ogg"
    hide blob with crash

    "Девушка-капля растекается в лужу!"

    $ world.battle.victory(1)

label blob_a:
    if world.party.player.bind == 1:
        call blob_a4
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,3)

        if random == 1:
            call blob_a1
            $ world.battle.main()
        elif random == 2:
            call blob_a2
            $ world.battle.main()
        elif random == 3 and world.battle.delay[0] > 5:
            call blob_a3
            $ world.battle.main()

label blob_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Have a taste of my slime.{image=note}",
        "From top to bottom, I'll cover you with plenty of slime.{image=note}",
        "Drown in the pleasure of a slime.{image=note}"
    ])

    $ world.battle.show_skillname("Blob Spread")
    play sound "audio/se/ero_slime2.ogg"

    "The Blob Girl spreads her slime all over Luka's body!"

    "The sticky slime wriggles all over Luka's body!"

    $ world.battle.skillcount(2, 3374)

    python:
        world.battle.enemy[0].damage = {1: (75, 85), 2: (85, 95), 3: (95, 105)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label blob_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Aha... Can you endure this?",
        "When you can't take it anymore, it's fine to come inside my slime.{image=note}",
        "Does my slime feel good?"
    ])

    $ world.battle.show_skillname("Blob Draw")
    play sound "audio/se/ero_slime1.ogg"

    "The Blob Girl's slime covers Luka's groin!"

    "As the slime clings to his penis, it vibrates!"

    $ world.battle.skillcount(2, 3375)

    python:
        world.battle.enemy[0].damage = {1: (80, 90), 2: (90, 100), 3: (100, 110)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label blob_a3:
    if not world.party.player.surrendered:
        if world.battle.first[0] == 1:
            world.battle.enemy[0].name "This time, I'll engulf you.{image=note}"
        else:
            world.battle.enemy[0].name "Time to engulf you.{image=note}"

        $ world.battle.first[0] = 1

    $ world.battle.show_skillname("Blob Heaven")
    play sound "audio/se/ero_slime3.ogg"

    "The Blob Girl falls onto Luka, enveloping his body!"

    $ world.battle.skillcount(2, 3376)

    $ world.battle.enemy[0].damage = {1: (100, 110), 2: (110, 120), 3: (120, 130)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Luka's been absorbed into the Blob Girl's body!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    $ world.battle.face(2)

    world.battle.enemy[0].name "Your body is inside mine.{image=note}{w}\nI'll rub and tighten around you until you're dissolved, so get ready.{image=note}"

    if world.party.player.earth == 0:
        $ world.battle.enemy[0].power = 100
    elif world.party.player.earth > 0 and persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 1
    elif world.party.player.earth > 0 and persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 2
    elif world.party.player.earth > 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return


label blob_a4:
    "Luka is stuck inside the Blob Girl..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "If I do it like this, you won't be able to stand it, will you?",
        "Once I wrap someone, there's no human who can endure me.{image=note}",
        "Hehe... I'll turn you into mush inside me.{image=note}",
        "Drown inside of me.{image=note}",
        "I'll melt you into nothing.{image=note}"
    ])

    $ world.battle.show_skillname("Blob Heaven")
    play sound "audio/se/ero_slime3.ogg"

    $ world.battle.cry(narrator, [
        "The slime rotates around his penis!",
        "The slime tightens around his penis!",
        "The slime rubs against Luka's whole body, as if he was covered in tongues!",
        "The slime flows around Luka's body, as if playing with him!",
        "Wrapped in her body, the slime toys with Luka's body!"
    ], True)

    $ world.battle.skillcount(2, 3376)

    python:
        world.battle.enemy[0].damage = {1: (100, 110), 2: (110, 120), 3: (120, 130)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label blob_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Хочешь, чтобы я тебя растворила?{w}\nЛадно... я сделаю это.{image=note}"

    $ world.battle.enemy[0].attack()

label blob_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Blob Spread"
    $ list2 = "Blob Draw"
    $ list3 = "Blob Heaven"

    if persistent.skills[3374][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3375][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3376][0] > 0:
        $ list3_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump blob_onedari1
    elif world.battle.result == 2:
        jump blob_onedari2
    elif world.battle.result == 3:
        jump blob_onedari3
    elif world.battle.result == -1:
        jump blob_main


label blob_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want my slime all over you?"

    while True:
        call blob_a1


label blob_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want my slime all over your penis?"

    while True:
        call blob_a2


label blob_onedari3:
    call onedari_syori_k

    world.battle.enemy[0].name "You want to melt into mush inside me?"

    if not world.party.player.bind:
        call blob_a3

    while True:
        call blob_a4


label blob_h1:
    $ world.party.player.moans(1)
    with syasei1
    show blob bk03 zorder 10 as bk
    $ world.ejaculation()

    "As the slime covers his body, Luka comes, covering her with semen."

    $ world.battle.lose()
    $ world.battle.count([11, 11])
    $ bad1 = "Luka was forced to come by the Blob Girl's blob spread skill."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Haha, my slime wriggling all over your body made you come.{image=note}{w}\nIt felt amazing, didn't it?{w} My slime.{image=note}"

    jump blob_h


label blob_h2:
    $ world.party.player.moans(1)
    with syasei1
    show blob bk03 zorder 10 as bk
    $ world.ejaculation()

    "As the slime covers and wriggles around his penis, Luka comes, covering her with semen."

    $ world.battle.lose()
    $ world.battle.count([11, 11])
    $ bad1 = "Luka was forced to come by the Blob Girl's blob draw skill."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Haha, my slime wriggling all over your penis made you come.{image=note}{w}\nIt felt amazing, didn't it?{w} My slime.{image=note}"

    jump blob_h


label blob_h3:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Arere?..{w} Already coming?"

    with syasei1
    show blob h2
    $ world.ejaculation()

    "As soon as Luka's body is engulfed by the Blob Girl's, he comes, filling her with semen."

    $ world.battle.lose()
    $ world.battle.count([11, 11])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka was forced to an orgasm as soon as the Blob Girl engulfed him."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Ahaha, did it really feel that good?{w}\nYou couldn't endure it even a little bit.{image=note}"
    world.battle.enemy[0].name "Now then, it's time to digest you.{image=note}"

    jump blob_h


label blob_h4:
    $ world.party.player.moans(2)
    with syasei1
    show blob h2
    $ world.ejaculation()

    "With his body stuck inside the Blob Girl, Luka is forced to come, filling her body with semen."

    $ world.battle.lose()
    $ world.battle.count([11, 11])
    $ bad1 = "Stuck inside the Blob Girl's body, Luka was forced to come."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Ahaha, it was amazing, wasn't it?{w}\nIt felt so good, it was like you were melting, didn't it?"
    world.battle.enemy[0].name "Now then... It's time to really melt you.{image=note}"

    jump blob_h


label blob_h:
    $ bad2 = "Dissolved inside her body, Luka was eaten by the Blob Girl."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.party.player.bind:
        play hseanwave "audio/se/hsean18_slime1.ogg"
        hide bk
        show blob h3

        jump blob_hb

    l "Ahh..."
    world.battle.enemy[0].name "Hehehe... Now, it's time to eat.{w}\nI'll wrap you in my body and dissolve you..."

    play hseanwave "audio/se/hsean18_slime1.ogg"
    hide bk
    show blob h3

    "The Blob Girl's wriggling body moves onto mine, sucking me in."
    "Rather than feeling like a liquid, her slime feels like warm flesh.{w}\nThat warm flesh wraps around my body, absorbing me inside her."


label blob_hb:
    l "Ahhh!{w} Stop!"
    "Despite struggling with all of my strength, my limbs just helplessly move through her body, unable to get me out."
    world.battle.enemy[0].name "Before I eat you, I'll rape you.{w}\nI'll violate that penis of yours..."
    l "Stop... Ahhh!"
    "Her warm slime starts to violently wriggle as it swirls and tightens around my penis.{w}\nAs the sticky warmth rubs against me, I involuntarily let out a moan."
    world.battle.enemy[0].name "Haha, already moaning?{w}\nBut I can make it feel even better...{image=note}"
    world.battle.enemy[0].name "If I wrap it like this...{w}\nI'll be able to rape that penis nice and good.{image=note}"
    l "Ahhh!"
    "The sticky slime wraps around my penis...{w}\nEntangling, rubbing, squeezing...{w} The Blob Girl varies the stimulations as she giggles and watches."
    world.battle.enemy[0].name "Hehe... It feels good, doesn't it?{w}\nMake sure you give me plenty of that delicious semen in return.{image=note}"
    l "Ahhh..."
    "With the viscous slime wriggling and squirming around me, it feels like I'm being raped inside her body."
    "Trembling, her warm sticky interior is forcing me to the edge."
    l "Ahh... I'm going to come..."
    world.battle.enemy[0].name "Good... Let it out inside me.{image=note}"
    "She squeezes the warm slime inside her around me in a final stimulus, forcing me to come."

    with syasei1
    show blob h4
    $ world.ejaculation()

    "I explode inside her warm body, shooting my semen out into her red slime."
    l "Haa..."
    "My body relaxes after my orgasm, feeling the afterglow as her warm body continues to squirm around me."
    world.battle.enemy[0].name "Hehe, was it good?{w}\nMy rape is unbearable, isn't it?"
    world.battle.enemy[0].name "But it isn't over yet...{w}\nI'm going to quickly squeeze you dry, then dissolve your body.{image=note}"
    l "D...Don't... Ahh!"
    "Suddenly, I feel the slime around my penis start to move in a new pattern.{w}\nThe sticky slime starts to slowly spin around the tip of my penis, as if creating a tiny whirlpool."
    "At times it tightens around me, but the sticky slime slowly spins faster and faster."
    l "Aa... Ahhh!"
    world.battle.enemy[0].name "Haha, what a pathetic moan.{w}\nCome on... Shoot out that semen quickly!{image=note}"
    "I tremble inside her warm body as the whirlpool around my penis increases in speed.{w}\nHelpless inside her sticky slime, I can only feebly shake side to side as she forces me to another orgasm."
    l "Ahh... I'm going to come again..."
    world.battle.enemy[0].name "Good, let out a lot!{image=note}"
    l "N...No!{w} Ahhh!"

    with syasei1
    show blob h5
    $ world.ejaculation()

    world.battle.enemy[0].name "Ahaha, you came again.{image=note}{w}\nBut I can still squeeze out a lot more.{image=note}"
    l "Ahh... Ahhh!"
    "The vortex of slime inside her speeds up even faster.{w}\nUnable to take it any longer, all I can do is yell and shake as the stimulation overpowers me."
    "I'm being dominated by her body, pathetically flailing around inside her as she looks on and laughs."
    l "Ahhh... No more..."
    world.battle.enemy[0].name "I told you I'd squeeze out more, didn't I?{w}\nCome on, come on, I know you have more!{image=note}"
    l "Ahhh!"

    with syasei1
    show blob h6
    $ world.ejaculation()

    world.battle.enemy[0].name "...Not too much came out that time.{w}\nI guess I can squeeze out a bit more as I digest you."
    l "N...No.."
    "I shudder at the horrifying words she calmly speaks.{w}\nI'm going to be... Eaten by her?"
    world.battle.enemy[0].name "Now then, it's time for my meal.{image=note}{w}\nI'll dissolve your body into mush, then eat you.{image=note}"
    l "Ahhh!{w} Stop!"

    play hseanwave "audio/se/hsean19_slime2.ogg"
    show blob h7

    "Ignoring my pleas, her body sucks me deeper inside."
    "The pressure around me is even more than before, and so is the warmth..."

    if persistent.vore == 1:
        jump vore

    world.battle.enemy[0].name "My body itself is a digestive organ.{w}\nSo you're already inside my stomach itself.{image=note}"
    l "S...Stop this... Let me go!"
    "Terrified, I struggle with everything I have to get away.{w}\nI'm struggling with everything I have... But..."
    world.battle.enemy[0].name "Ahaha, struggling is pointless.{w}\nThere's nothing left you can do but be digested.{image=note}"

    show blob h8

    "As I start to yell out, her warm slime covers my face itself.{w}\nNow even deeper inside her body, no amount of struggling can free me."
    "The warm slime covers every inch of my body except for my mouth, wriggling and squirming all over me.{w}\nThe stickiness causes a sweet sensation to flood over me as the slime pulls me deeper into her body."
    "The strange comfort brought by being wrapped inside of her slows my movements, until finally I can't move at all."
    l "Aaa... Uuu..."
    world.battle.enemy[0].name "Hehe, I've already started digesting you.{w}\nWell?{w} How does it feel to be dissolved alive?"
    l "Ahh... Hel...p me..."
    "Wrapped in the calming sensation of her warm slime, I barely manage to speak those final, desperate words."
    world.battle.enemy[0].name "Hahaha.{w} No.{image=note}"

    show blob h9

    "At last, I'm sucked in so far into her body that I can't even see the outside world any longer."
    "Drowning in her body, I give in to the ruinous relaxing feeling being forced into me."
    l "Haaa..."
    "The terror I was feeling before melts away.{w}\nAlong with my body, everything is dissolving into nothingness."
    world.battle.enemy[0].name "Hehe, you taste really good.{image=note}{w}\nIt feels good being digested, doesn't it?"
    l "A...aa...{w}\nGood..."

    with syasei1
    $ world.ejaculation()

    "Invited to pleasure by her squirming body, I get a final orgasm."
    l "Aa...a..."
    "Giving up, I let the pleasant feelings fill me.{w}\nI even start to think that this is the best thing in the world..."
    "My body is being melted, and pleasure is taking its place...{w}\nThat's all I can feel anymore, so that's all that matters..."
    world.battle.enemy[0].name "Aha, this is the end.{w}\nTime to dissolve the last bits of you.{image=note}"
    l "A...a..."
    "At last, my body is dissolved into nourishment for the Blob Girl to use.{w}\nDigested by the Blob Girl, I was in bliss until the very end."

    stop hseanwave fadeout 1

    world.battle.enemy[0].name "That was delicious.{image=note}{w}\nIt's been a while since I had a boy that tasty.{image=note}"

    scene bg black with Dissolve(3.0)

    "..................."

    jump badend


label slime_green_start:
    python:
        world.troops[33] = Troop(world)
        world.troops[33].enemy[0] = Enemy(world)

        world.troops[33].enemy[0].name = Character("Зелёная девушка-слизь")
        world.troops[33].enemy[0].sprite =  ["slime_green st01", "slime_green st02", "slime_green st03", "slime_green st02"]
        world.troops[33].enemy[0].position = center
        world.troops[33].enemy[0].defense = 100
        world.troops[33].enemy[0].evasion = 95
        world.troops[33].enemy[0].max_life = world.troops[33].battle.difficulty(1200, 1280, 1330)
        world.troops[33].enemy[0].power = world.troops[33].battle.difficulty(0, 1, 2)

        if persistent.difficulty < 3:
            world.party.player.earth_keigen = 40
        else:
            world.party.player.earth_keigen = 20

        world.troops[33].battle.tag = "slime_green"
        world.troops[33].battle.name = world.troops[33].enemy[0].name
        world.troops[33].battle.background = "bg 097"
        world.troops[33].battle.music = 1
        world.troops[33].battle.exp = 15000
        world.troops[33].battle.exit = "lb_0055"

        world.troops[33].battle.init()

    world.battle.enemy[0].name "Э?..{w} Что человек делает здесь?"
    l "Я пришёл сюда чтобы найти Ундину и одолжить её силу."
    world.battle.enemy[0].name "... Ундина ненавидит людей.{w}\nТак что такой человек, как ты, никогда не встретится с ней."
    world.battle.enemy[0].name "Но лично я люблю людей.{w}\nЯ буду тебя дразнить, чтобы услышать твои милые стоны~{image=note}."
    l ".........."
    $ world.party.player.skill16(say=False)

label slime_green_v:
    $ world.battle.face(3)
    world.battle.enemy[0].name "Ой..."
    play sound "se/syometu.ogg"
    hide slime_green with crash
    "Зелёная девушка-слизь растекается в лужу!"
    stop music fadeout 1.0
    play sound "se/ero_slime3.ogg"
    q "... Я тебя предупреждала, но ты всё ещё продолжаешь идти дальше.{w}\nК тому же, тебе хватило смелости поднять свой меч на моих сестёр."
    "Внезапно, гнетущая аура наполняется воздух."
    l "Эрубети..."
    $ world.battle.victory(1)

label erubetie_ng_start:
    python:
        world.troops[34] = Troop(world)
        world.troops[34].enemy[0] = Enemy(world)

        world.troops[34].enemy[0].name = Character("Эрубети")
        world.troops[34].enemy[0].sprite =  ["erubetie st01"] * 4
        world.troops[34].enemy[0].position = center
        world.troops[34].enemy[0].defense = 90
        world.troops[34].enemy[0].evasion = 80
        world.troops[34].enemy[0].max_life = world.troops[34].battle.difficulty(36000, 38000, 40000)
        world.troops[34].enemy[0].henkahp[0] = world.troops[34].battle.difficulty(27000, 28500, 0)
        world.troops[34].enemy[0].henkahp[1] = world.troops[34].battle.difficulty(18000, 19000, 0)
        world.troops[34].enemy[0].henkahp[2] = world.troops[34].battle.difficulty(10000, 10000, 0)
        world.troops[34].enemy[0].power = world.troops[34].battle.difficulty(0, 1, 2)

        if persistent.difficulty < 3:
            world.party.player.earth_keigen = 30
        else:
            world.party.player.earth_keigen = 25

        world.troops[34].battle.tag = "erubetie_ng"
        world.troops[34].battle.name = world.troops[34].enemy[0].name
        world.troops[34].battle.background = "bg 097"

        if persistent.music:
            world.troops[34].battle.music = 25
        else:
            world.troops[34].battle.music = 4

        world.troops[34].battle.exp = 1750000
        world.troops[34].battle.exit = "lb_0056"
        world.troops[34].battle.ng = True

        world.troops[34].battle.init(0)

    world.battle.enemy[0].name "Хорошо.{w}\nЕсли ты так сильно хочешь умереть, тогда я с радостью исполню твою мольбу."
    l "Я здесь, чтобы повидаться с Ундиной, а не вредить источнику или играть в твои глупые игры!{w}\nПросто дай мне пройти!"
    "Я понимаю, что это бесполезно. Но я всё ещё не могу не попытаться переубедить её."
    world.battle.enemy[0].name "... Как ты смеешь так разговаривать со мной?!.."
    "На мгноение я вижу, как на её вечно безразличном лице вспыхивает выражение гнева."
    world.battle.enemy[0].name "... Очень хорошо.{w}\nТогда я сделаю твою смерть настолько мучительной, насколько это возможно."
    $ world.battle.show_skillname("Мор")
    play sound "se/ero_makituki5.ogg"
    "Токсичная жидкость цепляется за рот Луки!"
    l "Аргх!"
    "Прежде чем я успеваю среагировать, Эрубети насильно проталкивает её в моё горло.{w}\nЧтобы избежать удушья, я был вынужден проглотить это."
    pause .5

    $ world.troops[34].enemy[0].aqua = 1
    $ world.troops[34].enemy[0].aqua_turn = -1
    $ world.party.player.status = 9
    $ world.party.player.poison = 1
    $ world.party.player.poison_turn = -1
    $ world.party.player.status_print()

    play sound "se/aqua3.ogg"
    "Лука отравлен!"
    $ world.battle.hide_skillname()
    l "Что ты сделала со мной?!"
    world.battle.enemy[0].name "Загрязнённая слизь отравит твоё тело и медленно разрушит его.{w}\nКогда ты будешь достаточно ослаблен, что даже не сможешь продолжать сражаться, я поглощу тебя."
    l "Гах..."
    "У Эрубети очень извращённый ум...{w}\nНо я не могу так просто сдаться!"
    l "Если мне придётся победить тебя, чтобы продолжить путь, то так тому и быть!"
    $ world.party.player.skill_set(2)
    "Я вынимаю свой меч, приготовившись к битве."
    $ world.battle.stage[1] = 0
    $ world.battle.main()

label erubetie_ng_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label erubetie_ng_v:
    $ world.battle.face(3)
    world.battle.enemy[0].name "Аргх!{w}\nКак это возможно?!"
    world.battle.enemy[0].name "Для обычного человека иметь такую силу..."
    $ world.battle.victory(1)

label erubetie_ng001:
    $ world.battle.stage[1] = 1
    l "Эрубети, пожалуйста, послушай меня!{w}\nЯ не хочу сражаться с тобой или вредить девушкам-слизям! Эта не та причина, по которой я здесь!"
    world.battle.enemy[0].name "Жалкие молитвы, которыми ты пытаешься искупить свои грехи...{w}\nНеужели ты ожидаешь, что я поверю в них, после того, как ты напал на моих подчинённых?{w}\nТвоя просьба оскорбительна!"
    l "........."
    return

label erubetie_ng002:
    $ world.battle.stage[1] = 2
    l "Неужели ты думаешь, что я хотел с ними сражаться?!{w}\nЭти девушки-слизи первые меня атаковали! Я просто защищал себя!"
    world.battle.enemy[0].name "Жалкое оправдание...{w}\nКак будто бы они угрожали твоей жизни."
    world.battle.enemy[0].name "Ты дерёшься со мной, но, несмотря на это, тебе хватает смелости обвинять моих подчинённых?!"
    l "Но тогда что насчёт остальных людей, которые пропали в этом источнике?{w}\nЧто они такого сделали, чем заслужили смерть?!"
    world.battle.enemy[0].name "Люди ступили на нашу священную землю.{w}\nОни заслужили свою смерть за это святотатство."
    world.battle.enemy[0].name "И вскоре ты присоединишься к ним."
    return

label erubetie_ng003:
    $ world.battle.stage[1] = 3
    l "Ты серьёзно думаешь, что беспричинная жестокость решит вашу проблему?{w}\nЧто дало убийство этих людей?{w}\nЭто не вернёт ваши места обитания..."
    world.battle.enemy[0].name "Умолкни!"
    world.battle.enemy[0].name "Если у нас не будет шанса выжить, тогда я самолично поглощу и растворю каждого живущего человека на этой планете.{w} Поэтому я уничтожу любого, кто встанет у меня на пути!"
    return

label erubetie_ng_a:
    if world.battle.enemy[0].life < world.battle.enemy[0].henkahp[2] and world.battle.stage[1] == 2:
        call erubetie_ng003
    elif world.battle.enemy[0].life < world.battle.enemy[0].henkahp[1] and world.battle.stage[1] == 1:
        call erubetie_ng002
    elif world.battle.enemy[0].life < world.battle.enemy[0].henkahp[0] and world.battle.stage[1] == 0:
        call erubetie_ng001

    if not world.party.player.bind:
        hide clone

    if world.party.player.bind == 1:
        call erubetie_ng_a7
        $ world.battle.main()
    elif world.party.player.bind == 2:
        call erubetie_ng_a10
        $ world.battle.main()
    elif world.party.player.bind == 3:
        call erubetie_ng_a11
        $ world.battle.main()
    elif world.party.player.bind == 4:
        call erubetie_ng_a12
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,7)

        if random == 1:
            call erubetie_ng_a1
            $ world.battle.main()
        elif random == 2:
            call erubetie_ng_a2
            $ world.battle.main()
        elif random == 3:
            call erubetie_ng_a3
            $ world.battle.main()
        elif random == 4:
            call erubetie_ng_a4
            $ world.battle.main()
        elif random == 5:
            call erubetie_ng_a5
            $ world.battle.main()
        elif random == 6:
            call erubetie_ng_a6
            $ world.battle.main()
        elif random == 7:
            call erubetie_ng_a9
            $ world.battle.main()

label erubetie_ng_ab:
    while True:
        $ random = rand().randint(1,3)
        if random == 1:
            call erubetie_ng_a1
            $ world.battle.main()
        elif random == 2:
            call erubetie_ng_a2
            $ world.battle.main()
        elif random == 3:
            call erubetie_ng_a3
            $ world.battle.main()

label erubetie_ng_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я использую свою слизь, чтобы поиграть с твоим членом...",
        "Как долго ты сможешь выдержать мою слизь?",
        "Наполни мою слизь своей спермой..."
    ])

    $ world.battle.show_skillname("Вытягивание Агарты")
    play sound "audio/se/ero_slime1.ogg"
    "Слизь Эрубети сжимает член Луки!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 51:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 46:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 41:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (540, 570), 2: (560, 590), 3: (580, 610)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 25, 2: 25, 3: 20}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label erubetie_ng_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Окунись в мою слизь своим телом...",
        "Я обернусь вокруг твоего тела...",
        "Я буду ласкать тебя своей слизью..."
    ])

    $ world.battle.show_skillname("Распространение Аркадии")
    play sound "audio/se/ero_makituki2.ogg"
    "Слизь Эрубети обволакивает тело Луки!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 51:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 46:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 41:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (560, 580), 2: (580, 600), 3: (600, 620)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 25, 2: 25, 3: 20}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label erubetie_ng_a3:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Я поиграю с твоей задней дырочкой...",
        "Моя бесформенная слизь довольно легко может проскользнуть внутрь...",
        "Дай мне посмотреть на эту жалкую эякуляцию от такого метода..."
    ])

    $ world.battle.show_skillname("Поглаживание Ксанаду")
    play sound "audio/se/ero_makituki2.ogg"
    "Слизь Эрубети проникает в анальное отверстие Луки, стимулируя его простату!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 51:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 46:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 41:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (540, 560), 2: (560, 580), 3: (580, 600)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 25, 2: 25, 3: 20}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label erubetie_ng_a4:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Скорчись от вихря наслаждения!",
        "Что-то, что может сделать только слизь...",
        "Я попытаю его в вихре наслаждения..."
    ])

    $ world.battle.show_skillname("Закручивание Шангри-Ла")
    play sound "audio/se/ero_slime1.ogg"
    "Слизь Эрубети стекает член Луки!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 51:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 46:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 41:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (180, 200), 2: (187, 207), 3: (194, 214)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 25, 2: 25, 3: 20}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/ero_slime1.ogg"
    "Слизь принимает форму вокруг члена Луки и сжимает его!{w}{nw}"

    python:
        if world.party.player.wind and world.party.player.wind_guard_calc({1: 25, 2: 25, 3: 20}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    play sound "audio/se/ero_slime1.ogg"
    "После этого, она начинает вращаться вокруг него, словно водоворот!"

    python:
        if world.party.player.wind and world.party.player.wind_guard_calc({1: 25, 2: 25, 3: 20}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label erubetie_ng_a5:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Растворись в волне наслаждения!",
        "Расплавься в гигантской волне...",
        "Растворись в моём теле..."
    ])

    $ world.battle.show_skillname("Растворяющий Шторм")
    play sound "audio/se/ero_makituki2.ogg"
    "Слизь Эрубети создаёт массивное цунами!"

    python:
        if not world.party.player.bind and world.party.player.aqua:
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (1850, 1950), 2: (1950, 2050), 3: (2000, 2100)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 75, 2: 75, 3: 70}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            narrator("Огромная массивная волна слизи обрушивается на тело Луки, захватывая его в вихрь слизи!")
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label erubetie_ng_a6:
    if world.party.player.surrendered:
        world.battle.enemy[0].name "Утони в наслаждении моего тела!"

    $ world.battle.show_skillname("Райская Тюрьма")
    play sound "audio/se/ero_slime3.ogg"
    "Слизь Эрубети обволакивает тело Луки и насильно поглужает его в неё!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 101:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 46:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 41:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (330, 360), 2: (350, 380), 3: (370, 400)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 85, 2: 85, 3: 80}):
            world.party.player.wind_guard()
            renpy.return_statement()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
            renpy.return_statement()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        if world.party.player.daystar:
            world.party.player.bind = 0
            world.party.player.skill22x()

        world.party.player.bind = 1
        world.party.player.status_print()

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

        if world.party.player.surrendered:
            renpy.return_statement()

    if world.battle.first[0] != 1:
        world.battle.enemy[0].name "Теперь пришло время выжать твоё семя...{w}\nДай мне насладиться твоей агонией от наслаждения..."
    else:
        world.battle.enemy[0].name "На этот раз тебе не вырваться.{w}\nДай мне насладиться твоей агонией от наслаждения..."

    $ world.battle.first[0] = 1

    if world.party.player.earth < 1:
        $ world.battle.enemy[0].power = 2
    elif world.party.player.earth > 0:
        $ world.battle.enemy[0].power = 0

    return

label erubetie_ng_a7:
    "Лука застрял в слизи Эрубети..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Окунись в мою слизь своим телом...",
        "Я обернусь вокруг твоего тела...",
        "Я буду ласкать тебя своей слизью..."
    ])

    $ world.battle.show_skillname("Райская Тюрьма")
    play sound "audio/se/ero_slime3.ogg"
    "Слизь Эрубети проникает в анальное отверстие Луки, стимулируя его простату!"

    python:
        world.battle.enemy[0].damage = {1: (530, 560), 2: (550, 580), 3: (570, 600)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label erubetie_ng_a9:
    if world.party.player.surrendered:
        world.battle.enemy[0].name "Хе-хе... Дай мне немного приобнять тебя..."

    $ world.battle.show_skillname("Божественная Буря")
    play sound "audio/se/ero_makituki2.ogg"
    "Эрубети подбирается к Луке со спины, и обнимает его!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 101:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 46:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 41:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (400, 440), 2: (420, 460), 3: (440, 480)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 85, 2: 85, 3: 80}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    "Задняя часть тела Луки утопает в теле Эрубети!"

    python:
        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

        if world.party.player.surrendered:
            renpy.return_statement()

    world.battle.enemy[0].name "Моё тело приятно, не правда ли?{w}\nПозволь мне сделать тебе ещё приятнее..."

    if world.party.player.earth < 1:
        $ world.battle.enemy[0].power = 1
    elif world.party.player.earth > 0:
        $ world.battle.enemy[0].power = 0

    return

label erubetie_ng_a10:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Хе-хе... я могу делать и так..."

    $ world.battle.show_skillname("Божественный поток")
    play sound "audio/se/ero_slime3.ogg"
    show erubetie st01 at xy(X=50)
    show erubetie st01 at xy(X=400) as clone

    "Эрубети разделяется ещё на одно тело, которое обнимает Луку спереди!{w}{nw}"

    $ world.battle.enemy[0].damage = {1: (400, 440), 2: (420, 460), 3: (440, 480)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.party.player.bind = 3

    extend "\nЛука зажат меж двух Эрубети!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")
    if world.party.player.surrendered:
        return

    e_a "Хе-хе... теперь стало в два раза лучше, ведь так?"
    e_b "Я собираюсь сделать тебе ещё приятнее..."

    return

label erubetie_ng_a11:
    if world.party.player.surrendered:
        world.battle.enemy[0].name "Я дам тебе насладиться сексом, который могут дать только девушки-слизи..."

    $ world.battle.show_skillname("Божественное Обуздание")

    "Эрубети разделяется вновь!{w}\nНовое тело хватается за нижнюю часть тела Луки!"

    $ world.battle.enemy[0].damage = {1: (400, 440), 2: (420, 460), 3: (440, 480)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    $ world.party.player.bind = 4

    "Тело Луки удерживается тремя Эрубети!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump("badend_h")
    if world.party.player.surrendered:
        return

    e_a "Хе-хе... Подготовка завершена."
    e_b "Пришло время веселья..."
    e_c "Скорчись в агонии от адского наслаждения, которое может принести только слизь..."

    return

label erubetie_ng_a12:
    if world.party.player.surrendered:
        world.battle.enemy[0].name "Теперь, сойди с ума от наслаждения..."

    $ world.battle.show_skillname("Божественная Судьба")
    "Все Эрубети, держащиеся за тело Луки, начинают одновременно двигаться!"

    jump badend_h

label erubetie_ng_hpoison:
    jump badend_h

label erubetie_ng_kousan:
    "Если я сдамся, она сразу же меня прикончит..."

    $ world.battle.main()

label erubetie_ng_onedari:
    "Она не тот противник, кого я могу попросить об атаке!"

    $ world.battle.main()

label undine_ng_start:
    python:
        world.troops[35] = Troop(world)
        world.troops[35].enemy[0] = Enemy(world)

        world.troops[35].enemy[0].name = Character("Ундина")
        world.troops[35].enemy[0].sprite =  ["undine st01", "undine st02", "undine st01", "undine st02"]
        world.troops[35].enemy[0].position = center
        world.troops[35].enemy[0].defense = 80
        world.troops[35].enemy[0].evasion = 90
        world.troops[35].enemy[0].max_life = world.troops[35].battle.difficulty(18000, 18500, 19500)
        world.troops[35].enemy[0].power = world.troops[35].battle.difficulty(0, 1, 2)

        world.party.player.earth_keigen = 20

        world.troops[35].battle.tag = "undine_ng"
        world.troops[35].battle.name = world.troops[35].enemy[0].name
        world.troops[35].battle.background = "bg 097"

        if persistent.music:
            world.troops[35].battle.music = 23
        else:
            world.troops[35].battle.music = 10

        world.troops[35].battle.exp = 1250000
        world.troops[35].battle.exit = "lb_0057"
        world.troops[35].battle.ng = True

        world.troops[35].battle.init(0)

    undine "Тогда хорошо.{w}\nЕсли ты собираешься атаковать всех подряд в полную силу...{w}\nТо я тоже не стану сдерживаться."
    "Ледяной голос Ундины пронзает меня."
    l "Да послушай же ты меня-я-я!{w}\nЗнаю, я выгляжу как плохой парень, но я не твой враг!"
    show undine st04
    undine "Ты вторгся в мой источник и атаковал здешних девушек-слизей.{w}\nТы даже чуть не убил Эрубети..."
    $ world.battle.face(1)
    undine "Ты заплатишь за это."
    l "......................"
    $ world.party.player.skill_set(2)
    $ world.battle.world.battle.nostar = 2
    $ world.battle.main()

label undine_ng001:
    $ world.battle.stage[2] = 1
    show undine st04
    world.battle.enemy[0].name "... Хмпф.{w}\nДолжна признать, я поражена..."
    $ world.battle.face(1)
    world.battle.enemy[0].name "... Но я не позволю тебе победить."
    call undine_ng_serene2
    l "Подожди-ка...{w}\nТы только что использовала Безмятежный Разум?!"
    world.battle.enemy[0].name "Ты серьёзно удивлён, что дух воды обладает такой способностью?{w}\nТеперь ты меня даже не каснёшься."
    l "........"
    "Похоже, что теперь попасть по ней будет сложнее.{w}\nНо если я воспользуюсь достаточно быстрой техникой..."
    return

label undine_ng002:
    $ world.battle.stage[5] = 1
    world.battle.enemy[0].name "Хм?{w}\nСаламандра с тобой?.."
    world.battle.enemy[0].name "Как...{w} ностальгично."
    $ world.battle.show_skillname("Стена Воды")
    show undine st11
    pause .5
    play sound "se/aqua.ogg"
    with flash
    pause .5
    play sound "se/aqua2.ogg"
    "Толстая стенка воды окружает Ундины, защищая её от пламени!"
    $ world.battle.enemy[0].defense = 50
    show undine st04
    world.battle.enemy[0].name "Попробуй-ка пробиться через это. Если сможешь..."
    $ world.battle.hide_skillname()
    return

label undine_ng_main:
    python:
        if world.battle.result == 44 and world.party.player.fire and world.battle.stage[4] < 1:
            renpy.jump(f"{world.battle.tag}_minilixir")

        world.battle.call_cmd()

        if world.party.player.fire and world.battle.world.battle.nostar < 2:
            world.battle.world.battle.nostar = 1
        if not world.party.player.fire and world.battle.world.battle.nostar < 2:
            world.battle.world.battle.nostar = 0

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label undine_ng_v:
    show undine st04
    world.battle.enemy[0].name ".............{w}\nПолагаю будет глупостью продолжать сражение."
    show undine st05
    world.battle.enemy[0].name "Хорошо.{w}\nЯ увидела достаточно."
    world.battle.enemy[0].name "Я признаю своё поражение.{w}\nТы победил."
    "Ундина сдаётся..."
    $ world.battle.victory(1)

label undine_ng_kousan:
    $ world.battle.common_surrender()

    "Лука прекращает сопротивляться."
    world.battle.enemy[0].name "Ох?{w}\nТы уже всё?{w}\nХорошо."

    call undine_ng_a3b

    $ world.battle.enemy[0].attack()

label undine_ng_a_aqua:
    $ world.battle.stage[0] = 2
    $ random = rand().randint(1,3)
    if world.party.player.fire and (world.battle.enemy[0].life < world.battle.enemy[0].max_life * 2/3) and world.battle.enemy[0].aqua < 1:
        if random == 1:
            call undine_ng_serene2
            $ world.battle.main()
        if random > 1:
            call undine_ng_serene2
            $ world.battle.main()

    call undine_ng_serene2
    $ world.battle.main()

label undine_ng_a:
    while True:
        if world.party.player.fire and world.battle.stage[5] < 1:
            call undine_ng002

        if world.battle.stage[4] > 0:
            $ world.battle.stage[4] -= 1

        if world.battle.enemy[0].life < (world.battle.enemy[0].max_life / 2) and world.battle.stage[2] < 1:
            call undine_ng001

        if world.battle.enemy[0].life < (world.battle.enemy[0].max_life * 45 / 100) and world.battle.nostar > 1 and world.battle.stage[4] > 0 and world.party.player.fire:
            call undine_ng_heal

        if world.battle.enemy[0].life < (world.battle.enemy[0].max_life * 37 / 100) and world.battle.nostar < 2 and world.battle.stage[4] > 0 and world.party.player.fire:
            call undine_ng_heal

        if world.battle.enemy[0].life < (world.battle.enemy[0].max_life / 2) and world.party.salamander_rep > 19 and world.party.player.fire:
            call undine_ng_minilixir

        if world.battle.enemy[0].life < (world.battle.enemy[0].max_life / 2) and world.battle.stage[1] < 1 and world.battle.enemy[0].aqua < 1:
            jump undine_ng_a_aqua

        if world.party.player.bind:
            call undine_ng_a3b

        if world.party.player.fire and world.battle.stage[2] < 1:
            jump undine_ng_sala

        if world.party.player.aqua and world.battle.stage[1] < 1:
            jump undine_ng_serene

        if world.battle.enemy[0].life < (world.battle.enemy[0].max_life / 2) and world.party.player.mp < 8:
            $ world.battle.progress = 0

        if world.battle.stage[3] != 1 and world.battle.enemy[0].life < (world.battle.enemy[0].max_life / 2) and world.party.player.mp > 10:
            $ world.battle.progress += 1
            $ world.battle.stage[3] = 1

        if world.battle.enemy[0].life < (world.battle.enemy[0].max_life / 2) and world.party.player.mp > 10 and world.battle.stage[4] < 1:
            call undine_ng_heal

        if world.battle.enemy[0].life < (world.battle.enemy[0].max_life / 3) and world.battle.stage[4] < 1:
            call undine_ng_heal

        $ random = rand().randint(1,7)

        if world.party.player.bind and world.battle.enemy[0].life > (world.battle.enemy[0].max_life / 2):
            call undine_ng_a4
            $ world.battle.main()

        if world.party.player.bind and world.battle.enemy[0].life < (world.battle.enemy[0].max_life / 2):
            call undine_ng_a3c
            $ world.battle.main()

        if random == 1:
            call undine_ng_a1
            $ world.battle.main()

        elif random == 2:
            call undine_ng_a2
            $ world.battle.main()

        elif random == 3 and world.battle.enemy[0].life > (world.battle.enemy[0].max_life * 2/3):
            call undine_ng_a3
            $ world.battle.main()

        elif random == 3 and world.battle.enemy[0].life < (world.battle.enemy[0].max_life * 2/3):
            call undine_ng_a3b
            $ world.battle.main()

        elif random == 4:
            call undine_ng_a5
            $ world.battle.main()

        elif random == 5 and world.battle.enemy[0].life < (world.battle.enemy[0].max_life * 2/3):
            call undine_ng_a6
            $ world.battle.main()

        elif random == 6:
            call undine_ng_a7
            $ world.battle.main()

        elif random == 7:
            call undine_ng_a8
            $ world.battle.main()

label undine_ng_nostar:
    show undine st07
    world.battle.enemy[0].name "Я знала, что ты сделаешь так!"
    $ world.battle.counter()
    $ world.battle.show_skillname("Слизневый Шторм")
    "Ундина создаёт массивное цунами из слизи над Денницей!{nw}{w}"
    play sound "audio/se/wind2.ogg"
    extend "\nПосле чего, это цунами поглощает Денницу, уничтожая её!"
    $ world.battle.hide_skillname()
    if world.battle.stage[3] < 1:
        call undine_ng_nostar2
    return

label undine_ng_nostar2:
    $ world.battle.stage[3] = 1
    l "Что?!..{w}\nМоя атака!"
    $ world.battle.face(1)
    if world.battle.stage[5] > 0:
        world.battle.enemy[0].name "Думаешь такой умный, вызывая Саламандру после такой атаки?{w}\nЯ не настолько глупа, малыш."
    l "Гах...{w}\nЧёрт возьми..."

    return
label undine_ng_sala:
    show undine st06
    world.battle.enemy[0].name "Саламандра...{w}{nw}"

    if world.party.salamander_rep > 4:
        extend "\nК тому же, их связь довольно сильна..."

    world.battle.enemy[0].name "Похоже это будет тяжелее, чем я думала..."
    $ world.battle.stage[2] = 1
    $ world.battle.face(1)
    jump undine_ng_a

label undine_ng_serene2:
    show undine st11
    world.battle.enemy[0].name "..........................."
    $ world.battle.show_skillname("Безмятежный Разум: Уровень 2")
    play sound "audio/se/aqua.ogg"
    with flash
    pause .5
    "Ундина очищает свой разум и соединяется с потоками мира!"
    $ world.battle.enemy[0].aqua = 2
    $ world.battle.enemy[0].aqua_turn = -1
    $ world.battle.hide_skillname()
    $ world.battle.face(1)
    return

label undine_ng_serene3:
    show undine st11
    world.battle.enemy[0].name "..........................."
    $ world.battle.show_skillname("Безмятежный Разум: Уровень 3")
    play sound "audio/se/aqua.ogg"
    with flash
    pause .5
    "Ундина очищает свой разум и соединяется с потоками мира!"
    $ world.battle.enemy[0].aqua = 3
    $ world.battle.enemy[0].aqua_turn = 12
    $ world.battle.hide_skillname()
    $ world.battle.face(1)
    return

label undine_ng_minilixir:
    show undine st06

    world.battle.enemy[0].name "Ах!{w}\nСаламандра тоже сильна...!"

    $ world.battle.show_skillname("Миниксир")
    play sound "audio/se/aqua.ogg"
    $ world.battle.enemy[0].life_regen(3000)
    $ world.battle.hide_skillname()
    $ world.battle.stage[4] = 1
    $ world.battle.face(1)
    return

label undine_ng_heal:
    $ world.battle.face(2)
    if world.battle.stage[0] == 0:
        world.battle.enemy[0].name "Я не позволю тебе так легко победить..."
    elif world.battle.stage[0] == 1:
        world.battle.enemy[0].name "Это бесполезно..."
    elif world.battle.stage[0] > 1:
        world.battle.enemy[0].name "Ты не достоин моей силы..."

    $ world.battle.show_skillname("Эликсир")
    play sound "audio/se/aqua.ogg"
    "Ундина со всей своей силой поглощает влагу из воздуха..."
    play sound "audio/se/power.ogg"
    "Ундина накапливает свои силы!"
    if world.party.player.daystar:
        $ world.party.player.skill22x()
        $ world.battle.main()

    $ world.battle.enemy[0].life_regen(world.battle.enemy[0].max_life / 3)
    $ world.battle.hide_skillname()
    $ world.battle.stage[4] = 2
    if world.battle.stage[0] == 0:
        call undine_ng_heal2
    elif world.battle.stage[0] == 1:
        call undine_ng_heal3
    $ world.battle.face(1)
    return

label undine_ng_heal2:
    l "Что за!.."
    $ world.battle.stage[0] = 1
    "Она просто исцелилась?!"
    world.battle.enemy[0].name "Глупое дитя...{w}\nТвоя вина, что ты дал мне шанс."
    l "Дерьмово..."
    "Ну, а теперь то что?{w}\nМне просто продолжать атаковать её?"
    return

label undine_ng_heal3:
    $ world.battle.stage[0] = 2
    l "Снова?!"
    "Но если она продолжит так просто лечиться...!"
    world.battle.enemy[0].name "Хмпф.{w}\nТы правда настолько беспомощен?"
    return

label undine_ng_serene:
    $ world.battle.stage[1] = 1
    world.battle.enemy[0].name "Безмятежный разум...{w}\nЭто... странно."
    "Ундина выглядит озадаченной."
    world.battle.enemy[0].name "... Ты пытаешься на меня произвести впечатление или что?"
    l "Хах, что?.."
    world.battle.enemy[0].name "Твоё умение обращаться с потоками - отстой.{w}\nЯ могу легко обойти это..."
    l "Что?!"
    "\"Отстой\"?!{w}\nЯ правда настолько плох?.."
    $ world.battle.stage[1] = 1
    jump undine_ng_a

label undine_ng_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Если ты кончишь от этого — провалишь испытание...",
        "Терпи это удовольствие.",
        "Приятно, правда?.. Но ты должен терпеть."
    ])

    $ world.battle.show_skillname("Аква-змея")
    play sound "audio/se/ero_makituki1.ogg"
    "Ундина хватает член Луки своей рукой!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 6:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 1:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 1:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (540, 570), 2: (650, 680), 3: (700, 740)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 5, 2: 3, 3: 1}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label undine_ng_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Как долго ты сможешь терпеть это?..",
        "Кончай... Если ты сдаёшься.",
        "Вытерпишь ли ты это?"
    ])

    $ world.battle.show_skillname("Аква-поглаживания")
    play sound "audio/se/ero_slime1.ogg"
    "Хвост Ундины хватает член Луки, поглаживая его!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 6:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 1:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 1:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (540, 570), 2: (650, 680), 3: (700, 740)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 5, 2: 5, 3: 5}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label undine_ng_a3:
    if world.party.player.surrendered:
        world.battle.enemy[0].name "Я обездвижу твоё тело..."

    $ world.battle.show_skillname("Водная Тюрьма")
    play sound "audio/se/ero_slime3.ogg"

    "Тело Ундины поглощает Луку!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 6:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 1:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 1:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (440, 480), 2: (520, 560), 3: (570, 640)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.party.player.bind = 1
        world.party.player.status_print()

        world.battle.hide_skillname()

    "Лука застрял в теле Ундины!"

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    if world.party.player.surrendered:
        return

    if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
        $ world.party.player.common_s1()

    if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
        $ world.party.player.common_s2()

    $ world.battle.face(2)
    if world.battle.first[0] == 0:
        world.battle.enemy[0].name "Если ты достаточно силён, то тебе не составит проблем вырваться, да?.."
    elif world.battle.first[0] == 1:
        world.battle.enemy[0].name "Выберишься ли ты в этот раз?.."

    $ world.battle.first[0] = 1

    if world.party.player.earth == 0:
        $ world.battle.enemy[0].power = 1

    elif world.party.player.earth > 0:
        $ world.battle.enemy[0].power = 0

    return

label undine_ng_a3b:
    if world.party.player.surrendered:
        world.battle.enemy[0].name "Твоя сила ошеломительна...{w}\nНо!"

    $ world.battle.show_skillname("Объятье Водного Духа")
    play sound "audio/se/ero_slime3.ogg"

    "Ундина захватывает Луку своим хвостом!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 11:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 6:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 1:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (140, 180), 2: (220, 260), 3: (370, 440)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 10, 2: 5, 3: 3}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.party.player.bind = 1
        world.party.player.status_print()

        world.battle.hide_skillname()

    "Лука схвачен хвостом Ундины!"

    if not world.party.player.life:
        $ renpy.jump("badend_h")

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    world.battle.enemy[0].name "Тебе не вырваться.{w}\nТеперь ты весь мой..."

    play sound "audio/se/ero_pyu1.ogg"
    show undine hb1

    "Ундина проталкивает Луку внутрь себя!"

    python:
        world.battle.enemy[0].damage = {1: (640, 680), 2: (720, 760), 3: (870, 940)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.first[0] = 1

        if not world.party.player.life:
            renpy.jump("badend_h")

        if not world.party.player.bind:
            if world.party.player.earth == 0:
                world.battle.enemy[0].power = 1

            elif world.party.player.earth > 0:
                world.battle.enemy[0].power = 0

        if world.party.player.surrendered:
            renpy.jump("undine_ng_a3c")

    return

label undine_ng_a4:
    "Лука застрял в теле Ундины..."
    $ random = rand().randint(1,5)

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Стань единым целым со мной...",
        "Пришла пора заканчивать...",
        "Я выжму твоё семя и растворю тебя...",
        "Если я простимулирую твой член, ты кончишь в меня?",
        "Просто расслабься..."
    ])

    $ world.battle.show_skillname("Аква-объятья")
    play sound "audio/se/ero_makituki3.ogg"

    $ world.battle.cry(narrator, [
        "Тело Ундины поглощает Луку!""Тело Ундины сжимает член Луки!",
        "Холодная слизь тела Ундины заставляет Луку расслабиться!",
        "Слизистое тело Ундины дрожит, касаясь члена Луки!",
        "Тело Ундины начинает вибрировать!"
    ], True)

    $ world.battle.skillcount(2, 3394)

    python:
        world.battle.enemy[0].damage = {1: (840, 880), 2: (920, 960), 3: (970, 1020)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if world.party.player.surrendered:
            renpy.return_statement()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()
    return

label undine_ng_a5:
    $ world.battle.skillcount(1, 3028)
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Ты кончишь от этого...",
        "Я очень хороша в этом...",
        "Ты не сможешь сопртивляться..."
    ])

    $ world.battle.show_skillname("Аква-минет")
    play sound "audio/se/ero_chupa4.ogg"
    "Ундина берёт член Луки в рот и начинает неистово сосать!"
    play sound "audio/se/ero_chupa5.ogg"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 6:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 1:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 1:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (680, 740), 2: (760, 820), 3: (830, 880)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 5, 2: 5, 3: 5}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label undine_ng_a6:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Повинуйся моей песни.",
        "Ты не сможешь сопртивляться...",
        "Я легко загипнотизирую тебя..."
    ])

    $ world.battle.show_skillname("Песнь Одинокой Русалки")
    play sound "audio/se/ero_makituki5.ogg"
    "Ундина поёт прекрасную песню!"
    play sound "audio/se/ero_chupa5.ogg"

    python:
        if world.party.player.wind and world.party.player.wind_guard_calc({1: 100, 2: 100, 3: 100}):
                world.party.player.wind_guard()
                renpy.return_statement()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 100, 2: 75, 3: 50}):
                world.party.player.wind_guard()
                renpy.return_statement()

        if world.party.player.daystar:
            world.party.player.skill22x()

        world.party.player.status = 1
        world.party.player.status_print()
        world.party.player.status_turn = 2


    "Лука загипнотизирован песней Ундины!"

    $ world.battle.hide_skillname()
    $ world.battle.face(2)

    world.battle.enemy[0].name "Ха-ха-ха...{w}\nТеперь ты в моей власти..."

    return

label undine_ng_a7:
    $ world.battle.skillcount(1, 3026)
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Как долго ты сможешь терпеть это?..",
        "Просто сдайся...",
        "Вытерпишь ли ты это?"
    ])

    $ world.battle.show_skillname("Липкое пайзури")
    play sound "audio/se/ero_slime3.ogg"
    pause .4
    play sound "audio/se/ero_koki1.ogg"
    "Ундина зажимает член Луки меж своей липкой груди!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.aqua and persistent.difficulty == 1 and random < 6:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 2 and random < 1:
                world.party.player.aqua_guard()
            elif world.party.player.aqua and persistent.difficulty == 3 and random < 1:
                world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (660, 720), 2: (740, 780), 3: (820, 880)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 5, 2: 5, 3: 5}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label undine_ng_a8:
    if rand().randint(1,100) < 50:
        jump undine_ng_a

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Теперь у меня есть силы для этого...",
        "Прощай, Лука...",
        "Это конец..."
    ])

    $ world.battle.show_skillname("Слизневый Шторм")
    play sound "audio/se/ero_slime3.ogg"

    "Ундина создаёт массивное цунами из слизи!"

    $ world.battle.skillcount(2, 3653)

    python:
        if not world.party.player.bind and world.party.player.aqua:
            world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (1650, 1750), 2: (1650, 1750), 3: (1800, 1900)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 75, 2: 75, 3: 70}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            narrator("Массивная стена из слизи врезается в тело Луки, унося его в водоворот слизи!")
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label kamakiri_start:
    python:
        world.troops[36] = Troop(world)
        world.troops[36].enemy[0] = Enemy(world)

        world.troops[36].enemy[0].name = Character("Девушка-богомол")
        world.troops[36].enemy[0].sprite =  ["kamakiri st01", "kamakiri st02", "kamakiri st03", "kamakiri ha1"]
        world.troops[36].enemy[0].position = center
        world.troops[36].enemy[0].defense = 100
        world.troops[36].enemy[0].evasion = 90
        world.troops[36].enemy[0].max_life = world.troops[36].battle.difficulty(2800, 3200, 3600)
        world.troops[36].enemy[0].power = world.troops[36].battle.difficulty(0, 1, 2)

        if persistent.difficulty < 3:
            world.party.player.earth_keigen = 40
        else:
            world.party.player.earth_keigen = 20

        world.troops[36].battle.tag = "kamakiri"
        world.troops[36].battle.name = world.troops[36].enemy[0].name
        world.troops[36].battle.background = "bg 061"
        world.troops[36].battle.music = 1
        world.troops[36].battle.exp = 15000
        world.troops[36].battle.exit = "lb_0059"

        world.troops[36].battle.init()

    world.battle.enemy[0].name "Человеческий мужчина... какая находка.{w}\nЯ схвачу тебя... и совокуплюсь..."
    l "..................."
    "Почему они не могут просто вежливо попросить?{w}\nЭто ведь регион Ноа. Чёрт возьми, я уверен, что тут найдётся и тот, кто скажет её \"да\"."

    $ world.battle.main()

label kamakiri_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()


label kamakiri_v:
    $ world.battle.face(3)
    world.battle.enemy[0].name "Хее....Хее-хее..."

    play sound "se/syometu.ogg"
    hide kamakiri with crash

    "Девушка-богомол превращается в обычного богомола!"

    $ world.battle.victory(1)

label kamakiri_a:
    if world.party.player.before_action == 45:
        jump kamikiri001

    if world.party.player.bind == 1:
        call kamakiri_a4
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,3)

        if random == 1:
            call kamakiri_a1
            $ world.battle.main()
        elif random == 2:
            call kamakiri_a2
            $ world.battle.main()
        elif random == 3 and world.battle.delay[0] > 3:
            call kamakiri_a3
            $ world.battle.main()


label kamakiri_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Heehee... Male's... Taste...",
        "Hee... Semen... Suck...",
        "Penis... Lick..."
    ])

    $ world.battle.show_skillname("Mantis Blowjob")
    play sound "audio/se/ero_buchu2.ogg"

    "The Mantis Girl violently sucks on Luka's penis!"

    $ world.battle.skillcount(2, 3395)

    python:
        world.battle.enemy[0].damage = {1: (95, 105), 2: (110, 120), 3: (125, 135)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label kamakiri_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Male semen... Spit it out...",
        "Heehee... Semen... Semen...",
        "Hee... Chest... Ejaculate..."
    ])

    $ world.battle.show_skillname("Mantis Tit Fuck")
    play sound "audio/se/ero_paizuri.ogg"

    "The Mantis Girl squeezes Luka's penis between her full breasts!"

    $ world.battle.skillcount(2, 3396)

    python:
        world.battle.enemy[0].damage = {1: (100, 110), 2: (115, 125), 3: (130, 140)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label kamakiri_a3:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "Heehee... Man... Rape..."

    $ world.battle.show_skillname("Embrace")

    "The Mantis Girl embraces Luka with her bladed arms!{w}{nw}"

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    extend "{w=.7}\nLuka's being bound by the Mantis Girl!"

    $ world.battle.hide_skillname()

    "The Mantis Girl hovers her genitals over Luka's penis!"

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    if world.battle.first[0] == 1:
        world.battle.enemy[0].name "Mate... This time we mate..."
    else:
        world.battle.enemy[0].name "Heehee... Mating..."

    $ world.battle.first[0] = 1

    if world.party.player.earth == 0:
        $ world.battle.enemy[0].power = 100
    elif world.party.player.earth > 0:
        $ world.battle.enemy[0].power = 0
    return


label kamakiri_a4:
    world.battle.enemy[0].name "Time to mate..."

    $ world.battle.show_skillname("Mantis Rape")
    play sound "audio/se/ero_makituki2.ogg"
    show kamakiri ha2

    "Luka's penis is forced into the Mantis Girl's egg-laying organ!"

    $ world.battle.skillcount(2, 3397)

    python:
        world.battle.enemy[0].damage = (130, 150)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

    "Luka is being raped by the Mantis Girl!"
    l "Ahhh!"
    "Luka writhes around in her tight embrace as she forces him all the way inside her sticky vagina!"

    jump kamakiri_a5


label kamakiri_a5:
    "Luka is being raped by the Mantis Girl..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Heehee... Mating... Mating...",
        "Hee...Hee... Does it feel good?",
        "Hurry, semen... Shoot it out...",
        "Hurry, come... Impregnate me...",
        "Hee... Fertilize my eggs..."
    ])

    $ world.battle.show_skillname("Mantis Rape")
    play sound "audio/se/ero_makituki2.ogg"

    $ world.battle.cry(narrator, [
        "The Mantis Girl pumps Luka!",
        "The Mantis Girl shakes her hips around!",
        "The ribbed feeling of her vagina stimulates Luka!",
        "The Mantis Girl tightens around Luka!",
        "The sticky liquid inside of her sticks to Luka's penis!"
    ], True)

    $ world.battle.skillcount(2, 3397)

    python:
        world.battle.enemy[0].damage = (130, 150)
        world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    jump kamakiri_a5


label kamakiri_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Ке-ке... спариваться... спариваться!"

    $ world.battle.enemy[0].attack()

label kamakiri_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Mantis Blowjob"
    $ list2 = "Mantis Tit Fuck"
    $ list3 = "Mantis Rape"

    if persistent.skills[3395][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3396][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3397][0] > 0:
        $ list3_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump kamakiri_onedari1
    elif world.battle.result == 2:
        jump kamakiri_onedari2
    elif world.battle.result == 3:
        jump kamakiri_onedari3
    elif world.battle.result == -1:
        jump kamakiri_main


label kamakiri_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "Hee... You want me to suck out your semen with my mouth?"

    while True:
        call kamakiri_a1


label kamakiri_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "Hee... You want me to squeeze out your semen with my breasts?"

    while True:
        call kamakiri_a2


label kamakiri_onedari3:
    call onedari_syori_k

    world.battle.enemy[0].name "Hee.... You want to mate?"

    if not world.party.player.bind:
        call kamakiri_a3

    while True:
        call kamakiri_a4


label kamakiri_h1:
    $ world.party.player.moans(1)
    with syasei1
    show kamakiri bk01 zorder 10 as bk
    $ world.ejaculation()

    "Unable to take the Mantis Girl's intense blowjob, Luka comes, filling her mouth with semen."

    $ world.battle.lose()
    $ world.battle.count([4, 1])
    $ bad1 = "The Mantis Girl's blowjob forced Luka to come."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Nnn... Semen in my mouth..."

    jump kamakiri_h


label kamakiri_h2:
    $ world.party.player.moans(1)
    with syasei1
    show kamakiri bk02 zorder 10 as bk
    $ world.ejaculation()

    "Squeezed between the Mantis Girl's plump breasts, Luka comes, covering her chest with semen."

    $ world.battle.lose()
    $ world.battle.count([7, 1])
    $ bad1 = "The Mantis Girl's tit fuck forced Luka to come."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Semen... All over my chest..."

    jump kamakiri_h


label kamakiri_h3:
    $ world.party.player.moans(3)
    with syasei1
    show kamakiri ha3
    $ world.ejaculation()

    "The instant he was forced into the Mantis Girl, Luka orgasms, filling her with semen."

    $ world.battle.lose()
    $ world.battle.count([1, 1])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka was forced to orgasm just from insertion."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Already ejaculated... Mating... Over?.."
    "The Mantis Girl laughs in delight.{w}\nShe smirks, with the face of a female who has dominated her mate."
    "But she doesn't take my penis out of her...{w}\nIt seems like she wants more..."

    jump kamakiri_h


label kamakiri_h4:
    $ world.party.player.moans(2)
    with syasei1
    show kamakiri ha3
    $ world.ejaculation()

    "Unable to endure the pleasure, Luka comes, filling up the Mantis Girl with his semen."

    $ world.battle.lose()
    $ world.battle.count([1, 1])
    $ bad1 = "Luka was forced to orgasm as the Mantis Girl raped him."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Semen came out... Now we've mated..."
    "The Mantis Girl laughs in delight.{w}\nShe smirks, with the face of a female who has dominated her mate."
    "But she doesn't take my penis out of her...{w}\nIt seems like she wants more..."

    jump kamakiri_h


label kamakiri_h:
    $ bad2 = "After that, Luka was nothing more than a breeding tool for the Mantis Girl."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.party.player.bind:
        play hseanwave "audio/se/hsean20_tentacle1.ogg"
        jump kamakiri_hb

    l "Ahh..."
    "Exhausted, I fall down only to be embraced in the Mantis Girl's arms."
    world.battle.enemy[0].name "Mating... Impregnate me..."

    hide bk
    show kamakiri ha1

    "The Mantis Girl pushes her abdomen toward me.{w} At the end of her abdomen, a tiny hole opens and closes as it approaches my penis."
    world.battle.enemy[0].name "I'm ready to mate...{w}\nNow you need to impregnate me..."
    l "Ah...ahh..."
    "That's her sexual organ...{w}\nThat horrifying hole is going to suck my penis inside and force me to mate with her..."
    "Stuck in her embrace, she's going to make me come inside her and get her pregnate..."
    l "N...No... Stop...!"
    "I writhe in her tight embrace, but it's no use.{w}\nI look on in horror as her vagina slowly gets closer to my penis."
    world.battle.enemy[0].name "Mating... Give my eggs plenty of your seed..."
    l "Noo... Ahh!"

    play hseanwave "audio/se/hsean20_tentacle1.ogg"
    show kamakiri ha2

    "With a slurping noise, my penis is sucked deep inside her body."
    "Her warm juices drip out of her onto me as the soft flesh inside tightens around my penis."


label kamakiri_hb:
    l "Ahh..."
    "I sigh as the pleasure rushes over me at once.{w}\nExpecting something horrible, the amazing stimulation catches me off guard."
    "The power leaves my body as my male instinct takes over, making me feel like I want to ejaculate inside her."
    "But I quickly regain my senses as I hold myself back."
    l "N...No... I can't come..."
    "I'm mating with a Mantis Girl right now...{w}\nShe forced me into her, and is raping me..."
    "This is breaking Ilias's commandment... I absolutely can't allow myself to ejaculate inside her..."
    world.battle.enemy[0].name "You aren't going to come?..{w} We'll be mating a lot, though..."
    "Her soft walls tighten around me as the flesh starts to wriggle and move inside her."
    "The sticky juices inside of her rub into me as her tight vagina squirms around my penis, making me shiver despite my attempts to endure the stimulation."
    l "Ahh..."
    "My body reacts in ecstasy despite my resistance."
    "Her sexual organs, designed to force men to come no matter what, crack my endurance as if it was made of nothing but brittle glass."
    l "Ahh... I shouldn't, but... I... I'm going to come..."
    "The tightness in my waist gradually relaxes as it's replaced with a dull ache."
    "My focus slowly switches from denying the pleasure to accepting the pleasure."
    "My spirit finally breaks, and my body takes control.{w}\nFinally able to perform what it's purpose is, my seed erupts from my penis and fills the Mantis Girl's vagina."
    l "Ahhhh!"

    with syasei1
    show kamakiri ha3
    $ world.ejaculation()

    "The Mantis Girl's vagina doesn't stop tightening and wriggling around me as I ejaculate, squeezing out every drop of my semen into her waiting womb."

    stop hseanwave fadeout 1

    l "Ahh... I came..."
    "My semen is inside of her horrifying organ,..{w}\nIt isn't the first time she made me orgasm, but the immorality still makes me shiver."
    world.battle.enemy[0].name "You gave me lots of semen...{w}\nNow my eggs have been fertilized..."
    "With a satisfied look on her face, the Mantis Girl happily starts to shake her hips.{w}{nw}"

    show kamakiri hb1
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nLike that, she forces me to the ground and lays on top of me."
    l "Wh...What are you doing?!"
    "It looks like she isn't letting me go...{w}\nJust what else does she want?.."
    world.battle.enemy[0].name "Egg-laying..."

    play hseanwave "audio/se/hsean22_tentacle3.ogg"
    show kamakiri hb2

    "Something starts to come out of her hole."
    "It's a small egg.{w}\nEntangling my body, she wraps me in those small eggs."
    l "Wh...What is this?!"
    "Some of the frothy eggs start to wriggle.{w}\nThe ones around my groin squirm, unexpectedly hitting me with pleasure."
    l "Ahh... Wh...What is this?.."
    "Some of the strange sticky eggs cling to my penis as they squirm and wriggle."
    world.battle.enemy[0].name "Nourishment for the eggs before they hatch...{w}\nMale semen... The eggs will absorb it..."
    l "N...No way... Ahh!"
    "My semen will be used to nourish these eggs?..{w}\nI shiver in horror at the thought."
    "But the frothy liquid that's around the eggs is also covering my groin.{w}\nThe bizarre feeling causes my entire lower body to ache in pleasure."
    l "Ahh... Th..This feeling..."
    "Even though I'm horrified, it feels good."
    "The slimy feeling of the wriggling eggs feels good...{w}\nTheir birthing must have evolved this way..."
    world.battle.enemy[0].name "More eggs... To give birth to..."

    show kamakiri hb3

    "More frothy eggs come out of her, covering my body all the way up to my shoulder in squirming eggs."
    l "Ah...Ahh..."
    "The stimulations cover more and more of my body, making it feel like I'm being massaged all over."
    "Letting out a sigh, I relax as the eggs force me to an orgasm."

    with syasei1
    $ world.ejaculation()

    "I ejaculate, spraying my semen all over the eggs.{w}\nContrary to a feeling of release, I feel overwhelmed by a feeling of disgust."
    "Wrapped in eggs and forced to come just to feed them...{w}\nIt feels like my worth as a man has been crushed..."
    world.battle.enemy[0].name "Give them more nourishment...{w}\nUntil they're nice and big..."
    l "Haaa..."

    show kamakiri hb4

    "Even more eggs come out of her, covering more of my body."
    l "Ah...ahh..."
    "The feeling of another orgasm slowly pushes away the revulsion as more of my body is covered by the wriggling eggs."
    "Trembling, I let out a moan as I'm brought to another orgasm."

    with syasei1
    $ world.ejaculation()

    "My penis twitches as I come, shooting semen out over the eggs.{w}\nAbsorbing the semen, they start to wriggle and vibrate even more, as if encouraged by the food being given to them."
    "As they squirm with even more intensity on my penis, I feel another orgasm already coming on."
    l "Ahhh..."

    with syasei1
    $ world.ejaculation()

    "Wrapped in the pleasant feelings of the eggs all over my body, I reach another climax."
    "Every time I ejaculate, I feel weaker.{w}\nSlowly, my vision starts to fade as I feel myself about to fall unconscious."
    l "Ah...ahh..."
    "While still leaking semen, I faint."
    world.battle.enemy[0].name "Come a lot...{w}\nFeed our daughters..."
    "Even as I'm unconscious, I'm still forced to orgasm.{w}\nJust like I was awake, there's no escape..."
    "For a full week, I lay there covered in her eggs..."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "After the eggs hatched, I wasn't freed.{w}\nAs soon as they had hatched, the Mantis Girl mated with me again, and then wrapped me up in more eggs."
    "Like this, I'm used as nothing more than a breeding tool for the Mantis Girl...{w}\nMy adventure ends here."
    ".............."

    jump badend

label scylla_start:
    python:
        world.troops[37] = Troop(world)
        world.troops[37].enemy[0] = Enemy(world)

        world.troops[37].enemy[0].name = Character("Сцилла")
        world.troops[37].enemy[0].sprite =  ["scylla st01", "scylla st02", "scylla st03", "scylla st02"]
        world.troops[37].enemy[0].position = center
        world.troops[37].enemy[0].defense = 100
        world.troops[37].enemy[0].evasion = 95
        world.troops[37].enemy[0].max_life = world.troops[37].battle.difficulty(3000, 3200, 3400)
        world.troops[37].enemy[0].power = world.troops[37].battle.difficulty(0, 1, 2)

        if persistent.difficulty < 3:
            world.party.player.earth_keigen = 30
        else:
            world.party.player.earth_keigen = 15

        world.troops[37].battle.tag = "scylla"
        world.troops[37].battle.name = world.troops[37].enemy[0].name
        world.troops[37].battle.background = "bg 099"
        world.troops[37].battle.music = 1
        world.troops[37].battle.exp = 16000
        world.troops[37].battle.exit = "lb_0060"

        world.troops[37].battle.init()

    $ world.battle.face(2)

    world.battle.enemy[0].name "Хе-хе... Маленьким милым мальчикам очень опасно ходить здесь в такое время."
    world.battle.enemy[0].name "Стра-а-ашные девочки придут за тобой и поймают тебя своими щупальцами, знаешь ли?"
    l "Чёрт... Сцилла..."
    "Она из расы знаменитых и сильных монстров.{w}\nЕё щупальца способны и на сильные атаки, и на мощные сковывания..."
    world.battle.enemy[0].name "Хе-хе... Я поиграюсь с тобой своими щупальцами."
    world.battle.enemy[0].name "Хочешь, чтобы я сильно сжала тебя своими щупальцами?{w}\nИли хочешь, чтобы я погладила ими твой член?"

    $ world.battle.face(1)
    $ world.battle.main()

label scylla_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label scylla_v:
    $ world.battle.face(3)
    world.battle.enemy[0].name "Э-этого.... не может быть!.."

    play sound "se/syometu.ogg"
    hide scylla with crash

    "Сцилла превращается в осминога!"

    $ world.battle.victory(1)

label scylla_a:
    if world.party.player.bind == 1:
        while True:
            $ random = rand().randint(1,11)

            if random == 1:
                call scylla_a6
                call scylla_a1
                $ world.battle.main()
            elif random == 2:
                call scylla_a6
                call scylla_a2
                $ world.battle.main()
            elif random == 4:
                call scylla_a6
                call scylla_a4
                $ world.battle.main()
            elif random == 6:
                call scylla_a1
                call scylla_a3
                $ world.battle.main()
            elif random == 8:
                call scylla_a2
                call scylla_a3
                $ world.battle.main()
            elif random == 9:
                call scylla_a2
                call scylla_a4
                $ world.battle.main()
            elif random == 11 and world.battle.delay[1] > 6:
                call scylla_a7
                $ world.battle.main()

    while True:
        $ random = rand().randint(1,8)

        if random == 1:
            call scylla_a1
            $ world.battle.main()
        elif random == 2:
            call scylla_a2
            $ world.battle.main()
        elif random == 3:
            call scylla_a3
            $ world.battle.main()
        elif random == 4:
            call scylla_a4
            $ world.battle.main()
        elif random > 4 and world.battle.delay[0] > 7:
            call scylla_a5
            $ world.battle.main()


label scylla_a1:
    if world.party.player.bind != 0:
        "Luka is bound by Scylla's tentacles..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Have a taste of my tentacles...",
        "Let my tentacles make you feel as good as you want...",
        "I'll caress your whole body with my tentacles..."
    ])

    $ world.battle.show_skillname("Tentacle Body Torture")
    play sound "audio/se/ero_makituki1.ogg"

    "The Scylla's tentacles crawl all over Luka's body!"

    $ world.battle.skillcount(2, 3398)

    python:
        world.battle.enemy[0].damage = {1: (60, 70), 2: (70, 80), 3: (80, 90)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label scylla_a2:
    if world.party.player.bind != 0:
        "Luka is bound by Scylla's tentacles..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Enjoying the feel of my tentacles on your nipples... What a shameful boy.",
        "Feeling good from your nipples, even though you're a boy?",
        "I'll caress your nipples with my tentacles..."
    ])

    $ world.battle.show_skillname("Tentacle Nipple Torture")
    play sound "audio/se/ero_makituki1.ogg"

    "Scylla's tentacles massage Luka's nipples!"

    $ world.battle.skillcount(2, 3399)

    python:
        world.battle.enemy[0].damage = {1: (65, 75), 2: (75, 85), 3: (85, 95)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label scylla_a3:
    if world.party.player.bind != 0:
        "Luka is bound by Scylla's tentacles..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Don't feel shy...",
        "My tentacles will squeeze out your sticky semen...",
        "Hehe... I'll make your penis feel good with my tentacles..."
    ])

    $ world.battle.show_skillname("Tentacle Groin Torture")
    play sound "audio/se/ero_makituki2.ogg"

    "Scylla's tentacles coil around Luka's penis, slowly rubbing him with it!"

    $ world.battle.skillcount(2, 3400)

    python:
        world.battle.enemy[0].damage = {1: (70, 80), 2: (80, 90), 3: (90, 100)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label scylla_a4:
    if world.party.player.bind != 0:
        "Luka is bound by Scylla's tentacles..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "My tentacles will violate you...",
        "Feeling it even from your ass... What a pitiful boy.",
        "Will you moan as I violate your ass?"
    ])

    $ world.battle.show_skillname("Tentacle Anal Torture")
    play sound "audio/se/ero_makituki2.ogg"

    "One of Scylla's tentacles slips into Luka's ass and starts to vibrate!"

    $ world.battle.skillcount(2, 3401)

    python:
        world.battle.enemy[0].damage = {1: (85, 95), 2: (95, 105), 3: (105, 115)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label scylla_a5:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "My tentacles will coil around you..."

    $ world.battle.show_skillname("Tentacle Bind")
    play sound "audio/se/ero_makituki3.ogg"

    "Scylla's tentacles coil around Luka's body!"

    $ world.battle.enemy[0].damage = {1: (75, 85), 2: (85, 95), 3: (95, 105)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Luka has been bound by her powerful tentacles!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h5")

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    if world.battle.first[0] == 1:
        world.battle.enemy[0].name "Hehe, you won't get away this time...{w}\nNow I'm going to use my tentacles to play with you..."
    else:
        world.battle.enemy[0].name "Hehe... I caught you.{w}\nNow I'm going to use my tentacles to play with you..."

    $ world.battle.first[0] = 1

    if world.party.player.earth == 0 and persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 3
    elif world.party.player.earth == 0 and persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 4
    elif world.party.player.earth == 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 5
    elif world.party.player.earth > 0 and persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 1
    elif world.party.player.earth > 0 and persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 2
    elif world.party.player.earth > 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return


label scylla_a6:
    "Luka is bound by Scylla's tentacles..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll tighten a little...",
        "Here, I'll tease you a little...",
        "Come on... Let me hear that pathetic voice of yours..."
    ])

    $ world.battle.show_skillname("Tentacle Tighten")
    play sound "audio/se/ero_simetuke1.ogg"

    "Scylla tightens her tentacles around Luka's body!"

    $ world.battle.skillcount(2, 3402)

    python:
        world.battle.enemy[0].damage = {1: (75, 85), 2: (85, 95), 3: (95, 105)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h5")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label scylla_a7:
    "Luka is bound by Scylla's tentacles..."

    if not world.party.player.surrendered:
        world.battle.enemy[0].name "It's about time to give you a taste of heaven...{w}\nMy tentacles will caress you everywhere little boys can feel something..."

    $ world.battle.show_skillname("Elysion - De - Scylla")
    play sound "audio/se/ero_makituki3.ogg"

    "Scylla's tentacles move toward Luka's weak points!"

    $ world.battle.skillcount(2, 3403)
    play sound "audio/se/ero_makituki1.ogg"

    "A tentacle tickles his nipples!"

    $ world.battle.enemy[0].damage = (65, 75)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    play sound "audio/se/ero_makituki1.ogg"

    "A tentacle gently coils around his balls!"

    $ world.battle.enemy[0].damage = (65, 75)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    play sound "audio/se/ero_makituki2.ogg"

    "A tentacle slips into his ass and starts to vibrate!"

    $ world.battle.enemy[0].damage = (65, 75)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    play sound "audio/se/ero_makituki2.ogg"

    "A tentacle wraps around his penis and rubs it!"

    $ world.battle.enemy[0].damage = (65, 75)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h6")

    jump scylla_a8


label scylla_a8:
    $ world.battle.skillcount(2, 3403)

    "Luka is bound by Scylla's tentacles..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll play with all of your weak points...",
        "Where does it feel best? Here? ...Or here?",
        "Pitiful little boy, let me hear that voice of yours..."
    ])

    $ world.battle.show_skillname("Elysion - De - Scylla")
    play sound "audio/se/ero_makituki3.ogg"

    "Scylla's tentacles move toward Luka's weak points!"

    play sound "audio/se/ero_makituki1.ogg"

    $ world.battle.cry(narrator, [
        "A tentacle tickles his nipples!",
        "A sucker sticks to his nipples and starts to suck!",
        "The tip of a tentacle coils around his nipple!"
    ], True)

    $ world.battle.enemy[0].damage = (65, 75)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    play sound "audio/se/ero_makituki1.ogg"

    $ world.battle.cry(narrator, [
        "A tentacle gently coils around Luka's balls!",
        "A tentacle tightens around Luka's balls!",
        "The tip of a tentacle plays with Luka's balls!"
    ])

    $ world.battle.enemy[0].damage = (65, 75)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    play sound "audio/se/ero_makituki2.ogg"

    $ world.battle.cry(narrator, [
        "A tentacle slips into Luka's ass, and starts to spin inside!",
        "A tentacle slips into Luka's ass and stimulates his prostate!",
        "A tentacle goes in and out of Luka's ass!"
    ])

    $ world.battle.enemy[0].damage = (65, 75)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    play sound "audio/se/ero_makituki2.ogg"

    $ world.battle.cry(narrator, [
        "A tentacle coils around Luka's penis!",
        "A tentacle tickles the tip of Luka's penis!",
        "A tentacle slowly tightens around Luka's penis!",
        "A sucker on her tentacle sticks to the tip of Luka's penis!"
    ])

    $ world.battle.enemy[0].damage = (65, 75)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h7")

    jump scylla_a8


label scylla_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Хочешь, чтобы я поиграла с тобой своими щупальцами?..{w}\nТогда я буду трогать тебя ими, пока ты не кончишь..."

    $ world.battle.enemy[0].attack()

label scylla_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Tentacle Body Torture"
    $ list2 = "Tentacle Nipple Torture"
    $ list3 = "Tentacle Groin Torture"
    $ list4 = "Tentacle Anal Torture"
    $ list5 = "Tentacle Tighten"
    $ list6 = "Elysion - De - Scylla"

    if persistent.skills[3398][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3399][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3400][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3401][0] > 0:
        $ list4_unlock = 1

    if persistent.skills[3402][0] > 0:
        $ list5_unlock = 1

    if persistent.skills[3403][0] > 0:
        $ list6_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump scylla_onedari1
    elif world.battle.result == 2:
        jump scylla_onedari2
    elif world.battle.result == 3:
        jump scylla_onedari3
    elif world.battle.result == 4:
        jump scylla_onedari4
    elif world.battle.result == 5:
        jump scylla_onedari5
    elif world.battle.result == 6:
        jump scylla_onedari6
    elif world.battle.result == -1:
        jump scylla_main


label scylla_onedari1:
    call onedari_syori_k

    world.battle.enemy[0].name "You want to feel my tentacles all over your body?"

    if not world.party.player.bind:
        call scylla_a5

    while True:
        call scylla_a1


label scylla_onedari2:
    call onedari_syori_k

    world.battle.enemy[0].name "You want me to play with your nipples?"

    if not world.party.player.bind:
        call scylla_a5

    while True:
        call scylla_a2


label scylla_onedari3:
    call onedari_syori_k

    world.battle.enemy[0].name "You want my tentacles to play with your penis?"

    if not world.party.player.bind:
        call scylla_a5

    while True:
        call scylla_a3


label scylla_onedari4:
    call onedari_syori_k

    world.battle.enemy[0].name "You want to be raped by my tentacles?"

    if not world.party.player.bind:
        call scylla_a5

    while True:
        call scylla_a4


label scylla_onedari5:
    call onedari_syori_k

    world.battle.enemy[0].name "You want my tentacles to tighten around your body?"

    if not world.party.player.bind:
        call scylla_a5

    while True:
        call scylla_a6


label scylla_onedari6:
    call onedari_syori_k

    world.battle.enemy[0].name "You want to be dominated by my tentacles?"

    if not world.party.player.bind:
        call scylla_a5

    while True:
        call scylla_a7


label scylla_h1:
    $ world.party.player.moans(1)
    with syasei1
    show scylla bk03 at xy(X=140) zorder 10 as bk
    $ world.ejaculation()

    "As Scylla's tentacles run all over his body, Luka comes, covering her tentacles with his semen."

    $ world.battle.lose()
    $ world.battle.count([10, 3])
    $ bad1 = "Luka was forced to orgasm by Scylla's body torture."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hehe... Did you enjoy my tentacles?"

    jump scylla_h


label scylla_h2:
    $ world.party.player.moans(1)
    with syasei1
    show scylla bk03 at xy(X=140) zorder 10 as bk
    $ world.ejaculation()

    "As Scylla's tentacles rub his nipples, Luka comes, covering her tentacles with his semen."

    $ world.battle.lose()
    $ world.battle.count([23, 3])
    $ bad1 = "Luka was forced to orgasm by Scylla's nipple torture."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Coming just from having your nipples played with?..{w}\nIt's like you're a girl... Hehe."

    jump scylla_h


label scylla_h3:
    $ world.party.player.moans(1)
    with syasei1
    show scylla bk03 at xy(X=140) zorder 10 as bk
    $ world.ejaculation()

    "As Scylla's tentacles play with his penis, Luka comes, covering her tentacles with his semen."

    $ world.battle.lose()
    $ world.battle.count([10, 3])
    $ bad1 = "Luka was forced to orgasm by Scylla's groin torture."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hehe... I milked it out with my tentacles.{w}\nComing like that is the best, isn't it?"

    jump scylla_h


label scylla_h4:
    $ world.party.player.moans(1)
    with syasei1
    show scylla bk03 at xy(X=140) zorder 10 as bk
    $ world.ejaculation()

    "As her tentacles violate his ass, Luka comes, covering Scylla's tentacles with semen."

    $ world.battle.lose()
    $ world.battle.count([9, 3])
    $ bad1 = "Luka was forced to orgasm by Scylla's anal torture."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hahaha! You came from that?!{w}\nEven though you're a boy... How pathetic!"

    jump scylla_h


label scylla_h5:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Hehe... What's wrong?"

    with syasei1
    show scylla bk03 at xy(X=140) zorder 10 as bk
    $ world.ejaculation()

    "Unable to endure the feeling of her tentacles on his body, Luka comes, covering her with semen."

    $ world.battle.lose()
    $ world.battle.count([10, 3])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka came just from the feeling of Scylla's tentacles on him."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Arara... Coming just from that?{w}\nIf you could have held it for a moment, I could have dome something even better..."

    jump scylla_h


label scylla_h6:
    $ world.party.player.moans(1)
    with syasei1
    show scylla bk03 at xy(X=140) zorder 10 as bk
    $ world.ejaculation()

    "As the tentacles tighten around his body, Luka comes, covering her with semen."

    $ world.battle.lose()
    $ world.battle.count([16, 3])
    $ bad1 = "Luka came as Scylla's tentacles tightened around him."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Arara, coming as I tighten around you...{w}\nYou're quite a pervert, aren't you?"

    jump scylla_h


label scylla_h7:
    $ world.party.player.moans(2)
    with syasei1
    show scylla bk03 at xy(X=140) zorder 10 as bk
    $ world.ejaculation()

    "As the tentacles play with every weak point on Luka's body, he comes, covering Scylla with semen."

    $ world.battle.lose()
    $ world.battle.count([10, 3])
    $ bad1 = "Luka came as Scylla's tentacles overwhelmed his body."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hehe, you came a lot, didn't you?{w}\nIt was amazing, wasn't it? Hehe."

    jump scylla_h


label scylla_h:
    $ bad2 = "Caught in her tentacles, Luka became Scylla's semen slave."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    l "Ahh..."

    hide bk
    show scylla h0

    "Scylla pins me down with her tentacles, and gets on top of me with a dirty smile on her face."
    "Sitting on top of me, Scylla shows off a hole underneath her body."
    world.battle.enemy[0].name "Aren't you interested in how it feels inside of me?"
    "To show me inside, she spreads her opening with two tentacles.{w}\nBright pink flesh inside of her squirms and wriggles around, just waiting for something to play with..."
    world.battle.enemy[0].name "How does it feel inside me?..{w}\nWhy don't you check with your penis?"
    l "St...Stop this..."
    "Scylla laughs and slowly lowers her body down on me, forcing me inside her.{w}{nw}"

    play hseanwave "audio/se/hsean09_innerworks_b1.ogg"
    show scylla h1
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nShe forces me all the way into her body!"
    l "Ahhh!!"
    "Her slimy flesh wriggles and tightens around my penis as soon as I'm forced inside her, making me yell out in surprise pleasure."
    "Just like her squishy lower body, her vagina is just as squishy and flexible."
    world.battle.enemy[0].name "Inside my hole...{w}\nIt's a squishy heaven that will squeeze out your semen.{image=note}"
    l "Ahh... Ahhh!"
    "Deep inside of her, something new starts to wriggle and squirm around my penis.{w}\nIt's as if there are tiny tentacles lining her soft walls."
    "They crawl all over my penis as they start to coil and entangle it inside of her."
    l "Ahhhg... Something inside of you..."
    world.battle.enemy[0].name "Hehe... I have more tentacles, even inside my pussy...{w}\nTentacles designed to play with your penis.{image=note}"
    l "Ah... Aahhh!"
    "The tentacles inside of her coil around the tip of my penis, down the shaft and start to rub the back muscle of my penis."
    "Another tentacle coils around my penis and starts to tighten."
    l "Ahh... That feels good..."
    world.battle.enemy[0].name "It's fine to shoot out your little boy juice...{w}\nThis is the place where you're supposed to shoot your semen, so you're supposed to let out as much as you can..."
    l "B...But...{w} Ahhh!"
    "She clamps tightly around me, cutting off my protest.{w}\nMy head goes white as I'm attacked by the pleasure, forgetting why I was resisting this."
    "I push up into her, wanting to feel more.{w}\nAs soon as I do, I feel a tingling spread throughout my entire body."
    world.battle.enemy[0].name "Hehe, come without holding back.{w}\nIf you come inside me, I can make it feel even better.{image=note}"
    l "Ahh..."
    "I finally surrender to the pleasure, entrusting my body over to her.{w}\nAs if rewarding my submission, a powerful orgasm wracks my body."

    with syasei1
    show scylla h2
    $ world.ejaculation()

    l "Haa..."

    play hseanwave "audio/se/hsean10_innerworks_b2.ogg"

    "As soon as my ejaculation starts, the tentacles movements change.{w}\nThey stop wriggling, and start to press and tighten around me."
    l "Ahh... What... Is this?.."
    world.battle.enemy[0].name "When you come, my tentacles will crawl and coil around you...{w}\nThey'll make it feel reeeeeally good as they help you ejaculate."
    "As I ejaculate, the tentacles tightly squeeze and release me.{w}\nThe rhythmic pressure causes each spurt of semen to explode out of me when she releases my penis."
    l "Ahh..."
    "Her tentacles squeeze me until the very last drop is out.{w}\nExhausted, I lay back on the ground after the long ejaculation, extended by her strange technique."

    show scylla h3

    world.battle.enemy[0].name "Do you like my special semen squeezing hole?{w}\nBut I'm not satisfied yet..."
    l "Ah... Aiie!"
    "The tips of two tentacles start to gently rub against the back muscle and tip of my urethral opening at the same time."
    world.battle.enemy[0].name "Hehe... The pee hole and the back muscle...{w}\nI know every weak point on a boy..."
    l "Ahh..."
    "As the two tentacles carefully work on those two weak points, the rest continue to massage me."
    "Scylla's hole, designed to squeeze out a man's semen as he's overpowered by pleasure...{w}\nGasping in ecstasy, I indulge in the stimulations she's giving me."
    l "Ahh..."
    "Another squishy tentacle starts to coil around the tip of my penis."
    "It starts to vibrate and rhythmically squeeze me.{w}\nDominated by the pleasure as she sits on top of me, I feel another orgasm quickly rising through my body."
    l "Ahh... I'm going to come again..."
    world.battle.enemy[0].name "Hehe... Even though you just came, already going to come again?{w}\nThat's so fast... What a pitiful little boy."
    "Scylla squeezes me even tighter as she laughs in my face.{w}\nAll at once, I'm forced to a climax."
    l "Ahhhh!!"

    with syasei1
    show scylla h4
    $ world.ejaculation()

    "Moaning in pleasure, I explode inside her again."
    "But just like before, her tentacles tighten around me, controlling my ejaculation to make me slowly spurt inside of her as she lets me."
    l "Haa... Ahh..."
    "The pleasure from the orgasm lasts much longer than usual thanks to her strange technique.{w}\nEach spurt of semen comes with a powerful sense of release."

    show scylla h5

    world.battle.enemy[0].name "Hehe, I'll squeeze out a lot more...{w}\nMake sure you give me a lot, too."
    l "Haa... Ahhhh!"
    "The teasing of her tentacles on my weak spots continues right away, giving me no break."
    "Completely helpless underneath her, I feel another orgasm coming on after only a short while from the last."
    world.battle.enemy[0].name "Hehehe... It feels like you're about to come again.{w}\nI'll squeeze out everything... Hehehe."
    l "Ahh..."
    "I can taste that amazing pleasure if I come again...{w}\nThinking that, I relax, thrusting up into her waiting body."

    with syasei1
    show scylla h6
    $ world.ejaculation()

    l "Haa..."
    "Her tentacles coil around me again, beginning the ejaculation technique I was waiting for.{w}\nI start to drool as the ecstasy fills my body."
    "After the last drop of semen is milked out, I let out a long sigh and relax my body."
    world.battle.enemy[0].name "Hehe... It looks like you really like my special pussy."
    world.battle.enemy[0].name "I really like you, too.{w}\nI'm going to make you into my own personal use semen slave."
    l "Ahh..."
    world.battle.enemy[0].name "It doesn't sound bad at all, does it?{w}\nHahaha, here...!"
    l "Ah!{w} Ahhhh!"
    "She starts again, working on the next orgasm.{w}\nAbsorbed in the pleasure, I stop thinking about what she just said."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "After that, I was captured by her tentacles.{w}\nSqueezed dry every day by Scylla, I'll live out the rest of my life as her personal semen slave."
    "Able to indulge in pleasure every day, I'm not unhappy.{w}\nGiving semen to Scylla every day, I'm happiest when I'm inside her."
    "..............."

    jump badend

label yougan_start:
    python:
        world.troops[38] = Troop(world)
        world.troops[38].enemy[0] = Enemy(world)

        world.troops[38].enemy[0].name = Character("Девушка-лава")
        world.troops[38].enemy[0].sprite =  ["yougan st01", "yougan st02", "yougan st03", "yougan h1"]
        world.troops[38].enemy[0].position = center
        world.troops[38].enemy[0].defense = 100
        world.troops[38].enemy[0].evasion = 95
        world.troops[38].enemy[0].max_life = world.troops[38].battle.difficulty(4800, 5300, 7200)
        world.troops[38].enemy[0].power = world.troops[38].battle.difficulty(0, 1, 2)

        if persistent.difficulty < 3:
            world.party.player.earth_keigen = 40
        else:
            world.party.player.earth_keigen = 20

        world.troops[38].battle.tag = "yougan"
        world.troops[38].battle.name = world.troops[38].enemy[0].name
        world.troops[38].battle.background = "bg 115"
        world.troops[38].battle.music = 1
        world.troops[38].battle.exp = 25000
        world.troops[38].battle.exit = "lb_0062"

        world.troops[38].battle.init()

    world.battle.enemy[0].name "Не может быть... человек?{w}\nПрошли десятилетия, с тех пор как сюда в последний раз приходил человек..."
    l "Я пришёл сюда за Саламандрой.{w}\nЯ не желаю драки с тобой..."
    world.battle.enemy[0].name "Прошло уже столько времени, с тех пор как я пировала...{w}\nДай моей магме впитать всю твою сперму..."
    l "............."
    "Они просто не слышат меня или что?{w}\nНе важно..."

    $ world.battle.main()

label yougan_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label yougan_v:
    $ world.battle.face(3)
    world.battle.enemy[0].name "Я... Как меня смогли запечатать?.."

    play sound "se/syometu.ogg"
    hide yougan with crash

    "Девушка-лава превратилась в лужицу лавы!"

    $ world.battle.victory(1)

label yougan_a:
    if world.party.player.bind == 1:
        call yougan_a4
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,3)

        if random == 1:
            call yougan_a1
            $ world.battle.main()
        elif random == 2:
            call yougan_a2
            $ world.battle.main()
        elif random == 3 and world.battle.delay[0] > 6:
            call yougan_a3
            $ world.battle.main()


label yougan_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "My hand is nice and hot...",
        "Ejaculate on my hot palm...",
        "It feels good like this, doesn't it?"
    ])

    $ world.battle.show_skillname("Magma Shaft")
    play sound "audio/se/ero_koki1.ogg"

    "The Lava Girl strokes Luka's penis with her hot hand!"

    $ world.battle.skillcount(2, 3502)

    python:
        world.battle.enemy[0].damage = {1: (110, 120), 2: (130, 140), 3: (150, 160)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label yougan_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Pour your cool semen in my hot magma...",
        "Ejaculate in my magma...",
        "It feels nice and hot, doesn't it?"
    ])

    $ world.battle.show_skillname("Magma Draw")
    play sound "audio/se/ero_slime1.ogg"

    "The Lava Girl's hot magma flows over Luka's body, stimulating him!"

    $ world.battle.skillcount(2, 3503)

    python:
        world.battle.enemy[0].damage = {1: (115, 125), 2: (135, 145), 3: (155, 165)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label yougan_a3:
    world.battle.enemy[0].name "Pour your semen in me..."

    $ world.battle.show_skillname("Magma Hold")
    play sound "audio/se/ero_slime3.ogg"

    "The Lava Girl's hot magma flows over Luka's body!"

    $ world.battle.enemy[0].damage = {1: (120, 140), 2: (140, 160), 3: (160, 180)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Luka is stuck inside the Lava Girl!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    if world.battle.first[0] == 1:
        world.battle.enemy[0].name "This time, don't run...{w}\nI'll make your penis even hotter..."
    else:
        world.battle.enemy[0].name "It feels nice and hot, doesn't it?{w}\nI'll make your penis even hotter..."

    $ world.battle.first[0] = 1

    if world.party.player.earth == 0 and persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 3
    elif world.party.player.earth == 0 and persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 4
    elif world.party.player.earth == 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 5
    elif world.party.player.earth > 0 and persistent.difficulty < 2:
        $ world.battle.enemy[0].power = 1
    elif world.party.player.earth > 0 and persistent.difficulty == 2:
        $ world.battle.enemy[0].power = 2
    elif world.party.player.earth > 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return


label yougan_a4:
    "Luka's body is bound by the Lava Girl..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "My magma will swirl around your penis...",
        "Drown in my body...",
        "I'll heat up your penis...",
        "Let my magma stick to your body...",
        "Let me heat you up to a boil..."
    ])

    $ world.battle.show_skillname("Magma Bath")
    play sound "audio/se/ero_makituki2.ogg"

    $ world.battle.cry(narrator, [
        "Her magma swirls around Luka's penis!",
        "The magma flows all over Luka's body, drowning him in pleasure!",
        "Her magma bubbles around Luka's penis!",
        "Magma crawls around Luka's body, as if a hot tongue was licking him all over!",
        "The sticky magma jiggles as it flows around Luka's body!"
    ], True)

    $ world.battle.skillcount(2, 3504)

    python:
        world.battle.enemy[0].damage = {1: (120, 140), 2: (140, 160), 3: (160, 180)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label yougan_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "You want to come inside my magma?{w}\nThen let me heat you up..."

    $ world.battle.enemy[0].attack()

label yougan_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Magma Shaft"
    $ list2 = "Magma Draw"
    $ list3 = "Magma Bath"

    if persistent.skills[3502][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3503][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3504][0] > 0:
        $ list3_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump yougan_onedari1
    elif world.battle.result == 2:
        jump yougan_onedari2
    elif world.battle.result == 3:
        jump yougan_onedari3
    elif world.battle.result == -1:
        jump yougan_main


label yougan_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want to be stroked by my hot hand?"

    while True:
        call yougan_a1


label yougan_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want to come inside my magma?"

    while True:
        call yougan_a2


label yougan_onedari3:
    call onedari_syori_k

    world.battle.enemy[0].name "You want to drown in my body?"

    if not world.party.player.bind:
        call yougan_a3

    while True:
        call yougan_a4


label yougan_h1:
    $ world.party.player.moans(1)
    with syasei1
    show yougan bk03 zorder 10 as bk
    $ world.ejaculation()

    "As her hot hand strokes Luka, he comes, covering her magma formed hand with semen."

    $ world.battle.lose()
    $ world.battle.count([5, 3])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka was forced to an orgasm as the Lava Girl's hot hand stroked him."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Your cold semen is all over my hand..."

    jump yougan_h


label yougan_h2:
    $ world.party.player.moans(1)
    with syasei1
    show yougan bk03 zorder 10 as bk
    $ world.ejaculation()

    "As her hot magma flows over his body, Luka comes, mixing his semen in with her magma."

    $ world.battle.lose()
    $ world.battle.count([11, 3])
    $ bad1 = "Luka was forced to an orgasm as the Lava Girl's magma flowed over his body."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Your cold semen is mixing with my magma..."

    jump yougan_h


label yougan_h3:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "You're already done?.."

    with syasei1
    show yougan h2
    $ world.ejaculation()

    "As her hot magma wraps around his body, Luka accidentally comes, mixing his semen in with her magma."

    $ world.battle.lose()
    $ world.battle.count([11, 3])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka was forced to come just from the Lava Girl's magma flowing over him."
    $ world.battle.face(2)

    world.battle.enemy[0].name "You couldn't take my heat..."

    jump yougan_h


label yougan_h4:
    $ world.party.player.moans(2)
    with syasei1
    show yougan h2
    $ world.ejaculation()

    "Stuck in her hot magma as it runs all over his body, Luka comes, mixing his semen in with her magma."

    $ world.battle.lose()
    $ world.battle.count([11, 3])
    $ bad1 = "Luka was forced to an orgasm as he was bound in the Lava Girl's magma."
    $ world.battle.face(2)

    world.battle.enemy[0].name "So much cold semen inside me..."

    jump yougan_h


label yougan_h:
    $ bad2 = "Stuck inside her body, Luka's semen was endlessly squeezed."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    l "Ahh..."

    play hseanwave "audio/se/hsean18_slime1.ogg"
    hide bk
    show yougan h3

    if world.party.player.bind:
        jump yougan_hb

    "Exhausted after my orgasm, I fall to my knees.{w}\nQuickly taking her chance, the Lava Girl's magma washes over me and sucks me into her body."
    l "Aieee!!"
    world.battle.enemy[0].name "There's no need to be afraid...{w}\nI lowered my body temperature so as not to burn you."
    world.battle.enemy[0].name "Men are so rare here, there's no way I would let one go to waste.{w}\nI can use my magma like this to squeeze out their semen..."


label yougan_hb:
    "Her hot sticky magma coiled around me flows around my body, slightly solidifying around my penis."
    "The initial burst of fear from being burned fades as the relaxing warmth from her body fills me."
    l "Ahh... What is this?.."
    "The heat quickly exhausts me, making me melt in the warm stimulations.{w}\nAfter no time at all, I feel myself giving in to the relaxing pleasure."
    l "Ah... That feels good..."

    with syasei1
    show yougan h4
    $ world.ejaculation()

    l "Eh?..{w} I... I came?.."
    "The strange sense of release fills my lower body as I come.{w}\nI gasp in surprise at the sudden orgasm, as there was no preceding rise to it."
    "The odd ejaculation felt more like incontinence than anything.{w}\nBut with this relaxing warmth all around me, the afterglow feels many times better than anything I've felt before."
    l "Ahhh... Amazing..."
    "Her warm magma body covers my lower body.{w}\nJust as she said, the temperature feels like a nice relaxing bath, without burning me."
    "As the warmth fills my body, it feels like I could melt away in bliss inside of her."
    l "Ahh... It's so warm..."
    world.battle.enemy[0].name "Do you want to indulge in my warmth even more?{w}\nThen let me bring you further inside..."

    show yougan h5

    "Her magma bubbles up, covering me all the way to my neck.{w}\nMore of my body is immersed in the relaxing warmth of her body."
    "With my penis well inside of her body, the feelings of the last orgasm I had comes flooding back to me."
    l "So warm... So good..."
    "Her sticky magma, almost like jelly, flows around my penis.{w}\nIt was a bliss I've never experienced before in my life."
    "Completely relaxed by her warm body, I immerse myself in the pleasure."
    l "Ahhhh..."

    with syasei1
    $ world.ejaculation()

    "Feeling like I could melt into nothing, semen leaks out of me into her.{w}\nJust like before, it's almost like I'm peeing instead of ejaculating."
    "Sighing in pleasure, I relax inside her body even deeper as the overwhelming feeling of release fills me."
    l "Ahh... Incredible... This feels so good..."
    world.battle.enemy[0].name "Being inside my body is the best, isn't it?{w}\nLet me take you further in..."
    world.battle.enemy[0].name "I'll make sure you stay alive, so come as much as you want..."

    play hseanwave "audio/se/hsean19_slime2.ogg"
    show yougan h6

    "Her magma bubbles up one more time, covering the rest of my body.{w}\nWith even my head immersed in her body, I'm drowning in the Lava Girl's warmth."
    l "Ahhh!"
    "The relaxing bliss gets even deeper, consuming more and more of me.{w}\nI feel so safe and secure inside her warm body, almost as if I'm back in my mother's womb."
    l "Ahhh..."

    with syasei1
    $ world.ejaculation()

    "Sighing in pleasure, more semen leaks out into the Lava Girl's body."
    l "Haaa... So good..."

    with syasei1
    $ world.ejaculation()

    "I leak out inside of her over and over, drowning in ecstasy."
    world.battle.enemy[0].name "I'll squeeze out your semen for the rest of your life like this...{w}\nI'll keep you safe inside my womb so that you can give me your semen forever."
    l "Ahhh..."

    with syasei1
    $ world.ejaculation()

    "Not bothering to endure it at all, I happily let myself come inside of her womb.{w}\nStuck in a warm blissful trance, I give myself over completely to her."
    world.battle.enemy[0].name "Good boy...{w}\nYou'll be giving me your semen for ever and ever..."
    world.battle.enemy[0].name "In return, I'll make you feel good for ever and ever..."
    l "Haaa..."

    with syasei1
    $ world.ejaculation()
    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "I lose myself in the Lava Girl's womb.{w}\nI abandon my quest and think of nothing but the pleasure."
    "Like that, the Lava Girl will squeeze my semen for the rest of my life.{w}\nA slave to the pleasure, my days go on as nothing more than a monster's source of food."
    "..........."

    jump badend

label basilisk_start:
    python:
        world.troops[39] = Troop(world)
        world.troops[39].enemy[0] = Enemy(world)

        world.troops[39].enemy[0].name = Character("Василиск")
        world.troops[39].enemy[0].sprite =  ["basilisk st01", "basilisk st02", "basilisk st03", "basilisk st02"]
        world.troops[39].enemy[0].position = center
        world.troops[39].enemy[0].defense = 100
        world.troops[39].enemy[0].evasion = 90
        world.troops[39].enemy[0].max_life = world.troops[39].battle.difficulty(5200, 5700, 7800)
        world.troops[39].enemy[0].power = world.troops[39].battle.difficulty(0, 1, 2)

        if persistent.difficulty < 3:
            world.party.player.earth_keigen = 20
        else:
            world.party.player.earth_keigen = 10

        world.troops[39].battle.tag = "basilisk"
        world.troops[39].battle.name = world.troops[39].enemy[0].name
        world.troops[39].battle.background = "bg 115"
        world.troops[39].battle.music = 1
        world.troops[39].battle.exp = 25000
        world.troops[39].battle.exit = "lb_0063"

        world.troops[39].battle.init()

    world.battle.enemy[0].name "О, человек?{w} Какая находка.{w}\nЯ повеселюсь, играя с тобой..."
    "Василиск... могучий монстр, представляющий собой смесь птицы и рептилии."
    "Их сила невероятна... но куда большую опасность представляет их способность обращать в камень."

    $ world.battle.face(2)

    world.battle.enemy[0].name "Я превращу тебя в камень и сделаю из тебя отличную игрущку."
    world.battle.enemy[0].name "Превращу всё, кроме твоего члена, в камень, и изнасилую тебя.{w}\nИли... может, мне и его обратить в камень, дабы он никогда не обмяк?"
    world.battle.enemy[0].name "Хе-хе... Так чего же ты хочешь?"
    l "Ни то, ни это!"

    $ world.battle.face(1)
    $ world.battle.main()


label basilisk_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()


label basilisk_v:
    $ world.battle.face(3)
    world.battle.enemy[0].name "Как я могла проиграть такому ребёнку?!.."

    play sound "se/syometu.ogg"
    hide basilisk with crash

    "Василиск превращается в цыплёнка!"

    $ world.battle.victory(1)

label basilisk_a:
    while True:
        $ random = rand().randint(1,3)

        if random == 1:
            call basilisk_a1
            $ world.battle.main()
        elif random == 2:
            call basilisk_a2
            $ world.battle.main()
        elif random == 3 and world.battle.stage[0] == 0 and world.battle.delay[0] > 2:
            call basilisk_a3
            $ world.battle.main()
        elif random == 3 and world.battle.stage[0] == 1 and world.battle.delay[0] > 2:
            call basilisk_a4
            $ world.battle.main()
        elif random == 3 and world.battle.stage[0] == 2 and world.battle.delay[0] > 2:
            call basilisk_a5
            $ world.battle.main()
        elif random == 3 and world.battle.stage[0] == 3 and world.battle.delay[0] > 2:
            call basilisk_a6
            $ world.battle.main()


label basilisk_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "My hands feel good ya know...",
        "Shall I squeeze out that semen of yours?",
        "I'll work that dick of yours..."
    ])

    $ world.battle.show_skillname("Bird Handjob")
    play sound "audio/se/ero_koki1.ogg"

    "The Basilisk grabs Luka's penis with her strange feeling hand, and strokes him!"

    $ world.battle.skillcount(2, 3505)

    python:
        world.battle.enemy[0].damage = {1: (80, 90), 2: (95, 105), 3: (110, 120)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label basilisk_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll squeeze out that semen of yours with my tits!",
        "My tits are nice and soft ya know?",
        "These tits of mine can make a boy like you come easily."
    ])

    $ world.battle.show_skillname("Ample Tit Fuck")
    play sound "audio/se/ero_koki1.ogg"

    "The Basilisk slowly pumps Luka's penis between her huge breasts!"

    $ world.battle.skillcount(2, 3506)

    python:
        world.battle.enemy[0].damage = {1: (85, 95), 2: (100, 110), 3: (115, 125)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label basilisk_a3:
    if world.battle.first[0] != 1:
        world.battle.enemy[0].name "Let me show ya something nice.{w}\nI can turn something to stone by licking it."
        l "Eh?.."
    else:
        world.battle.enemy[0].name "Don't avoid it this time!{w}\nI'll lick it and petrify ya..."

    $ world.battle.first[0] = 1
    $ world.battle.show_skillname("Petrifying Lick")
    play sound "audio/se/ero_pyu2.ogg"

    "The Basilisk runs her tongue all over Luka's penis!"

    $ world.battle.skillcount(2, 3507)

    $ world.battle.enemy[0].damage = {1: (90, 100), 2: (105, 115), 3: (120, 130)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.stage[0] = 1
    play sound "audio/se/stone.ogg"
    $ world.party.player.status = 8
    $ world.party.player.status_print()

    "Part of Luka's penis turns to stone!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    l "Ah...Ahh!"
    "The places where she licked on my penis turn to stone.{w}\nOne side from the base to about halfway has turned to stone..."
    world.battle.enemy[0].name "I'll turn your penis to stone..."

    return


label basilisk_a4:
    world.battle.enemy[0].name "I'll lick your penis clean!"

    $ world.battle.show_skillname("Petrifying Lick")
    play sound "audio/se/ero_pyu2.ogg"

    "The Basilisk's tongue runs under the tip of Luka's penis!"

    $ world.battle.skillcount(2, 3507)

    $ world.battle.enemy[0].damage = {1: (90, 100), 2: (105, 115), 3: (120, 130)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.stage[0] = 2
    play sound "audio/se/stone.ogg"

    "Most of the area under the tip of Luka's penis turns to stone!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    l "N...No way!"
    "This time, the skin around the tip of my penis has turned mostly to stone.{w}\nI shudder at the heavy feeling of the weight between my legs."
    world.battle.enemy[0].name "Well? How does it feel having your penis turned to stone?"

    return


label basilisk_a5:
    world.battle.enemy[0].name "This time I'm aiming for the tip!"

    $ world.battle.show_skillname("Petrifying Lick")
    play sound "audio/se/ero_pyu2.ogg"

    "The Basilisk's tongue runs over the tip of Luka's penis!"

    $ world.battle.skillcount(2, 3507)

    $ world.battle.enemy[0].damage = {1: (90, 100), 2: (105, 115), 3: (120, 130)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.stage[0] = 3
    play sound "audio/se/stone.ogg"

    "The tip of Luka's penis turns to stone!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    l "Ahh!"
    "Most of my penis has turned to stone...{w}\nA few spots of sensation remain, but it's almost completely solid..."
    world.battle.enemy[0].name "Next time, I'm going to suck on that partially stone dick of yours.{w}\nAfter that, it'll be solid stone..."

    return


label basilisk_a6:
    world.battle.enemy[0].name "Alright, time to suck...{w}\nYou'll be rock hard after my mouth is done with ya."

    $ world.battle.show_skillname("Petrifying Lick")
    play sound "audio/se/ero_buchu3.ogg"

    "The Basilisk sucks Luka's penis into her mouth, and violently sucks on him!"

    $ world.battle.skillcount(2, 3507)

    $ world.battle.enemy[0].damage = {1: (90, 100), 2: (105, 115), 3: (120, 130)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.stage[0] = 2
    play sound "audio/se/stone.ogg"

    "Luka's penis turns completely to stone!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h3")

    l "Ahh..."
    "My penis is completely stone at last.{w}\nIt should be sensitive, but now I can barely feel it..."

    if world.party.player.surrendered and world.battle.result == 3:
        return

    world.battle.enemy[0].name "Alright, the fun finally starts!{w}\nTime to rape that stone dick of yours!"

    $ world.battle.show_skillname("Stone Rape")
    play sound "audio/se/ero_pyu5.ogg"
    show basilisk ct01 at topleft zorder 15 as ct

    "The Basilisk forces Luka to the ground, and mounts him!{w}\nHis stone penis sinks into her vagina!"

    $ world.battle.skillcount(2, 3508)
    $ world.battle.enemy[0].damage = (120, 140)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)
    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Luka is being raped by the Basilisk!"

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h4")

    l "Ahhh...{w}\nWh...What is this?!"
    "Even though my penis is completely stone, there's a slight sensation left."
    "It's dulled, but I can feel her tight hole squeezing around my rock hard penis."
    world.battle.enemy[0].name "It's a different type of feeling being raped like this, isn't it?{w}\nI'll make sure ya get a good taste of it!"

    jump basilisk_a7


label basilisk_a7:
    "Luka is being raped by the Basilisk..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Haha, how's it feel?",
        "How does that petrified dick of yours feel?",
        "Feels strange, doesn't it?",
        "Ahh, I love this feeling!",
        "I love it... This stone dick of yours!"
    ])

    $ world.battle.show_skillname("Stone Rape")
    play sound "audio/se/ero_chupa4.ogg"

    $ world.battle.cry(narrator, [
        "The Basilisk's vagina squirms around Luka's penis!",
        "The Basilisk's vagina tightens around Luka's stone penis!",
        "The Basilisk's vagina tightens around Luka's petrified penis!",
        "The Basilisk bounces on top of Luka, forcing his stone penis in and out of her!",
        "The Basilisk grinds against Luka, forcing his stone penis to rub against her!"
    ], True)

    $ world.battle.skillcount(2, 3508)

    $ world.battle.enemy[0].damage = (120, 140)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h5")

    jump basilisk_a7


label basilisk_a8:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll suck on that stone dick of yours even more.",
        "Do you want me to suck on that hard dick of yours more?",
        "I'll suck on you as hard as I can."
    ])

    $ world.battle.show_skillname("Petrifying Lick")
    play sound "audio/se/ero_buchu3.ogg"

    "The Basilisk sucks on Luka's stone penis!"

    $ world.battle.skillcount(2, 3507)

    $ world.battle.enemy[0].damage = {1: (90, 100), 2: (105, 115), 3: (120, 130)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

    $ world.battle.hide_skillname()

    if not world.party.player.life:
        $ renpy.jump(f"{world.battle.tag}_h6")

    return


label basilisk_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Уже сдаёшься?{w}\nТогда немного оторвусь на тебе."

    $ world.battle.enemy[0].attack()

label basilisk_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Bird Handjob"
    $ list2 = "Ample Tit Fuck"
    $ list3 = "Petrifying Sucking"
    $ list4 = "Stone Rape"

    if persistent.skills[3505][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3506][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3507][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3508][0] > 0:
        $ list4_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump basilisk_onedari1
    elif world.battle.result == 2:
        jump basilisk_onedari2
    elif world.battle.result == 3:
        jump basilisk_onedari3
    elif world.battle.result == 4:
        jump basilisk_onedari4
    elif world.battle.result == -1:
        jump basilisk_main


label basilisk_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want me to stroke that dick of yours?"

    while True:
        call basilisk_a1


label basilisk_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want me to squeeze that dick of yours between my breasts?"

    while True:
        call basilisk_a2


label basilisk_onedari3:
    call onedari_syori

    world.battle.enemy[0].name "You want me to lick ya?{w}\nHehehe... You sure about that?"

    if world.battle.stage[0] == 0:
        call basilisk_a3

    if world.battle.stage[0] == 1:
        call basilisk_a4

    if world.battle.stage[0] == 2:
        call basilisk_a5

    call basilisk_a6

    while True:
        call basilisk_a8


label basilisk_onedari4:
    call onedari_syori

    world.battle.enemy[0].name "You want me to rape ya?{w}\nThen I need to make it good for me, first..."

    if world.battle.stage[0] == 0:
        call basilisk_a3

    if world.battle.stage[0] == 1:
        call basilisk_a4

    if world.battle.stage[0] == 2:
        call basilisk_a5

    while True:
        call basilisk_a6


label basilisk_h1:
    $ world.party.player.moans(1)
    with syasei1
    show basilisk bk03 zorder 10 as bk
    $ world.ejaculation()

    "As the Basilisk's uniquely shaped hand strokes Luka, he comes, covering her soft palm in semen."

    $ world.battle.lose()
    $ world.battle.count([5, 5])
    $ bad1 = "Luka was forced to an orgasm by the Basilisk's handjob."
    $ world.battle.face(2)

    world.battle.enemy[0].name "My hand is all sticky now..."

    jump basilisk_h


label basilisk_h2:
    $ world.party.player.moans(1)
    with syasei1
    show basilisk bk02 zorder 10 as bk
    $ world.ejaculation()

    "Squeezed between the Basilisk's giant breasts, Luka comes, covering her with semen."

    $ world.battle.lose()
    $ world.battle.count([7, 5])
    $ bad1 = "Luka was forced to an orgasm as the Basilisk squeezed him with her breasts."
    $ world.battle.face(2)

    world.battle.enemy[0].name "My tits are all sticky now..."

    jump basilisk_h


label basilisk_h3:
    $ world.party.player.moans(1)
    with syasei1
    show basilisk bk01 zorder 10 as bk
    $ world.ejaculation()

    "As the Basilisk licks Luka's penis, he comes, shooting semen out of his partially petrified penis."

    $ world.battle.lose()
    $ world.battle.count([4, 5])
    $ bad1 = "Luka was forced to an orgasm from the Basilisk licking him."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Ya came before I could make it stone?{w}\nHow pathetic..."

    jump basilisk_h


label basilisk_h4:
    $ world.party.player.moans(3)

    world.battle.enemy[0].name "Oi, oi... That's too fast."

    "The moment Luka's forced into the Basilisk's vagina, he reaches a climax.{w}\nBut no sense of release comes with it..."

    $ world.battle.lose()
    $ world.battle.count([1, 5])
    $ persistent.count_bouhatu += 1
    $ bad1 = "Luka was forced to a climax as soon as he was inserted into the Basilisk's orgasm."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hahaha, you can't come with a stone dick.{w}\nHere... I'll make it even more unbearable for ya."

    play sound "audio/se/flash.ogg"
    with flash

    "The Basilisk's eyes shine with a bright light!"
    "Suddenly, full feeling returns to the tip of my penis!"

    jump basilisk_h


label basilisk_h5:
    $ world.party.player.moans(2)

    "As the Basilisk rapes Luka, he reaches a climax.{w}\nBut no sense of release comes with it..."

    $ world.battle.lose()
    $ world.battle.count([1, 5])
    $ bad1 = "Luka was forced to a climax as the Basilisk raped him."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hahaha, you can't come with a stone dick.{w}\nHere... I'll make it even more unbearable for ya."

    play sound "audio/se/flash.ogg"
    with flash

    "The Basilisk's eyes shine with a bright light!"
    "Suddenly, full feeling returns to the tip of my penis!"

    jump basilisk_h


label basilisk_h6:
    $ world.party.player.moans(1)

    "As the Basilisk sucks on his stone penis, Luka reaches a climax.{w}\nBut no sense of release comes with it..."

    $ world.battle.lose()
    $ world.battle.count([4, 5])
    $ bad1 = "Luka was brought to a climax as the Basilisk sucked on his stone penis."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Hahaha... You can't come with a stone dick!{w}\nI love this!{w} Raping men like this is the best!"
    world.battle.enemy[0].name "Here... I'll make it even more unbearable for ya."

    play sound "audio/se/flash.ogg"
    with flash

    "The Basilisk's eyes shine with a bright light!"
    "Suddenly, full feeling returns to the tip of my penis!"

    jump basilisk_h


label basilisk_h:
    $ bad2 = "Petrifying Luka's penis, she used him as a simple sex toy for the rest of his life."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.party.player.bind:
        play hseanwave "audio/se/hsean01_innerworks_a1.ogg"
        jump basilisk_hc

    if world.party.player.status == 6:
        jump basilisk_hb

    l "Ahh..."
    "Exhausted from my orgasm, I drop to my knees.{w}\nStanding in front of me, the Basilisk has a cold smile on her face."
    world.battle.enemy[0].name "Heh, the fun starts now."

    play sound "audio/se/flash.ogg"
    with flash

    "The Basilisk's eyes shine with a bright light!"

    play sound "audio/se/stone.ogg"

    "After seeing that flash of light, my body starts to turn to stone!"
    l "Ahhhh!!"
    "I gradually lose all feeling from my hands, feet, thighs... Everything.{w}\nAfter only a few moments, my body is just like cold stone."
    world.battle.enemy[0].name "Hehe... Now you're nothing but stone.{w}\nEven your penis, of course...{w} But only the shaft. I left the tip."
    world.battle.enemy[0].name "Keep the shaft rock hard and let them feel it through the tip...{w}\nJust the thought of raping a man like this is making me wet!"


label basilisk_hb:
    play hseanwave "audio/se/hsean01_innerworks_a1.ogg"
    hide bk
    show basilisk ct01 at topleft zorder 15 as ct


    "The Basilisk drops down on me, forcing my half stone penis into her wet vagina."


label basilisk_hc:
    l "Ahhh!"
    "I yell out from the feeling of her vagina around my penis."
    "Only able to feel it from my tip, I feel my sensitive tip pushing into her warm flesh."
    world.battle.enemy[0].name "Heh... My pussy is both part reptile and part bird."
    world.battle.enemy[0].name "Due to that, I got muscles that let me control everything inside my pussy, letting me play with a penis inside me..."
    world.battle.enemy[0].name "I've also got lots of nice little bumps to rub against that tip of yours."
    world.battle.enemy[0].name "Haha... And your stone dick can enjoy it without ever getting soft!"
    l "Ahhh!!"
    "The Basilisk tightens her vagina around me, and at the same time uses her muscles to rub it up and down."
    "The tiny little bumps inside of her rub against my sensitive tip, forcing amazing pleasure to wrack through my body."
    "At the same time, the tightness around me is too much to take.{w}\nThis famous monster's combination of bird and reptile vagina is too good..."
    l "Ahh..."
    world.battle.enemy[0].name "Haha, I feel amazing, don't I?{w}\nCome on, lets see ya come!"
    "The Basilisk starts to grind against me, violently raping me."
    "After grinding her hips against me for a few moments, she starts to bounce on top of me instead, alternating the stimulation."
    "With pleasure exploding through my body with every thrust from her, I can't even try to endure it."
    l "Ahh..."
    "Giving myself over to the sensations, I listen to the wet sounds of her raping me.{w}\nAfter what seems like only a few moments, I feel a tingling in my waist as an orgasm is about to burst out."
    l "Ahh... I'm going to come..."
    world.battle.enemy[0].name "Haha, already?{w}\nAlright then, time to finish ya off!"
    "The Basilisk drops down as hard as she can, forcing me all the way inside.{w}\nOnce I'm deep inside her, she clamps her muscles around the tip of my penis, rubbing me with her tiny bumps."
    l "Ahhhh!"
    "I easily climax from her finishing blow."

    $ world.ejaculation()

    l "Ahh... W...What?.."
    "I should have ejaculated...{w}\nBut that sense of release after the orgasm never came."
    "I felt the powerful climax, but not the accompanying release...{w}\nFeeling like something is incomplete, I start to shudder."
    world.battle.enemy[0].name "Hahahaha!{w} You can't ejaculate with a stone dick.{w}\nBut ya can feel everything through that flesh and blood tip of yours."
    world.battle.enemy[0].name "Haha, orgasm as much as ya want!{w}\nNothing will ever come out, and you'll never get that full relief!"
    "The Basilisk laughs as she pumps me like a piston, raping me even harder than before.{w}\nEven though I didn't ejaculate, my tip is extra sensitive as if I did."
    "Forced in and out of her wet vagina with every pump, my body shivers in intense pleasure."
    world.battle.enemy[0].name "Haha, I'm going to rape you over and over!{w}\nYour heart and soul will be shattered as I pump ya!"

    play hseanwave "audio/se/hsean07_innerworks_a7.ogg"

    "The Basilisk, almost berserk in her own lust, violently pumps me however she wants."
    "Almost like an animal on pure instinct, she bounces and grinds against me as hard as she can."
    l "Ahhh!{w} Ahhh!!"
    world.battle.enemy[0].name "God, I love that voice!{w}\nLet me hear it more! Hahaha!"
    l "Hyaaa!{w} Ahh!"
    "Drunk on her own pleasure, she just sadistically taunts me as she rides on top of me."
    "Her tight vagina squeezes around my tip every time I'm all the way inside of her, quickly loosening as she bounces back up."
    "Every time I penetrate back inside of her, the tiny bumps rub against my tip, bringing unbearable pleasure with it."
    l "Ahhh...!!"
    "Violently raped, I'm quickly brought to another height."

    $ world.ejaculation()

    l "Ahh..."
    "The feeling of the climax blows over me, but the sense of release from the ejaculation still doesn't come.{w}\nA dull ache pounds away at the base of my penis, as if the semen inside of me is desperate to be released."
    "My body twists at the intense discomfort, the irritation at the incomplete orgasm gnawing away at me."
    world.battle.enemy[0].name "Hahaha, do you want to come in my pussy?{w}\nWant to shoot that semen of yours into my body?"
    "The Basilisk stops pumping me like a piston for a moment.{w}\nNow just seductively grinding her waist against me, she starts to taunt me with words."
    "My tip slowly rubs against her walls, only adding to the frustration from my inability to get relief."
    l "Ahh... N...No... I don't..."
    world.battle.enemy[0].name "Hahaha, I'll make ya climax over and over with my pussy then.{w}\nBut... You'll never get to ejaculate! Hahaha!"
    "The Basilisk starts her piston-like thrusting again as she looks down at me.{w}\nTrembling, I helplessly moan under her."
    "Way faster than before, I feel another climax coming up, my body screaming for an actual ejaculation."
    l "Ahhhh!!"

    $ world.ejaculation()

    "But the release doesn't come.{w}\nThe frustration and irritation only gets even stronger, gnawing at my brain."
    l "P...Please...{w}\nPlease let me come..."
    "I start to beg her with tears in my eyes.{w}\nI can't take this... This torture is too much."
    world.battle.enemy[0].name "Hahaha, already crying?{w}\nNever had a guy cry as fast as that."
    world.battle.enemy[0].name "Haha, that just makes me want to tease ya more!{w}\nCome on, let me hear that delicious scream of yours as I rape ya!"
    "The Basilisk ignores my begging, and continues her violent rape."
    l "Ahhh!!"

    $ world.ejaculation()

    "After only a few moments, I reach another climax.{w}\nThe pleasure explodes in me, but is quickly replaced by the gnawing frustration."
    "This is too much... I need to feel it... I need to release..."
    l "Ahh... L...Let me... Let me c..."
    world.battle.enemy[0].name "Hahaha, sure, I'll let you climax again!"

    $ world.ejaculation()

    l "N...No...{w}\nEjaculate... Let me come!"
    world.battle.enemy[0].name "Hehehe... What a pathetic guy.{w}\nYou want to come inside me that badly?"
    "The Basilisk giggles as she stares right into my eyes.{w}\nA smile of superiority is stuck on her face as she gloats over me."
    world.battle.enemy[0].name "Alright... Then beg me.{w}\nWhat exactly do you want?"
    l "I...I want to come..."
    world.battle.enemy[0].name "Hahaha, that ain't quite it is it?{w}\nIf you aren't perfectly clear, I'll never know what it is!"
    "The Basilisk clamps around my tip again as she grinds against me, forcing me to another frustrating climax."
    l "N...No... Ahhh!"

    $ world.ejaculation()

    "Another incomplete orgasm.{w}\nI'll go insane if I can't release."
    l "I... I want to ejaculate...{w}\nPlease... Please let me come inside you!!"
    world.battle.enemy[0].name "Hahahaha, what a pathetic desire!{w}\nDo you want to impregnate a monster like me that badly?"
    world.battle.enemy[0].name "If ya want it that badly, I guess I got no choice do I?{w}\nHere, I'll let ya come inside me just like you want."

    play sound "audio/se/flash.ogg"
    with flash

    "Her eyes flash with a bright light, suddenly releasing my penis from its petrification."
    l "Ahhhh..."
    "As the sensation returns to my shaft, still inside her vagina, the stimulation is much more powerful than before."
    "The bumps inside of her rub my shaft, now free to experience the pleasure of her vagina."
    "After mere seconds of feeling her, an intense climax starts to wrack my body."
    l "Haaa... Incredible...{w}\nAhhh!!"

    with syasei1
    show basilisk ct03 at topleft zorder 15 as ct
    show basilisk bk05 zorder 10 as bk
    $ world.ejaculation()

    "My back arches upward as I ejaculate, exploding inside of her.{w}\nI gasp in ecstasy as it feels like multiple ejaculations are stacked on top of each other, all being forced out at once."
    "In a trance of ecstasy, I moan as I drown in the bliss of the release finally granted to me."
    world.battle.enemy[0].name "Haha, you came, you came.{w}\nAhh, I love the feeling of semen bursting in me you can only get from denying ejaculations."
    world.battle.enemy[0].name "My pussy got filled with only one burst from that once stone dick of yours...{w}\nBeing able to shoot that much into me, you must have loved it too."
    l "Ahhh..."
    "After finally tasting the ejaculation so long denied to me, my limbs go limp as I start to drool from the comfortable ecstasy of release."

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "After that, I became nothing but her sex toy.{w}\nKeeping my penis half stone, she uses me as nothing but a tool to satisfy her lust."
    "Every so often she releases the petrification to let me come inside her, but she always puts it back."
    "Like that, I've been degraded to little more than furniture for her exclusive use..."
    ".............."

    jump badend

label dragon_start:
    python:
        world.troops[40] = Troop(world)
        world.troops[40].enemy[0] = Enemy(world)

        world.troops[40].enemy[0].name = Character("Девушка-дракон")
        world.troops[40].enemy[0].sprite =  ["dragon st01", "dragon st02", "dragon st03", "dragon h1"]
        world.troops[40].enemy[0].position = center
        world.troops[40].enemy[0].defense = 60
        world.troops[40].enemy[0].evasion = 99
        world.troops[40].enemy[0].max_life = world.troops[40].battle.difficulty(6000, 6000, 9000)
        world.troops[40].enemy[0].henkahp[0] = 2000
        world.troops[40].enemy[0].power = world.troops[40].battle.difficulty(0, 1, 2)

        if persistent.difficulty < 3:
            world.party.player.earth_keigen = 20
        else:
            world.party.player.earth_keigen = 10

        world.troops[40].battle.tag = "dragon"
        world.troops[40].battle.name = world.troops[40].enemy[0].name
        world.troops[40].battle.background = "bg 115"
        world.troops[40].battle.music = 1
        world.troops[40].battle.exp = 30000
        world.troops[40].battle.exit = "lb_0064"

        world.troops[40].battle.init()

    l "Дракон..."
    "Монстр из легенд... стоит прямо передо мной..."
    "Когти, способные разорвать любую броню в клочья...{w}\nПламя, способное обратить в пепел всё что угодно..."
    "Чешуя, способная отразить удар любого оружия..."

    $ world.battle.face(2)

    world.battle.enemy[0].name "Ох, человек?{w} Какая редкая добыча..."
    l "..............."
    "В прошлый раз этот монстр чуть не убил меня.{w}\nМне нужно быть осторожным."
    "Но если я смог выиграть тогда, то в этот раз уж подавно!"

    $ world.battle.face(1)

    $ world.battle.main()

label dragon_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label dragon_v:
    world.battle.enemy[0].name "Н-невозможно...{w} Кто ты вообще такой?!.."

    play sound "se/syometu.ogg"
    hide dragon with crash

    "Девушка-дракон принимает форму гигантской ящерицы!"

    $ world.battle.victory(1)

label dragon_a:
    if world.party.player.bind == 1:
        call dragon_a6
        $ world.battle.main()

    while True:
        $ random = rand().randint(1,5)

        if random == 1:
            call dragon_a1
            $ world.battle.main()
        elif random == 2:
            call dragon_a2
            $ world.battle.main()
        elif random == 3:
            call dragon_a3
            $ world.battle.main()
        elif random == 4 and world.battle.delay[0] > 6:
            call dragon_a4
            $ world.battle.main()
        elif random == 5 and world.battle.delay[1] > 7:
            call dragon_a5
            $ world.battle.main()


label dragon_a1:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "You'll cover this hand of mine with your semen!",
        "I'll squeeze out your semen!",
        "Is it feeling good?"
    ])

    $ world.battle.show_skillname("Dragon Hand")
    play sound "audio/se/ero_koki1.ogg"

    "The Dragon strokes Luka's penis!"

    $ world.battle.skillcount(2, 3509)

    python:
        world.battle.enemy[0].damage = {1: (120, 140), 2: (135, 155), 3: (150, 170)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h1")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label dragon_a2:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll get a good taste of that penis of yours...",
        "Nnnn... Come inside my mouth when you can't take it any longer...",
        "I'll suck out your semen..."
    ])

    $ world.battle.show_skillname("Dragon Mouth")
    play sound "audio/se/ero_buchu2.ogg"

    "The Dragon Girl sucks on Luka's penis with her giant mouth!"

    $ world.battle.skillcount(2, 3510)

    python:
        world.battle.enemy[0].damage = {1: (130, 150), 2: (145, 165), 3: (160, 180)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h2")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label dragon_a3:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Bury yourself in these breasts of mine...",
        "My breasts are soft, aren't they?",
        "It's fine to cover me in semen..."
    ])

    $ world.battle.show_skillname("Dragon Breasts")
    play sound "audio/se/ero_koki1.ogg"

    "The Dragon Girl squeezes Luka's penis between her soft breasts!"

    $ world.battle.skillcount(2, 3511)

    python:
        world.battle.enemy[0].damage = {1: (140, 160), 2: (155, 175), 3: (170, 190)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h3")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label dragon_a4:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "My tongue will make you writhe in pleasure!",
        "I'll cover you in my saliva!",
        "You can't endure my tongue, can you?"
    ])

    $ world.battle.show_skillname("Dragon Tongue")
    play sound "audio/se/ero_pyu2.ogg"

    "The Dragon Girl's dripping tongue licks Luka!"

    $ world.battle.skillcount(2, 3512)
    $ world.battle.enemy[0].damage = {1: (80, 90), 2: (87, 97), 3: (94, 104)}
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])
    play sound "audio/se/ero_pyu2.ogg"

    "Her huge tongue coils around Luka's body, licking him clean!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)
    play sound "audio/se/ero_pyu2.ogg"

    "The tip of her tongue plays with Luka's penis!{w}{nw}"

    python:
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h4")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label dragon_a5:
    if not world.party.player.surrendered:
        world.battle.enemy[0].name "...It's time to prey on you!"

    $ world.battle.show_skillname("Bite")

    "The Dragon Girl opens her giant mouth wide, and bites down on Luka!"

    $ world.party.player.bind = 1
    $ world.party.player.status_print()

    "Luka's been sucked into the Dragon Girl's mouth!"

    $ world.battle.hide_skillname()

    if world.party.player.surrendered:
        return

    $ world.battle.face(2)

    if world.battle.first[0] == 1:
        world.battle.enemy[0].name "You won't get away this time...{w}\nFirst, I'll suck you dry..."
    else:
        world.battle.enemy[0].name "I'll devour you like this...{w}\nFirst, I'll suck you dry..."

    $ world.battle.first[0] = 1

    if world.party.player.earth == 0 and persistent.difficulty < 3:
        $ world.battle.enemy[0].power = 5
    elif world.party.player.earth == 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 6
    elif world.party.player.earth > 0 and persistent.difficulty < 3:
        $ world.battle.enemy[0].power = 2
    elif world.party.player.earth > 0 and persistent.difficulty == 3:
        $ world.battle.enemy[0].power = 3

    return


label dragon_a6:
    "Luka is stuck inside the Dragon Girl's mouth..."

    $ world.battle.cry(world.battle.enemy[0].name, [
        "I'll lick you until you dissolve... Hehe.",
        "Hehe... I'll enjoy this.",
        "Let me suck out that male juice of yours.",
        "I'll wrap that body of yours with my tongue...",
        "You'll finish inside my mouth..."
    ])

    $ world.battle.show_skillname("Oral Fixation")
    play sound "audio/se/ero_makituki3.ogg"

    $ world.battle.cry(narrator, [
        "Her giant tongue licks Luka's body!",
        "Her female upper body hugs Luka inside her giant mouth, running her fingers all over Luka!",
        "She sucks on Luka's penis with her female upper body's mouth!",
        "Her giant tongue squeezes Luka's body!",
        "The tip of her giant tongue plays with Luka's penis!"
    ], True)

    $ world.battle.skillcount(2, 3513)

    python:
        world.battle.enemy[0].damage = {1: (120, 140), 2: (120, 140), 3: (160, 180)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump(f"{world.battle.tag}_h5")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label dragon_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Сдаёшься?..{w}\nТогда я воспользуюсь тобой как игрушкой!"

    $ world.battle.enemy[0].attack()

label dragon_onedari:
    $ cmd.onedari_clear()
    $ list1 = "Dragon Hand"
    $ list2 = "Dragon Mouth"
    $ list3 = "Dragon Breasts"
    $ list4 = "Dragon Tongue"
    $ list5 = "Oral Fixation"

    if persistent.skills[3509][0] > 0:
        $ list1_unlock = 1

    if persistent.skills[3510][0] > 0:
        $ list2_unlock = 1

    if persistent.skills[3511][0] > 0:
        $ list3_unlock = 1

    if persistent.skills[3512][0] > 0:
        $ list4_unlock = 1

    if persistent.skills[3513][0] > 0:
        $ list5_unlock = 1

    $ world.battle.call_cmd("onedari")

    if world.battle.result == 1:
        jump dragon_onedari1
    elif world.battle.result == 2:
        jump dragon_onedari2
    elif world.battle.result == 3:
        jump dragon_onedari3
    elif world.battle.result == 4:
        jump dragon_onedari4
    elif world.battle.result == 5:
        jump dragon_onedari5
    elif world.battle.result == -1:
        jump dragon_main


label dragon_onedari1:
    call onedari_syori

    world.battle.enemy[0].name "You want my hand?"

    while True:
        call dragon_a1


label dragon_onedari2:
    call onedari_syori

    world.battle.enemy[0].name "You want my mouth?"

    while True:
        call dragon_a2


label dragon_onedari3:
    call onedari_syori

    world.battle.enemy[0].name "You want my breasts?"

    while True:
        call dragon_a3


label dragon_onedari4:
    call onedari_syori

    world.battle.enemy[0].name "You want my tongue?"

    while True:
        call dragon_a4


label dragon_onedari5:
    call onedari_syori_k

    world.battle.enemy[0].name "You want me to suck on your body?"

    if not world.party.player.bind:
        call dragon_a5

    while True:
        call dragon_a6


label dragon_h1:
    $ world.party.player.moans(1)
    with syasei1
    show dragon bk03 zorder 10 as bk
    $ world.ejaculation()

    "As the Dragon Girl strokes Luka's penis with her hand, he comes, covering her hands with semen."

    $ world.battle.lose()
    $ world.battle.count([5, 11])
    $ bad1 = "Luka was forced to an orgasm by the Dragon Girl's handjob."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Already?.. Hehe."

    jump dragon_h


label dragon_h2:
    $ ikigoe = 1
    show dragon bk01 zorder 10 as bk
    call ikigoe
    with syasei1
    $ world.ejaculation()

    "As the Dragon Girl sucks on Luka's penis, he comes, filling her mouth with semen."

    $ world.battle.lose()
    $ world.battle.count([4, 11])
    $ bad1 = "Luka was forced to an orgasm by the Dragon Girl's blowjob."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Already?.. Hehe."

    jump dragon_h


label dragon_h3:
    $ world.party.player.moans(1)
    with syasei1
    show dragon bk02 zorder 10 as bk
    $ world.ejaculation()

    "As the Dragon Girl's soft breasts squeeze Luka's penis, he comes, covering her chest with semen."

    $ world.battle.lose()
    $ world.battle.count([7, 11])
    $ bad1 = "Luka was forced to an orgasm by the Dragon Girl's tit fuck."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Already?.. Hehe."

    jump dragon_h


label dragon_h4:
    $ world.party.player.moans(2)
    with syasei1
    show dragon bk04 zorder 10 as bk
    $ world.ejaculation()

    "As the Dragon Girl's huge tongue licks Luka's body, he comes, covering it with semen."

    $ world.battle.lose()
    $ world.battle.count([4, 11])
    $ bad1 = "Luka was forced to an orgasm by the Dragon Girl's huge tongue."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Already?.. Hehe."

    jump dragon_h


label dragon_h5:
    $ world.party.player.moans(2)
    with syasei1
    show dragon h2
    $ world.ejaculation()

    "Stuck inside the Dragon Girl's huge mouth, Luka comes, covering both her female upper body and giant tongue with semen."

    $ world.battle.lose()
    $ world.battle.count([4, 11])
    $ bad1 = "While stuck inside the Dragon Girl's huge mouth, Luka was forced to an orgasm."
    $ world.battle.face(2)

    world.battle.enemy[0].name "Already?.. Hehe."

    jump dragon_h


label dragon_h:
    $ bad2 = "With his whole body sucked on inside her giant mouth, Luka dissolved away."

    if persistent.hsean_cut == 1:
        call hsean_cut2
    elif persistent.hsean_cut == 2:
        call hsean_cut

    if world.party.player.bind:
        play hseanwave "audio/se/hsean21_tentacle2.ogg"
        jump dragon_hb

    l "Ahh..."
    world.battle.enemy[0].name "Well then, it's time to eat you..."
    l "Ahhh!"
    "The Dragon's long tongue coils around my limp body...{w}{nw}"

    play hseanwave "audio/se/hsean21_tentacle2.ogg"
    hide ct
    hide bk
    show dragon h1
    $ renpy.transition(dissolve, layer="master")

    extend "{w=.7}\nAnd easily pulls me inside her mouth!"


label dragon_hb:
    l "Ahhh... S... Stop it!"
    "Her hot, slimy mouth closes around my body as her long tongue stops my struggling."
    "It's like I'm stuck in a giant, soft creature...{w}\nHer saliva drips out of her mouth, covering my body in warm liquid."
    world.battle.enemy[0].name "I won't chew you, nor swallow you whole...{w}\nIt's been a while since I've had human, so I'd like to savor your taste until you melt away."
    l "N...No way... Ahh!"
    "Her long tongue starts to wriggle, pushing against my body.{w}\nAt the same time, her giant mouth starts to move."
    "The giant tongue wraps around my body, slowly tightening as it moves."
    l "Ahhh..."
    "A pleasant feeling starts to fill my body as her huge tongue toys with me inside her mouth."
    "Every time her tongue brushes over my penis, I shiver.{w}\nThe feeling of that soft, wet tongue brushing against me is too much to take."
    world.battle.enemy[0].name "...Shivering in pleasure from having your body sucked on?{w}\nWhat pitiful prey..."
    "She starts to move her tongue with more power, licking every corner of my body."
    l "Ahh... That feels good..."
    "As the stimulation attacks me, I unconsciously thrust my waist forward, sinking my penis into her soft tongue."
    "I moan in pleasure at the accidental stimulation, my sensitive penis rubbing against her saliva covered tongue."
    l "Haaaa..."
    world.battle.enemy[0].name "Pitiful men... Always wanting to be licked..."
    "Saying that, she moves the tip of her giant tongue into my groin, and starts to play with my penis."
    "Her soft, wet tongue runs up and down my penis, tiny by comparison."
    l "Ahhh!!"
    "I tremble in ecstasy from the direct stimulation."
    "My penis easily sinks into her giant tongue as it pushes against me, quickly bringing me close to an orgasm."
    l "Ahh... I'm going to come..."
    world.battle.enemy[0].name "Already?..{w} Men have gotten even more pathetic, it seems...{w}\nHere, I'll finish you off..."
    "Her wet tongue pushes against my penis even harder."
    "Sinking into her soft tongue, I give in as she rubs it against me."
    l "Ahhhh!!"

    with syasei1
    show dragon h2
    $ world.ejaculation()

    "With my body restrained by her tongue, and my penis played with by the tip of that same tongue, I'm forced to a pitiful orgasm."
    "But even though I'm humiliated, it feels so good..."
    l "Ahhh..."
    "I sigh as I feel even more strength drain from my body."
    world.battle.enemy[0].name "All it takes is a little moving from my tongue...{w}\nHere, I'll do it even more..."
    "Her tongue starts to squirm and wriggle around, with my body still firmly in its hold."
    "I'm stuck inside her mouth, and just being played with until she eats me...{w}\nBut even though it's horrifying, I can't help but indulge myself in the pleasure."
    l "Ahh... That feels good..."
    world.battle.enemy[0].name "Hehe, what a foolish boy.{w}\nAre you really that happy at being preyed on?"
    "The tip of her giant tongue continues to play with my penis.{w}\nSometimes licking it, sometimes flicking it, sometimes just pressing into me..."
    l "Ahhhh!!"
    "My body shakes with the intense pleasure.{w}\nBut with her giant tongue coiled around me, I can barely even do that."
    "Even though I'm stuck inside her mouth, and able to be swallowed whole at any moment...{w}\nDespite the humiliation and fear, the ecstasy blocks out all my other thoughts."
    l "Ah!{w} Ahh!"
    "My lower body, covered in her saliva, starts to ache as I feel another orgasm about to be licked out of me."
    l "Ahh... C...Coming..."

    with syasei1
    show dragon h3
    $ world.ejaculation()

    "I explode inside her mouth, covering her giant tongue with semen."
    "She stops playing with me for a moment, and I hold my breath in fear thinking she's about to swallow me."
    "But after a few moments, she speaks instead."
    world.battle.enemy[0].name "Hehe, what delicious semen.{w}\nAnd your penis tastes quite delicious, too..."
    l "Ahhh!"
    "Her tongue starts to move even faster, rubbing my penis quicker than I thought could even be possible.{w}\nAt the same time, the rest of her tongue around my body tenses up, tightening around me."
    "Her mouth itself gets a little smaller, as I feel a suction start to pull at me.{w}\nIt's like I'm a piece of candy, and she's using her entire mouth to suck on me..."
    world.battle.enemy[0].name "Hehehe... I'm going to suck on you until you dissolve.{w}\nWell? That's what you wanted, isn't it?"
    l "Ahh... N... No..."
    "The fear that was pushed aside by the pleasure comes back.{w}\nI don't want to be dissolved inside her mouth..."
    "I struggle for my life, desperate to escape.{w}\nBut there's no way out... I'm stuck inside her mouth, and her tongue is far too powerful..."
    world.battle.enemy[0].name "I won't let you go, you're just my food now.{w}\nI'll suck on you until there's nothing left."
    world.battle.enemy[0].name "But don't worry, I'll keep licking that penis of yours so you can die in pleasure.{w}\nAren't I nice? Hehehe..."
    l "Aieee!"

    if persistent.vore == 1:
        jump vore

    play hseanwave "audio/se/hsean22_tentacle3.ogg"

    "Liquid starts to drip out inside of her, covering my body in her warm juices.{w}\nAt the same time, the squirming and wriggling of her tongue around me gets even more intense."
    "Is this... Is this her digestive juices?{w}\nShe really is going to dissolve me into nothing as she licks me..."
    "As the realization of my fate finally strikes home, my thoughts are interrupted by her tongue, licking my penis with powerful licks."
    l "Ahhhh!!"

    with syasei1
    show dragon h4
    $ world.ejaculation()

    "As her giant tongue flicks my penis, I reach a climax, covering it with semen.{w}\nEven though she's dissolving me, I still came..."
    world.battle.enemy[0].name "See, it feels good, doesn't it?{w}\nYou can enjoy it as I dissolve you and make your body itself into nourishment for me..."
    world.battle.enemy[0].name "Of course, I'll enjoy eating that semen of yours as you shoot it out. Hehe."
    l "N...No... Stop... Ahhh!"
    "The loud, wet sounds of her sucking on my body fill my ears, drowning out the sounds of her laughter."
    "Despair stabs me through the heart as I writhe around inside her mouth, unable to even see the outside world..."
    l "No.... Help me..."

    with syasei1
    $ world.ejaculation()

    "Even as I beg for mercy, I come, spraying semen onto her giant tongue."
    world.battle.enemy[0].name "Look, your body is getting all soft...{w}\nYou're going to dissolve away soon..."
    "The warm liquid that I thought was saliva is covering my body.{w}\nI want to get it off, but my arms and legs are bound inside her tongue."
    "Feeling helpless, I slowly give myself over to the strange sense of comfort filling me.{w}\nAs my body melts away, my penis continues to tremble in joy."
    l "Ah...aaa..."

    with syasei1
    $ world.ejaculation()

    "As she sucks on my body, I relax.{w}\nForgetting the fear and horror at what's to come, I just focus on nothing but the pleasure."
    "Covered in her digestive juices, my body melts away.{w}\nTurning liquidy, it mixes with her saliva, swallowed down her giant throat every so often."
    l "...A...h..."
    "All the while, her tongue continues to play with me."
    world.battle.enemy[0].name "Hehe, your body tastes absolutely delicious.{w}\nI'm so happy I was able to prey on you."
    l "A....{w}\n................"

    scene bg black with Dissolve(3.0)
    stop hseanwave fadeout 1

    "Thus, the young boy known as Luka was preyed on.{w}\nHeld inside the Dragon Girl's mouth, she sucked on him until he dissolved into nothing."
    ".................."

    jump badend

label salamander_ng_start:
    python:
        world.troops[41] = Troop(world)
        world.troops[41].enemy[0] = Enemy(world)

        world.troops[41].enemy[0].name = Character("Саламандра")
        world.troops[41].enemy[0].sprite =  ["salamander st02", "salamander st02", "salamander st03", "salamander st02"]
        world.troops[41].enemy[0].position = center
        world.troops[41].enemy[0].defense = 70
        world.troops[41].enemy[0].evasion = 90
        world.troops[41].enemy[0].max_life = world.troops[41].battle.difficulty(25000, 25500, 26500)
        world.troops[41].enemy[0].power = world.troops[41].battle.difficulty(0, 1, 2)

        if persistent.difficulty < 3:
            world.party.player.earth_keigen = 30
        else:
            world.party.player.earth_keigen = 1

        world.troops[41].battle.tag = "salamander_ng"
        world.troops[41].battle.name = world.troops[41].enemy[0].name
        world.troops[41].battle.background = "bg 115"

        if persistent.music:
            world.troops[41].battle.music = 23
        else:
            world.troops[41].battle.music = 10

        world.troops[41].battle.exp = 1500000
        world.troops[41].battle.exit = "lb_0065"
        world.troops[41].battle.ng = True

        world.troops[41].battle.init(0)

    $ world.battle.face(2)
    $ world.party.player.skill_set(2)
    world.battle.enemy[0].name "Хорошо...{w}\nПолучается, ты не трус."
    l "Я никогда не убегаю!{w}\nЕсли мне придётся победить тебя, чтобы ты присоединилась ко мне, то именно так я и сделаю!"
    world.battle.enemy[0].name "Хм-м, ты мне уже нравишься.{w}\nДавай-ка посмотрим, действительно ли ты так силён, каким хочешь казаться."
    "Саламандра должна быть сильнейшим духом...{w}\nЕсли она действительно в этот раз будет сражаться всерьёз, то мне следует быть осторожным!"

    $ world.battle.world.battle.nostar = 2
    $ world.battle.enemy[0].fire = 1
    $ world.battle.enemy[0].fire_turn = -1

    $ world.battle.main()

label salamander_ng_nostar:
    $ world.battle.face(3)
    world.battle.enemy[0].name "Я так не думаю!"
    $ world.battle.counter()
    $ world.battle.show_skillname("Огненный шар")
    play sound "se/fire1.ogg"
    "Саламандра быстро заряжает свою магию...{w}{nw}"
    play sound "se/bom3.ogg"
    extend "\nПосле чего стреляет шаром огня прямо в звезду, уничтожая её!"

    jump salamander_ng_a

label salamander_ng_main:
    python:
        world.battle.call_cmd()

        if world.battle.result == 1 and world.party.player.bind:
            world.battle.common_struggle()
        elif world.battle.result == 1:
            world.battle.common_attack()
        elif world.battle.result == 2 and world.battle.enemy[0].power:
            world.battle.common_struggle()
        elif world.battle.result == 2 and not world.battle.enemy[0].power:
            world.battle.common_struggle(True)
        elif world.battle.result == 3:
            world.battle.common_guard()
        elif world.battle.result == 4:
            world.battle.common_wait()
        elif world.battle.result == 5:
            renpy.jump(f"{world.battle.tag}_kousan")
        elif world.battle.result == 6:
            renpy.jump(f"{world.battle.tag}_onedari")
        elif world.battle.result == 7:
            world.battle.common_skill()

label salamander_ng_v:
    show salamander st13
    world.battle.enemy[0].name "?!{w}\nКак это вообще возможно?!"
    world.battle.enemy[0].name "Я проиграла..."
    show salamander st17
    world.battle.enemy[0].name "......................"
    "Секунду Саламандра пристально смотрит на меня.{w}\nВ конце концов её взгляд смягчается."
    show salamander st16
    world.battle.enemy[0].name "... Могу ли я доверить тебе использовать мою силу лишь во имя добра?{w}\nДействительно ли ты достоин этого?"
    show salamander st18
    l "Я никогда не использовал твою силу таким способом..."
    l "Мне нужна сила духов, чтобы защищать остальных, а не вредить им."
    "Эта единственная причина, по которой я вообще взял в руки меч..."
    show salamander st16
    world.battle.enemy[0].name "...................{w}\nХорошо.{w} Я признаю своё поражение."
    show salamander st18

    $ world.battle.victory(1)

label salamander_ng001:
    $ world.battle.face(3)
    world.battle.enemy[0].name "Чёрт возьми!{w}\nА ты и правда силён..."
    l "Видишь?{w}\nОчевидно же, что я достоин твоей силы."
    "Если уж я в первый раз заслужил её доверие, то нет никаких причин, чтобы она в этот раз отказалась! Ведь так?"
    $ world.battle.face(1)
    "И правда...{w}\nНе похоже, что я смогу победить этой тактикой..."
    l "Получается ты идёшь со мной?"
    world.battle.enemy[0].name "...............{w}{nw}"
    $ world.battle.face(2)
    extend "\nУ меня есть более хорошая идея..."
    "Похотливая ухмылка расползается по её лицу."
    world.battle.enemy[0].name "Лучше вместо грубой силы я перейду к атакам удовольствия..."
    l "Ой, да брось! Ты серьёзно?!"
    world.battle.enemy[0].name "Если ты действительно силён, то это не должно составить никаких проблем. Правильно?"
    l "..............."
    "Почему.{w}\nНу почему всё должно быть так сложно?"
    "И похоже нет никакого способа избежать этой битвы..."
    $ world.party.player.earth_keigen = 5+world.party.player.gnome_buff
    $ world.battle.stage[1] = 1

label salamander_ng_a:
    if world.battle.enemy[0].life < world.battle.enemy[0].max_life/2 and world.battle.stage[1] < 1 and world.party.player.life > 1200:
        call salamander_ng001

    if not world.party.player.bind:
        $ world.battle.progress = 0
    elif world.party.player.bind:
        jump salamander_ng_a8

    if world.battle.stage[1] < 1:
        $ random = rand().randint(1,5)

        if random == 1:
            call salamander_ng_a1
            $ world.battle.main()
        elif random == 2:
            call salamander_ng_a2
            $ world.battle.main()
        elif random == 3:
            call salamander_ng_a3
            $ world.battle.main()
        elif random == 4:
            call salamander_ng_a4
            $ world.battle.main()
        elif random == 5:
            call salamander_ng_a5
            $ world.battle.main()

    elif world.battle.stage[1] > 0:
        $ random = rand().randint(1,4)

        if random == 1:
            call salamander_ng_a7
            $ world.battle.main()
        elif random == 2:
            call salamander_ng_a11
            $ world.battle.main()
        elif random == 3:
            call salamander_ng_a9
            $ world.battle.main()
        elif random == 4:
            call salamander_ng_a10
            $ world.battle.main()

label salamander_ng_a1:
    if world.battle.enemy[0].charge:
        jump salamander_ng_a

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Сможешь ли ты пережить это пламя?",
        "Надеюсь ты действительно столь силён, иначе это будет больно...",
        "Будет немного больно..."
    ])

    $ world.battle.show_skillname("Сгори!")
    play sound "audio/se/fire1.ogg"

    "Воздух вокруг Луки воспламеняется!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 46:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 41:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 36:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 26:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 21:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 16:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 21:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 16:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 11:
                    world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (600, 620), 2: (750, 770), 3: (800, 820)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 10, 2: 10, 3: 10}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], 1)

        world.battle.hide_skillname()
        world.battle.face(1)

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label salamander_ng_a2:
    if not world.battle.enemy[0].charge:
        jump salamander_ng_a

    world.battle.enemy[0].name "Тебе не пережить эту атаку!"

    $ world.battle.show_skillname("Метеорит")
    play sound "audio/se/dageki.ogg"

    "Саламандра высвобождает всю свою силу!"
    "Расплавленная глыба поднимается из лавы и падает на Луку!"

    pause 0.3
    window hide
    hide screen hp
    play sound "audio/se/bom3.ogg"
    show effect daystar ng zorder 30 with ImageDissolve("images/Transition/mask01.webp", 1.5, hard=True)
    show effect daystar ng zorder 30:
        align (0.5, 0.5)
        pause 1.0
        linear 1.5 zoom 3.0
    $ renpy.pause(1.6, hard=True)
    show color white
    hide effect with Dissolve(1.5)
    hide color with Dissolve(2.0)
    window show
    show screen hp
    window auto

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 86:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 81:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 76:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 81:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 76:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 71:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 71:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 66:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 61:
                    world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (2000, 2200), 2: (2600, 2800), 3: (3200, 3500)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 5, 2: 5, 3: 5}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], 1)

        world.battle.hide_skillname()
        world.battle.face(1)

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label salamander_ng_a3:
    if world.battle.enemy[0].charge:
        jump salamander_ng_a

    world.battle.enemy[0].name "Съешь это!"

    $ world.battle.show_skillname("Огненный кулак")
    play sound "audio/se/fire1.ogg"

    "Пламя пробегает вверх-вниз по руке Саламандры!{w}{nw}"

    play sound "audio/se/dageki.ogg"

    extend "\nПосле чего, апперкотом она бьёт Луку!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 101:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 96:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 91:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 96:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 91:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 86:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 81:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 76:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 71:
                    world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (950, 1070), 2: (1150, 1310), 3: (1390, 1560)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 20, 2: 15, 3: 10}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()
        world.battle.face(1)

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label salamander_ng_a4:
    if world.battle.enemy[0].charge:
        jump salamander_ng_a

    world.battle.enemy[0].name "Съешь это!"

    $ world.battle.show_skillname("Огненный пинок")
    play sound "audio/se/fire1.ogg"

    "Пламя пробегает вверх-вниз по ноге Саламандры!{w}{nw}"

    play sound "audio/se/dageki.ogg"

    extend "\nПосле чего, она пинает Луку!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 101:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 96:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 91:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 96:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 91:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 86:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 81:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 76:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 71:
                    world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (830, 960), 2: (860, 990), 3: (910, 1060)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 20, 2: 15, 3: 10}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()
        world.battle.face(1)

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label salamander_ng_a5:
    if world.battle.enemy[0].charge:
        jump salamander_ng_a

    world.battle.enemy[0].name "Этого ты не переживёшь!"

    $ world.battle.show_skillname("Огненный шар")
    play sound "audio/se/fire2.ogg"
    "Вспышка красного света вылетает из лавы в Луку!"

    pause 0.3
    window hide
    hide screen hp
    play sound "audio/se/bom3.ogg"
    show effect fireball zorder 30 with ImageDissolve("images/Transition/mask01.webp", 1.5, hard=True)
    show effect fireball zorder 30:
        align (0.5, 0.5)
        pause 1.0
        linear 1.5 zoom 3.0
    $ renpy.pause(1.6, hard=True)
    show color white
    hide effect with Dissolve(1.5)
    hide color with Dissolve(2.0)
    window show
    show screen hp
    window auto

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 101:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 96:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 91:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 96:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 91:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 86:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 81:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 76:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 71:
                    world.party.player.aqua_guard()

        world.battle.enemy[0].damage = {1: (780, 890), 2: (810, 930), 3: (870, 980)}

        if world.party.player.wind and world.party.player.wind_guard_calc({1: 20, 2: 15, 3: 10}):
            world.party.player.wind_guard()
        elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
            world.party.player.wind_guard()
        else:
            world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], 1)

        world.battle.hide_skillname()
        world.battle.face(1)

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return


label salamander_ng_a6:
    if world.battle.enemy[0].charge:
        jump salamander_ng_a

    world.battle.enemy[0].name ".........................."

    $ world.battle.show_skillname("Разгон")
    play sound "audio/se/power.ogg"

    "Саламандра заряжает свою магию!"

    $ world.battle.enemy[0].charge += 1
    $ world.battle.hide_skillname()

    return


label salamander_ng_a7:
    if world.battle.enemy[0].charge:
        jump salamander_ng_a

    $ world.battle.cry(world.battle.enemy[0].name, [
        "Прикончат ли тебя мои объятья?",
        "Я хороша ещё и в обездвиживающих приёмах...",
        "Позволить мне подойти так близко... Ты всё ещё слишком неопытен!"
    ])

    $ world.battle.show_skillname("Объятия Огненного Духа")
    play sound "audio/se/ero_makituki5.ogg"

    "Саламандра рванулась вперёд и обняла Луку, втискивая его лицо в свою грудь!"

    $ world.battle.skillcount(2, 3516)

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if (world.party.player.aqua > 2 and not world.party.player.skill[3]) or world.party.player.aqua == 2:
                world.party.player.aqua_guard()
            elif world.party.player.aqua > 2:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 86:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 81:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 76:
                    world.party.player.aqua_guard()

            if world.party.player.wind and world.party.player.wind_guard_calc({1: 20, 2: 15, 3: 10}):
                    world.party.player.wind_guard()
                    renpy.return_statement()
            elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
                    world.party.player.wind_guard()
                    renpy.return_statement()

        world.battle.enemy[0].damage = {1: (150, 250), 2: (150, 250), 3: (250, 350)}

        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=1)
        world.party.player.bind = 2
        world.party.player.status_print()

    "Саламандра крепко обняла Луку!"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Хах!{w} Задыхайся в моих сиськах!"

    play sound "audio/se/ero_makituki5.ogg"

    "Саламандра зажала голову Луки между своей грудью!{w}{nw}"

    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=2)
    play sound "audio/se/ero_simetuke1.ogg"

    "Она сильнее сжимает руки, обнимая Луку ещё крепче!{w}{nw}"

    python:
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty], line=3)

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

        if world.party.player.earth == 0 and persistent.difficulty < 3:
            world.battle.enemy[0].power = 3
        elif world.party.player.earth == 0 and persistent.difficulty == 3:
            world.battle.enemy[0].power = 4
        elif world.party.player.earth > 0 and persistent.difficulty < 3:
            world.battle.enemy[0].power = 0
        elif world.party.player.earth > 0 and persistent.difficulty == 3:
            world.battle.enemy[0].power = 1

    return

label salamander_ng_a8:
    if world.battle.enemy[0].charge:
        jump salamander_ng_a
    world.battle.enemy[0].name "Это конец..."

    $ world.battle.show_skillname("Киска огненного Духа")
    show salamander hb1
    play sound "audio/se/ero_pyu5.ogg"

    "Саламанадра с силой садится на член Луки!{w}{nw}"
    $ world.party.player.bind = 2

    $ world.battle.skillcount(2, 3029)

    $ world.battle.enemy[0].damage = (900, 950)
    $ world.battle.enemy[0].deal(world.battle.enemy[0].damage, raw_damage=True)

    play sound "audio/se/ero_simetuke1.ogg"

    "Она сильнее сжимает руки, обнимая Луку ещё крепче!{w}{nw}"

    python:
        world.battle.enemy[0].damage = {1: (45, 55), 2: (35, 45), 3: (45, 55)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    "Лука был изнасилован Саламандрой!"
    world.battle.enemy[0].name "Хи-хи-хи...{w}\nТеперь тебе не сбежать..."
    $ world.battle.progress += 1

    jump salamander_ng_a8b

label salamander_ng_a8b:
    $ world.battle.show_skillname("Киска огненного духа")

    play sound "se/ero_chupa4.ogg"

    "Саламандра двигает своими бёдрами вверх-вниз!"
    $ world.battle.enemy[0].deal((900, 1000), raw_damage=True)

    if not world.party.player.life:
        jump badend_h

    "Саламандра насилует Луку!"
    world.battle.enemy[0].name "Отныне для тебя всё потеряно...{w}\nТеперь всё-ё-ё твоё тело моё..."

    play sound "se/ero_chupa3.ogg"

    "Саламандра садится на член Луки что есть мочи!"

    if persistent.difficulty < 3:
        $ world.battle.enemy[0].deal((850, 900), raw_damage=True)
    else:
        $ world.battle.enemy[0].deal((1200, 1300), raw_damage=True)

    if not world.party.player.life:
        jump badend_h

    world.battle.enemy[0].name "Хм-хм-хм... а ты крепче, чем кажешься.{w}\nОднако!..."

    jump salamander_ng_a8b

label salamander_ng_a9:
    $ world.battle.cry(world.battle.enemy[0].name, [
        "Мне хочется пососать его...",
        "Ммм...~",
        "Дай мне попробовать свою сперму..."
    ])

    $ world.battle.show_skillname("Минет Огненного Духа")
    play sound "audio/se/ero_makituki2.ogg"

    "Саламандра хватает член Луки!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 76:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 66:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 36:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 21:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 16:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 11:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 61:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 56:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 51:
                    world.party.player.aqua_guard()

            if world.party.player.wind and world.party.player.wind_guard_calc({1: 5, 2: 5, 3: 5}):
                    world.party.player.wind_guard()
                    renpy.return_statement()
            elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
                    world.party.player.wind_guard()
                    renpy.return_statement()

    play sound "ngdata/se/lewdsuck.ogg"

    $ world.battle.skillcount(2, 3516)
    python:
        world.battle.enemy[0].damage = {1: (800, 880), 2: (900, 100), 3: (1020, 1050)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label salamander_ng_a10:
    world.battle.enemy[0].name "Моя грудь очень хороша..."

    $ world.battle.show_skillname("Пайзури Огненного Духа")
    play sound "audio/se/ero_paizuri.ogg"

    "Саламандра с силой сжимает член Луки меж своих сисек!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 76:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 66:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 36:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 21:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 16:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 11:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 61:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 56:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 51:
                    world.party.player.aqua_guard()

            if world.party.player.wind and world.party.player.wind_guard_calc({1: 5, 2: 5, 3: 5}):
                    world.party.player.wind_guard()
                    renpy.return_statement()
            elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
                    world.party.player.wind_guard()
                    renpy.return_statement()

    play sound "audio/se/ero_paizuri.ogg"
    "После чего, она сдавливает свою грудь вокруг его члена!"
    play sound "audio/se/ero_pyu1.ogg"

    $ world.battle.skillcount(2, 3516)
    python:
        world.battle.enemy[0].damage = {1: (800, 880), 2: (900, 100), 3: (1020, 1050)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label salamander_ng_a11:
    world.battle.enemy[0].name "Моя грудь очень хороша..."

    $ world.battle.show_skillname("Бёдра Огненного Духа")
    play sound "audio/se/ero_paizuri.ogg"

    "Саламандра с силой сжимает член Луки меж своих бёдер!"

    python:
        if not world.party.player.bind:
            random = rand().randint(1, 100)+world.party.player.undine_buff

            if world.party.player.skill[3]:
                if world.party.player.aqua == 2 and persistent.difficulty == 1 and random < 46:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 2 and random < 36:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua == 2 and persistent.difficulty == 3 and random < 26:
                    world.party.player.aqua_guard()

                if world.party.player.aqua > 2 and persistent.difficulty == 1 and random < 11:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 2 and random < 6:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua > 2 and persistent.difficulty == 3 and random < 1:
                    world.party.player.aqua_guard()
            else:
                if world.party.player.aqua and persistent.difficulty == 1 and random < 21:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 2 and random < 16:
                    world.party.player.aqua_guard()
                elif world.party.player.aqua and persistent.difficulty == 3 and random < 11:
                    world.party.player.aqua_guard()

            if world.party.player.wind and world.party.player.wind_guard_calc({1: 5, 2: 5, 3: 5}):
                    world.party.player.wind_guard()
                    renpy.return_statement()
            elif world.party.player.holy and world.party.player.wind_guard_calc({1: 3, 2: 2, 3: 1}):
                    world.party.player.wind_guard()
                    renpy.return_statement()

    play sound "audio/se/ero_paizuri.ogg"
    "После чего, она сжимает ими его член со всей своей силой."

    $ world.battle.skillcount(2, 3516)
    python:
        world.battle.enemy[0].damage = {1: (800, 880), 2: (900, 100), 3: (1020, 1050)}
        world.battle.enemy[0].deal(world.battle.enemy[0].damage[persistent.difficulty])

        world.battle.hide_skillname()

        if not world.party.player.life:
            renpy.jump("badend_h")

        if world.party.player.max_life > world.party.player.life * 2 and not world.party.player.hphalf:
            world.party.player.common_s1()

        if world.party.player.life > world.party.player.life * 5 and not world.party.player.kiki:
            world.party.player.common_s2()

    return

label salamander_ng_kousan:
    $ world.battle.common_surrender()

    "Поддавшись своей похоти, Лука отдаётся противнику!"

    $ world.battle.face(3)

    world.battle.enemy[0].name "И это всё?{w} Какая же пустая трата времени.{w}{nw}"

    $ world.battle.face(2)

    world.battle.enemy[0].name "Хорошо, раз ты так этого хочешь..."

    call salamander_ng_a8

    $ world.battle.enemy[0].attack()
