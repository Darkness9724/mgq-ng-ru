init -2 python:
    import re, os, collections, hashlib
    import random as random_module
    from autoslot import Slots
    from datetime import date

    if renpy.emscripten:
        import emscripten

    config.keymap["spacebar"] = ["K_SPACE"]

    config.underlay.append(
        renpy.Keymap(
            K_F5=lambda: renpy.run(QuickSave()) if store._save else None,
            K_F6=lambda: renpy.run(QuickLoad())
        )
    )

    def extend_say(who, what, *args, **kwargs):
        try:
            store._mgq_last_raw_what = what

            if who is not extend:
                if who is None:
                    who = store.narrator

                if getattr(who, "record_say", True):
                    store._mgq_last_say_who = who
                    store._mgq_last_say_what = what
                    store._mgq_last_say_args = args
                    store._mgq_last_say_kwargs = kwargs
            else:
                who = who.get_who()
                who.do_extend()
                if kwargs.get("fallback", False):
                    what = store._last_say_what + config.extend_interjection + store._last_raw_what
                else:
                    what = store._mgq_last_say_what + config.extend_interjection + store._mgq_last_raw_what
                args += store._mgq_last_say_args
                kwargs = dict(store._mgq_last_say_kwargs)
                kwargs["interact"] = kwargs.get("interact", True)

            renpy.exports.say(narrator, what, *args, **kwargs)

        finally:
            store._mgq_last_say_what = what
            store._mgq_last_raw_what = ""

    # Legacy way to extend dialogues
    def _extend_say(who, what, line=0):
        if store._mgq_last_who == who and not line:
            store._mgq_last_what = what
            who(store._mgq_last_what, interact=True)
        elif store._mgq_last_who == who and line:
            store._mgq_last_what += what
            who(store._mgq_last_what, slow=False, interact=True)
        else:
            store._mgq_last_what = what
            who(what, interact=True)

        store._mgq_last_who = who

    def dynamic_variable(name, eq):
        _stack = name.split(".")
        if len(name.split(".")) > 1:
            if "[" and "]" in _stack[-1]:
                array = re.split("\[([0-9]*?)\]", _stack[-1])[:-1]
                getattr(eval(".".join(_stack[:-1])), array[0])[int(array[1])] = eq
            else:
                setattr(eval(".".join(_stack[:-1])), _stack[-1], eq)
        else:
            globals()[str(name)] = eq

    def shader_handler(name, vertex_priority, fragment_priority, path="shaders/"):
        shader_components = {
            "variables": renpy.open_file(f"{path}{name}.conf", "UTF-8").read(),
            f"vertex_{vertex_priority}": renpy.open_file(f"{path}{name}.vert", "UTF-8").read(),
            f"fragment_{fragment_priority}": renpy.open_file(f"{path}{name}.frag", "UTF-8").read()
        }

        return shader_components

    def is_integer(var):
        try:
            float(var)
        except ValueError:
            return False
        else:
            return float(var).is_integer()

    def rand():
        try:
            random_module.SystemRandom()
        except NotImplementedError:
            return random_module.Random()
        else:
            return random_module.SystemRandom()

    def len_without_none(array):
        return len([element for element in array if element is not None])

    def error(msg, code):
        renpy.call_screen("error", msg, code)

        print(msg)
        print(f"Error code: {code}")

        sys.exit(code)

label splashscreen:
    python:
        if not sys.version_info.major == 3:
            error(
                f"Требуется минимум Python 3 для игры.\nВы используете Python {sys.version_info.major}.{sys.version_info.minor}.",
                1
            )

        if (not "audio" or not "fonts" or not "gui" or not "images") in os.listdir(renpy.config.gamedir):
            error(
                "Игровые ресурсы не найдены.",
                2
            )

        if hashlib.md5(renpy.open_file("version").read()).hexdigest() != "9cdea4c7a8ab2711ac83303912a73d98":
            error(
                "Найдены устаревшие ресурсы.",
                3
            )

    return

label after_load:
    if renpy.get_screen("skillname"):
        hide screen skillname
    window auto
    return

#просто заглушки
label vore:
    stop hseanwave fadeout 1
    scene bg black with Dissolve(3.0)
    show expression Text("Сцена пожирания была пропущена...", size=50, pos=(0.5, 0.5))

    "Будучи переваренным монстром, Лука закончил свои приключения..."
    "...."

    jump badend

label badend:
    $ world.battle.badend()

label badend_h:
    play sound "audio/se/down.ogg"

    translator "Ты ожидал здесь увидеть хентай, да?{w}\nНо это был я, Даркнесс!"

    $ persistent.count_end += 1
    $ world.battle.lose()
    $ world.battle.badend()
