#!/usr/bin/env python3

import pathlib, shutil, argparse

files = [
    "archlinux",
    "errors.txt",
    "game/audio",
    "game/fonts",
    "game/gui",
    "game/icon.ico",
    "game/icon.png",
    "game/images",
    "game/presplash_background.png",
    "game/presplash_foreground.png",
    "logo.png",
    "logo.xcf",
    "log.txt",
    "presplash.xcf",
    "progressive_download.txt",
    "say.txt",
    "screenshots",
    "start.bat",
    "start.sh",
    "thumb_bg.png",
    "thumb_c.png",
    "thumb.png",
    "thumb.xcf",
    "tools",
    "traceback.txt",
    "version.txt",
    "web-presplash.webp"
]

def mask(outdir):
    pathlib.Path(outdir).mkdir(parents=True, exist_ok=True)

    for file in files:
        shutil.move(file, f"{outdir}/{file}")


def unmask(outdir):
    for file in files:
        shutil.move(f"{outdir}/{file}", file)

def main():
    parser = argparse.ArgumentParser(
        description="Resource file manager for RAPT.\nby Darkness9724"
    )

    parser.add_argument(
        "-x", "--unmask", default=False, action=argparse.BooleanOptionalAction, help="unmask files"
    )
    parser.add_argument(
        "-o", "--output", default="../android", metavar="outdir", type=str, help="output directory"
    )

    args = parser.parse_args()

    if args.unmask:
        unmask(args.output)
    else:
        mask(args.output)

if __name__ == "__main__":
    main()
