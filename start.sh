#!/usr/bin/env sh

ARCH=$(uname)
CWD=$(dirname "$0")
EXE="renpy $CWD"

if [ "$ARCH" == "Darwin" ]; then
    $EXE
else
    #SDL_VIDEODRIVER=$XDG_SESSION_TYPE $EXE
    $EXE
fi
